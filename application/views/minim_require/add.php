
<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-sm-12">
                <div class="box box-success">
                    <div class="box-header ui-sortable-handle">
                        <i class="fa fa-wrench"></i>
                        <h3 class="box-title">Requerimientos mínimos</h3>
                    </div>

                    <form class="form-horizontal" id="addRequirements">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="requeriment" class="col-sm-2 control-label">Requerimiento</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control" name="requirement" id="requirement" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="description" class="col-sm-2 control-label">Descripción</label>
                                <div class="col-sm-10">
                                    <textarea class="form-control textarea" name="description" id="description" required></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary pull-right">Guardar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>

<?php $this->view('footer'); ?>


<script>
    $(document).ready(function() {

        $('#li-contractors').addClass('menu-open');
        $('#ul-contractors').css('display', 'block');

        $('#li-m_requeriments').addClass('menu-open');
        $('#ul-m_requeriments').css('display', 'block');

        $("#addRequirements").submit(function(event) {
            event.preventDefault();

            $.post(
                site_url + "/cMinimum_Requirements/addRequirement",{
                    requirement     :   $("#requirement").val(),
                    description     :   $("#description").val()
                },
                function(data){
                    if (data == 1) {
                        window.location.replace(site_url+"/cMinimum_Requirements/");
                    }
                    else {
                        alert("Error en el proceso...")
                        window.location.replace(site_url+"/cMinimum_Requirements/add");
                    }
                }
            );
        })   
    })
</script>

</body>
</html>


