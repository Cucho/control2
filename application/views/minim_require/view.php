<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-success">
					<div class="box-header ui-sortable-handle">
					    <i class="fa fa-calendar"></i>
						<h3 class="box-title">Proyecto</h3>
				  	</div>

				  	<div class="box-body">
			  			<section class="content">
			  			    <table class="table">
		  		    		    <tbody>
		  		    		    	<tr>
		  		    		    		<th>Id</th>
		  			    				<td><?php echo $requirement[0]['id'];?></td>
		  		    		    	</tr>
		  		    		    	<tr>
		  		    		    		<th>Requerimiento</th>
		  			    				<td><?php echo $requirement[0]['requirement'];?></td>
		  		    		    	</tr>
		  		    		    	<tr>
		  		    		    		<th>Descripción</th>
		  			    				<td><?php echo $requirement[0]['description'];?></td>
		  		    		    	</tr>
		  		    		    	<tr>
		  		    		    		<th>Creado</th>
		  			    				<td><?php echo $requirement[0]['created'];?></td>
		  		    		    	</tr>
		  		    		    	<tr>
		  		    	    			<th>Modificado</th>
		  		    	    			<td><?php echo $requirement[0]['modified'];?></td>
		  		    	    		</tr>
		  		    	    	</tbody>
		  			    	</table>
			  			</section>
				  	</div>
				  	<div class="box-footer">
				  		<a href="<?php echo site_url(); ?>/cMinimum_Requirements/" class="btn btn-primary pull-right" role="button">
	                    <i class='fa fa-undo'></i> Volver
	                </a>
				  	</div>
				</div>
			</div>
		</div>
	</section>
	
</div>

<?php $this->view('footer'); ?>

<script>
    $(document).ready(function() {
      	$('#li-contractors').addClass('menu-open');
        $('#ul-contractors').css('display', 'block');

        $('#li-m_requeriments').addClass('menu-open');
        $('#ul-m_requeriments').css('display', 'block');
    });
</script>
</body>
</html>