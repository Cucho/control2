<div class="content-wrapper">
	<section class="content">
    <div class="row">
      <div class="col-sm-12">
        <div class="box box-success">
          <div class="box-header ui-sortable-handle">
              <i class="fa fa-users"></i>
              <h3 class="box-title">Estado usuarios</h3>
          </div>

          <div class="box-body">
            <section class="content">
                <table id="users_state" class="table table-striped table-bordered table-condensed" style="width:100%;">
                    <thead>
                      <tr>
                        <th width="10%">ID</th>
                        <th>Estado</th>
                        <th>Acción</th>
                      </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </section>
          </div>
          <div class="box-footer"></div>
        </div>
      </div>
    </div>
  </section>
	
</div>

<?php $this->view('footer'); ?>

<script>
  var edit = <?php echo $this->session->userdata('edit'); ?>;
  var del = <?php echo $this->session->userdata('del'); ?>;

    $(document).ready(function() {
      $('#users_state').DataTable({
          "lengthMenu": [[5, 10, 15, 20,], [5, 10, 15, 20]],
          'responsive': true,
          'paging': true,
          'info': true,
          'filter': true,
          'ordering': true,
          // 'stateSave': true,
          'processing':true,
          'serverSide':true,
          'language': {
            "url": base_url + "assets/Spanish.json"
          },
          "order": [[0, "asc"]],
          'ajax': {
            "url": site_url + "/cUsers_State/datatable",
            "type":"POST",
          },
          "columns": [
            { "data": "ID"},
            { "data": "Estado" },
            { "data": "Acción" }
          ],
          "columnDefs": [
            {
              "targets": [0],
              "orderable": true,
              "render": function(data, type, row) {
                return row.id
              }
            },
            {
              "targets": [1],
              "orderable": true,
              "render": function(data, type, row) {
                return row.state
              }
            },
            {
              "targets": [2],
              "orderable": false,
              "render": function(data, type, row) {
                return edit == true && del == true ? `
                  <a href="<?php echo site_url(); ?>/cUsers_State/view?id=`+row.id+`" class="btn btn-primary btn-xs" role="button">
                    <i class='fa fa-search'></i> Ver
                  </a>
                  <a href="<?php echo site_url(); ?>/cUsers_State/edit?id=`+row.id+`" class="btn btn-warning btn-xs" role="button">
                      <i class='fa fa-pencil-square-o'></i> Editar
                  </a>
                  <a href="#" class="btn btn-danger btn-xs" role="button" onclick="delUser_State(`+row.id+`);">
                      <i class='fa fa-trash-o'></i> Eliminar
                  </a>` : edit == true && del == false ? `
                  <a href="<?php echo site_url(); ?>/cUsers_State/view?id=`+row.id+`" class="btn btn-primary btn-xs" role="button">
                    <i class='fa fa-search'></i> Ver
                  </a>
                  <a href="<?php echo site_url(); ?>/cUsers_State/edit?id=`+row.id+`" class="btn btn-warning btn-xs" role="button">
                      <i class='fa fa-pencil-square-o'></i> Editar
                  </a>` : edit == false && del == true ? `
                  <a href="<?php echo site_url(); ?>/cUsers_State/view?id=`+row.id+`" class="btn btn-primary btn-xs" role="button">
                    <i class='fa fa-search'></i> Ver
                  </a>
                  <a href="#" class="btn btn-danger btn-xs" role="button" onclick="delUser_State(`+row.id+`);">
                      <i class='fa fa-trash-o'></i> Eliminar
                  </a>` : edit == false && del == false ? `
                  <a href="<?php echo site_url(); ?>/cUsers_State/view?id=`+row.id+`" class="btn btn-primary btn-xs" role="button">
                    <i class='fa fa-search'></i> Ver
                  </a>` : '';
              }
            }
           ],
        });
      $('#li-users').addClass('menu-open');
      $('#ul-users').css('display', 'block');
    });

    function delUser_State(id) {
      if (confirm('¡Seguro de eliminar!')) {
        $.post(
          site_url + "/cUsers_State/deleteUser_State",{
            id  :   id
        },
        function(data){
          if (data == 1) {
            window.location.reload();
          }
          else {
            alert("Error en el proceso...")
          }
        }
      );
    }
  }
</script>

</body>
</html>
