<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-success">
					<div class="box-header ui-sortable-handle">
					    <i class="fa fa-users"></i>
						<h3 class="box-title">Agregar estado usuario</h3>
				  	</div>

				  	<form class="form-horizontal" id="addUser_State">
			  			<div class="box-body">
			  				<div class="form-group">
			  					<label for="state" class="col-sm-2 control-label">Estado usuario</label>
			  					<div class="col-sm-10">
			  						<input type="text" class="form-control" name="state" id="state" required>
			  					</div>
			  				</div>
			  			</div>
			  			<div class="box-footer">
			  				<button type="submit" class="btn btn-primary pull-right">Guardar</button>
			  			</div>
			  		</form>
				</div>
			</div>
		</div>
	</section>
	
</div>

<?php $this->view('footer'); ?>

<script>
	$(document).ready(function() {
		$("#addUser_State").submit(function(event) {
			event.preventDefault();

			$.post(
				site_url + "/cUsers_State/addUser_State",{
					state : 	$("#state").val()
				},
				function(data){
					if (data == 1) {
						window.location.replace(site_url+"/cUsers_State/");
					}
					else {
						alert("Error en el proceso...")
						window.location.replace(site_url+"/cUsers_State/add");
					}
				}
			);
		});

		$('#li-users').addClass('menu-open');
		$('#ul-users').css('display', 'block');
	});
</script>
</body>
</html>
