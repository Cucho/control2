<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-success">
					<div class="box-header ui-sortable-handle">
					    <i class="fa fa-users"></i>
						<h3 class="box-title">Persona</h3>
				  	</div>
					
					<div class="box-body">
			  			<section class="content">
			  			    <table id="people" class="table">
		  		    		    <tbody>
		  		    		    	<tr>
		  		    		    		<th>Id</th>
		  			    				<td><?php echo $people[0]['id'];?></td>
		  		    		    	</tr>
									<tr>
		  		    		    		<th>Rut</th>
		  			    				<td><?php echo $people[0]['rut'].'-'.$people[0]['digit'];?></td>
		  		    		    	</tr>
                          			<tr>
		  		    	    			<th>Nombre</th>
		  		    	    			<td><?php echo $people[0]['name'];?></td>
		  		    	    		</tr>
                          			<tr>
                          				<th>Apellidos</th>
                          				<td><?php echo $people[0]['last_name'];?></td>
                          			</tr>
                          			<tr>
                          				<th>Dirección</th>
                          				<td><?php echo $people[0]['address'];?></td>
                          			</tr>
                          			<tr>
                          				<th>E-mail</th>
                          				<td><?php echo $people[0]['email'];?></td>
                          			</tr>
                          			<tr>
                          				<th>Teléfono</th>
                          				<td><?php echo $people[0]['phone'];?></td>
                          			</tr>
                                <tr>
                                  <th>NFC Código</th>
                                  <td>
                                    <?php
                                    if(!empty($people[0]['nfc_code']))
                                      echo $people[0]['nfc_code'];
                                    else
                                      echo 'NO ASIGNADO';
                                    ?>
                                  </td>
                                </tr>
                          			<tr>
                          				<th>Acceso total</th>
                          				<td><?php echo ($people[0]['allow_all'] == 1) ? 'Si' : 'No';?></td>
                          			</tr>
                          			<tr>
                          				<th>Es visitado</th>
                          				<td><?php echo ($people[0]['is_visited'] == 1) ? 'Si' : 'No';?></td>
                          			</tr>
                          			<tr>
                          				<th>Interno</th>
                          				<td><?php
                                    if($people[0]['internal'] == 0)
                                      echo 'EXTERNO VISITA';
                                    else if($people[0]['internal'] == 1)
                                      echo 'INTERNO';
                                    else if($people[0]['internal'] == 2)
                                      echo 'EXTERNO CONTRATISTA';
                                  ?></td>
                          			</tr>
                          			<tr>
                          				<th>Departamento</th>
                          				<td><?php 

                          				if(empty($people[0]['departments_id']))
                          					echo 'NO ASIGNADO';
                          				else
                          				{
                          					$this->db->select('department');
                          					$this->db->from('departments');
                          					$this->db->where('id', $people[0]['departments_id']);
                          					$this->db->limit(1);

                          					$res = $this->db->get()->result_array();
                          					if(!empty($res[0]['department']))
                          						echo $res[0]['department'];
                          					else
                          						echo 'DEPARTAMENTO INEXISTENTE';
                          				}

                          				?></td>
                          			</tr>

                                

                          			<tr>
                          				<th>Perfil</th>
                          				<td><?php echo $people[0]['profile'];?></td>
                          			</tr>
                          			<tr>
                          				<th>Empresa</th>
                          				<td><?php echo $people[0]['company'];?></td>
                          			</tr>
                                <tr>
                                  <th>Estado</th>
                                  <td><?php echo $people[0]['state'];?></td>
                                </tr>
		  		    		    	<tr>
		  		    	    			<th>Creado</th>
		  		    	    			<td><?php echo $people[0]['created'];?></td>
		  		    	    		</tr>
		  		    	    		<tr>
		  		    	    			<th>Modificado</th>
		  		    	    			<td><?php echo $people[0]['modified'];?></td>
		  		    	    		</tr>
		  		    		    </tbody>
		  			    	</table>
			  			</section>
				  	</div>
				  	<div class="box-footer">
				  		<a href="<?php echo site_url(); ?>/cPeople/" class="btn btn-primary pull-right" role="button">
	                    <i class='fa fa-undo'></i> Volver
	                </a>
				  	</div>
				</div>
			</div>
		</div>
	</section>

</div>

<?php $this->view('footer'); ?>

<script>
    $(document).ready(function() {
      	$('#li-people').addClass('menu-open');
		$('#ul-people').css('display', 'block');
    });
</script>
</body>
</html>
