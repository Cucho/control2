<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-success">
					<div class="box-header ui-sortable-handle">
					    <i class="fa fa-users"></i>
						<h3 class="box-title">Carga Masiva</h3>
				  	</div>

				  	<form class="form-horizontal" enctype="multipart/form-data" id="addPeople" method="post">
			  			<div class="box-body">
			  				<div class="form-group">
			  					<label for="internal" class="col-sm-2 control-label">Interno</label>
			  					<div class="col-sm-10">
			  						<label class="radio-inline">
								  		<input type="radio" name="internal" id="internal1" value="1" required> Interno
									</label>
									<label class="radio-inline">
										<input type="radio" name="internal" id="internal2" value="0" required> Externo Visita
									</label>
									<label class="radio-inline">
										<input type="radio" name="internal" id="internal3" value="2" required> Externo Contratista
									</label>
			  					</div>
			  				</div>
			  				<div class="form-group">
			  					<label for="companies" class="col-sm-2 control-label">Perfil</label>
			  					<div class="col-sm-5">
			  						<select name="people_profiles" id="people_profiles" class="form-control" required>
			  							<option value="">Seleccione una opción</option>
			  							<?php foreach ($people_profile as $key) { ?>
			  								<option value="<?php echo $key->id; ?>"><?php echo $key->profile; ?></option>
			  							<?php } ?>
			  						</select>
			  					</div>
			  				</div>
			  				<div class="form-group">
			  					<label for="people" class="col-sm-2 control-label">Empresa</label>
			  					<div class="col-sm-5">
			  						<select name="companies" id="companies" class="form-control" required>
			  							<option value="">Seleccione una opción</option>
			  							<?php foreach ($companies as $key) { ?>
			  								<option value="<?php echo $key->id; ?>"><?php echo $key->company; ?></option>
			  							<?php } ?>
			  						</select>
			  					</div>
			  				</div>
			  				
			  				<div class="form-group">
			  					<label for="upload" class="col-sm-2 control-label">Carga excel</label>
			  					<div class="col-sm-5">
			  						<input type="file" class="form-control upi" name="file" id="file" required>
			  					</div>
			  				</div>

			  			</div>
			  			<div class="box-footer">
			  				<input type="submit" class="btn btn-primary pull-right" value="Guardar">
			  			</div>
			  		</form>
				</div>
			</div>
		</div>
	</section>

</div>

<?php $this->view('footer'); ?>

<script>
	$(document).ready(function() {
		$("#addPeople").submit(function(event) {
			event.preventDefault();

			var internal = $("input[name='internal']:checked").val();
			var people_profiles_id 	= $("#people_profiles").val();
			var companies_id = 	$("#companies").val();

			
            var file_upload = document.getElementsByClassName('upi');
            var elementos = new FormData();
            for(var j = 0; j < file_upload.length; j++){
                var archivo = file_upload[j].files;
                for(var i = 0; i < archivo.length; i++){
                    elementos.append('archivo'+i, archivo[i]);
                }
            }

			$.ajax({
	            url: site_url + '/cPeople/excelfile?internal='+internal+'&people_profiles_id='+people_profiles_id+'&companies_id='+companies_id,//upload
	            type: 'post',
	            contentType: false,
	            data: elementos,
	            processData: false,
	            cache: false,
	            success: function(dato) {
	            	if (dato == 1) {
	            		window.location.replace(site_url+"/cPeople/");
	            	}
	            	else if (dato != 1) {
	            		alert("Error durante la carga masiva");
	            		window.location.replace(site_url+"/cPeople/");
	            	}
	            }
	        });
			
		});

		$('#li-people').addClass('menu-open');
		$('#ul-people').css('display', 'block');
	});
</script>
</body>
</html>
