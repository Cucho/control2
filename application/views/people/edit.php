<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-success">
					<div class="box-header ui-sortable-handle">
					    <i class="fa fa-users"></i>
						<h3 class="box-title">Editar persona #<?php echo $people[0]['id']; ?></h3>
				  	</div>

					<form class="form-horizontal" id="editPeople">
  			  			<div class="box-body">
  			  				<div class="form-group">
			  					<label for="rut" class="col-sm-2 control-label">Rut</label>
			  					<div class="col-sm-3">
			  						<input type="text" class="form-control" name="rut" id="rut" value="<?php echo $people[0]['rut'].'-'.$people[0]['digit']; ?>" oninput="checkRut(this)" onfocus="checkRut(this)" placeholder="12345678-9" maxlength="10" required>
			  					</div>
			  				</div>
			  				<div class="form-group">
			  					<label for="name" class="col-sm-2 control-label">Nombre</label>
			  					<div class="col-sm-10">
			  						<input type="text" class="form-control" name="name" id="name" value="<?php echo $people[0]['name']; ?>" required>
			  					</div>
			  				</div>
			  				<div class="form-group">
			  					<label for="last_name" class="col-sm-2 control-label">Apellidos</label>
			  					<div class="col-sm-10">
			  						<input type="text" class="form-control" name="last_name" id="last_name" value="<?php echo $people[0]['last_name']; ?>" required>
			  					</div>
			  				</div>
			  				<div class="form-group">
			  					<label for="address" class="col-sm-2 control-label">Dirección</label>
			  					<div class="col-sm-10">
			  						<input type="text" class="form-control" name="address" id="address" value="<?php echo $people[0]['address']; ?>" required>
			  					</div>
			  				</div>
			  				<div class="form-group">
			  					<label for="email" class="col-sm-2 control-label">E-mail</label>
			  					<div class="col-sm-5">
			  						<input type="email" class="form-control" name="email" id="email" value="<?php echo $people[0]['email']; ?>" required>
			  					</div>
			  				</div>
			  				<div class="form-group">
			  					<label for="phone" class="col-sm-2 control-label">Teléfono</label>
			  					<div class="col-sm-5">
			  						<input type="tel" class="form-control" name="phone" id="phone" value="<?php echo $people[0]['phone']; ?>" required>
			  					</div>
			  				</div>
			  				<div class="form-group">
			  					<label for="nfc_code" class="col-sm-2 control-label">NFC Código</label>
			  					<div class="col-sm-5">
			  						<input class="form-control" name="nfc_code" id="nfc_code" value="0">
			  					</div>
			  				</div>
			  				<div class="form-group">
			  					<label for="allow_all" class="col-sm-2 control-label">Acceso total</label>
			  					<div class="col-sm-10">
			  						<label class="radio-inline">
								  		<input type="radio" name="allow_all" id="internal1" value="1" required> Si
									</label>
									<label class="radio-inline">
										<input type="radio" name="allow_all" id="internal2" value="0" required> No
									</label>
			  					</div>
			  				</div>
							<div class="form-group">
			  					<label for="is_visited" class="col-sm-2 control-label">Es visitado</label>
			  					<div class="col-sm-10">
			  						<label class="radio-inline">
								  		<input type="radio" name="is_visited" id="internal1" value="1" required> Si
									</label>
									<label class="radio-inline">
										<input type="radio" name="is_visited" id="internal2" value="0" required> No
									</label>
			  					</div>
			  				</div>
			  				<div class="form-group">
			  					<label for="internal" class="col-sm-2 control-label">Interno</label>
			  					<div class="col-sm-10">
			  						<label class="radio-inline">
								  		<input type="radio" name="internal" id="internal1" value="1" required> Interno
									</label>
									<label class="radio-inline">
										<input type="radio" name="internal" id="internal2" value="0" required> Externo Visita
									</label>
									<label class="radio-inline">
										<input type="radio" name="internal" id="internal3" value="2" required> Externo Contratista
									</label>
			  					</div>
			  				</div>
			  				<div class="form-group">
			  					<label for="departments_id" class="col-sm-2 control-label">Departamento</label>
			  					<div class="col-sm-5">
			  						<select name="departments_id" id="departments_id" class="form-control" required>
			  							<option value="0">Seleccione una opción</option>
			  							<?php
			  							$this->db->select('id, department');
			  							$this->db->from('departments');
			  							$this->db->order_by('id');
			  							$res = $this->db->get()->result_array();

			  							foreach($res as $r)
			  							{
			  								echo '<option value="'.$r['id'].'">'.$r['department'].'</option>';
			  							}
			  							?>
			  						</select>
			  					</div>
			  				</div>
			  				<div class="form-group">
			  					<label for="companies" class="col-sm-2 control-label">Perfil</label>
			  					<div class="col-sm-5">
			  						<select name="people_profiles" id="people_profiles" class="form-control" required>
			  							<option value="">Seleccione una opción</option>
			  							<?php foreach ($people_profile as $key) { ?>
			  								<option value="<?php echo $key->id; ?>"><?php echo $key->profile; ?></option>
			  							<?php } ?>
			  						</select>
			  					</div>
			  				</div>
			  				<div class="form-group">
			  					<label for="people" class="col-sm-2 control-label">Empresa</label>
			  					<div class="col-sm-5">
			  						<select name="companies" id="companies" class="form-control" required>
			  							<option value="">Seleccione una opción</option>
			  							<?php foreach ($companies as $key) { ?>
			  								<option value="<?php echo $key->id; ?>"><?php echo $key->company; ?></option>
			  							<?php } ?>
			  						</select>
			  					</div>
			  				</div>

			  				<div class="form-group">
			  					<label for="people" class="col-sm-2 control-label">Estado</label>
			  					<div class="col-sm-5">
			  						<select name="states" id="states" class="form-control" required>
			  							<option value="">Seleccione una opción</option>
			  							<?php
			  							$this->db->select('id, state');
			  							$this->db->from('states');
			  							$this->db->order_by('id','asc');
			  							$res = $this->db->get()->result_array();
			  							if(!empty($res))
			  							{
			  								foreach($res as $r)
			  								{
			  									echo '<option value="'.$r['id'].'">'.$r['state'].'</option>';
			  								}
			  							}
			  							?>
			  						</select>
			  					</div>
			  				</div>
  			  			</div>
			  			<div class="box-footer">
			  				<button type="submit" class="btn btn-primary pull-right">Guardar</button>
			  			</div>
			  		</form>
				</div>
			</div>
		</div>
	</section>

</div>

<?php $this->view('footer'); ?>

<script>

	var cuerpo = null;
	var dv = null;

	function checkRut(rut) {
	    // Despejar Puntos
	    var valor = rut.value.replace('.','');
	    // Despejar Guión
	    valor = valor.replace('-','');
	    
	    // Aislar Cuerpo y Dígito Verificador
	    cuerpo = valor.slice(0,-1);
	    dv = valor.slice(-1).toUpperCase();
	    
	    // Formatear RUN
	    rut.value = cuerpo + '-'+ dv
	    
	    // Si no cumple con el mínimo ej. (n.nnn.nnn)
	    if(cuerpo.length < 7) { rut.setCustomValidity("RUT Incompleto"); return false;}
	    
	    // Calcular Dígito Verificador
	    suma = 0;
	    multiplo = 2;
	    
	    // Para cada dígito del Cuerpo
	    for(i=1;i<=cuerpo.length;i++) {
	    
	        // Obtener su Producto con el Múltiplo Correspondiente
	        index = multiplo * valor.charAt(cuerpo.length - i);
	        
	        // Sumar al Contador General
	        suma = suma + index;
	        
	        // Consolidar Múltiplo dentro del rango [2,7]
	        if(multiplo < 7) { multiplo = multiplo + 1; } else { multiplo = 2; }
	  
	    }
	    
	    // Calcular Dígito Verificador en base al Módulo 11
	    dvEsperado = 11 - (suma % 11);
	    
	    // Casos Especiales (0 y K)
	    dv = (dv == 'K')?10:dv;
	    dv = (dv == 0)?11:dv;
	    
	    // Validar que el Cuerpo coincide con su Dígito Verificador
	    if(dvEsperado != dv) 
	    { 
	    	rut.setCustomValidity("RUT Inválido"); 
	    	return false; 
	    }
	    else
	    {
	    	//pequeña validacion
	    	if(dv == 11)
	    		dv = 0;
	    	
	    	if(dv == 10)
	    		dv = 'K';
	    }
	    
	    // Si todo sale bien, eliminar errores (decretar que es válido)
	    rut.setCustomValidity('');
	}

	$(document).ready(function() {

		$('#departments_id').val('<?php echo $people[0]['departments_id'];?>');
		$('#states').val('<?php echo $people[0]["states_id"];?>');

		if (<?php echo $people[0]['allow_all']?> == 1) {
			$('input:radio[name="allow_all"][value="1"]').prop('checked', true);
		}
		else {
			$('input:radio[name="allow_all"][value="0"]').prop('checked', true);
		}

		if (<?php echo $people[0]['is_visited']?> == 1) {
			$('input:radio[name="is_visited"][value="1"]').prop('checked', true);
		}
		else {
			$('input:radio[name="is_visited"][value="0"]').prop('checked', true);
		}

		$('input:radio[name="internal"][value="<?php echo $people[0]['internal']?>"]').prop('checked', true);

		$("#people_profiles").val(<?php echo $people[0]['people_profiles_id']; ?>)
		$("#companies").val(<?php echo $people[0]['companies_id']; ?>)
		$('#nfc_code').val('<?php echo $people[0]['nfc_code'];?>')

		$("#editPeople").submit(function(event) {
			event.preventDefault();

			if (cuerpo == null) { 
				var rut = $("#rut").val();
				rut = rut.replace('-','');
				cuerpo = rut.slice(0,-1);
	    		dv = rut.slice(-1).toUpperCase();
			}

			$.post(
				site_url + "/cPeople/editPeople",{
					id 					: 	<?php echo $people[0]['id']; ?>,
					rut 				: 	cuerpo,
					digit 				: 	dv,
					name 				: 	$("#name").val(),
					last_name 			: 	$("#last_name").val(),
					address 			: 	$("#address").val(),
					email 				: 	$("#email").val(),
					phone 				: 	$("#phone").val(),
					allow_all 			: 	$('input:radio[name=allow_all]:checked').val(),
					is_visited 			: 	$('input:radio[name=is_visited]:checked').val(),
					internal 			: 	$('input:radio[name=internal]:checked').val(),
					people_profiles_id 	: 	$("#people_profiles").val(),
					companies_id 		: 	$("#companies").val(),
					departments_id		: 	$('#departments_id').val(),
					nfc_code			: 	$('#nfc_code').val(),
					states_id			:   $('#states').val()
				},
				function(data){
					if (data == 1) {
						window.location.replace(site_url+"/cPeople/");
					}
					else {
						alert("Error en el proceso...")
						window.location.replace(site_url+"/cPeople/edit?id="+<?php echo $people[0]['id']; ?>);
					}
				}
			);
		});

		$('#li-people').addClass('menu-open');
		$('#ul-people').css('display', 'block');
	});

	

</script>
</body>
</html>
