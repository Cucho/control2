<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-success">
					<div class="box-header ui-sortable-handle">
					    <i class="fa fa-cogs"></i>
						<h3 class="box-title">Editar sensor #<?php echo $sensors[0]['id']; ?></h3>
				  	</div>
					<form class="form-horizontal" id="editSensor">
  			  			<div class="box-body">
  			  				<div class="form-group">
			  					<label for="code" class="col-sm-2 control-label">Code</label>
			  					<div class="col-sm-10">
			  						<input type="text" class="form-control" name="code" id="code" value="<?php echo $sensors[0]['code']; ?>" required>
			  					</div>
			  				</div>
			  				<div class="form-group">
			  					<label for="sensor" class="col-sm-2 control-label">Sensor</label>
			  					<div class="col-sm-10">
			  						<input type="text" class="form-control" name="sensor" id="sensor" value="<?php echo $sensors[0]['sensor']; ?>" required>
			  					</div>
			  				</div>
			  				<div class="form-group">
			  					<label for="description" class="col-sm-2 control-label">Descripción</label>
			  					<div class="col-sm-10">
			  						<input type="text" class="form-control" name="description" id="description" value="<?php echo $sensors[0]['description']; ?>">
			  					</div>
			  				</div>
			  				<div class="form-group">
			  					<label for="ip" class="col-sm-2 control-label">Ip</label>
			  					<div class="col-sm-10">
			  						<input type="text" class="form-control" name="ip" id="ip" value="<?php echo $sensors[0]['ip']; ?>" requerid>
			  					</div>
			  				</div>
			  				<div class="form-group">
			  					<label for="sensors_type" class="col-sm-2 control-label">Tipo sensor</label>
			  					<div class="col-sm-5">
			  						<select name="sensors_type" id="sensors_type" class="form-control" required>
			  							<option value="">Seleccione una opción</option>
			  							<?php foreach ($type as $key) { ?>
			  								<option value="<?php echo $key->id; ?>"><?php echo $key->type; ?></option>
			  							<?php } ?>
			  						</select>
			  					</div>
			  				</div>
			  				<div class="form-group">
			  					<label for="entry" class="col-sm-2 control-label">Orientación</label>
			  					<div class="col-sm-10">
			  							<label class="radio-inline">
								  			<input type="radio" name="entry" id="entry1" value="0" required> Ingreso
										</label>
										<label class="radio-inline">
											<input type="radio" name="entry" id="entry2" value="1" required> Salida
										</label>
										<label class="radio-inline">
											<input type="radio" name="entry" id="entry3" value="2" required> Ambos
										</label>
			  					</div>
			  				</div>
  			  			</div>
			  			<div class="box-footer">
			  				<button type="submit" class="btn btn-primary pull-right">Guardar</button>
			  			</div>
			  		</form>
				</div>
			</div>
		</div>
	</section>
	
</div>


<?php $this->view('footer'); ?>

<script>
	$(document).ready(function() {

		$("#sensors_type").val(<?php echo $sensors[0]['sensors_type']; ?>)
		$('input:radio[name="entry"][value="'+<?php echo $sensors[0]['entry']?>+'"]').prop('checked', true);

		$("#editSensor").submit(function(event) {
			event.preventDefault();

			$.post(
				site_url + "/cSensors/editSensor",{
					id 				: 	<?php echo $sensors[0]['id']; ?>,
					code 			: 	$("#code").val(),
					sensor 			: 	$("#sensor").val(),
					description 	: 	$("#description").val(),
					ip 				: 	$("#ip").val(),
					sensors_type	: 	$("#sensors_type").val(),
					entry 			: 	$("input:radio[name='entry']:checked").val()
				},
				function(data){
					if (data == 1) {
						window.location.replace(site_url+"/cSensors/");
					}
					else {
						alert("Error en el proceso...")
						window.location.replace(site_url+"/cSensors/edit?id="+<?php echo $sensors[0]['id']; ?>);
					}
				}
			);

			$('#li-configuration').addClass('menu-open');
      		$('#ul-configuration').css('display', 'block');

			$('#li-sensors').addClass('menu-open');
			$('#ul-sensors').css('display', 'block');
		});
	});
	
</script>
</body>
</html>
