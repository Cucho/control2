<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-success">
					<div class="box-header ui-sortable-handle">
					    <i class="fa fa-cogs"></i>
					    <i class="fa fa-sign-in"></i>
							<h3 class="box-title">Vincular Sensor - Puerta</h3>
			  	</div>

				  	<form class="form-horizontal" id="addSensors_Doors">
			  			<div class="box-body">
			  				<div class="form-group">
			  					<label for="sensors" class="col-sm-2 control-label">Sensor</label>
			  					<div class="col-sm-5">
			  						<select required class="form-control" name="sensors" id="sensors" required>
			  							<option value="">Seleccione una opción</option>
			  							<?php
		  									foreach($sensors as $a) {
		  										echo '<option value="'.$a['id'].'">'.$a['sensor'].'</option>';
		  									}
		  								?>
			  						</select>
			  					</div>
			  				</div>
			  				<div class="form-group">
		  						<label for="doors" class="col-sm-2 control-label">Puerta</label>
			  					<div class="col-sm-5">
			  						<select required class="form-control" name="doors" id="doors" required>
			  							<option value="">Seleccione una opción</option>
			  							<?php
		  									foreach($doors as $a) {
		  										echo '<option value="'.$a['id'].'">'.$a['door'].'</option>';
		  									}
		  								?>
			  						</select>
			  					</div>
			  				</div>
			  			</div>
			  			<div class="box-footer">
			  				<button type="submit" class="btn btn-primary pull-right">Guardar</button>
			  			</div>
			  		</form>
				</div>
			</div>
		</div>
	</section>

</div>

<?php $this->view('footer'); ?>

<script>
	$(document).ready(function() {
		$("#addSensors_Doors").submit(function(event) {
			event.preventDefault();

			$.post(
				site_url + "/cSensors_Doors/addSensor_Door",{
					sensors 	: 	$("#sensors").val(),
					doors 		: 	$("#doors").val()
				},
				function(data){
					if (data == 1) {
						window.location.replace(site_url+"/cSensors_Doors/");
					}
					else {
						alert("Error en el proceso...")
						window.location.replace(site_url+"/cSensors_Doors/add");
					}
				}
			);
		});

		$('#li-configuration').addClass('menu-open');
      	$('#ul-configuration').css('display', 'block');
      	
		$('#li-sensors').addClass('menu-open');
		$('#ul-sensors').css('display', 'block');
	});
</script>
</body>
</html>
