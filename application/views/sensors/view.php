<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-success">
					<div class="box-header ui-sortable-handle">
					    <i class="fa fa-cogs"></i>
						<h3 class="box-title">Sensor</h3>
				  	</div>
					
					<div class="box-body">
			  			<section class="content">
			  			    <table id="sensors" class="table">
		  		    		    <tbody>
		  		    		    	<tr>
		  		    		    		<th>Id</th>
		  			    				<td><?php echo $sensors[0]['id'];?></td>
		  		    		    	</tr>
									<tr>
		  		    		    		<th>Código</th>
		  			    				<td><?php echo $sensors[0]['code'];?></td>
		  		    		    	</tr>
                          			<tr>
		  		    	    			<th>Sensor</th>
		  		    	    			<td><?php echo $sensors[0]['sensor'];?></td>
		  		    	    		</tr>
                          			<tr>
                          				<th>Descripción</th>
                          				<td><?php echo $sensors[0]['description'];?></td>
                          			</tr>
                          			<tr>
                          				<th>Ip</th>
                          				<td><?php echo $sensors[0]['ip'];?></td>
                          			</tr>
                          			<tr>
                          				<th>Tipo sensor</th>
                          				<td><?php echo $sensors[0]['type'];?></td>
                          			</tr>
                          			<tr>
                          				<th>Orientación</th>
                          				<td><?php 
                          				if($sensors[0]['entry'] == 0)
                          					echo 'Ingreso';
                          				else if($sensors[0]['entry'] == 1)
                          					echo 'Salida';
                          				else
                          					echo 'Ambos';
                          				?></td>
                          			</tr>
                          			<tr>
		  		    	    			<th>Creado</th>
		  		    	    			<td><?php echo $sensors[0]['created'];?></td>
		  		    	    		</tr>
		  		    	    		<tr>
		  		    	    			<th>Modificado</th>
		  		    	    			<td><?php echo $sensors[0]['modified'];?></td>
		  		    	    		</tr>
		  		    		    </tbody>
		  			    	</table>
			  			</section>
				  	</div>
				  	<div class="box-footer">
				  		<a href="<?php echo site_url(); ?>/cSensors/" class="btn btn-primary pull-right" role="button">
	                    <i class='fa fa-undo'></i> Volver
	                </a>
				  	</div>
				</div>
			</div>
		</div>
	</section>
	
</div>

<?php $this->view('footer'); ?>

<script>
    $(document).ready(function() {
    	$('#li-configuration').addClass('menu-open');
      	$('#ul-configuration').css('display', 'block');
      	
      	$('#li-sensors').addClass('menu-open');
		$('#ul-sensors').css('display', 'block');
    });
</script>
</body>
</html>