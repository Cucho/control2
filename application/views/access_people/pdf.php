  <style type="text/css">
    fieldset {
        border: 1px solid #ddd !important;
        margin: 0;
        min-width: 0;
        padding: 10px;       
        position: relative;
        border-radius:4px;
        background-color:#f5f5f5;
        padding-left:10px!important;
    }
    legend {
        font-size:14px;
        font-weight:bold;
        margin-bottom: 0px; 
        width: 35%; 
        border: 1px solid #ddd;
        border-radius: 4px; 
        padding: 5px 5px 5px 10px; 
        background-color: #ffffff;
    }
    .form-control {
      background-color: #ffffff;
      border: 1px solid #light;
      border-radius: 0px;
      box-shadow: none;
      color: #565656;
      height: 20px;
      width: 100%;
      max-width: 100%;
      padding: 7px 12px;
      transition: all 300ms linear 0s;
      font-size: 11px;
    }
    .table {
      width: 100%;
      max-width: 100%;
      margin-bottom: 20px;
      border-spacing: 0;
      border-collapse: collapse;
      display: table;
      border-collapse: separate;
      border-spacing: 2px;
      border-color: grey;
      font-size: 12px;
    }
  </style>
        <center>
          <h3>Formulario de control entrada</h3>
        </center>
        <?php echo $access_people[0]->created; ?>
        <br><br>
        <fieldset>
          <table class="table">
            <tr>
              <td width="15%">Título: </td>
              <td width="30%">
              <?php if (!empty($access_people_form_in[0]->title)) {
                echo '<input readonly style="width:250px;height:50px;" value="'.$access_people_form_in[0]->title.'">'; }
                else{ echo 'N/A'; } ?>
              </td>
              <td width="15%">Observación: </td>
              <td width="30%">
              <?php if (!empty($access_people_form_in[0]->observation)) { ?>
                <textarea readonly style="width:250px;"><?php echo $access_people_form_in[0]->observation; ?></textarea>
              <?php }else { echo 'N/A'; } ?>
              </td>
            </tr>
          </table>
        </fieldset>
        <br>
        <fieldset>
          <table class="table">
            <?php $i = 0;
              if (!empty($access_people_form_inDetail) && !empty($getDetail_Access_People_Form_in_answers)) {
                foreach ($access_people_form_inDetail as $k) { ?>
                  <tr>
                      <td width="15%"><?php echo $k->question; ?>:</td>
                      <td width="60%">
                        <?php if ($k->answers_type_id != 2)  { ?>
                          <input readonly style="width:250px; height:25px;" value="
                          <?php if(!empty($getDetail_Access_People_Form_in_answers[$i]->answer)){
                              echo $getDetail_Access_People_Form_in_answers[$i]->answer; 
                              echo $k->measure;
                            }
                            else {
                              echo 'N/A';
                            } ?>">
                        <?php }else { ?>
                          <textarea readonly style="width:260px;">
                            <?php if(!empty($getDetail_Access_People_Form_in_answers[$i]->answer)) {
                              echo $getDetail_Access_People_Form_in_answers[$i]->answer;
                              echo $k->measure;
                            }
                            else {
                              echo 'N/A';
                            } ?>
                          </textarea>
                        <?php } ?>
                      </td>
                  </tr>
              <?php $i++;
            } } else { echo 'N/A';} ?>
          </table>
        </fieldset>
        <br>
        <h3>Formulario de control salida</h3>
        <fieldset>
          <table class="table">
            <tr>
              <td width="15%">Título: </td>
              <td width="30%">
              <?php if (!empty($access_people_form_in[1]->title)) {
                echo '<input readonly style="width:250px;height:50px;" value="'.$access_people_form_in[1]->title.'">'; } ?>
              </td>
              <td width="15%">Observación: </td>
              <td width="30%">
              <?php if (!empty($access_people_form_in[1]->observation)) { ?>
                <textarea readonly style="width:250px;"><?php echo $access_people_form_in[1]->observation; ?></textarea>
              <?php } ?>
              </td>
            </tr>
          </table>
        </fieldset>
        <br>
        <fieldset>
          <table class="table">
            <?php $i = 0;
              if (!empty($access_people_form_outDetail) && !empty($getDetail_Access_People_Form_out_answers)) {
                foreach ($access_people_form_outDetail as $k) { ?>
                  <tr>
                      <td width="15%"><?php echo $k->question; ?>:</td>
                      <td width="60%">
                        <?php if ($k->answers_type_id != 2)  { ?>
                          <input readonly style="width:250px; height:25px;" value="<?php echo $getDetail_Access_People_Form_out_answers[$i]->answer; ?> <?php  echo $k->measure; ?>">
                        <?php }else { ?>
                          <textarea readonly style="width:260px;">
                            <?php echo $getDetail_Access_People_Form_out_answers[$i]->answer; ?> <?php echo $k->measure; ?>
                          </textarea>
                        <?php } ?>
                      </td>
                  </tr>
              <?php $i++;
            } } ?>
          </table>
        </fieldset>