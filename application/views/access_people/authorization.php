<style type="text/css">
  @import url('https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');

  main {
    min-width: 320px;
    max-width: 1100px;
    padding: 20px;
    margin: 0 auto;
    background: #fff;
  }

  .tab-pane {
    display: none;
    padding: 20px 0 0;
    border-top: 1px solid #ddd;
  }

  input {
    display: none;
  }

  label {
    display: inline-block;
    margin: 0 0 -1px;
    padding: 15px 25px;
    font-weight: 600;
    text-align: center;
    //color: #bbb;
    border: 1px solid transparent;
  }

  label:before {
    font-family: fontawesome;
    font-weight: normal;
    margin-right: 10px;
  }

  label:hover {
    color: #888;
    cursor: pointer;
  }

  input:checked + label {
    color: #555;
    border: 1px solid #ddd;
    border-top: 2px solid orange;
    border-bottom: 1px solid #fff;
  }

  #tab1:checked ~ #content1,
  #tab2:checked ~ #content2,
  #tab3:checked ~ #content3,
  #tab4:checked ~ #content4,
  #tab5:checked ~ #content5 {
    display: block;
  }
</style>
<div class="content-wrapper">
    <section class="content">

        <div class="row">
            <div class="col-sm-12">
                <div class="box box-success">
                    <div class="box-header ui-sortable-handle">
                        <i class="fa fa-sign-in"></i>
                        <h3 class="box-title">Autorización</small></h3>
                    </div>
                    <div class="box-body">
                        <!--                       
                        <ul id="tabs" class="nav nav-tabs" data-tabs="tabs">
                            <li class="active"><a href="#people" data-toggle="tab">Personas</a></li>
                        </ul>
                    -->
                        <div id="my-tab-content" class="tab-content">
                            <div class="tab-pane active" id="people">
                                <div class="col-md-6 col-md-offset-3">
                                    <div class="box-header">
                                        <h3>Autorización Personas:</h3>
                                    </div>
                                    
                                    <form class="form-inline" role="form" method="post" accept-charset="utf-8" id="form-authorization">
                                    
                                        <div class="box-body">
                                            <div class="form-group" text required>
                                                  <label for="input-rut">Rut</label>
                                                  <input autocomplete="off" class="form-control" type="text" name="inputrut" autofocus="autofocus" required="required" maxlength="9" id="inputrut" >&nbsp;&nbsp;&nbsp;&nbsp;
                                                  <button type="submit" class="btn btn-primary">Generar</button>
                                                  <a href="<?php echo site_url(); ?>/cAccess_People/PDF_authorization" class="btn btn-primary">Excel</a>
                                            </div>
                                            <input class="form-control" type="hidden" name="check" value="1"/> 
                                        </div>
                                    </form>

                                    <!-- FORM -->
                                    <form role="form" method="post" accept-charset="utf-8" id="form-authorization2">
                                        <input type="hidden" class="form-control" type="text" name="inputrut" autofocus="autofocus" required="required" maxlength="10" id="inputrut2" >
                                    </form>

                                    <!-- FORM -->
                                    <form role="form" method="post" accept-charset="utf-8" id="form-authorization3">
                                        <input type="hidden" class="form-control" type="text" name="inputrut" autofocus="autofocus" required="required" maxlength="10" id="inputrut3" >
                                    </form>
                                </div>
                                <br>
                                
                                <div class="col-md-12">
                                    <div class="box">
                                        <div class="box-header">
                                            <h4>Registros de ingresos diarios:</h4>
                                        </div>
                                        <div class="box-body">
                                            <table id="table-register_access" class="table table-striped table-bordered table-condensed" style="width:100%;">
                                                <thead>
                                                    <tr>
                                                      <th>RUT</th>
                                                      <th>NOMBRE</th>
                                                      <th>EMPRESA</th>
                                                      <th>VEHICULO</th>
                                                      <th>FECHA</th>
                                                      <th>DETALLE</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="body-register_access">
                                                    <?php foreach ($in as $k) { ?>
                                                        <tr id="<?php echo $k->id; ?>">
                                                            <td name="rut" data-id="<?php echo $k->id; ?>"><?php echo $k->rut.'-'.$k->digit; ?></td>
                                                            <td><?php echo $k->name.' '.$k->last_name; ?></td>
                                                            <td><?php echo $k->company; ?></td>
                                                            <td><?php
                                                            $v_id = $k->vehicles_id;
                                                            if( $v_id != 0)
                                                            {
                                                                $this->db->select('patent');
                                                                $this->db->from('vehicles');
                                                                $this->db->where('id', $v_id);
                                                                $res = $this->db->get()->result_array();
                                                                if(!empty($res[0]['patent']))
                                                                    echo $res[0]['patent'];
                                                                else
                                                                    echo 'N/A';
                                                            }
                                                            else
                                                                echo 'N/A';
                                                            ?></td>
                                                            
                                                            <td id="exit" data-salida="<?php echo $k->end_time; ?>"><?php echo $k->created; ?></td>
                                                            <td>
                                                                <a href="<?php echo site_url(); ?>/cAccess_People/getDetail?id=<?php echo $k->id; ?>" class="btn btn-primary btn-xs" role="button">
                                                                    <i class='fa fa-search'></i> Ver
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="box">
                                        <div class="box-header">
                                            <h4>Registros de salidas diarias:</h4>
                                        </div>
                                        <div class="box-body">
                                            <table id="table-register_out" class="table table-striped table-bordered table-condensed" style="width:100%;">
                                                <thead>
                                                    <tr>
                                                        <th>RUT</th>
                                                        <th>NOMBRE</th>
                                                        <th>EMPRESA</th>
                                                        <th>VEHICULO</th>
                                                        <th>FECHA</th>
                                                        <th>DETALLE</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="body-register_out">
                                                    <?php foreach ($out as $k) { ?>
                                                        <tr id="<?php echo $k->id; ?>">
                                                            <td name="rut" data-id="<?php echo $k->id; ?>"><?php echo $k->rut.'-'.$k->digit; ?></td>
                                                            <td><?php echo $k->name.' '.$k->last_name; ?></td>
                                                            <td><?php echo $k->company; ?></td>
                                                            <td><?php
                                                            $v_id = $k->vehicles_id;
                                                            if( $v_id != 0)
                                                            {
                                                                $this->db->select('patent');
                                                                $this->db->from('vehicles');
                                                                $this->db->where('id', $v_id);
                                                                $res = $this->db->get()->result_array();
                                                                if(!empty($res[0]['patent']))
                                                                    echo $res[0]['patent'];
                                                                else
                                                                    echo 'N/A';
                                                            }
                                                            else
                                                                echo 'N/A';
                                                            ?></td>
                                                            <td><?php echo $k->exit_time; ?></td>
                                                            <td>
                                                                <a href="<?php echo site_url(); ?>/cAccess_People/getDetail?id=<?php echo $k->id; ?>" class="btn btn-primary btn-xs" role="button">
                                                                    <i class='fa fa-search'></i> Ver
                                                                </a>
                                                            </td>
                                                        </tr>
                                                    <?php } ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane" id="vehicles"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <!--  
        MODAL
        -->
        <div id="modal-add-people" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Registrar Persona</h4>
                    </div>
                    <div class="modal-body">
                        <div class="table-responsive">
                            <form id="formModal-add-people">
                            <table class="table table-condensed" style="width:100%;">
                                <tr>
                                    <td><label>Rut</label></td>
                                    <td>
                                        <input type="text" class="form-control" name="rut" id="rut" maxlength="9" required disabled>
                                    </td>
                                </tr>

                                <tr>
                                    <td><label>Nombre</label></td>
                                    <td>
                                        <input type="text" class="form-control" name="name" id="name" required>
                                    </td>
                                </tr>

                                <tr>
                                    <td><label>Apellidos</label></td>
                                    <td>
                                        <input type="text" class="form-control" name="last_name" id="last_name" required>
                                    </td>
                                </tr>

                                <tr>
                                    <td><label>Dirección</label></td>
                                    <td>
                                        <input type="text" class="form-control" name="address" id="address" required>
                                    </td>
                                </tr>

                                <tr>
                                    <td><label>Email</label></td>
                                    <td>
                                        <input type="email" class="form-control" name="email" id="email" required>
                                    </td>
                                </tr>

                                <tr>
                                    <td><label>Teléfono</label></td>
                                    <td>
                                        <input type="number" class="form-control" name="phone" id="phone" required>
                                    </td>
                                </tr>
                                <tr>
                                    <td><label>Perfil</label></td>
                                    <td>
                                        <select name="people_profiles" id="people_profiles" class="form-control" required>

                                        </select>
                                    </td>
                                </tr>

                                <tr>
                                    <td><label>Empresa</label></td>
                                    <td>
                                        <select name="companies" id="companies" class="form-control" required>
                                            
                                        </select>
                                    </td>
                                </tr>

                                <tr>
                                    <td colspan="2">
                                        <button type="submit" class="btn btn-primary" id="btnAgregar">Agregar</button>
                                    </td>
                                </tr>
                            </table>
                            </form>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- MODAL_DETALLE -->
        <div  id="modal-detail_access" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Detalle</h4>
                    </div>
                    <div class="modal-body">
                        <main>
                    
                            <input id="tab1" type="radio" name="tabs" checked>
                            <label for="tab1"><i class="fa fa-list-alt" aria-hidden="true"></i>&nbsp;Datos Acceso</label>

                            <input id="tab2" type="radio" name="tabs">
                            <label for="tab2"><i class="fa fa-user" aria-hidden="true"></i>&nbsp;Persona</label>
                              
                            <input id="tab3" type="radio" name="tabs">
                            <label for="tab3"><i class="fa fa-list-alt" aria-hidden="true"></i>&nbsp;Motivos</label>
                              
                            <input id="tab4" type="radio" name="tabs">
                            <label for="tab4"><i class="fa fa-map-marker" aria-hidden="true"></i>&nbsp;Ubicaciones</label>

                            <div id="content1" class="tab-pane">
                                <fieldset>
                                    <legend>Datos Acceso</legend>

                                    <table style="width: 100%;">
                                        <tr>
                                            <td><label>Portería</label></td>
                                            <td id="td-main_access"></td>    
                                        </tr>

                                        <tr>
                                            <td><label>Horas</label></td>
                                            <td id="td-hours"></td>    
                                        </tr>

                                        <tr>
                                            <td><label>Hora Salida</label></td>
                                            <td id="td-end_time"></td>    
                                        </tr>

                                        <tr>
                                            <td><label>Estado</label></td>
                                            <td id="td-access_state"></td>    
                                        </tr>

                                        <tr>
                                            <td><label>Observación</label></td>
                                            <td>
                                                <textarea readonly class="form-control" rows="6" id="area-observation"></textarea>
                                            </td>    
                                        </tr>
                                    </table>
                                </fieldset>
                            </div>
                              
                            <div id="content2" class="tab-pane">
                                <fieldset>
                                    <legend>Datos Persona</legend>
                                    <table style="width: 100%;">

                                        <tr>
                                            <td><label>Rut</label></td>
                                            <td id="td-rut">Cargando...</td>
                                        </tr>

                                        <tr>
                                            <td><label>Nombre</label></td>
                                            <td id="td-name">Cargando...</td>
                                        </tr>

                                        <tr>
                                            <td><label>Teléfono</label></td>
                                            <td id="td-phone">Cargando...</td>
                                        </tr>

                                        <tr>
                                            <td><label>Email</label></td>
                                            <td id="td-email">Cargando...</td>
                                        </tr>

                                        <tr>
                                            <td><label>Perfil</label></td>
                                            <td id="td-profile">Cargando...</td>
                                        </tr>

                                        <tr>
                                            <td><label>Empresa</label></td>
                                            <td id="td-company">Cargando...</td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </div>
                              
                            <div id="content3" class="tab-pane">
                                <fieldset>
                                    <legend>Motivos de Visita</legend>
                                    <table style="width: 100%;" id="table-reasons_visit">                        
                                        <tr>
                                            <td><label>Motivo</label></td>    
                                            <td name="datos-reasons">Cargando...</td>
                                        </tr>
                                    </table>
                                </fieldset>
                                <br>
                                <fieldset>
                                    <legend>Personas a Visitar</legend>
                                    <table style="width: 100%;" id="table-people_visit">
                                        <tr>
                                            <td><label>Persona</label></td>    
                                            <td name="datos-visit">Cargando...</td>
                                        </tr>
                                    </table>
                                </fieldset>
                            </div>
                            
                            <div id="content4" class="tab-pane">

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <fieldset>
                                                <legend>Zonas</legend>
                                                <table style="width: 100%;" id="table-zones">
                                      
                                                </table>
                                            </fieldset>
                                        </div>

                                        <div class="col-md-4">
                                            <fieldset>
                                                <legend>Áreas</legend>
                                                <table style="width: 100%;" id="table-areas">
                                      
                                                </table>
                                            </fieldset>
                                        </div>

                                        <div class="col-md-4">
                                            <fieldset>
                                                <legend>Deptos.</legend>
                                                <table style="width: 100%;" id="table-department">

                                                </table>
                                            </fieldset>
                                        </div>
                                    </div>
                                </div>
                                <br>
                              
                                <fieldset>
                                    <legend>Ruta de Puertas</legend>
                                    <br>
                                    <div id="div-route" style="width: 100%;"></div>
                                </fieldset>
                            </div>

                        </main>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal" onclick="llama()">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- FIN MODALS -->

        <!-- Modal_Observacion -->
        <div  id="modal-salida" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Observación tiempo permitido excedido</h4>
                    </div>
                    <div class="modal-body">
                        <main>
                            <div id="ob" class="hidden">
                                <fieldset>
                                    <table style="width: 100%;">
                                        <tr>
                                            <td><label>Observación</label></td>
                                            <td>
                                                <textarea id="observacion_salida" class="form-control textarea" rows="6" id="area-observation"></textarea>
                                            </td> 
                                        </tr>
                                    </table>
                                </fieldset>
                            </div>
                            <div id="mform"></div>
                        </main>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Guardar</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Fin Modal -->
    </section>
</div>



<?php $this->load->view('footer');?>

<script>
    var node_server = 'localhost';
    var node_port = 8001;
    var node_route = 'parametros';

    function getIndexArrayItem(array, item)
    {
        var index = -1; 
        for(var i=0;i<array.length;i++)
        {
            if(array[i].rut == item)
            {
                index = i;
            }
        }

        return index;
    }

    var in_people = [];
    var obs = '';

    var table_in;
    var table_out;

    $(document).ready(function() {
        
        table_in = $("#table-register_access").DataTable(
            { 
                'language': { "url": base_url + "assets/Spanish.json" },
            }
        );
        table_out = $("#table-register_out").DataTable(
            {
                'language': { "url": base_url + "assets/Spanish.json" },
            }
        );

        $.widget.bridge('uibutton', $.ui.button);

        $("#formModal-add-people").submit(function(event) {
            event.preventDefault();

            var rut = $('#rut').val();
            var digit = rut[rut.length-1];
            rut = rut.slice(0, -2);
            var name = $('#name').val();
            var last_name = $('#last_name').val();
            var address = $('#address').val();
            var email = $('#email').val();
            var phone = $('#phone').val();
            var profile = $('#people_profiles').val();
            var company = $('#companies').val();

            $.ajax({
                url: site_url + '/cAccess_People/addPeople/',
                type: 'post',
                data: {rut: rut, digit: digit, name: name, last_name: last_name, address: address, email: email, phone: phone, allow_all: 0, is_visited: 0, internal: 0, people_profiles_id: profile, companies_id: company},
                dataType: 'text',
                success: function(data)
                {
                    if(data == 1)
                    {
                        $('#inputrut3').val($('#rut').val());
                        $('#form-authorization3').attr('action',site_url+'/cAccess_People/authorization/');
                        $('#form-authorization3').submit();
                    }
                    else
                    {
                        alert('Verifique que el RUT no este ingresado.');
                    }
                }
            });
        });

        $("#form-authorization").submit(function(event) {
            event.preventDefault();

            $.ajax({
                url: site_url + '/cAccess_People/in_people',
                type: 'POST',
                dataType: 'json',
                success: function(resp){

                    for (var i = 0; i < resp.length; i++) {
                        in_people.push({'rut': resp[i].rut+'-'+resp[i].digit, 'id': resp[i].id});
                    }
                    //correción de llamada asincrona mediante ajax
                    var rut = document.getElementById('inputrut');
                    if(checkRut(rut)) {//rut formato válido
                        //LO PRIMERO ES VALIDAR QUE NO TENGA ACCESOS, SI ES QUE TIENE, GENERAR SALIDA, DE LO CONTRARIO GENERAR ACCESO
                        if (in_people.length > 0) {
                            //for (var i = 0; i < in_people.length; i++) {
                                var indice = getIndexArrayItem(in_people, rut.value);
                                in_people = [];
                                if ( indice > -1) {/////validar para crear salida
                                    //[SALIDA]
                                    window.location.replace(site_url + "/cAccess_People/searchPeopleControl?rut=" + rut.value)
                                    //[/SALIDA]
                                }
                                else {
                                    //[ACCESO]
                                    //validar si existe el rut en la BD
                                    
                                    $.ajax({
                                        url: site_url + '/cAccess_People/generateAuthorization/',
                                        data: { rut: rut.value },
                                        type: 'post',
                                        dataType: 'json',
                                        success: function(data)
                                        {
                                            if(data.response == 1) {
                                                //existe en la bd
                                                if (data.people[0].internal == 0 && data.people[0].states_id == 1) {//verifica el acceso solo a externos, y persona estado activa
                                                    $('#inputrut2').val(rut.value);
                                                    $('#form-authorization2').attr('action',site_url+'/cAccess_People/authorization/');
                                                    $('#form-authorization2').submit();
                                                }
                                                else
                                                {
                                                    $('#inputrut').val('');
                                                    $('#inputrut2').val('');
                                                    $('#inputrut3').val('');
                                                    $('#inputrut').focus();
                                                    if (data.people[0].states_id != 1) {
                                                        alert("La persona esta suspendida o bloqueada");
                                                    }
                                                }
                                            }
                                            else if(data.response == 0) {
                                                //no existe en la bd
                                                $('#rut').val(data.rut);

                                                var people_profile;
                                                var companies;

                                                if(data.people_profile.length > 0 && data.people_profile != null) {
                                                    people_profile = '<option value="">Seleccione una opción</option>';
                                                    for(var i=0;i<data.people_profile.length;i++) {
                                                        people_profile += '<option value="'+data.people_profile[i].id+'">'+data.people_profile[i].profile+'</option>';
                                                    }
                                                }

                                                if(data.companies.length > 0 && data.companies != null) {
                                                    companies = '<option value="">Seleccione una opción</option>';
                                                    for(var i=0;i<data.companies.length;i++) {
                                                        companies += '<option value="'+data.companies[i].id+'">'+data.companies[i].company+'</option>';
                                                    }
                                                }

                                                $('#people_profiles').html(people_profile);
                                                $('#companies').html(companies);

                                                $('#modal-add-people').modal('show');
                                                setTimeout(function(){ $('#name').focus(); }, 500);
                                            }
                                            else if (data.response == 2) {
                                                alert('Ya existe una autorización vigente.');
                                                $("#inputrut").val('');
                                            }
                                        }
                                    });
                                    //[/ACCESO]
                                }
                            //}
                        }
                        else {
                            //[ACCESO]
                            //validar si existe el rut en la BD
                            $.ajax({
                                url: site_url + '/cAccess_People/generateAuthorization/',
                                data: {rut: rut.value},
                                type: 'post',
                                dataType: 'json',
                                success: function(data)
                                {
                                    if(data.response == 1) {
                                        //existe en la bd
                                        if (data.people[0].internal == 0 && data.people[0].states_id == 1) {//verifica el acceso solo a externos, y persona estado activa
                                            $('#inputrut2').val(rut.value);
                                            $('#form-authorization2').attr('action',site_url+'/cAccess_People/authorization/');
                                            $('#form-authorization2').submit();
                                        }
                                        else {
                                            $('#inputrut').val('');
                                            $('#inputrut2').val('');
                                            $('#inputrut3').val('');
                                            $('#inputrut').focus();
                                            if (data.people[0].states_id != 1) {
                                                alert("La persona esta suspendida o bloqueada");
                                            }
                                        }
                                    }
                                    else if(data.response == 0) {
                                        //no existe en la bd
                                        $('#rut').val(data.rut);

                                        var people_profile;
                                        var companies;

                                        if(data.people_profile.length > 0 && data.people_profile != null) {
                                            people_profile = '<option value="">Seleccione una opción</option>';
                                            for(var i=0;i<data.people_profile.length;i++) {
                                                people_profile += '<option value="'+data.people_profile[i].id+'">'+data.people_profile[i].profile+'</option>';
                                            }
                                        }

                                        if(data.companies.length > 0 && data.companies != null) {
                                            companies = '<option value="">Seleccione una opción</option>';
                                            for(var i=0;i<data.companies.length;i++) {
                                                companies += '<option value="'+data.companies[i].id+'">'+data.companies[i].company+'</option>';
                                            }
                                        }

                                        $('#people_profiles').html(people_profile);
                                        $('#companies').html(companies);

                                        $('#modal-add-people').modal('show');
                                        setTimeout(function(){ $('#name').focus(); }, 500);
                                    }
                                    else if (data.response == 2) {
                                        alert('Ya existe una autorización vigente.');
                                        $("#inputrut").val('');
                                    }
                                }
                            });
                            //[/ACCESO]
                        }
                        
                    }

                }
            });
            

            
        });
    });
    

    function checkRut(rut) {
        // Despejar Puntos
        var valor = rut.value.replace('.','');
        // Despejar Guión
        valor = valor.replace('-','');
        
        // Aislar Cuerpo y Dígito Verificador
        var cuerpo = valor.slice(0,-1);
        var dv = valor.slice(-1).toUpperCase();
        // Formatear RUN
        rut.value = cuerpo + '-'+ dv
        // Si no cumple con el mínimo ej. (n.nnn.nnn)
        if(cuerpo.length < 7) 
        {
            rut.setCustomValidity("RUT Incompleto");
            rut.setCustomValidity('');
            cuerpo = '';
            dv = '';
            rut.value = '';
            return false;
        }
        else
        {
            // Calcular Dígito Verificador
            suma = 0;
            multiplo = 2;
            
            // Para cada dígito del Cuerpo
            for(i=1;i<=cuerpo.length;i++) {
            
                // Obtener su Producto con el Múltiplo Correspondiente
                index = multiplo * valor.charAt(cuerpo.length - i);
                
                // Sumar al Contador General
                suma = suma + index;
                
                // Consolidar Múltiplo dentro del rango [2,7]
                if(multiplo < 7) { multiplo = multiplo + 1; } else { multiplo = 2; }
          
            }
            
            // Calcular Dígito Verificador en base al Módulo 11
            dvEsperado = 11 - (suma % 11);
            
            // Casos Especiales (0 y K)
            dv = (dv == 'K')?10:dv;
            dv = (dv == 0)?11:dv;
            
            // Validar que el Cuerpo coincide con su Dígito Verificador
            if(dvEsperado != dv) 
            { 
                rut.setCustomValidity("RUT Inválido");
                rut.setCustomValidity('');
                cuerpo = '';
                dv = '';
                rut.value = '';
                return false;
            }
            // Si todo sale bien, eliminar errores (decretar que es válido)
            rut.setCustomValidity('');
            return true;
        }
    }

    var html ='';
    var html2 ='';
    var ra = [];
    var vr = [];
    var zn = [];
    var ar = [];
    var dp = [];
    var dr = [];

    function verDetalle(id, entry) {
        //consultar por el id y rescatar todo para llenar la modal
        $.ajax({
            url: site_url + '/cAccess_People/getDetail/',
            type: 'post',
            dataType: 'json',
            data: { id: id, entry: entry },
            success: function(data) {
                if (data.length > 0) {
                    
                    for (var i = 0; i < data.length; i++) {
                        if (zn.includes(data[i].zones_id) == false) {
                            zn.push(data[i].zones_id);
                            $("#table-zones").append(`<tr><td>`+data[i].zone+`</td></tr>`);
                        }
                        if (ar.includes(data[i].areas_id) == false) {
                            ar.push(data[i].areas_id);
                            $("#table-areas").append(`<tr><td>`+data[i].area+`</td></tr>`);
                        }
                        if (dp.includes(data[i].departments_id) == false) {
                            dp.push(data[i].departments_id);
                            $("#table-department").append(`<tr><td>`+data[i].department+`</td></tr>`);
                        }
                        if (dr.includes(data[i].doors_id) == false) {
                            dr.push(data[i].doors_id);
                            $("#div-route").append(`<tr><td>`+data[i].door+`</td></tr>`);
                        }
                    }
                    $("td[name='datos-reasons']").html(html);
                    $("td[name='datos-visit']").html(html2);
                    
                }
                
            }
        });
        $("#modal-detail_access").modal('show');//al cerrar modal limpiar td llenadas con html
    }

    function llama(){
        $("#table-zones").html('');
        $("#table-areas").html('');
        $("#table-department").html('');
        $("#div-route").html('');
        $("td[name='datos-reasons']").html('');
        $("td[name='datos-visit']").html('');
        html ='';
        html2 ='';
        ra = [];
        vr = [];
        zn = [];
        ar = [];
        dp = [];
        dr = [];
    }
</script>

</body>
</html>
