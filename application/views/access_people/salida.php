<?php $this->view('header'); ?>
<?php $this->view('aside'); ?>

<?php
	if(empty($excedido))
		$excedido = false;

	if(empty($control_end))
		$control_end = 0;
?>

<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-success">
					<div class="box-header ui-sortable-handle">
					    <i class="fa fa-out"></i>
						<h3 class="box-title">Registro de salida</h3>
						<h5 id="mensaje" class="text text-danger"></h5>
				  	</div>
				  	
					<form class="form-horizontal" id="Salida" name="salida">
			  			<div class="box-body">
			  				<?php if ($excedido == true) { ?>
		  					<fieldset>
		  						<legend>Tiempo excedido:</legend>
		  						<table class="table table-condensed" style="width:100%;">
			  						<tr>
		  								<td>Observación:</td>
		  								<td><textarea class="form-control area fm" style="width:700px;" rows="5" id="tiempo" required></textarea></td>
			  						</tr>
		  						</table>
		  					</fieldset>
	  						<?php } ?>
	  						<br>
	  						<?php if ($control_end == 1) { ?>
	  							<fieldset>
			  						<legend>Control de salida:</legend>
				  					<table class="table table-condensed" style="width:100%;">
				  						<tr>
				  							<td>Título:</td>
				  							<td><input type="text" class="form-control" style="width:700px;" id="formtitle" required></td>
				  						</tr>
				  						<tr>
				  							<td>Observación:</td>
				  							<td><textarea id="formobservation" class="form-control area textarea" style="width:700px;" rows="5" required></textarea></td>
				  						</tr>
				  						<?php foreach ($form as $k) { ?>
				  							<tr>
					  							<td><?php echo $k->question.':'; ?></td>
					  							<?php if ($k->atid == 1) { ?>
					  								<td><input type="text" class="form-control fm" style="width:700px;" id="<?php echo 'rc'.$k->order; ?>" placeholder="<?php echo $k->placeholder; ?>" required></td>
					  							<?php } else if ($k->atid == 2) { ?>
					  								<td><textarea class="form-control area fm textarea" style="width:700px;" rows="5" id="<?php echo 'rp'.$k->order; ?>" placeholder="<?php echo $k->placeholder; ?>" required></textarea></td>
					  							<?php } else if ($k->atid == 3) { ?>
					  								<td><input type="number" class="fm" style="width:200px;" id="<?php echo 'rc'.$k->order; ?>" placeholder="<?php echo $k->placeholder; ?>" required><?php echo ' '.$k->measure; ?></td>
					  							<?php } else if ($k->atid == 4) { ?>
					  								<td><input type="date" class="fm" style="width:200px;" id="<?php echo 'f'.$k->order; ?>" placeholder="<?php echo $k->placeholder; ?>" required></td>
					  							<?php } else if ($k->atid == 5) { ?>
					  								<td><input type="checkbox" class="check" style="width:20px;" id="<?php echo 'rb'.$k->order; ?>" required> <?php echo $k->placeholder; ?></td>
					  							<?php } ?>
					  						</tr>
					  					<?php } ?>
				  					</table>
			  					</fieldset>
	  						<?php } ?>
			  			<div class="box-footer">
			  				<button type="submit" class="btn btn-primary pull-right">Guardar</button>
			  			</div>
		  			</form>
				</div>
			</div>
		</div>
	</section>
	
</div>

<?php $this->view('footer'); ?>

<script src="<?php echo base_url()?>assets/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>

<script>
	$(document).ready(function() {
		
		var answer = [];
		var forder = [];

		var excedido = <?php echo json_encode($excedido); ?>;
		var control_end = <?php echo json_encode($control_end); ?>

		$("#Salida").submit(function(event) {
			event.preventDefault();

			var end = false;
			var tiempo = false;

			if (control_end == 1) {
				$(".fm").each(function(index, el) {
					if ($(this).val() == '') {
						$("#mensaje").text('Debe contestar todas las preguntas del formulario.');
						answer = [];
						forder = [];
					}
					else {
						answer.push($(this).val())
						forder.push(index+1)
					}
				});
				$(".check").each(function(index, el) {
					var indexx = forder.length;

					if ($(this).prop('checked') == true) {
						answer.push(1);
						forder.push(indexx+1);
					}
					else if ($(this).prop('checked') == false) {
						answer.push(0);
						forder.push(indexx+1);
					}
				});
				if ($("#formtitle").val() == '') {
					$("#mensaje").text('Debe incluir un título para el formulario.');
					answer = [];
					forder = [];
				}
				else {
					end = true;
				}
			}

			if (excedido == 1) {
				if ($("#tiempo").val() == '') {
					$("#mensaje").text('Debe incluir una observación por exceder el tiempo permitido.');
					answer = [];
					forder = [];
				}
				else {
					tiempo = true;
				}
			}

			if (control_end == 1 && end == true && excedido == 1 && tiempo == true) {
				$.ajax({
					url: site_url + '/cAccess_People/generateAccessOut2',
					type: 'POST',
					dataType: 'json',
					data: {
						tobservation 		: $("#tiempo").val(),
						title 				: $("#formtitle").val(),
						observation 		: $("#formobservation").val(),
						order 				: forder,
						answers 			: answer,
						access_people_id 	: <?php echo json_encode($access_people_id); ?>,
						forms_id 			: <?php echo json_encode($forms_id); ?>,
						rut 				: <?php echo json_encode($rut); ?>
					},
					success: function(data){
						if (data == 1) {
							window.location.replace(site_url + "/cAccess_People/");
						}
						else if (data == 0) {
							alert('Error en el proceso.');
							window.location.replace(site_url + "/cAccess_People/");
						}
					}
				});
			}
			else if (control_end == 1 && end == true && excedido == 0 && tiempo == false) {
				$.ajax({
					url: site_url + '/cAccess_People/generateAccessOut2',
					type: 'POST',
					dataType: 'json',
					data: {
						tobservation 		: $("#tiempo").val(),
						title 				: $("#formtitle").val(),
						observation 		: $("#formobservation").val(),
						order 				: forder,
						answers 			: answer,
						access_people_id 	: <?php echo json_encode($access_people_id); ?>,
						forms_id 			: <?php echo json_encode($forms_id); ?>,
						rut 				: <?php echo json_encode($rut); ?>
					},
					success: function(data){
						if (data == 1) {
							window.location.replace(site_url + "/cAccess_People/");
						}
						else if (data == 0) {
							alert('Error en el proceso.');
							window.location.replace(site_url + "/cAccess_People/");
						}
					}
				});
			}
			else if (control_end == 0 && end == false && excedido == 1 && tiempo == true) {
				$.ajax({
					url: site_url + '/cAccess_People/generateAccessOut2',
					type: 'POST',
					dataType: 'json',
					data: {
						tobservation 		: $("#tiempo").val(),
						title 				: $("#formtitle").val(),
						observation 		: $("#formobservation").val(),
						order 				: forder,
						answers 			: answer,
						access_people_id 	: <?php echo json_encode($access_people_id); ?>,
						forms_id 			: <?php echo json_encode($forms_id); ?>,
						rut 				: <?php echo json_encode($rut); ?>
					},
					success: function(data){
						if (data == 1) {
							window.location.replace(site_url + "/cAccess_People/");
						}
						else if (data == 0) {
							alert('Error en el proceso.');
							window.location.replace(site_url + "/cAccess_People/");
						}
					}
				});
			}
			else if (control_end == 0 && end == false && excedido == 0 && tiempo == false) {
				$.ajax({
					url: site_url + '/cAccess_People/generateAccessOut2',
					type: 'POST',
					dataType: 'json',
					data: {
						tobservation 		: $("#tiempo").val(),
						title 				: $("#formtitle").val(),
						observation 		: $("#formobservation").val(),
						order 				: forder,
						answers 			: answer,
						access_people_id 	: <?php echo json_encode($access_people_id); ?>,
						forms_id 			: <?php echo json_encode($forms_id); ?>,
						rut 				: <?php echo json_encode($rut); ?>
					},
					success: function(data){
						if (data == 1) {
							window.location.replace(site_url + "/cAccess_People/");
						}
						else if (data == 0) {
							alert('Error en el proceso.');
							window.location.replace(site_url + "/cAccess_People/");
						}
					}
				});
			}
			
		});
	});
</script>