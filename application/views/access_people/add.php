
<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-success">
					<div class="box-header ui-sortable-handle">
					    <i class="fa fa-sign-in"></i>
						<h3 class="box-title">Autorización <small>> Datos de ingreso</small></h3>
				  	</div>
			    	<div class="box-body">
		    			<div class="wizard">
		    		        <div class="wizard-inner">
		    		            <div class="connecting-line"></div>
		    		            <ul class="nav nav-tabs" role="tablist">
		    		                <li role="presentation" class="active">
		    		                    <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Persona/s">
		    		                        <span class="round-tab">
		    		                            <i class="fa fa-users"></i>
		    		                        </span>
		    		                    </a>
		    		                </li>

		    		                <li role="presentation" class="disabled">
		    		                    <a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="Motivos">
		    		                        <span class="round-tab">
		    		                            <i class="fa fa-list-alt"></i>
		    		                        </span>
		    		                    </a>
		    		                </li>

		    		                <li role="presentation" class="disabled">
		    		                    <a href="#step3" data-toggle="tab" aria-controls="step3" role="tab" title="Ubicaciones">
		    		                        <span class="round-tab">
		    		                            <i class="fa fa-map-marker"></i>
		    		                        </span>
		    		                    </a>
		    		                </li>

		    		                <li role="presentation" class="disabled">
                                        <a href="#step4" data-toggle="tab" aria-controls="step4" role="tab" title="Formulario">
                                            <span class="round-tab">
                                                <i class="fa fa-file-text"></i>
                                            </span>
                                        </a>
                                    </li>

		    		                <li role="presentation" class="disabled">
		    		                    <a href="#complete" data-toggle="tab" aria-controls="complete" role="tab" title="Finalizar">
		    		                        <span class="round-tab">
		    		                            <i class="glyphicon glyphicon-ok"></i>
		    		                        </span>
		    		                    </a>
		    		                </li>
		    		            </ul>
		    		        </div>
		    		      	
    		                <form name="formWizard" id="formWizard">
    		                    <div class="tab-content">
    		                        <div class="tab-pane active" role="tabpanel" id="step1">
    		    	                	<div class="table-responsive">
    		    			          		<fieldset>
	    		    			    			<legend>Datos Persona:</legend>
	    		    			              	<table id="table-add-people" class="table table-condensed" style="width:100%;">
		    		    			                <tr>
		    		    			                  <td><label>Rut</label></td>
		    		    			                  <td>
		    		    			                    <input readonly class="form-control" name="input-add-rut1" id="input-add-rut1" data-id="<?php if(!empty($people[0]['id']))  echo $people[0]['id'];?>" value="<?php if(!empty($people[0]['rut']))  echo $people[0]['rut'].'-'.$people[0]['digit'];?>">
		    		    			                  </td>
		    		    			                </tr>

		    		    			                <tr>
		    		    			                  <td><label>Nombre</label></td>
		    		    			                  <td>
		    		    			                    <input readonly class="form-control" name="input-add-name" id="input-add-name" value="<?php if(!empty($people[0]['name']))  echo $people[0]['name'];?>">
		    		    			                  </td>
		    		    			                </tr>

		    		    			                <tr>
		    		    			                  <td><label>Apellido</label></td>
		    		    			                  <td>
		    		    			                    <input readonly class="form-control" name="input-add-last_name" id="input-add-last_name" value="<?php if(!empty($people[0]['last_name']))  echo $people[0]['last_name'];?>">
		    		    			                  </td>
		    		    			                </tr>

		    		    			                <tr>
		    		    			                  <td><label>Teléfono</label></td>
		    		    			                  <td>
		    		    			                    <input readonly class="form-control" name="input-add-phone" id="input-add-phone" value="<?php if(!empty($people[0]['phone']))  echo $people[0]['phone'];?>"> 
		    		    			                  </td>
		    		    			                </tr>

		    		    			                <tr>
		    		    			                  <td><label>Email</label></td>
		    		    			                  <td>
		    		    			                    <input type="email" readonly class="form-control" name="input-add-email" id="input-add-email" value="<?php if(!empty($people[0]['email']))  echo $people[0]['email'];?>">
		    		    			                  </td>
		    		    			                </tr>

		    		    			                <tr>
		    		    			                  <td><label>Perfil</label></td>
		    		    			                  <td>
		    		    			                   <input readonly class="form-control" name="input-add-profile" id="input-add-profile" value="<?php if(!empty($people[0]['profile']))  echo $people[0]['profile'];?>">
		    		    			                  </td>
		    		    			                </tr>

		    		    			                <tr>
		    		    			                  <td><label>Empresa</label></td>
		    		    			                  <td>
		    		    			                    <input readonly class="form-control" name="input-add-company" id="input-add-company" value="<?php if(!empty($people[0]['company']))  echo $people[0]['company'];?>">
		    		    			                  </td>
		    		    			                </tr>
												</table>
    		    			          		</fieldset>
    		    			          		<br>
    		    			          		<fieldset>
    		    			    				<legend>Personas Adicionales: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    		    			    					<button class="btn btn-success btn-xs" type="button" onclick="openModalAddAditionalPeople();"><i class="fa fa-plus" aria-hidden="true"></i></button>
    		    			    				</legend>
    		    			    			
	    		    			              	<table id="table-add-aditional_people" class="table table-condensed" style="width:100%;">
	    		    			              		<thead id="thead-aditional">
	    		    			              			<tr>
	    		    			              				<th>Rut</th>
	    		    			              				<th>Nombre</th>
	    		    			              				<th>Apellidos</th>
	    		    			              				<th>Remover</th>
	    		    			              			</tr>
	    		    			              		</thead>
	    		    			              		<tbody id="tbody-aditional"></tbody>
	    		    			              	</table>
    		    			          		</fieldset>
    		    			          		<br>
    		    			          		<fieldset>
    		    			          			<legend>Datos de acceso:</legend>
    		    			          			<fieldset>
    		    			          				<div class="col-md-3">
    		    			          					<input type="checkbox" id="checkauto"> <label> Acceso Vehículo</label>
    		    			          				</div>
    		    			          				<div class="col-md-3 hidden" id="sectionAuto">
    		    			          					<label for="">Patente: </label>
    		    			          					<input type="text" id="patente" maxlength="6"> 
    		    			          				</div>
    		    			          				
    		    			          				<div class="col-md-12"><hr style="border-top:1px solid #6b6b6b"></div>
    		    			          				<div class="col-md-12">
    		    			          					<table id="table-add-auto" class="table table-condensed table-bordered">
    		    			          						<thead>
    		    			          						    <tr>
    		    			          						        <th>Patente</th>
    		    			          						        <th>Clasificación</th>
    		    			          						        <th>Empresa</th>
    		    			          						        <th>Responsable</th>
    		    			          						        <th>Tipo</th>
    		    			          						        <th>Perfil</th>
    		    			          						        <th>Acción</th>
    		    			          						    </tr>
    		    			          						</thead>
    		    			          						<tbody id="tbody-vehicle"></tbody>
    		    			          					</table>
    		    			          				</div>

    		    			          				<div class="col-md-12"><hr style="border-top:1px solid #6b6b6b"></div>
    		    			          				<div class="col-md-2">
    		    			          					<input type="checkbox" id="acin" disabled> <label>Control al entrar</label>
    		    			          				</div>
    		    			          				<div class="col-md-2"></div>
    		    			          				<div class="col-md-2">
    		    			          					<input type="checkbox" id="acout" disabled> <label>Control al salir</label>
    		    			          				</div>
    		    			          			</fieldset>
    		    			          			<br>
    		    			          			<table id="table-add-info" class="table table-condensed" style="width:100%;">
	    		    			              		<tbody>
		    		    			              		<tr>
		    		    			              			<td><label>Movimiento</label></td>
		    		    			              			<td>
		    		    			              				<select name="entry" id="entry" required>
		    		    			              					<option value="0">Ingreso</option>
		    		    			              				</select>
		    		    			              			</td>
		    		    			              		</tr>
		    		    			              		<tr>
		    		    			              			<td><label>Horas permitidas</label></td>
		    		    			              			<td>
		    		    			              				<input type="number" min="1" max="24" id="permitidas" value="1" name="permitidas">
		    		    			              			</td>
		    		    			              		</tr>
		    		    			              		<tr>
		    		    			              			<td><label>Hora de salida</label></td>
		    		    			              			<td>
		    		    			              				<?php 
		    		    			              				date_default_timezone_set('America/Santiago');
		    		    			              				?>
		    		    			              				<span id="salida" name="<?php echo date('H:i', time()); ?>"><?php echo date('H:i Y-m-d', time()); ?></span>
		    		    			              			</td>
		    		    			              		</tr>
			    		    			                <tr>
			    		    			                	<td><label>Estado de acceso</label></td>
			    		    			                	<td>
			    		    			                		<select name="state" id="state" required>
			    		    			                			<!--
			    		    			                			<option value="">Seleccione una opción</option>
			    		    			                			-->
			    		    			                			<option value="1">Pendiente</option>
			    		    			                			<option value="2">Permitido</option>
			    		    			                			<option value="3">Rechazado</option>
			    		    			                			
			    		    			                		</select>
			    		    			                	</td>
			    		    			                </tr>
			    		    			                <tr>
			    		    			                	<td width="270px"><label>Observación</label></td>
			    		    			                	<td>
			    		    			                		<textarea class="textarea" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" name="observation" id="observation"></textarea>
	    		    			                		    </td>
			    		    			                </tr>
			    		    			            </tbody>
		    		    			            </table>
			    		    			    </fieldset>
    		    			            </div>
    		    				        <br>
    		                            <ul class="list-inline pull-left">
    		                                <li>
    		                                	<button type="button" class="btn btn-primary next-step">Guardar y continuar</button>
    		                                </li>
    		                            </ul>
    		                        </div>
    		                        <div class="tab-pane" role="tabpanel" id="step2">
    		                            <fieldset>
    		                            	<legend>Motivo de visita: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <button class="btn btn-success btn-xs" type="button" onclick="openModalAddReason_Visit();"><i class="fa fa-plus" aria-hidden="true"></i></button></legend>
    		                            	<div id="tre">
    		                            		<ul class="list list-group" id="ulreason"></ul>
    		                            	</div>
    		                            </fieldset>
    		                            <br>
    		                            <fieldset>
    		                            	<legend>Persona: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</legend>
		                            		<div class="input-group col-md-4">
    		                            	   	<select class="form-control" name="listvisit" id="listvisit" >
   	    		                            		<!--
   	    		                            		<option value="">Seleccione una opción</option>
   	    		                            		-->
   													<?php foreach ($peopleVisited as $key) { ?>
   														<option value="<?php echo $key->id; ?>" data-rut="<?php echo $key->rut.'-'.$key->digit; ?>"><?php echo $key->name.' '.$key->last_name; ?></option>
   													<?php } ?>
	    		                            		</select>
    		                            	   	<span class="input-group-btn">
		                            	        	<button type="button" class="btn btn-success" onclick="addvisi();"><i class="fa fa-plus fw" aria-hidden="true"></i></button>
    		                            	   	</span>
    		                            	</div>
	    		                            <br>	
    		                            	<table id="table-add-people-visit" class="table table-condensed" style="width:100%;">
    		                            		<thead>
    		                            			<tr>
    		                            				<th>Rut</th>
    		                            				<th>Nombre</th>
    		                            				<th>Apellidos</th>
    		                            				<th>Remover</th>
    		                            			</tr>
    		                            		</thead>
    		                            		<tbody name="visitbody">
    		                            			
    		                            		</tbody>
    		                            	</table>
    		                            </fieldset>
										<br>
    		                            <ul class="list-inline pull-right">
    		                                <li>
    		                                	<button type="button" class="btn btn-default prev-step">Anterior</button>
    		                                </li>
    		                                <li>
    		                                	<button type="button" class="btn btn-primary next-step">Guardar y continuar</button>
    		                                </li>
    		                            </ul>
    		                        </div>
    		                        <div class="tab-pane" role="tabpanel" id="step3">
    		                            <fieldset id="ubicacion">
    		                            	<legend>Ubicación:</legend>
    		                            	<div class="row">
    		                            		<div class="col-lg-4">
    		                            		  <div class="box no-border">
    		                            		  	<div class="box-header"><label>Zonas</label></div>
    		                            		  	<div class="box-body">
    		                            		    	<?php foreach ($treeview['zona'] as $k) { ?>
    		                            		      		<p><input type="checkbox" name="<?php echo $k['zid']; ?>" id="<?php echo $k['zid']; ?>" data-level="0" data-type="zone"> <?php echo $k['zone']; ?></p>
		                            		      		<?php } ?>
    		                            		    </div>
    		                            		  </div>
    		                            		</div>
    		                            		<div class="col-lg-4">
    		                            		  <div class="box no-border">
    		                            		  	<div class="box-header"><label>Áreas</label></div>
    		                            		  	<div class="box-body">
    		                            		    	<?php foreach ($treeview['area'] as $k) { ?>
    		                            		      		<p><input type="checkbox" name="<?php echo $k['area_parent']; ?>" id="<?php echo $k['aid']; ?>" data-level="1" data-type="area"> <?php echo $k['area']; ?></p>
    		                            		      	<?php } ?>
    		                            		    </div>
    		                            		  </div>
    		                            		</div>
    		                            		<div class="col-lg-4">
    		                            		  <div class="box no-border">
    		                            		  	<div class="box-header"><label>Departamentos</label></div>
    		                            		  	<div class="box-body">
    		                            		    	<?php foreach ($treeview['dpto'] as $k) { ?>
    		                            		    		<p><input type="checkbox" name="<?php echo $k['department_parent']; ?>" id="<?php echo $k['did']; ?>" data-level="2" data-type="dpto"> <?php echo $k['department']; ?></p>
    		                            		    	<?php }?>
    		                            		    </div>
    		                            		  </div>
    		                            		</div>
    		                            	</div>
    		                            </fieldset>
    		                            <br>
    		                            <fieldset id="rutapuertas">
    		                            	<legend>Ruta:</legend>
    		                            		<?php for ($i=0; $i < count($doorlevel); $i++) { ?>
		                            			<div class="row">
		                            				<?php foreach ($doorlevel as $k) { 
		                            					if ($k->level == $i) { ?>
		                            						<a href="#" style="color:#000" name="puertas" onMouseOver="this.style.color='#5cb85c'" onMouseOut="this.style.color='#000'" id="<?php echo $k->id; ?>" data-level="<?php echo $k->level; ?>">
    		                            					<div class="col-md-3">
		    		                            				<div class="box box-solid box-default">
											                    	<div class="box-header">
											                       		<h5 class="box-title">Nivel <?php echo $k->level ?></h5>
											                    	</div>
											                    	<div class="box-body">
											                        <?php echo $k->door; ?> <i class="fa fa-sign-in fa-2x pull-right"></i>
											                        	<div><label><small><?php echo $k->description; ?></small></label></div>
											                    	</div>
											                	</div>
		    		                            			</div>
		    		                            			</a>
		                            				<?php }} ?>
		                            			</div>
		                            		<?php } ?>
    		                            </fieldset>
    		                            <br>
    		                            <ul class="list-inline pull-right">
    		                                <li>
    		                                	<button type="button" class="btn btn-default prev-step">Anterior</button>
    		                                </li>
    		                                <li>
    		                                	<button type="button" class="btn btn-primary btn-info-full next-step">Guardar y continuar</button>
    		                                </li>
    		                            </ul>
    		                        </div>
    		                        <div class="tab-pane" role="tabpanel" id="step4">
                                        <fieldset>
                                            <legend>Formulario control de ingreso:</legend>
                                            <div class="row">
                                            	<div class="col-md-12">
                                            		<label>Formulario: </label>
                                            		<select name="optionForm" id="optionForm" disabled>
                                            			<option value="">Seleccione una opción</option>
                                            			<?php foreach ($form as $k) { ?>
                                            				<option value="<?php echo $k->id; ?>"><?php echo $k->title.' - '.$k->description; ?></option>
                                            			<?php } ?>
                                            		</select>
                                            		<div class="col-md-12" id="bodyform"></div>
                                            	</div>
                                            </div>
                                        </fieldset>
                                        <br>
                                        <fieldset>
                                        	<legend>Formulario control de salida:</legend>
                                        	<div class="row">
                                        		<div class="col-md-12">
                                        			<label>Formulario: </label>
                                        			<select name="optionformEnd" id="optionformEnd" disabled>
                                        				<option value="">Seleccione una opción</option>
                                        				<?php foreach ($form as $k) { ?>
                                        					<option value="<?php echo $k->id; ?>"><?php echo $k->title.' - '.$k->description; ?></option>
                                        				<?php } ?>
                                        			</select>
                                        		</div>
                                        	</div>
                                        </fieldset>
                                        <br>
                                        <ul class="list-inline pull-right">
                                            <li>
                                                <button type="button" class="btn btn-default prev-step">Anterior</button>
                                            </li>
                                            <li>
                                                <button type="button" class="btn btn-primary next-step">Guardar y continuar</button>
                                            </li>
                                        </ul> 
                                    </div>
    		                        <div class="tab-pane" role="tabpanel" id="complete">
    		                        	<div class="row">
    		                        		<div class="col-md-5 col-md-offset-3">
												<div id="mensaje-error" class="text text-danger"></div>
    		                        			<div class="box box-success">
		    		                            	<div class="box-header">
		    		                            		<h3>Procesar el registro de ingreso </h3></i>
		    		                            	</div>
		    		                            	<div class="box-body">
		    		                            		<div class="col-md-2 col-md-offset-4" id="div-question_loading">
		    		                            			<label class="text-primary"><i class="fa fa-question-circle-o fa-5x"></i></label>
		    		                            		</div>
		    		                            	</div>
		    		                            	<div class="box-footer">
		    		                            		<div class="col-md-8 col-md-offset-2">
		    		                            			<button type="submit" class="btn btn-success"">
		    		                            				<i class="fa fa-check"></i> Aceptar
		    		                            			</button>
		    		                            			<button type="button" class="btn btn-danger" onclick="btnCancelAdd()">
		    		                            				<i class="fa fa-times"></i> Cancelar
		    		                            			</button>
		    		                            		</div>
		    		                            	</div>
	    		                            	</div>
    		                        		</div>
    		                        	</div>
    		                           
    		                        </div>
    		                        <div class="clearfix"></div>
    		                    </div>
    		                </form>
    		            </div>

    		            <!-- MODALS -->
    		            <div id="modal-add-people" class="modal fade" role="dialog">
    		             	<div class="modal-dialog">
								<!-- Modal content-->
	    		                <div class="modal-content">
	    		                  	<div class="modal-header">
	    		                    	<button type="button" class="close" data-dismiss="modal">&times;</button>
	    		                    	<h4 class="modal-title" id="modal-title">Datos Persona Adicional</h4>
	    		                  	</div>
	    		                  	<div class="modal-body">
				                    	<div class="table-responsive">
				                    		<form id="formModal-add-aditional_people" name="formModal-add-aditional_people">
						                    	<table class="table table-condensed" style="width:100%;">
													<tr>
						                    			<td><label>Rut</label></td>
						                    			<td>
						                    				<input type="text" class="form-control" name="rut" id="rut" maxlength="9" required>
						                    			</td>
						                    		</tr>

						                    		<tr>
						                    			<td><label>Nombre</label></td>
						                    			<td>
						                    				<input type="text" class="form-control" name="name" id="name" required disabled>
						                    			</td>
						                    		</tr>

						                    		<tr>
						                    			<td><label>Apellidos</label></td>
						                    			<td>
						                    				<input type="text" class="form-control" name="last_name" id="last_name" required disabled>
						                    			</td>
						                    		</tr>

						                    		<tr>
						                    			<td><label>Dirección</label></td>
						                    			<td>
						                    				<input type="text" class="form-control" name="address" id="address" required disabled>
						                    			</td>
						                    		</tr>

						                    		<tr>
						                    			<td><label>Email</label></td>
						                    			<td>
						                    				<input type="email" class="form-control" name="email" id="email" required disabled>
						                    			</td>
						                    		</tr>

						                    		<tr>
						                    			<td><label>Teléfono</label></td>
						                    			<td>
						                    				<input type="number" class="form-control" name="phone" id="phone" required disabled>
						                    			</td>
						                    		</tr>

						                    		<tr>
						                    			<td><label>Perfil</label></td>
						                    			<td>
						                    				<select name="people_profiles" id="people_profiles" class="form-control" required disabled>
								  								<option value="">Seleccione una opción</option>
								  								<?php foreach ($people_profile as $key) { ?>
								  								<option value="<?php echo $key->id; ?>"><?php echo $key->profile; ?></option>
								  								<?php } ?>
								  							</select>
						                    			</td>
						                    		</tr>

						                    		<tr>
						                    			<td><label>Empresa</label></td>
						                    			<td>
						                    				<select name="companies" id="companies" class="form-control" required disabled>
								  								<option value="">Seleccione una opción</option>
								  								<?php foreach ($companies as $key) { ?>
								  								<option value="<?php echo $key->id; ?>"><?php echo $key->company; ?></option>
								  								<?php } ?>
								  							</select>
						                    			</td>
						                    		</tr>

						                    		
						                    		<tr>
						                    			<td colspan="2">
						                    				<button type="submit" class="btn btn-primary" id="btnAgregar">Agregar</button>
						                    			</td>
						                    		</tr>									
												</table>
											</form>
							  			</div>
	    		                  	</div>
	    		                  	<div class="modal-footer">
	    		                    	<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	    		                  	</div>
	    		                </div>
							</div>
    		            </div>
						<!-- FIN MODALS -->
    		            <!-- MODALS -->
    		            <div id="modal-add-reason" class="modal fade" role="dialog">
    		             	<div class="modal-dialog">
								<!-- Modal content-->
	    		                <div class="modal-content">
	    		                  	<div class="modal-header">
	    		                    	<button type="button" class="close" data-dismiss="modal">&times;</button>
	    		                    	<h4 class="modal-title">Motivo de visita</h4>
	    		                  	</div>
	    		                  	<div class="modal-body">
				                    	<div class="table-responsive">
				                    		<form id="formReason" name="formReason">
					                    		<table class="table table-condensed" style="width:100%;">
													<tr>
						                    			<td><label>Motivo</label></td>
						                    			<td>
						                    				<input type="text" class="form-control" name="reason" id="reason" required>
						                    			</td>
						                    		</tr>

						                    		<tr>
						                    			<td colspan="2">
						                    				<button type="submit" class="btn btn-primary" id="btnMotivo">Agregar</button>
						                    			</td>
						                    		</tr>
												</table>
											</form>
							  			</div>
	    		                  	</div>
	    		                  	<div class="modal-footer">
	    		                    	<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
	    		                  	</div>
	    		                </div>
							</div>
    		            </div>
						<!-- FIN MODALS -->
						<div id="modal-add-vehicle" class="modal fade" role="dialog">
						    <div class="modal-dialog">
						        <div class="modal-content">
						            <div class="modal-header">
						                <button type="button" class="close" data-dismiss="modal">&times;</button>
						                <h4 class="modal-title">Vehículo</h4>
						            </div>
						            <div class="modal-body">
						                <div class="table-responsive">
						                    <form id="formModal-add-vehicle">
						                        <table class="table table-condensed" style="width:100%;">
						                            <tr>
						                                <td><label>Patente</label></td>
						                                <td>
						                                    <input type="text" class="form-control" name="patent" id="patent" maxlength="6" required placeholder="XXYY99">
						                                </td>
						                            </tr>

						                            <tr>
						                                <td><label>Modelo</label></td>
						                                <td>
						                                    <input type="text" class="form-control" name="model" id="model" disabled>
						                                </td>
						                            </tr>

						                            <tr>
						                            	<td><label>Control</label></td>
						                            	<td>
						                            		<select name="control" id="control" disabled>
						                            			<option value="0">Visita</option>
						                            		</select>
						                            	</td>
						                            </tr>

						                            <tr>
						                                <td><label>Empresa</label></td>
						                                <td>
						                                    <select name="vcompanies" id="vcompanies" class="form-control" required disabled>
						                                        <?php foreach ($companies as $k) { ?>
						                                            <option value="<?php echo $k->id; ?>"><?php echo $k->company; ?></option>
						                                        <?php } ?>
						                                    </select>
						                                </td>
						                            </tr>

						                            <tr>
						                                <td><label>Responsable</label></td>
						                                <td>
						                                    <select name="vehicle_people" id="vehicle_people" class="form-control" required disabled>
						                                        <?php foreach ($peopleResponsable as $k) { 
						                                        	if ($k->internal == 0) { ?>
						                                        		<option value="<?php echo $k->id ?>"><?php echo $k->name.' '.$k->last_name; ?></option>
						                                        <?php } } ?>
						                                    </select>
						                                </td>
						                            </tr>

						                            <tr>
						                                <td><label>Tipo vehículo</label></td>
						                                <td>
						                                    <select name="vehicle_type" id="vehicle_type" class="form-control" required disabled>
						                                        <?php foreach ($vehicle_type as $k) { ?>
						                                            <option value="<?php echo $k->id; ?>"><?php echo $k->type; ?></option>
						                                        <?php } ?>
						                                    </select>
						                                </td>
						                            </tr>

						                            <tr>
						                                <td><label>Vehículo perfil</label></td>
						                                <td>
						                                    <select name="vprofile" id="vprofile" class="form-control" required disabled>
						                                        <?php foreach ($vehicle_profiles as $k) { ?>
						                                            <option value="<?php echo $k->id; ?>"><?php echo $k->profile; ?></option>
						                                        <?php } ?>
						                                    </select>
						                                </td>
						                            </tr>

						                            <tr>
						                                <td colspan="2">
						                                    <button type="submit" class="btn btn-primary" id="btnAgregarV">Agregar</button>
						                                </td>
						                            </tr>
						                        </table>
						                    </form>
						                </div>
						            </div>
						            <div class="modal-footer">
						                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
						            </div>
						        </div>
						    </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

<?php $this->view('footer'); ?>

<script src="<?php echo base_url()?>assets/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>

<script>
	var node_server = '<?php echo NODE_HOST; ?>';
  	var node_port = '<?php echo NODE_PORT; ?>';
  	var node_route = 'parametros';

	var existe = false;
	var aditional_people_id = [];
	var ruts = [];
	var reason = [];
	var accion_form = 0;
	
	//wizard process
	var wpuertas = [];
	var wareas = [];
	var wzones = [];
	var wdptos = [];
	var wreason = [];
	var waditional = [];

	var vehicles = 0;

	var form_id_end = 0;
	var form_id = 0;
	var ftitle = '';
	var fobservation = '';
	var forder = [];
	var answer = [];

	$(document).ready(function() {
		Number.prototype.padDigit = function () {
	       return (this < 10) ? '0' + this : this;
	   	}

	   	var fecha = '<?php echo date('Y-m-d', time()) ?>';
	   	var fecha = fecha.split('-');

     	var t1 = $("#salida").attr('name');
     	t1 = t1.split(':');

     	var t2 = $("#permitidas").val();

	    var mins = 0;
	    var hrs = 0;

    	mins = Number(t1[1]);
    	//minhrs = Math.floor(parseInt(mins / 60));
    	hrs = Number(t1[0]) + Number(t2);
    	mins = mins % 60;

    	hrsdia = Math.floor(parseInt(hrs / 24));
    	dia = Number(fecha[2]) + hrsdia;
    	
    	(hrs >= 24) ? hrs = hrs-24 : hrs = hrs;
    	t1 = hrs.padDigit() + ':' + mins.padDigit()
    	$("#salida").text(t1+' '+fecha[0]+'-'+fecha[1]+'-'+dia.padDigit())

    	//auto
    	$("#checkauto").change(function(event) {
    		if ($(this).prop('checked') == true) {
    			$("#sectionAuto").removeClass('hidden')
    		}
    		else if ($(this).prop('checked') == false) {
    			$("#sectionAuto").addClass('hidden')
    			$("#patente").val('')
    		}
    	});

    	$("#patente").keydown(function(event) {
    		if (event.which == 13) {
    	        event.preventDefault();

    	        var patente = $("#patente").val();
    	        $.ajax({
    	            url: site_url + '/cAccess_People/SearchVehicle/',
    	            type: 'post',
    	            data: {
    	                patent : patente,
    	                internal : 0
    	            },
    	            dataType: 'json',
    	            success: function(data) {
    	                if(data.mensaje == 1 && data.vehicle[0].internal == 0 && data.vehicle[0].states_id == 1) {
    	                	vehicles = (data.vehicle[0].id);
    	                	var html = '';
    	                	var tipo;
	                        if (data.vehicle[0].internal == 0) {
	                        	tipo = '<td>Visita</td>';
	                        }
	                        else if (data.vehicle[0].internal == 1) {
	                        	tipo = '<td>Interno</td>';
	                        }
	                        else if (data.vehicle[0].internal == 2) {
	                        	tipo = '<td>Contratista</td>';
	                        }
    	                	html += '<tr>';
    	                	html += '<td>'+patente+'</td>';
    	                	html += tipo;
    	                	html += '<td>'+data.vehicle[0].company+'</td>';
    	                	html += '<td>'+data.vehicle[0].name+' '+data.vehicle[0].last_name+'</td>';
    	                	html += '<td>'+data.vehicle[0].type+'</td>';
    	                	html += '<td>'+data.vehicle[0].profile+'</td>';
    	                	html += '<td><button type="button" class="btn btn-xs btn-danger" onclick="menosauto()">Remover</button></td>';
    	                	html += '</tr>';
    	                	$("#tbody-vehicle").append(html);
    	                	$("#checkauto").attr('disabled', true);
    	                	$("#sectionAuto").addClass('hidden');
    	                	$("#acin").prop('disabled', false);
    						$("#acout").prop('disabled', false);
    	                }
    	                else if (data.mensaje == 1 && data.vehicle[0].internal != 0) {
    	                	alert("El vehículo posee un registró como interno o contratista.")
    	                }
    	                else if (data.mensaje == 1 && data.vehicle[0].states_id != 1) {
    	                	alert("El vehículo posee un estado de bloqueo o suspendido.")
    	                }
    	                else {
    	                	$("#patent").val(patente);
    	                	$("#patent").attr('disabled', true);
    	                    $("#model").attr('disabled', false);
    	                    $("#control").attr('disabled', false);
    	                    $("#vcompanies").attr('disabled', false);
    	                    $("#vehicle_people").attr('disabled', false);
    	                    $("#vehicle_type").attr('disabled', false);
    	                    $("#vprofile").attr('disabled', false);
    	                    $("#modal-add-vehicle").modal('show');
    	                }
    	            }
    	        });
    	    }
    	});

    	$("#formModal-add-vehicle").submit(function(event) {
    		event.preventDefault();

    		var patent 		= $("#patent").val();
    		var model 		= $("#model").val();
    		var internal 	= $("#control option:selected").text();
    		var empresa 	= $("#vcompanies option:selected").text();
    		var responsable = $("#vehicle_people option:selected").text();
    		var tipo 		= $("#vehicle_type option:selected").text();
    		var perfil 		= $("#vprofile option:selected").text();

    		$.ajax({
    		    url: site_url + '/cAccess_People/addVehicle/',
    		    type: 'post',
    		    data: {
    		        patent                  : $("#patent").val(),
    		        model                   : $("#model").val(),
    		        internal                : $("#control option:selected").val(),
    		        nfc_code                : 0,
    		        companies_id            : $("#vcompanies option:selected").val(),
    		        people_id               : $("#vehicle_people option:selected").val(),
    		        vehicles_type_id        : $("#vehicle_type option:selected").val(),
    		        vehicles_profiles_id    : $("#vprofile option:selected").val()
    		    },
    		    dataType: 'json',
    		    success: function(data) {
    		        if (data != 0) {
    		            vehicles = (data);
                        var html = '';
                        html += '<tr>';
                        html += '<td>'+patent+'</td>';
                        html += '<td>'+tipo+'</td>';
                        html += '<td>'+empresa+'</td>';
                        html += '<td>'+responsable+'</td>';
                        html += '<td>'+tipo+'</td>';
                        html += '<td>'+perfil+'</td>';
                        html += '<td><button type="button" class="btn btn-xs btn-danger" onclick="menosauto()">Remover</button></td>';
                        html += '</tr>';
                        $("#tbody-vehicle").append(html);
                        $("#modal-add-vehicle").modal('hide');
                        $("#patent").val('');
                        $("#checkauto").attr('disabled', true);
                        $("#sectionAuto").addClass('hidden');
                        $("#acin").prop('disabled', false);
    					$("#acout").prop('disabled', false);
    		        }
    		        else if (data == 0) {
    		            alert("Error en el proceso...")
    		            window.location.replace(site_url+"/cAccess_People/");
    		        }
    		    }
    		});
    	});

		//form
    	$("#optionForm").change(function(){
    		form_id = $(this).val();
			if ($(this).val() != '') {
    			$.ajax({
    				url: site_url + '/cAccess_People/LoadForm/',
    				type: 'POST',
    				dataType: 'html',
    				data: {
    					form: form_id
    				},
    				success: function(pagina, data){
    					$("#bodyform").html(pagina);
    					$('.area').wysihtml5();
    				}
    			});
    		}
    		else if ($(this).val() == '') {
    			$("#bodyform").html('');
    		}
    	});

    	$("#acin").change(function(event) {
    		if ($(this).prop('checked') == true){
				$("#optionForm").attr('disabled', false);
			}
			else if($(this).prop('checked') == false){
				$("#optionForm").attr('disabled', true);
			}
    	});
    	
    	$("#acout").change(function(event) {
    		if ($(this).prop('checked') == true) {
				$("#optionformEnd").prop('disabled', false);
			}
			else if ($(this).prop('checked') == false) {
				$("#optionformEnd").prop('disabled', true);
			}
    	});
		

    	$("#optionformEnd").change(function() {
    		form_id_end = $(this).val();
    	});

    	//ruta puertas
		$("a[name='puertas']").click(function(event) {
			event.preventDefault();

			var id = $(this).attr('id')
			var level = $(this).data('level')
			
			if ($(this).find('.box-header').css("background-color") == "rgb(210, 214, 222)") {
				wpuertas.push($(this).attr('id'));
				$(this).find('.box-header').css("background-color", "#5cb85c");
				$(this).find('.box-header').css("background", "#5cb85c");
			}
			else {
				$(this).find('.box-header').css("background-color", "#d2d6de");
				$(this).find('.box-header').css("background", "#d2d6de");
				wpuertas.splice(wpuertas.indexOf(id), 1);
			}
		});

		//agrego al array la id de la persona principal
		aditional_people_id.push('<?php if(!empty($people[0]['id'])) echo $people[0]["id"];?>');

		$("#formModal-add-aditional_people").submit(function(event) {
            event.preventDefault();

			var rut = document.getElementById('rut');
            if(checkRut(rut)) {
                //rut formato válido
                //validar si existe el rut en la BD
                $.ajax({
                    url: site_url + '/cAccess_People/generateAuthorization/',
                    data: {rut: rut.value},
                    type: 'post',
                    dataType: 'json',
                    success: function(data) {
                        if(data.response == 1) {
                            //existe en la bd traer los datos y desplegarlos y agregarlo al listado de adicionales a ingresar
                            if(data.people.length > 0) {
                            	if(data.people[0].internal == '0' && data.people[0].states_id == 1)
                            	{
                            		$('#name').val(data.people[0].name);
	                            	$('#last_name').val(data.people[0].last_name);
	                            	$('#address').val(data.people[0].address);
	                            	$('#email').val(data.people[0].email);
	                            	$('#phone').val(data.people[0].phone);
	                            	$('#people_profiles').val(data.people[0].people_profiles_id);
	                            	$('#companies').val(data.people[0].companies_id);

	                            	addAditionalPeople(data.people[0].id, rut.value, data.people[0].name, data.people[0].last_name);
	                            	$('#modal-add-people').modal('hide');
                            	}
                            	else {
                            		$('#modal-add-people').modal('hide');
                            		if (data.people[0].states_id != 1) {
                            			alert("La persona esta suspendida o bloqueada");
                            		}
                            	}
                            	            
                            }
                        }
                        else if(data.response == 2) {
                            //existe en la bd traer los datos y desplegarlos y agregarlo al listado de adicionales a ingresar
                            if(data.people.length > 0) {
                            	if(data.people[0].internal == '0')
                            	{
                            		alert('Esta Persona actualmente se encuentra al interior del Recinto.');
	                            	$('#modal-add-people').modal('hide');
                            	}
                            	else
                            		$('#modal-add-people').modal('hide');
                            	            
                            }
                        }
                        else if(data.response == 0) {
                            //no existe en la bd
                            $('#modal-title').html('Persona Adicional Inexistente (Agregar)');
                            $('#rut').val(data.rut);

                            $('#rut').attr('disabled', true);
                            $('#name').attr('disabled', false);
                            $('#last_name').attr('disabled', false);
                            $('#address').attr('disabled', false);
                            $('#email').attr('disabled', false);
                            $('#phone').attr('disabled', false);
                            $('#people_profiles').attr('disabled', false);
                            $('#companies').attr('disabled', false);

                            var people_profile;
                            var companies;

                            if(data.people_profile.length > 0 && data.people_profile != null) {
                                people_profile = '<option value="">Seleccione una opción</option>';
                                for(var i=0;i<data.people_profile.length;i++)
                                {
                                    people_profile += '<option value="'+data.people_profile[i].id+'">'+data.people_profile[i].profile+'</option>';
                                }
                            }

                            if(data.companies.length > 0 && data.companies != null) {
                                companies = '<option value="">Seleccione una opción</option>';
                                for(var i=0;i<data.companies.length;i++)
                                {
                                    companies += '<option value="'+data.companies[i].id+'">'+data.companies[i].company+'</option>';
                                }
                            }

                            $('#people_profiles').html(people_profile);
                            $('#companies').html(companies);

                            $('#modal-add-people').modal('show');
                            $('#name').focus();
                        }
                    }
                });
            }
            else {
            	//rut formato inválido
            }

            if(accion_form == 0) {
	            var rut = document.getElementById('rut');
	            if(checkRut(rut)) {
	                //rut formato válido
	                //validar si existe el rut en la BD
	                $.ajax({
	                    url: site_url + '/cAccess_People/generateAuthorization/',
	                    data: {rut: rut.value},
	                    type: 'post',
	                    dataType: 'json',
	                    success: function(data) {
	                        if(data.response == 1) {
	                            //existe en la bd traer los datos y desplegarlos y agregarlo al listado de adicionales a ingresar
	                            if(data.people.length > 0) {
	                            	
	                            	if(data.people[0].internal == '0' && data.people[0].states_id == 1)
	                            	{
	                            		$('#name').val(data.people[0].name);
		                            	$('#last_name').val(data.people[0].last_name);
		                            	$('#address').val(data.people[0].address);
		                            	$('#email').val(data.people[0].email);
		                            	$('#phone').val(data.people[0].phone);
		                            	$('#people_profiles').val(data.people[0].people_profiles_id);
		                            	$('#companies').val(data.people[0].companies_id);

		                            	addAditionalPeople(data.people[0].id, rut.value, data.people[0].name, data.people[0].last_name);
		                            	$('#modal-add-people').modal('hide');
	                            	}
	                            	else {
                            			$('#modal-add-people').modal('hide');
	                            		if (data.people[0].states_id != 1) {
	                            			alert("La persona esta suspendida o bloqueada");
	                            		}
                            		}
	                            }
	                        }
	                        else if(data.response == 0) {
	                            //no existe en la bd
	                            //$('form#formModal-add-aditional_people').removeAttr('id');
	                            accion_form = 1;

	                            $('#modal-title').html('<div style="background-color:red;color:white;">Persona Adicional Inexistente (Agregar)</div>');
	                            $('#rut').val(data.rut);

	                            $('#rut').attr('disabled', true);
	                            $('#name').attr('disabled', false);
	                            $('#last_name').attr('disabled', false);
	                            $('#address').attr('disabled', false);
	                            $('#email').attr('disabled', false);
	                            $('#phone').attr('disabled', false);
	                            $('#people_profiles').attr('disabled', false);
	                            $('#companies').attr('disabled', false);

	                            var people_profile;
	                            var companies;

	                            if(data.people_profile.length > 0 && data.people_profile != null) {
	                                people_profile = '<option value="">Seleccione una opción</option>';
	                                for(var i=0;i<data.people_profile.length;i++)
	                                {
	                                    people_profile += '<option value="'+data.people_profile[i].id+'">'+data.people_profile[i].profile+'</option>';
	                                }
	                            }

	                            if(data.companies.length > 0 && data.companies != null) {
	                                companies = '<option value="">Seleccione una opción</option>';
	                                for(var i=0;i<data.companies.length;i++)
	                                {
	                                    companies += '<option value="'+data.companies[i].id+'">'+data.companies[i].company+'</option>';
	                                }
	                            }

	                            $('#people_profiles').html(people_profile);
	                            $('#companies').html(companies);

	                            $('#modal-add-people').modal('show');
	                            $('#name').focus();
	                        }
	                    }
	                });
	            }
	            else {
	            	//rut formato inválido
	            }
	        }
	        else if(accion_form == 1) {
	        	var rut = $('#rut').val();
	            var digit = rut[rut.length-1];
	            rut = rut.slice(0, -2);
	            var name = $('#name').val();
	            var last_name = $('#last_name').val();
	            var address = $('#address').val();
	            var email = $('#email').val();
	            var phone = $('#phone').val();
	            var profile = $('#people_profiles').val();
	            var company = $('#companies').val();

	            $.ajax({
	                url: site_url + '/cAccess_People/addPeople_getID/',
	                type: 'post',
	                data: {rut: rut, digit: digit, name: name, last_name: last_name, address: address, email: email, phone: phone, allow_all: 0, is_visited: 0,internal:0, people_profiles_id: profile, companies_id: company},
	                dataType: 'text',
	                success: function(data) {
	                    if(data > 0) {
	                    	//si se ingresa correctamente, retorno el id, listar en personas adicionales
	                    	addAditionalPeople(data, rut+'-'+digit, name, last_name);
	                    	$('#modal-add-people').modal('hide');
	                    }
	                    else {
	                        alert('Verifique que el RUT no este ingresado.');
	                    }
	                }
	            });
	        }
        });

		$.post(
			site_url + "/cAccess_People/getAllReasons_Visit",{},
			function(data){
				var dato = JSON.parse(data)
				reason = dato;
				for (var i = 0; i < reason.length; i++) {
					$("#ulreason").append(`
						<li class="list-group-item">
							<div class="wrapepr">
								<span>
									<label>
										<input type="checkbox" name="rvisit" id="`+reason[i].id+`"> &nbsp;&nbsp;
										<span> `+reason[i].reason+`</span>
									</label>
								</span>
							</div>
						</li>
						`)
				}
			}
		);

		$("#formReason").submit(function(e) {
			e.preventDefault();

			var reasons = $("#reason").val()

			$.post(
				site_url + "/cAccess_People/addReason_Visit",{
					reason 	: 	reasons
				},
				function(data){
					reason.push({id : data, reason : reasons})
					$('#modal-add-reason').modal('hide');
					$("#ulreason").append(`
						<li class="list-group-item">
							<div class="wrapepr">
								<span>
									<label>
										<input type="checkbox" name="rvisit" id="`+data+`"> &nbsp;&nbsp;
										<span> `+reasons+`</span>
									</label>
								</span>
							</div>
						</li>
						`);
				}
			)
		});

		var cont = 0;
		/////////
		$("#formWizard").submit(function(e) {
			e.preventDefault();
			
			var hours = $("#permitidas").val();
			var end_time = $("#salida").text();
			var access_state_id = $("#state").val();
			var observation = $("#observation").val();
			var control_init = ($("#acin").prop('checked') == true) ? '1' : '0';
			var control_end = ($("#acout").prop('checked') == true) ? '1' : '0';

			if (cont == 0) {//
				waditional.push($("#input-add-rut1").data('id'));

				$("tr[name='aditionalpeople']").each(function(index, el) {
					waditional.push($(this).attr('id').substr(3))
				});
				$("input:checkbox:checked[data-type='area']").each(function(index, el) {
	        		wareas.push($(this).attr('id'));
	        	});
	        	$("input:checkbox:checked[data-type='dpto']").each(function(index, el) {
	        		wdptos.push($(this).attr('id'));
	        	});
	        	$("input:checkbox:checked[data-type='zone']").each(function(index, el) {
	        		wzones.push($(this).attr('id'));
	        	});
	        	$("input:checkbox:checked[name='rvisit']").each(function(index, el) {
	        		wreason.push($(this).attr('id'));
	        	});
			}
			if (cont != 0) {
				if (waditional.length == 0) {
					waditional.push($("#input-add-rut1").data('id'));
					$("tr[name='aditionalpeople']").each(function(index, el) {
						waditional.push($(this).attr('id').substr(3))
					});
				}
				if (wareas.length == 0) {
					$("input:checkbox:checked[data-type='area']").each(function(index, el) {
		        		wareas.push($(this).attr('id'));
		        	});
				}
				if (wdptos.length == 0) {
					$("input:checkbox:checked[data-type='dpto']").each(function(index, el) {
		        		wdptos.push($(this).attr('id'));
		        	});
				}
				if (wzones.length == 0) {
					$("input:checkbox:checked[data-type='zone']").each(function(index, el) {
						wzones.push($(this).attr('id'));
					});
				}
				if (wreason.length == 0) {
					$("input:checkbox:checked[name='rvisit']").each(function(index, el) {
						wreason.push($(this).attr('id'));
					});
				}
			}		
			
			//validaciones minimas
			if ($("#acin").prop('checked') == true) {
				answer = [];
				forder = [];
				$(".fm").each(function(index, el) {
					if ($(this).val() == '') {
						$("#mensaje-error").text('Debe contestar todas las preguntas del formulario.');
					}
					else {
						answer.push($(this).val())
						forder.push(index+1)
					}
				});
				$(".checks").each(function(el) {
					var indexx = forder.length;

					if ($(this).prop('checked') == true) {
						answer.push('1');
						forder.push(indexx+1);
					}
					else if ($(this).prop('checked') == false) {
						answer.push('0');
						forder.push(indexx+1);
					}
				});
				if ($("#optionForm").val() == '') {
					$("#mensaje-error").text('Debe seleccionar un formulario.');
					answer = [];
					forder = [];
				}
	    		else if($("#formtitle").val() == '') {
	    			$("#mensaje-error").text('El campo título del formulario no puede estar vacío.');
	    			answer = [];
					forder = [];
	    		}
			}
			
			if ($("#acin").prop('checked') == true && answer.length == 0) {
				$("#mensaje-error").text('Debe completar el formulario de ingreso.');
				cont++;
			}
			else if ($("#acout").prop('checked') == true && $("#optionformEnd").val() == '') {
				$("#mensaje-error").text('Debe seleccionar el formulario a usarse para el control de salida.');
				cont++;
			}
			else if (hours == 0) {
        		$("#mensaje-error").text('El campo Horas permitidas debe contener un numero mayor a 0')
        		cont++;
        	}
        	else if (wreason.length == 0) {
        		$("#mensaje-error").text('Debe marcar por lo menos un motivo de visita')
        		cont++;
        	}
        	else if (wvisit.length == 0) {
        		$("#mensaje-error").text('Debe agregar por lo menos una persona a ser visitada')
        		cont++;
        	}
        	else if (wzones.length == 0) {
        		$("#mensaje-error").text('Debe marcar por lo menos una zona')
        		cont++;
        	}
        	else if (wpuertas.length == 0) {
        		$("#mensaje-error").text('Debe marcar por lo menos una puerta en la ruta')
        		cont++;
        	}
        	else if (hours > 0 && wreason.length > 0 && wvisit.length > 0 && wzones.length > 0 && wpuertas.length > 0) {
        		$.ajax({
	                url: site_url + '/cAccess_People/generateAccessIn/',
	                data: { 
	                	hours 			:	hours,
	                	end_time		:	end_time,
	                	people_id 		:	waditional,
	                	access_state_id	: 	access_state_id,
	                	observation		:	observation,

	                	people_areas 		: wareas,
	           			people_department 	: wdptos,
	           			people_zones 		: wzones,
	           			reasons_visit 		: wreason,
	           			route 				: wpuertas,
	           			people_visit 		: wvisit,

	           			vehicle_id 		: vehicles,
	           			control_init 	: control_init,
	           			control_end 	: control_end,

	           			ftitle 			: $("#formtitle").val(),
	           			fobservation 	: $("#formobservation").val(),
	           			forder 			: forder,
	           			answer 			: answer,
	           			form_id			: form_id,
	           			form_id_end 	: form_id_end
	           		},
	                type: 'post',
	                dataType: 'json',
	                beforeSend: function(){
	                	$('#div-question_loading').html('<img style="width:70px;height:70px;" src="<?php echo base_url();?>assets/img/cargando.gif">'); 
	                 }, 
	                success: function(data) {
	                	if (data[0].mensaje == 1) {
	                		window.location.replace(site_url+"/cAccess_People/");
	                	}
	                	else if (data[0].mensaje == 0) {
	                		alert("Error en el proceso...");
	                		window.location.replace(site_url+"/cAccess_People/");
	                	}
	                }
	            });
        	}
		});

		$.widget.bridge('uibutton', $.ui.button);

	    //Initialize tooltips
	    $('.nav-tabs > li a[title]').tooltip();
	    
	    //Wizard
	    $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
	        var $target = $(e.target);
	        if ($target.parent().hasClass('disabled')) {
	            return false;
	        }
	    });

	    $(".next-step").click(function (e) {
	    	var stepid = $('.wizard .nav-tabs li.active a').attr('aria-controls');
	    	if (validateformstep(stepid)) {
	    		var $active = $('.wizard .nav-tabs li.active');
	        	$active.next().removeClass('disabled');
	        	nextTab($active);
	    	}
	        
	    });

	    $(".prev-step").click(function (e) {
	        var $active = $('.wizard .nav-tabs li.active');
	        prevTab($active);
	    });
	});

	function nextTab(elem) {
	    $(elem).next().find('a[data-toggle="tab"]').click();
	}

	function prevTab(elem) {
	    $(elem).prev().find('a[data-toggle="tab"]').click();
	}

	var wwreason = [];
	var wwzones = [];
	var wwpuertas = [];

	function validateformstep(stepid){
		$("input:checkbox:checked[name='rvisit']").each(function(index, el) {
    		wwreason.push($(this).attr('id'));
    	});
    	$("input:checkbox:checked[data-type='zone']").each(function(index, el) {
			wwzones.push($(this).attr('id'));
		});
		switch(stepid) {
		    case 'step1':
		        return true;
		        break;

		    case 'step2':
		        if (wwreason.length == 0) {
		        	alert('Debe marcar por lo menos un motivo de visita');
		        	return false;
		        }
		        else if (wvisit.length == 0) {
        			alert('Debe agregar por lo menos una persona a ser visitada');
        			return false;
        		}
	        	else {
	        		return true;
	        	}
		        break;

	        case 'step3':
		        if (wwzones.length == 0) {
		        	alert('Debe marcar por lo menos una zona');
		        	return false;
		        }
		        else if (wpuertas.length == 0){
		        	alert('Debe marcar por lo menos una puerta');
		        	return false;
		        }
		        else {
		        	return true;
		        }
		        break;

	        case 'step4':
	        	var retorno = true;
	        	if ($("#acin").prop('checked') == false && $("#acout").prop('checked') == false) {
	        		return true;
	        	}
	        	else if ($("#acin").prop('checked') == true && $("#acout").prop('checked') == false) {
	        		if ($("#optionForm").val() == '') {
						alert('Debe seleccionar un formulario.');
						return false;
					}
					else if($("#formtitle").val() == '') {
    					alert('El campo título del formulario no puede estar vacío.');
    					return false;
    				}
    				$(".fm").each(function(index, el) {
						if ($(this).val() == '') {
							if (retorno == true) {
								alert('Debe contestar todas las preguntas del formulario.');
							}
							retorno = false;
						}
					});
					$(".checks").each(function(el) {
						if ($(this).prop('checked') == false) {
							if (retorno == true) {
								alert('Debe contestar todas las preguntas del formulario.');
							}
							retorno = false;
						}
					});
    				return retorno;
	        	}
	        	else if ($("#acin").prop('checked') == true && $("#acout").prop('checked') == true) {
	        		if ($("#optionformEnd").val() == '') {
	        			alert('Debe seleccionar un formulario.');
						return false;
	        		}
	        		else if ($("#optionForm").val() == '') {
						alert('Debe seleccionar un formulario.');
						return false;
					}
					else if($("#formtitle").val() == '') {
    					alert('El campo título del formulario no puede estar vacío.');
    					return false;
    				}
    				$(".fm").each(function(index, el) {
						if ($(this).val() == '') {
							if (retorno == true) {
								alert('Debe contestar todas las preguntas del formulario.');
							}
							retorno = false;
						}
					});
					$(".checks").each(function(el) {
						if ($(this).prop('checked') == false) {
							if (retorno == true) {
								alert('Debe contestar todas las preguntas del formulario.');
							}
							retorno = false;
						}
					});
    				return retorno;
	        	}
	        	else if ($("#acin").prop('checked') == false && $("#acout").prop('checked') == true) {
	        		if ($("#optionformEnd").val() == '') {
	        			alert('Debe seleccionar un formulario.');
						return false;
	        		}
	        		else
	        			return true;
	        	}
	        	break;
		}
	}

	function openModalAddAditionalPeople() {
		$('.input_aditional_people').val('');

		var select = $('#select-add-aditional_is_visited');
		select.val($('option:first', select).val());

		select = $('#select-add-aditional_profile');
		select.val($('option:first', select).val());

		select = $('#select-add-aditional_company');
		select.val($('option:first', select).val());

		accion_form = 0;
		$('#modal-title').html('Datos Persona Adicional');

		$('#rut').val('');
		$('#name').val('');
    	$('#last_name').val('');
    	$('#address').val('');
    	$('#email').val('');
    	$('#phone').val('');
    	$('#people_profiles').val('');
    	$('#companies').val('');

    	$('#rut').attr('disabled', false);
        $('#name').attr('disabled', true);
        $('#last_name').attr('disabled', true);
        $('#address').attr('disabled', true);
        $('#email').attr('disabled', true);
        $('#phone').attr('disabled', true);
        $('#people_profiles').attr('disabled', true);
        $('#companies').attr('disabled', true);

		$('#modal-add-people').modal('show');
		setTimeout(function(){ $('#rut').focus(); }, 500);
	}

	function openModalAddReason_Visit() {
		$('#modal-add-reason').modal('show');
	}

	$("#permitidas").change(function(){
    	Number.prototype.padDigit = function () {
	       return (this < 10) ? '0' + this : this;
	   	}

	   	var fecha = '<?php echo date('Y-m-d', time()) ?>';
	   	var fecha = fecha.split('-');

     	var t1 = $("#salida").attr('name');
     	t1 = t1.split(':');

     	var t2 = $("#permitidas").val();

	    var mins = 0;
	    var hrs = 0;

    	mins = Number(t1[1]);
    	//minhrs = Math.floor(parseInt(mins / 60));
    	hrs = Number(t1[0]) + Number(t2);
    	mins = mins % 60;

    	hrsdia = Math.floor(parseInt(hrs / 24));
    	dia = Number(fecha[2]) + hrsdia;
    	
    	(hrs >= 24) ? hrs = hrs-24 : hrs = hrs;
    	t1 = hrs.padDigit() + ':' + mins.padDigit()
    	$("#salida").text(t1+' '+fecha[0]+'-'+fecha[1]+'-'+dia.padDigit())
    })

    var wvisit = [];
    var visitselect = [];

    function addvisi() {
    	if (wvisit.includes($("#listvisit").val()) != true) {
    		var n = $("#listvisit option:selected").text().split(' ');
    		$("tbody[name=visitbody]").append(`
    			<tr id="tr-`+$("#listvisit").val()+`">
    			<td>`+$("#listvisit option:selected").data('rut')+`</td>
    			<td>`+n[0]+`</td>
    			<td>`+n[1]+`</td>
    			<td><button type="button" class="btn btn-danger btn-xs" onclick="removeVisit(`+$("#listvisit").val()+`);">remover</button></td>;
    			</tr>
    			`);
    		wvisit.push($("#listvisit option:selected").val());
    		var temp = [];
    		$("#ulreason input:checkbox:checked").each(function(index, el) {
    			temp.push($(this).attr('name'))
    		});
    		visitselect.push(temp)
    	}
    }

    function removeVisit(id){
    	var i = myIndexOf(wvisit, id);
    	if (i != -1) {
    		wvisit.splice(i, 1);
    		$("#tr-"+id).remove();
    	}
    }

    function searchbrother(level, name, id) {
    	var mensaje = false
    	$("#ubicacion input:checkbox:checked").each(function(index, el) {
    		if ($(this).data('level') == level && $(this).attr('name') == name && $(this).attr('id') != id) {
    			mensaje = true
    		}
    	});
    	return mensaje
    }

    $("#ubicacion input:checkbox").change(function(event) {
    	if ($(this).is(':checked')) {
    		var name = $(this).attr('name');
    		var id = $(this).attr('id');
    		var level = $(this).data('level');

    		var padres = [];
    		if ($(this).data('level') == 1) { //si es area
    			ruta($(this).attr('id'), 'add', 'area')
    			$("#ubicacion input:checkbox").each(function(index, el) {
					if ($(this).data('level') == 0 && $(this).attr('id') == name) { //marco zona padre
						$(this).prop('checked', true)
						ruta($(this).attr('id'), 'add', 'zona')
					}
					if ($(this).data('level') == 2 && $(this).attr('name') == id) { //marca dpto hijo
						$(this).prop('checked', true)
						ruta($(this).attr('id'), 'add', 'dpto')
					}
				});
			}
    		if ($(this).data('level') == 2) { //si es dpto
    			ruta($(this).attr('id'), 'add', 'dpto')
    			$("#ubicacion input:checkbox").each(function(index, el) {
					if ($(this).data('level') == 1 && $(this).attr('id') == name) { //marco area padre
						$(this).prop('checked', true)
						padres.push($(this).attr('name'));
						ruta($(this).attr('id'), 'add', 'area')
					}
				});
				for (var j = 0; j < padres.length; j++) {
					$("#ubicacion input:checkbox").each(function(index, el) {
						if ($(this).data('level') == 0 && $(this).attr('id') == padres[j]) { //marco zona abuelo
							$(this).prop('checked', true)
							ruta($(this).attr('id'), 'add', 'zona')
						}
					});
				}
				padres = [];
			}
    		if ($(this).data('level') == 0) { // si es zona
    			ruta($(this).attr('id'), 'add', 'zona')
    			$("#ubicacion input:checkbox").each(function(index, el) {
    				if ($(this).data('level') == 1 && $(this).attr('name') == id) { //marca areas hijos
	    				$(this).prop('checked', true)
	    				padres.push($(this).prop('id'));
	    				ruta($(this).attr('id'), 'add', 'area')
	    			}
				});
				for (var j = 0; j < padres.length; j++) {
					$("#ubicacion input:checkbox").each(function(index, el) {
		    			if ($(this).data('level') == 2 && $(this).attr('name') == padres[j]) { //marca dptos nietos
		    				$(this).prop('checked', true)
		    				ruta($(this).attr('id'), 'add', 'dpto')
		    			}
					});
				}
				padres = [];
    		}
    	}
    	var check = $(this).is(':checked');
    	if (!check) {
    		var name = $(this).attr('name');
    		var id = $(this).attr('id');
    		var level = $(this).data('level');
    		var padres = [];
    		if ($(this).data('level') == 1) { //si es area
    			$("#ubicacion input:checkbox").each(function(index, el) {
					if ($(this).data('level') == 0 && $(this).attr('id') == name) { //marco zona padre
						if (!searchbrother(level, name, id)) {
							$(this).prop('checked', false)
							ruta($(this).attr('id'), 'del', 'zona')
						}
					}
					if ($(this).data('level') == 2 && $(this).attr('name') == id) { //marca dpto hijo
						$(this).prop('checked', false)
						ruta($(this).attr('id'), 'del', 'dpto')
					}
				});
			}
    		if ($(this).data('level') == 2) { //si es dpto
    			$("#ubicacion input:checkbox").each(function(index, el) {
					if ($(this).data('level') == 1 && $(this).attr('id') == name) { //marco area padre
						if (!searchbrother(level, name, id)) {
							$(this).prop('checked', false)//borro area
							if (!searchbrother($(this).data('level'), $(this).attr('name'), $(this).attr('id'))) {
								padres.push($(this).attr('name'));
								ruta($(this).attr('id'), 'del', 'area')
							}
							
						}
					}
				});
				for (var j = 0; j < padres.length; j++) {
					$("#ubicacion input:checkbox").each(function(index, el) {
						if ($(this).data('level') == 0 && $(this).attr('id') == padres[j]) { //marco zona abuelo
							$(this).prop('checked', false)
							ruta($(this).attr('id'), 'del', 'zona')
						}
					});
				}
				padres = [];
			}
    		if ($(this).data('level') == 0) { // si es zona
    			$("#ubicacion input:checkbox").each(function(index, el) {
    				if ($(this).data('level') == 1 && $(this).attr('name') == id) { //marca areas hijos
	    				$(this).prop('checked', false)
	    				padres.push($(this).attr('id'));
	    				ruta($(this).attr('id'), 'del', 'area')
	    			}
				});
				for (var j = 0; j < padres.length; j++) {
					$("#ubicacion input:checkbox").each(function(index, el) {
		    			if ($(this).data('level') == 2 && $(this).attr('name') == padres[j]) { //marca dptos nietos
		    				$(this).prop('checked', false)
		    				ruta($(this).attr('id'), 'del', 'dpto')
		    			}
					});
				}
				padres = [];
    		}
    		check = null;
    	}
    });

	var dzones = <?php echo json_encode($treeview['dzones']); ?>;
	var dareas = <?php echo json_encode($treeview['dareas']); ?>;
	var ddptos = <?php echo json_encode($treeview['ddepto']); ?>;
	var rdoor = [];

    function ruta(id, accion, ubicacion){
    	if (ubicacion === 'zona') {
    		if (accion === 'add') {
    			for (var i = 0; i < dzones.length; i++) {
    				if (dzones[i].zones_id == id) {
    					rdoor.push(dzones[i].door_z)
    				}
    			}
    			$("a[name='puertas']").each(function(index, el) {
    				if(rdoor.includes($(this).attr('id'))){
    					//validar si se agrega o no la puerta
    					var i = myIndexOf(wpuertas, $(this).attr('id') );
						if(i == -1)
    						wpuertas.push($(this).attr('id'));
    					//-------------------------------------------
    					$(this).find('.box-header').css("background-color", "#5cb85c");
    					$(this).find('.box-header').css("background", "#5cb85c");
    				} 
    			});
    		}
    		if (accion === 'del') {
    			$("a[name='puertas']").each(function(index, el) {
    				if(rdoor.includes($(this).attr('id'))){
    					$(this).find('.box-header').css("background-color", "#d2d6de");
						$(this).find('.box-header').css("background", "#d2d6de");
						//validar si se elimina o no la puerta
    					var i = myIndexOf(wpuertas, $(this).attr('id') );
						if(i != -1)
						{
    						wpuertas.splice(i, 1);
    						rdoor.splice(rdoor.indexOf($(this).attr('id')), 1);
						}
    					//-------------------------------------------
    				} 
    			});
    		}
    	}
    	if (ubicacion === 'area') {
    		if (accion === 'add') {
    			for (var i = 0; i < dareas.length; i++) {
    				if (dareas[i].areas_id == id) {
    					rdoor.push(dareas[i].door_a)
    				}
    			}
    			$("a[name='puertas']").each(function(index, el) {
    				if(rdoor.includes($(this).attr('id'))){
    					//validar si se agrega o no la puerta
    					var i = myIndexOf(wpuertas, $(this).attr('id') );
						if(i == -1)
    						wpuertas.push($(this).attr('id'));
    					//-------------------------------------------
    					$(this).find('.box-header').css("background-color", "#5cb85c");
    					$(this).find('.box-header').css("background", "#5cb85c");
    				} 
    			});
    		}
    		if (accion === 'del') {
    			$("a[name='puertas']").each(function(index, el) {
    				if(rdoor.includes($(this).attr('id'))){
    					$(this).find('.box-header').css("background-color", "#d2d6de");
						$(this).find('.box-header').css("background", "#d2d6de");
						//validar si se elimina o no la puerta
    					var i = myIndexOf(wpuertas, $(this).attr('id') );
						if(i != -1)
						{
    						wpuertas.splice(i, 1);
    						rdoor.splice(rdoor.indexOf($(this).attr('id')), 1);
						}
    					//-------------------------------------------
    				} 
    			});
    		}
    	}
    	if (ubicacion === 'dpto') {
    		if (accion === 'add') {
    			for (var i = 0; i < ddptos.length; i++) {
    				if (ddptos[i].departments_id == id) {
    					rdoor.push(ddptos[i].door_d)
    				}
    			}
    			$("a[name='puertas']").each(function(index, el) {
    				if(rdoor.includes($(this).attr('id'))){
    					//validar si se agrega o no la puerta
    					var i = myIndexOf(wpuertas, $(this).attr('id') );
						if(i == -1)
    						wpuertas.push($(this).attr('id'));
    					//-------------------------------------------
    					$(this).find('.box-header').css("background-color", "#5cb85c");
    					$(this).find('.box-header').css("background", "#5cb85c");
    				} 
    			});
    		}
    		if (accion === 'del') {
    			$("a[name='puertas']").each(function(index, el) {
    				if(rdoor.includes($(this).attr('id'))){
    					$(this).find('.box-header').css("background-color", "#d2d6de");
						$(this).find('.box-header').css("background", "#d2d6de");
						//validar si se elimina o no la puerta
    					var i = myIndexOf(wpuertas, $(this).attr('id') );
						if(i != -1)
						{
    						wpuertas.splice(i, 1);
    						rdoor.splice(rdoor.indexOf($(this).attr('id')), 1);
						}
    					//-------------------------------------------
    				} 
    			});
    		}
    	}
    }

    function btnCancelAdd() {
    	window.location.replace(site_url+"/cAccess_People/");
    }

    function menosauto(){
    	$("#tbody-vehicle tr").remove();
    	$("#checkauto").attr('disabled', false);
    	$("#checkauto").prop('checked', false);
    	// $("#sectionAuto").removeClass('hidden');

    	$("#patente").val('');
    	vehicles = 0;

    	$("#acin").prop('disabled', true);
		$("#acout").prop('disabled', true);
    }

    //------------------------------------------------------------------------

    function checkRut(rut) {
        // Despejar Puntos
        var valor = rut.value.replace('.','');
        // Despejar Guión
        valor = valor.replace('-','');
        
        // Aislar Cuerpo y Dígito Verificador
        var cuerpo = valor.slice(0,-1);
        var dv = valor.slice(-1).toUpperCase();
        // Formatear RUN
        rut.value = cuerpo + '-'+ dv
        // Si no cumple con el mínimo ej. (n.nnn.nnn)
        if(cuerpo.length < 7) 
        {
            rut.setCustomValidity("RUT Incompleto");
            rut.setCustomValidity('');
            cuerpo = '';
            dv = '';
            rut.value = '';
            return false;
        }
        else
        {
            // Calcular Dígito Verificador
            suma = 0;
            multiplo = 2;
            
            // Para cada dígito del Cuerpo
            for(i=1;i<=cuerpo.length;i++) {
            
                // Obtener su Producto con el Múltiplo Correspondiente
                index = multiplo * valor.charAt(cuerpo.length - i);
                
                // Sumar al Contador General
                suma = suma + index;
                
                // Consolidar Múltiplo dentro del rango [2,7]
                if(multiplo < 7) { multiplo = multiplo + 1; } else { multiplo = 2; }
          
            }
            
            // Calcular Dígito Verificador en base al Módulo 11
            dvEsperado = 11 - (suma % 11);
            
            // Casos Especiales (0 y K)
            dv = (dv == 'K')?10:dv;
            dv = (dv == 0)?11:dv;
            
            // Validar que el Cuerpo coincide con su Dígito Verificador
            if(dvEsperado != dv) 
            { 
                rut.setCustomValidity("RUT Inválido");
                rut.setCustomValidity('');
                cuerpo = '';
                dv = '';
                rut.value = '';
                return false;
            }
            // Si todo sale bien, eliminar errores (decretar que es válido)
            rut.setCustomValidity('');
            return true;
        }
    }

    function addAditionalPeople(id, rut, nombre, apellidos) {
    	var html = '';

    	html += '<tr id="tr-'+id+'" name="aditionalpeople">';
    	html += '<td>'+rut+'</td>';
    	html += '<td>'+nombre+'</td>';
    	html += '<td>'+apellidos+'</td>';
    	html += '<td><button type="button" class="btn btn-danger btn-xs" onclick="removeAditionalPeople('+id+');">remover</button></td>';
    	html += '</tr>';

    	if(aditional_people_id.indexOf(id) == -1){
    		aditional_people_id.push(id);
    		$('#tbody-aditional').append(html);
    	}
    }

    function removeAditionalPeople(id) {
    	var i = myIndexOf(aditional_people_id, id);

    	if(i != -1){
    		aditional_people_id.splice(i, 1);
    		$('#tr-'+id).remove();
    	}
    }

    function myIndexOf(array, item) {
    	var indice = -1;
    	var i = 0;
    	while(i < array.length)
    	{
    		if(array[i] == item)
    			indice = i;
    		i++;
    	}

    	return indice;
    }

    function removeVehicle(id) {
        var i = myIndexOf(vehicles, id);

        if(i != -1){
            vehicles.splice(i, 1);
            $('#tr-'+id).remove();
        }
    }

			  
</script>

</body>
</html>


