<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-success">
					<div class="box-header ui-sortable-handle">
					    <i class="fa fa-balance-scale"></i>
						<h3 class="box-title">Editar estado de acceso #<?php echo $access_state[0]['id']; ?></h3>
				  	</div>
				  	
					<form class="form-horizontal" id="editAccess_State">
			  			<div class="box-body">
			  				<div class="form-group">
			  					<label for="state" class="col-sm-2 control-label">Nombre</label>
			  					<div class="col-sm-10">
			  						<input type="text" class="form-control" name="state" id="state" value="<?php echo $access_state[0]['state']; ?>" required>
			  					</div>
			  			</div>
			  			<div class="box-footer">
			  				<button type="submit" class="btn btn-primary pull-right">Guardar</button>
			  			</div>
			  		</form>
				</div>
			</div>
		</div>
	</section>
	
</div>


<?php $this->view('footer'); ?>

<script>
	$(document).ready(function() {
		$("#editAccess_State").submit(function(event) {
			event.preventDefault();

			$.post(
				site_url + "/cAccess_State/editAccess_State",{
					id 			: 	<?php echo $access_state[0]['id']; ?>,
					state 		: 	$("#state").val()
				},
				function(data){
					if (data == 1) {
						window.location.replace(site_url+"/cAccess_State/");
					}
					else {
						alert("Error en el proceso...")
						window.location.replace(site_url+"/cAccess_State/edit?id="+<?php echo $access_state[0]['id']; ?>);
					}
				}
			);

			// $('#li-measure').addClass('menu-open');
			// $('#ul-measure').css('display', 'block');
		});
	});
	
</script>
</body>
</html>
