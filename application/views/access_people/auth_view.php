<style type="text/css">
  @import url('<?php echo base_url()?>assets/font-awesome/css/font-awesome.min.css');

  main {
    min-width: 320px;
    max-width: 1100px;
    padding: 20px;
    margin: 0 auto;
    background: #fff;
  }

  .tab-pane {
    display: none;
    padding: 20px 0 0;
    border-top: 1px solid #ddd;
  }

  input {
    display: none;
  }

  label {
    display: inline-block;
    margin: 0 0 -1px;
    padding: 15px 25px;
    font-weight: 600;
    text-align: center;
    //color: #bbb;
    border: 1px solid transparent;
  }

  label:before {
    font-family: fontawesome;
    font-weight: normal;
    margin-right: 10px;
  }

  label:hover {
    color: #888;
    cursor: pointer;
  }

  input:checked + label {
    color: #555;
    border: 1px solid #ddd;
    border-top: 2px solid orange;
    border-bottom: 1px solid #fff;
  }

  #tab1:checked ~ #content1,
  #tab2:checked ~ #content2,
  #tab3:checked ~ #content3,
  #tab4:checked ~ #content4,
  #tab5:checked ~ #content5,
  #tab6:checked ~ #content6,
  #tab7:checked ~ #content7 {
    display: block;
  }
</style>

<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-sm-12">
                <div class="box box-success">
                    <div class="box-header ui-sortable-handle">
                        <i class="fa fa-sign-in"></i>
                        <h3 class="box-title">Detalle</h3>
                    </div>
                    
                    <div class="box-body">
                        <main>
                    
                            <input id="tab1" type="radio" name="tabs" checked>
                            <label for="tab1"><i class="fa fa-list-alt" aria-hidden="true"></i>&nbsp;Acceso</label>

                            <input id="tab2" type="radio" name="tabs">
                            <label for="tab2"><i class="fa fa-user" aria-hidden="true"></i>&nbsp;Persona</label>
                              
                            <input id="tab3" type="radio" name="tabs">
                            <label for="tab3"><i class="fa fa-list-alt" aria-hidden="true"></i>&nbsp;Motivos</label>
                              
                            <input id="tab4" type="radio" name="tabs">
                            <label for="tab4"><i class="fa fa-map-marker" aria-hidden="true"></i>&nbsp;Ubicaciones</label>

                            <input id="tab5" type="radio" name="tabs">
                            <label for="tab5"><i class="fa fa-random" aria-hidden="true"></i>&nbsp;Movimientos</label>

                            <input id="tab6" type="radio" name="tabs">
                            <label for="tab6"><i class="fa fa-file-text" aria-hidden="true"></i>&nbsp;Formularios</label>

                            <input id="tab7" type="radio" name="tabs">
                            <label for="tab7"><i class="fa fa-history" aria-hidden="true"></i>&nbsp;Estados</label>

                            <div id="content1" class="tab-pane">
                                <fieldset>
                                    <legend>Datos Acceso</legend>

                                    <table style="width: 100%;">
                                        <tbody>
                                            <tr>
                                                <td><label>Portería</label></td>
                                                <td id="td-main_access">

                                                <?php
                                                    if(!empty($access_people[0]['access']))
                                                    {
                                                        echo $access_people[0]['access'];
                                                    }
                                                ?>
                                                    
                                                </td>    
                                            </tr>

                                            <tr>
                                                <td><label>Creado</label></td>
                                                <td id="td-created">

                                                <?php
                                                    if(!empty($access_people[0]['created']))
                                                    {
                                                        echo $access_people[0]['created'];
                                                    }
                                                ?>
                                                    
                                                </td>    
                                            </tr>

                                            <tr>
                                                <td><label>Movimiento</label></td>
                                                <td id="td-entry">

                                                <?php
                                                    if(!empty($access_people[0]['entry']))
                                                    {
                                                        if($access_people[0]['entry'] == 0)
                                                            echo '<label class="label label-success">Ingreso</label>';
                                                        else
                                                            echo '<label class="label label-danger">Salida</label>';
                                                    }
                                                    else
                                                        echo '<label class="label label-success">Ingreso</label>';
                                                ?>
                                                    
                                                </td>    
                                            </tr>

                                            <tr>
                                                <td><label>Horas</label></td>
                                                <td id="td-hours">
                                                    <?php
                                                    if(!empty($access_people[0]['hours']))
                                                    {
                                                        echo $access_people[0]['hours'];
                                                    }
                                                    ?>
                                                </td>    
                                            </tr>

                                            <tr>
                                                <td><label>Hora Salida</label></td>
                                                <td id="td-end_time">
                                                    <?php
                                                        if(!empty($access_people[0]['end_time']))
                                                        {
                                                            echo $access_people[0]['end_time'];
                                                        }
                                                    ?>
                                                </td>    
                                            </tr>

                                            <tr>
                                                <td><label>Permitido por:</label></td>
                                                <td>
                                                    <?php
                                                    if(!empty($access_people[0]['approved_by']))
                                                    {
                                                        $approved_by = $access_people[0]['approved_by'];

                                                        $this->db->select('rut, digit, name, last_name');
                                                        $this->db->from('people');
                                                        $this->db->where('id', $approved_by);
                                                        $this->db->limit(1);

                                                        $res = $this->db->get()->result_array();
                                                        if(!empty($res))
                                                        {
                                                            echo $res[0]['rut'].'-'.$res[0]['digit'].' | '.$res[0]['name'].' '.$res[0]['last_name'];
                                                        }
                                                        else
                                                            echo 'S/A';
                                                    }
                                                    else
                                                        echo 'S/A';
                                                    ?>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td><label>Estado</label></td>
                                                <td id="td-access_state">
                                                    <?php
                                                        if(!empty($access_people[0]['state']))
                                                        {
                                                            if($access_people[0]['access_state_id'] == 1)
                                                                echo '<label class="label label-warning">'.$access_people[0]['state'].'</label>';
                                                            else if($access_people[0]['access_state_id'] == 2)
                                                                echo '<label class="label label-success">'.$access_people[0]['state'].'</label>';
                                                            else
                                                                echo '<label class="label label-danger">'.$access_people[0]['state'].'</label>';
                                                        }
                                                    ?>
                                                </td>    
                                            </tr>

                                            <tr>
                                                <td><label>Vehículo</label></td>
                                                <td id="td-access_vehicles">
                                                    <?php 
                                                        if (!empty($access_people_vehicle[0]['patent'])) {
                                                            echo $access_people_vehicle[0]['patent'].' | '.$access_people_vehicle[0]['type'].' | '.$access_people_vehicle[0]['profile'];
                                                        }
                                                        else {
                                                            echo 'N/A';
                                                        }
                                                     ?>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td><label>Controlado</label></td>
                                                <td id="td-access_control">
                                                    <?php 
                                                    if (!empty($access_people_control[0]['control_init'])) {
                                                        echo '<i class="fa fa-check-square-o" aria-hidden="true"></i> Entrada &nbsp;';
                                                    }
                                                    else {
                                                        echo '<i class="fa fa-square-o" aria-hidden="true"></i> Entrada &nbsp;';
                                                    }
                                                    if (!empty($access_people_control[0]['control_end'])) {
                                                        echo '<i class="fa fa-check-square-o" aria-hidden="true"></i> Salida &nbsp;';
                                                    }
                                                    else {
                                                        echo '<i class="fa fa-square-o" aria-hidden="true"></i> Salida &nbsp;';
                                                    }
                                                    ?>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td><label>Observación</label></td>
                                                <td>
                                                    <textarea disabled class="form-control textarea" rows="6" id="area-observation">
                                                        <?php
                                                            if(!empty($access_people[0]['observation']))
                                                            {
                                                                echo $access_people[0]['observation'];
                                                            }
                                                        ?>
                                                    </textarea>
                                                </td>    
                                            </tr>
                                        </tbody>
                                    </table>
                                </fieldset>
                            </div>
                            
                            <div id="content2" class="tab-pane">
                                <fieldset>
                                    <legend>Datos Persona</legend>
                                    <table style="width: 100%;">
                                        <tbody>
                                            <tr>
                                                <td><label>Rut</label></td>
                                                <td id="td-rut">
                                                    <?php
                                                    
                                                        $rut = '';

                                                        if(!empty($access_people[0]['rut']))
                                                        {
                                                            $rut .= $access_people[0]['rut'];

                                                            if(!empty($access_people[0]['digit']))
                                                            {
                                                                $rut .= '-'.$access_people[0]['digit'];
                                                            }

                                                            echo $rut;
                                                        }
                                                    ?>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td><label>Nombre</label></td>
                                                <td id="td-name">
                                                    <?php
                                                    
                                                        $name = '';

                                                        if(!empty($access_people[0]['name']))
                                                        {
                                                            $name .= $access_people[0]['name'];

                                                            if(!empty($access_people[0]['last_name']))
                                                            {
                                                                $name .= ' '.$access_people[0]['last_name'];
                                                            }

                                                            echo $name;
                                                        }
                                                    ?>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td><label>Teléfono</label></td>
                                                <td id="td-phone">
                                                    <?php
                                                        if(!empty($access_people[0]['phone']))
                                                        {
                                                            echo $access_people[0]['phone'];
                                                        }
                                                    ?>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td><label>Email</label></td>
                                                <td id="td-email">
                                                    <?php
                                                        if(!empty($access_people[0]['email']))
                                                        {
                                                            echo $access_people[0]['email'];
                                                        }
                                                    ?>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td><label>Perfil</label></td>
                                                <td id="td-profile">
                                                    <?php
                                                        if(!empty($access_people[0]['profile']))
                                                        {
                                                            echo $access_people[0]['profile'];
                                                        }
                                                    ?>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td><label>Empresa</label></td>
                                                <td id="td-company">
                                                    <?php
                                                        if(!empty($access_people[0]['company']))
                                                        {
                                                            echo $access_people[0]['company'];
                                                        }
                                                    ?>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </fieldset>
                            </div>
                              
                            <div id="content3" class="tab-pane">
                                <fieldset>
                                    <legend>Motivos de Visita</legend>
                                    <table style="width: 40%;" id="table-reasons_visit">
                                        <tbody>
                                            <tr>
                                                <td><label>Motivo/s</label></td>    
                                                <td name="datos-reasons">
                                                    <?php
                                                        if(!empty($access_people_reasons))
                                                        {
                                                            foreach($access_people_reasons as $apr)
                                                            {
                                                                echo  $apr['reason'].'<br>';
                                                            }
                                                        }
                                                    ?>
                                                </td>
                                            </tr>
                                        </tbody>                        
                                    </table>
                                </fieldset>
                                <br>
                                <fieldset>
                                    <legend>Personas a Visitar</legend>
                                    <table style="width: 50%;" id="table-people_visit">
                                        <tbody>
                                            <tr>
                                                <td><label>Persona/s</label></td>    
                                                <td name="datos-visit">
                                                    <?php
                                                        if(!empty($access_people_visit))
                                                        {
                                                            foreach($access_people_visit as $apv)
                                                            {
                                                                echo  $apv['rut'].'-'.$apv['digit'].' | '.$apv['name'].' '.$apv['last_name'].'<br>';
                                                            }
                                                        }
                                                    ?>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </fieldset>
                            </div>
                            
                            <div id="content4" class="tab-pane">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <fieldset>
                                                <legend>Zonas</legend>
                                                <table style="width: 100%;" id="table-zones">
                                                    <tbody>
                                                        <tr>
                                                            <td>
                                                                <?php
                                                                    if(!empty($access_people_zones))
                                                                    {
                                                                        foreach($access_people_zones as $apz)
                                                                        {
                                                                            echo  $apz['zone'].'<br>';
                                                                        }
                                                                    }
                                                                ?>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </fieldset>
                                        </div>

                                        <div class="col-md-4">
                                            <fieldset>
                                                <legend>Áreas</legend>
                                                <table style="width: 100%;" id="table-areas">
                                                    <tbody>
                                                        <tr>
                                                            <td>
                                                                <?php
                                                                    if(!empty($access_people_areas))
                                                                    {
                                                                        foreach($access_people_areas as $apa)
                                                                        {
                                                                            echo  $apa['area'].'<br>';
                                                                        }
                                                                    }
                                                                ?>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </fieldset>
                                        </div>

                                        <div class="col-md-4">
                                            <fieldset>
                                                <legend>Deptos.</legend>
                                                <table style="width: 100%;" id="table-department">
                                                    <tbody>
                                                        <tr>
                                                            <td>
                                                                <?php
                                                                    if(!empty($access_people_departments))
                                                                    {
                                                                        foreach($access_people_departments as $apd)
                                                                        {
                                                                            echo  $apd['department'].'<br>';
                                                                        }
                                                                    }
                                                                ?>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </fieldset>
                                        </div>
                                    </div>
                                </div>
                                <br>                          
                                <fieldset>
                                    <legend>Ruta de Puertas</legend>
                                    <br>

                                    <?php

                                    if(!empty($access_people_route))
                                    {

                                        for($i=0;$i<count($access_people_route);$i++)
                                        {
                                            if($i % 3 == 0)
                                            {
                                                echo '<div class="row"><div class="col-md-12">';
                                                echo '<div class="col-md-2">';
                                                echo '<div class="card" style="width: 18rem;">';
                                                echo '<img class="card-img-top" src="'.base_url().'/assets/img/door.png" alt="Card image cap">';
                                                echo '<div class="card-body">';
                                                echo '<h5 class="card-title">'.$access_people_route[$i]['door'].'</h5>';
                                                echo '<p class="card-text">'.$access_people_route[$i]['description'].'</p>';
                                                echo '</div>';
                                                echo '</div>';
                                                echo '</div>';

                                                if($i < count($access_people_route) -1)
                                                {
                                                    echo '<div class="col-md-2"><i class="fa fa-arrow-right fa-2x" aria-hidden="true"></i></div>';
                                                }

                                            }
                                            else if(($i + 1) % 3 == 0 )
                                            {
                                                echo '<div class="col-md-2">';
                                                echo '<div class="card" style="width: 18rem;">';
                                                echo '<img class="card-img-top" src="'.base_url().'/assets/img/door.png" alt="Card image cap">';
                                                echo '<div class="card-body">';
                                                echo '<h5 class="card-title">'.$access_people_route[$i]['door'].'</h5>';
                                                echo '<p class="card-text">'.$access_people_route[$i]['description'].'</p>';
                                                echo '</div>';
                                                echo '</div>';
                                                echo  '</div>';

                                                if($i < count($access_people_route) -1 )
                                                {
                                                echo  '<div class="col-md-2"><i class="fa fa-arrow-right fa-2x" aria-hidden="true"></i></div>';
                                                }

                                                echo  '</div></div><br><br>';
                                            }
                                            else
                                            {
                                                echo  '<div class="col-md-2">';
                                                echo '<div class="card" style="width: 18rem;">';
                                                echo '<img class="card-img-top" src="'.base_url().'/assets/img/door.png" alt="Card image cap">';
                                                echo '<div class="card-body">';
                                                echo '<h5 class="card-title">'.$access_people_route[$i]['door'].'</h5>';
                                                echo '<p class="card-text">'.$access_people_route[$i]['description'].'</p>';
                                                echo '</div>';
                                                echo '</div>';
                                                echo  '</div>';

                                                if($i < count($access_people_route) -1 )
                                                {
                                                    echo  '<div class="col-md-2"><i class="fa fa-arrow-right fa-2x" aria-hidden="true"></i></div>';
                                                }
                                            }
                                        }

                                    }

                                    ?>
                                </fieldset>
                            </div>

                            <div id="content5" class="tab-pane">
                                <div class="row">
                                    <div class="col-md-12">
                                        <fieldset>
                                            <legend>Movimientos: <small><span class="badge" style="background-color: #00a65a">Exito</span> <span class="badge" style="background-color: #dd4b39">Error</span> </small></legend>
                                            <br>
                                                <div class="table responsive">
                                                    <table id="table-intents" class="table table-striped table-bordered table-condensed" style="width:100%;">
                                                        <thead>
                                                            <tr>
                                                                <th>N</th>
                                                                <th>Puerta</th>
                                                                <th>Descripción</th>
                                                                <th>Movimiento</th>
                                                                <th>Tipo</th>
                                                                <th>Fecha</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php
                                                            if(!empty($access_people_intents))
                                                            {
                                                                for($i=0;$i<count($access_people_intents);$i++)
                                                                {
                                                                    $flow = $access_people_intents[$i]['flow'];

                                                                    if($flow == 0)
                                                                        $flow = 'PEATONAL';
                                                                    else
                                                                        $flow = 'VEHICULAR';

                                                                    $entry = $access_people_intents[$i]['entry'];

                                                                    if($entry == 0)
                                                                        $entry = 'ENTRADA';
                                                                    else
                                                                        $entry = 'SALIDA';

                                                                    if($access_people_intents[$i]['success'] == 1)
                                                                    {
                                                                        echo '<tr style="background-color: #00a65a;color:white;">';
                                                                        echo '<td>'.($i+1).'</td>';
                                                                        echo '<td>'.$access_people_intents[$i]['door'].'</td>';
                                                                        echo '<td>'.$access_people_intents[$i]['description'].'</td>';
                                                                        echo '<td>'.$entry.'</td>';
                                                                        echo '<td>'.$flow.'</td>';
                                                                        echo '<td>'.$access_people_intents[$i]['created'].'</td>';
                                                                        echo '</tr>';
                                                                    }
                                                                    else
                                                                    {
                                                                        echo '<tr style="background-color: #dd4b39;color:white;">';
                                                                        echo '<td>'.($i+1).'</td>';
                                                                        echo '<td>'.$access_people_intents[$i]['door'].'</td>';
                                                                        echo '<td>'.$access_people_intents[$i]['description'].'</td>';
                                                                        echo '<td>'.$entry.'</td>';
                                                                        echo '<td>'.$flow.'</td>';
                                                                        echo '<td>'.$access_people_intents[$i]['created'].'</td>';
                                                                        echo '</tr>';
                                                                    }
                                                                    
                                                                }
                                                            }
                                                        ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                        </fieldset>
                                    </div>
                                </div>
                                <br>
                            </div>

                            <div id="content6" class="tab-pane">
                                <fieldset>
                                    <legend>Formulario de control entrada</legend>
                                    <table style="width: 100%;" class="table" id="table-form-in">
                                        <tbody>
                                             <tr>
                                                 <td><strong>Título</strong></td>
                                                 <td>
                                                    <?php 
                                                    if (!empty($access_people_form_in[0]->title)) {
                                                        echo '<span>'.$access_people_form_in[0]->title.'</span>';
                                                    }
                                                    ?>
                                                 </td>
                                             </tr>
                                             <tr>
                                                 <td><strong>Observación</strong></td>
                                                 <td>
                                                     <?php 
                                                     if (!empty($access_people_form_in[0]->observation)) {
                                                         echo '<span>'.$access_people_form_in[0]->observation.'</span>';
                                                     }
                                                     ?>
                                                 </td>
                                             </tr>
                                            <?php 
                                            $i = 0;
                                            if (!empty($access_people_form_inDetail) && !empty($getDetail_Access_People_Form_in_answers)) {
                                                foreach ($access_people_form_inDetail as $k) { ?>
                                                    <tr>
                                                        <td><strong><?php echo $k->question; ?></strong></td>
                                                        <td><span>
                                                            <?php 
                                                            if(!empty($getDetail_Access_People_Form_in_answers[$i]->answer)) {
                                                                echo $getDetail_Access_People_Form_in_answers[$i]->answer; 
                                                                echo $k->measure;
                                                            }
                                                            else {
                                                                echo 'N/A';
                                                            } ?>
                                                            </span></td>
                                                    </tr>
                                            <?php $i++;}
                                             } ?>
                                             <tr>
                                                 
                                             </tr>
                                        </tbody>                        
                                    </table>
                                </fieldset>
                                <br>
                                <fieldset>
                                    <legend>Formulario de control salida</legend>
                                    <table style="width: 100%;" class="table" id="table-form-in">
                                        <tbody>
                                             <tr>
                                                 <td><strong>Título</strong></td>
                                                 <td>
                                                    <?php 
                                                    if (!empty($access_people_form_in[1]->title)) {
                                                        echo '<span>'.$access_people_form_in[1]->title.'</span>';
                                                    }
                                                    else if (!empty($access_people_form_in[1]->title)) {
                                                        echo '<span>'.$access_people_form_in[1]->title.'</span>';
                                                    }
                                                    ?>
                                                 </td>
                                             </tr>
                                             <tr>
                                                 <td><strong>Observación</strong></td>
                                                 <td>
                                                     <?php 
                                                     if (!empty($access_people_form_in[1]->observation)) {
                                                         echo '<span>'.$access_people_form_in[1]->observation.'</span>';
                                                     }
                                                     else if (!empty($access_people_form_in[1]->observation)) {
                                                         echo '<span>'.$access_people_form_in[1]->observation.'</span>';
                                                     }
                                                     ?>
                                                 </td>
                                             </tr>
                                            <?php 
                                            $i = 0;
                                            if (!empty($access_people_form_outDetail) && !empty($getDetail_Access_People_Form_out_answers)) {
                                                foreach ($access_people_form_outDetail as $k) { ?>
                                                    <tr>
                                                        <td><strong><?php echo $k->question; ?></strong></td>
                                                        <td><span><?php echo $getDetail_Access_People_Form_out_answers[$i]->answer; ?> <?php echo $k->measure; ?></span></td>
                                                    </tr>
                                            <?php $i++;}
                                             } ?>
                                             <tr>
                                                 
                                             </tr>
                                        </tbody>                        
                                    </table>
                                </fieldset>
                                <br>
                                <?php 
                                    if (!empty($access_people_form_in[0]->title) || !empty($access_people_form_in[1]->title)) { ?>
                                        <a href="<?php echo site_url(); ?>/CAccess_People/getDetailPDF?id=<?php echo $myid; ?>" class="btn btn-primary btn-xs" role="button">
                                            <i class='fa fa-file-pdf-o'></i> PDF
                                        </a>
                                <?php } ?>
                            </div>

                            <div id="content7" class="tab-pane">
                                <fieldset>
                                    <legend>Historial de Estados</legend>
                                    <table style="width: 80%;" class="table" id="table-reasons_visit">
                                        <thead>
                                            <th>Estado</th>
                                            <th>Descripción</th>
                                            <th>Fecha</th>
                                        </thead>
                                        <tbody>
                                             <?php
                                                if(!empty($access_people_state_history))
                                                {
                                                    foreach($access_people_state_history as $apsh)
                                                    {
                                                        echo '<tr>';
                                                        echo '<td>'.$apsh['state'].'</td>';
                                                        echo '<td>'.$apsh['description'].'</td>';
                                                        echo '<td>'.$apsh['created'].'</td>';
                                                        echo '</tr>';
                                                    }
                                                }
                                            ?>
                                        </tbody>                        
                                    </table>
                                </fieldset>
                            </div>
                        </main>
                    </div>
                    <div class="box-footer">
                        <a href="<?php echo site_url(); ?>/cAccess_People/List_Access_People" class="btn btn-primary pull-right" role="button">
                            <i class='fa fa-undo'></i> Volver
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<?php $this->load->view('footer');?>

<script>

    $(document).ready(function() {
        $('#table-intents').DataTable(
            { 
                'language': { "url": base_url + "assets/Spanish.json" },
            }
        );

        $('#li-visits').addClass('menu-open');
        $('#ul-visits').css('display', 'block');
        
        $('#li-access_people_vehicles').addClass('menu-open');
        $('#ul-access_people_vehicles').css('display', 'block');
    });
    /*
    var intents = '<?php //echo json_encode($intents); ?>;';
    
    for (var i = 0; i < intents.length; i++) {
        $(".bg-yellow").each(function(index, el) {
            if ($(this).attr('id') == intents[i].doors_id && intents[i].success == 1) {
                $(this).removeClass('bg-yellow')
                $(this).addClass('bg-green')
            }
            else if($(this).attr('id') == intents[i].doors_id && intents[i].success == 0) {
                $(this).removeClass('bg-yellow')
                $(this).addClass('bg-red')
            }
        });
    }

    */
</script>

</body>
</html>
