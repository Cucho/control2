<div class="content-wrapper">
	<section class="content">
    <div class="row">
      <div class="col-sm-12">
        <div class="box box-success">
          <div class="box-header ui-sortable-handle">
              <i class="fa fa-list-alt"></i>
            <h3 class="box-title">Registros Peatonales</h3>
            </div>

            <div class="box-body">
              <section class="content">
                  <table id="access_people" class="table table-striped table-bordered table-condensed" style="width:100%;">
                      <thead>
                        <tr>
                          <th width="10%">ID</th>
                          <th>Rut</th>
                          <th>Nombre</th>
                          <th>Perfil</th>
                          <th>Portería</th>
                          <th>Empresa</th>
                          <th>Movimiento</th>
                          <th>Estado</th>
                          <th>Creado</th>
                          <th>Salida</th>
                          <th>Acciones</th>
                        </tr>
                      </thead>
                      <tbody></tbody>
                  </table>
              </section>
            </div>
            <div class="box-footer"></div>
        </div>
      </div>
    </div>
  </section>
	
</div>

<?php $this->view('footer'); ?>

<script>
  var edit = <?php echo $this->session->userdata('edit'); ?>;
  var del = <?php echo $this->session->userdata('del'); ?>;

    $(document).ready(function() {
      
      $('#access_people').DataTable({
          "lengthMenu": [[5, 10, 15, 20,], [5, 10, 15, 20]],
          'responsive': true,
          'paging': true,
          'info': true,
          'filter': true,
          'ordering': true,
          // 'stateSave': true,
          'processing':true,
          'serverSide':true,
          'language': {
            "url": base_url + "assets/Spanish.json"
          },
          "order": [[0, "desc"]],
          'ajax': {
            "url": site_url + "/cAccess_People/datatable",
            "type":"POST",
          },
          "columns": [
            { "data": "ID"},
            { "data": "Rut" },
            { "data": "Persona" },
            { "data": "Perfil" },
            { "data": "Portería" },
            { "data": "Empresa" },
            { "data": "Movimiento" },
            { "data": "Estado" },
            { "data": "Creado" },
            { "data": "Salida" },
            { "data": "Acciones" }
          ],
          "columnDefs": [
            {
              "targets": [0],
              "orderable": true,
              "render": function(data, type, row) {
                return row.id
              }
            },
            {
              "targets": [1],
              "orderable": true,
              "render": function(data, type, row) {
                return row.rut+'-'+row.digit
              }
            },
            {
              "targets": [2],
              "orderable": true,
              "render": function(data, type, row) {
                return row.name+' '+row.last_name
              }
            },
            {
              "targets": [3],
              "orderable": true,
              "render": function(data, type, row) {
                return row.profile
              }
            },
            {
              "targets": [4],
              "orderable": true,
              "render": function(data, type, row) {
                return row.access
              }
            },
            {
              "targets": [5],
              "orderable": true,
              "render": function(data, type, row) {
                return row.company
              }
            },
            {
              "targets": [6],
              "orderable": true,
              "render": function(data, type, row) {
                if(row.entry == 0)
                  return '<label class="label label-success">Ingreso</label>'
                else
                  return '<label class="label label-danger">Salida</label>'
              }
            },
            {
              "targets": [7],
              "orderable": true,
              "render": function(data, type, row) {
                if(row.access_state_id == 1)
                  return '<label class="label label-warning">'+row.state+'</label>'
                else if(row.access_state_id == 2)
                  return '<label class="label label-success">'+row.state+'</label>'
                else
                  return '<label class="label label-danger">'+row.state+'</label>'
              }
            },
            {
              "targets": [8],
              "orderable": true,
              "render": function(data, type, row) {
                return row.created
              }
            },
            {
              "targets": [9],
              "orderable": true,
              "render": function(data, type, row) {
                return row.exit_time
              }
            },
            {
              "targets": [10],
              "orderable": false,
              "render": function(data, type, row) {
                return del == true ? `
                  <a href="<?php echo site_url(); ?>/cAccess_People/getDetail?id=`+row.id+`" class="btn btn-primary btn-xs" role="button">
                    <i class='fa fa-search'></i> Ver
                  </a>
                  <button class="btn btn-danger btn-xs" role="button" onclick="deleteAccessPeople(`+row.id+`);">
                    <i class='fa fa-trash'></i> Eliminar
                  </button>` : del == false ? `
                  <a href="<?php echo site_url(); ?>/cAccess_People/getDetail?id=`+row.id+`" class="btn btn-primary btn-xs" role="button">
                    <i class='fa fa-search'></i> Ver
                  </a>` : '';
              }
            }
           ],
        });

      $('#li-visits').addClass('menu-open');
      $('#ul-visits').css('display', 'block');
        
      $('#li-access_people_vehicles').addClass('menu-open');
      $('#ul-access_people_vehicles').css('display', 'block');
    });

  function deleteAccessPeople(id)
  {
    var c = confirm('Confirme la eliminación de este registro');

    if(c)
    {
      $.ajax({
        url: site_url + '/cAccess_People/deleteAccessPeople',
        data: {id: id},
        type: 'post',
        dataType: 'text',
        success: function(data)
        {
          if(data == 1)
            location.reload();
          else
            alert('Ha ocurrido un error con la eliminación.\nExisten dependencias asociadas a este registro.');
        }
      });
    }
  }

  
</script>

</body>
</html>
