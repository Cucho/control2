<br>
<table class="table table-condensed" style="width:100%;">
	<tr>
		<td><label>Título:</label></td>
		<td><input type="text" class="form-control" style="width:700px;" id="formtitle" required></td>
	</tr>
	<tr>
		<td><label>Observación:</label></td>
		<td><textarea id="formobservation" class="form-control area" style="width:700px;" rows="5" required></textarea></td>
	</tr>
<?php foreach ($form as $k) { ?>
	<tr>
		<td><label><?php echo $k->question.':'; ?></label></td>
		<?php if ($k->atid == 1) { ?>
			<td><input type="text" class="form-control fm" style="width:700px;" id="<?php echo 'rc'.$k->order; ?>" placeholder="<?php echo $k->placeholder; ?>" required></td>
		<?php } else if ($k->atid == 2) { ?>
			<td><textarea class="form-control area fm" style="width:700px;" rows="5" id="<?php echo 'rp'.$k->order; ?>"" placeholder="<?php echo $k->placeholder; ?>" required></textarea></td>
		<?php } else if ($k->atid == 3) { ?>
			<td><input type="number" class="fm" style="width:200px;" id="<?php echo 'rc'.$k->order; ?>" placeholder="<?php echo $k->placeholder; ?>" required><?php echo ' '.$k->measure; ?></td>
		<?php } else if ($k->atid == 4) { ?>
			<td><input type="date" class="fm" style="width:200px;" id="<?php echo 'f'.$k->order; ?>" placeholder="<?php echo $k->placeholder; ?>" required></td>
		<?php } else if ($k->atid == 5) { ?>
			<td><input type="checkbox" class="checks" style="width:20px;" id="<?php echo 'rb'.$k->order; ?>" required><?php echo $k->placeholder; ?></td>
		<?php } ?>
	</tr>
<?php } ?>
</table>