<div class="content-wrapper">
	<section class="content">
    <div class="row">
      <div class="col-sm-12">
        <div class="box box-success">
          <div class="box-header ui-sortable-handle">
              <i class="fa fa-sign-in"></i>
            <h3 class="box-title">Listado accesos al interior </h3>
            </div>

            <div class="box-body">
              <section class="content">
                  <button class="btn btn-primary" onclick="redirectStateActualList();">Actualizar</button>
                  <span></span>
                  <a href="<?php echo site_url(); ?>/cAccess/PDF_Access_In" class="btn btn-primary">Excel</a>
                  <br><br>
                  <table id="table-access_in" class="table table-striped table-bordered table-condensed" style="width:100%;">
                      <thead>
                        <tr>
                          <!--<th width="10%">ID</th>-->
                          <th>Rut</th>
                          <th>Nombre</th>
                          <th>Perfil</th>
                          <th>Empresa</th>
                          <th>Tipo</th>
                          <th>Puerta</th>
                          <th>Movimiento</th>
                          <th>Salida</th>
                          <th>Creado</th>
                          <th>Acción</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                        if(!empty($list_access_in))
                        {
                          foreach($list_access_in as $lai)
                          {
                            $flow = $lai['flow'];
                            if($flow == 0)
                              $flow = 'Peatonal';
                            else
                              $flow = 'Vehicular '.$lai['patent'];

                            $internal = $lai['internal'];

                            if($internal == 0)
                              $internal = 'Visita';
                            else if($internal == 1)
                              $internal = 'Interno';
                            else
                              $internal = 'Contratista';

                            $action = $lai['action'];
                            if($action == 0)
                              $action = 'Ingreso';
                            else
                              $action = 'Salida';

                            echo '<tr>';
                            //echo '<td>'.$lai['id'].'</td>';
                            echo '<td>'.$lai['rut'].'-'.$lai['digit'].'</td>';
                            echo '<td>'.$lai['name'].' '.$lai['last_name'].'</td>';
                            echo '<td>'.$internal.' | '.$lai['profile'].'</td>';
                            echo '<td>'.$lai['company'].'</td>';
                            echo '<td>'.$flow.'</td>';
                            echo '<td>'.$lai['door'].'</td>';
                            echo '<td>'.$action.'</td>';
                            echo '<td>'.$lai['end_time'].'</td>';
                            echo '<td>'.$lai['created'].'</td>';
                            echo '<td><button onclick="generateExit('.$lai['rut'].', '.$lai['internal'].');" class="btn btn-warning btn-xs">salir</button></td>';
                            echo '</tr>';
                          }
                        }
                        ?>
                      </tbody>
                  </table>
              </section>
            </div>
            <div class="box-footer"></div>
        </div>
      </div>
    </div>
  </section>
	
</div>

<?php $this->view('footer'); ?>

<script>
    $(document).ready(function() {
      $('#table-access_in').DataTable({
        'language': {
            "url": base_url + "assets/Spanish.json"
          },
      });
    });

    function generateExit(rut, internal)
    {
      var p = prompt('Ingrese motivo de salida');
      while(p.trim().length == 0)
        p = prompt('Ingrese motivo de salida');

      $.ajax({
        url: site_url+'/CAccess/generateExit',
        type: 'post',
        dataType: 'text',
        data: {rut: rut, internal: internal, motivo: p},
        success: function(response)
        {
          var res = parseInt(response);
          if(res == 1){
            alert('Salida generada para el rut '+rut);
            location.reload();
          }
          else if(res == 0)
            alert('Error del proceso');
          else
            alert('Las salidas manuales solo son posibles desde una portería.');
        }
      });
    }

</script>

</body>
</html>
