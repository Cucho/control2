<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Estado Actual Interior</title>
  <!--  CSS -->
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>estadoActual/public/assets/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>estadoActual/public/assets/css/custom.css">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>estadoActual/public/assets/css/font-awesome/css/font-awesome.min.css">
  <!-- JS NODE -->
  <script src="<?php echo base_url();?>estadoActual/node_modules/socket.io-client/dist/socket.io.js"></script>
  <script src="<?php echo base_url();?>estadoActual/public/actual_state.js"></script>
  <!--  -->
  
</head>
<body>
<div class="wrapper">
  <div class="content-wrapper">
  <section class="content">
    <div class="row">
      <div class="col-sm-12">
        <div class="box box-success">
          <div class="box-header ui-sortable-handle">
            
            <div class="row">
              <div class="col-md-12">
                <div class="col-md-8 col-md-offset-2">
                  <!--<h4 class="box-title">Estado Actual Interior :</h4>-->
                  <table class="table">
                    <tr>
                      <td align="center"><h2>PEATONES</h2></td>
                      <td align="center"><h2 id="h2-general-people-count">0</h2></td>
                      <td align="center"><h2>VEHICULOS</h2></td>
                      <td align="center"><h2 id="h2-general-vehicles-count">0</h2></td>
                    </tr>
                  </table>
                   <hr>
                </div>
              </div>
            </div>
           
            <div class="row">
              <div class="col-md-12">

                <div class="col-md-2">
                  <div class="wrimagecard wrimagecard-topimage">
                    <a href="#">
                    <div class="wrimagecard-topimage_header" style="background-color: rgba(119, 178, 88, 0.1)">
                      
                      
                      <table style="width: 100%;">
                        <tr>
                          <td align="center" width="40%"><i class="fa fa-users fa-3x"></i></td>
                          <td align="center" width="60%"><label style="font-size: 40px;" id="label-external-people-count">0</label></td>
                        </tr>
                      </table>
                    
                    </div>
                    <div class="wrimagecard-topimage_title">
                      <center>
                        <h5>Externos</h5>
                      </center>
                    </div>
                    </a>
                  </div>
                </div>

                <div class="col-md-2">
                  <div class="wrimagecard wrimagecard-topimage">
                    <a href="#">
                    <div class="wrimagecard-topimage_header" style="background-color: rgba(119, 178, 88, 0.1)">

                      <table style="width: 100%;">
                        <tr>
                          <td align="center" width="40%"><i class="fa fa-car fa-3x"></i></td>
                          <td align="center" width="60%"><label style="font-size: 40px;" id="label-external-vehicles-count">0</label></td>
                        </tr>
                      </table>

                    </div>
                    <div class="wrimagecard-topimage_title">
                      <center>
                        <h5>Externos</h5>
                      </center>
                    </div>
                    </a>
                  </div>
                </div>

                <div class="col-md-2">
                  <div class="wrimagecard wrimagecard-topimage">
                    <a href="#">
                    <div class="wrimagecard-topimage_header" style="background-color: red;">
                      <table style="width: 100%;">
                        <tr>
                          <td align="center" width="40%"><font color="white"><i class="fa fa-users fa-3x"></i></font></td>
                          <td align="center" width="60%"><font color="white"><label style="font-size: 40px;" id="label-external-people-exc-count">0</label></font></td>
                        </tr>
                      </table>
                    </div>
                    <div class="wrimagecard-topimage_title">
                      <center>
                        <h5>Excedidos</h5>
                      </center>
                    </div>
                    </a>
                  </div>
                </div>

                <div class="col-md-2">
                  <div class="wrimagecard wrimagecard-topimage">
                    <a href="#">
                    <div class="wrimagecard-topimage_header" style="background-color: red;">
                      <table style="width: 100%;">
                        <tr>
                          <td align="center" width="40%"><font color="white"><i class="fa fa-car fa-3x"></i></font></td>
                          <td align="center" width="60%"><font color="white"><label style="font-size: 40px;" id="label-external-vehicles-exc-count">0</label></font></td>
                        </tr>
                      </table>
                    </div>
                    <div class="wrimagecard-topimage_title">
                      <center>
                        <h5>Excedidos</h5>
                      </center>
                    </div>
                    </a>
                  </div>
                </div>

                <div class="col-md-2">
                  <div class="wrimagecard wrimagecard-topimage">
                    <a href="#">
                    <div class="wrimagecard-topimage_header" style="background-color: rgba(119, 178, 88, 0.1)">
                      
                      
                      <table style="width: 100%;">
                        <tr>
                          <td align="center" width="40%"><i class="fa fa-users fa-3x"></i></td>
                          <td align="center" width="60%"><label style="font-size: 40px;" id="label-internal-people-count">0</label></td>
                        </tr>
                      </table>
                    
                    </div>
                    <div class="wrimagecard-topimage_title">
                      <center>
                        <h5>Internos</h5>
                      </center>
                    </div>
                    </a>
                  </div>
                </div>

                <div class="col-md-2">
                  <div class="wrimagecard wrimagecard-topimage">
                    <a href="#">
                    <div class="wrimagecard-topimage_header" style="background-color: rgba(119, 178, 88, 0.1)">

                      <table style="width: 100%;">
                        <tr>
                          <td align="center" width="40%"><i class="fa fa-car fa-3x"></i></td>
                          <td align="center" width="60%"><label style="font-size: 40px;" id="label-internal-vehicles-count">0</label></td>
                        </tr>
                      </table>

                    </div>
                    <div class="wrimagecard-topimage_title">
                      <center>
                        <h5>Internos</h5>
                      </center>
                    </div>
                    </a>
                  </div>
                </div>


              </div>
            </div>

            <div class="row">
              <div class="col-md-12">

                <div class="col-md-12">
                  <h3>Notificaciones:</h3>
                      <table id="table-notifications" class="table table-bordered table-striped">
                          <thead>
                            <tr>
                              <th>N</th>
                              <th>Notificación</th>
                              <th>Puerta</th>
                              <th>Movimiento</th>
                              <th>Fecha</th>
                              <!-- <th>Encargado</th>-->
                            </tr>
                          </thead>
                          <tbody id="body-notifications">
                            <?php
                            $i = 1;
                            if(!empty($notifications))
                            {
                              foreach($notifications as $n)
                              {
                                $entry = $n['entry'];
                                if($entry == 0)
                                  $entry = 'ENTRADA';
                                else
                                  $entry = 'SALIDA';

                                echo '<tr>';
                                echo '<td>'.$i.'</td>';
                                echo '<td>'.$n['notification'].'</td>';
                                echo '<td>'.$n['door'].'</td>';
                                echo '<td>'.$entry.'</td>';
                                echo '<td>'.$n['created'].'</td>'; 
                                echo '</tr>';
                                $i++;
                              }
                            }
                            ?>
                          </tbody>
                      </table>
                
              </div>
              </div>
            </div>


          </div>


        </div>
      </div>
    </div>
  </section>
  
</div>
</div>


<script src="<?php echo base_url();?>estadoActual/public/assets/js/jquery.min.js"></script>
<script src="<?php echo base_url();?>estadoActual/public/assets/js/bootstrap.min.js"></script>

<script type="text/javascript">

  var cont_external_people = 0;
  var cont_external_vehicles = 0;
  var cont_people_exc = 0;
  var cont_vehicles_exc = 0;
  var cont_internal_people = 0;
  var cont_internal_vehicles = 0;

  $(document).ready(function(){
    setInterval(getAllCounts, 500);
  });  

  function getAllCounts()
  {
    $.ajax({
      url: '<?php echo site_url("CAccess/getAllCounts");?>',
      dataType: 'json',
      success: function(data)
      {
        $('#label-external-people-count').html(data.cont_external_people);
        $('#label-external-vehicles-count').html(data.cont_external_vehicles);
        
        $('#label-external-people-exc-count').html(data.cont_people_exc);
        $('#label-external-vehicles-exc-count').html(data.cont_vehicles_exc);
        
        $('#label-internal-people-count').html(data.cont_internal_people);
        $('#label-internal-vehicles-count').html(data.cont_internal_vehicles);

        $('#h2-general-people-count').html(data.sum_people);
        $('#h2-general-vehicles-count').html(data.sum_vehicles);
      }
    });
  }

</script>

</body>
</html>