<style type="text/css">
  @import url('https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');

  main {
    min-width: 320px;
    max-width: 1100px;
    padding: 20px;
    margin: 0 auto;
    background: #fff;
  }

  .tab-pane {
    display: none;
    padding: 20px 0 0;
    border-top: 1px solid #ddd;
  }

  input {
    display: none;
  }

  label {
    display: inline-block;
    margin: 0 0 -1px;
    padding: 15px 25px;
    font-weight: 600;
    text-align: center;
    //color: #bbb;
    border: 1px solid transparent;
  }

  label:before {
    font-family: fontawesome;
    font-weight: normal;
    margin-right: 10px;
  }

  label:hover {
    color: #888;
    cursor: pointer;
  }

  input:checked + label {
    color: #555;
    border: 1px solid #ddd;
    border-top: 2px solid orange;
    border-bottom: 1px solid #fff;
  }

  #tab1:checked ~ #content1,
  #tab2:checked ~ #content2,
  #tab3:checked ~ #content3,
  #tab4:checked ~ #content4,
  #tab5:checked ~ #content5,
  #tab6:checked ~ #content6 {
    display: block;
  }
</style>

<div class="content-wrapper">
  <section class="content">
    <div class="row">
      <div class="col-sm-12">
        <div class="box box-success">
          <div class="box-header ui-sortable-handle">
              <i class="fa fa-male"></i>
            <h3 class="box-title">Accesos Pendientes Peatones</h3>
            </div>

            <div class="box-body">
              <section class="content">
                  <table id="table-access_people_pending" class="table table-striped table-bordered table-condensed" style="width:100%;">
                      <thead>
                        <tr>
                          <th>ID</th>
                          <th>Rut</th>
                          <th>Nombre</th>
                          <th>Perfil</th>
                          <th>Portería</th>
                          <th>Empresa</th>
                          <th>Estado</th>
                          <th>Fecha</th>
                          <th>Acciones</th>
                        </tr>
                      </thead>
                      <tbody id="body-access_people_pending"></tbody>
                  </table>
              </section>
            </div>
            <div class="box-footer"></div>

        </div>
      </div>
    </div>

    <!--
    <div class="row">
      <div class="col-sm-12">
        <div class="box box-success">
          <div class="box-header ui-sortable-handle">
              <i class="fa fa-car"></i>
            <h3 class="box-title">Accesos Pendientes Vehículos</h3>
            </div>

            <div class="box-body">
              <section class="content">
                  <table id="table-access_vehicles_pending" class="table table-striped table-bordered table-condensed" style="width:100%;">
                      <thead>
                        <tr>
                          <th>ID</th>
                          <th>Rut</th>
                          <th>Nombre</th>
                          <th>Perfil</th>
                          <th>Portería</th>
                          <th>Empresa</th>
                          <th>Estado</th>
                          <th>Fecha</th>
                          <th>Acciones</th>
                        </tr>
                      </thead>
                      <tbody id="body-access_vehicles_pending"></tbody>
                  </table>
              </section>
            </div>
            <div class="box-footer"></div>

        </div>
      </div>
    </div>
    -->

  </section>
  
</div>

<!-- MODALS -->
<div  id="modal-access_people" class="modal fade" role="dialog">
  <div class="modal-dialog modal-lg">
      <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title"></h4>
          </div>
          <div class="modal-body">
            <main>
            
            <input id="tab1" type="radio" name="tabs" checked>
            <label for="tab1"><i class="fa fa-list-alt" aria-hidden="true"></i>&nbsp;Datos Acceso</label>

            <input id="tab2" type="radio" name="tabs">
            <label for="tab2"><i class="fa fa-user" aria-hidden="true"></i>&nbsp;Persona</label>
              
            <input id="tab3" type="radio" name="tabs">
            <label for="tab3"><i class="fa fa-list-alt" aria-hidden="true"></i>&nbsp;Motivos</label>
              
            <input id="tab4" type="radio" name="tabs">
            <label for="tab4"><i class="fa fa-map-marker" aria-hidden="true"></i>&nbsp;Ubicaciones</label>

            <input id="tab5" type="radio" name="tabs">
            <label for="tab5"><i class="fa fa-file-text" aria-hidden="true"></i>&nbsp;Formulario</label>

            <input id="tab6" type="radio" name="tabs">
            <label for="tab6"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>&nbsp;Acción</label>
            
              
            <div id="content1" class="tab-pane">
              <fieldset>
                <legend>Datos Acceso</legend>

                <table style="width: 100%;">
                  <tr>
                    <td><label>Portería</label></td>
                    <td id="td-main_access"></td>    
                  </tr>

                  <tr>
                    <td><label>Creado</label></td>
                    <td id="td-created"></td>    
                  </tr>

                  <tr>
                    <td><label>Vehículo</label></td>
                    <td id="td-vehicle"></td>    
                  </tr>

                  <tr>
                    <td width="20%"><label>Control Ingreso</label></td>
                    <td id="td-control_init"></td>    
                  </tr>

                  <tr>
                    <td><label>Control Salida</label></td>
                    <td id="td-control_end"></td>    
                  </tr>

                  <tr>
                    <td><label>Movimiento</label></td>
                    <td id="td-entry"></td>    
                  </tr>

                  <tr>
                    <td><label>Horas</label></td>
                    <td id="td-hours"></td>    
                  </tr>
                
                  <tr>
                    <td><label>Hora Salida</label></td>
                    <td id="td-end_time"></td>    
                  </tr>

                  <tr>
                    <td><label>Estado</label></td>
                    <td id="td-access_state"></td>    
                  </tr>

                  <tr>
                    <td><label>Observación</label></td>
                    <td>
                      <textarea readonly class="form-control textarea" rows="6" id="area-observation"></textarea>
                    </td>    
                  </tr>

                </table>

              </fieldset>
            </div>
              
            <div id="content2" class="tab-pane">
              <fieldset>
                <legend>Datos Persona</legend>
                  <table style="width: 100%;">

                    <tr>
                      <td><label>Rut</label></td>
                      <td id="td-rut">Cargando...</td>
                    </tr>

                    <tr>
                      <td><label>Nombre</label></td>
                      <td id="td-name">Cargando...</td>
                    </tr>

                    <tr>
                      <td><label>Teléfono</label></td>
                      <td id="td-phone">Cargando...</td>
                    </tr>

                    <tr>
                      <td><label>Email</label></td>
                      <td id="td-email">Cargando...</td>
                    </tr>

                    <tr>
                      <td><label>Perfil</label></td>
                      <td id="td-profile">Cargando...</td>
                    </tr>

                    <tr>
                      <td><label>Empresa</label></td>
                      <td id="td-company">Cargando...</td>
                    </tr>

                  </table>
              </fieldset>
            </div>
              
            <div id="content3" class="tab-pane">
              <fieldset>
                <legend>Motivos de Visita</legend>
                
                <table style="width: 100%;" id="table-reasons_visit">
                  
                  <tr>
                    <td><label>Motivo</label></td>    
                    <td>Cargando...</td>
                  </tr>
                
                </table>
              
              </fieldset>
              <br>
              <fieldset>
                <legend>Personas a Visitar</legend>

                <table style="width: 100%;" id="table-people_visit">

                  <tr>
                    <td><label>Persona</label></td>    
                    <td>Cargando...</td>
                  </tr>

                </table>

              </fieldset>
            </div>
            
            <div id="content4" class="tab-pane">

              <div class="row">
                <div class="col-md-12">

                  <div class="col-md-4">
                    <fieldset>
                    <legend>Zonas</legend>
                    <table style="width: 100%;" id="table-zones">
                      
                    </table>
                    </fieldset>
                  </div>

                  <div class="col-md-4">
                    <fieldset>
                    <legend>Áreas</legend>
                    <table style="width: 100%;" id="table-areas">
                      
                    </table>
                    </fieldset>
                  </div>

                  <div class="col-md-4">
                    <fieldset>
                    <legend>Deptos.</legend>
                     <table style="width: 100%;" id="table-department">
                      
                    </table>
                    </fieldset>
                  </div>

                </div>
              </div>
              <br>
              
              <fieldset>
              <legend>Ruta de Puertas</legend>
                <br>
                <div id="div-route" style="width: 100%;"></div>
              </fieldset>
            </div>

            <div id="content5" class="tab-pane">

              <div class="row">
                <div class="col-md-12">
                  <fieldset>
                    <legend>Formulario de control entrada</legend>
                    <table style="width: 100%;" id="table-form-in">
                        <tbody id="form">
                            <tr>
                              <td width="20%"><label>Título</label></td>
                              <td id="formtitle"></td>
                            </tr>
                            <tr>
                              <td><label>Observación</label></td>
                              <td id="formobservation"></td>
                            </tr>
                        </tbody>                        
                    </table>
                  </fieldset>
                </div>
              </div>
            </div>

            <div id="content6" class="tab-pane">
              <table style="width: 100%;">

                <tr>
                  <td width="35%"><label>Observación</label></td>
                  <td width="65%" colspan="2">
                    <textarea class="textarea" id="area-add-observation" rows="5" style="width: 100%;"></textarea>
                  </td>
                </tr>

                <tr>
                  <td></td>
                  <td colspan="2"><button class="btn btn-success btn-xs" role="button" onclick="allowSuccess();"><i class="fa fa-check"></i> Permitir</button>&nbsp;<button class="btn btn-danger btn-xs" role="button" onclick="allowFailed();"><i class="fa fa-minus-circle"></i> Rechazar</button></td>
                </tr>

              </table>
            </div>
            
          </main>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          </div>
      </div>
  </div>
</div>
<!-- FIN MODALS -->

<?php $this->view('footer'); ?>

<script>
  var node_server = '<?php echo NODE_HOST;?>';
  var node_port = '<?php echo NODE_PORT;?>';
  var node_route = 'parametros';

  var id = '';
  $(document).ready(function() {

    $('#li-visits').addClass('menu-open');
    $('#ul-visits').css('display', 'block');
      
    $('#li-access_people_vehicles').addClass('menu-open');
    $('#ul-access_people_vehicles').css('display', 'block');

    getPending_people();
    //getPending_vehicles();

  });

  function getPending_people()
  {
    var html = '';

    $.ajax({
      url: site_url + '/cAccess/getPending/',
      dataType: 'json',
      success: function(data)
      {
        if(data.length > 0 && data != null)
        {
          for(var i=0;i<data.length;i++)
          {
            html += '<tr>';
            html += '<td>'+data[i].id+'</td>';
            html += '<td>'+data[i].rut+'-'+data[i].digit+'</td>';
            html += '<td>'+data[i].name+' '+data[i].last_name+'</td>';
            html += '<td>'+data[i].profile+'</td>';
            html += '<td>'+data[i].access+'</td>';
            html += '<td>'+data[i].company+'</td>';
            html += '<td><label class="label label-warning">'+data[i].state+'</label></td>';
            html += '<td>'+data[i].created+'</td>';
            html += '<td><button class="btn btn-primary btn-xs" role="button" onclick="showAccessPeople('+data[i].id+');"><i class="fa fa-search"></i> Ver</button>&nbsp;<button class="btn btn-success btn-xs" role="button" onclick="allowSuccessShortcut('+data[i].id+');"><i class="fa fa-check"></i> Permitir</button>&nbsp;<button class="btn btn-danger btn-xs" role="button" onclick="allowFailedShortcut('+data[i].id+');"><i class="fa fa-minus-circle"></i> Rechazar</button></td>';
            html += '</tr>';
          }

          $('#body-access_people_pending').html(html);
          
        }

        $('#table-access_people_pending').DataTable({
              deferRender: true,
              aoColumnDefs: [
                  { bSortable: false, aTargets: [8] }
                 ],
              language: {
                   url: '<?php echo base_url();?>assets/Spanish.json'
              }
          });
      }
    });
  }

  function getPending_vehicles()
  {
    $('#table-access_vehicles_pending').DataTable({
          deferRender: true,
          aoColumnDefs: [
              { bSortable: false, aTargets: [8] }
             ],
          language: {
               url: '<?php echo base_url();?>assets/Spanish.json'
          }
      });
  }

  function showAccessPeople(access_people)
  {
    id = access_people;

    $.ajax({
      url: site_url + '/cAccess/getAccessPeople/',
      data: {access_people_id: access_people},
      type: 'post',
      dataType: 'json',
      success: function(data)
      {
        if(data != null)
        {
          //GENERAL & PEOPLE
          if(data.access_people.length > 0 && data.access_people != null)
          {
            $('#td-rut').html(data.access_people[0].rut+'-'+data.access_people[0].digit);
            $('#td-name').html(data.access_people[0].name+' '+data.access_people[0].last_name);
            $('#td-phone').html(data.access_people[0].phone);
            $('#td-profile').html(data.access_people[0].profile);
            $('#td-email').html(data.access_people[0].email);
            $('#td-company').html(data.access_people[0].company);

            $('#td-main_access').html(data.access_people[0].access);
            $('#td-created').html(data.access_people[0].created);
            if (!data.access_people[0].patent )
              $('#td-vehicle').html('N/A');
            else
              $('#td-vehicle').html(data.access_people[0].patent+' - '+data.access_people[0].model);
            
            if (data.access_people[0].control_init == 0)
              $('#td-control_init').html('No');
            else
              $('#td-control_init').html('Sí');
            
            if (data.access_people[0].control_end == 0)
              $('#td-control_end').html('No');
            else
              $('#td-control_end').html('Sí');
            
            if(data.access_people[0].entry == 0)
              $('#td-entry').html('Ingreso');
            else
              $('#td-entry').html('Salida');
            $('#td-hours').html(data.access_people[0].hours);
            $('#td-end_time').html(data.access_people[0].end_time);
            $('#td-access_state').html('<label class="label label-warning">'+data.access_people[0].state+'</label>');
            $('#area-observation').html(data.access_people[0].observation);
          }
          //REASONS
          if(data.access_people_reasons.length > 0 && data.access_people_reasons != null)
          {
            var html = '';
            for(var i=0;i<data.access_people_reasons.length;i++)
            {
              html += '<tr>';
              html += '<td><label>Motivo '+(i+1)+'</label></td>';
              html += '<td>'+data.access_people_reasons[i].reason+'</td>';
              html += '</tr>';
            }

            $('#table-reasons_visit').html(html);
          }
          //PEOPLE TO VISIT
          if(data.access_people_visit.length > 0 && data.access_people_visit != null)
          {
            var html = '';
            for(var i=0;i<data.access_people_visit.length;i++)
            {
              html += '<tr>';
              html += '<td><label>Persona '+(i+1)+'</label></td>';
              html += '<td>'+data.access_people_visit[i].rut+'-'+data.access_people_visit[i].digit+' | '+data.access_people_visit[i].name+' '+data.access_people_visit[i].last_name+'</td>';
              html += '</tr>';
            }

            $('#table-people_visit').html(html);
          }
          //ZONES
          if(data.access_people_zones.length > 0 && data.access_people_zones != null)
          {
            var html = '';
            for(var i=0;i<data.access_people_zones.length;i++)
            {
              html += '<tr>';
              html += '<td><label>Zona '+(i+1)+'</label></td>';
              html += '<td>'+data.access_people_zones[i].zone+'</td>';
              html += '</tr>';
            }

            $('#table-zones').html(html);
          }
          //AREAS
          if(data.access_people_areas.length > 0 && data.access_people_areas != null)
          {
            var html = '';
            for(var i=0;i<data.access_people_areas.length;i++)
            {
              html += '<tr>';
              html += '<td><label>Área '+(i+1)+'</label></td>';
              html += '<td>'+data.access_people_areas[i].area+'</td>';
              html += '</tr>';
            }

            $('#table-areas').html(html);
          }
          //DEPARTMENT
          if(data.access_people_department.length > 0 && data.access_people_department != null)
          {
            var html = '';
            for(var i=0;i<data.access_people_department.length;i++)
            {
              html += '<tr>';
              html += '<td><label>Departamento '+(i+1)+'</label></td>';
              html += '<td>'+data.access_people_department[i].department+'</td>';
              html += '</tr>';
            }

            $('#table-department').html(html);
          }
          //ROUTE
          if(data.access_people_route.length > 0 && data.access_people_route != null)
          {

            var html = '';

            for(var i=0;i<data.access_people_route.length;i++)
            {
              if(i % 3 == 0)
              {
                html += '<div class="row"><div class="col-md-12">';

                html += '<div class="col-md-2">';
                html +='<div class="card" style="width: 18rem;">';
                html +='<img class="card-img-top" src="<?php echo base_url();?>/assets/img/door.png" alt="Card image cap">';
                html +='<div class="card-body">';
                html +='<h5 class="card-title">'+data.access_people_route[i].door+'</h5>';
                html +='<p class="card-text">'+data.access_people_route[i].description+'</p>';
                html +='</div>';
                html +='</div>';
                html += '</div>';

                if(i < data.access_people_route.length-1)
                {
                  html += '<div class="col-md-2"><i class="fa fa-arrow-right fa-2x" aria-hidden="true"></i></div>';
                }
              }
              else if((i + 1) % 3 == 0 )
              {
                html += '<div class="col-md-2">';
                html +='<div class="card" style="width: 18rem;">';
                html +='<img class="card-img-top" src="<?php echo base_url();?>/assets/img/door.png" alt="Card image cap">';
                html +='<div class="card-body">';
                html +='<h5 class="card-title">'+data.access_people_route[i].door+'</h5>';
                html +='<p class="card-text">'+data.access_people_route[i].description+'</p>';
                html +='</div>';
                html +='</div>';
                html += '</div>';

                if(i < data.access_people_route.length-1)
                {
                  html += '<div class="col-md-2"><i class="fa fa-arrow-right fa-2x" aria-hidden="true"></i></div>';
                }

                html += '</div></div><br><br>';
              }
              else
              {
                html += '<div class="col-md-2">';
                html +='<div class="card" style="width: 18rem;">';
                html +='<img class="card-img-top" src="<?php echo base_url();?>/assets/img/door.png" alt="Card image cap">';
                html +='<div class="card-body">';
                html +='<h5 class="card-title">'+data.access_people_route[i].door+'</h5>';
                html +='<p class="card-text">'+data.access_people_route[i].description+'</p>';
                html +='</div>';
                html +='</div>';
                html += '</div>';

                if(i < data.access_people_route.length-1)
                {
                  html += '<div class="col-md-2"><i class="fa fa-arrow-right fa-2x" aria-hidden="true"></i></div>';
                }
              }
            }

            $('#div-route').html(html);
          }
          //Form
          if (data.access_people_form_in.length > 0 && data.access_people_form_in != null) {
            $("#formtitle").text(data.access_people_form_in[0].title);
            $("#formobservation").text(data.access_people_form_in[0].observation);
          }
          if (data.access_people_form_inDetail.length > 0 && data.access_people_form_inDetail != null) {
            for (var i = 0; i < data.access_people_form_inDetail.length; i++) {
              var html = '';
              html += '<tr>';
              html += '<td><label>';
              html += data.access_people_form_inDetail[i].question;
              html += '</label></td>';
              if (data.access_people_form_inDetail[i].answers_type_id == 3) {
                html += '<td id="quest'+i+'"> '+data.access_people_form_inDetail[i].measure;+'</td>';
              }
              else {
                html += '<td id="quest'+i+'"></td>';
              }
              html += '</tr>';
              $("#form").append(html);
            }
          }
          if (data.getDetail_Access_People_Form_in_answers.length > 0 && data.getDetail_Access_People_Form_in_answers != null) {
            for (var i = 0; i < data.getDetail_Access_People_Form_in_answers.length; i++) {
              var q = 'quest'+data.getDetail_Access_People_Form_in_answers[i].order;
              if (q == ('quest'+(i+1))) {
                $("#quest"+i).text(data.getDetail_Access_People_Form_in_answers[i].answer);
              }
            }
          }
        }
      }
    });

    $('#tab1').prop('checked',true);
    $('#tab2').prop('checked',false);
    $('#tab3').prop('checked',false);
    $('#tab4').prop('checked',false);
    $('#tab5').prop('checked',false);

    $('.modal-title').html('Registro Acceso Peatonal # '+access_people);
    $('#modal-access_people').modal('show');
  }

  function allowSuccess()
  {
    var c = confirm('Confirme Permitir este registro de acceso.');
    if(c)
    {
      var observation = $('#area-add-observation').val();
      //access_state_id: 2
      //id state "Permitido"

      $.ajax({
        url: site_url + '/cAccess/changeStateAccessPeople',
        type: 'post',
        data: {access_people_id: id, access_state_id: 2, observation: observation},
        dataType: 'json',
        success: function(data)
        {
          if(data.response == 1)
          {
            $.ajax({
              url: 'http://'+node_server+':'+node_port+'/'+node_route,
              type: 'get',
              data: {add_id: id, add_rut: data.people[0].rut+'-'+data.people[0].digit, add_name: data.people[0].name+' '+data.people[0].last_name, add_profile: data.people[0].profile, add_access_type: 'PEATONAL', add_company: data.people[0].company, add_date_time_entrance: data.time_entrance, add_date_time_end: data.end_time, operation: 1},
              dataType: 'text',
              success: function(data)
              {
                location.reload();
              }
            });
            
          }
          else
          {
            alert('Ha ocurrido un problema al generar esta operación.\nVerifique no haya sido cambiado de estado este registro.');
          }
        }
      });
    }
  } 

  function allowFailed()
  {
    var c = confirm('Confirme Rechazar este registro de acceso.');
    if(c)
    {
      var observation = $('#area-add-observation').val();
      //access_state_id: 3
      //id state "Rechazado"

      $.ajax({
        url: site_url + '/cAccess/changeStateAccessPeople',
        type: 'post',
        data: {access_people_id: id, access_state_id: 3, observation: observation},
        dataType: 'json',
        success: function(data)
        {
          if(data.response == 1)
          {
            location.reload();
          }
          else
          {
            alert('Ha ocurrido un problema al generar esta operación.\nVerifique no haya sido cambiado de estado este registro.');
          }
        }
      });
    }    
  }

  function allowSuccessShortcut(access_people_id)
  {
    var c = confirm('Confirme Permitir este registro de acceso.');
    if(c)
    {
      var observation = $('#area-add-observation').val();
      //access_state_id: 2
      //id state "Permitido"

      $.ajax({
        url: site_url + '/cAccess/changeStateAccessPeople',
        type: 'post',
        data: {access_people_id: access_people_id, access_state_id: 2},
        dataType: 'json',
        success: function(data)
        {
          if(data.response == 1)
          {
            $.ajax({
              url: 'http://'+node_server+':'+node_port+'/'+node_route,
              type: 'get',
              data: {add_id: access_people_id, add_rut: data.people[0].rut+'-'+data.people[0].digit, add_name: data.people[0].name+' '+data.people[0].last_name, add_profile: data.people[0].profile, add_access_type: 'PEATONAL', add_company: data.people[0].company, add_date_time_entrance: data.time_entrance, add_date_time_end: data.end_time, operation: 1},
              dataType: 'text',
              success: function(data)
              {
                location.reload();
              }
            });
          }
          else
          {
            alert('Ha ocurrido un problema al generar esta operación.\nVerifique no haya sido cambiado de estado este registro.');
          }
        }
      });
    }
  } 

  function allowFailedShortcut(access_people_id)
  {
    var c = confirm('Confirme Rechazar este registro de acceso.');
    if(c)
    {
      var observation = $('#area-add-observation').val();
      //access_state_id: 3
      //id state "Rechazado"

      $.ajax({
        url: site_url + '/cAccess/changeStateAccessPeople',
        type: 'post',
        data: {access_people_id: access_people_id, access_state_id: 3},
        dataType: 'json',
        success: function(data)
        {
          if(data.response == 1)
          {
            location.reload();
          }
          else
          {
            alert('Ha ocurrido un problema al generar esta operación.\nVerifique no haya sido cambiado de estado este registro.');
          }
        }
      });
    }    
  }




</script>
</body>
</html>