<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-success">
					<div class="box-header ui-sortable-handle">
					     <i class="fa fa-file-text"></i>
						<h3 class="box-title">Bitácora</h3>
				  	</div>

				  	<div class="box-body">
			  			<section class="content">
			  			    <table class="table">
		  		    		    <tbody>
		  		    		    	<tr>
		  		    		    		<th>Id</th>
		  			    				<td><?php if(!empty($logbook[0]['id'])) echo $logbook[0]['id'];?></td>
		  		    		    	</tr>
		  		    		    	<tr>
		  		    		    		<th>Título</th>
		  			    				<td><?php if(!empty($logbook[0]['title'])) echo $logbook[0]['title'];?></td>
		  		    		    	</tr>
		  		    		    	<tr>
		  		    	    			<th>Cuerpo</th>
		  		    	    			<td><?php if(!empty($logbook[0]['body'])) echo $logbook[0]['body'];?></td>
		  		    	    		</tr>
		  		    	    		<tr>
		  		    	    			<th>Persona</th>
		  		    	    			<td><?php echo $logbook[0]['rut'].'-'.$logbook[0]['digit'].' | '.$logbook[0]['name'].' '.$logbook[0]['last_name'];?></td>
		  		    	    		</tr>
		  		    	    		<tr>
		  		    		    		<th>Tipo</th>
		  			    				<td><?php if(!empty($logbook[0]['type'])) echo $logbook[0]['type'];?></td>
		  		    		    	</tr>
		  		    		    	<tr>
		  		    		    		<th>Creado</th>
		  			    				<td><?php if(!empty($logbook[0]['created'])) echo $logbook[0]['created'];?></td>
		  		    		    	</tr>
		  		    		    	<tr>
		  		    		    		<th>Modificado</th>
		  			    				<td><?php if(!empty($logbook[0]['modified'])) echo $logbook[0]['modified'];?></td>
		  		    		    	</tr>
		  		    		    </tbody>
		  			    	</table>
		  			    	<hr>
		  			    	<table class="table">
		  			    		<tr>
		  			    			<th>Documentos</th>
		  			    		</tr>
		  			    		<?php
	  			    				if(!empty($documents))
	  			    				{
	  			    					foreach($documents as $d)
	  			    					{
	  			    						$document = explode('/', $d);
	  			    						$document = $document[count($document)-1];
	  			    						echo '<tr>';
	  			    						echo '<td><a href="'.$d.'" download>'.$document.'</a></td>';
	  			    						echo '</tr>';
	  			    					}
	  			    				}
  			    				?>
		  			    	</table>
			  			</section>
				  	</div>
				  	<div class="box-footer">
				  		<a href="<?php echo site_url(); ?>/CLogbook/index" class="btn btn-primary pull-right" role="button">
	                    <i class='fa fa-undo'></i> Volver
	                </a>
				  	</div>
				</div>
			</div>
		</div>
	</section>
	
</div>

<?php $this->view('footer'); ?>

<script>
    $(document).ready(function() {

      	$('#li-logbook').addClass('menu-open');
      	$('#ul-logbook').css('display', 'block');

    });
</script>
</body>
</html>