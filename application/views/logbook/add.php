<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-success">
					<div class="box-header ui-sortable-handle">
					    <i class="fa fa-file-text"></i>
						<h3 class="box-title">Agregar Bitácora</h3>
				  	</div>

				  	<form class="form-horizontal" id="add-logbook">
			  			<div class="box-body">
			  				<div class="box-body">

				  				<div class="form-group">
				  					<label for="input-title" class="col-sm-2 control-label">Título</label>
				  					<div class="col-sm-10">
				  						<input type="text" class="form-control" name="input-title" id="input-title" required value="">
				  					</div>
				  				</div>

				  				<div class="form-group">
				  					<label for="area-body" class="col-sm-2 control-label">Cuerpo</label>
				  					<div class="col-sm-10">
				  						<textarea rows="15" class="form-control area" name="area-body" id="area-body"></textarea>
				  					</div>
				  				</div>

				  				<div class="form-group">
				  					<label for="select-type" class="col-sm-2 control-label">Tipo</label>
				  					<div class="col-sm-10">
				  						<select required id="select-type" name="select-type" class="form-control">
				  							<?php
				  								$options = '';

				  								$this->db->select('id, type');
				  								$this->db->from('type_logbook');
				  								$this->db->order_by('type', 'asc');
				  								$res = $this->db->get()->result_array();

				  								echo '<option value="">Seleccione una opción</option>';
				  								if(!empty($res))
				  								{
				  									foreach($res as $r)
				  									{
				  										echo '<option value="'.$r['id'].'">'.$r['type'].'</option>';
				  									}
				  								}
				  							?>
				  						</select>
				  					</div>
				  				</div>

				  				<div class="form-group">
				  					<label for="area-body" class="col-sm-2 control-label">Adjuntos</label>
				  					<div class="col-sm-10">
				  						<div class="box-header">
	                                        <button type="button" class="btn btn-xs btn-success" onclick="addFormUpload()"><i class="fa fa-plus"></i> Subir archivos</button>
	                                    </div>
	                                    <div class="box-body" id="buploads">
	                                        
	                                    </div>
	                                    <div class="box-footer" id="fuploads">
	                                        
	                                    </div>
				  					</div>
				  				</div>

				  			</div>
			  			</div>
			  			<div class="box-footer">
			  				<button type="submit" class="btn btn-primary pull-right">Guardar</button>
			  			</div>
			  		</form>
				</div>
			</div>
		</div>
	</section>
	
</div>


<?php $this->view('footer'); ?>

<script>
	var up = 0;
	$(document).ready(function() {
		
		$('.area').wysihtml5();

		$("#add-logbook").submit(function(event) {
			event.preventDefault();

			var archivoup = 0;
            if (up == 1) 
            {
                var file_upload = document.getElementsByClassName('files-uploads');
                var elementos = new FormData();
                for(var j = 0; j < file_upload.length; j++)
                {
                    var archivo = file_upload[j].files;
                    for(var i = 0; i < archivo.length; i++)
                    {
                        elementos.append('archivo'+archivoup, archivo[i]);
                        archivoup++;
                    }
                }
            }

            $.ajax({
            	url: site_url+'/CLogbook/addLogbook',
            	type: 'post',
            	data: {title: $('#input-title').val(), body: $('#area-body').val(), type: $('#select-type').val()},
            	dataType: 'text',
            	success: function(data)
            	{
            		var id_logbook = parseInt(data);
            		if(id_logbook > 0)
            		{
            			if(up == 1)
            			{
            				$.ajax({
				            	url: site_url + '/CLogbook/addDocuments?id_logbook='+id_logbook,
				            	type: 'post',
				            	contentType: false,
				                data: elementos,
				                processData: false,
				                cache: false,
				            	success: function(res)
				            	{
				            		res = parseInt(res);
				            		if(res == 1)
				            			window.location.replace(site_url+"/CLogbook/");
				            		else
				            			alert('Error en el proceso');
				            	}
				            });
            			}
            			else
            			{
            				window.location.replace(site_url+"/CLogbook/");
            			}
            		}
            		else
            			alert('Error en el proceso');
            	}
            });

            
			
		});

		$('#li-logbook').addClass('menu-open');
      	$('#ul-logbook').css('display', 'block');
	});


	var contadorUp = 0;
    function addFormUpload() {
        var html = '<form enctype="multipart/form-data" id="form-upload'+contadorUp+'" method="post">';
        html += '<input class="files-uploads form-control" type="file" multiple="multiple" id="input-archivos'+contadorUp+'" name="archivo1"/><br><button class="btn btn-xs btn-danger" id="rem'+contadorUp+'" onclick="removeFormUpload('+contadorUp+')"><i class="fa fa-minus"></i> Remover</button><br><hr>';
       
        $('#buploads').append(html);
        // $("#fuploads").append('')
        up = 1;
        contadorUp++;
    }

    function removeFormUpload(id) {
        $("#form-upload"+id).remove();
        $("#rem"+id).remove();
        up = 0;
    }
	
</script>
</body>
</html>
