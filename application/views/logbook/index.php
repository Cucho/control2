<div class="content-wrapper">
	<section class="content">
    <div class="row">
      <div class="col-sm-12">
        <div class="box box-success">
          <div class="box-header ui-sortable-handle">
              <i class="fa fa-file-text"></i>
              <h3 class="box-title">Bitácoras</h3>
          </div>

          <div class="box-body">
            <section class="content">
                <a href="<?php echo site_url() ?>/CLogbook/add" class="btn btn-primary">Agregar</a><br><hr>
                <table id="table-logbook" class="table table-striped table-bordered table-condensed" style="width:100%;">
                    <thead>
                      <tr>
                        <th width="10%">ID</th>
                        <th>Título</th>
                        <th>Persona</th>
                        <th>Tipo</th>
                        <th>Creado</th>
                        <th>Acción</th>
                      </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </section>
          </div>
          <div class="box-footer"></div>
        </div>
      </div>
    </div>
  </section>
	
</div>

<?php $this->view('footer'); ?>

<script>
  var edit = <?php echo $this->session->userdata('edit'); ?>;
  var del = <?php echo $this->session->userdata('del'); ?>;

    $(document).ready(function() {
      
      $('#table-logbook').DataTable({
          "lengthMenu": [[5, 10, 15, 20,], [5, 10, 15, 20]],
          'responsive': true,
          'paging': true,
          'info': true,
          'filter': true,
          'ordering': true,
          // 'stateSave': true,
          'processing':true,
          'serverSide':true,
          'language': {
            "url": base_url + "assets/Spanish.json"
          },
          "order": [[0, "asc"]],
          'ajax': {
            "url": site_url + "/CLogbook/datatable",
            "type":"POST",
          },
          "columns": [
            { "data": "ID"},
            { "data": "Título" },
            { "data": "Persona" },
            { "data": "Tipo" },
            { "data": "Creado" },
            { "data": "Acción" }
          ],
          "columnDefs": [
            {
              "targets": [0],
              "orderable": true,
              "render": function(data, type, row) {
                return row.id
              }
            },
            {
              "targets": [1],
              "orderable": true,
              "render": function(data, type, row) {
                return row.title
              }
            },
            {
              "targets": [2],
              "orderable": true,
              "render": function(data, type, row) {
                return row.rut+'-'+row.digit+' | '+row.name+' '+row.last_name
              }
            },
            {
              "targets": [3],
              "orderable": true,
              "render": function(data, type, row) {
                return row.type
              }
            },
            {
              "targets": [4],
              "orderable": true,
              "render": function(data, type, row) {
                return row.created
              }
            },
            {
              "targets": [5],
              "orderable": false,
              "render": function(data, type, row) {
                if(row.edited == 1)
                {
                  return `
                  <a href="<?php echo site_url(); ?>/CLogbook/view?id=`+row.id+`" class="btn btn-primary btn-xs" role="button">
                    <i class='fa fa-search'></i> Ver
                  </a>
                  <a href="<?php echo site_url(); ?>/CLogbook/edit?id=`+row.id+`" class="btn btn-warning btn-xs" role="button">
                      <i class='fa fa-pencil-square-o'></i> Editar
                  </a>`;
                }
                else
                {
                  return `
                  <a href="<?php echo site_url(); ?>/CLogbook/view?id=`+row.id+`" class="btn btn-primary btn-xs" role="button">
                    <i class='fa fa-search'></i> Ver
                  </a>`;
                }
              }
            }
           ],
        });

      $('#li-logbook').addClass('menu-open');
      $('#ul-logbook').css('display', 'block');
      
    });

</script>

</body>
</html>
