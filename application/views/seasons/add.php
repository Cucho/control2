<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-success">
					<div class="box-header ui-sortable-handle">
					    <i class="fa fa-calendar"></i>
						<h3 class="box-title">Agregar temporada</h3>
				  	</div>

				  	<form class="form-horizontal" id="addSeason">
			  			<div class="box-body">
			  				<div class="form-group">
			  					<label for="season" class="col-sm-2 control-label">Temporada</label>
			  					<div class="col-sm-10">
			  						<input type="text" class="form-control" name="season" id="season" required>
			  					</div>
			  				</div>
			  			</div>
			  			<div class="box-footer">
			  				<button type="submit" class="btn btn-primary pull-right">Guardar</button>
			  			</div>
			  		</form>
				</div>
			</div>
		</div>
	</section>
	
</div>

<?php $this->view('footer'); ?>

<script>
	$(document).ready(function() {
		$("#addSeason").submit(function(event) {
			event.preventDefault();

			$.post(
				site_url + "/cSeasons/addSeason",{
					seasons : 	$("#season").val()
				},
				function(data){
					if (data == 1) {
						window.location.replace(site_url+"/cSeasons/");
					}
					else {
						alert("Error en el proceso...")
						window.location.replace(site_url+"/cSeasons/add");
					}
				}
			);
		});

		$('#li-form').addClass('menu-open');
		$('#ul-form').css('display', 'block');
	});
</script>
</body>
</html>
