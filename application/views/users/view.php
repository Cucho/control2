<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-success">
					<div class="box-header ui-sortable-handle">
					    <i class="fa fa-users"></i>
						<h3 class="box-title">Usuario</h3>
				  	</div>
					
					<div class="box-body">
			  			<section class="content">
			  			    <table id="sensors" class="table">
		  		    		    <tbody>
		  		    		    	<tr>
		  		    		    		<th>Id</th>
		  			    				<td><?php echo $users[0]['id'];?></td>
		  		    		    	</tr>
									<tr>
		  		    		    		<th>Usuario</th>
		  			    				<td><?php echo $users[0]['user'];?></td>
		  		    		    	</tr>
                          			<tr>
                          				<th>Rol</th>
                          				<td><?php echo $users[0]['rol'];?></td>
                          			</tr>
                          			<tr>
                          				<th>Persona</th>
                          				<td><?php echo $users[0]['name'];?></td>
                          			</tr>
                          			<tr>
                          				<th>Estado</th>
                          				<td><?php echo $users[0]['state'];?></td>
                          			</tr>

                          			<tr>
                          				<th>Permitido crear</th>
                          				<td>
                          					<?php if ($users[0]['save'] == 1) { echo 'Si'; } else { echo 'No'; }?>
                      					</td>
                          			</tr>
                          			<tr>
                          				<th>Permitido Editar</th>
                          				<td>
                          					<?php if ($users[0]['edit'] == 1) { echo 'Si'; } else { echo 'No'; }?>
                      					</td>
                          			</tr>
                          			<tr>
                          				<th>Permitido Eliminar</th>
                          				<td>
                          					<?php if ($users[0]['del'] == 1) { echo 'Si'; } else { echo 'No'; }?>
                      					</td>
                          			</tr>

                          			<tr>
		  		    	    			<th>Creado</th>
		  		    	    			<td><?php echo $users[0]['created'];?></td>
		  		    	    		</tr>
		  		    	    		<tr>
		  		    	    			<th>Modificado</th>
		  		    	    			<td><?php echo $users[0]['modified'];?></td>
		  		    	    		</tr>
		  		    		    </tbody>
		  			    	</table>
			  			</section>
				  	</div>
				  	<div class="box-footer">
				  		<a href="<?php echo site_url(); ?>/cUsers/" class="btn btn-primary pull-right" role="button">
	                    <i class='fa fa-undo'></i> Volver
	                </a>
				  	</div>
				</div>
			</div>
		</div>
	</section>
	
</div>

<?php $this->view('footer'); ?>

<script>
    $(document).ready(function() {

    	$('#li-configuration').addClass('menu-open');
      	$('#ul-configuration').css('display', 'block');
      	
      	$('#li-users').addClass('menu-open');
		$('#ul-users').css('display', 'block');
    });
</script>
</body>
</html>