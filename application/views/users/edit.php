<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-success">
					<div class="box-header ui-sortable-handle">
					    <i class="fa fa-users"></i>
						<h3 class="box-title">Editar usuario #<?php echo $users[0]['id']; ?></h3>
				  	</div>
				  	
				  	<form class="form-horizontal" id="editUser">
  			  			<div class="box-body">
  			  				<div class="form-group">
			  					<label for="user" class="col-sm-2 control-label">Usuario</label>
			  					<div class="col-sm-10">
			  						<input type="text" class="form-control" name="user" id="user" value="<?php echo $users[0]['user']; ?>" required>
			  					</div>
			  				</div>
			  				<div class="form-group">
			  					<label for="password" class="col-sm-2 control-label">Contraseña</label>
			  					<div class="col-sm-10">
			  						<input type="password" class="form-control" name="password" id="password" value="<?php echo $users[0]['password']; ?>" required>
			  					</div>
			  				</div>
			  				<div class="form-group">
			  					<label for="roles_id" class="col-sm-2 control-label">Rol</label>
			  					<div class="col-sm-5">
			  						<select name="roles_id" id="roles_id" class="form-control" required>
			  							<option value="">Seleccione una opción</option>
			  							<?php foreach ($roles as $key) { ?>
			  								<option value="<?php echo $key->id; ?>"><?php echo $key->rol; ?></option>
			  							<?php } ?>
			  						</select>
			  					</div>
			  				</div>
			  				<div class="form-group">
			  					<label for="people_id" class="col-sm-2 control-label">Persona</label>
			  					<div class="col-sm-5">
			  						<select name="people_id" id="people_id" class="form-control" required>
			  							<option value="">Seleccione una opción</option>
			  							<?php foreach ($people as $key) { ?>
			  								<option value="<?php echo $key->id; ?>"><?php echo $key->name." ".$key->last_name; ?></option>
			  							<?php } ?>
			  						</select>
			  					</div>
			  				</div>
			  				<div class="form-group">
			  					<label for="users_state_id" class="col-sm-2 control-label">Estado de usuario</label>
			  					<div class="col-sm-5">
			  						<select name="users_state_id" id="users_state_id" class="form-control" required>
			  							<option value="">Seleccione una opción</option>
			  							<?php foreach ($users_state as $key) { ?>
			  								<option value="<?php echo $key->id; ?>"><?php echo $key->state; ?></option>
			  							<?php } ?>
			  						</select>
			  					</div>
			  				</div>
			  				<!--  -->
			  				<div class="form-group">
			  					<label class="col-sm-2 control-label">
			  						Permitido
			  					</label>
			  					<div class="checkbox col-sm-5">
			  						<label>
			  							<input type="checkbox" id="users_save"> Crear
			  						</label>
			  					</div>
			  				</div>

			  				<div class="form-group">
			  					<label class="col-sm-2 control-label">
			  						Permitido
			  					</label>
			  					<div class="checkbox col-sm-5">
			  						<label>
			  							<input type="checkbox" id="users_edit"> Editar
			  						</label>
			  					</div>
			  				</div>

			  				<div class="form-group">
			  					<label class="col-sm-2 control-label">
			  						Permitido
			  					</label>
			  					<div class="checkbox col-sm-5">
			  						<label>
			  							<input type="checkbox" id="users_del"> Eliminar
			  						</label>
			  					</div>
			  				</div>
			  				<!--  -->
  			  			</div>
			  			<div class="box-footer">
			  				<button type="submit" class="btn btn-primary pull-right">Guardar</button>
			  			</div>
			  		</form>
				</div>
			</div>
		</div>
	</section>
	
</div>


<?php $this->view('footer'); ?>

<script>
	$(document).ready(function() {
		$("#roles_id").val(<?php echo $users[0]['roles_id']; ?>)
		$("#people_id").val(<?php echo $users[0]['people_id']; ?>)
		$("#users_state_id").val(<?php echo $users[0]['users_state_id']; ?>)

		if (<?php echo $users[0]['save']; ?> == 1) {
			$("#users_save").prop('checked', true);
		}
		if (<?php echo $users[0]['edit']; ?> == 1) {
			$("#users_edit").prop('checked', true);
		}
		if (<?php echo $users[0]['del']; ?> == 1) {
			$("#users_del").prop('checked', true);
		}

		$("#editUser").submit(function(event) {
			event.preventDefault();

			$.post(
				site_url + "/cUsers/editUser",{
					id 				: 	<?php echo $users[0]['id']; ?>,
					user 			: 	$("#user").val(),
					password 		: 	$("#password").val(),
					roles_id 		: 	$("#roles_id").val(),
					people_id 		: 	$("#people_id").val(),
					users_state_id	: 	$("#users_state_id").val(),
					users_save		: 	$("#users_save").prop('checked') == true ? '1' : '0',
					users_edit		: 	$("#users_edit").prop('checked') == true ? '1' : '0',
					users_del		: 	$("#users_del:checked").prop('checked') == true ? '1' : '0'
				},
				function(data){
					if (data == 1) {
						window.location.replace(site_url+"/cUsers/");
					}
					else {
						alert("Error en el proceso...")
						window.location.replace(site_url+"/cUsers/edit?id="+<?php echo $users[0]['id']; ?>);
					}
				}
			);
		});

		$('#li-configuration').addClass('menu-open');
      	$('#ul-configuration').css('display', 'block');
      	
		$('#li-users').addClass('menu-open');
		$('#ul-users').css('display', 'block');
	});
	
</script>
</body>
</html>
