<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-success">
					<div class="box-header ui-sortable-handle">
					    <i class="fa fa-sign-in"></i>
						<h3 class="box-title">Detalle</h3>
				  	</div>

			  		<div class="box-body">
			  			<fieldset>
			  				<legend>Ingresos</legend>
			  				<table id="internalcontrolin" class="table table-striped table-bordered table-condensed" style="width: 100%">
                                <thead>
                                  <tr>
                                    <th>Título</th>
                                    <th>Empresa</th>
                                    <th>Chofer</th>
                                    <th>Patente</th>
                                    <th>Fecha</th>
                                    <th>Acción</th>
                                  </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
			  			</fieldset>
			  			<br>
			  			<fieldset>
			  				<legend>Salidas</legend>
			  				<table id="internalcontrolout" class="table table-striped table-bordered table-condensed" style="width: 100%">
                                <thead>
                                  <tr>
                                    <th>Título</th>
                                    <th>Empresa</th>
                                    <th>Chofer</th>
                                    <th>Patente</th>
                                    <th>Fecha</th>
                                    <th>Acción</th>
                                  </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
			  			</fieldset>
					</div>

  				    <div class="box-footer">
  				    	<a href="<?php echo site_url(); ?>/CInternal_Forms/list" class="btn btn-primary pull-right" role="button">
                            <i class='fa fa-undo'></i> Volver
                        </a>
  				    </div>
  				</div>
			</div>
		</div>
	</section>
</div>

<div id="modal-detail-internalform" class="modal fade" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Detalle de control</h4>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table id="details" style="width: 100%" class="table table-bordered table-striped"></table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<?php $this->view('footer'); ?>

<script src="<?php echo base_url()?>assets/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>

<script>

	$(document).ready(function() {
		
		$('#internalcontrolin').DataTable({
		  "lengthMenu": [[5, 10, 15], [5, 10, 15]],
		  'responsive': true,
		  'paging': true,
		  'info': true,
		  'filter': true,
		  'ordering': true,
		  // 'stateSave': true,
		  'processing':true,
		  'serverSide':true,
		  'language': {
		    "url": base_url + "assets/Spanish.json",
		    "type":"POST",
		  },
		  dom: 'Bfrtip',
    	  buttons: [
        	'excel'
      		],
		  "order": [[0, "asc"]],
		  'ajax': {
		    "url": site_url + "/CInternal_Forms/datain",
		    "type":"POST",
		  },
		  "columns": [
		    { "data": "Título"},
		    { "data": "Empresa"},
		    { "data": "Chofer"},
		    { "data": "Patente" },
		    { "data": "Fecha" },
		    { "data": "Acción" }
		  ],
		  "columnDefs": [
		    {
		      "targets": [0],
		      "orderable": true,
		      "render": function(data, type, row) {
		        return row.title
		      }
		    },
		    {
		      "targets": [1],
		      "orderable": true,
		      "render": function(data, type, row) {
		        return row.company
		      }
		    },
		    {
		      "targets": [2],
		      "orderable": true,
		      "render": function(data, type, row) {
		        return row.rut+'-'+row.digit+' | '+row.name+' '+row.last_name
		      }
		    },
		    {
		      "targets": [3],
		      "orderable": true,
		      "render": function(data, type, row) {
		        return row.patent
		      }
		    },
		    {
		      "targets": [4],
		      "orderable": true,
		      "render": function(data, type, row) {
		        return row.created
		      }
		    },
		    {
		      "targets": [5],
		      "orderable": false,
		      "render": function(data, type, row) {
		        return `
		          <a href="#" class="btn btn-primary btn-xs" role="button" onclick="modaldetailinternalform('`+row.id+`', '`+0+`')">
		            <i class='fa fa-search'></i> Ver
	        	</a>
	        	<a href="<?php echo site_url(); ?>/CInternal_Forms/detailsPDF?id=`+row.id+`&mov=0" class="btn btn-primary btn-xs" role="button">
		            <i class='fa fa-file-pdf-o'></i> PDF
	        	</a>`
		      }
		    }
		   ],
		});

		$('#internalcontrolout').DataTable({
		  "lengthMenu": [[5, 10, 15], [5, 10, 15]],
		  'responsive': true,
		  'paging': true,
		  'info': true,
		  'filter': true,
		  'ordering': true,
		  // 'stateSave': true,
		  'processing':true,
		  'serverSide':true,
		  'language': {
		    "url": base_url + "assets/Spanish.json"
		  },
		  dom: 'Bfrtip',
    	  buttons: [
        	'excel'
      		],
		  "order": [[0, "asc"]],
		  'ajax': {
		    "url": site_url + "/CInternal_Forms/dataout",
		    "type":"POST",
		  },
		  "columns": [
		    { "data": "Título"},
		    { "data": "Empresa"},
		    { "data": "Chofer"},
		    { "data": "Patente" },
		    { "data": "Fecha" },
		    { "data": "Acción" }
		  ],
		  "columnDefs": [
		    {
		      "targets": [0],
		      "orderable": true,
		      "render": function(data, type, row) {
		        return row.title
		      }
		    },
		    {
		      "targets": [1],
		      "orderable": true,
		      "render": function(data, type, row) {
		        return row.company
		      }
		    },
		    {
		      "targets": [2],
		      "orderable": true,
		      "render": function(data, type, row) {
		        return row.rut+'-'+row.digit+' | '+row.name+' '+row.last_name
		      }
		    },
		    {
		      "targets": [3],
		      "orderable": true,
		      "render": function(data, type, row) {
		        return row.patent
		      }
		    },
		    {
		      "targets": [4],
		      "orderable": true,
		      "render": function(data, type, row) {
		        return row.created
		      }
		    },
		    {
		      "targets": [5],
		      "orderable": false,
		      "render": function(data, type, row) {
		        return `
		          <a href="#" class="btn btn-primary btn-xs" role="button" onclick="modaldetailinternalform('`+row.id+`', '`+1+`')">
		            <i class='fa fa-search'></i> Ver
		        </a>
		        <a href="<?php echo site_url(); ?>/CInternal_Forms/detailsPDF?id=`+row.id+`&mov=1" class="btn btn-primary btn-xs" role="button">
		            <i class='fa fa-file-pdf-o'></i> PDF
	        	</a>`
		      }
		    }
		   ],
		});

		$('#li-internals').addClass('menu-open');
		$('#ul-internals').css('display', 'block');

		$('#li-internal-sub').addClass('menu-open');
		$('#ul-internal-sub').css('display', 'block');
    });

    function modaldetailinternalform(internal, control){
        $.ajax({
            url: site_url + '/CInternal_Forms/details',
            type: 'POST',
            dataType: 'json',
            data: {
                internal_id: internal,
                control: control
            },
            success: function(data){
                if (data.mensaje != 0) {
                    var html = '';
                    html += '<tbody>';
                    html += '<tr><td><label>Título</label></td>';
                    html += '<td><label>'+data.det[0].title+'</label></td></tr>';
                    html += '<tr><td><label>Observación</label></td>';
                    html += '<td><label>'+data.det[0].observation+'</label></td></tr>';
                    html += '<tr><td><label>Patente</label></td>';
                    html += '<td><label>'+data.det[0].patent+'</label></td></tr>';
                    html += '<tr><td><label>Tipo</label></td>';
                    html += '<td><label>'+data.det[0].type+'</label></td></tr>';
                    for (var i = 0; i < data.det.length; i++) {
                        html += '<tr><td><label>'+data.det[i].question+'</label></td>';
                        html += '<td><label>'+data.det[i].answer+'</label></td></tr>';
                    }
                    html += '</tbody>';
                    $("#details").html(html);
                    $("#modal-detail-internalform").modal('show');
                }
            }
        });
    }
</script>
</body>
</html>
