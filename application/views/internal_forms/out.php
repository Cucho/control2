<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-success">
					<div class="box-header ui-sortable-handle">
					    <i class="fa fa-file-text"></i>
						<h3 class="box-title">Formulario control de salida</h3>
				  	</div>

			  		<div class="box-body">
			  			<form id="inControl" class="form-inline">
		  				<div class="col-md-10">
		  				    <div class="form-group">
			                  	<label for="input-rut">Rut</label>
			                  	<input autocomplete="off" class="form-control" type="text" name="inputrut" autofocus="autofocus" required="required" maxlength="9" id="inputrut" >&nbsp;&nbsp;&nbsp;&nbsp;
			                  	<button type="submit" class="btn btn-primary">Buscar</button>
			                </div>
			            </div>
	  				    </form>
	  				    <div class="col-md-12"><br></div>
	  				    <div class="col-md-10">
	  				    	<div class="form-group hidden" name="selv">
	  				    		<label for="vehicles">Vehículos</label>
	  				    		<select name="vehicles" id="vehicles" class="form-control">
	  				    			<option value="">Seleccione un vehículo</option>
	  				    		</select>
	  				    	</div>
	  				    </div>
						
						<div class="col-md-10">
	  				    	<div class="form-group hidden" name="self">
	  				    		<label for="forms">Formularios</label>
	  				    		<select name="forms" id="forms" class="form-control">
	  				    			<option value="">Seleccione un formulario</option>
	  				    		</select>
	  				    	</div>
	  				    </div>

	  				    <div class="col-md-12"><br></div>
						<form id="formin">
						<div class="col-md-10 hidden" id="formtb">
							<table class="table table-condensed" id="tb-form">
								<tr>
									<td><label>Título:</label></td>
									<td><input type="text" class="form-control" style="width:700px;" id="formtitle"></td>
								</tr>
								<tr>
									<td><label>Observación:</label></td>
									<td><textarea id="formobservation" class="form-control textarea" style="width:700px;" rows="5"></textarea></td>
								</tr>
							</table>
						</div>
					</div>

  				    <div class="box-footer hidden">
  				    	<div class="col-md-6 alert alert-danger alert-dismissible hidden" id="alert">
                			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            				<span id="mensaje" class="text-danger"></span>
              			</div>
  				    	<button type="submit" class="btn btn-primary pull-right">Guardar</button>
  				    </div>
  				    	</form>
  				</div>
			</div>
		</div>
	</section>
	
</div>

<?php $this->view('footer'); ?>

<script src="<?php echo base_url()?>assets/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>

<script>
	var people_id;//inControl
	var forms_id;//vehicles
	var vehicles_id;//vehicles

	$(document).ready(function() {
		Number.prototype.padDigit = function () {
	       return (this < 10) ? '0' + this : this;
	   	}

	   	$("#inControl").submit(function(event) {
			event.preventDefault();

			var rut = document.getElementById('inputrut');
			if (checkRut(rut)) {			
				$.ajax({
					url: site_url + '/CInternal_Forms/searchPeople/',
					type: 'POST',
					dataType: 'json',
					data: {
						rut: $("#inputrut").val()
					},
					success: function(data){
						if (data.mensaje != 0) {
							if (data.people[0].internal == 1) {
								people_id = data.people[0].people_id;
								var html = '';
								for (var i = 0; i < data.people.length; i++) {
									html += '<option value="'+data.people[i].vehicles_id+'">'+data.people[i].patent+' / '+data.people[i].type+'</option>';
								}
								$("#vehicles").find('option').remove();
								$("#vehicles").html('<option value="">Seleccione un vehículo</option>');
								$("#vehicles").append(html);
								$("div[name='selv']").removeClass('hidden');
							}
							else if (data.people[0].internal != 1) {
								alert("El rut no corresponde a personal interno");
							}
							
						}
						else if (data.mensaje == 0) {
							$("#vehicles").find('option').remove();
							$("#vehicles").html('<option value="">Seleccione un vehículo</option>');
							$("div[name='selv']").addClass('hidden');
							alert("El rut no esta asociado a ningún vehículo");
						}
					}
				});
			}
		});

		// $("#projects").change(function(event) {
		// 	if ($(this).val() != '') {
		// 		projects_id = $(this).val();
		// 		$.ajax({
		// 			url: site_url + '/CProjects_Forms/searchProjectsVehicles/',
		// 			type: 'post',
		// 			dataType: 'json',
		// 			data: {
		// 				project: projects_id,
		// 			},
		// 			success: function(data){
		// 				if (data.mensaje != 0) {
		// 					var html = '';
		// 					for (var i = 0; i < data.vehicles.length; i++) {
		// 						html += '<option value="'+data.vehicles[i].vehicles_id+'">'+data.vehicles[i].patent+' / '+data.vehicles[i].type+'</option>';
		// 					}
		// 					$("#vehicles").find('option').remove();
		// 					$("#vehicles").html('<option value="">Seleccione un vehículo</option>');
		// 					$("#vehicles").append(html);
		// 					$("div[name='selv']").removeClass('hidden');
		// 				}
		// 				else if (data.mensaje == 0) {
		// 					$("#vehicles").find('option').remove();
		// 					$("#vehicles").html('<option value="">Seleccione un vehículo</option>');
		// 					$("div[name='selv']").addClass('hidden');
		// 				}
		// 			}
		// 		});
		// 	}
		// 	else {
		// 		$("#vehicles").find('option').remove();
		// 		$("#vehicles").html('<option value="">Seleccione un vehículo</option>');
		// 		$("div[name='selv']").addClass('hidden');
		// 	}
		// });

		$("#vehicles").change(function(event) {
			if ($(this).val() != '') {
				vehicles_id = $(this).val();
				$.ajax({
					url: site_url + '/CInternal_Forms/searchForm/',
					type: 'POST',
					dataType: 'json',
					success: function(data){
						if (data.mensaje != 0) {
							var html = '';
							for (var i = 0; i < data.forms.length; i++) {
								html += '<option value="'+data.forms[i].forms_id+'">'+data.forms[i].title+' / '+data.forms[i].description+'</option>';
							}
							$("#forms").find('option').remove();
							$("#forms").html('<option value="">Seleccione un formulario</option>');
							$("#forms").append(html);
							$("div[name='self']").removeClass('hidden');
						}
						else if (data.mensaje == 0) {
							$("#forms").find('option').remove();
							$("#forms").html('<option value="">Seleccione un formulario</option>');
							$("div[name='self']").addClass('hidden');
						}
					}
				});
			}
			else {
				$("#forms").find('option').remove();
				$("#forms").html('<option value="">Seleccione un formulario</option>');
				$("div[name='self']").addClass('hidden');
			}
		});

		$("#forms").change(function(event) {
			if ($(this).val() != '') {
				forms_id = $(this).val();
				$("table#tb-form tr.noexiste").remove();
				$.ajax({
					url: site_url + '/CInternal_Forms/getForms/',
					type: 'POST',
					dataType: 'json',
					data: {
						form : forms_id
					},
					success: function(data){
						if (data.mensaje != 0) {
							for (var i = 0; i < data.forms.length; i++) {
								if (forms_id == data.forms[i].forms_id) {
									var h = '';
									h += '<tr class="noexiste">';
									h += '<td><label>'+data.forms[i].question+'</label></td>';
									if (data.forms[i].ans == 1) {
										h += '<td><input type="text" class="form-control fm" id="q'+data.forms[i].order+'" placeholder="'+data.forms[i].placeholder+'"></td>';
									}
									else if (data.forms[i].ans == 2) {
										h += '<td><textarea class="form-control area fm" id="q'+data.forms[i].order+'" placeholder="'+data.forms[i].placeholder+'"></textarea></td>';
									}
									else if (data.forms[i].ans == 3) {
										h += '<td><input type="number" class="fm" style="width:200px;" id="q'+data.forms[i].order+'" placeholder="'+data.forms[i].placeholder+'"> '+data.forms[i].measure+'</td>';
									}
									else if (data.forms[i].ans == 4) {
										h += '<td><input type="date" class="fm" style="width:200px;" id="q'+data.forms[i].order+'" placeholder="'+data.forms[i].placeholder+'"></td>';
									}
									else if (data.forms[i].ans == 5) {
										h += '<td><input type="checkbox" class="checks fm" style="width:20px;" id="q'+data.forms[i].order+'"> '+data.forms[i].placeholder+'</td>';
									}
									h += '</tr>';
								}
								$("#tb-form").append(h);
								$("#formtb").removeClass('hidden');
							}
							$("div .box-footer").removeClass('hidden');
							$('.area').wysihtml5();
						}
					}
				});
			}
			else {
				$("#tb-form").find('tr').remove();
				$("#tb-form").append(`<tr>
						<td><label>Título:</label></td>
						<td><input type="text" class="form-control" style="width:700px;" id="formtitle"></td>
					</tr>
					<tr>
						<td><label>Observación:</label></td>
						<td><textarea id="formobservation" class="form-control textarea" style="width:700px;" rows="5"></textarea></td>
					</tr>`);
				$("div .box-footer").addClass('hidden');
				$('.area').wysihtml5();
			}
		});

		$("#formin").submit(function(event) {
			event.preventDefault();

			if ($("#formtitle").val() == '') {
				$("#alert").removeClass('hidden');
				$("#mensaje").text('El título es obligatorio.');
				setInterval(function(){ 
					$("#mensaje").text('');
					$("#alert").addClass('hidden');
				}, 1500);
			}

			answer = [];
			forder = [];
			$(".fm").each(function(index, el) {
				if ($(this).attr('type') != 'checkbox') {
					if ($(this).val() == '') {
						$("#alert").removeClass('hidden');
						$("#mensaje").text('Debe contestar todas las preguntas del formulario.');
						setInterval(function(){ 
							$("#mensaje").text('');
							$("#alert").addClass('hidden');
						}, 1500);
					}
					else if ($(this).prop('type') != 'checkbox'){
						answer.push($(this).val())
						forder.push(index+1)
					}
				}
				else {
					if ($(this).prop('checked') == true) {
						answer.push('1');
						forder.push(index+1);
					}
					else if ($(this).prop('checked') == false) {
						answer.push('0');
						forder.push(index+1);
					}
				}
				
			});
			
			if ($("#formtitle").val() != '' && answer.length > 0 && forder.length > 0) {
				$.ajax({
					url: site_url + '/CInternal_Forms/addFormOut/',
					type: 'POST',
					dataType: 'json',
					data: {
						title 			: $("#formtitle").val(),
						observation 	: $("#formobservation").val(),
						answers 		: answer,
						order 			: forder,
						people_id 		: people_id,
						forms_id		: forms_id,
						vehicles_id 	: vehicles_id
					},
					success: function(data){
						if (data == 1) {
							window.location.replace(site_url+"/CInternal_Forms/in");
						}
						else if (data == 0) {
							alert("Error en el proceso...")
    		            	window.location.replace(site_url+"/CInternal_Forms/in");
						}
					}
				});
			}
		});

		$('#li-internals').addClass('menu-open');
		$('#ul-internals').css('display', 'block');

		$('#li-internal-sub').addClass('menu-open');
		$('#ul-internal-sub').css('display', 'block');
	});

	function checkRut(rut) {
		// Despejar Puntos
        var valor = rut.value.replace('.','');
        // Despejar Guión
        valor = valor.replace('-','');
        
        // Aislar Cuerpo y Dígito Verificador
        var cuerpo = valor.slice(0,-1);
        var dv = valor.slice(-1).toUpperCase();
        // Formatear RUN
        rut.value = cuerpo + '-'+ dv
        // Si no cumple con el mínimo ej. (n.nnn.nnn)
        if(cuerpo.length < 7){
            rut.setCustomValidity("RUT Incompleto");
            rut.setCustomValidity('');
            cuerpo = '';
            dv = '';
            rut.value = '';
            return false;
        }
        else{
            // Calcular Dígito Verificador
            suma = 0;
            multiplo = 2;
            
            // Para cada dígito del Cuerpo
            for(i=1;i<=cuerpo.length;i++) {
            
                // Obtener su Producto con el Múltiplo Correspondiente
                index = multiplo * valor.charAt(cuerpo.length - i);
                
                // Sumar al Contador General
                suma = suma + index;
                
                // Consolidar Múltiplo dentro del rango [2,7]
                if(multiplo < 7) { multiplo = multiplo + 1; } else { multiplo = 2; }
          
            }
            
            // Calcular Dígito Verificador en base al Módulo 11
            dvEsperado = 11 - (suma % 11);
            
            // Casos Especiales (0 y K)
            dv = (dv == 'K')?10:dv;
            dv = (dv == 0)?11:dv;
            
            // Validar que el Cuerpo coincide con su Dígito Verificador
            if(dvEsperado != dv){ 
                rut.setCustomValidity("RUT Inválido");
                rut.setCustomValidity('');
                cuerpo = '';
                dv = '';
                rut.value = '';
                return false;
            }
            // Si todo sale bien, eliminar errores (decretar que es válido)
            rut.setCustomValidity('');
            return true;
        }
    }
</script>
</body>
</html>
