<style type="text/css">
  fieldset 
  {
        border: 1px solid #ddd !important;
        margin: 0;
        xmin-width: 0;
        padding: 10px;       
        position: relative;
        border-radius:4px;
        //background-color:#f5f5f5;
        padding-left:10px!important;
    }
    legend
    {
        font-size:14px;
        font-weight:bold;
        margin-bottom: 0px; 
        width: 35%; 
        border: 1px solid #ddd;
        border-radius: 4px; 
        padding: 5px 5px 5px 10px; 
        background-color: #ffffff;
    }
    .form-control {
      background-color: #ffffff;
      border: 1px solid #light;
      border-radius: 0px;
      box-shadow: none;
      color: #565656;
      height: 20px;
      width: 100%;
      max-width: 100%;
      padding: 7px 12px;
      transition: all 300ms linear 0s;
      font-size: 11px;
  }
  .table {
      width: 100%;
      max-width: 100%;
      margin-bottom: 20px;
      border-spacing: 0;
      border-collapse: collapse;
      display: table;
      border-collapse: separate;
      border-spacing: 2px;
      border-color: grey;
      font-size: 11px;
  }
</style>

<center>
  <h3>Formulario control de <?php if ($details[0]->control == 0) { echo 'ingreso';} else { echo 'salida';} ?></h3>
</center>
<?php echo $details[0]->created; ?>
<br><br>
<fieldset>
  
  <table class="table">
    <tr>

      <td width="15%">Título: </td>
      <td width="45%">
        <?php echo '<input readonly style="width:250px;height:25px;" value="'.$details[0]->title.'">'; ?>
      </td>
    </tr>

    <tr>
      <td width="15%">Observación: </td>
      <td width="45%"><textarea style="width:259px;"><?php echo $details[0]->observation; ?></textarea></td>
    </tr>

    <tr>
      <td width="15%">Patente: </td>
      <td width="45%"><input readonly style="width:250px;height:25px;" value="<?php echo $details[0]->patent; ?>"></td>
    </tr>

    <tr>
      <td width="15%">Tipo: </td>
      <td width="45%">
        <?php echo '<input readonly style="width:250px;height:25px;" value="'.$details[0]->type.'">'; ?>
      </td>
    </tr>

  </table>
</fieldset>
<br>
<fieldset>
  <table class="table">
    <?php foreach ($details as $k => $val) { ?>
      <tr>
          <td width="15%"><?php echo $val->question; ?>:</td>
          <td width="60%">
            <?php if ($k != 1) { 
              echo '<input readonly style="width:250px;height:50px;" value="'.$val->answer.'">';
            }
            else {
              echo '<textarea style="width:259px;">'.$val->answer.'</textarea>';
            } ?>
          </td>
      </tr>
    <?php } ?>
  </table>
</fieldset> 