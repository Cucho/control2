<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-success">
					<div class="box-header ui-sortable-handle">
					    <i class="fa fa-car"></i>
						<h3 class="box-title">Vehículo</h3>
				  	</div>
					
					<div class="box-body">
			  			<section class="content">
			  			    <table id="vehicles" class="table">
		  		    		    <tbody>
		  		    		    	<tr>
		  		    		    		<th>Id</th>
		  			    				<td><?php echo $vehicles[0]['id'];?></td>
		  		    		    	</tr>
									<tr>
		  		    		    		<th>Patente</th>
		  			    				<td><?php echo $vehicles[0]['patent'];?></td>
		  		    		    	</tr>
                          			<tr>
		  		    	    			<th>Modelo</th>
		  		    	    			<td><?php echo $vehicles[0]['model'];?></td>
		  		    	    		</tr>
                          			<tr>
                          				<th>Interno</th>
                          				<td>
                          					<?php
		  		    	    					switch ($vehicles[0]['internal']) {
		  		    	    					 	case 0:
		  		    	    					 		echo 'EXTERNO VISITA';
		  		    	    					 		break;
		  		    	    					 	case 1:
		  		    	    					 		echo 'INTERNO';
		  		    	    					 		break;
		  		    	    					 	case 2:
		  		    	    					 		echo 'EXTERNO CONTRATISTA';
		  		    	    					 		break;
		  		    	    					 } ?>
                          				</td>
                          			</tr>
                          			<tr>
                          				<th>Empresa</th>
                          				<td><?php echo $vehicles[0]['company'];?></td>
                          			</tr>
                          			<tr>
                          				<th>Encargado</th>
                          				<td><?php echo $vehicles[0]['name'];?></td>
                          			</tr>
                          			<tr>
                          				<th>Tipo vehículo</th>
                          				<td><?php echo $vehicles[0]['type'];?></td>
                          			</tr>
                          			<tr>
                          				<th>Vehículo perfil</th>
                          				<td><?php echo $vehicles[0]['profile'];?></td>
                          			</tr>
                          			<tr>
                          				<th>Estado</th>
                          				<td><?php echo $vehicles[0]['state'];?></td>
                          			</tr>
		  		    		    	<tr>
		  		    	    			<th>Creado</th>
		  		    	    			<td><?php echo $vehicles[0]['created'];?></td>
		  		    	    		</tr>
		  		    	    		<tr>
		  		    	    			<th>Modificado</th>
		  		    	    			<td><?php echo $vehicles[0]['modified'];?></td>
		  		    	    		</tr>
		  		    		    </tbody>
		  			    	</table>
			  			</section>
				  	</div>
				  	<div class="box-footer">
				  		<a href="<?php echo site_url(); ?>/cVehicles/" class="btn btn-primary pull-right" role="button">
	                    <i class='fa fa-undo'></i> Volver
	                </a>
				  	</div>
				</div>
			</div>
		</div>
	</section>
	
</div>

<?php $this->view('footer'); ?>

<script>
    $(document).ready(function() {
      	$('#li-vehicles').addClass('menu-open');
		$('#ul-vehicles').css('display', 'block');
    });
</script>
</body>
</html>