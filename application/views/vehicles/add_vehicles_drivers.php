<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-success">
					<div class="box-header ui-sortable-handle">
					    
					   <h3 class="box-title">Vincular  <i class="fa fa-car"></i> Vehículo - <i class="fa fa-male"></i> Chofer</h3>
			  	</div>
					
				  	<form class="form-horizontal" id="addVehicle_Driver">
			  			<div class="box-body"> 
			  				<div class="form-group">
			  					<label for="vehicle" class="col-sm-2 control-label">Vehículo</label>
			  					<div class="col-sm-5">
			  						<select required class="form-control" name="vehicle" id="vehicle" required>
			  							<option value="">Seleccione una opción</option>
			  							<?php
		  									foreach($vehicle as $a) {
		  										echo '<option value="'.$a['id'].'">['.$a['patent'].'] '.$a['model'].'</option>';
		  									}
		  								?>
			  						</select>
			  					</div>
			  				</div>
			  				<div class="form-group">
		  						<label for="driver" class="col-sm-2 control-label">Chofer</label>
			  					<div class="col-sm-5">
			  						<select required class="form-control" name="driver" id="driver" required>
			  							<option value="">Seleccione una opción</option>
			  							<?php
		  									foreach($driver as $a) {
		  										echo '<option value="'.$a['id'].'">['.$a['rut'].'] '.$a['name'].' '.$a['last_name'].'</option>';
		  									}
		  								?>
			  						</select>
			  					</div>
			  				</div>
			  			</div>
			  			<div class="box-footer">
			  				<button type="submit" class="btn btn-primary pull-right">Guardar</button>
			  			</div>
			  		</form>
				</div>
			</div>
		</div>
	</section>

</div>

<?php $this->view('footer'); ?>

<script>
	$(document).ready(function() {
		$("#addVehicle_Driver").submit(function(event) {
			event.preventDefault();

			$.post(
				site_url + "/cVehicles_Drivers/addVehicle_Driver",{
					vehicle 	: 	$("#vehicle").val(),
					driver 		: 	$("#driver").val()
				},
				function(data){
					if (data == 1) {
						window.location.replace(site_url+"/cVehicles_Drivers/");
					}
					else {
						alert("Error en el proceso...")
						window.location.replace(site_url+"/cVehicles_Drivers/add");
					}
				}
			);
		});

		$('#li-vehicles').addClass('menu-open');
		$('#ul-vehicles').css('display', 'block');
	});
</script>
</body>
</html>
