<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-success">
					<div class="box-header ui-sortable-handle">
					    <i class="fa fa-car"></i>
						<h3 class="box-title">Agregar vehículo</h3>
				  	</div>
					
				  	<form class="form-horizontal" id="addVehicles">
			  			<div class="box-body">
			  				<div class="form-group">
			  					<label for="patents" class="col-sm-2 control-label">Patente</label>
			  					<div class="col-sm-10">
			  						<input type="text" class="form-control" name="patents" id="patents" required>
			  					</div>
			  				</div>
			  				<div class="form-group">
			  					<label for="model" class="col-sm-2 control-label">Modelo</label>
			  					<div class="col-sm-10">
			  						<input type="text" class="form-control" name="model" id="model" required>
			  					</div>
			  				</div>
			  				<div class="form-group">
			  					<label for="internal" class="col-sm-2 control-label">Interno</label>
			  					<div class="col-sm-10">
			  						<label class="radio-inline">
								  		<input type="radio" name="internal" id="internal1" value="1" required> Interno
									</label>
									<label class="radio-inline">
										<input type="radio" name="internal" id="internal2" value="0" required> Externo Visita
									</label>
									<label class="radio-inline">
										<input type="radio" name="internal" id="internal3" value="2" required> Externo Contratista
									</label>
			  					</div>
			  				</div>
			  				<div class="form-group">
			  				</div>
							<div class="form-group">
			  					<label for="companies" class="col-sm-2 control-label">Empresa</label>
			  					<div class="col-sm-5">
			  						<select name="companies" id="companies" class="form-control" required>
			  							<option value="">Seleccione una opción</option>
			  							<?php foreach ($companies as $key) { ?>
			  								<option value="<?php echo $key->id; ?>"><?php echo $key->company; ?></option>
			  							<?php } ?>
			  						</select>
			  					</div>
			  				</div>
			  				<div class="form-group">
			  					<label for="people" class="col-sm-2 control-label">Encargado</label>
			  					<div class="col-sm-5">
			  						<select name="people" id="people" class="form-control" required>
			  							<option value="">Seleccione una opción</option>
			  							<?php foreach ($people as $key) { ?>
			  								<option value="<?php echo $key->id; ?>"><?php echo $key->name.' '.$key->last_name; ?></option>
			  							<?php } ?>
			  						</select>
			  					</div>
			  				</div>
			  				<div class="form-group">
			  					<label for="type" class="col-sm-2 control-label">Tipo vehiculo</label>
			  					<div class="col-sm-5">
			  						<select name="type" id="type" class="form-control" required>
			  							<option value="">Seleccione una opción</option>
			  							<?php foreach ($type as $key) { ?>
			  								<option value="<?php echo $key->id; ?>"><?php echo $key->type; ?></option>
			  							<?php } ?>
			  						</select>
			  					</div>
			  				</div>
			  				<div class="form-group">
			  					<label for="profile" class="col-sm-2 control-label">Vehiculo perfil</label>
			  					<div class="col-sm-5">
			  						<select name="profile" id="profile" class="form-control" required>
			  							<option value="">Seleccione una opción</option>
			  							<?php foreach ($profiles as $key) { ?>
			  								<option value="<?php echo $key->id; ?>"><?php echo $key->profile; ?></option>
			  							<?php } ?>
			  						</select>
			  					</div>
			  				</div>

			  				<div class="form-group">
  			  					<label for="profile" class="col-sm-2 control-label">Estado</label>
  			  					<div class="col-sm-5">
  			  						<select name="state" id="state" class="form-control" required>
  			  							<option value="">Seleccione una opción</option>
  			  							<?php
  			  							$this->db->select('id, state');
  			  							$this->db->from('states');
  			  							$res = $this->db->get()->result_array();
  			  							if(!empty($res))
  			  							{
  			  								foreach($res as $r)
  			  								{
  			  									echo '<option value="'.$r['id'].'">'.$r['state'].'</option>';
  			  								}
  			  							}
  			  							?>
  			  						</select>
  			  					</div>
  			  				</div>
			  			</div>
			  			<div class="box-footer">
			  				<button type="submit" class="btn btn-primary pull-right">Guardar</button>
			  			</div>
			  		</form>
				</div>
			</div>
		</div>
	</section>
	
</div>

<?php $this->view('footer'); ?>

<script>
	$(document).ready(function() {
		$("#addVehicles").submit(function(event) {
			event.preventDefault();

			$.post(
				site_url + "/cVehicles/addVehicles",{
					patents 	: 	$("#patents").val(),
					model 		: 	$("#model").val(),
					internal 	: 	$("input[name='internal']:checked").val(),
					companies 	: 	$("#companies").val(),
					people 		: 	$("#people").val(),
					type 		: 	$("#type").val(),
					profile 	: 	$("#profile").val(),
					states_id   : 	$("#state").val()
				},
				function(data){
					if (data == 1) {
						window.location.replace(site_url+"/cVehicles/");
					}
					else {
						alert("Error en el proceso...")
						window.location.replace(site_url+"/cVehicles/add");
					}
				}
			);
		});

		$('#li-vehicles').addClass('menu-open');
		$('#ul-vehicles').css('display', 'block');
	});
</script>
</body>
</html>
