<div class="content-wrapper">
	<section class="content">
    <div class="row">
      <div class="col-sm-12">
        <div class="box box-success">
          <div class="box-header ui-sortable-handle">
              <i class="fa fa-car"></i>
            <h3 class="box-title">Vehículos</h3>
            </div>

            <div class="box-body">
              <section class="content">
                <?php if ($this->session->userdata('save')) { ?>
                  <a href="<?php echo site_url() ?>/cVehicles/add" class="btn btn-primary">Agregar</a><br><hr>
                <?php } ?>
                  <table id="vehicles" class="table table-striped table-bordered table-condensed" style="width:100%;">
                      <thead>
                        <tr>
                          <th>ID</th>
                          <th>Patente</th>
                          <th>Modelo</th>
                          <!-- <th>Interno</th> -->
                          <th>Empresa</th>
                          <th>Encargado</th>
                          <th>Tipo vehículo</th>
                          <th>Vehículo perfil</th>
                          <th>Estado</th>
                          <th>Acción</th>
                        </tr>
                      </thead>
                      <tbody></tbody>
                  </table>
              </section>
            </div>
            <div class="box-footer"></div>
        </div>
      </div>
    </div>
  </section>
	
</div>

<?php $this->view('footer'); ?>

<script>
  var edit = <?php echo $this->session->userdata('edit'); ?>;
  var del = <?php echo $this->session->userdata('del'); ?>;

    $(document).ready(function() {
      $('#vehicles').DataTable({
          "lengthMenu": [[5, 10, 15, 20,], [5, 10, 15, 20]],
          'responsive': true,
          'paging': true,
          'info': true,
          'filter': true,
          'ordering': true,
          // 'stateSave': true,
          'processing':true,
          'serverSide':true,
          'language': {
            "url": base_url + "assets/Spanish.json"
          },
          "order": [[0, "asc"]],
          'ajax': {
            "url": site_url + "/cVehicles/datatable",
            "type":"POST",
          },
          "columns": [
            { "data": "ID"},
            { "data": "Patente" },
            { "data": "Modelo" },
            //{ "data": "Interno" },
            { "data": "Empresa" },
            { "data": "Encargado" },
            { "data": "Tipo vehículo" },
            { "data": "Vehículo perfil" },
            { "data": "Estado"},
            { "data": "Acción" }
          ],
          "columnDefs": [
            {
              "targets": [0],
              "orderable": true,
              "render": function(data, type, row) {
                return row.id
              }
            },
            {
              "targets": [1],
              "orderable": true,
              "render": function(data, type, row) {
                return row.patent
              }
            },
            {
              "targets": [2],
              "orderable": true,
              "render": function(data, type, row) {
                return row.model
              }
            },

            /*
            {
              "targets": [3],
              "orderable": true,
              "render": function(data, type, row) {
                return row.internal == 1 ? 'Si' : 'No' 
              }
            },
            */
            {
              "targets": [3],
              "orderable": true,
              "render": function(data, type, row) {
                return row.company
              }
            },
            {
              "targets": [4],
              "orderable": true,
              "render": function(data, type, row) {
                return row.name+' '+row.last_name
              }
            },
            {
              "targets": [5],
              "orderable": true,
              "render": function(data, type, row) {
                return row.type
              }
            },
            {
              "targets": [6],
              "orderable": true,
              "render": function(data, type, row) {
                return row.profile
              }
            },
            {
              "targets": [7],
              "orderable": true,
              "render": function(data, type, row) {
                return row.state
              }
            },
            {
              "targets": [8],
              "orderable": false,
              "render": function(data, type, row) {
                return edit == true && del == true ? `
                  <a href="<?php echo site_url(); ?>/cVehicles/view?id=`+row.id+`" class="btn btn-primary btn-xs" role="button">
                    <i class='fa fa-search'></i> Ver
                  </a>
                  <a href="<?php echo site_url(); ?>/cVehicles/edit?id=`+row.id+`" class="btn btn-warning btn-xs" role="button">
                      <i class='fa fa-pencil-square-o'></i> Editar
                  </a>
                  <a href="#" class="btn btn-danger btn-xs" role="button" onclick="delVehicles(`+row.id+`);">
                      <i class='fa fa-trash-o'></i> Eliminar
                  </a>` : edit == true && del == false ? `
                  <a href="<?php echo site_url(); ?>/cVehicles/view?id=`+row.id+`" class="btn btn-primary btn-xs" role="button">
                    <i class='fa fa-search'></i> Ver
                  </a>
                  <a href="<?php echo site_url(); ?>/cVehicles/edit?id=`+row.id+`" class="btn btn-warning btn-xs" role="button">
                      <i class='fa fa-pencil-square-o'></i> Editar
                  </a>` : edit == false && del == true ? `
                  <a href="<?php echo site_url(); ?>/cVehicles/view?id=`+row.id+`" class="btn btn-primary btn-xs" role="button">
                    <i class='fa fa-search'></i> Ver
                  </a>
                  <a href="#" class="btn btn-danger btn-xs" role="button" onclick="delVehicles(`+row.id+`);">
                      <i class='fa fa-trash-o'></i> Eliminar
                  </a>` : edit == false && del == false ? `
                  <a href="<?php echo site_url(); ?>/cVehicles/view?id=`+row.id+`" class="btn btn-primary btn-xs" role="button">
                    <i class='fa fa-search'></i> Ver
                  </a>` : '';
              }
            }
           ],
        });
      $('#li-vehicles').addClass('menu-open');
      $('#ul-vehicles').css('display', 'block');
    });

    function delVehicles(id) {
      if (confirm('¡Seguro de eliminar!')) {
        $.post(
          site_url + "/cVehicles/deleteVehicle",{
          id  :   id
        },
        function(data){
          if (data == 1) {
            window.location.reload();
          }
          else {
            alert("Este registro posee dependencias asociadas.\nNo se puede eliminar.")
          }
        }
      );
    }
  }
</script>

</body>
</html>
