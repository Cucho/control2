<div class="content-wrapper">
	<section class="content">
    <div class="row">
      <div class="col-sm-12">
        <div class="box box-success">
          <div class="box-header ui-sortable-handle">
              <i class="fa fa-car"></i>
              <i class="fa fa-male"></i>
              <h3 class="box-title">Vehículo - Chofer</h3>
          </div>

          <div class="box-body">
            <section class="content">
              <?php if ($this->session->userdata('save')) { ?>
                <a  href="<?php echo site_url() ?>/cVehicles_Drivers/add" class="btn btn-primary">Agregar</a><br><hr>
              <?php } ?>
                <table id="Vehicles_Drivers" class="table table-striped table-bordered table-condensed" style="width:100%;">
                    <thead>
                      <tr>
                        <th>Vehículo</th>
                        <th>Chofer</th>
                        <th>Acción</th>
                      </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </section>
          </div>
          <div class="box-footer"></div>
        </div>
      </div>
    </div>
  </section>
	
</div>

<?php $this->view('footer'); ?>

<script>
  var edit = <?php echo $this->session->userdata('edit'); ?>;
  var del = <?php echo $this->session->userdata('del'); ?>;

    $(document).ready(function() {
      $('#Vehicles_Drivers').DataTable({
          "lengthMenu": [[5, 10, 15, 20,], [5, 10, 15, 20]],
          'responsive': true,
          'paging': true,
          'info': true,
          'filter': true,
          'ordering': true,
          // 'stateSave': true,
          'processing':true,
          'serverSide':true,
          'language': {
            "url": base_url + "assets/Spanish.json"
          },
          "order": [[0, "asc"]],
          'ajax': {
            "url": site_url + "/cVehicles_Drivers/datatable",
            "type":"POST",
          },
          "columns": [
            { "data": "Vehículo" },
            { "data": "Chofer" },
            { "data": "Acción" }
          ],
          "columnDefs": [
            {
              "targets": [0],
              "orderable": true,
              "render": function(data, type, row) {
                return '['+row.patent+'] '+row.model
              }
            },
            {
              "targets": [1],
              "orderable": true,
              "render": function(data, type, row) {
                return '['+row.rut+'] '+row.name+' '+row.last_name
              }
            },
            {
              "targets": [2],
              "orderable": false,
              "render": function(data, type, row) {
                return del == true ? `
                  <a href="#" class="btn btn-danger btn-xs" role="button" onclick="delVehicle_Driver(`+row.vid+`,`+row.pid+`);">
                      <i class='fa fa-trash-o'></i> Eliminar
                  </a>` : '';
              }
            }
           ],
        });
      $('#li-vehicles').addClass('menu-open');
      $('#ul-vehicles').css('display', 'block');
    });

    function delVehicle_Driver(vehicles_id, people_id) {
      if (confirm('¡Seguro de eliminar!')) {
        $.post(
        site_url + "/cVehicles_Drivers/deletecVehicle_Driver",{
          vehicles_id   :   vehicles_id,
          people_id     :   people_id
        },
        function(data){
          if (data == 1) {
            window.location.reload();
          }
          else {
            alert("Error en el proceso...")
          }
        }
      );
    }
  }
</script>

</body>
</html>
