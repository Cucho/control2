<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-success">
					<div class="box-header ui-sortable-handle">
					    <i class="fa fa-sign-in"></i>
						<h3 class="box-title">Editar puerta #<?php echo $doors[0]['id']; ?></h3>
				  	</div>
					<form class="form-horizontal" id="addDoors">
			  			<div class="box-body">
			  				<div class="form-group">
			  					<label for="doors" class="col-sm-2 control-label">Puerta</label>
			  					<div class="col-sm-10">
			  						<input type="text" class="form-control" name="doors" id="doors" required value="<?php echo $doors[0]['door'] ?>">
			  					</div>
			  				</div>
			  				<div class="form-group">
			  					<label for="description" class="col-sm-2 control-label">Descripción</label>
			  					<div class="col-sm-10">
			  						<input type="text" class="form-control" name="description" id="description" value="<?php echo $doors[0]['description'] ?>">
			  					</div>
			  				</div>
			  				<div class="form-group">
			  					<label for="level" class="col-sm-2 control-label">Nivel</label>
			  					<div class="col-sm-3">
			  						<input type="number" class="form-control" name="level" id="level" required value="<?php echo $doors[0]['level'] ?>">
			  					</div>
			  				</div>
			  				<div class="form-group">
			  					<label for="doors_type" class="col-sm-2 control-label">Tipo Puerta</label>
			  					<div class="col-sm-5">
			  						<select name="doors_type" id="doors_type" class="form-control" required>
			  							<option value="">Seleccione una opción</option>
			  							<?php foreach ($types['data'] as $key) { ?>
			  								<option value="<?php echo $key->id; ?>"><?php echo $key->type; ?></option>
			  							<?php } ?>
			  						</select>
			  					</div>
			  				</div>
			  			</div>
			  			<div class="box-footer">
			  				<button type="submit" class="btn btn-primary pull-right">Guardar</button>
			  			</div>
			  		</form>
				</div>
			</div>
		</div>
	</section>
	
</div>


<?php $this->view('footer'); ?>

<script>
	$(document).ready(function() {
		$("#doors_type").val(<?php echo $doors[0]['doors_type_id']; ?>);

		$("#addDoors").submit(function(event) {
			event.preventDefault();

			$.post(
				site_url + "/cDoors/editDoors",{
					id 			: 	<?php echo $doors[0]['id']; ?>,
					doors 		: 	$("#doors").val(),
					description : $("#description").val(),
					level 		: $("#level").val(),
					doors_type 	: $("#doors_type").val()
				},
				function(data){
					if (data == 1) {
						window.location.replace(site_url+"/cDoors/");
					}
					else {
						alert("Error en el proceso...")
						window.location.replace(site_url+"/cDoors/edit?id="+<?php echo $doors[0]['id']; ?>);
					}
				}
			);
		});


		$('#li-configuration').addClass('menu-open');
      	$('#ul-configuration').css('display', 'block');
      	
		$('#li-doors').addClass('menu-open');
		$('#ul-doors').css('display', 'block');
	});
	
</script>
</body>
</html>
