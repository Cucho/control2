<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-success">
					<div class="box-header ui-sortable-handle">
					   <i class="fa fa-sing-in"></i> 
					   <h3 class="box-title">Vincular Puerta - Puerta Padre</h3>
			  	</div>
					
				  	<form class="form-horizontal" id="addDoor_Parent">
			  			<div class="box-body"> 
			  				<div class="form-group">
			  					<label for="doors" class="col-sm-2 control-label">Puerta</label>
			  					<div class="col-sm-5">
			  						<select class="form-control" name="doors" id="doors" required>
			  							<option value="">Seleccione una opción</option>
			  							<?php
		  									foreach($doors as $a) {
		  										echo '<option value="'.$a['did'].'" name="'.$a['level'].'">'.$a['door'].' ['.$a['level'].']</option>';
		  									}
		  								?>
			  						</select>
			  					</div>
			  				</div>
							<div class="box no-border">
								<div class="box-header">
									<h3 class="box-title">Puerta Padre</h3>
								</div>
								<div class="box-body">
									<?php foreach ($doors as $key) { ?>
									<p name="<?php echo $key['level'] ?>">
						      			<input id="check-<?php echo $key['did'] ?>" type="checkbox" name="<?php echo $key['level'] ?>" value="<?php echo $key['did'] ?>">
						      			<span>&nbsp;<?php echo $key['door'].' ['.$key['level'].']'; ?></span>
						    		</p>
						    		<?php } ?>
								</div>
							</div>
			  			</div>
			  			<div class="box-footer">
			  				<button type="submit" class="btn btn-primary pull-right">Guardar</button>
			  			</div>
			  		</form>
				</div>
			</div>
		</div>
	</section>

</div>

<?php $this->view('footer'); ?>

<script>
	var level;
	$("#doors").on('change', function(event) {
		$("select option:selected").each(function(index, el) {
			level = $(this).attr('name')
		});
		hideshow()
		getParentsDoor()
	})

	function getParentsDoor()
	{
		var door = parseInt($('#doors').val());

		$('input:checkbox').prop('checked', false);
		
		if(door > 0)
		{
			$.ajax({
				url: site_url+'/cDoors_Parents/getParentsDoor/',
				data: {doors_id: door},
				type: 'post',
				dataType: 'json',
				success: function(data)
				{
					if(data != null && data.length > 0)
					{
						for(var i=0;i<data.length;i++)
						{
							$('#check-'+data[i].parent).prop('checked', true);
						}
					}
				}
			});
		}
		else
		{
			$('input:checkbox').prop('checked', false);
		}

		
	}

	function hideshow() {
		$("p").each(function(index, el) {
			var level2 = $(this).attr('name');
			if (parseInt(level) <= parseInt(level2)) {
				$(this).hide();
			}
			else {
				$(this).show();
			}
		});
	}
	parent = [];

	$(document).ready(function() {
		$("#addDoor_Parent").submit(function(event) {
			event.preventDefault();

			$("input:checkbox").each(function(index, el) {
				if ( $(this).prop('checked')) {
					parent.push($(this).val())
				}
			});

			$.post(
				site_url + "/cDoors_Parents/addDoor_Parent",{
					doors_id 	: 	$("#doors").val(),
					parent 		: 	parent
				},
				function(data){
					if (data == 1) {
						window.location.replace(site_url+"/cDoors_Parents/");
					}
					else {
						alert("Error en el proceso...")
						window.location.replace(site_url+"/cDoors_Parents/add");
					}
				}
			);
		});

		$('#li-configuration').addClass('menu-open');
      	$('#ul-configuration').css('display', 'block');

		$('#li-doors').addClass('menu-open');
		$('#ul-doors').css('display', 'block');
	});
</script>
</body>
</html>
