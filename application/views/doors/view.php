<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-success">
					<div class="box-header ui-sortable-handle">
					    <i class="fa fa-sign-in"></i>
						<h3 class="box-title">Puerta</h3>
				  	</div>

				  	<div class="box-body">
			  			<section class="content">
			  			    <table id="doors" class="table">
		  		    		    <tbody>
		  		    		    	<tr>
		  		    		    		<th>Id</th>
		  			    				<td><?php echo $doors[0]['id'];?></td>
		  		    		    	</tr>
		  		    		    	<tr>
		  		    		    		<th>Puerta</th>
		  			    				<td><?php echo $doors[0]['door'];?></td>
		  		    		    	</tr>
		  		    		    	<tr>
		  		    	    			<th>Descripción</th>
		  		    	    			<td><?php echo $doors[0]['description'];?></td>
		  		    	    		</tr>
		  		    	    		<tr>
		  		    	    			<th>Nivel</th>
		  		    	    			<td><?php echo $doors[0]['level'];?></td>
		  		    	    		</tr>
		  		    	    		<tr>
		  		    	    			<th>Tipo puerta</th>
		  		    	    			<td><?php echo $doors[0]['type'];?></td>
		  		    	    		</tr>
		  		    	    		<tr>
		  		    	    			<th>Creado</th>
		  		    	    			<td><?php echo $doors[0]['created'];?></td>
		  		    	    		</tr>
		  		    	    		<tr>
		  		    	    			<th>Modificado</th>
		  		    	    			<td><?php echo $doors[0]['modified'];?></td>
		  		    	    		</tr>
		  		    		    </tbody>
		  			    	</table>
			  			</section>
				  	</div>
				  	<div class="box-footer">
				  		<a href="<?php echo site_url(); ?>/cDoors/" class="btn btn-primary pull-right" role="button">
	                    <i class='fa fa-undo'></i> Volver
	                </a>
				  	</div>
				</div>
			</div>
		</div>
	</section>
	
</div>

<?php $this->view('footer'); ?>

<script>
    $(document).ready(function() {

    	$('#li-configuration').addClass('menu-open');
      	$('#ul-configuration').css('display', 'block');
      	
      	$('#li-doors').addClass('menu-open');
		$('#ul-doors').css('display', 'block');
    });
</script>
</body>
</html>