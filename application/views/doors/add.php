<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-success">
					<div class="box-header ui-sortable-handle">
					    <i class="fa fa-sign-in"></i>
						<h3 class="box-title">Agregar puerta</h3>
				  	</div>

				  	<form class="form-horizontal" id="addDoors">
			  			<div class="box-body">
			  				<div class="form-group">
			  					<label for="doors" class="col-sm-2 control-label">Puerta</label>
			  					<div class="col-sm-10">
			  						<input type="text" class="form-control" name="doors" id="doors" required>
			  					</div>
			  				</div>
			  				<div class="form-group">
			  					<label for="description" class="col-sm-2 control-label">Descripción</label>
			  					<div class="col-sm-10">
			  						<input type="text" class="form-control" name="description" id="description">
			  					</div>
			  				</div>
			  				<div class="form-group">
			  					<button type="button" class="btn btn-default btn-xs" title="¿Qué es esto?" onclick="showMessageHelp();">?</button>
			  					<label for="level" class="col-sm-2 control-label">Nivel</label>
			  					<div class="col-sm-3">
			  						<input required type="number" class="form-control" name="level" id="level">
			  					</div>
			  				</div>
			  				<div class="form-group">
			  					<label for="doors_type" class="col-sm-2 control-label">Tipo Puerta</label>
			  					<div class="col-sm-5">
			  						<select name="doors_type" id="doors_type" class="form-control" required>
			  							<?php
			  								if(!empty($data)) { ?>
			  									<option value="">Seleccione una opción</option>
			  								<?php 
			  									foreach ($data as $key) { ?>
			  										<option value="<?php echo $key->id; ?>"><?php echo $key->type; ?></option>
			  							<?php } } ?>
			  						</select>
			  					</div>
			  				</div>
			  			</div>
			  			<div class="box-footer">
			  				<button type="submit" class="btn btn-primary pull-right">Guardar</button>
			  			</div>
			  		</form>
				</div>
			</div>
		</div>
	</section>
	
</div>

<?php $this->view('footer'); ?>

<script>
	$(document).ready(function() {
		$("#addDoors").submit(function(event) {
			event.preventDefault();

			$.post(
				site_url + "/cDoors/addDoors",{
					doors 		: $("#doors").val(),
					description : $("#description").val(),
					level 		: $("#level").val(),
					doors_type 	: $("#doors_type").val()
				},
				function(data){
					if (data == 1) {
						window.location.replace(site_url+"/cDoors/");
					}
					else {
						alert("Error en el proceso...")
						window.location.replace(site_url+"/cDoors/add");
					}
				}
			);
		});

		$('#li-configuration').addClass('menu-open');
      	$('#ul-configuration').css('display', 'block');

		$('#li-doors').addClass('menu-open');
		$('#ul-doors').css('display', 'block');
	});

	function showMessageHelp()
	{
		alert('El nivel permite llevar un orden y jerarquía de las puertas ingresadas al sistema.');
	}
</script>
</body>
</html>
