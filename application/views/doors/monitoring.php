<div class="content-wrapper">
	<section class="content">
    <div class="row">
      <div class="col-sm-12">
        <div class="box box-success">
          <div class="box-header ui-sortable-handle">
              <i class="fa fa-sign-in"></i>
            <h3 class="box-title">Monitoreo</h3>
            </div>

            <div class="box-body">
              <section class="content">

                  <table id="table-monitoring" class="table table-striped table-bordered table-condensed" style="width:100%;">
                    <thead>
                      <tr>
                        <th>ID</th>
                        <th>Puerta</th>
                        <th>Descripción</th>
                        <th>Sensores</th>
                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      if(!empty($doors))
                      {
                        foreach($doors as $d)
                        {
                          echo '<tr>';
                          echo '<td>'.$d['id'].'</td>';
                          echo '<td>'.$d['door'].'</td>';
                          echo '<td>'.$d['description'].'</td>';
                          echo '<td>';
                          echo '<table class="table" style="font-size:11px;">';
                          echo '<tbody id="tbody-sensors-'.$d['id'].'">';
                          foreach($d['sensors_info'] as $si)
                          {
                            echo '<tr>';
                            echo '<td>'.$si['sensor'].'</td>';
                            echo '<td class="td-ip">'.$si['ip'].'</td>';
                            echo '<td class="td-states-'.str_replace('.', '', $si['ip']).'" id="td-'.str_replace(".","",$si['ip']).'">cargando...</td>';
                            echo '<td><button onclick="openDoor(\''.$si['ip'].'\');" class="btn-states-'.str_replace('.', '', $si['ip']).' btn btn-success btn-xs" id="btn-'.str_replace(".","",$si['ip']).'"><i class="fa fa-hand-pointer-o" aria-hidden="true"></i></button></td>';
                            echo '</tr>';
                          }
                          echo '</tbody>';
                          echo '</table>';
                          echo '</td>';
                          
                          echo '</tr>';
                        }
                      }
                      ?>
                    </tbody>
                  </table>

              </section>
            </div>
            <div class="box-footer"></div>


        </div>
      </div>
    </div>
  </section>
	
</div>

<?php $this->view('footer'); ?>

<script>
    $(document).ready(function() {

      $('#table-monitoring').DataTable(
        { 
            'language': { "url": base_url + "assets/Spanish.json" },
        }
      );

      setInterval(getStatus, 3000);

      $('#li-configuration').addClass('menu-open');
      $('#ul-configuration').css('display', 'block');

      $('#li-doors').addClass('menu-open');
      $('#ul-doors').css('display', 'block');
    });

    function getStatus()
    {

      var td_state = document.getElementsByClassName('td-state');
      var td_ip = document.getElementsByClassName('td-ip');

      for(var i=0; i < td_ip.length;i++)
      {
        var ip = $(td_ip[i]).html();
        
        $.ajax({
          url: site_url+'/CDoors/getStateHost',
          data: {ip: ip},
          type: 'post',
          dataType: 'json',
          success: function(data)
          {
            $('.td-states-'+data.ip).html(data.resp);
            if(data.state == '1')
              $('.btn-states-'+data.ip).attr(data.attr, true);
            
            else
              $('.btn-states-'+data.ip).attr(data.attr, false);
          }
        });
        
      }
    }

    function openDoor(ip)
    {
      var ip_ = ip.replace('.','').replace('.','').replace('.','');
      $('.btn-states-'+ip_).attr('disabled', true);

      $.ajax({
        url: 'http://'+ip+'/pulsadores/open.php',
        dataType: 'text',
        success: function(response)
        {
          if(response == 1)
          {
            $('.btn-states-'+ip_).attr('disabled', false);
          }
        }
      });
    }

</script>

</body>
</html>
