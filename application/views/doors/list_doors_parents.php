<div class="content-wrapper">
	<section class="content">
    <div class="row">
      <div class="col-sm-12">
        <div class="box box-success">
          <div class="box-header ui-sortable-handle">
              <i class="fa fa-sing-in"></i> 
             <h3 class="box-title">Puerta - Puerta Padre</h3>
          </div>

          <div class="box-body">
            <section class="content">
                <table id="Doors_Parents" class="table table-striped table-bordered table-condensed" style="width:100%;">
                    <thead>
                      <tr>
                        <th>Puerta</th>
                        <th>Puerta padre</th>
                        <th>Acción</th>
                      </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </section>
          </div>
          <div class="box-footer"></div>
        </div>
      </div>
    </div>
  </section>
	
</div>

<?php $this->view('footer'); ?>

<script>
  var edit = <?php echo $this->session->userdata('edit'); ?>;
  var del = <?php echo $this->session->userdata('del'); ?>;

    $(document).ready(function() {
      $('#Doors_Parents').DataTable({
          "lengthMenu": [[5, 10, 15, 20,], [5, 10, 15, 20]],
          'responsive': true,
          'paging': true,
          'info': true,
          'filter': true,
          'ordering': true,
          // 'stateSave': true,
          'processing':true,
          'serverSide':true,
          'language': {
            "url": base_url + "assets/Spanish.json"
          },
          "order": [[0, "asc"]],
          'ajax': {
            "url": site_url + "/cDoors_Parents/datatable",
            "type":"POST",
          },
          "columns": [
            { "data": "Puerta" },
            { "data": "Puerta padre" },
            { "data": "Acción" }
          ],
          "columnDefs": [
            {
              "targets": [0],
              "orderable": true,
              "render": function(data, type, row) {
                return row.doord
              }
            },
            {
              "targets": [1],
              "orderable": true,
              "render": function(data, type, row) {
                return row.doorp
              }
            },
            {
              "targets": [2],
              "orderable": false,
              "render": function(data, type, row) {
                return del == true ? `
                  <a href="#" class="btn btn-danger btn-xs" role="button" onclick="delDoor_Parent(`+row.doors_id+`,`+row.parent_id+`);">
                      <i class='fa fa-trash-o'></i> Eliminar
                  </a>` : '';
              }
            }
           ],
        });

      $('#li-configuration').addClass('menu-open');
      $('#ul-configuration').css('display', 'block');

      $('#li-doors').addClass('menu-open');
      $('#ul-doors').css('display', 'block');
    });

    function delDoor_Parent(doors_id, parent) {
      if (confirm('¡Seguro de eliminar!')) {
        $.post(
        site_url + "/cDoors_Parents/deleteDoor_Parent",{
          doors_id   :   doors_id,
          parent     :   parent
        },
        function(data){
          if (data == 1) {
            window.location.reload();
          }
          else {
            alert("Error en el proceso...")
          }
        }
      );
    }
  }
</script>

</body>
</html>
