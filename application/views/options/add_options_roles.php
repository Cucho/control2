<style type="text/css">
  @import url('https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');

  main {
    min-width: 320px;
    max-width: 1100px;
    padding: 20px;
    margin: 0 auto;
    background: #fff;
    font-size: 12px;
  }

  .tab-pane {
    display: none;
    padding: 20px 0 0;
    border-top: 1px solid #ddd;
  }

  #tab1, #tab2, #tab3, #tab4, #tab5, #tab6, #tab7, #tab8, #tab9, #tab10, #tab11{
    display: none;
  }

  label {
    display: inline-block;
    margin: 0 0 -1px;
    padding: 15px 25px;
    font-weight: 600;
    text-align: center;
    //color: #bbb;
    border: 1px solid transparent;
  }

  label:before {
    font-family: fontawesome;
    font-weight: normal;
    margin-right: 10px;
  }

  label:hover {
    color: #888;
    cursor: pointer;
  }

  input:checked + label {
    color: #555;
    border: 1px solid #ddd;
    border-top: 2px solid orange;
    border-bottom: 1px solid #fff;
  }

  #tab1:checked ~ #content1,
  #tab2:checked ~ #content2,
  #tab3:checked ~ #content3,
  #tab4:checked ~ #content4,
  #tab5:checked ~ #content5,
  #tab6:checked ~ #content6,
  #tab7:checked ~ #content7,
  #tab8:checked ~ #content8,
  #tab9:checked ~ #content9,
  #tab10:checked ~ #content10,
  #tab11:checked ~ #content11 {
    display: block;
  }
</style>
<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-success">
					<div class="box-header ui-sortable-handle">
					    <i class="fa fa-cog"></i>
						<h3 class="box-title">Vincular opciónes - rol</h3>
				  	</div>
					
				  	<form class="form-horizontal" id="addOption_Roles">
			  			<div class="box-body">
			  				<div class="form-group">
		  						<label for="rol" class="col-sm-2 control-label">Rol</label>
			  					<div class="col-sm-5">
			  						<select required class="form-control" name="rol" id="rol" required>
			  							<option value="">Seleccione una opción</option>
			  							<?php
		  									foreach($roles as $a) {
		  										echo '<option value="'.$a['id'].'">'.$a['rol'].'</option>';
		  									}
		  								?>
			  						</select>
			  					</div>
			  				</div>
			  				<hr>
			  				
		  					
					    	<div class="col-md-12">
					    		<a href="#" class="btn btn-primary" id="seleccion" onclick="seleccion(1);">Marcar todo</a>
					    	</div>
							
					    	<div class="col-md-12"><hr></div>

					    	<div class="row">
					    		<div class="col-md-12">

					    		<main>
					    			<input id="tab1" type="radio" name="tabs" checked>
		                            <label for="tab1">Configuración</label>

		                            <input id="tab2" type="radio" name="tabs">
		                            <label for="tab2">Autorización</label>

		                            <input id="tab3" type="radio" name="tabs">
		                            <label for="tab3">Informes</label>

		                            <input id="tab4" type="radio" name="tabs">
		                            <label for="tab4">Internos</label>
		                              
		                            <input id="tab5" type="radio" name="tabs">
		                            <label for="tab5">Visitas</label>

		                            <input id="tab6" type="radio" name="tabs">
		                            <label for="tab6">Contratistas</label>

		                            <input id="tab7" type="radio" name="tabs">
		                            <label for="tab7">Empresas</label>

		                            <input id="tab8" type="radio" name="tabs">
		                            <label for="tab8">Personas</label>

		                            <input id="tab9" type="radio" name="tabs">
		                            <label for="tab9">Formularios</label>

		                            <input id="tab10" type="radio" name="tabs">
		                            <label for="tab10">Vehículos</label>

		                            <input id="tab11" type="radio" name="tabs">
		                            <label for="tab11">Bitácora</label>

		                            <div id="content1" class="tab-pane">
					    				<?php
						    				$this->db->select('id, code, option');
						    				$this->db->from('options');
						    				$this->db->like('code','01','after');
						    				$this->db->order_by('code', 'asc');
						    				$res = $this->db->get()->result_array();
						    				if(!empty($res))
						    				{
						    					foreach($res as $r)
						    					{?>

						    					<div class="checkbox col-sm-5">
											    	<label>
											      		<input type="checkbox" name="<?php echo $r['id'] ?>" value="<?php echo $r['id'] ?>"> <?php echo $r['code'].' | '.$r['option']; ?>
											    	</label>
										    	</div>

						    					<?php
						    					}
						    				}
					    				?>
					    			</div>

					    			<div id="content2" class="tab-pane">
					    				<?php
						    				$this->db->select('id, code, option');
						    				$this->db->from('options');
						    				$this->db->like('code','02','after');
						    				$this->db->order_by('code', 'asc');
						    				$res = $this->db->get()->result_array();
						    				if(!empty($res))
						    				{
						    					foreach($res as $r)
						    					{?>

						    					<div class="checkbox col-sm-5">
											    	<label>
											      		<input type="checkbox" name="<?php echo $r['id'] ?>" value="<?php echo $r['id'] ?>"> <?php echo $r['code'].' | '.$r['option']; ?>
											    	</label>
										    	</div>

						    					<?php
						    					}
						    				}
					    				?>
					    			</div>

					    			<div id="content3" class="tab-pane">
					    				<?php
						    				$this->db->select('id, code, option');
						    				$this->db->from('options');
						    				$this->db->like('code','03','after');
						    				$this->db->order_by('code', 'asc');
						    				$res = $this->db->get()->result_array();
						    				if(!empty($res))
						    				{
						    					foreach($res as $r)
						    					{?>

						    					<div class="checkbox col-sm-5">
											    	<label>
											      		<input type="checkbox" name="<?php echo $r['id'] ?>" value="<?php echo $r['id'] ?>"> <?php echo $r['code'].' | '.$r['option']; ?>
											    	</label>
										    	</div>

						    					<?php
						    					}
						    				}
					    				?>
					    			</div>

					    			<div id="content4" class="tab-pane">
					    				<?php
						    				$this->db->select('id, code, option');
						    				$this->db->from('options');
						    				$this->db->like('code','04','after');
						    				$this->db->order_by('code', 'asc');
						    				$res = $this->db->get()->result_array();
						    				if(!empty($res))
						    				{
						    					foreach($res as $r)
						    					{?>

						    					<div class="checkbox col-sm-5">
											    	<label>
											      		<input type="checkbox" name="<?php echo $r['id'] ?>" value="<?php echo $r['id'] ?>"> <?php echo $r['code'].' | '.$r['option']; ?>
											    	</label>
										    	</div>

						    					<?php
						    					}
						    				}
					    				?>
					    			</div>

					    			<div id="content5" class="tab-pane">
					    				<?php
						    				$this->db->select('id, code, option');
						    				$this->db->from('options');
						    				$this->db->like('code','05','after');
						    				$this->db->order_by('code', 'asc');
						    				$res = $this->db->get()->result_array();
						    				if(!empty($res))
						    				{
						    					foreach($res as $r)
						    					{?>

						    					<div class="checkbox col-sm-5">
											    	<label>
											      		<input type="checkbox" name="<?php echo $r['id'] ?>" value="<?php echo $r['id'] ?>"> <?php echo $r['code'].' | '.$r['option']; ?>
											    	</label>
										    	</div>

						    					<?php
						    					}
						    				}
					    				?>
					    			</div>

					    			<div id="content6" class="tab-pane">
					    				<?php
						    				$this->db->select('id, code, option');
						    				$this->db->from('options');
						    				$this->db->like('code','06','after');
						    				$this->db->order_by('code', 'asc');
						    				$res = $this->db->get()->result_array();
						    				if(!empty($res))
						    				{
						    					foreach($res as $r)
						    					{?>

						    					<div class="checkbox col-sm-5">
											    	<label>
											      		<input type="checkbox" name="<?php echo $r['id'] ?>" value="<?php echo $r['id'] ?>"> <?php echo $r['code'].' | '.$r['option']; ?>
											    	</label>
										    	</div>

						    					<?php
						    					}
						    				}
					    				?>
					    			</div>

					    			<div id="content7" class="tab-pane">
					    				<?php
						    				$this->db->select('id, code, option');
						    				$this->db->from('options');
						    				$this->db->like('code','07','after');
						    				$this->db->order_by('code', 'asc');
						    				$res = $this->db->get()->result_array();
						    				if(!empty($res))
						    				{
						    					foreach($res as $r)
						    					{?>

						    					<div class="checkbox col-sm-5">
											    	<label>
											      		<input type="checkbox" name="<?php echo $r['id'] ?>" value="<?php echo $r['id'] ?>"> <?php echo $r['code'].' | '.$r['option']; ?>
											    	</label>
										    	</div>

						    					<?php
						    					}
						    				}
					    				?>
					    			</div>

					    			<div id="content8" class="tab-pane">
					    				<?php
						    				$this->db->select('id, code, option');
						    				$this->db->from('options');
						    				$this->db->like('code','08','after');
						    				$this->db->order_by('code', 'asc');
						    				$res = $this->db->get()->result_array();
						    				if(!empty($res))
						    				{
						    					foreach($res as $r)
						    					{?>

						    					<div class="checkbox col-sm-5">
											    	<label>
											      		<input type="checkbox" name="<?php echo $r['id'] ?>" value="<?php echo $r['id'] ?>"> <?php echo $r['code'].' | '.$r['option']; ?>
											    	</label>
										    	</div>

						    					<?php
						    					}
						    				}
					    				?>
					    			</div>

					    			<div id="content9" class="tab-pane">
					    				<?php
						    				$this->db->select('id, code, option');
						    				$this->db->from('options');
						    				$this->db->like('code','09','after');
						    				$this->db->order_by('code', 'asc');
						    				$res = $this->db->get()->result_array();
						    				if(!empty($res))
						    				{
						    					foreach($res as $r)
						    					{?>

						    					<div class="checkbox col-sm-5">
											    	<label>
											      		<input type="checkbox" name="<?php echo $r['id'] ?>" value="<?php echo $r['id'] ?>"> <?php echo $r['code'].' | '.$r['option']; ?>
											    	</label>
										    	</div>

						    					<?php
						    					}
						    				}
					    				?>
					    			</div>

					    			<div id="content10" class="tab-pane">
					    				<?php
						    				$this->db->select('id, code, option');
						    				$this->db->from('options');
						    				$this->db->like('code','10','after');
						    				$this->db->order_by('code', 'asc');
						    				$res = $this->db->get()->result_array();
						    				if(!empty($res))
						    				{
						    					foreach($res as $r)
						    					{?>

						    					<div class="checkbox col-sm-5">
											    	<label>
											      		<input type="checkbox" name="<?php echo $r['id'] ?>" value="<?php echo $r['id'] ?>"> <?php echo $r['code'].' | '.$r['option']; ?>
											    	</label>
										    	</div>

						    					<?php
						    					}
						    				}
					    				?>
					    			</div>

					    			<div id="content11" class="tab-pane">
					    				<?php
						    				$this->db->select('id, code, option');
						    				$this->db->from('options');
						    				$this->db->like('code','11','after');
						    				$this->db->order_by('code', 'asc');
						    				$res = $this->db->get()->result_array();
						    				if(!empty($res))
						    				{
						    					foreach($res as $r)
						    					{?>

						    					<div class="checkbox col-sm-5">
											    	<label>
											      		<input type="checkbox" name="<?php echo $r['id'] ?>" value="<?php echo $r['id'] ?>"> <?php echo $r['code'].' | '.$r['option']; ?>
											    	</label>
										    	</div>

						    					<?php
						    					}
						    				}
					    				?>
					    			</div>

					    		</main>
					    		</div>
					    	</div>
						  	
			  			</div>
			  			<div class="box-footer">
			  				<button type="submit" class="btn btn-primary pull-right">Guardar</button>
			  			</div>
			  		</form>
				</div>
			</div>
		</div>
	</section>
	
</div>

<?php $this->view('footer'); ?>

<script>
	var opt = <?php echo json_encode($options); ?>;
	var opt_rol = <?php echo json_encode($options_roles); ?>;
	var options = [];

	$("select").on('change', function(){
		$("input:checkbox").each(function(index, el) {
			$(this).prop('checked', false)
		});

		for (var i = 0; i < opt_rol.length; i++) {
			if ($(this).val() == opt_rol[i].roles_id) {
				$('input:checkbox[name="'+opt_rol[i].options_id+'"]').prop('checked', true);
			}
		}
	})

	$(document).ready(function() {
		$("#addOption_Roles").submit(function(event) {
			event.preventDefault();

			$("input:checkbox").each(function(index, el) {
				if ($(this).prop('checked') == true) {
					options.push($(this).val())
				}
			});

			$.post(
				site_url + "/cOptions_Roles/addOption_Rol",{
					option 	: 	options,
					rol 	: 	$("#rol").val()
				},
				function(data){
					if (data == 1) {
						window.location.replace(site_url+"/cOptions_Roles/");
					}
					else {
						alert("Error en el proceso...")
						window.location.replace(site_url+"/cOptions_Roles/add");
					}
				}
			);
		});

		$('#li-configuration').addClass('menu-open');
      	$('#ul-configuration').css('display', 'block');
	});

	function seleccion(opcion){
		if (opcion == 1) {
			$("input:checkbox").each(function(index, el) {
				$(this).prop('checked', true)
			});
			$("#seleccion").attr('onclick', 'seleccion(0)');
			$("#seleccion").text('Desmarcar todo');
		}
		else if (opcion == 0) {
			$("input:checkbox").each(function(index, el) {
				$(this).prop('checked', false)
			});
			$("#seleccion").attr('onclick', 'seleccion(1)');
			$("#seleccion").text('Marcar todo');
		}
		
	}
</script>
</body>
</html>
