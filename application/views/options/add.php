<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-success">
					<div class="box-header ui-sortable-handle">
					    <i class="fa fa-cog"></i>
						<h3 class="box-title">Agregar opciones</h3>
				  	</div>

				  	<form class="form-horizontal" id="addOption">
			  			<div class="box-body">
			  				<div class="form-group">
			  					<label for="option" class="col-sm-2 control-label">Opción</label>
			  					<div class="col-sm-10">
			  						<input type="text" class="form-control" name="option" id="option" required>
			  					</div>
			  				</div>
			  			</div>
			  			<div class="box-body">
			  				<div class="form-group">
			  					<label for="code" class="col-sm-2 control-label">Código</label>
			  					<div class="col-sm-10">
			  						<input type="text" class="form-control" name="code" id="code" required>
			  					</div>
			  				</div>
			  			</div>
			  			<div class="box-footer">
			  				<button type="submit" class="btn btn-primary pull-right">Guardar</button>
			  			</div>
			  		</form>
				</div>
			</div>
		</div>
	</section>
	
</div>

<?php $this->view('footer'); ?>

<script>
	$(document).ready(function() {
		$("#addOption").submit(function(event) {
			event.preventDefault();

			$.post(
				site_url + "/cOptions/addOption",{
					option 	: 	$("#option").val(),
					code 	: 	$("#code").val()
				},
				function(data){
					if (data == 1) {
						window.location.replace(site_url+"/cOptions/");
					}
					else {
						alert("Error en el proceso...")
						window.location.replace(site_url+"/cOptions/add");
					}
				}
			);
		});

		$('#li-configuration').addClass('menu-open');
      	$('#ul-configuration').css('display', 'block');
	});
</script>
</body>
</html>
