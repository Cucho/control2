<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-success">
					<div class="box-header ui-sortable-handle">
					    <i class="fa fa-cog"></i>
						<h3 class="box-title">Editar opción #<?php echo $options[0]['id']; ?></h3>
				  	</div>
					
				  	<form class="form-horizontal" id="editOption">
			  			<div class="box-body">
			  				<div class="form-group">
			  					<label for="option" class="col-sm-2 control-label">Opción</label>
			  					<div class="col-sm-10">
			  						<input type="text" class="form-control" name="option" id="option" required value="<?php echo $options[0]['option'] ?>">
			  					</div>
			  				</div>
			  			</div>
			  			<div class="box-body">
			  				<div class="form-group">
			  					<label for="code" class="col-sm-2 control-label">Código</label>
			  					<div class="col-sm-10">
			  						<input type="text" class="form-control" name="code" id="code" required value="<?php echo $options[0]['code'] ?>">
			  					</div>
			  				</div>
			  			</div>
			  			<div class="box-footer">
			  				<button type="submit" class="btn btn-primary pull-right">Guardar</button>
			  			</div>
			  		</form>
				</div>
			</div>
		</div>
	</section>
	
</div>


<?php $this->view('footer'); ?>

<script>
	$(document).ready(function() {
		$("#editOption").submit(function(event) {
			event.preventDefault();

			$.post(
				site_url + "/cOptions/editOption",{
					id 		: 	<?php echo $options[0]['id']; ?>,
					option 	: 	$("#option").val(),
					code 	: 	$("#code").val()
				},
				function(data){
					if (data == 1) {
						window.location.replace(site_url+"/cOptions/");
					}
					else {
						alert("Error en el proceso...")
						window.location.replace(site_url+"/cOptions/edit?id="+<?php echo $options[0]['id']; ?>);
					}
				}
			);

			$('#li-configuration').addClass('menu-open');
      		$('#ul-configuration').css('display', 'block');
		});
	});
	
</script>
</body>
</html>
