<div class="content-wrapper">
	<section class="content">
    <div class="row">
      <div class="col-sm-12">
        <div class="box box-success">
          <div class="box-header ui-sortable-handle">
              <i class="fa fa-cog"></i>
              <h3 class="box-title">Opciones - Roles</h3>
          </div>

          <div class="box-body">
            <section class="content">
              <?php if ($this->session->userdata('save')) { ?>
                <a href="<?php echo site_url() ?>/cOptions_Roles/add" class="btn btn-primary">Agregar</a><br><hr>
              <?php } ?>
                <table id="options_roles" class="table table-striped table-bordered table-condensed" style="width:100%;">
                    <thead>
                      <tr>
                        <th>Rol</th>
                        <th>Opción</th>
                      </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </section>
          </div>
          <div class="box-footer"></div>
        </div>
      </div>
    </div>
  </section>
	
</div>

<?php $this->view('footer'); ?>

<script>
    $(document).ready(function() {
      $('#options_roles').DataTable({
          "lengthMenu": [[5, 10, 15, 20,], [5, 10, 15, 20]],
           'responsive': true,
          'paging': true,
          'info': true,
          'filter': true,
          'ordering': true,
          // 'stateSave': true,
          'processing':true,
          'serverSide':true,
          'language': {
            "url": base_url + "assets/Spanish.json"
          },
          "order": [[0, "asc"]],
          'ajax': {
            "url": site_url + "/cOptions_Roles/datatable",
            "type":"POST",
          },
          "columns": [
            { "data": "Rol" },
            { "data": "Opción" }
          ],
          "columnDefs": [
            {
              "targets": [0],
              "orderable": true,
              "render": function(data, type, row) {
                return row.rol
              }
            },
            {
              "targets": [1],
              "orderable": true,
              "render": function(data, type, row) {
                return row.option
              }
            },
            // {
            //   "targets": [2],
            //   "orderable": false,
            //   "render": function(data, type, row) {
            //     return `
            //       <a href="#" class="btn btn-danger btn-xs" role="button" onclick="delOption_Roles(`+row.options_id+`,`+row.roles_id+`);">
            //           <i class='fa fa-trash-o'></i> Eliminar
            //       </a>`
            //   }
            // }
           ],
        });


      $('#li-configuration').addClass('menu-open');
      $('#ul-configuration').css('display', 'block');
    });

    function delOption_Roles(option, rol) {
      if (confirm('¡Seguro de eliminar!')) {
        $.post(
        site_url + "/cOptions_Roles/deleteOption_Rol",{
          option  :   option,
          rol     :   rol
        },
        function(data){
          if (data == 1) {
            window.location.reload();
          }
          else {
            alert("Error en el proceso...")
          }
        }
      );
    }
  }
</script>

</body>
</html>
