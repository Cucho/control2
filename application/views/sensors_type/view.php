<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-success">
					<div class="box-header ui-sortable-handle">
					    <i class="fa fa-cogs"></i>
						<h3 class="box-title">Tipo sensor</h3>
				  	</div>
					
					<div class="box-body">
			  			<section class="content">
			  			    <table class="table">
		  		    		    <tbody>
		  		    		    	<tr>
		  		    		    		<th>Id</th>
		  			    				<td><?php echo $sensors_type[0]['id'];?></td>
		  		    		    	</tr>
		  		    		    	<tr>
		  		    		    		<th>Nombre</th>
		  			    				<td><?php echo $sensors_type[0]['type'];?></td>
		  		    		    	</tr>
		  		    		    </tbody>
		  			    	</table>
			  			</section>
				  	</div>
				  	<div class="box-footer">
				  		<a href="<?php echo site_url(); ?>/cSensors_Type/" class="btn btn-primary pull-right" role="button">
	                    <i class='fa fa-undo'></i> Volver
	                </a>
				  	</div>
				</div>
			</div>
		</div>
	</section>
	
</div>

<?php $this->view('footer'); ?>

<script>
    $(document).ready(function() {
    	$('#li-configuration').addClass('menu-open');
      	$('#ul-configuration').css('display', 'block');
      	
      	$('#li-sensors').addClass('menu-open');
		$('#ul-sensors').css('display', 'block');
    });
</script>
</body>
</html>