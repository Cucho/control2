<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-success">
					<div class="box-header ui-sortable-handle">
					    <i class="fa fa-cogs"></i>
						<h3 class="box-title">Editar tipo sensor #<?php echo $sensors_type[0]['id']; ?></h3>
				  	</div>
					<form class="form-horizontal" id="editSensor_Type">
			  			<div class="box-body">
			  				<div class="form-group">
			  					<label for="type" class="col-sm-2 control-label">Nombre</label>
			  					<div class="col-sm-10">
			  						<input type="text" class="form-control" name="type" id="type" value="<?php echo $sensors_type[0]['type']; ?>" required>
			  					</div>
			  			</div>
			  			<div class="box-footer">
			  				<button type="submit" class="btn btn-primary pull-right">Guardar</button>
			  			</div>
			  		</form>
				</div>
			</div>
		</div>
	</section>
	
</div>


<?php $this->view('footer'); ?>

<script>
	$(document).ready(function() {
		$("#editSensor_Type").submit(function(event) {
			event.preventDefault();

			$.post(
				site_url + "/cSensors_Type/editSensor_Type",{
					id 			: 	<?php echo $sensors_type[0]['id']; ?>,
					type 		: 	$("#type").val()
				},
				function(data){
					if (data == 1) {
						window.location.replace(site_url+"/cSensors_Type/");
					}
					else {
						alert("Error en el proceso...")
						window.location.replace(site_url+"/cSensors_Type/edit?id="+<?php echo $sensors_type[0]['id']; ?>);
					}
				}
			);

			$('#li-configuration').addClass('menu-open');
      		$('#ul-configuration').css('display', 'block');

			$('#li-sensors').addClass('menu-open');
			$('#ul-sensors').css('display', 'block');
		});
	});
	
</script>
</body>
</html>
