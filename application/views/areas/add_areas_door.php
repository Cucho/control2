<div class="content-wrapper">
	<br>
	<section class="content">
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-success">
					<div class="box-header ui-sortable-handle">
					    <i class="fa fa-briefcase"></i>
              			<i class="fa fa-sign-in"></i>
						<h3 class="box-title">Vincular Área - Puerta</h3>
				  	</div>

				  	<form class="form-horizontal" id="form-add-door_areas">
			  			<div class="box-body">
			  				<div class="form-group">
			  					<label for="area" class="col-sm-2 control-label">Área</label>
			  					<div class="col-sm-10">
			  						<select required class="form-control" name="area" id="area" required>
			  							<?php 
			  								if(!empty($areas)) { ?>
			  									<option value="">Seleccione una opción</option>
		  									<?php
			  									foreach($areas as $a) {
			  										echo '<option value="'.$a['id'].'">'.$a['area'].'</option>';
			  									}
			  								} ?>
			  						</select>
			  					</div>
			  				</div>
			  				<div class="form-group">
			  					<label for="door" class="col-sm-2 control-label">Puerta</label>
			  					<div class="col-sm-10">
			  						<select required class="form-control" name="door" id="door" required>
			  							<?php 
			  								if(!empty($doors)) { ?>
												<option value="">Seleccione una opción</option> 
											<?php
			  									foreach($doors as $d) {
			  										echo '<option value="'.$d['id'].'">'.$d['door'].'</option>';
			  									}
			  								} ?>
			  						</select>
			  					</div>
			  				</div>
			  			</div>
			  			<div class="box-footer">
			  				<button type="submit" class="btn btn-primary pull-right">Guardar</button>
			  			</div>
			  		</form>

				</div>
			</div>
		</div>
	</section>
</div>

<?php $this->view('footer'); ?>

<script>
	$(document).ready(function() {
		$("#form-add-door_areas").submit(function(event) {
			event.preventDefault();
			addArea_Door();
		});

		$('#li-configuration').addClass('menu-open');
      	$('#ul-configuration').css('display', 'block');

		$('#li-ubications').addClass('menu-open');
	    $('#ul-ubications').css('display', 'block');

	    $('#li-areas').addClass('menu-open');
	    $('#ul-areas').css('display', 'block');
	});

	function addArea_Door()
	{
		$.post(
			site_url + "/cAreas_Doors/addArea_Door",{
				area: $('#area').val(),
				door : 	$("#door").val()
			},
			function(data){
				if (data == 1) {
					window.location.replace(site_url+"/cAreas_Doors/");
				}
				else {
					alert('Se ha producido un error.\nVerifique que esta vinculación no exista.');
				}
			}
		);
	}
</script>
</body>
</html>
