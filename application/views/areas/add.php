<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-success">
					<div class="box-header ui-sortable-handle">
					    <i class="fa fa-briefcase"></i>
						<h3 class="box-title">Agregar Área</h3>
				  	</div>

				  	<form class="form-horizontal" id="addAreas">
			  			<div class="box-body">
			  				<div class="form-group">
			  					<label for="areas" class="col-sm-2 control-label">Área</label>
			  					<div class="col-sm-10">
			  						<input type="text" class="form-control" name="areas" id="areas" required>
			  					</div>
			  				</div>
			  				<div class="form-group">
			  					<label for="zones" class="col-sm-2 control-label">Zona</label>
			  					<div class="col-sm-5">
			  						<select name="zones" id="zones" class="form-control" required>
			  							<option value="">Seleccione una opción</option>
			  							<?php foreach ($data as $key) { ?>
			  								<option value="<?php echo $key->id; ?>"><?php echo $key->zone; ?></option>
			  							<?php } ?>
			  						</select>
			  					</div>
			  				</div>
			  			</div>
			  			<div class="box-footer">
			  				<button type="submit" class="btn btn-primary pull-right">Guardar</button>
			  			</div>
			  		</form>
				</div>
			</div>
		</div>
	</section>
	
</div>

<?php $this->view('footer'); ?>

<script>
	$(document).ready(function() {
		$("#addAreas").submit(function(event) {
			event.preventDefault();

			$.post(
				site_url + "/cAreas/addAreas",{
					area : 	$("#areas").val(),
					zones_id : $("#zones").val()
				},
				function(data){
					if (data == 1) {
						window.location.replace(site_url+"/cAreas/");
					}
					else {
						alert("Error en el proceso...")
						window.location.replace(site_url+"/cAreas/add");
					}
				}
			);
		});

		$('#li-configuration').addClass('menu-open');
      	$('#ul-configuration').css('display', 'block');

		$('#li-ubications').addClass('menu-open');
	    $('#ul-ubications').css('display', 'block');

	    $('#li-areas').addClass('menu-open');
	    $('#ul-areas').css('display', 'block');
	});
</script>
</body>
</html>
