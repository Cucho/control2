<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-success">
					<div class="box-header ui-sortable-handle">
					    <i class="fa fa-cog"></i>
						<h3 class="box-title">Editar configuración #<?php echo $configurations[0]['id']; ?></h3>
				  	</div>

					<form class="form-horizontal" id="editConfigurations">
  			  			<div class="box-body">
			  				<div class="form-group">
			  					<label for="codec" class="col-sm-2 control-label">Código empresa</label>
			  					<div class="col-sm-3">
			  						<input type="number" class="form-control" name="codec" id="codec" value="<?php echo $configurations[0]['cod_company'] ?>" required>
			  					</div>
			  				</div>
			  				<div class="form-group">
			  					<label for="company" class="col-sm-2 control-label">Empresa</label>
			  					<div class="col-sm-10">
			  						<input type="text" class="form-control" name="company" id="company" value="<?php echo $configurations[0]['company'] ?>" required>
			  					</div>
			  				</div>
			  				<div class="form-group">
			  					<label for="cinstall" class="col-sm-2 control-label">Código instalación</label>
			  					<div class="col-sm-3">
			  						<input type="number" class="form-control" name="cinstall" id="cinstall" value="<?php echo $configurations[0]['cod_installation'] ?>" required>
			  					</div>
			  				</div>
			  				<div class="form-group">
			  					<label for="install" class="col-sm-2 control-label">Instalación</label>
			  					<div class="col-sm-10">
			  						<input type="text" class="form-control" name="install" id="install" value="<?php echo $configurations[0]['installation'] ?>" required>
			  					</div>
			  				</div>
			  				<div class="form-group">
			  					<label for="address" class="col-sm-2 control-label">Dirección</label>
			  					<div class="col-sm-10">
			  						<input type="text" class="form-control" name="address" id="address" value="<?php echo $configurations[0]['address'] ?>" required>
			  					</div>
			  				</div>
			  				<div class="form-group">
			  					<label for="email" class="col-sm-2 control-label">E-Mail</label>
			  					<div class="col-sm-10">
			  						<input type="email" class="form-control" name="email" id="email" value="<?php echo $configurations[0]['email'] ?>" required>
			  					</div>
			  				</div>
			  				<div class="form-group">
			  					<label for="phone" class="col-sm-2 control-label">Teléfono</label>
			  					<div class="col-sm-3">
			  						<input type="tel" class="form-control" name="phone" id="phone" value="<?php echo $configurations[0]['phone'] ?>" required>
			  					</div>
			  				</div>
			  				<div class="form-group">
			  					<label for="people" class="col-sm-2 control-label">Contacto</label>
			  					<div class="col-sm-5">
			  						<select name="people" id="people" class="form-control" required>
			  							<option value="">Seleccione una opción</option>
			  							<?php foreach ($people as $key) { ?>
			  								<option value="<?php echo $key->id; ?>"><?php echo $key->name.' '.$key->last_name; ?></option>
			  							<?php } ?>
			  						</select>
			  					</div>
			  				</div>
						</div>
			  			<div class="box-footer">
			  				<button type="submit" class="btn btn-primary pull-right">Guardar</button>
			  			</div>
			  		</form>
				</div>
			</div>
		</div>
	</section>

</div>

<?php $this->view('footer'); ?>

<script>

	
	$(document).ready(function() {
		$("#people").val(<?php echo $configurations[0]['people_id']; ?>)

		$("#editConfigurations").submit(function(event) {
			event.preventDefault();

			$.post(
				site_url + "/cConfigurations/editConfiguration",{
					id 					: 	<?php echo $configurations[0]['id']; ?>,
					cod_company			: 	$("#codec").val(),
					company 			: 	$("#company").val(),
					cod_installation	: 	$("#cinstall").val(),
					installation 		: 	$("#install").val(),
					address 			: 	$("#address").val(),
					email 				: 	$("#email").val(),
					phone 				: 	$("#phone").val(),
					people_id 			: 	$("#people").val()
				},
				function(data){
					if (data == 1) {
						window.location.replace(site_url+"/cConfigurations/");
					}
					else {
						alert("Error en el proceso...")
						window.location.replace(site_url+"/cConfigurations/edit?id="+<?php echo $configurations[0]['id']; ?>);
					}
				}
			);

			// $('#li-people').addClass('menu-open');
			// $('#ul-people').css('display', 'block');
		});
	});

	

</script>
</body>
</html>
