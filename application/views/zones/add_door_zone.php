<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-success">
					<div class="box-header ui-sortable-handle">
					    <i class="fa fa-briefcase"></i>
        			<i class="fa fa-sign-in"></i>
							<h3 class="box-title">Vincular Zona - Puerta</h3>
			  	</div>

				  	<form class="form-horizontal" id="form-add-door_zone">
			  			<div class="box-body">
			  				<div class="form-group">
			  					<label for="zone" class="col-sm-2 control-label">Zona</label>
			  					<div class="col-sm-5">
			  						<select required class="form-control" name="zone" id="zone">
												<option value="">Seleccione una opción</option>
			  							<?php
			  								if(!empty($zones))
			  								{
			  									foreach($zones as $z)
			  									{
			  										echo '<option value="'.$z['id'].'">'.$z['zone'].'</option>';
			  									}
			  								}
			  							?>
			  						</select>
			  					</div>
			  				</div>
			  				<div class="form-group">
			  					<label for="door" class="col-sm-2 control-label">Puerta</label>
			  					<div class="col-sm-5">
			  						<select required class="form-control" name="door" id="door">
												<option value="">Seleccione una opción</option>
			  							<?php
			  								if(!empty($doors))
			  								{
			  									foreach($doors as $d)
			  									{
			  										echo '<option value="'.$d['id'].'">'.$d['door'].'</option>';
			  									}
			  								}
			  							?>
			  						</select>
			  					</div>
			  				</div>
			  			</div>
			  			<div class="box-footer">
			  				<button type="submit" class="btn btn-primary pull-right">Guardar</button>
			  			</div>
			  		</form>

				</div>
			</div>
		</div>
	</section>
</div>

<?php $this->view('footer'); ?>

<script>
	$(document).ready(function() {
		$("#form-add-door_zone").submit(function(event) {
			event.preventDefault();
			addDoor_Zone();
		});

		$('#li-configuration').addClass('menu-open');
	    $('#ul-configuration').css('display', 'block');

	    $('#li-ubications').addClass('menu-open');
	    $('#ul-ubications').css('display', 'block');

	    $('#li-zones').addClass('menu-open');
	    $('#ul-zones').css('display', 'block');
	    
	});

	function addDoor_Zone()
	{
		$.post(
			site_url + "/cDoors_Zones/addDoor_Zone",{
				door: $('#door').val(),
				zone : 	$("#zone").val()
			},
			function(data){
				if (data == 1) {
					window.location.replace(site_url+"/cDoors_Zones/");
				}
				else {
					alert('Se ha producido un error.\nVerifique que esta vinculación no exista.');
				}
			}
		);
	}
</script>
</body>
</html>
