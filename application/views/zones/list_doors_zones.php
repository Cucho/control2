<div class="content-wrapper">
	<br>
  <section class="content">
  	<div class="row">
  		<div class="col-md-12">
  			<div class="box box-success">
  				<div class="box-header ui-sortable-handle">
  				    <i class="fa fa-briefcase"></i>
              <i class="fa fa-sign-in"></i>
  					<h3 class="box-title">Zonas - Puertas</h3>
  			  	</div>

  			  	<div class="box-body">
  		  			<section class="content">
                <?php if ($this->session->userdata('save')) { ?>
                  <a href="<?php echo site_url() ?>/cDoors_Zones/add" class="btn btn-primary">Agregar</a><br><hr>
                <?php } ?>
  		  			    <table id="Zones_Doors" class="table table-striped table-bordered table-condensed" style="width:100%;">
  	  		    		    <thead>
  	  		    			    <tr>
                          <th width="45%">Zona</th>
  	  		    			      <th width="45%">Puerta</th>
                          <th width="10">Acción</th>
  	  		    			    </tr>
  	  		    		    </thead>
  	  		    	    	<tbody></tbody>
  	  			    	</table>
  		  			</section>
  			  	</div>
  			  	<div class="box-footer"></div>
  			</div>
  		</div>
  	</div>
  </section>
</div>

<?php $this->view('footer'); ?>

<script>
  var edit = <?php echo $this->session->userdata('edit'); ?>;
  var del = <?php echo $this->session->userdata('del'); ?>;

    $(document).ready(function() {

      $('#Zones_Doors').DataTable({
          "lengthMenu": [[5, 10, 15, 20,], [5, 10, 15, 20]],
          'responsive': true,
          'paging': true,
          'info': true,
          'filter': true,
          'ordering': true,
          // 'stateSave': true,
          'processing':true,
          'serverSide':true,
          'language': {
            "url": base_url + "assets/Spanish.json"
          },
          "order": [[0, "asc"]],
          'ajax': {
            "url": site_url + "/cDoors_Zones/datatable",
            "type":"POST",
          },
          "columns": [
            { "data": "Zona" },
            { "data": "Puerta"},
            { "data": "Acción" }
          ],
          "columnDefs": [
            {
              "targets": [0],
              "orderable": true,
              "render": function(data, type, row) {
                return row.zone
              }
            },
            {
              "targets": [1],
              "orderable": true,
              "render": function(data, type, row) {
                return row.door
              }
            },
            {
              "targets": [2],
              "orderable": false,
              "render": function(data, type, row) {
                return del == true ? `
                  <a href="#" class="btn btn-danger btn-xs" role="button" onclick="delDoor_Zone(`+row.doors_id+`, `+row.zones_id+`);">
                    <i class='fa fa-trash-o'></i> Eliminar
                  </a>` : '';
              }
            }
           ],
        });
      
      $('#li-configuration').addClass('menu-open');
      $('#ul-configuration').css('display', 'block');

      $('#li-ubications').addClass('menu-open');
      $('#ul-ubications').css('display', 'block');

      $('#li-zones').addClass('menu-open');
      $('#ul-zones').css('display', 'block');
      
    });

    function delDoor_Zone(door, zone) {
      if (confirm('¡Seguro de eliminar!')) {
        $.post(
        site_url + "/cDoors_Zones/deleteDoor_Zone",{
        door  :   door,
        zone: zone
        },
        function(data){
          if (data == 1) {
            window.location.reload();
          }
          else {
            alert("Este registro posee dependencias asociadas.\nNo se puede eliminar.")
          }
        }
      );
    }
  }

</script>

</body>
</html>
