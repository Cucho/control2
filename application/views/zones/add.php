<div class="content-wrapper">
	<br>
	<section class="content">
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-success">
					<div class="box-header ui-sortable-handle">
					    <i class="fa fa-briefcase"></i>
						<h3 class="box-title">Agregar Zona</h3>
				  	</div>

				  	<form class="form-horizontal" id="form-add-zone">
			  			<div class="box-body">
			  				<div class="form-group">
			  					<label for="zone" class="col-sm-2 control-label">Zona</label>
			  					<div class="col-sm-10">
			  						<input type="text" class="form-control" name="zone" id="zone" required>
			  					</div>
			  				</div>
			  			</div>
			  			<div class="box-footer">
			  				<button type="submit" class="btn btn-primary pull-right">Guardar</button>
			  			</div>
			  		</form>

				</div>
			</div>
		</div>
	</section>
</div>

<?php $this->view('footer'); ?>

<script>
	$(document).ready(function() {
		$("#form-add-zone").submit(function(event) {
			event.preventDefault();
			addZone();
		});
		
		$('#li-configuration').addClass('menu-open');
	    $('#ul-configuration').css('display', 'block');

	    $('#li-ubications').addClass('menu-open');
	    $('#ul-ubications').css('display', 'block');

	    $('#li-zones').addClass('menu-open');
	    $('#ul-zones').css('display', 'block');
	});

	function addZone() {
		$.post(
			site_url + "/cZones/addZone",{
				zone : 	$("#zone").val()
			},
			function(data){
				if (data == 1) {
					window.location.replace(site_url+"/cZones/");
				}
				else {
					alert('Se ha producido un error.\nVerifique que no se repita la zona.');
				}
			}
		);
	}
</script>
</body>
</html>
