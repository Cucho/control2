<div class="content-wrapper">
	<br>
	<section class="content">
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-success">
					<div class="box-header ui-sortable-handle">
					    <i class="fa fa-briefcase"></i>
						<h3 class="box-title">Ver Zona</h3>
				  	</div>

				  	<div class="box-body">
			  			<section class="content">
			  			    <table id="Zones" class="table">
			  			    	<tr>
			  			    		<th>Id</th>
			  			    		<td><?php echo $zone[0]['id'];?></td>
			  			    	</tr>
			  			    	<tr>
			  			    		<th>Zona</th>
			  			    		<td><?php echo $zone[0]['zone'];?></td>
			  			    	</tr>
			  			    	<tr>
			  			    		<th>Creado</th>
			  			    		<td><?php echo $zone[0]['created'];?></td>
			  			    	</tr>
			  			    	<tr>
			  			    		<th>Modificado</th>
			  			    		<td><?php echo $zone[0]['modified'];?></td>
			  			    	</tr>
		  			    	</table>
			  			</section>
				  	</div>
				  	<div class="box-footer">
				  		<a href="<?php echo site_url(); ?>/cZones/" class="btn btn-primary pull-right" role="button">
	                    <i class='fa fa-undo'></i> Volver
	                </a>
				  	</div>
				</div>
			</div>
		</div>
	</section>
</div>

<?php $this->view('footer'); ?>

<script>
    $(document).ready(function() {
      	$('#li-configuration').addClass('menu-open');
	    $('#ul-configuration').css('display', 'block');

	    $('#li-ubications').addClass('menu-open');
	    $('#ul-ubications').css('display', 'block');

	    $('#li-zones').addClass('menu-open');
	    $('#ul-zones').css('display', 'block');
    });
</script>
</body>
</html>