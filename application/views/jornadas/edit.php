<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-success">
					<div class="box-header ui-sortable-handle">
					    <i class="fa fa-calendar"></i>
						<h3 class="box-title">Editar jornada #<?php echo $jornadas[0]['id']; ?></h3>
				  	</div>

				  	<form class="form-horizontal" id="editJornada">
			  			<div class="box-body">
			  				<div class="box-body">
				  				<div class="form-group">
				  					<label for="jornada" class="col-sm-2 control-label">Jornada</label>
				  					<div class="col-sm-5">
				  						<input type="text" class="form-control" name="jornada" id="jornada" required value="<?php echo $jornadas[0]['jornada'] ?>">
				  					</div>
				  				</div>
				  			</div>
				  			<div class="box-body">
				  				<div class="form-group">
				  					<label for="time_init" class="col-sm-2 control-label">Inicio</label>
				  					<div class="col-sm-3">
				  						<input type="time" class="form-control" name="time_init" id="time_init" required value="<?php echo $jornadas[0]['time_init'] ?>">
				  					</div>
				  				</div>
				  			</div>
				  			<div class="box-body">
				  				<div class="form-group">
				  					<label for="time_end" class="col-sm-2 control-label">Término</label>
				  					<div class="col-sm-3">
				  						<input type="time" class="form-control" name="time_end" id="time_end" required value="<?php echo $jornadas[0]['time_end'] ?>">
				  					</div>
				  				</div>
				  			</div>
			  			</div>
			  			<div class="box-footer">
			  				<button type="submit" class="btn btn-primary pull-right">Guardar</button>
			  			</div>
			  		</form>
				</div>
			</div>
		</div>
	</section>
	
</div>


<?php $this->view('footer'); ?>

<script>
	$(document).ready(function() {
		$("#editJornada").submit(function(event) {
			event.preventDefault();

			$.post(
				site_url + "/cJornadas/editJornada",{
					id 			: 	<?php echo $jornadas[0]['id']; ?>,
					jornada 	: 	$("#jornada").val(),
					time_init 	: 	$("#time_init").val(),
					time_end 	: 	$("#time_end").val()
				},
				function(data){
					if (data == 1) {
						window.location.replace(site_url+"/cJornadas/");
					}
					else {
						alert("Error en el proceso...")
						window.location.replace(site_url+"/cJornadas/edit?id="+<?php echo $jornadas[0]['id']; ?>);
					}
				}
			);

			$('#li-configuration').addClass('menu-open');
      		$('#ul-configuration').css('display', 'block');
		});
	});
	
</script>
</body>
</html>
