<div class="content-wrapper">
	<section class="content">
    <div class="row">
      <div class="col-sm-12">
        <div class="box box-success">
          <div class="box-header ui-sortable-handle">
              <i class="fa fa-exclamation-triangle"></i>
            <h3 class="box-title">Notificaciones</h3>
            </div>

            <div class="box-body">
              <section class="content">
                  <table id="notifications" class="table table-striped table-bordered table-condensed" style="width:100%;">
                      <thead>
                        <tr>
                          <th>ID</th>
                          <th>Notificación</th>
                          <th>Puerta</th>
                          <th>Sensor</th>
                          <th>Movimiento</th>
                          <th>Fecha</th>
                        </tr>
                      </thead>
                      <tbody></tbody>
                  </table>
              </section>
            </div>
            <div class="box-footer"></div>
        </div>
      </div>
    </div>
  </section>

</div>

<?php $this->view('footer'); ?>

<script>
    $(document).ready(function() {
      $('#notifications').DataTable({
          "lengthMenu": [[5, 10, 15, 20,], [5, 10, 15, 20]],
          'responsive': true,
          'paging': true,
          'info': true,
          'filter': true,
          'ordering': true,
          // 'stateSave': true,
          'processing':true,
          'serverSide':true,
          'language': {
            "url": base_url + "assets/Spanish.json"
          },
          "order": [[0, "desc"]],
          'ajax': {
            "url": site_url + "/cNotifications/datatable",
            "type":"POST",
          },
          "columns": [
            { "data": "ID"},
            { "data": "Notificación" },
            { "data": "Puerta" },
            { "data": "Sensor" },
            { "data": "Movimiento" },
            { "data": "Fecha" }
          ],
          "columnDefs": [
            {
              "targets": [0],
              "orderable": true,
              "render": function(data, type, row) {
                return row.id
              }
            },
            {
              "targets": [1],
              "orderable": true,
              "render": function(data, type, row) {
                return row.notification
              }
            },
            {
              "targets": [2],
              "orderable": true,
              "render": function(data, type, row) {
                return row.door
              }
            },
            {
              "targets": [3],
              "orderable": true,
              "render": function(data, type, row) {
                return row.sensor
              }
            },
            {
              "targets": [4],
              "orderable": true,
              "render": function(data, type, row) {
                if(row.entry == 1)
                  return 'Salida'
                else
                  return 'Entrada'
              }
            },
            {
              "targets": [5],
              "orderable": true,
              "render": function(data, type, row) {
                return row.created
              }
            }
            
           ],
        });
      $('#li-notifications').addClass('menu-open');
      $('#ul-notifications').css('display', 'block');
    });
</script>

</body>
</html>
