<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-success">
					<div class="box-header ui-sortable-handle">
					    <i class="fa fa-file-text"></i>
						<h3 class="box-title">Editar tipo respuesta #<?php echo $answers[0]['id']; ?></h3>
				  	</div>
					<form class="form-horizontal" id="editAnswer_Type">
			  			<div class="box-body">
			  				<div class="form-group">
			  					<label for="type" class="col-sm-2 control-label">Título</label>
			  					<div class="col-sm-10">
			  						<input type="text" class="form-control" name="type" id="type" required value="<?php echo $answers[0]['type'] ?>">
			  					</div>
			  				</div>
			  			</div>
			  			<div class="box-footer">
			  				<button type="submit" class="btn btn-primary pull-right">Guardar</button>
			  			</div>
			  		</form>
				</div>
			</div>
		</div>
	</section>
	
</div>

<?php $this->view('footer'); ?>

<script>
	$(document).ready(function() {
		$("#editAnswer_Type").submit(function(event) {
			event.preventDefault();

			$.post(
				site_url + "/cAnswers_Type/editAnswer_Type",{
					id 		: 	<?php echo $answers[0]['id']; ?>,
					type : 	$("#type").val(),
				},
				function(data){
					if (data == 1) {
						window.location.replace(site_url+"/cAnswers_Type/");
					}
					else {
						alert("Error en el proceso...")
						window.location.replace(site_url+"/cAnswers_Type/edit?id="+<?php echo $answers[0]['id']; ?>);
					}
				}
			);

			// $('#li-forms').addClass('menu-open');
			// $('#ul-forms').css('display', 'block');
		});
	});
	
</script>
</body>
</html>
