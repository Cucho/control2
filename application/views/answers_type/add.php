<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-success">
					<div class="box-header ui-sortable-handle">
					    <i class="fa fa-file-text"></i>
						<h3 class="box-title">Agregar tipo respuesta</h3>
				  	</div>

				  	<form class="form-horizontal" id="addAnswer_Type">
			  			<div class="box-body">
			  				<div class="form-group">
			  					<label for="type" class="col-sm-2 control-label">Tipo respuesta</label>
			  					<div class="col-sm-10">
			  						<input type="text" class="form-control" name="type" id="type" required>
			  					</div>
			  				</div>
			  			</div>
			  			<div class="box-footer">
			  				<button type="submit" class="btn btn-primary pull-right">Guardar</button>
			  			</div>
			  		</form>
				</div>
			</div>
		</div>
	</section>
	
</div>

<?php $this->view('footer'); ?>

<script>
	$(document).ready(function() {
		$("#addAnswer_Type").submit(function(event) {
			event.preventDefault();

			$.post(
				site_url + "/cAnswers_Type/addAnswer_Type",{
					type 		: 	$("#type").val(),
				},
				function(data){
					if (data == 1) {
						window.location.replace(site_url+"/cAnswers_Type/");
					}
					else {
						alert("Error en el proceso...")
						window.location.replace(site_url+"/cAnswers_Type/add");
					}
				}
			);
		});

		// $('#li-form').addClass('menu-open');
		// $('#ul-form').css('display', 'block');
	});
</script>
</body>
</html>
