<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-success">
					<div class="box-header ui-sortable-handle">
					    <i class="fa fa-sign-in"></i>
						<h3 class="box-title">Motivo visita</h3>
				  	</div>
					
					<div class="box-body">
			  			<section class="content">
			  			    <table class="table">
		  		    		    <tbody>
		  		    		    	<tr>
		  		    		    		<th>Id</th>
		  			    				<td><?php echo $reasons_visit[0]['id'];?></td>
		  		    		    	</tr>
		  		    		    	<tr>
		  		    		    		<th>Motivo visiata</th>
		  			    				<td><?php echo $reasons_visit[0]['reason'];?></td>
		  		    		    	</tr>
		  		    		    </tbody>
		  			    	</table>
			  			</section>
				  	</div>
				  	<div class="box-footer">
				  		<a href="<?php echo site_url(); ?>/cReasons_Visit/" class="btn btn-primary pull-right" role="button">
	                    <i class='fa fa-undo'></i> Volver
	                </a>
				  	</div>
				</div>
			</div>
		</div>
	</section>
	
</div>

<?php $this->view('footer'); ?>

<script>
    $(document).ready(function() {
		$('#li-visits').addClass('menu-open');
		$('#ul-visits').css('display', 'block');

		$('#li-reasons-visit').addClass('menu-open');
		$('#ul-reasons-visit').css('display', 'block');
    });
</script>
</body>
</html>