<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-success">
					<div class="box-header ui-sortable-handle">
					    <i class="fa fa-balance-scale"></i>
						<h3 class="box-title">Editar motivo visita #<?php echo $reasons_visit[0]['id']; ?></h3>
				  	</div>
				  	
					<form class="form-horizontal" id="editReasons_Visit">
			  			<div class="box-body">
			  				<div class="form-group">
			  					<label for="reason" class="col-sm-2 control-label">Motivo visita</label>
			  					<div class="col-sm-10">
			  						<input type="text" class="form-control" name="reason" id="reason" value="<?php echo $reasons_visit[0]['reason']; ?>" required>
			  					</div>
			  			</div>
			  			<div class="box-footer">
			  				<button type="submit" class="btn btn-primary pull-right">Guardar</button>
			  			</div>
			  		</form>
				</div>
			</div>
		</div>
	</section>
	
</div>


<?php $this->view('footer'); ?>

<script>
	$(document).ready(function() {
		$("#editReasons_Visit").submit(function(event) {
			event.preventDefault();

			$.post(
				site_url + "/cReasons_Visit/editReason_Visit",{
					id 			: 	<?php echo $reasons_visit[0]['id']; ?>,
					reason 		: 	$("#reason").val()
				},
				function(data){
					if (data == 1) {
						window.location.replace(site_url+"/cReasons_Visit/");
					}
					else {
						alert("Error en el proceso...")
						window.location.replace(site_url+"/cReasons_Visit/edit?id="+<?php echo $reasons_visit[0]['id']; ?>);
					}
				}
			);
		});
		$('#li-visits').addClass('menu-open');
		$('#ul-visits').css('display', 'block');

		$('#li-reasons-visit').addClass('menu-open');
		$('#ul-reasons-visit').css('display', 'block');
	});
	
</script>
</body>
</html>
