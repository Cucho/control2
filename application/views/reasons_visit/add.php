<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-success">
					<div class="box-header ui-sortable-handle">
					    <i class="fa fa-sign-in"></i>
						<h3 class="box-title">Agregar motivo visita</h3>
				  	</div>

				  	<form class="form-horizontal" id="addAReasons_visit">
			  			<div class="box-body">
			  				<div class="form-group">
			  					<label for="reason" class="col-sm-2 control-label">Motivo visita</label>
			  					<div class="col-sm-10">
			  						<input type="text" class="form-control" name="reason" id="reason" required>
			  					</div>
			  				</div>
			  			</div>
			  			<div class="box-footer">
			  				<button type="submit" class="btn btn-primary pull-right">Guardar</button>
			  			</div>
			  		</form>
				</div>
			</div>
		</div>
	</section>
	
</div>

<?php $this->view('footer'); ?>

<script>
	$(document).ready(function() {
		$("#addAReasons_visit").submit(function(event) {
			event.preventDefault();

			$.post(
				site_url + "/cReasons_Visit/addReason_Visit",{
					reason	: 	$("#reason").val(),
				},
				function(data){
					if (data == 1) {
						window.location.replace(site_url+"/cReasons_Visit/");
					}
					else {
						alert("Error en el proceso...")
						window.location.replace(site_url+"/cReasons_Visit/add");
					}
				}
			);
		});

		$('#li-visits').addClass('menu-open');
		$('#ul-visits').css('display', 'block');

		$('#li-reasons-visit').addClass('menu-open');
		$('#ul-reasons-visit').css('display', 'block');
	});
</script>
</body>
</html>
