<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-success">
					<div class="box-header ui-sortable-handle">
					    <i class="fa fa-cog"></i>
						<h3 class="box-title">Editar portería #<?php echo $main_access[0]['id']; ?></h3>
				  	</div>
					<form class="form-horizontal" id="editMain_Access">
			  			<div class="box-body">
			  				<div class="form-group">
			  					<label for="name" class="col-sm-2 control-label">Nombre</label>
			  					<div class="col-sm-10">
			  						<input type="text" class="form-control" name="name" id="name" value="<?php echo $main_access[0]['name']; ?>" required>
			  					</div>
			  				</div>
			  				<div class="form-group">
			  					<label for="ubication" class="col-sm-2 control-label">Ubicación</label>
			  					<div class="col-sm-10">
			  						<input type="text" class="form-control" name="ubication" id="ubication" value="<?php echo $main_access[0]['ubication']; ?>" required>
		  						</div>
			  				</div>
			  				<div class="form-group">
			  					<label for="ip_host" class="col-sm-2 control-label">Ip</label>
			  					<div class="col-sm-10">
			  						<input type="text" class="form-control" name="ip_host" id="ip_host" value="<?php echo $main_access[0]['ip_host']; ?>" required>
			  					</div>
			  				</div>
			  				<div class="form-group">
			  					<label for="name_host" class="col-sm-2 control-label">Host</label>
			  					<div class="col-sm-10">
			  						<input type="text" class="form-control" name="name_host" id="name_host" value="<?php echo $main_access[0]['name_host']; ?>" required>
			  					</div>
			  				</div>
			  				<div class="form-group">
			  					<label for="entry" class="col-sm-2 control-label">Movimiento</label>
									<div class="col-sm-10">
			  						<label class="radio-inline">
								  		<input type="radio" name="entry" id="entry1" value="0" required> Ingreso
										</label>
										<label class="radio-inline">
											<input type="radio" name="entry" id="entry2" value="1" required> Salida
										</label>
										<label class="radio-inline">
											<input type="radio" name="entry" id="entry3" value="2" required> Ambos
										</label>
			  					</div>
			  				</div>
			  				<div class="form-group">
			  					<label for="flow" class="col-sm-2 control-label">Flujo</label>
									<div class="col-sm-10">
			  						<label class="radio-inline">
								  		<input type="radio" name="flow" id="flow1" value="0" required> Personas
										</label>
										<label class="radio-inline">
											<input type="radio" name="flow" id="flow2" value="1" required> Vehiculos
										</label>
										<label class="radio-inline">
											<input type="radio" name="flow" id="flow3" value="2" required> Ambos
										</label>
			  					</div>
			  				</div>
			  				<div class="form-group">
			  					<label for="internal" class="col-sm-2 control-label">Permitido</label>
									<div class="col-sm-10">
			  						<label class="radio-inline">
								  		<input type="radio" name="internal" id="internal1" value="0" required> Interno
										</label>
										<label class="radio-inline">
											<input type="radio" name="internal" id="internal2" value="1" required> Externo
										</label>
										<label class="radio-inline">
											<input type="radio" name="internal" id="internal3" value="2" required> Ambos
										</label>
			  					</div>
			  				</div>
							<div class="form-group">
								<label for="state" class="col-sm-2 control-label">Estado</label>
								<div class="col-sm-10">
									<label class="radio-inline">
						  			<input type="radio" name="state" id="state1" value="1" required> Activo
									</label>
									<label class="radio-inline">
										<input type="radio" name="state" id="state2" value="0" required> No activo
									</label>
		  						</div>
		  					</div>
		  					<div class="form-group">
								<label for="state" class="col-sm-2 control-label">Pop ups</label>
								<div class="col-sm-10">
									<label class="radio-inline">
						  			<input type="radio" name="pop_up" id="pop_up1" value="1" required> Activos
									</label>
									<label class="radio-inline">
										<input type="radio" name="pop_up" id="pop_up2" value="0" required> No activos
									</label>
		  						</div>
		  					</div>

		  					<div class="form-group">
								<label for="door" class="col-sm-2 control-label">Puerta</label>
								<div class="col-sm-10">
									<select name="puerta" id="puerta" class="form-control" required>
										<option value="">Seleccione una puerta</option>
										<?php if (!empty($doors)){
											foreach ($doors as $k) { ?>
												<option value="<?php echo $k->id; ?>"><?php echo $k->door; ?></option>
											<?php }
										} ?>
									</select>
								</div>
		  					</div>
		  					<div class="form-group">
								<label for="main" class="col-sm-2 control-label">Principal</label>
								<div class="col-sm-10">
									<select name="main" id="main" class="form-control" required>
										<option value="1">Si</option>
										<option value="0">No</option>
									</select>
								</div>
		  					</div>

			  			</div>
			  			<div class="box-footer">
			  				<button type="submit" class="btn btn-primary pull-right">Guardar</button>
			  			</div>
			  		</form>
				</div>
			</div>
		</div>
	</section>

</div>


<?php $this->view('footer'); ?>

<script>
	$(document).ready(function() {

		$('input:radio[name="pop_up"][value="'+<?php echo $main_access[0]['pop_up']?>+'"]').prop('checked', true);
		$('input:radio[name="flow"][value="'+<?php echo $main_access[0]['flow']?>+'"]').prop('checked', true);
		$('input:radio[name="entry"][value="'+<?php echo $main_access[0]['entry']?>+'"]').prop('checked', true);
		$('input:radio[name="internal"][value="'+<?php echo $main_access[0]['internal']?>+'"]').prop('checked', true);
		$('input:radio[name="state"][value="'+<?php echo $main_access[0]['state']?>+'"]').prop('checked', true);

		$("#puerta").val('<?php echo $main_access[0]['doors_id']; ?>');
		$("#main").val('<?php echo $main_access[0]['main']; ?>');

		$("#editMain_Access").submit(function(event) {
			event.preventDefault();

			$.post(
				site_url + "/cMain_Access/editMain_Access",{
					id 			: 	<?php echo $main_access[0]['id']; ?>,
					name 		: 	$("#name").val(),
					ubication 	: 	$("#ubication").val(),
					ip_host 	: 	$("#ip_host").val(),
					name_host 	: 	$("#name_host").val(),
					entry 		: 	$('input:radio[name=entry]:checked').val(),
					flow 		: 	$('input:radio[name=flow]:checked').val(),
					internal 	: 	$('input:radio[name=internal]:checked').val(),
					state :	$('input:radio[name=state]:checked').val(),
					pop_up :	$('input:radio[name=pop_up]:checked').val(),
					doors 		: 	$("#puerta").val(),
					main 		: 	$("#main").val()
				},
				function(data){
					if (data == 1) {
						window.location.replace(site_url+"/cMain_Access/");
					}
					else {
						alert("Error en el proceso...")
						window.location.replace(site_url+"/cMain_Access/edit?id="+<?php echo $main_access[0]['id']; ?>);
					}
				}
			);
		});

		$('#li-configuration').addClass('menu-open');
      	$('#ul-configuration').css('display', 'block');

		$('#li-access').addClass('menu-open');
		$('#ul-access').css('display', 'block');
	});

</script>
</body>
</html>
