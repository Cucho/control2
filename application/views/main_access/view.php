<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-success">
					<div class="box-header ui-sortable-handle">
					    <i class="fa fa-cog"></i>
						<h3 class="box-title">Portería</h3>
				  	</div>
					<div class="box-body">
			  			<section class="content">
			  			    <table id="areas" class="table">
		  		    		    <tbody>
		  		    		    	<tr>
		  		    		    		<th>Id</th>
		  			    				<td><?php echo $main_access[0]['id'];?></td>
		  		    		    	</tr>
		  		    		    	<tr>
		  		    		    		<th>Nombre</th>
		  			    				<td><?php echo $main_access[0]['name'];?></td>
		  		    		    	</tr>
		  		    		    	<tr>
		  		    	    			<th>Ubicación</th>
		  		    	    			<td><?php echo $main_access[0]['ubication'];?></td>
		  		    	    		</tr>
		  		    	    		<tr>
		  		    	    			<th>Ip</th>
		  		    	    			<td><?php echo $main_access[0]['ip_host'];?></td>
		  		    	    		</tr>
		  		    	    		<tr>
		  		    	    			<th>Host</th>
		  		    	    			<td><?php echo $main_access[0]['name_host'];?></td>
		  		    	    		</tr>
		  		    	    		<tr>
		  		    	    			<th>Movimiento</th>
		  		    	    			<td>
		  		    	    				<?php
		  		    	    					switch ($main_access[0]['entry']) {
		  		    	    					 	case 0:
		  		    	    					 		echo 'Ingreso';
		  		    	    					 		break;
		  		    	    					 	case 1:
		  		    	    					 		echo 'Salida';
		  		    	    					 		break;
		  		    	    					 	case 2:
		  		    	    					 		echo 'Ambos';
		  		    	    					 		break;
		  		    	    					 } ?>
		  		    	    			</td>
		  		    	    		</tr>
		  		    	    		<tr>
		  		    	    			<th>Flujo</th>
		  		    	    			<td>
											<?php
		  		    	    					switch ($main_access[0]['flow']) {
		  		    	    					 	case 0:
		  		    	    					 		echo 'Persona';
		  		    	    					 		break;
		  		    	    					 	case 1:
		  		    	    					 		echo 'Vehículo';
		  		    	    					 		break;
		  		    	    					 	case 2:
		  		    	    					 		echo 'Ambos';
		  		    	    					 		break;
		  		    	    					 } ?>
										</td>
		  		    	    		</tr>
		  		    	    		<tr>
		  		    	    			<th>Permitido</th>
		  		    	    			<td>
											<?php
		  		    	    					switch ($main_access[0]['internal']) {
		  		    	    					 	case 0:
		  		    	    					 		echo 'Interno';
		  		    	    					 		break;
		  		    	    					 	case 1:
		  		    	    					 		echo 'Externo';
		  		    	    					 		break;
		  		    	    					 	case 2:
		  		    	    					 		echo 'Ambos';
		  		    	    					 		break;
		  		    	    					 } ?>
										</td>
		  		    	    		</tr>
									<tr>
		  		    	    			<th>Estado</th>
		  		    	    			<td>
											<?php
		  		    	    					switch ($main_access[0]['state']) {
		  		    	    					 	case 0:
		  		    	    					 		echo 'No activo';
		  		    	    					 		break;
		  		    	    					 	case 1:
		  		    	    					 		echo 'Activo';
		  		    	    					 		break;
		  		    	    					 } ?>
										</td>
		  		    	    		</tr>
		  		    	    		<tr>
		  		    	    			<th>Pop-ups</th>
		  		    	    			<td>
											<?php
		  		    	    					switch ($main_access[0]['pop_up']) {
		  		    	    					 	case 0:
		  		    	    					 		echo 'No activos';
		  		    	    					 		break;
		  		    	    					 	case 1:
		  		    	    					 		echo 'Activos';
		  		    	    					 		break;
		  		    	    					 } ?>
										</td>
		  		    	    		</tr>
		  		    	    		<tr>
		  		    	    			<th>Puerta</th>
		  		    	    			<td>
		  		    	    				<?php foreach ($doors as $k) {
		  		    	    					if ($k->id == $main_access[0]['doors_id']) {
		  		    	    						echo $main_access[0]['doors_id'].' - '.$k->door;
		  		    	    					}
		  		    	    				} ?>
		  		    	    			</td>
		  		    	    		</tr>
		  		    	    		<tr>
		  		    	    			<th>Principal</th>
		  		    	    			<td>
											<?php
		  		    	    					switch ($main_access[0]['main']) {
		  		    	    					 	case 0:
		  		    	    					 		echo 'No';
		  		    	    					 		break;
		  		    	    					 	case 1:
		  		    	    					 		echo 'Si';
		  		    	    					 		break;
		  		    	    					 } ?>
										</td>
		  		    	    		</tr>
		  		    	    		<tr>
		  		    	    			<th>Creado</th>
		  		    	    			<td><?php echo $main_access[0]['created'];?></td>
		  		    	    		</tr>
		  		    	    		<tr>
		  		    	    			<th>Modificado</th>
		  		    	    			<td><?php echo $main_access[0]['modified'];?></td>
		  		    	    		</tr>
		  		    		    </tbody>
		  			    	</table>
			  			</section>
				  	</div>
				  	<div class="box-footer">
				  		<a href="<?php echo site_url(); ?>/cMain_Access/" class="btn btn-primary pull-right" role="button">
	                    <i class='fa fa-undo'></i> Volver
	                </a>
				  	</div>
				</div>
			</div>
		</div>
	</section>

</div>

<?php $this->view('footer'); ?>

<script>
    $(document).ready(function() {

    	$('#li-configuration').addClass('menu-open');
      	$('#ul-configuration').css('display', 'block');
      	
      	$('#li-access').addClass('menu-open');
		$('#ul-access').css('display', 'block');
    });
</script>
</body>
</html>
