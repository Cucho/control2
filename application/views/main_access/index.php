<div class="content-wrapper">
	<section class="content">
    <div class="row">
      <div class="col-sm-12">
        <div class="box box-success">
          <div class="box-header ui-sortable-handle">
              <i class="fa fa-cog"></i>
            <h3 class="box-title">Porterías</h3>
            </div>

            <div class="box-body">
              <section class="content">
                <?php if ($this->session->userdata('save')) { ?>
                  <!-- <hr>-->
                  <a href="<?php echo site_url() ?>/cMain_Access/add" class="btn btn-primary">Agregar</a><br><hr>
                <?php } ?>
                  <div class="table-responsive">
                    <table id="main_access" class="table table-striped table-bordered table-condensed" style="width:100%;">
                        <thead>
                          <tr>
                            <th>ID</th>
                            <th>Nombre</th>
                            <th>Ubicación</th>
                            <th>Ip</th>
                            <th>Host</th>
                            <th>Movimiento</th>
                            <th>Flujo</th>
                            <th>Permitido</th>
                            <th>Estado</th>
                            <th>Pop-ups</th>
                            <th>Puerta</th>
                            <th>Principal</th>
                            <th>Acción</th>
                          </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                  </div>
              </section>
            </div>
            <div class="box-footer"></div>
        </div>
      </div>
    </div>
  </section>

</div>

<?php $this->view('footer'); ?>

<script>
  var edit = <?php echo $this->session->userdata('edit'); ?>;
  var del = <?php echo $this->session->userdata('del'); ?>;

    $(document).ready(function() {
      $('#main_access').DataTable({
          "lengthMenu": [[5, 10, 15, 20,], [5, 10, 15, 20]],
          'responsive': true,
          'paging': true,
          'info': true,
          'filter': true,
          'ordering': true,
          // 'stateSave': true,
          'processing':true,
          'serverSide':true,
          'language': {
            "url": base_url + "assets/Spanish.json"
          },
          "order": [[0, "asc"]],
          'ajax': {
            "url": site_url + "/cMain_Access/datatable",
            "type":"POST",
          },
          "columns": [
            { "data": "ID"},
            { "data": "Nombre" },
            { "data": "Ubicación" },
            { "data": "Ip" },
            { "data": "Host" },
            { "data": "Movimiento" },
            { "data": "Flujo" },
            { "data": "Permitido" },
            { "data": "Estado" },
            { "data": "Puerta" },
            { "data": "Principal" },
            { "data": "Acción" }
          ],
          "columnDefs": [
            {
              "targets": [0],
              "orderable": true,
              "render": function(data, type, row) {
                return row.id
              }
            },
            {
              "targets": [1],
              "orderable": true,
              "render": function(data, type, row) {
                return row.name
              }
            },
            {
              "targets": [2],
              "orderable": true,
              "render": function(data, type, row) {
                return row.ubication
              }
            },
            {
              "targets": [3],
              "orderable": true,
              "render": function(data, type, row) {
                return row.ip_host
              }
            },
            {
              "targets": [4],
              "orderable": true,
              "render": function(data, type, row) {
                return row.name_host
              }
            },
            {
              "targets": [5],
              "orderable": true,
              "render": function(data, type, row) {
                return function() {
									if (row.entry == 0) return 'Ingreso'
									else if (row.entry == 1) return 'Salida'
									else return 'Ing/Sal'
								}
              }
            },
            {
              "targets": [6],
              "orderable": true,
              "render": function(data, type, row) {
                return function() {
									if (row.flow == 0) return 'Personas'
									else if (row.flow == 1) return 'Vehiculos'
									else return 'Per/Veh'
								}
              }
            },
            {
              "targets": [7],
              "orderable": true,
              "render": function(data, type, row) {
                return function() {
									if (row.internal == 0) return 'Interno'
									else if (row.internal == 1) return 'Externo'
									else return 'Ambos'
								}
              }
            },
            {
              "targets": [8],
              "orderable": true,
              "render": function(data, type, row) {
                return row.state == 1 ? 'Activo' : 'No activo'
              }
            },
            {
              "targets": [9],
              "orderable": false,
              "render": function(data, type, row) {
                if(row.pop_up == 0)
                  return 'No Activos'
                else
                  return 'Activos'
              }
            },
            {
              "targets": [10],
              "orderable": false,
              "render": function(data, type, row) {
                return row.door
              }
            },
            {
              "targets": [11],
              "orderable": false,
              "render": function(data, type, row) {
                if(row.main == 0)
                  return 'No'
                else
                  return 'Si'
              }
            },
            {
              "targets": [12],
              "orderable": false,
              "render": function(data, type, row) {
                return edit == true && del == true ? `
                  <a href="<?php echo site_url(); ?>/cMain_Access/view?id=`+row.id+`" class="btn btn-primary btn-xs" role="button">
                    <i class='fa fa-search'></i> Ver
                  </a>
                  <a href="<?php echo site_url(); ?>/cMain_Access/edit?id=`+row.id+`" class="btn btn-warning btn-xs" role="button">
                      <i class='fa fa-pencil-square-o'></i> Editar
                  </a>
                  <a href="#" class="btn btn-danger btn-xs" role="button" onclick="delMain_Access(`+row.id+`);">
                      <i class='fa fa-trash-o'></i> Eliminar
                  </a>` : edit == true && del == false ? `
                  <a href="<?php echo site_url(); ?>/cMain_Access/view?id=`+row.id+`" class="btn btn-primary btn-xs" role="button">
                    <i class='fa fa-search'></i> Ver
                  </a>
                  <a href="<?php echo site_url(); ?>/cMain_Access/edit?id=`+row.id+`" class="btn btn-warning btn-xs" role="button">
                      <i class='fa fa-pencil-square-o'></i> Editar
                  </a>` : edit == false && del == true ? `
                  <a href="<?php echo site_url(); ?>/cMain_Access/view?id=`+row.id+`" class="btn btn-primary btn-xs" role="button">
                    <i class='fa fa-search'></i> Ver
                  </a>
                  <a href="#" class="btn btn-danger btn-xs" role="button" onclick="delMain_Access(`+row.id+`);">
                      <i class='fa fa-trash-o'></i> Eliminar
                  </a>` : edit == false && del == false ? `
                  <a href="<?php echo site_url(); ?>/cMain_Access/view?id=`+row.id+`" class="btn btn-primary btn-xs" role="button">
                    <i class='fa fa-search'></i> Ver
                  </a>` : '';
              }
            }
           ],
        });

      $('#li-configuration').addClass('menu-open');
      $('#ul-configuration').css('display', 'block');
      
      $('#li-access').addClass('menu-open');
      $('#ul-access').css('display', 'block');
    });

    function delMain_Access(id) {
      if (confirm('¡Seguro de eliminar!')) {
        $.post(
	        site_url + "/cMain_Access/deleteMain_Access",{
	        	id  :   id
	        },
	        function(data){
	          if (data == 1) {
	            window.location.reload();
	          }
	          else {
	            alert("Este registro posee dependencias asociadas.\nNo se puede eliminar.")
	          }
	        }
      	);
    	}
  	}
</script>

</body>
</html>
