<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-success">
					<div class="box-header ui-sortable-handle">
					    <i class="fa fa-users"></i>
						<h3 class="box-title">Agregar horario especial</h3>
				  	</div>
					
				  	<form class="form-horizontal" id="addSpecial_Schedule">
			  			<div class="box-body">

			  				<div class="form-group">
			  					<label for="reason" class="col-sm-2 control-label">Motivo</label>
			  					<div class="col-sm-5">
			  						<input type="text" class="form-control" name="reason" id="reason" required>
			  					</div>
			  				</div>

			  				<div class="form-group">
			  					<label for="people_id" class="col-sm-2 control-label">Persona</label>
			  					<div class="col-sm-5">
			  						<select name="people_id" id="people_id" class="form-control" required>
			  							<option value="">Seleccione una opción</option>
		  								<?php 
		  									foreach ($people as $key) { ?>
		  										<option value="<?php echo $key->id; ?>"><?php echo $key->name.' '.$key->last_name; ?></option>
			  							<?php } ?>
			  						</select>
			  					</div>
			  				</div>
			  				<div class="form-group">
			  					<label for="doors_id" class="col-sm-2 control-label">Puerta</label>
			  					<div class="col-sm-5">
			  						<select name="doors_id" id="doors_id" class="form-control" required>
			  							<option value="">Seleccione una opción</option>
		  								<?php 
		  									foreach ($doors as $key) { ?>
		  										<option value="<?php echo $key->id; ?>"><?php echo $key->door; ?></option>
			  							<?php } ?>
			  						</select>
			  					</div>
			  				</div>
			  				<div class="form-group">
			  					<label for="date_init" class="col-sm-2 control-label">Inicio</label>
			  					<div class="col-sm-5">
			  						<input type="datetime-local" class="form-control" name="date_init" id="date_init" required>
			  					</div>
			  				</div>
			  				<div class="form-group">
			  					<label for="date_end" class="col-sm-2 control-label">Término</label>
			  					<div class="col-sm-5">
			  						<input type="datetime-local" class="form-control" name="date_end" id="date_end" required>
			  					</div>
			  				</div>
			  			</div>
			  			<div class="box-footer">
			  				<button type="submit" class="btn btn-primary pull-right">Guardar</button>
			  			</div>
			  		</form>
				</div>
			</div>
		</div>
	</section>
	
</div>

<?php $this->view('footer'); ?>

<script>
	$(document).ready(function() {
		$("#addSpecial_Schedule").submit(function(event) {
			event.preventDefault();

			$.post(
				site_url + "/cSpecial_Schedule/addSpecial_Schedule",{
					people_id 	: 	$("#people_id").val(),
					doors_id 	: 	$("#doors_id").val(),
					date_init 	: 	$("#date_init").val(),
					date_end 	: 	$("#date_end").val(),
					reason		:   $('#reason').val()
				},
				function(data){
					if (data == 1) {
						window.location.replace(site_url+"/cSpecial_Schedule/");
					}
					else {
						alert("Error en el proceso...")
						window.location.replace(site_url+"/cSpecial_Schedule/add");
					}
				}
			);
		});

		$('#li-people').addClass('menu-open');
      	$('#ul-people').css('display', 'block');
	});
</script>
</body>
</html>
