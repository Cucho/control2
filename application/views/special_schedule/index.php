<div class="content-wrapper">
	<section class="content">
    <div class="row">
      <div class="col-sm-12">
        <div class="box box-success">
          <div class="box-header ui-sortable-handle">
              <i class="fa fa-users"></i>
              <h3 class="box-title">Horarios especiales</h3>
          </div>

          <div class="box-body">
            <section class="content">
              <?php if ($this->session->userdata('save')) { ?>
                <a href="<?php echo site_url() ?>/cSpecial_Schedule/add" class="btn btn-primary">Agregar</a><br><hr>
              <?php } ?>
                <table id="special_schedule" class="table table-striped table-bordered table-condensed" style="width:100%;">
                    <thead>
                      <tr>
                        <th width="10%">ID</th>
                        <th>Motivo</th>
                        <th>Persona</th>
                        <th>Puerta</th>
                        <th>Inicio</th>
                        <th>Término</th>
                        <th>Creado</th>
                      </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </section>
          </div>
          <div class="box-footer"></div>
        </div>
      </div>
    </div>
  </section>
	
</div>

<?php $this->view('footer'); ?>

<script>
    $(document).ready(function() {
      $('#special_schedule').DataTable({
          "lengthMenu": [[5, 10, 15, 20,], [5, 10, 15, 20]],
          'responsive': true,
          'paging': true,
          'info': true,
          'filter': true,
          'ordering': true,
          // 'stateSave': true,
          'processing':true,
          'serverSide':true,
          'language': {
            "url": base_url + "assets/Spanish.json"
          },
          "order": [[0, "asc"]],
          'ajax': {
            "url": site_url + "/cSpecial_Schedule/datatable",
            "type":"POST",
          },
          "columns": [
            { "data": "ID"},
            { "data": "Motivo" },
            { "data": "Persona" },
            { "data": "Puerta" },
            { "data": "Inicio" },
            { "data": "Término" },
            { "data": "Creado" }
          ],
          "columnDefs": [
            {
              "targets": [0],
              "orderable": true,
              "render": function(data, type, row) {
                return row.scid
              }
            },
            {
              "targets": [1],
              "orderable": false,
              "render": function(data, type, row) {
                return row.reason
              }
            },
            {
              "targets": [2],
              "orderable": true,
              "render": function(data, type, row) {
                return row.rut+'-'+row.digit+' '+row.name+' '+row.last_name
              }
            },
            {
              "targets": [3],
              "orderable": true,
              "render": function(data, type, row) {
                return row.door
              }
            },
            {
              "targets": [4],
              "orderable": true,
              "render": function(data, type, row) {
                return row.date_init
              }
            },
            {
              "targets": [5],
              "orderable": true,
              "render": function(data, type, row) {
                return row.date_end
              }
            },
            {
              "targets": [6],
              "orderable": true,
              "render": function(data, type, row) {
                return row.created
              }
            }
           ],
        });
      $('#li-people').addClass('menu-open');
      $('#ul-people').css('display', 'block');
    });

    
</script>

</body>
</html>
