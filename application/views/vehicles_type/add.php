<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-success">
					<div class="box-header ui-sortable-handle">
					    <i class="fa fa-car"></i>
						<h3 class="box-title">Agregar tipo vehículo</h3>
				  	</div>

				  	<form class="form-horizontal" id="addVehicles_Type">
			  			<div class="box-body">
			  				<div class="form-group">
			  					<label for="vehicles_type" class="col-sm-2 control-label">Tipo vehículo</label>
			  					<div class="col-sm-10">
			  						<input type="text" class="form-control" name="vehicles_type" id="vehicles_type" required>
			  					</div>
			  				</div>
			  			</div>
			  			<div class="box-footer">
			  				<button type="submit" class="btn btn-primary pull-right">Guardar</button>
			  			</div>
			  		</form>
				</div>
			</div>
		</div>
	</section>
	
</div>

<?php $this->view('footer'); ?>

<script>
	$(document).ready(function() {
		$("#addVehicles_Type").submit(function(event) {
			event.preventDefault();

			$.post(
				site_url + "/cVehicles_Type/addVehicles_Type",{
					vehicles_type : 	$("#vehicles_type").val()
				},
				function(data){
					if (data == 1) {
						window.location.replace(site_url+"/cVehicles_Type/");
					}
					else {
						alert("Error en el proceso...")
						window.location.replace(site_url+"/cVehicles_Type/add");
					}
				}
			);
		});

		$('#li-vehicles').addClass('menu-open');
		$('#ul-vehicles').css('display', 'block');
	});
</script>
</body>
</html>
