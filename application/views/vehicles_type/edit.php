<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-success">
					<div class="box-header ui-sortable-handle">
					    <i class="fa fa-car"></i>
						<h3 class="box-title">Editar tipo vehículo #<?php echo $vehicles_type[0]['id']; ?></h3>
				  	</div>

				  	<form class="form-horizontal" id="addVehicles_type">
			  			<div class="box-body">
			  				<div class="form-group">
			  					<label for="vehicles_type" class="col-sm-2 control-label">Tipo vehículo</label>
			  					<div class="col-sm-10">
			  						<input type="text" class="form-control" name="vehicles_type" id="vehicles_type" required value="<?php echo $vehicles_type[0]['type'] ?>">
			  					</div>
			  				</div>
			  			</div>
			  			<div class="box-footer">
			  				<button type="submit" class="btn btn-primary pull-right">Guardar</button>
			  			</div>
			  		</form>
				</div>
			</div>
		</div>
	</section>
	
</div>


<?php $this->view('footer'); ?>

<script>
	$(document).ready(function() {
		$("#addVehicles_type").submit(function(event) {
			event.preventDefault();

			$.post(
				site_url + "/cVehicles_type/editVehicles_type",{
					id 		: 	<?php echo $vehicles_type[0]['id']; ?>,
					vehicles_type : 	$("#vehicles_type").val()
				},
				function(data){
					if (data == 1) {
						window.location.replace(site_url+"/cVehicles_type/");
					}
					else {
						alert("Error en el proceso...")
						window.location.replace(site_url+"/cVehicles_type/edit?id="+<?php echo $vehicles_type[0]['id']; ?>);
					}
				}
			);

			$('#li-vehicles').addClass('menu-open');
			$('#ul-vehicles').css('display', 'block');
		});
	});
	
</script>
</body>
</html>
