<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-success">
					<div class="box-header ui-sortable-handle">
					    <i class="fa fa-users"></i>
						<h3 class="box-title">Editar perfil usuario #<?php echo $people_profiles[0]['id']; ?></h3>
				  	</div>

				  	<form class="form-horizontal" id="editPeople_Profile">
			  			<div class="box-body">
			  				<div class="form-group">
			  					<label for="profile" class="col-sm-2 control-label">Perfil usuario</label>
			  					<div class="col-sm-10">
			  						<input type="text" class="form-control" name="profile" id="profile" required value="<?php echo $people_profiles[0]['profile'] ?>">
			  					</div>
			  				</div>
			  			</div>
			  			<div class="box-footer">
			  				<button type="submit" class="btn btn-primary pull-right">Guardar</button>
			  			</div>
			  		</form>
				</div>
			</div>
		</div>
	</section>
	
</div>


<?php $this->view('footer'); ?>

<script>
	$(document).ready(function() {
		$("#editPeople_Profile").submit(function(event) {
			event.preventDefault();

			$.post(
				site_url + "/cPeople_Profiles/editPeople_Profile",{
					id 		: 	<?php echo $people_profiles[0]['id']; ?>,
					profile : 	$("#profile").val()
				},
				function(data){
					if (data == 1) {
						window.location.replace(site_url+"/cPeople_Profiles/");
					}
					else {
						alert("Error en el proceso...")
						window.location.replace(site_url+"/cPeople_Profiles/edit?id="+<?php echo $people_profiles[0]['id']; ?>);
					}
				}
			);

		});

		$('#li-people').addClass('menu-open');
		$('#ul-people').css('display', 'block');
	});
	
</script>
</body>
</html>
