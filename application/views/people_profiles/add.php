<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-success">
					<div class="box-header ui-sortable-handle">
					    <i class="fa fa-users"></i>
						<h3 class="box-title">Agregar perfil usuario</h3>
				  	</div>

				  	<form class="form-horizontal" id="addPeople_Profile">
			  			<div class="box-body">
			  				<div class="form-group">
			  					<label for="profile" class="col-sm-2 control-label">Perfil usuario</label>
			  					<div class="col-sm-10">
			  						<input type="text" class="form-control" name="profile" id="profile" required>
			  					</div>
			  				</div>
			  			</div>
			  			<div class="box-footer">
			  				<button type="submit" class="btn btn-primary pull-right">Guardar</button>
			  			</div>
			  		</form>
				</div>
			</div>
		</div>
	</section>
	
</div>

<?php $this->view('footer'); ?>

<script>
	$(document).ready(function() {
		$("#addPeople_Profile").submit(function(event) {
			event.preventDefault();

			$.post(
				site_url + "/cPeople_Profiles/addPeople_Profile",{
					profile : 	$("#profile").val()
				},
				function(data){
					if (data == 1) {
						window.location.replace(site_url+"/cPeople_Profiles/");
					}
					else {
						alert("Error en el proceso...")
						window.location.replace(site_url+"/cPeople_Profiles/add");
					}
				}
			);
		});

		$('#li-people').addClass('menu-open');
		$('#ul-people').css('display', 'block');
	});
</script>
</body>
</html>
