<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-success">
					<div class="box-header ui-sortable-handle">
					    <i class="fa fa-users"></i>
						<h3 class="box-title">Editar rol #<?php echo $roles[0]['id']; ?></h3>
				  	</div>

				  	<form class="form-horizontal" id="editRol">
			  			<div class="box-body">
			  				<div class="form-group">
			  					<label for="rol" class="col-sm-2 control-label">Rol</label>
			  					<div class="col-sm-10">
			  						<input type="text" class="form-control" name="rol" id="rol" required value="<?php echo $roles[0]['rol'] ?>">
			  					</div>
			  				</div>
			  			</div>
			  			<div class="box-footer">
			  				<button type="submit" class="btn btn-primary pull-right">Guardar</button>
			  			</div>
			  		</form>
				</div>
			</div>
		</div>
	</section>
	
</div>


<?php $this->view('footer'); ?>

<script>
	$(document).ready(function() {
		$("#editRol").submit(function(event) {
			event.preventDefault();

			$.post(
				site_url + "/cRoles/editRol",{
					id 		: 	<?php echo $roles[0]['id']; ?>,
					rol : 	$("#rol").val()
				},
				function(data){
					if (data == 1) {
						window.location.replace(site_url+"/cRoles/");
					}
					else {
						alert("Error en el proceso...")
						window.location.replace(site_url+"/cRoles/edit?id="+<?php echo $roles[0]['id']; ?>);
					}
				}
			);
		});

		$('#li-configuration').addClass('menu-open');
      	$('#ul-configuration').css('display', 'block');
      	
		$('#li-users').addClass('menu-open');
		$('#ul-users').css('display', 'block');
	});
	
</script>
</body>
</html>
