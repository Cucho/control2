<style type="text/css">
  fieldset {
      border: 1px solid #ddd !important;
    margin: 0;
    xmin-width: 0;
    padding: 10px;
    position: relative;
    border-radius:4px;
    background-color:#fff;
    padding-left:10px!important;
  }
  legend
  {
    font-size:14px;
    font-weight:bold;
    margin-bottom: 0px;
    width: 35%;
    border: 1px solid #ddd;
    border-radius: 4px;
    padding: 5px 5px 5px 10px;
    background-color: #ffffff;
  }
</style>
<div class="content-wrapper">
	<section class="content">
    <div class="row">
      <div class="col-sm-12">
        <div class="box box-success">
          <div class="box-header ui-sortable-handle">
              <i class="fa fa-wpforms"></i>
              <h3 class="box-title">Informes</h3>
          </div>

          <div class="box-body">
            <section class="content">

                <div class="row">
                  <div class="col-md-12">

                    <div class="col-md-6">
                      <fieldset>
                        <legend>Tipo <!-- <input type="radio" name="type_profile" value="1">--></legend>
                          <input onclick="checkRadioType();" type="radio" name="type" value="1"> Empresa<br>
                          <input onclick="checkRadioType();" type="radio" name="type" value="0"> Persona<br>
                          <input onclick="checkRadioType();" type="radio" name="type" value="2"> Vehículo<br>

                      </fieldset>
                    </div>
                    <!--
                    <div class="col-md-4">
                      <fieldset>
                        <legend>Perfil <input type="radio" name="type_profile" value="2"></legend>
                          <input type="radio" name="profile" value="1"> Interno<br>
                          <input type="radio" name="profile" value="0"> Visita<br>
                          <input type="radio" name="profile" value="2"> Contratista
                      </fieldset>
                    </div>
                    -->
                    <div class="col-md-6">
                      <fieldset>
                        <legend>Fecha</legend>
                          <input onclick="checkRadioDate();" type="radio" name="date" value="2"> Intervalo<br>
                          <input onclick="checkRadioDate();" type="radio" name="date" value="0"> Mes<br>
                          <input onclick="checkRadioDate();" type="radio" name="date" value="1"> Fecha
                      </fieldset>
                    </div>

                  </div>
                </div>
                <br>
                <div class="row">
                  <div class="col-md-12">

                    <div class="col-md-6">
                      <select onchange="loadSelects();" id="select-company" class="form-control">
                        <option value="0">Seleccione empresa</option>
                        <?php
                        if(!empty($companies))
                        {
                          foreach($companies as $c)
                          {
                            echo '<option value="'.$c['id'].'">'.$c['company'].'</option>';
                          }
                        }
                        ?>
                      </select>
                      <br>
                      <select disabled id="select-people" class="form-control">
                        <option value="0">Seleccione persona</option>
                        <?php
                        if(!empty($people))
                        {
                          foreach($people as $p)
                          {
                            echo '<option value="'.$p['id'].'">'.$p['rut'].'-'.$p['digit'].' | '.$p['name'].' '.$p['last_name'].'</option>';
                          }
                        }
                        ?>
                      </select>
                      <br>
                      <select disabled id="select-vehicles" class="form-control">
                        <option value="0">Seleccione vehículo</option>
                        <?php
                        if(!empty($vehicles))
                        {
                          foreach($vehicles as $v)
                          {
                            echo '<option value="'.$v['id'].'">'.$v['patent'].'</option>';
                          }
                        }
                        ?>
                      </select>
                    </div>

                    <!--
                    <div class="col-md-4">

                    </div>
                    -->
                    <div class="col-md-6" id="div-date">

                    </div>

                  </div>
                </div>
                <br>
                <button onclick="generateReport();" class="btn btn-primary">Generar</button>
                <span></span>
                <br><br>
                <div class="row">
                  <div class="col-md-12">

                    <div class="table-responsive" id="div-table_reports">

                      <table id="table-reports" class="table table-bordered table-striped">
                        <thead>
                          <tr>
                            <th>ID</th>
                            <th>Rut</th>
                            <th>Nombre</th>
                            <th>Perfil</th>
                            <th>Puerta</th>
                            <th>Estado</th>
                            <th>Movimiento</th>
                            <th>Tipo</th>
                            <th>Vehículo</th>
                            <th>Fecha</th>
                          </tr>
                        </thead>
                        <tbody id="tbody-reports">

                        </tbody>
                      </table>

                    </div>
                  </div>
                </div>

            </section>
          </div>
          <div class="box-footer"></div>
        </div>
      </div>
    </div>
  </section>

</div>

<?php $this->view('footer'); ?>

<script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>

<script>
    $(document).ready(function() {

      //$("input[name=type_profile][value=1]").attr('checked', 'checked');

      $("input[name=type][value=1]").attr('checked', 'checked');
      //$("input[name=profile][value=1]").attr('checked', 'checked');
      $("input[name=date][value=2]").attr('checked', 'checked');

      checkRadioDate();
      //checkRadioCompany();

      $('#li-reports').addClass('menu-open');
      $('#ul-reports').css('display', 'block');
    });

    function checkRadioType()
    {
      var radio_type = $('input[name=type]:checked').val();

      if(radio_type == 1)
      {
        $('#select-company').attr('disabled', false);

        $('#select-people').attr('disabled', true);
        $('#select-vehicles').attr('disabled', true);

        $('#select-people').val(0);
        $('#select-vehicles').val(0);
      }
      else if(radio_type == 2)
      {
        loadVehicles(0);

        $('#select-vehicles').attr('disabled', false);

        $('#select-company').attr('disabled', false);
        $('#select-people').attr('disabled', true);

        $('#select-people').val(0);
        $('#select-company').val(0);
      }
      else if(radio_type == 0)
      {
        loadPeople(0);

        $('#select-people').attr('disabled', false);

        $('#select-company').attr('disabled', false);
        $('#select-vehicles').attr('disabled', true);

        $('#select-vehicles').val(0);
        $('#select-company').val(0);
      }
    }

    function checkRadioDate()
    {
      var radio_date = $('input[name=date]:checked').val();
      var html = '';

      if(radio_date == 1)
      {
        html += '<input type="date" id="input-date" class="form-control">';
      }
      else if(radio_date == 2)
      {
        html += '<div class="col-md-5"><input class="form-control" type="date" id="input-date-init"></div>&nbsp;';
        html += '<div class="col-md-5"><input class="form-control" type="date" id="input-date-end"></div>';
      }
      else
      {
        html += '<div class="col-md-5"><select class="form-control" id="select-year">';
        for(var i=2018; i <= '<?php echo date("Y");?>'; i++ )
        {
          html += '<option value="'+i+'">'+i+'</option>';
        }
        html += '</select></div>';
        html += '&nbsp;&nbsp;';
        html += '<div class="col-md-5"><select class="form-control" id="select-month">';
        html += '<option value="01">Enero</option>';
        html += '<option value="02">Febrero</option>';
        html += '<option value="03">Marzo</option>';
        html += '<option value="04">Abril</option>';
        html += '<option value="05">Mayo</option>';
        html += '<option value="06">Junio</option>';
        html += '<option value="07">Julio</option>';
        html += '<option value="08">Agosto</option>';
        html += '<option value="09">Septiembre</option>';
        html += '<option value="10">Octubre</option>';
        html += '<option value="11">Noviembre</option>';
        html += '<option value="12">Diciembre</option>';
        html += '</select></div>';
      }

      $('#div-date').html(html);
    }

    function loadPeople(companies_id)
    {
      var options = '<option value="0">Seleccione persona</option>';

      $.ajax({
        url: site_url+'/cReports/getPeopleByCompany',
        type: 'post',
        data: {companies_id: companies_id},
        dataType: 'json',
        success: function(data)
        {
          if(data != null && data.length  > 0)
          {
            for(var i=0; i<data.length;i++)
            {
              options += '<option value="'+data[i].id+'">'+data[i].rut+'-'+data[i].digit+' | '+data[i].name+' '+data[i].last_name+'</option>';
            }

            $('#select-people').html(options);
          }

        }
      });
    }

    function loadVehicles(companies_id)
    {
      var options = '<option value="0">Seleccione vehículo</option>';

      $.ajax({
        url: site_url+'/cReports/getVehiclesByCompany',
        type: 'post',
        data: {companies_id: companies_id},
        dataType: 'json',
        success: function(data)
        {
          if(data != null && data.length  > 0)
          {
            for(var i=0; i<data.length;i++)
            {
              options += '<option value="'+data[i].id+'">'+data[i].patent+'</option>';
            }
          }

          $('#select-vehicles').html(options);
        }
      });
    }

    function loadSelects()
    {
      var companies_id = $('#select-company').val();
      loadPeople(companies_id);
      loadVehicles(companies_id);
    }

    function generateReport()
    {

      var table = '<table id="table-reports" class="table table-striped table-bordered table-condensed" style="width:100%;">';
      table += '<thead>';
      table += '<tr>';
      table += '<th>ID</th>';
      table += '<th>Rut</th>';
      table += '<th>Nombre</th>';
      table += '<th>Perfil</th>';
      table += '<th>Puerta</th>';
      table += '<th>Estado</th>';
      table += '<th>Movimiento</th>';
      table += '<th>Tipo</th>';
      table += '<th>Vehículo</th>';
      table += '<th>Fecha</th>';
      table += '</tr>';
      table += '</thead>';
      table += '<tbody id="tbody-reports">';
      table += '</tbody>';
      table += '</table>';

      $('#div-table_reports').html(table);

      var type_profile = $('input[name=type_profile]:checked').val();
      var radio_date = $('input[name=date]:checked').val();

      var fecha = '';
      var fecha_ini = '';
      var fecha_fin = '';
      var mes = '';
      var year = '';

      var type = $('input[name=type]:checked').val();

      if(type == 1)
      {
        //empresa
        var company = $('#select-company').val();

        if(radio_date == 1)
        {
          //fecha
          fecha = $('#input-date').val();

          if(fecha.length > 0)
          {
            $.ajax({
              url: site_url + '/CReports/generateReport',
              type: 'post',
              data: {
                radio_date: radio_date,
                date: fecha,
                type: type,
                company: $('#select-company').val()
              },
              dataType: 'json',
              success: function(data)
              {
                var body = '';

                if(data != null && data.length > 0)
                {
                  for(var i=0;i<data.length;i++)
                  {
                    var type = '';
                    var patent = 'N/A';
                    if(data[i].flow == null)
                      type = 'Peatonal';
                    else
                    {
                      if(data[i].flow == 0)
                        type = 'Peatonal';
                      else
                      {
                        patent = data[i].patent;
                        type = 'Vehicular';
                      }
                    }

                    var success = '';
                    if(data[i].success == null)
                      success = 'Permitido';
                    else
                    {
                      if(data[i].success == 1)
                        success = 'Permitido';
                      else
                        success = 'Rechazado';
                    }

                    var entry = data[i].entry;
                    if(entry == 1)
                      entry = 'Salida';
                    else
                      entry = 'Entrada';

                    body += '<tr>';
                    body += '<td>'+data[i].id+'</td>';
                    body += '<td>'+data[i].rut+'-'+data[i].digit+'</td>';
                    body += '<td>'+data[i].name+' '+data[i].last_name+'</td>';
                    body += '<td>'+data[i].profile+'</td>';
                    body += '<td>'+data[i].door+'</td>';
                    body += '<td>'+success+'</td>';
                    body += '<td>'+entry+'</td>';
                    body += '<td>'+type+'</td>';
                    body += '<td>'+patent+'</td>';
                    body += '<td>'+data[i].created+'</td>';
                    body += '</tr>';

                  }
                }

                $('#tbody-reports').html(body);
                $('#table-reports').DataTable(
                    {
                        'language': { "url": base_url + "assets/Spanish.json" },
                        dom: 'Bfrtip',
                        buttons: [
                            'excel'
                        ],
                    }
                );
              }
            });
          }
          else
            alert('Fecha vacía.');

        }
        else if(radio_date == 2)
        {
          //intervalo
          //company
          fecha_ini = $('#input-date-init').val();
          fecha_fin = $('#input-date-end').val();

          if((fecha_ini.length > 0 && fecha_fin.length > 0) && (fecha_ini <= fecha_fin))
          {
            $.ajax({
              url: site_url + '/CReports/generateReport',
              type: 'post',
              data: {
                radio_date: radio_date,
                date_init: fecha_ini,
                date_end: fecha_fin,
                type: type,
                company: $('#select-company').val()
              },
              dataType: 'json',
              success: function(data)
              {
                var body = '';

                if(data != null && data.length > 0)
                {
                  for(var i=0;i<data.length;i++)
                  {
                    var type = '';
                    var patent = 'N/A';
                    if(data[i].flow == null)
                      type = 'Peatonal';
                    else
                    {
                      if(data[i].flow == 0)
                        type = 'Peatonal';
                      else
                      {
                        patent = data[i].patent;
                        type = 'Vehicular';
                      }
                    }

                    var success = '';
                    if(data[i].success == null)
                      success = 'Permitido';
                    else
                    {
                      if(data[i].success == 1)
                        success = 'Permitido';
                      else
                        success = 'Rechazado';
                    }

                    var entry = data[i].entry;
                    if(entry == 1)
                      entry = 'Salida';
                    else
                      entry = 'Entrada';

                    body += '<tr>';
                    body += '<td>'+data[i].id+'</td>';
                    body += '<td>'+data[i].rut+'-'+data[i].digit+'</td>';
                    body += '<td>'+data[i].name+' '+data[i].last_name+'</td>';
                    body += '<td>'+data[i].profile+'</td>';
                    body += '<td>'+data[i].door+'</td>';
                    body += '<td>'+success+'</td>';
                    body += '<td>'+entry+'</td>';
                    body += '<td>'+type+'</td>';
                    body += '<td>'+patent+'</td>';
                    body += '<td>'+data[i].created+'</td>';
                    body += '</tr>';

                  }
                }

                $('#tbody-reports').html(body);
                $('#table-reports').DataTable(
                    {
                        'language': { "url": base_url + "assets/Spanish.json" },
                        dom: 'Bfrtip',
                        buttons: [
                            'excel'
                        ],
                    }
                );
              }
            });
          }
          else
            alert('Fechas vacías y/o Fecha inicio tiene que ser menor o igual a la Fecha final.');
        }
        else if(radio_date == 0)
        {
          //year
          year = $('#select-year').val();
          //mes
          mes = $('#select-month').val();

          $.ajax({
            url: site_url + '/CReports/generateReport',
            type: 'post',
            data: {
              radio_date: radio_date,
              year: year,
              month: mes,
              type: type,
              company: $('#select-company').val()
            },
            dataType: 'json',
            success: function(data)
            {
              var body = '';

                if(data != null && data.length > 0)
                {
                  for(var i=0;i<data.length;i++)
                  {
                    var type = '';
                    var patent = 'N/A';
                    if(data[i].flow == null)
                      type = 'Peatonal';
                    else
                    {
                      if(data[i].flow == 0)
                        type = 'Peatonal';
                      else
                      {
                        patent = data[i].patent;
                        type = 'Vehicular';
                      }
                    }

                    var success = '';
                    if(data[i].success == null)
                      success = 'Permitido';
                    else
                    {
                      if(data[i].success == 1)
                        success = 'Permitido';
                      else
                        success = 'Rechazado';
                    }

                    var entry = data[i].entry;
                    if(entry == 1)
                      entry = 'Salida';
                    else
                      entry = 'Entrada';

                    body += '<tr>';
                    body += '<td>'+data[i].id+'</td>';
                    body += '<td>'+data[i].rut+'-'+data[i].digit+'</td>';
                    body += '<td>'+data[i].name+' '+data[i].last_name+'</td>';
                    body += '<td>'+data[i].profile+'</td>';
                    body += '<td>'+data[i].door+'</td>';
                    body += '<td>'+success+'</td>';
                    body += '<td>'+entry+'</td>';
                    body += '<td>'+type+'</td>';
                    body += '<td>'+patent+'</td>';
                    body += '<td>'+data[i].created+'</td>';
                    body += '</tr>';

                  }
                }

                $('#tbody-reports').html(body);
                $('#table-reports').DataTable(
                    {
                        'language': { "url": base_url + "assets/Spanish.json" },
                        dom: 'Bfrtip',
                        buttons: [
                            'excel'
                        ],
                    }
                );
            }
          });
        }

      }
      else if(type == 2)
      {
        //vehiculo
        var vehicle = $('#select-vehicles').val();

        if(radio_date == 1)
        {
          //fecha
          fecha = $('#input-date').val();

          if(fecha.length > 0)
          {
            $.ajax({
              url: site_url + '/CReports/generateReport',
              type: 'post',
              data: {radio_date: radio_date, date: fecha, type: type, vehicle: $('#select-vehicles').val()},
              dataType: 'json',
              success: function(data)
              {
                var body = '';

                if(data != null && data.length > 0)
                {
                  for(var i=0;i<data.length;i++)
                  {
                    var type = 'Vehicular';
                    var patent = data[i].patent;

                    var success = '';
                    if(data[i].success == null)
                      success = 'Permitido';
                    else
                    {
                      if(data[i].success == 1)
                        success = 'Permitido';
                      else
                        success = 'Rechazado';
                    }

                    var entry = data[i].entry;
                    if(entry == 1)
                      entry = 'Salida';
                    else
                      entry = 'Entrada';

                    body += '<tr>';
                    body += '<td>'+data[i].id+'</td>';
                    body += '<td>'+data[i].rut+'-'+data[i].digit+'</td>';
                    body += '<td>'+data[i].name+' '+data[i].last_name+'</td>';
                    body += '<td>'+data[i].profile+'</td>';
                    body += '<td>'+data[i].door+'</td>';
                    body += '<td>'+success+'</td>';
                    body += '<td>'+entry+'</td>';
                    body += '<td>'+type+'</td>';
                    body += '<td>'+patent+'</td>';
                    body += '<td>'+data[i].created+'</td>';
                    body += '</tr>';

                  }
                }

                $('#tbody-reports').html(body);
                $('#table-reports').DataTable(
                    {
                        'language': { "url": base_url + "assets/Spanish.json" },
                        dom: 'Bfrtip',
                        buttons: [
                            'excel'
                        ],
                    }
                );
              }
            });
          }
          else
            alert('Fecha vacía.');


        }
        else if(radio_date == 2)
        {
          //intervalo
          fecha_ini = $('#input-date-init').val();
          fecha_fin = $('#input-date-end').val();

          if((fecha_ini.length > 0 && fecha_fin.length > 0) && (fecha_ini <= fecha_fin))
          {
            $.ajax({
              url: site_url + '/CReports/generateReport',
              type: 'post',
              data: {radio_date: radio_date, date_init: fecha_ini, date_end: fecha_fin, type: type, vehicle: $('#select-vehicles').val()},
              dataType: 'json',
              success: function(data)
              {
                var body = '';

                if(data != null && data.length > 0)
                {
                  for(var i=0;i<data.length;i++)
                  {
                    var type = 'Vehicular';
                    var patent = data[i].patent;

                    var success = '';
                    if(data[i].success == null)
                      success = 'Permitido';
                    else
                    {
                      if(data[i].success == 1)
                        success = 'Permitido';
                      else
                        success = 'Rechazado';
                    }

                    var entry = data[i].entry;
                    if(entry == 1)
                      entry = 'Salida';
                    else
                      entry = 'Entrada';

                    body += '<tr>';
                    body += '<td>'+data[i].id+'</td>';
                    body += '<td>'+data[i].rut+'-'+data[i].digit+'</td>';
                    body += '<td>'+data[i].name+' '+data[i].last_name+'</td>';
                    body += '<td>'+data[i].profile+'</td>';
                    body += '<td>'+data[i].door+'</td>';
                    body += '<td>'+success+'</td>';
                    body += '<td>'+entry+'</td>';
                    body += '<td>'+type+'</td>';
                    body += '<td>'+patent+'</td>';
                    body += '<td>'+data[i].created+'</td>';
                    body += '</tr>';

                  }
                }

                $('#tbody-reports').html(body);
                $('#table-reports').DataTable(
                    {
                        'language': { "url": base_url + "assets/Spanish.json" },
                        dom: 'Bfrtip',
                        buttons: [
                            'excel'
                        ],
                    }
                );
              }
            });
          }
          else
            alert('Fechas vacías y/o Fecha inicio tiene que ser menor o igual a la Fecha final.');
        }
        else if(radio_date == 0)
        {
          //year
          year = $('#select-year').val();
          //mes
          mes = $('#select-month').val();

          $.ajax({
            url: site_url + '/CReports/generateReport',
            type: 'post',
            data: {radio_date: radio_date, year: year, month: mes, type: type, vehicle: $('#select-vehicles').val()},
            dataType: 'json',
            success: function(data)
            {
              var body = '';

                if(data != null && data.length > 0)
                {
                  for(var i=0;i<data.length;i++)
                  {
                    var type = 'Vehicular';
                    var patent = data[i].patent;

                    var success = '';
                    if(data[i].success == null)
                      success = 'Permitido';
                    else
                    {
                      if(data[i].success == 1)
                        success = 'Permitido';
                      else
                        success = 'Rechazado';
                    }

                    var entry = data[i].entry;
                    if(entry == 1)
                      entry = 'Salida';
                    else
                      entry = 'Entrada';

                    body += '<tr>';
                    body += '<td>'+data[i].id+'</td>';
                    body += '<td>'+data[i].rut+'-'+data[i].digit+'</td>';
                    body += '<td>'+data[i].name+' '+data[i].last_name+'</td>';
                    body += '<td>'+data[i].profile+'</td>';
                    body += '<td>'+data[i].door+'</td>';
                    body += '<td>'+success+'</td>';
                    body += '<td>'+entry+'</td>';
                    body += '<td>'+type+'</td>';
                    body += '<td>'+patent+'</td>';
                    body += '<td>'+data[i].created+'</td>';
                    body += '</tr>';

                  }
                }

                $('#tbody-reports').html(body);
                $('#table-reports').DataTable(
                    {
                        'language': { "url": base_url + "assets/Spanish.json" },
                        dom: 'Bfrtip',
                        buttons: [
                            'excel'
                        ],
                    }
                );
            }
          });
        }

      }
      else if(type == 0)
      {
        //persona
        var people = $('#select-people').val();

        if(radio_date == 1)
        {
          //fecha
          fecha = $('#input-date').val();
          if(fecha.length > 0)
          {
            $.ajax({
              url: site_url + '/CReports/generateReport',
              type: 'post',
              data: {radio_date: radio_date, date: fecha, type: type, people: $('#select-people').val()},
              dataType: 'json',
              success: function(data)
              {
                var body = '';

                if(data != null && data.length > 0)
                {
                  for(var i=0;i<data.length;i++)
                  {
                    var type = 'Peatonal';
                    var patent = 'N/A';

                    var success = '';
                    if(data[i].success == null)
                      success = 'Permitido';
                    else
                    {
                      if(data[i].success == 1)
                        success = 'Permitido';
                      else
                        success = 'Rechazado';
                    }

                    var entry = data[i].entry;
                    if(entry == 1)
                      entry = 'Salida';
                    else
                      entry = 'Entrada';

                    body += '<tr>';
                    body += '<td>'+data[i].id+'</td>';
                    body += '<td>'+data[i].rut+'-'+data[i].digit+'</td>';
                    body += '<td>'+data[i].name+' '+data[i].last_name+'</td>';
                    body += '<td>'+data[i].profile+'</td>';
                    body += '<td>'+data[i].door+'</td>';
                    body += '<td>'+success+'</td>';
                    body += '<td>'+entry+'</td>';
                    body += '<td>'+type+'</td>';
                    body += '<td>'+patent+'</td>';
                    body += '<td>'+data[i].created+'</td>';
                    body += '</tr>';

                  }
                }

                $('#tbody-reports').html(body);
                $('#table-reports').DataTable(
                    {
                        'language': { "url": base_url + "assets/Spanish.json" },
                        dom: 'Bfrtip',
                        buttons: [
                            'excel'
                        ],
                    }
                );
              }
            });
          }
          else
            alert('Fecha vacía.');
        }
        else if(radio_date == 2)
        {
          //intervalo
          fecha_ini = $('#input-date-init').val();
          fecha_fin = $('#input-date-end').val();

          if((fecha_ini.length > 0 && fecha_fin.length > 0) && (fecha_ini <= fecha_fin))
          {
            $.ajax({
              url: site_url + '/CReports/generateReport',
              type: 'post',
              data: {radio_date: radio_date, date_init: fecha_ini, date_end: fecha_fin, type: type, people: $('#select-people').val()},
              dataType: 'json',
              success: function(data)
              {
                var body = '';

                if(data != null && data.length > 0)
                {
                  for(var i=0;i<data.length;i++)
                  {
                    var type = 'Peatonal';
                    var patent = 'N/A';

                    var success = '';
                    if(data[i].success == null)
                      success = 'Permitido';
                    else
                    {
                      if(data[i].success == 1)
                        success = 'Permitido';
                      else
                        success = 'Rechazado';
                    }

                    var entry = data[i].entry;
                    if(entry == 1)
                      entry = 'Salida';
                    else
                      entry = 'Entrada';

                    body += '<tr>';
                    body += '<td>'+data[i].id+'</td>';
                    body += '<td>'+data[i].rut+'-'+data[i].digit+'</td>';
                    body += '<td>'+data[i].name+' '+data[i].last_name+'</td>';
                    body += '<td>'+data[i].profile+'</td>';
                    body += '<td>'+data[i].door+'</td>';
                    body += '<td>'+success+'</td>';
                    body += '<td>'+entry+'</td>';
                    body += '<td>'+type+'</td>';
                    body += '<td>'+patent+'</td>';
                    body += '<td>'+data[i].created+'</td>';
                    body += '</tr>';

                  }
                }

                $('#tbody-reports').html(body);
                $('#table-reports').DataTable(
                    {
                        'language': { "url": base_url + "assets/Spanish.json" },
                        dom: 'Bfrtip',
                        buttons: [
                            'excel'
                        ],
                    }
                );
              }
            });
          }
          else
            alert('Fechas vacías y/o Fecha inicio tiene que ser menor o igual a la Fecha final.');
        }
        else if(radio_date == 0)
        {
          //year
          year = $('#select-year').val();
          //mes
          mes = $('#select-month').val();

          $.ajax({
            url: site_url + '/CReports/generateReport',
            type: 'post',
            data: {radio_date: radio_date, year: year, month: mes, type: type, people: $('#select-people').val()},
            dataType: 'json',
            success: function(data)
            {
              var body = '';

                if(data != null && data.length > 0)
                {
                  for(var i=0;i<data.length;i++)
                  {
                    var type = 'Peatonal';
                    var patent = 'N/A';

                    var success = '';
                    if(data[i].success == null)
                      success = 'Permitido';
                    else
                    {
                      if(data[i].success == 1)
                        success = 'Permitido';
                      else
                        success = 'Rechazado';
                    }

                    var entry = data[i].entry;
                    if(entry == 1)
                      entry = 'Salida';
                    else
                      entry = 'Entrada';

                    body += '<tr>';
                    body += '<td>'+data[i].id+'</td>';
                    body += '<td>'+data[i].rut+'-'+data[i].digit+'</td>';
                    body += '<td>'+data[i].name+' '+data[i].last_name+'</td>';
                    body += '<td>'+data[i].profile+'</td>';
                    body += '<td>'+data[i].door+'</td>';
                    body += '<td>'+success+'</td>';
                    body += '<td>'+entry+'</td>';
                    body += '<td>'+type+'</td>';
                    body += '<td>'+patent+'</td>';
                    body += '<td>'+data[i].created+'</td>';
                    body += '</tr>';

                  }
                }

                $('#tbody-reports').html(body);
                $('#table-reports').DataTable(
                    {
                        'language': { "url": base_url + "assets/Spanish.json" },
                        dom: 'Bfrtip',
                        buttons: [
                          {
                            extend: 'excel',
                            className: 'btn btn-primary'
                          },
                        ],
                    }
                );
            }
          });
        }
      }
    }

    // function exportExcel(){

    //   var table = '<table id="table-reports" class="table table-bordered table-striped">';
    //   table += '<thead>';
    //   table += '<tr>';
    //   table += '<th>ID</th>';
    //   table += '<th>Rut</th>';
    //   table += '<th>Nombre</th>';
    //   table += '<th>Perfil</th>';
    //   table += '<th>Puerta</th>';
    //   table += '<th>Estado</th>';
    //   table += '<th>Movimiento</th>';
    //   table += '<th>Tipo</th>';
    //   table += '<th>Vehículo</th>';
    //   table += '<th>Fecha</th>';
    //   table += '</tr>';
    //   table += '</thead>';
    //   table += '<tbody id="tbody-reports">';
    //   table += '</tbody>';
    //   table += '</table>';

    //   $('#div-table_reports').html(table);

    //   var type_profile = $('input[name=type_profile]:checked').val();
    //   var radio_date = $('input[name=date]:checked').val();

    //   var fecha = '';
    //   var fecha_ini = '';
    //   var fecha_fin = '';
    //   var mes = '';
    //   var year = '';

    //   var type = $('input[name=type]:checked').val();

    //   if(type == 1){
    //     //empresa
    //     var company = $('#select-company').val();

    //     if(radio_date == 1){
    //       //fecha
    //       fecha = $('#input-date').val();

    //       if(fecha.length > 0){
    //         $.ajax({
    //           url: site_url + '/CReports/exportExcel',
    //           type: 'post',
    //           data: {radio_date: radio_date, date: fecha, type: type, company: $('#select-company').val()},
    //           dataType: 'json'
    //         });
    //       }
    //       else
    //         alert('Fecha vacía.');

    //     }
    //     else if(radio_date == 2)
    //     {
    //       //intervalo
    //       //company
    //       fecha_ini = $('#input-date-init').val();
    //       fecha_fin = $('#input-date-end').val();

    //       if((fecha_ini.length > 0 && fecha_fin.length > 0) && (fecha_ini <= fecha_fin)){
    //         $.ajax({
    //           url: site_url + '/CReports/exportExcel',
    //           type: 'post',
    //           data: {radio_date: radio_date, date_init: fecha_ini, date_end: fecha_fin, type: type, company: $('#select-company').val()},
    //           dataType: 'json'
    //         });
    //       }
    //       else
    //         alert('Fechas vacías y/o Fecha inicio tiene que ser menor o igual a la Fecha final.');
    //     }
    //     else if(radio_date == 0){
    //       //year
    //       year = $('#select-year').val();
    //       //mes
    //       mes = $('#select-month').val();

    //       $.ajax({
    //         url: site_url + '/CReports/exportExcel',
    //         type: 'post',
    //         data: {radio_date: radio_date,year: year, month: mes, type: type, company: $('#select-company').val()},
    //         dataType: 'json'
    //       });
    //     }

    //   }
    //   else if(type == 2){
    //     //vehiculo
    //     var vehicle = $('#select-vehicles').val();

    //     if(radio_date == 1){
    //       //fecha
    //       fecha = $('#input-date').val();

    //       if(fecha.length > 0){
    //         $.ajax({
    //           url: site_url + '/CReports/exportExcel',
    //           type: 'post',
    //           data: {radio_date: radio_date, date: fecha, type: type, vehicle: $('#select-vehicles').val()},
    //           dataType: 'json'
    //         });
    //       }
    //       else
    //         alert('Fecha vacía.');
    //     }
    //     else if(radio_date == 2){
    //       //intervalo
    //       fecha_ini = $('#input-date-init').val();
    //       fecha_fin = $('#input-date-end').val();

    //       if((fecha_ini.length > 0 && fecha_fin.length > 0) && (fecha_ini <= fecha_fin)){
    //         $.ajax({
    //           url: site_url + '/CReports/exportExcel',
    //           type: 'post',
    //           data: {radio_date: radio_date, date_init: fecha_ini, date_end: fecha_fin, type: type, vehicle: $('#select-vehicles').val()},
    //           dataType: 'json'
    //         });
    //       }
    //       else
    //         alert('Fechas vacías y/o Fecha inicio tiene que ser menor o igual a la Fecha final.');
    //     }
    //     else if(radio_date == 0){
    //       //year
    //       year = $('#select-year').val();
    //       //mes
    //       mes = $('#select-month').val();

    //       $.ajax({
    //         url: site_url + '/CReports/exportExcel',
    //         type: 'post',
    //         data: {radio_date: radio_date, year: year, month: mes, type: type, vehicle: $('#select-vehicles').val()},
    //         dataType: 'json'
    //       });
    //     }

    //   }
    //   else if(type == 0){
    //     //persona
    //     var people = $('#select-people').val();

    //     if(radio_date == 1){
    //       //fecha
    //       fecha = $('#input-date').val();
    //       if(fecha.length > 0){
    //         $.ajax({
    //           url: site_url + '/CReports/exportExcel',
    //           type: 'post',
    //           data: {radio_date: radio_date, date: fecha, type: type, people: $('#select-people').val()},
    //           dataType: 'json'
    //         });
    //       }
    //       else
    //         alert('Fecha vacía.');
    //     }
    //     else if(radio_date == 2){
    //       //intervalo
    //       fecha_ini = $('#input-date-init').val();
    //       fecha_fin = $('#input-date-end').val();

    //       if((fecha_ini.length > 0 && fecha_fin.length > 0) && (fecha_ini <= fecha_fin)){
    //         $.ajax({
    //           url: site_url + '/CReports/exportExcel',
    //           type: 'post',
    //           data: {radio_date: radio_date, date_init: fecha_ini, date_end: fecha_fin, type: type, people: $('#select-people').val()},
    //           dataType: 'json'
    //         });
    //       }
    //       else
    //         alert('Fechas vacías y/o Fecha inicio tiene que ser menor o igual a la Fecha final.');
    //     }
    //     else if(radio_date == 0){
    //       //year
    //       year = $('#select-year').val();
    //       //mes
    //       mes = $('#select-month').val();

    //       $.ajax({
    //         url: site_url + '/CReports/exportExcel',
    //         type: 'post',
    //         data: {radio_date: radio_date, year: year, month: mes, type: type, people: $('#select-people').val()},
    //         dataType: 'json'
    //       });
    //     }
    //   }
    // }
</script>

</body>
</html>
