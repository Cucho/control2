<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-success">
					<div class="box-header ui-sortable-handle">
					    <i class="fa fa-file-text"></i>
						<h3 class="box-title">Agregar Tipo de Bitácora</h3>
				  	</div>

				  	<form class="form-horizontal" id="add-type_logbook">
			  			<div class="box-body">
			  				<div class="box-body">
				  				<div class="form-group">
				  					<label for="input-type" class="col-sm-2 control-label">Tipo</label>
				  					<div class="col-sm-5">
				  						<input type="text" class="form-control" name="input-type" id="input-type" required value="">
				  					</div>
				  				</div>
				  			</div>
			  			</div>
			  			<div class="box-footer">
			  				<button type="submit" class="btn btn-primary pull-right">Guardar</button>
			  			</div>
			  		</form>
				</div>
			</div>
		</div>
	</section>
	
</div>


<?php $this->view('footer'); ?>

<script>
	$(document).ready(function() {
		$("#add-type_logbook").submit(function(event) {
			event.preventDefault();

			$.post(
				site_url + "/CType_Logbook/addType_Logbook",{
				type     	: 	$("#input-type").val()
				},
				function(data){
					if (data == 1) {
						window.location.replace(site_url+"/CType_Logbook/index");
					}
					else {
						alert("Error en el proceso...")
					}
				}
			);

			$('#li-type_logbook').addClass('menu-open');
      		$('#ul-type_logbook').css('display', 'block');
		});
	});
	
</script>
</body>
</html>
