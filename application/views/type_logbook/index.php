<div class="content-wrapper">
	<section class="content">
    <div class="row">
      <div class="col-sm-12">
        <div class="box box-success">
          <div class="box-header ui-sortable-handle">
              <i class="fa fa-file-text"></i>
              <h3 class="box-title">Tipos de Bitácora</h3>
          </div>

          <div class="box-body">
            <section class="content">
                <a href="<?php echo site_url() ?>/CType_Logbook/add" class="btn btn-primary">Agregar</a><br><hr>
                <table id="table-type_logbook" class="table table-striped table-bordered table-condensed" style="width:100%;">
                    <thead>
                      <tr>
                        <th width="10%">ID</th>
                        <th>Tipo</th>
                        <th>Acción</th>
                      </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </section>
          </div>
          <div class="box-footer"></div>
        </div>
      </div>
    </div>
  </section>
	
</div>

<?php $this->view('footer'); ?>

<script>
  var edit = <?php echo $this->session->userdata('edit'); ?>;
  var del = <?php echo $this->session->userdata('del'); ?>;

    $(document).ready(function() {
      
      $('#table-type_logbook').DataTable({
          "lengthMenu": [[5, 10, 15, 20,], [5, 10, 15, 20]],
          'responsive': true,
          'paging': true,
          'info': true,
          'filter': true,
          'ordering': true,
          // 'stateSave': true,
          'processing':true,
          'serverSide':true,
          'language': {
            "url": base_url + "assets/Spanish.json"
          },
          "order": [[0, "asc"]],
          'ajax': {
            "url": site_url + "/CType_Logbook/datatable",
            "type":"POST",
          },
          "columns": [
            { "data": "ID"},
            { "data": "Tipo" },
            { "data": "Acción" }
          ],
          "columnDefs": [
            {
              "targets": [0],
              "orderable": true,
              "render": function(data, type, row) {
                return row.id
              }
            },
            {
              "targets": [1],
              "orderable": true,
              "render": function(data, type, row) {
                return row.type
              }
            },
            {
              "targets": [2],
              "orderable": false,
              "render": function(data, type, row) {
                return `
                  <a href="<?php echo site_url(); ?>/CType_Logbook/view?id=`+row.id+`" class="btn btn-primary btn-xs" role="button">
                    <i class='fa fa-search'></i> Ver
                  </a>
                  <a href="<?php echo site_url(); ?>/CType_Logbook/edit?id=`+row.id+`" class="btn btn-warning btn-xs" role="button">
                      <i class='fa fa-pencil-square-o'></i> Editar
                  </a>
                  <a href="#" class="btn btn-danger btn-xs" role="button" onclick="delete_type_logbook(`+row.id+`);">
                      <i class='fa fa-trash-o'></i> Eliminar
                  </a>`;
              }
            }
           ],
        });
      $('#li-logbook').addClass('menu-open');
      $('#ul-logbook').css('display', 'block');
      
    });

    function delete_type_logbook(id) {
      
      if (confirm('¡Seguro de eliminar!')) {
        $.post(
        site_url + "/CType_Logbook/deleteType_Logbook",{
        id  :   id
        },
        function(data){
          if (data == 1) {
            window.location.reload();
          }
          else {
            alert("Error en el proceso...")
          }
        }
      );
    }
    
  }
</script>

</body>
</html>
