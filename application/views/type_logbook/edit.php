<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-success">
					<div class="box-header ui-sortable-handle">
					    <i class="fa fa-file-text"></i>
						<h3 class="box-title">Editar Tipo de Bitácora #<?php echo $type_logbook[0]['id']; ?></h3>
				  	</div>

				  	<form class="form-horizontal" id="edit-type_logbook">
			  			<div class="box-body">
			  				<div class="box-body">
				  				<div class="form-group">
				  					<label for="input-type" class="col-sm-2 control-label">Tipo</label>
				  					<div class="col-sm-5">
				  						<input type="text" class="form-control" name="input-type" id="input-type" required value="<?php echo $type_logbook[0]['type'] ?>">
				  					</div>
				  				</div>
				  			</div>
			  			</div>
			  			<div class="box-footer">
			  				<button type="submit" class="btn btn-primary pull-right">Guardar</button>
			  			</div>
			  		</form>
				</div>
			</div>
		</div>
	</section>
	
</div>


<?php $this->view('footer'); ?>

<script>
	$(document).ready(function() {
		$("#edit-type_logbook").submit(function(event) {
			event.preventDefault();

			$.post(
				site_url + "/CType_Logbook/editType_Logbook",{
					id 			: 	<?php echo $type_logbook[0]['id']; ?>,
					type     	: 	$("#input-type").val()
				},
				function(data){
					if (data == 1) {
						window.location.replace(site_url+"/CType_Logbook/index");
					}
					else {
						alert("Error en el proceso...")
						window.location.replace(site_url+"/CType_Logbook/edit?id="+<?php echo $type_logbook[0]['id']; ?>);
					}
				}
			);

			$('#li-type_logbook').addClass('menu-open');
      		$('#ul-type_logbook').css('display', 'block');
		});
	});
	
</script>
</body>
</html>
