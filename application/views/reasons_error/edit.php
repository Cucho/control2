<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-success">
					<div class="box-header ui-sortable-handle">
					    <i class="fa fa-cog"></i>
						<h3 class="box-title">Editar motivo error #<?php echo $reasons_error[0]['id']; ?></h3>
				  	</div>
					<?php print_r($reasons_error) ?>
				  	<form class="form-horizontal" id="editReason_Error">
			  			<div class="box-body">
			  				<div class="form-group">
			  					<label for="reason" class="col-sm-2 control-label">Opción</label>
			  					<div class="col-sm-10">
			  						<input type="text" class="form-control" name="reason" id="reason" required value="<?php echo $reasons_error[0]['reason'] ?>">
			  					</div>
			  				</div>
			  			</div>
			  			<div class="box-footer">
			  				<button type="submit" class="btn btn-primary pull-right">Guardar</button>
			  			</div>
			  		</form>
				</div>
			</div>
		</div>
	</section>
	
</div>


<?php $this->view('footer'); ?>

<script>
	$(document).ready(function() {
		$("#editReason_Error").submit(function(event) {
			event.preventDefault();

			$.post(
				site_url + "/cReasons_Error/editReason_Error",{
					id 		: 	<?php echo $reasons_error[0]['id']; ?>,
					reason 	: 	$("#reason").val()
				},
				function(data){
					if (data == 1) {
						window.location.replace(site_url+"/cReasons_Error/");
					}
					else {
						alert("Error en el proceso...")
						window.location.replace(site_url+"/cReasons_Error/edit?id="+<?php echo $reasons_error[0]['id']; ?>);
					}
				}
			);

			$('#li-internal').addClass('menu-open');
			$('#ul-internal').css('display', 'block');
		});
	});
	
</script>
</body>
</html>
