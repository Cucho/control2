<div class="content-wrapper">
  <section class="content">
    <div class="row">
      <div class="col-sm-12">
        <div class="box box-success">
          <div class="box-header ui-sortable-handle">
              <i class="fa fa-exclamation"></i>
            <h3 class="box-title">Registros vehículares internos</h3>
            </div>

            <div class="box-body">
              <section class="content">
                  <table id="internal_vehicles" class="table table-striped table-bordered table-condensed" style="width:100%;">
                      <thead>
                        <tr>
                          <th width="10%">ID</th>
                          <th>Rut</th>
                          <th>Nombre</th>
                          <th>Vehículo</th>
                          <th>Departamento</th>
                          <th>Perfil</th>
                          <th>Puerta</th>
                          <th>Movimiento</th>
                          <th>Estado</th>
                          <th>Motivo</th>
                          <th>Fecha</th>
                        </tr>
                      </thead>
                      <tbody></tbody>
                  </table>
              </section>
            </div>
            <div class="box-footer"></div>
        </div>
      </div>
    </div>
  </section>
  
</div>

<?php $this->view('footer'); ?>

<script>
    $(document).ready(function() {
      $('#internal_vehicles').DataTable({
          "lengthMenu": [[5, 10, 15, 20,], [5, 10, 15, 20]],
          'responsive': true,
          'paging': true,
          'info': true,
          'filter': true,
          'ordering': true,
          // 'stateSave': true,
          'processing':true,
          'serverSide':true,
          'language': {
            "url": base_url + "assets/Spanish.json"
          },
          "order": [[0, "desc"]],
          'ajax': {
            "url": site_url + "/cInternal_Vehicles/datatable",
            "type":"POST",
          },
          "columns": [
            { "data": "ID"},
            { "data": "Rut"},
            { "data": "Nombre"},
            { "data": "Vehículo"},
            { "data": "Departamento"},
            { "data": "Perfil"},
            { "data": "Puerta"},
            { "data": "Movimiento"},
            { "data": "Estado"},
            { "data": "Motivo"},
            { "data": "Fecha"}
          ],
          "columnDefs": [
            {
              "targets": [0],
              "orderable": true,
              "render": function(data, type, row) {
                return row.id
              }
            },
            {
              "targets": [1],
              "orderable": true,
              "render": function(data, type, row) {
                return function(){
                  return row.rut+'-'+row.digit
                }
              }
            },
            {
              "targets": [2],
              "orderable": true,
              "render": function(data, type, row) {
                return row.name+' '+row.last_name
              }
            },
            {
              "targets": [3],
              "orderable": true,
              "render": function(data, type, row) {
                return row.patent
              } 
            },
            {
              "targets": [4],
              "orderable": true,
              "render": function(data, type, row) {
                return row.department
              }
            },
            {
              "targets": [5],
              "orderable": true,
              "render": function(data, type, row) {
                return row.profile
              }
            },
            {
              "targets": [6],
              "orderable": true,
              "render": function(data, type, row) {
                return function(){
                  return row.door
                }
              }
            },
            {
              "targets": [7],
              "orderable": true,
              "render": function(data, type, row) {
                return function(){
                  if(row.entry == 0)
                    return '<label class="label label-success">Ingreso</label>'
                  else
                    return '<label class="label label-danger">Salida</label>'
                }
              }
            },
            {
              "targets": [8],
              "orderable": true,
              "render": function(data, type, row) {
                return function(){
                  if(row.success == 1)
                    return '<label class="label label-success">Permitido</label>'
                  else
                    return '<label class="label label-danger">Rechazado</label>'
                }
              }
            },
            {
              "targets": [8],
              "orderable": true,
              "render": function(data, type, row) {
                return function(){
                  if(row.success == 0)
                    return row.reason
                  else
                    return 'N/A'
                }
              }
            },
            {
              "targets": [9],
              "orderable": true,
              "render": function(data, type, row) {
                return function(){
                  return row.created
                }
              }
            }
           ],
      });

      $('#li-internals').addClass('menu-open');
      $('#ul-internals').css('display', 'block');

      $('#li-internal-c').addClass('menu-open');
      $('#ul-internal-c').css('display', 'block');
    });
</script>

</body>
</html>
