<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Generar Código QR
      <!-- <small>Control panel</small> -->
    </h1>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="row">
      <form class="form-inline" role="form" method="post" accept-charset="utf-8" id="form-generate_qr">             
      <div class="box-body">
          <div class="form-group" text required>
                <label for="input-rut">Ingrese un Rut</label>&nbsp;&nbsp;
                <input autocomplete="off" class="form-control" type="text" name="inputrut" autofocus="autofocus" required="required" maxlength="10" id="inputrut" oninput="checkRut(this)">&nbsp;&nbsp;&nbsp;&nbsp;
                <button type="submit" class="btn btn-primary">Generar</button>
                &nbsp;&nbsp;
                <button type="button" class="btn btn-primary" onclick="clearQRCode();">Limpiar</button>
          </div>
          <input class="form-control" type="hidden" name="check" value="1"/> 
      </div>
      </form>
      <hr>
      <center>
        <h5 id="h5-title">Esperando generación de Código QR</h5>
        <img id="img-qr_code" style="width: 150px;height: 150px;" src="<?php echo base_url()?>assets/img/cargando.gif">
      </center>
    </div>
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<?php $this->load->view('footer');?>

<script type="text/javascript">
  var cuerpo;
  var dv;

  $(document).ready(function(){

    $("#form-generate_qr").submit(function(event) {
      event.preventDefault();

      $.ajax({
        url: site_url + '/CAccess/emitQRCode',
        type:'post',
        data:{rut: cuerpo, dv: dv},
        dataType:'text',
        success: function(data)
        {
          $('#h5-title').html('Código QR Generado para el rut: '+cuerpo+'-'+dv+'&nbsp;&nbsp; <a download="qr_'+cuerpo+'_'+dv+'.png" href="<?php echo base_url();?>'+data+'" title="Imprimir Código QR" class="btn btn-warning btn-xs"><i class="fa fa-print" aria-hidden="true"></i></a>');
          $('#img-qr_code').attr('src','<?php echo base_url();?>'+data);
        }
      });

    });

  });

  function clearQRCode()
  {
    $('#inputrut').val('');
    $('#h5-title').html('Esperando generación de Código QR');
    $('#img-qr_code').attr('src','<?php echo base_url()?>assets/img/cargando.gif');
    setTimeout(function(){ $('#inputrut').focus(); }, 1);
  }


  function checkRut(rut) {
      // Despejar Puntos
      var valor = rut.value.replace('.','');
      // Despejar Guión
      valor = valor.replace('-','');
      
      // Aislar Cuerpo y Dígito Verificador
      cuerpo = valor.slice(0,-1);
      dv = valor.slice(-1).toUpperCase();
      
      // Formatear RUN
      rut.value = cuerpo + '-'+ dv
      
      // Si no cumple con el mínimo ej. (n.nnn.nnn)
      if(cuerpo.length < 7) { rut.setCustomValidity("RUT Incompleto"); return false;}
      
      // Calcular Dígito Verificador
      suma = 0;
      multiplo = 2;
      
      // Para cada dígito del Cuerpo
      for(i=1;i<=cuerpo.length;i++) {
      
          // Obtener su Producto con el Múltiplo Correspondiente
          index = multiplo * valor.charAt(cuerpo.length - i);
          
          // Sumar al Contador General
          suma = suma + index;
          
          // Consolidar Múltiplo dentro del rango [2,7]
          if(multiplo < 7) { multiplo = multiplo + 1; } else { multiplo = 2; }
    
      }
      
      // Calcular Dígito Verificador en base al Módulo 11
      dvEsperado = 11 - (suma % 11);
      
      // Casos Especiales (0 y K)
      dv = (dv == 'K')?10:dv;
      dv = (dv == 0)?11:dv;
      
      // Validar que el Cuerpo coincide con su Dígito Verificador
      if(dvEsperado != dv) 
      { 
        rut.setCustomValidity("RUT Inválido"); 
        return false; 
      }
      else
      {
        //pequeña validacion
        if(dv == 11)
          dv = 0;

        if(dv == 10)
          dv = 'K';
      }
      
      // Si todo sale bien, eliminar errores (decretar que es válido)
      rut.setCustomValidity('');
  }


</script>