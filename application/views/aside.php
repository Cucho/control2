<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu" data-widget="tree">
      
      <?php if (!empty($this->session->userdata('options'))) { ?>
        <?php 
          $codes = [];
          for ($i=0; $i < count($this->session->userdata('options')); $i++) { 
            array_push($codes, $this->session->userdata('options')[$i]['code']);
          } ?>

        <?php if (in_array("01000000", $codes) || in_array("01010000", $codes) || in_array("01020000", $codes) || in_array("01020100", $codes) || in_array("01020101", $codes) || in_array("01020102", $codes) || in_array("01020200", $codes) || in_array("01020201", $codes) || in_array("01020202", $codes) || in_array("01020300", $codes) || in_array("01020301", $codes) || in_array("01020302", $codes) || in_array("01030000", $codes) || in_array("01040000", $codes) || in_array("01050000", $codes) || in_array("01060000", $codes) || in_array("01070000", $codes) || in_array("01070100", $codes) || in_array("01070200", $codes) || in_array("01080000", $codes) || in_array("01080100", $codes) || in_array("01080200", $codes) || in_array("01080300", $codes) || in_array("01090000", $codes) || in_array("01090100", $codes) || in_array("01090200", $codes)) { ?>
                
            <li class="treeview" id="li-configuration">
              <a href="#">
                <i class="fa fa-cog"></i> <span>Configuración</span>
                <span class="pull-right-container">
                  <i class="fa fa-angle-left pull-right"></i>
                </span>
              </a>
              <ul class="treeview-menu" id="ul-configuration">

                <?php if (in_array("01010000", $codes) ) { ?>
                  <li><a href="#"><i class="fa fa-circle-o"></i> Instalación</a></li>
                <?php } ?>

                <?php if (in_array("01020000", $codes) || in_array("01020100", $codes) || in_array("01020101", $codes) || in_array("01020102", $codes) || in_array("01020200", $codes) || in_array("01020201", $codes) || in_array("01020202", $codes) || in_array("01020300", $codes) || in_array("01020301", $codes) || in_array("01020302", $codes)) { ?>
                  <li class="treeview" id="li-ubications">
                    <a href="#">
                      <i class="fa fa-circle-o"></i> <span>Ubicaciones</span>
                      <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                      </span>
                    </a>
                    <ul class="treeview-menu" id="ul-ubications">

                      <?php if (in_array("01020100", $codes) || in_array("01020101", $codes) || in_array("01020102", $codes)) { ?>
                        <li class="treeview" id="li-zones">
                          <a href="#">
                            <i class="fa fa-circle-o"></i>
                            <span>Zonas</span>
                            <span class="pull-right-container">
                              <i class="fa fa-angle-left pull-right"></i>
                            </span>
                          </a>
                          <ul class="treeview-menu" id="ul-zones">

                            <?php if (in_array("01020101", $codes)) { ?>
                              <li><a href="<?php echo site_url() ?>/cZones/"><i class="fa fa-circle-o"></i>Zonas</a></li>
                            <?php } ?>

                            <?php if (in_array("01020102", $codes)) { ?>
                              <li><a href="<?php echo site_url() ?>/cDoors_Zones/index"><i class="fa fa-circle-o"></i>Zonas - Puertas</a></li>
                            <?php } ?>

                          </ul>
                      <?php } ?>
                        </li>

                      <?php if (in_array("01020200", $codes) || in_array("01020201", $codes) || in_array("01020202", $codes)) { ?>
                        <li class="treeview" id="li-areas">
                          <a href="#">
                            <i class="fa fa-circle-o"></i>
                            <span>Áreas</span>
                            <span class="pull-right-container">
                              <i class="fa fa-angle-left pull-right"></i>
                            </span>
                          </a>
                          <ul class="treeview-menu" id="ul-areas">

                            <?php if (in_array("01020201", $codes)) { ?>
                              <li><a href="<?php echo site_url() ?>/cAreas/"><i class="fa fa-circle-o"></i> Áreas</a></li>
                            <?php } ?>

                            <?php if (in_array("01020202", $codes)) { ?>
                              <li><a href="<?php echo site_url() ?>/cAreas_Doors/"><i class="fa fa-circle-o"></i> Áreas - Puertas</a></li>
                            <?php } ?>

                          </ul>
                      <?php } ?>
                        </li>

                      <?php if (in_array("01020300", $codes) || in_array("01020301", $codes) || in_array("01020302", $codes)) { ?>
                        <li class="treeview" id="li-departments">
                          <a href="#">
                            <i class="fa fa-circle-o"></i> <span>Departamentos</span>
                            <span class="pull-right-container">
                              <i class="fa fa-angle-left pull-right"></i>
                            </span>
                          </a>
                          <ul class="treeview-menu" id="ul-departments">

                            <?php if (in_array("01020301", $codes)) { ?>
                              <li><a href="<?php echo site_url() ?>/cDepartments/"><i class="fa fa-circle-o"></i>Departamentos</a></li>
                            <?php } ?>

                            <?php if (in_array("01020302", $codes)) { ?>
                              <li><a href="<?php echo site_url() ?>/cDoors_Departments/"><i class="fa fa-circle-o"></i>Departamentos - Puertas</a></li>
                            <?php } ?>

                          </ul>
                      <?php } ?>
                        </li>

                    </ul>
                <?php } ?>
                  </li>

                <?php if (in_array("01030000", $codes) ) { ?>
                  <li><a href="<?php echo site_url() ?>/cMain_Access/"><i class="fa fa-circle-o"></i>Porterías</a></li>
                <?php } ?>

                <?php if (in_array("01040000", $codes) ) { ?>
                  <li><a href="<?php echo site_url() ?>/cOptions/"><i class="fa fa-circle-o"></i>Opciones SYS</a></li>
                <?php } ?>

                <?php if (in_array("01050000", $codes) ) { ?>
                  <li><a href="<?php echo site_url() ?>/cOptions_Roles/"><i class="fa fa-circle-o"></i>Opciones asignadas</a></li>
                <?php } ?>

                <?php if (in_array("01060000", $codes) ) { ?>
                  <li><a href="<?php echo site_url() ?>/cJornadas/"><i class="fa fa-circle-o"></i>Jornadas</a></li>
                <?php } ?>

                <?php if (in_array("01070000", $codes) || in_array("01070100", $codes) || in_array("01070200", $codes)) { ?>
                  <li class="treeview" id="li-doors">
                    <a href="#">
                      <i class="fa fa-circle-o"></i> <span>Puertas</span>
                      <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                      </span>
                    </a>
                    <ul class="treeview-menu" id="ul-doors">

                      <?php if (in_array("01070100", $codes) ) { ?>
                        <li><a href="<?php echo site_url() ?>/cDoors/"><i class="fa fa-circle-o"></i>Puertas</a></li>
                      <?php } ?>

                      <?php if (in_array("01070200", $codes) ) { ?>
                        <li><a href="<?php echo site_url() ?>/cDoors/Monitoring"><i class="fa fa-circle-o"></i> Monitoreo</a></li>
                      <?php } ?>

                    </ul>
                <?php } ?>
                  </li>

                <?php if (in_array("01080000", $codes) || in_array("01080100", $codes) || in_array("01080200", $codes) || in_array("01080300", $codes)) { ?>
                  <li class="treeview" id="li-sensors">
                    <a href="#">
                      <i class="fa fa-circle-o"></i> <span>Sensores</span>
                      <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                      </span>
                    </a>
                    <ul class="treeview-menu" id="ul-sensors">

                      <?php if (in_array("01080100", $codes) ) { ?>
                        <li><a href="<?php echo site_url() ?>/cSensors/"><i class="fa fa-circle-o"></i> Sensores</a></li>
                      <?php } ?>

                      <?php if (in_array("01080200", $codes) ) { ?>
                        <li><a href="<?php echo site_url() ?>/cSensors_Type/"><i class="fa fa-circle-o"></i>Tipos de sensores</a></li>
                      <?php } ?>

                      <?php if (in_array("01080300", $codes) ) { ?>
                        <li><a href="<?php echo site_url() ?>/cSensors_Doors/"><i class="fa fa-circle-o"></i>Sensores - Puertas</a></li>
                      <?php } ?>

                    </ul>
                <?php } ?>
                  </li>

                <?php if (in_array("01090000", $codes) || in_array("01090100", $codes) || in_array("01090200", $codes)) { ?>
                  <li class="treeview" id="li-users">
                    <a href="#">
                      <i class="fa fa-circle-o"></i> <span>Usuarios</span>
                      <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                      </span>
                    </a>
                    <ul class="treeview-menu" id="ul-users">

                      <?php if (in_array("01090100", $codes)) { ?>
                        <li><a href="<?php echo site_url() ?>/cUsers/"><i class="fa fa-circle-o"></i>Usuarios</a></li>
                      <?php } ?>

                      <?php if (in_array("01090200", $codes)) { ?>
                        <li><a href="<?php echo site_url() ?>/cRoles/"><i class="fa fa-circle-o"></i>Roles</a></li>
                      <?php } ?>

                    </ul>
                <?php } ?>
                  </li>
                
              </ul>
            </li>

        <?php } ?>

        <?php foreach ($this->session->userdata('main') as $y) { ?>
          <?php if (in_array("02000000", $codes) && $this->session->userdata('ip') == $y->ip_host) { ?>
            <li class="treeview-menu">
              <a href="#">
                <li><a href="<?php echo site_url() ?>/cAccess_People/"><i class="fa fa-hand-stop-o"></i> Autorización</a></li>
              </a>
            </li>
          <?php } ?>
        <?php } ?>

        <?php if (in_array("03000000", $codes) || in_array("03010000", $codes)) { ?>
              <li class="treeview" id="li-reports">
                <a href="#">
                  <i class="fa fa-circle-o"></i>
                  <span>Informes</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu" id="ul-reports">

                  <?php if (in_array("03010000", $codes)) { ?>
                    <li><a href="<?php echo site_url() ?>/CReports/index"><i class="fa fa-circle-o"></i> Informes</a></li>
                  <?php } ?>
                  
                </ul>
            <?php } ?>
              </li>
        
        <?php if (in_array("11000000", $codes) || in_array("11010000", $codes) || in_array("11020000", $codes)) { ?>
        <li class="treeview" id="li-logbook">
          <a href="#">
            <i class="fa fa-file-text"></i>
            <span>Bitácora</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu" id="ul-logbook">
            <?php if (in_array("11010000", $codes)) { ?>
              <li>
                <a href="<?php echo site_url() ?>/CLogbook/index"><i class="fa fa-circle-o"></i> Bitácoras</a>
              </li>
            <?php } ?>
            <?php if (in_array("11020000", $codes)) { ?>
              <li>
                <a href="<?php echo site_url() ?>/CType_Logbook/index"><i class="fa fa-circle-o"></i> Tipos</a>
              </li>
            <?php } ?>
          </ul>
        </li>
        <?php } ?> 

        <?php if (in_array("04000000", $codes) || in_array("04010000", $codes) || in_array("04010100", $codes) || in_array("04010200", $codes) || in_array("04010300", $codes) || in_array("04020000", $codes) || in_array("04020100", $codes) || in_array("04020200", $codes)) { ?>
              <li class="treeview" id="li-internals">
                <a href="#">
                  <i class="fa fa-user"></i>
                  <span>Internos</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu" id="ul-internals">
                  
                  <?php if (in_array("04010000", $codes) || in_array("04010100", $codes) || in_array("04010200", $codes) || in_array("04010300", $codes)) { ?>
                    <li class="treeview" id="li-internal-sub">
                      <a href="#"><i class="fa fa-circle-o"></i> Control
                        <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                      </a>
                      <ul class="treeview-menu" id="ul-internal-sub">

                        <?php if (in_array("04010100", $codes)) { ?>
                          <li><a href="<?php echo site_url() ?>/CInternal_Forms/list"><i class="fa fa-circle-o"></i> Controles</a></li>
                        <?php } ?>

                        <?php if (in_array("04010200", $codes)) { ?>
                          <li><a href="<?php echo site_url() ?>/CInternal_Forms/in"><i class="fa fa-circle-o"></i> Ingreso</a></li>
                        <?php } ?>

                        <?php if (in_array("04010300", $codes)) { ?>
                          <li><a href="<?php echo site_url() ?>/CInternal_Forms/out"><i class="fa fa-circle-o"></i> Salida</a></li>
                        <?php } ?>

                      </ul>
                  <?php } ?>
                    </li>

                  <?php if (in_array("04020000", $codes) || in_array("04020100", $codes) || in_array("04020200", $codes)) { ?>
                  <li class="treeview" id="li-internal-c">
                    <a href="#"><i class="fa fa-circle-o"></i> Registros
                      <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                      </span>
                    </a>
                    <ul class="treeview-menu" id="ul-internal-c">

                      <?php if (in_array("04020100", $codes)) { ?>
                        <li><a href="<?php echo site_url() ?>/cInternal_People/"><i class="fa fa-circle-o"></i> Registros peatones</a></li>
                      <?php } ?>

                      <?php if (in_array("04020200", $codes)) { ?>
                        <li><a href="<?php echo site_url() ?>/cInternal_Vehicles/"><i class="fa fa-circle-o"></i> Registros vehículos</a></li>
                      <?php } ?>                            
                      
                    </ul>
                <?php } ?>
                  </li>

                  
                </ul>
            <?php } ?>
              </li>

        <?php if (in_array("05000000", $codes) || in_array("05010000", $codes) || in_array("05010100", $codes) || in_array("05020000", $codes) || in_array("05020100", $codes) || in_array("05020200", $codes) || in_array("05010300", $codes)) { ?>
              <li class="treeview" id="li-visits">
                <a href="#">
                  <i class="fa fa-user"></i>
                  <span>Visitas</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu" id="ul-visits">
                  <?php if (in_array("05010300", $codes)) { ?>
                    <li>
                      <a href="<?php echo site_url() ?>/cAccess_People/list"><i class="fa fa-circle-o"></i> Controles</a>
                    </li>
                  <?php } ?>
                  
                    <?php if (in_array("05010000", $codes) || in_array("05010100", $codes) || in_array("05020000", $codes) || in_array("05020100", $codes) || in_array("05020200", $codes)) { ?>
                      <li class="treeview" id="li-reasons-visit">
                        <a href="#">
                          <i class="fa fa-circle-o"></i> <span>Motivos de visita</span>
                          <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                          </span>
                        </a>
                        <ul class="treeview-menu" id="ul-reasons-visit">

                          <?php if (in_array("05010100", $codes) || in_array("05020000", $codes) || in_array("05020100", $codes) || in_array("05020200", $codes)) { ?>
                            <li><a href="<?php echo site_url() ?>/cReasons_Visit/"><i class="fa fa-circle-o"></i>Motivos</a></li>
                          <?php } ?>
                          
                        </ul>
                    <?php } ?>
                      </li>

                      <?php if (in_array("05020000", $codes) || in_array("05020100", $codes) || in_array("05020200", $codes)) { ?>
                      <li class="treeview" id="li-access_people_vehicles">
                        <a href="#">
                          <i class="fa fa-circle-o"></i> <span>Registros visitas</span>
                          <span class="pull-right-container">
                            <i class="fa fa-angle-left pull-right"></i>
                          </span>
                        </a>
                        <ul class="treeview-menu" id="ul-access_people_vehicles">

                          <?php if (in_array("05020100", $codes)) { ?>
                            <li><a href="<?php echo site_url();?>/cAccess_People/List_Access_People"><i class="fa fa-circle-o"></i> Registros</a></li>
                          <?php } ?>

                          <?php if (in_array("05020200", $codes)) { ?>
                            <li><a href="<?php echo site_url() ?>/cAccess/Pending"><i class="fa fa-circle-o"></i> Registros pendientes</a></li>
                          <?php } ?>                            

                        </ul>
                    <?php } ?>
                      </li>
                </ul>
            <?php } ?>
              </li>

        <?php if (in_array("06000000", $codes) || in_array("06010000", $codes) || in_array("06020000", $codes) || in_array("06020100", $codes) || in_array("06030000", $codes) || in_array("06030100", $codes) || in_array("06030200", $codes) || in_array("06030300", $codes)) { ?>
              <li class="treeview" id="li-contractors">
                <a href="#">
                  <i class="fa fa-user"></i>
                  <span>Contratistas</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu" id="ul-contractors">

                  <?php if (in_array("06010000", $codes)) { ?>
                    <li><a href="<?php echo site_url() ?>/cProjects/"><i class="fa fa-circle-o"></i>Proyectos</a></li>
                  <?php } ?>

                  <?php if (in_array("06020000", $codes) || in_array("06020100", $codes)) { ?>
                    <li class="treeview" id="li-m_requeriments">
                      <a href="#"><i class="fa fa-circle-o"></i> Requerimientos
                        <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                      </a>
                      <ul class="treeview-menu" id="ul-m_requeriments">

                        <?php if (in_array("06020100", $codes)) { ?>
                          <li><a href="<?php echo site_url() ?>/cMinimum_Requirements/"><i class="fa fa-circle-o"></i>Requerimientos</a></li>
                        <?php } ?>

                      </ul>
                  <?php } ?>
                    </li>

                    <?php if (in_array("06030000", $codes) || in_array("06030100", $codes) || in_array("06030200", $codes) || in_array("06030300", $codes)) { ?>
                    <li class="treeview" id="li-projects-c">
                      <a href="#"><i class="fa fa-circle-o"></i> Control
                        <span class="pull-right-container">
                          <i class="fa fa-angle-left pull-right"></i>
                        </span>
                      </a>
                      <ul class="treeview-menu" id="ul-projects-c">

                        <?php if (in_array("06030100", $codes)) { ?>
                          <li><a href="<?php echo site_url() ?>/CProjects_Forms/list"><i class="fa fa-circle-o"></i> Controles</a></li>
                        <?php } ?>

                        <?php if (in_array("06030200", $codes)) { ?>
                          <li><a href="<?php echo site_url() ?>/CProjects_Forms/in"><i class="fa fa-circle-o"></i> Ingreso</a></li>
                        <?php } ?>

                        <?php if (in_array("06030300", $codes)) { ?>
                          <li><a href="<?php echo site_url() ?>/CProjects_Forms/out"><i class="fa fa-circle-o"></i> Salida</a></li>
                        <?php } ?>

                      </ul>
                  <?php } ?>
                    </li>
                  
                </ul>
            <?php } ?>
              </li>

        <?php if (in_array("07000000", $codes) || in_array("07010000", $codes)) { ?>
              <li class="treeview" id="li-companies">
                <a href="#">
                  <i class="fa fa-industry"></i>
                  <span>Empresas</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu" id="ul-companies">

                  <?php if (in_array("07010000", $codes)) { ?>
                    <li><a href="<?php echo site_url() ?>/cCompanies/"><i class="fa fa-circle-o"></i> Empresas</a></li>
                  <?php } ?>
                  
                </ul>
            <?php } ?>
              </li>

        <?php if (in_array("08000000", $codes) || in_array("08010000", $codes) || in_array("08020000", $codes) || in_array("08030000", $codes) || in_array("08040000", $codes)) { ?>
              <li class="treeview" id="li-people">
                <a href="#">
                  <i class="fa fa-users"></i> <span>Personas</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu" id="ul-people">

                  <?php if (in_array("08010000", $codes)) { ?>
                    <li><a href="<?php echo site_url() ?>/cPeople/"><i class="fa fa-circle-o"></i> Personas</a></li>
                  <?php } ?>

                  <?php if (in_array("08020000", $codes)) { ?>
                    <li><a href="<?php echo site_url() ?>/cPeople_Profiles/"><i class="fa fa-circle-o"></i>Perfiles</a></li>
                  <?php } ?>

                  <?php if (in_array("08030000", $codes)) { ?>
                    <li><a href="<?php echo site_url() ?>/cSpecial_Schedule/"><i class="fa fa-circle-o"></i> Horarios especiales</a></li>
                  <?php } ?>

                  <?php if (in_array("08040000", $codes)) { ?>
                    <li><a href="<?php echo site_url() ?>/cProfiles_Doors_Schedules/add"><i class="fa fa-circle-o"></i> Agregar perfil-puerta-horario</a></li>
                  <?php } ?>
                  
                </ul>
            <?php } ?>
              </li>
          
        <?php if (in_array("09000000", $codes) || in_array("09010000", $codes) || in_array("09020000", $codes)) { ?>
              <li class="treeview" id="li-form">
                <a href="#">
                  <i class="fa fa-file-text"></i> <span>Formularios</span>
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu" id="ul-form">

                  <?php if (in_array("09010000", $codes)) { ?>
                    <li><a href="<?php echo site_url() ?>/cForms/"><i class="fa fa-circle-o"></i> Formularios</a></li>
                  <?php } ?>

                  <?php if (in_array("09020000", $codes)) { ?>
                    <li><a href="<?php echo site_url() ?>/cMeasures/"><i class="fa fa-circle-o"></i>Tipos de medidas</a></li>
                  <?php } ?>
                  
                </ul>
            <?php } ?>
              </li>  
            
        <?php if (in_array("10000000", $codes) || in_array("10010000", $codes) || in_array("10020000", $codes) || in_array("10030000", $codes) || in_array("10040000", $codes)) { ?>
          <li class="treeview" id="li-vehicles">
            <a href="#">
              <i class="fa fa-car"></i> <span>Vehículos</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu" id="ul-vehicles">

                <?php if (in_array("10010000", $codes)) { ?>
                  <li><a href="<?php echo site_url() ?>/cVehicles/"><i class="fa fa-circle-o"></i> Vehículos</a></li>
                <?php } ?>

                <?php if (in_array("10020000", $codes)) { ?>
                  <li><a href="<?php echo site_url() ?>/cVehicles_Type/"><i class="fa fa-circle-o"></i> Tipos</a></li>
                <?php } ?>

                <?php if (in_array("10030000", $codes)) { ?>
                  <li><a href="<?php echo site_url() ?>/cVehicles_Profiles/"><i class="fa fa-circle-o"></i> Perfiles</a></li>
                <?php } ?>

                <?php if (in_array("10040000", $codes)) { ?>
                  <li><a href="<?php echo site_url() ?>/cVehicles_Drivers/"><i class="fa fa-circle-o"></i> Vehículo-Chofer</a></li>
                <?php } ?>
              
            </ul>
        <?php } ?>
          </li>

      <?php } ?>

    </ul>
  </section>
  <!-- /.sidebar -->
</aside>
