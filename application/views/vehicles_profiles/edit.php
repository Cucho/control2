<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-success">
					<div class="box-header ui-sortable-handle">
					    <i class="fa fa-car"></i>
						<h3 class="box-title">Editar vehículo perfil #<?php echo $vehicles_profiles[0]['id']; ?></h3>
				  	</div>

				  	<form class="form-horizontal" id="addVehicles_profiles">
			  			<div class="box-body">
			  				<div class="form-group">
			  					<label for="vehicles_profiles" class="col-sm-2 control-label">Tipo vehículo</label>
			  					<div class="col-sm-10">
			  						<input type="text" class="form-control" name="vehicles_profiles" id="vehicles_profiles" required value="<?php echo $vehicles_profiles[0]['profile'] ?>">
			  					</div>
			  				</div>
			  			</div>
			  			<div class="box-footer">
			  				<button type="submit" class="btn btn-primary pull-right">Guardar</button>
			  			</div>
			  		</form>
				</div>
			</div>
		</div>
	</section>
	
</div>


<?php $this->view('footer'); ?>

<script>
	$(document).ready(function() {
		$("#addVehicles_profiles").submit(function(event) {
			event.preventDefault();

			$.post(
				site_url + "/cVehicles_profiles/editVehicles_profiles",{
					id 		: 	<?php echo $vehicles_profiles[0]['id']; ?>,
					vehicles_profiles : 	$("#vehicles_profiles").val()
				},
				function(data){
					if (data == 1) {
						window.location.replace(site_url+"/cVehicles_profiles/");
					}
					else {
						alert("Error en el proceso...")
						window.location.replace(site_url+"/cVehicles_profiles/edit?id="+<?php echo $vehicles_profiles[0]['id']; ?>);
					}
				}
			);

			$('#li-vehicles').addClass('menu-open');
			$('#ul-vehicles').css('display', 'block');
		});
	});
	
</script>
</body>
</html>
