<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-success">
					<div class="box-header ui-sortable-handle">
					    <i class="fa fa-users"></i>
              			<i class="fa fa-sign-in"></i>
             			<i class="fa fa-clock-o"></i>
						<h3 class="box-title">Agregar Perfil - Puerta - Horario</h3>
				  	</div>
					
					<form class="form-horizontal" id="addProfiles_Doors_Schedules">
			  			<div class="box-body">
			  				<div class="form-group">
			  					<label for="profile_id" class="col-sm-2 control-label">Perfil</label>
			  					<div class="col-sm-3">
			  						<select name="profile_id" id="profile_id" class="form-control" required>
			  							<option value="">Seleccione una opción</option>
		  								<?php 
		  									foreach ($profiles as $key) { ?>
		  										<option value="<?php echo $key->id; ?>"><?php echo $key->profile; ?></option>
			  							<?php } ?>
			  						</select>
			  					</div>
			  				</div>
			  				<div class="form-group">
			  					<label for="jornadas" class="col-sm-2 control-label">Jornadas</label>
			  					<div class="col-sm-3">
			  						<select name="jornadas" id="jornadas" class="form-control" required disabled>
			  							<option value="">Seleccione una opción</option>
		  								<?php
		  									foreach ($jornadas as $key) { ?>
		  										<option value="<?php echo $key->id; ?>"><?php echo $key->jornada; ?></option>
			  							<?php } ?>
			  						</select>
			  					</div>
			  				</div>
			  				<span>¡Al cambiar alguna de sus elección los cambios sin guardados se perderán!</span>
			  				<div id="cuerpo" class="hidden">
			  					<div class="box">
			  						<div class="box-header">
			  							<button type="button" class="btn btn-xs btn-success" id="marcar" value="1">Seleccionar todo</button>
			  						</div>
									<div class="box-body">
										<table class="table table-bordered table-striped table-condensed" style="width:100%;">
											<thead>
												<tr>
													<th>Puertas</th>
													<th>Inicio</th>
													<th>Término</th>
													<th>L</th>
													<th>M</th>
													<th>Mi</th>
													<th>J</th>
													<th>V</th>
													<th>S</th>
													<th>D</th>
												</tr>
											</thead>
											<tbody>
												<?php foreach ($doors as $key) { ?>
												<tr>
													<td width="40%">
														<input type="checkbox" id="<?php echo $key->id; ?>" value="<?php echo $key->id; ?>" name="door"> <?php echo $key->door; ?>
													</td>
													<td>
														<span name="init"></span>
													</td>
													<td>
														<span name="end"></span>
													</td>
													<td><input type="checkbox" data-name="semana" data-door="<?php echo $key->id; ?>"" name="L" disabled> <label>No</label></td>
													<td><input type="checkbox" data-name="semana" data-door="<?php echo $key->id; ?>"" name="M" disabled> <label>No</label></td>
													<td><input type="checkbox" data-name="semana" data-door="<?php echo $key->id; ?>"" name="Mi" disabled> <label>No</label></td>
													<td><input type="checkbox" data-name="semana" data-door="<?php echo $key->id; ?>"" name="J" disabled> <label>No</label></td>
													<td><input type="checkbox" data-name="semana" data-door="<?php echo $key->id; ?>"" name="V" disabled> <label>No</label></td>
													<td><input type="checkbox" data-name="semana" data-door="<?php echo $key->id; ?>"" name="S" disabled> <label>No</label></td>
													<td><input type="checkbox" data-name="semana" data-door="<?php echo $key->id; ?>"" name="D" disabled> <label>No</label></td>
												</tr>
												<?php } ?>
											</tbody>
										</table>
									</div>
								</div>
			  				</div>
			  			</div>
			  			<div class="box-footer">
			  				<button type="submit" class="btn btn-primary pull-right">Guardar</button>
			  			</div>
			  		</form>
				</div>
			</div>
		</div>
	</section>
	
</div>

<?php $this->view('footer'); ?>

<script>

	var seleccion;
	var texti;
	var texte;
	var doors = [];
	var jornadas = <?php echo json_encode($jornadas); ?>;
	var pro_doo_sche = <?php echo json_encode($pro_doo_sche); ?>;
	var perfil;
	var jornada;

	$("#marcar").on('click', function(){
		if ($(this).val() == 1) {
			$("input:checkbox").each(function(index, el) {
				$(this).prop('checked', true);
				$(this).prop('disabled', false)
				$(this).next().text('Si');
			});
			$(this).val(0)
			$(this).text('Deseleccionar todo');
		}
		else {
			$("input:checkbox").each(function(index, el) {
				$(this).prop('checked', false);
				$(this).prop('disabled', true)
				$(this).next().text('No');
			});
			$(this).val(1)
			$(this).text('Seleccionar todo');
		}	
	})

	$("#profile_id").on('change', function() {
		$("select[name='jornadas']").prop('selectedIndex', 0)
		$("#cuerpo").addClass('hidden');

		if ($("#profile_id").val()) {
			perfil = $("#profile_id").val();
			$("select[name='jornadas']").attr('disabled', false)
		}
		else {
			$("select[name='jornadas']").attr('disabled', true)
			perfil = 0;
		}
	})

	var previo;
	$("select[name='jornadas']").focus(function(){
		previo = $(this).val()
	}).on('change', function(){
		// if(parseInt($("select[name='jornadas']").val()) > 0 && previo.length > 0) {
		// 	alert('¡Los cambios sin guardados se perderán!');
		// }
		jornada = $("select[name='jornadas']").val();
			
		$("input:checkbox[name='door']").each(function(index, el) {
			$(this).prop('checked', false);
			for (var i = 0; i < pro_doo_sche.length; i++) {
				if ($("#profile_id").val() == pro_doo_sche[i].profiles_people_id && $("#jornadas").val() == pro_doo_sche[i].jornada_id && pro_doo_sche[i].doors_id == $(this).val()) {
					$(this).prop('checked', true);
				}
			}
			var id = $(this).attr('id')
			if ($(this).prop('checked')) {
				$("input:checkbox[data-name='semana']").each(function(index, el) {
					if ($(this).data('door') == id) {
						$(this).prop('disabled', false)
					}
				});
			}
		});

		$("input:checkbox[name='door']").on('change', function(){
			var id = $(this).attr('id')
			if ($(this).prop('checked')) {
				$("input:checkbox[data-name='semana']").each(function(index, el) {
					if ($(this).data('door') == id) {
						$(this).prop('disabled', false)
					}
				});
			}
			else if($(this).prop('checked') == false) {
				$("input:checkbox[data-name='semana']").each(function(index, el) {
					if ($(this).data('door') == id) {
						$(this).prop('disabled', true)
						$(this).prop('checked', false)
						$(this).next().text('No');
					}
				});
			}
		})
		
		///////////
		$("input:checkbox[data-name='semana']").each(function(index, el) {
			$(this).prop('checked', false);
			$(this).next().text('No');
			for (var i = 0; i < pro_doo_sche.length; i++) {
				if (pro_doo_sche[i].doors_id == $(this).data('door')) {
					if (pro_doo_sche[i].jornada_id == jornada && pro_doo_sche[i].profiles_people_id == perfil && pro_doo_sche[i].D1 == 1) {
						if ($(this).attr('name') == 'L') {
							$(this).prop('checked', true)
							$(this).next().text('Si')
						}
					}
					if (pro_doo_sche[i].jornada_id == jornada && pro_doo_sche[i].profiles_people_id == perfil && pro_doo_sche[i].D2 == 1) {
						if ($(this).attr('name') == 'M') {
							$(this).prop('checked', true)
							$(this).next().text('Si')
						}
					}
					if (pro_doo_sche[i].jornada_id == jornada && pro_doo_sche[i].profiles_people_id == perfil && pro_doo_sche[i].D3 == 1) {
						if ($(this).attr('name') == 'Mi') {
							$(this).prop('checked', true)
							$(this).next().text('Si')
						}
					}
					if (pro_doo_sche[i].jornada_id == jornada && pro_doo_sche[i].profiles_people_id == perfil && pro_doo_sche[i].D4 == 1) {
						if ($(this).attr('name') == 'J') {
							$(this).prop('checked', true)
							$(this).next().text('Si')
						}
					}
					if (pro_doo_sche[i].jornada_id == jornada && pro_doo_sche[i].profiles_people_id == perfil && pro_doo_sche[i].D5 == 1) {
						if ($(this).attr('name') == 'V') {
							$(this).prop('checked', true)
							$(this).next().text('Si')
						}
					}
					if (pro_doo_sche[i].jornada_id == jornada && pro_doo_sche[i].profiles_people_id == perfil && pro_doo_sche[i].D6 == 1) {
						if ($(this).attr('name') == 'S') {
							$(this).prop('checked', true)
							$(this).next().text('Si')
						}
					}
					if (pro_doo_sche[i].jornada_id == jornada && pro_doo_sche[i].profiles_people_id == perfil && pro_doo_sche[i].D7 == 1) {
						if ($(this).attr('name') == 'D') {
							$(this).prop('checked', true)
							$(this).next().text('Si')
						}
					}
				}
			}
		});
		///////

		$("#cuerpo").removeClass('hidden');
		seleccion = $(this).val();

		for (var i = 0; i < jornadas.length; i++) {
			if (seleccion == jornadas[i].id) {
				texti = ''+jornadas[i].time_init+'';
				texte = ''+jornadas[i].time_end+'';
				$("span[name='init']").each(function(index, el) {
					$(this).text(texti)
				});
				$("span[name='end']").each(function(index, el) {
					$(this).text(texte)
				});
			}
		}	
	})
	

	
	var door = [];
	$(document).ready(function() {
		$("input:checkbox[data-name='semana']").on('change', function(e){
			if ($(this).prop('checked')) {
				$(this).next().text('Si');
			}
			else {
				$(this).next().text('No');
			}	
		})

		$("#addProfiles_Doors_Schedules").submit(function(event) {
			event.preventDefault();

			$("input:checkbox[name='door']").each(function(index, el) {
				if ($(this).prop('checked')) {
					doors.push($(this).val())
				}
			});

			$("input:checkbox[data-name='semana']").each(function(index, el) {
				if ($(this).prop('checked') && doors.includes(String($(this).data('door')))) {
					door.push({puerta: $(this).data('door'), dia: $(this).attr('name'), estado: 1});
				}
				else if($(this).prop('checked') == false && doors.includes(String($(this).data('door')))) {
					door.push({puerta: $(this).data('door'), dia: $(this).attr('name'), estado: 0})
				}
			});

			$.post(
				site_url + "/cProfiles_Doors_Schedules/addProfile_Door_Schedule", {
					profiles_people_id 	: 	$("#profile_id").val(),
					jornada 			: 	seleccion,
					time_init 			: 	texti,
					time_end 			: 	texte,
					doors_id 			: 	doors,
					door 				: 	door
				},
				function(data){
					if (data == 1) {
						alert("Los registros se actualizaron correctamente...")
						window.location.replace(site_url+"/cProfiles_Doors_Schedules/add");
					}
					else {
						alert("Error en el proceso...")
						window.location.replace(site_url+"/cProfiles_Doors_Schedules/add");
					}
				}
			);
		});

		$('#li-people').addClass('menu-open');
		$('#ul-people').css('display', 'block');
	});
	
</script>
</body>
</html>
