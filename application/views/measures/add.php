<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-success">
					<div class="box-header ui-sortable-handle">
					    <i class="fa fa-balance-scale"></i>
						<h3 class="box-title">Agregar unidad medida</h3>
				  	</div>

				  	<form class="form-horizontal" id="addMeasures">
			  			<div class="box-body">
			  				<div class="form-group">
			  					<label for="measure" class="col-sm-2 control-label">Unidad medida</label>
			  					<div class="col-sm-10">
			  						<input type="text" class="form-control" name="measure" id="measure" required>
			  					</div>
			  				</div>
			  				<div class="form-group">
			  					<label for="acronimo" class="col-sm-2 control-label">Acrónimo</label>
			  					<div class="col-sm-10">
			  						<input type="text" class="form-control" name="acronimo" id="acronimo" required>
			  					</div>
			  				</div>
			  			</div>
			  			<div class="box-footer">
			  				<button type="submit" class="btn btn-primary pull-right">Guardar</button>
			  			</div>
			  		</form>
				</div>
			</div>
		</div>
	</section>
	
</div>

<?php $this->view('footer'); ?>

<script>
	$(document).ready(function() {
		$("#addMeasures").submit(function(event) {
			event.preventDefault();

			$.post(
				site_url + "/cMeasures/addMeasure",{
					measure 		: 	$("#measure").val(),
					acronimo		: 	$("#acronimo").val()
				},
				function(data){
					if (data == 1) {
						window.location.replace(site_url+"/cMeasures/");
					}
					else {
						alert("Error en el proceso...")
						window.location.replace(site_url+"/cMeasures/add");
					}
				}
			);
		});

		$('#li-form').addClass('menu-open');
		$('#ul-form').css('display', 'block');
	});
</script>
</body>
</html>
