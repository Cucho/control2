<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-success">
					<div class="box-header ui-sortable-handle">
					    <i class="fa fa-balance-scale"></i>
						<h3 class="box-title">Unidad medida</h3>
				  	</div>
					
					<div class="box-body">
			  			<section class="content">
			  			    <table class="table">
		  		    		    <tbody>
		  		    		    	<tr>
		  		    		    		<th>Id</th>
		  			    				<td><?php echo $measures[0]['id'];?></td>
		  		    		    	</tr>
		  		    		    	<tr>
		  		    		    		<th>Nombre</th>
		  			    				<td><?php echo $measures[0]['measure'];?></td>
		  		    		    	</tr>
		  		    		    	<tr>
		  		    		    		<th>Acrónimo</th>
		  			    				<td><?php echo $measures[0]['acronimo'];?></td>
		  		    		    	</tr>
		  		    		    </tbody>
		  			    	</table>
			  			</section>
				  	</div>
				  	<div class="box-footer">
				  		<a href="<?php echo site_url(); ?>/cMeasures/" class="btn btn-primary pull-right" role="button">
	                    <i class='fa fa-undo'></i> Volver
	                </a>
				  	</div>
				</div>
			</div>
		</div>
	</section>
	
</div>

<?php $this->view('footer'); ?>

<script>
    $(document).ready(function() {
		$('#li-form').addClass('menu-open');
		$('#ul-form').css('display', 'block');
    });
</script>
</body>
</html>