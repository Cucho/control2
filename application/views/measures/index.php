<div class="content-wrapper">
	<section class="content">
    <div class="row">
      <div class="col-sm-12">
        <div class="box box-success">
          <div class="box-header ui-sortable-handle">
              <i class="fa fa-balance-scale"></i>
            <h3 class="box-title">Unidad medida</h3>
            </div>

            <div class="box-body">
              <section class="content">
                <?php if ($this->session->userdata('save')) { ?>
                  <a  href="<?php echo site_url() ?>/cMeasures/add" class="btn btn-primary">Agregar</a><br><hr>
                <?php } ?>
                  <table id="sensors_type" class="table table-striped table-bordered table-condensed" style="width:100%;">
                      <thead>
                        <tr>
                          <th width="10%">ID</th>
                          <th>Unidad medida</th>
                          <th>Acción</th>
                        </tr>
                      </thead>
                      <tbody></tbody>
                  </table>
              </section>
            </div>
            <div class="box-footer"></div>
        </div>
      </div>
    </div>
  </section>
	
</div>

<?php $this->view('footer'); ?>

<script>
  var edit = <?php echo $this->session->userdata('edit'); ?>;
  var del = <?php echo $this->session->userdata('del'); ?>;

    $(document).ready(function() {
      $('#sensors_type').DataTable({
          "lengthMenu": [[5, 10, 15, 20,], [5, 10, 15, 20]],
          'responsive': true,
          'paging': true,
          'info': true,
          'filter': true,
          'ordering': true,
          // 'stateSave': true,
          'processing':true,
          'serverSide':true,
          'language': {
            "url": base_url + "assets/Spanish.json"
          },
          "order": [[0, "asc"]],
          'ajax': {
            "url": site_url + "/cMeasures/datatable",
            "type":"POST",
          },
          "columns": [
            { "data": "ID"},
            { "data": "Unidad medida" },
            { "data": "Acción" }
          ],
          "columnDefs": [
            {
              "targets": [0],
              "orderable": true,
              "render": function(data, type, row) {
                return row.id
              }
            },
            {
              "targets": [1],
              "orderable": true,
              "render": function(data, type, row) {
                return row.measure+' - '+row.acronimo
              }
            },
            {
              "targets": [2],
              "orderable": false,
              "render": function(data, type, row) {
                return edit == true && del == true ? `
                  <a href="<?php echo site_url(); ?>/cMeasures/view?id=`+row.id+`" class="btn btn-primary btn-xs" role="button">
                    <i class='fa fa-search'></i> Ver
                  </a>
                  <a href="<?php echo site_url(); ?>/cMeasures/edit?id=`+row.id+`" class="btn btn-warning btn-xs" role="button">
                      <i class='fa fa-pencil-square-o'></i> Editar
                  </a>
                  <a href="#" class="btn btn-danger btn-xs" role="button" onclick="delMeasure(`+row.id+`);">
                      <i class='fa fa-trash-o'></i> Eliminar
                  </a>` : edit == true && del == false ? `
                  <a href="<?php echo site_url(); ?>/cMeasures/view?id=`+row.id+`" class="btn btn-primary btn-xs" role="button">
                    <i class='fa fa-search'></i> Ver
                  </a>
                  <a href="<?php echo site_url(); ?>/cMeasures/edit?id=`+row.id+`" class="btn btn-warning btn-xs" role="button">
                      <i class='fa fa-pencil-square-o'></i> Editar
                  </a>` : edit == false && del == true ? `
                  <a href="<?php echo site_url(); ?>/cMeasures/view?id=`+row.id+`" class="btn btn-primary btn-xs" role="button">
                    <i class='fa fa-search'></i> Ver
                  </a>
                  <a href="#" class="btn btn-danger btn-xs" role="button" onclick="delMeasure(`+row.id+`);">
                      <i class='fa fa-trash-o'></i> Eliminar
                  </a>` : edit == false && del == false ? `
                  <a href="<?php echo site_url(); ?>/cMeasures/view?id=`+row.id+`" class="btn btn-primary btn-xs" role="button">
                    <i class='fa fa-search'></i> Ver
                  </a>` : '';
              }
            }
           ],
        });
      $('#li-form').addClass('menu-open');
      $('#ul-form').css('display', 'block');
    });

    function delMeasure(id) {
      if (confirm('¡Seguro de eliminar!')) {
        $.post(
          site_url + "/cMeasures/deleteMeasure",{
            id  :   id
          },
          function(data){
            if (data == 1) {
              window.location.reload();
            }
            else {
              alert("Este registro posee dependencias asociadas.\nNo se puede eliminar.")
            }
          }
        );
      }
    }
</script>

</body>
</html>
