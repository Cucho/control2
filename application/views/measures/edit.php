<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-success">
					<div class="box-header ui-sortable-handle">
					    <i class="fa fa-balance-scale"></i>
						<h3 class="box-title">Editar unidad medida #<?php echo $measures[0]['id']; ?></h3>
				  	</div>
					<form class="form-horizontal" id="editMeasures">
			  			<div class="box-body">
			  				<div class="form-group">
			  					<label for="measure" class="col-sm-2 control-label">Nombre</label>
			  					<div class="col-sm-10">
			  						<input type="text" class="form-control" name="measure" id="measure" value="<?php echo $measures[0]['measure']; ?>" required>
			  					</div>
			  				</div>
			  				<div class="form-group">
			  					<label for="acronimo" class="col-sm-2 control-label">Acrónimo</label>
			  					<div class="col-sm-10">
			  						<input type="text" class="form-control" name="acronimo" id="acronimo" value="<?php echo $measures[0]['acronimo']; ?>" required>
			  					</div>
			  				</div>
			  			</div>
			  			<div class="box-footer">
			  				<button type="submit" class="btn btn-primary pull-right">Guardar</button>
			  			</div>
			  		</form>
				</div>
			</div>
		</div>
	</section>
	
</div>


<?php $this->view('footer'); ?>

<script>
	$(document).ready(function() {
		$("#editMeasures").submit(function(event) {
			event.preventDefault();

			$.post(
				site_url + "/cMeasures/editMeasure",{
					id 			: 	<?php echo $measures[0]['id']; ?>,
					measure 	: 	$("#measure").val(),
					acronimo	: 	$("#acronimo").val()
				},
				function(data){
					if (data == 1) {
						window.location.replace(site_url+"/cMeasures/");
					}
					else {
						alert("Error en el proceso...")
						window.location.replace(site_url+"/cMeasures/edit?id="+<?php echo $measures[0]['id']; ?>);
					}
				}
			);
		});

		$('#li-form').addClass('menu-open');
      	$('#ul-form').css('display', 'block');
	});
	
</script>
</body>
</html>
