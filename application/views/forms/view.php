<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-success">
					<div class="box-header ui-sortable-handle">
					    <i class="fa fa-file-text"></i>
						<h3 class="box-title">Formulario</h3>
				  	</div>
						
				  	<div class="box-body">
			  			<section class="content">
			  			    <table id="forms" class="table">
		  		    		    <tbody>
		  		    		    	<tr>
		  		    		    		<th>Id</th>
		  			    					<td><?php echo $forms[0]['id'];?></td>
		  		    		    	</tr>
		  		    		    	<tr>
		  		    		    		<th>Puerta</th>
		  			    					<td><?php echo $forms[0]['title'];?></td>
		  		    		    	</tr>
		  		    		    	<tr>
		  		    	    			<th>Descripción</th>
		  		    	    			<td><?php echo $forms[0]['description'];?></td>
		  		    	    		</tr>
		  		    	    		<tr>
		  		    	    			<th>Creado</th>
		  		    	    			<td><?php echo $forms[0]['created'];?></td>
		  		    	    		</tr>
		  		    	    		<tr>
		  		    	    			<th>Modificado</th>
		  		    	    			<td><?php echo $forms[0]['modified'];?></td>
		  		    	    		</tr>
										<?php for ($i=0; $i < count($forms); $i++) { ?>
											<tr>
												<th>Pregunta <?php echo $i+1 ?></th>
												<th>Tipo Respuesta</th>
												<th>Respuesta ejemplo</th>
											</tr>
											<tr>
												<td><?php echo $forms[$i]['question'] ?></td>
												<td><?php echo $forms[$i]['type'] ?></td>
												<td><?php echo $forms[$i]['placeholder'] ?></td>
											</tr>
										<?php } ?>
		  		    		    </tbody>
		  			    	</table>
			  			</section>
				  	</div>
				  	<div class="box-footer">
				  		<a href="<?php echo site_url(); ?>/cForms/" class="btn btn-primary pull-right" role="button">
	                    <i class='fa fa-undo'></i> Volver
	                </a>
				  	</div>
				</div>
			</div>
		</div>
	</section>

</div>

<?php $this->view('footer'); ?>

<script>
    $(document).ready(function() {
      	$('#li-form').addClass('menu-open');
		$('#ul-form').css('display', 'block');
    });
</script>
</body>
</html>
