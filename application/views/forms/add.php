<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-success">
					<div class="box-header ui-sortable-handle">
					    <i class="fa fa-file-text"></i>
						<h3 class="box-title">Agregar formulario</h3>
				  	</div>

				  	<form class="form-horizontal" id="addForms">
			  			<div class="box-body">
			  				<div class="form-group">
			  					<label for="title" class="col-sm-2 control-label">Título</label>
			  					<div class="col-sm-10">
			  						<input type="text" class="form-control" name="title" id="title" required>
			  					</div>
			  				</div>
			  				<div class="form-group">
			  					<label for="description" class="col-sm-2 control-label">Descripción</label>
			  					<div class="col-sm-10">
			  						<textarea class="form-control textarea" name="description" id="description" rows="5"></textarea>
			  					</div>
			  				</div>
								<div id="addQuestions"></div>
								<button type="button" class="btn btn-success" onclick="addQuestions()"><i class="fa fa-plus-circle"></i> Agregar Pregunta</button>

							</div>
							<div class="box-footer">
			  				<button type="submit" class="btn btn-primary pull-right">Guardar</button>
			  			</div>
			  		</form>
				</div>
			</div>
		</div>
	</section>

</div>

<?php $this->view('footer'); ?>

<script>

	var count = 0
	var question = []
	var answers_type = []
	var answers_placeholder = []
	var measure = []

	$(document).on('change', '.abs', function(){
		if ($(this).val() == 3) {
			var elemento = '#measures'+count;
			$(elemento).attr('class', 'form-group')
			$(elemento).attr('requerid', true)
		}
		else if ($(this).val() != 3) {
			var elemento = '#measures'+count;
			$(elemento).attr('class', 'form-group hidden')
			$(elemento).attr('requerid', false)
		}
	})


	$(document).ready(function() {
		$("#addForms").submit(function(event) {
			event.preventDefault();

			captura()

			$.post(
				site_url + "/cForms/addForm",{
					title 					: $("#title").val(),
					description 			: $("#description").val(),
					orders 					: count,
					questions 				: question,
					answers_types 			: answers_type,
					answers_placeholders 	: answers_placeholder,
					measures 				: measure
				},
				function(data){
					if (data == 1) {
						window.location.replace(site_url+"/cForms/");
					}
					else {
						alert("Error en el proceso...")
						window.location.replace(site_url+"/cForms/add");
					}
				}
			);

		});

		$('#li-form').addClass('menu-open');
		$('#ul-form').css('display', 'block');
	});

	function addQuestions() {
		count += 1
		$("#addQuestions").append(
			function() {
				return `<hr>
					<div class="form-group">
						<label for="question" class="col-sm-2 control-label">Pregunta</label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="question`+count+`" required>
						</div>
					</div>
					<div class="form-group">
						<label for="answers_type" class="col-sm-2 control-label">Tipo respuesta</label>
						<div class="col-sm-5">
							<select name="answers_type" id="answers_type`+count+`" class="form-control abs" required>
								<option value="">Seleccione una opción</option>
								<?php foreach ($answers as $key) { ?>
									<option value="<?php echo $key->id; ?>"><?php echo $key->type; ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="form-group hidden" id="measures`+count+`">
						<label for="measure" class="col-sm-2 control-label">Unidad Medida</label>
						<div class="col-sm-5">
							<select name="measure" id="measure`+count+`" class="form-control">
								<option value="">Seleccione una opción</option>
								<?php foreach ($measures as $key) { ?>
									<option value='<?php echo $key->id; ?>'><?php echo $key->measure ?></option>
							 	<?php } ?>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="answers_placeholder" class="col-sm-2 control-label"></label>
						<div class="col-sm-10">
							<input type="text" class="form-control" id="answers_placeholder`+count+`" required placeholder="Escriba un ejemplo de la respuesta esperada">
						</div>
					</div>`
			}
		)
	}

	function captura(){
		for (var i = 1; i <= count; i++) {
			question.push($("#question"+i).val())
			answers_type.push($("#answers_type"+i).val())
			if ($("#answers_type"+i).val() == 3) {
				measure.push($("#measure"+i).val())
			}
			else {
				measure.push(0)
			}
			answers_placeholder.push($("#answers_placeholder"+i).val())
		}
	}

</script>
</body>
</html>
