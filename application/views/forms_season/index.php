<div class="content-wrapper">
	<section class="content">
    <div class="row">
      <div class="col-sm-12">
        <div class="box box-success">
          <div class="box-header ui-sortable-handle">
              <i class="fa fa-file-text"></i>
              <i class="fa fa-dashboard"></i>
              <h3 class="box-title">Formulario - Temporada</h3>
          </div>

          <div class="box-body">
            <section class="content">
                <table id="forms_season" class="table table-striped table-bordered table-condensed" style="width:100%;">
                    <thead>
                      <tr>
                        <th>Tipo vehículo</th>
                        <th>Formulario</th>
                        <th>Temporada</th>
                        <th>Año</th>
                        <th>Acción</th>
                      </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </section>
          </div>
          <div class="box-footer"></div>
        </div>
      </div>
    </div>
  </section>
	
</div>

<?php $this->view('footer'); ?>

<script>
  var del = <?php echo $this->session->userdata('del'); ?>;

    $(document).ready(function() {
      $('#forms_season').DataTable({
          "lengthMenu": [[5, 10, 15, 20,], [5, 10, 15, 20]],
          'responsive': true,
          'paging': true,
          'info': true,
          'filter': true,
          'ordering': true,
          // 'stateSave': true,
          'processing':true,
          'serverSide':true,
          'language': {
            "url": base_url + "assets/Spanish.json"
          },
          "order": [[0, "asc"]],
          'ajax': {
            "url": site_url + "/cForms_Season/datatable",
            "type":"POST",
          },
          "columns": [
            { "data": "Tipo vehículo" },
            { "data": "Formulario" },
            { "data": "Temporada" },
            { "data": "Año" },
            { "data": "Acción" }
          ],
          "columnDefs": [
            {
              "targets": [0],
              "orderable": true,
              "render": function(data, type, row) {
                return row.type
              }
            },
            {
              "targets": [1],
              "orderable": true,
              "render": function(data, type, row) {
                return row.title
              }
            },
            {
              "targets": [2],
              "orderable": true,
              "render": function(data, type, row) {
                return row.season
              }
            },
            {
              "targets": [3],
              "orderable": true,
              "render": function(data, type, row) {
                return row.year
              }
            },
            {
              "targets": [4],
              "orderable": false,
              "render": function(data, type, row) {
                return del == true ? `
                  <a href="#" class="btn btn-danger btn-xs" role="button" onclick="delForms_Season(`+row.vechiles_type_id+','+ row.forms_id+','+ row.seasons_id+','+ row.year+`);">
                    <i class='fa fa-trash-o'></i> Eliminar
                </a>` : '';
              }
            }
           ],
        });
      $('#li-form').addClass('menu-open');
      $('#ul-form').css('display', 'block');
    });

    function delForms_Season(vechiles_type_id, forms_id, seasons_id, year) {
      if (confirm('¡Seguro de eliminar!')) {
        $.post(
          site_url + "/cForms_Season/delForms_Season",{
            vechiles_type_id  :   vechiles_type_id,
            forms_id          :   forms_id,
            seasons_id        :   seasons_id,
            year              :   year
        },
        function(data){
          if (data == 1) {
            window.location.reload();
          }
          else {
            alert("Error en el proceso...")
          }
        }
      );
    }
  }
</script>

</body>
</html>
