<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-success">
					<div class="box-header ui-sortable-handle">
					    <i class="fa fa-file-text"></i>
              			<i class="fa fa-dashboard"></i>
						<h3 class="box-title">Vincular Formulario - Temporada</h3>
				  	</div>
					
				  	<form class="form-horizontal" id="addForms_Season">
			  			<div class="box-body">
			  				<div class="form-group">
			  					<label for="vehicle_type" class="col-sm-2 control-label">Tipo vehículo</label>
			  					<div class="col-sm-5">
			  						<select name="vehicle_type" id="vehicle_type" class="form-control" required>
			  							<option value="">Seleccione una opción</option>
		  								<?php 
		  									foreach ($vehicles_type as $key) { ?>
		  										<option value="<?php echo $key->id; ?>"><?php echo $key->type; ?></option>
			  							<?php } ?>
			  						</select>
			  					</div>
			  				</div>
			  				<div class="form-group">
			  					<label for="forms" class="col-sm-2 control-label">Formulario</label>
			  					<div class="col-sm-5">
			  						<select name="forms" id="forms" class="form-control" required>
			  							<option value="">Seleccione una opción</option>
		  								<?php 
		  									foreach ($forms as $key) { ?>
		  										<option value="<?php echo $key->id; ?>"><?php echo $key->title; ?></option>
			  							<?php } ?>
			  						</select>
			  					</div>
			  				</div>
			  				<div class="form-group">
			  					<label for="seasons" class="col-sm-2 control-label">Temporada</label>
			  					<div class="col-sm-5">
			  						<select name="seasons" id="seasons" class="form-control" required>
			  							<option value="">Seleccione una opción</option>
		  								<?php 
		  									foreach ($seasons as $key) { ?>
		  										<option value="<?php echo $key->id; ?>"><?php echo $key->season; ?></option>
			  							<?php } ?>
			  						</select>
			  					</div>
			  				</div>
			  				<div class="form-group">
			  					<label for="year" class="col-sm-2 control-label">Año</label>
			  					<div class="col-sm-3">
			  						<input type="number" class="form-control" name="year" id="year" min="<?php echo date("Y"); ?>" required>
			  					</div>
			  				</div>
			  			</div>
			  			<div class="box-footer">
			  				<button type="submit" class="btn btn-primary pull-right">Guardar</button>
			  			</div>
			  		</form>
				</div>
			</div>
		</div>
	</section>
	
</div>

<?php $this->view('footer'); ?>

<script>
	$(document).ready(function() {


		$("#addForms_Season").submit(function(event) {
			event.preventDefault();

			$.post(
				site_url + "/cForms_Season/addForm_Season",{
					vechiles_type_id 	: 	$("#vehicle_type").val(),
					forms_id 			: 	$("#forms").val(),
					seasons_id 			: 	$("#seasons").val(),
					year 				: 	$("#year").val()
				},
				function(data){
					if (data == 1) {
						window.location.replace(site_url+"/cForms_Season/");
					}
					else {
						alert("Error en el proceso...")
						window.location.replace(site_url+"/cForms_Season/add");
					}
				}
			);
		});

		$('#li-form').addClass('menu-open');
		$('#ul-form').css('display', 'block');
	});
</script>
</body>
</html>
