<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-success">
					<div class="box-header ui-sortable-handle">
					    <i class="fa fa-industry"></i>
						<h3 class="box-title">Empresas</h3>
				  	</div>

				  	<div class="box-body">
			  			<section class="content">
			  			    <table id="Companies" class="table">
		  		    		    <tbody>
		  		    		    	<tr>
		  		    		    		<th>Id</th>
		  			    				<td><?php echo $company[0]['id'];?></td>
		  		    		    	</tr>
		  		    		    	<tr>
		  		    		    		<th>Empresa</th>
		  			    				<td><?php echo $company[0]['company'];?></td>
		  		    		    	</tr>
		  		    		    	<tr>
		  		    		    		<th>Dirección</th>
		  		    		    		<td><?php echo $company[0]['address'];?></td>
		  		    		    	</tr>
		  		    		    	<tr>
		  		    		    		<th>Teléfono</th>
		  		    		    		<td><?php echo $company[0]['phone'];?></td>
		  		    		    	</tr>
		  		    		    	<tr>
		  		    		    		<th>E-Mail</th>
		  		    		    		<td><?php echo $company[0]['email'];?></td>
		  		    		    	</tr>
		  		    		    	<tr>
		  		    		    		<th>Contacto</th>
		  		    		    		<td><?php echo $company[0]['contact'];?></td>
		  		    		    	</tr>
		  		    		    	<tr>
		  		    		    		<th>Interno</th>
		  		    		    		<td><?php
		  		    		    		if($company[0]['internal'] == 0)
		  		    		    			echo 'Visita';
		  		    		    		else if($company[0]['internal'] == 1)
		  		    		    			echo 'Interna';
		  		    		    		else
		  		    		    			echo 'Contratista';
		  		    		    		?></td>
		  		    		    	</tr>
		  		    	    		<tr>
		  		    	    			<th>Creado</th>
		  		    	    			<td><?php echo $company[0]['created'];?></td>
		  		    	    		</tr>
		  		    	    		<tr>
		  		    	    			<th>Modificado</th>
		  		    	    			<td><?php echo $company[0]['modified'];?></td>
		  		    	    		</tr>
		  			    		</tbody>
		  			    	</table>
			  			</section>
				  	</div>
				  	<div class="box-footer">
				  		<a href="<?php echo site_url(); ?>/cCompanies/" class="btn btn-primary pull-right" role="button">
	                    <i class='fa fa-undo'></i> Volver
	                </a>
				  	</div>
				</div>
			</div>
		</div>
	</section>
	
</div>

<?php $this->view('footer'); ?>

<script>
    $(document).ready(function() {
      	$('#li-companies').addClass('menu-open');
		$('#ul-companies').css('display', 'block');
    });
  
</script>
</body>
</html>