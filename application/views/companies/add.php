<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-success">
					<div class="box-header ui-sortable-handle">
					    <i class="fa fa-industry"></i>
						<h3 class="box-title">Agregar empresa</h3>
				  	</div>

				  	<form class="form-horizontal" id="addCompanies">
			  			<div class="box-body">
			  				<div class="form-group">
			  					<label for="company" class="col-sm-2 control-label">Empresa</label>
			  					<div class="col-sm-10">
			  						<input type="text" class="form-control" name="company" id="company" required>
			  					</div>
			  				</div>
			  				<div class="form-group">
			  					<label for="address" class="col-sm-2 control-label">Dirección</label>
			  					<div class="col-sm-10">
			  						<input type="text" class="form-control" name="address" id="address" required>
			  					</div>
			  				</div>
			  				<div class="form-group">
			  					<label for="phone" class="col-sm-2 control-label">Teléfono</label>
			  					<div class="col-sm-10">
			  						<input type="tel" class="form-control" name="phone" r id="phone" required>
			  					</div>
			  				</div>
			  				<div class="form-group">
			  					<label for="email" class="col-sm-2 control-label">E-mail</label>
			  					<div class="col-sm-10">
			  						<input type="email" class="form-control" name="email" r id="email" required>
			  					</div>
			  				</div>
			  				<div class="form-group">
			  					<label for="contact" class="col-sm-2 control-label">Contacto</label>
			  					<div class="col-sm-10">
			  						<input type="text" class="form-control" name="contact" id="contact" required>
			  					</div>
			  				</div>
			  				<div class="form-group">
			  					<label for="internal" class="col-sm-2 control-label">Interno</label>
			  					<div class="col-sm-10">
			  						<label class="radio-inline">
								  		<input type="radio" name="internal" id="internal1" value="1" required> Interno
									</label>
									<label class="radio-inline">
										<input type="radio" name="internal" id="internal2" value="0" required> Externo Visita
									</label>
									<label class="radio-inline">
										<input type="radio" name="internal" id="internal3" value="2" required> Externo Contratista
									</label>
			  					</div>
			  				</div>
			  			</div>
			  			<div class="box-footer">
			  				<button type="submit" class="btn btn-primary pull-right">Guardar</button>
			  			</div>
			  		</form>
				</div>
			</div>
		</div>
	</section>
	
</div>

<?php $this->view('footer'); ?>

<script>
	$(document).ready(function() {
		$("#addCompanies").submit(function(event) {
			event.preventDefault();

			$.post(
				site_url + "/cCompanies/addCompanies",{
					company : 	$("#company").val(),
					address : 	$("#address").val(),
					phone 	: 	$("#phone").val(),
					email 	: 	$("#email").val(),
					contact : 	$("#contact").val(),
					internal: 	$('input:radio[name=internal]:checked').val()
				},
				function(data){
					if (data == 1) {
						window.location.replace(site_url+"/cCompanies/");
					}
					else {
						alert("Error en el proceso...")
						window.location.replace(site_url+"/cCompanies/add");
					}
				}
			);
		});

		$('#li-companies').addClass('menu-open');
		$('#ul-companies').css('display', 'block');
	});
</script>
</body>
</html>
