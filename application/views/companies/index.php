<div class="content-wrapper">
	<section class="content">
    <div class="row">
      <div class="col-sm-12">
        <div class="box box-success">
          <div class="box-header ui-sortable-handle">
              <i class="fa fa-industry"></i>
            <h3 class="box-title">Empresas</h3>
            </div>

            <div class="box-body">
              <section class="content">
                <?php if ($this->session->userdata('save')) { ?>
                  <a href="<?php echo site_url() ?>/cCompanies/add" class="btn btn-primary">Agregar</a><br><hr>
                <?php } ?>
                  <table id="Companies" class="table table-striped table-bordered table-condensed" style="width:100%;">
                      <thead>
                        <tr>
                          <th width="10%">ID</th>
                          <th>Empresa</th>
                          <th>Dirección</th>
                          <th>Teléfono</th>
                          <th>E-Mail</th>
                          <th>Contacto</th>
                          <th>Acción</th>
                        </tr>
                      </thead>
                      <tbody></tbody>
                  </table>
              </section>
            </div>
            <div class="box-footer"></div>
        </div>
      </div>
    </div>
  </section>
	
</div>

<?php $this->view('footer'); ?>

<script>
  var edit = <?php echo $this->session->userdata('edit'); ?>;
  var del = <?php echo $this->session->userdata('del'); ?>;

    $(document).ready(function() {
      $('#Companies').DataTable({
          "lengthMenu": [[5, 10, 15, 20,], [5, 10, 15, 20]],
          'responsive': true,
          'paging': true,
          'info': true,
          'filter': true,
          'ordering': true,
          // 'stateSave': true,
          'processing':true,
          'serverSide':true,
          'language': {
            "url": base_url + "assets/Spanish.json"
          },
          "order": [[0, "asc"]],
          'ajax': {
            "url": site_url + "/cCompanies/datatable",
            "type":"POST",
          },
          "columns": [
            { "data": "ID"},
            { "data": "Empresa" },
            { "data": "Dirección" },
            { "data": "Teléfono" },
            { "data": "E-Mail" },
            { "data": "Contacto" },
            { "data": "Acción" },
          ],
          "columnDefs": [
            {
              "targets": [0],
              "orderable": true,
              "render": function(data, type, row) {
                return row.id
              }
            },
            {
              "targets": [1],
              "orderable": true,
              "render": function(data, type, row) {
                return row.company
              }
            },
            {
              "targets": [2],
              "orderable": false,
              "render": function(data, type, row) {
                return row.address
              }
            },
            {
              "targets": [3],
              "orderable": false,
              "render": function(data, type, row) {
                return row.phone
              }
            },
            {
              "targets": [4],
              "orderable": false,
              "render": function(data, type, row) {
                return row.email
              }
            },
            {
              "targets": [5],
              "orderable": false,
              "render": function(data, type, row) {
                return row.contact
              }
            },
            {
              "targets": [6],
              "orderable": false,
              "render": function(data, type, row) {
                return edit == true && del == true ? `
                  <a href="<?php echo site_url(); ?>/cCompanies/view?id=`+row.id+`" class="btn btn-primary btn-xs" role="button">
                    <i class='fa fa-search'></i> Ver
                  </a>
                  <a href="<?php echo site_url(); ?>/cCompanies/edit?id=`+row.id+`" class="btn btn-warning btn-xs" role="button">
                      <i class='fa fa-pencil-square-o'></i> Editar
                  </a>
                  <a href="#" class="btn btn-danger btn-xs" role="button" onclick="delCompanies(`+row.id+`);">
                      <i class='fa fa-trash-o'></i> Eliminar
                  </a>` : edit == true && del == false ? `
                  <a href="<?php echo site_url(); ?>/cCompanies/view?id=`+row.id+`" class="btn btn-primary btn-xs" role="button">
                    <i class='fa fa-search'></i> Ver
                  </a>
                  <a href="<?php echo site_url(); ?>/cCompanies/edit?id=`+row.id+`" class="btn btn-warning btn-xs" role="button">
                      <i class='fa fa-pencil-square-o'></i> Editar
                  </a>` : edit == false && del == true ? `
                  <a href="<?php echo site_url(); ?>/cCompanies/view?id=`+row.id+`" class="btn btn-primary btn-xs" role="button">
                    <i class='fa fa-search'></i> Ver
                  </a>
                  <a href="#" class="btn btn-danger btn-xs" role="button" onclick="delCompanies(`+row.id+`);">
                      <i class='fa fa-trash-o'></i> Eliminar
                  </a>` : edit == false && del == false ? `
                  <a href="<?php echo site_url(); ?>/cCompanies/view?id=`+row.id+`" class="btn btn-primary btn-xs" role="button">
                    <i class='fa fa-search'></i> Ver
                  </a>` : '';
              }
            }
           ],
        });
      $('#li-companies').addClass('menu-open');
      $('#ul-companies').css('display', 'block');
    });

    function delCompanies(id) {
      if (confirm('¡Seguro de eliminar!')) {
        $.post(
        site_url + "/cCompanies/deleteCompanies",{
        id  :   id
        },
        function(data){
          if (data == 1) {
            window.location.reload();
          }
          else {
            alert("Error en el proceso...")
          }
        }
      );
    }
  }
</script>

</body>
</html>
