<div class="content-wrapper">
	<section class="content">
    <div class="row">
      <div class="col-sm-12">
        <div class="box box-success">
          <div class="box-header ui-sortable-handle">
              <i class="fa fa-calendar"></i>
              <h3 class="box-title">Proyectos</h3>
          </div>

          <div class="box-body">
            <section class="content">
              <?php if ($this->session->userdata('save')) { ?>
                 <a href="<?php echo site_url() ?>/cProjects/add" class="btn btn-primary">Agregar</a><br><hr>
              <?php } ?>
                <table id="projects" class="table table-striped table-bordered table-condensed" style="width:100%;">
                    <thead>
                      <tr>
                        <th width="10%">ID</th>
                        <th>Título</th>
                        <th>Encargado</th>
                        <th>Fecha Inicio</th>
                        <th>Fecha Término</th>
                        <th>Acción</th>
                      </tr>
                    </thead>
                    <tbody></tbody>
                </table>
            </section>
          </div>
          <div class="box-footer"></div>
        </div>
      </div>
    </div>
  </section>
	
</div>

<?php $this->view('footer'); ?>

<script>
  var edit = <?php echo $this->session->userdata('edit'); ?>;
  var del = <?php echo $this->session->userdata('del'); ?>;

    $(document).ready(function() {
      $('#projects').DataTable({
          "lengthMenu": [[5, 10, 15, 20,], [5, 10, 15, 20]],
          'responsive': true,
          'paging': true,
          'info': true,
          'filter': true,
          'ordering': true,
          // 'stateSave': true,
          'processing':true,
          'serverSide':true,
          'language': {
            "url": base_url + "assets/Spanish.json"
          },
          "order": [[0, "asc"]],
          'ajax': {
            "url": site_url + "/cProjects/datatable",
            "type":"POST",
          },
          "columns": [
            { "data": "ID"},
            { "data": "Título"},
            { "data": "Encargado"},
            { "data": "Fecha Inicio"},
            { "data": "Fecha Término"},
            { "data": "Acción"}
          ],
          "columnDefs": [
            {
              "targets": [0],
              "orderable": true,
              "render": function(data, type, row) {
                return row.id
              }
            },
            {
              "targets": [1],
              "orderable": true,
              "render": function(data, type, row) {
                return row.title
              }
            },
            {
              "targets": [2],
              "orderable": true,
              "render": function(data, type, row) {
                return row.name+' '+row.last_name+' / '+row.company
              }
            },
            {
              "targets": [3],
              "orderable": true,
              "render": function(data, type, row) {
                return row.init
              }
            },
            {
              "targets": [4],
              "orderable": true,
              "render": function(data, type, row) {
                return row.end
              }
            },
            {
              "targets": [5],
              "orderable": false,
              "render": function(data, type, row) {
                return edit == true ? `
                  <a href="<?php echo site_url(); ?>/cProjects/view?id=`+row.id+`" class="btn btn-primary btn-xs" role="button">
                    <i class='fa fa-search'></i> Ver
                  </a>
                  <a href="<?php echo site_url(); ?>/cProjects/edit?id=`+row.id+`" class="btn btn-warning btn-xs" role="button">
                      <i class='fa fa-pencil-square-o'></i> Editar
                  </a>` : edit == false ? `
                  <a href="<?php echo site_url(); ?>/cProjects/view?id=`+row.id+`" class="btn btn-primary btn-xs" role="button">
                    <i class='fa fa-search'></i> Ver
                  </a>` : '';
              }
            }
           ],
        });

      $('#li-contractors').addClass('menu-open');
      $('#ul-contractors').css('display', 'block');
    });

    function delProject(id) {
      if (confirm('¡Seguro de eliminar!')) {
        $.post(
        site_url + "/cProjects/deleteProject",{
          id  :   id
        },
        function(data){
          if (data == 1) {
            window.location.reload();
          }
          else {
            alert("Error en el proceso...")
          }
        }
      );
    }
  }
</script>

</body>
</html>
