
<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-sm-12">
                <div class="box box-success">
                    <div class="box-header ui-sortable-handle">
                        <i class="fa fa-sign-in"></i>
                        <h3 class="box-title">Autorización Projecto Contratistas<small>> Datos de ingreso</small></h3>
                    </div>
                    <div class="box-body">
                        <div class="wizard wizard2">
                            <div class="wizard-inner">
                                <div class="connecting-line"></div>
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="active">
                                        <a href="#step1" data-toggle="tab" aria-controls="step1" role="tab" title="Persona/s">
                                            <span class="round-tab">
                                                <i class="fa fa-users"></i>
                                            </span>
                                        </a>
                                    </li>

                                    <li role="presentation" class="disabled">
                                        <a href="#step2" data-toggle="tab" aria-controls="step2" role="tab" title="Horarios">
                                            <span class="round-tab">
                                                <i class="fa fa-calendar"></i>
                                            </span>
                                        </a>
                                    </li>

                                    <li role="presentation" class="disabled">
                                        <a href="#step3" data-toggle="tab" aria-controls="step3" role="tab" title="Ubicaciones">
                                            <span class="round-tab">
                                                <i class="fa fa-map-marker"></i>
                                            </span>
                                        </a>
                                    </li>

                                    <li role="presentation" class="disabled">
                                        <a href="#complete" data-toggle="tab" aria-controls="complete" role="tab" title="Finalizar">
                                            <span class="round-tab">
                                                <i class="glyphicon glyphicon-ok"></i>
                                            </span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            
                            <form name="formProjects" id="formProjects">
                                <div class="tab-content">
                                    <div class="tab-pane active" role="tabpanel" id="step1">
                                        <div class="table-responsive">
                                            <fieldset>
                                                <legend>Datos Supervisor / Encargado:</legend>
                                                <table id="table-supervisor" class="table table-condensed" style="width:100%;">
                                                    <tr>
                                                      <td><label>Rut</label></td>
                                                      <td>
                                                        <input class="form-control" name="input-add-rut1" id="input-add-rut1" maxlength="9" data-id="" value="">
                                                      </td>
                                                    </tr>

                                                    <tr>
                                                      <td><label>Nombre</label></td>
                                                      <td>
                                                        <input readonly class="form-control" name="input-add-name" id="input-add-name" value="">
                                                      </td>
                                                    </tr>

                                                    <tr>
                                                      <td><label>Apellido</label></td>
                                                      <td>
                                                        <input readonly class="form-control" name="input-add-last_name" id="input-add-last_name" value="">
                                                      </td>
                                                    </tr>

                                                    <tr>
                                                      <td><label>Teléfono</label></td>
                                                      <td>
                                                        <input readonly class="form-control" name="input-add-phone" id="input-add-phone" value=""> 
                                                      </td>
                                                    </tr>

                                                    <tr>
                                                      <td><label>Email</label></td>
                                                      <td>
                                                        <input readonly type="email" class="form-control" name="input-add-email" id="input-add-email" value="">
                                                      </td>
                                                    </tr>

                                                    <tr>
                                                      <td><label>Perfil</label></td>
                                                      <td>
                                                       <input readonly class="form-control" name="input-add-profile" id="input-add-profile" value="">
                                                      </td>
                                                    </tr>

                                                    <tr>
                                                      <td><label>Empresa</label></td>
                                                      <td>
                                                        <input readonly class="form-control" name="input-add-company" id="input-add-company" value="">
                                                      </td>
                                                    </tr>
                                                </table>
                                            </fieldset>
                                            <br>
                                            <fieldset>
                                                <legend>Listado de contratistas: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <button class="btn btn-success btn-xs" type="button" onclick="openModalAddAditionalPeople(1);"><i class="fa fa-plus" aria-hidden="true"></i></button>
                                                </legend>
                                                <table id="table-add-aditional_contratista" class="table table-condensed" style="width:100%;">
                                                    <thead id="thead-aditional">
                                                        <tr>
                                                            <th>Rut</th>
                                                            <th>Nombre</th>
                                                            <th>Apellidos</th>
                                                            <th>Remover</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="tbody-aditional"></tbody>
                                                </table>
                                            </fieldset>
                                            <br>
                                            <fieldset>
                                                <legend>Datos del vehículo: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                    <button class="btn btn-success btn-xs" type="button" onclick="openModalAddAditionalVehicle();"><i class="fa fa-plus" aria-hidden="true"></i></button>
                                                </legend>
                                                <table id="table-add-vehicle" class="table table-condensed" style="width:100%;">
                                                    <thead>
                                                        <tr>
                                                            <th>Patente</th>
                                                            <th>Empresa</th>
                                                            <th>Responsable</th>
                                                            <th>Tipo</th>
                                                            <th>Control</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody id="tbody-vehicle"></tbody>
                                                </table>
                                            </fieldset>
                                            <br>
                                            <fieldset>
                                                <legend>Datos del Proyecto:</legend>
                                                <table class="table table-condensed" id="table-project">
                                                    <tr>
                                                        <td><label>Título</label></td>
                                                        <td><input class="form-control" type="text" name="title" id="title"></td>
                                                    </tr>
                                                    <tr>
                                                        <td><label>Descripción</label></td>
                                                        <td><textarea class="form-control textarea" name="description" id="description" cols="130" rows="5"></textarea></td>
                                                    </tr>
                                                    <tr>
                                                        <td><label>Fecha inicio</label></td>
                                                        <td><input type="date" id="init"></td>
                                                    </tr>
                                                    <tr>
                                                        <td><label>Fecha Fin</label></td>
                                                        <td><input type="date" id="end"></td>
                                                    </tr>
                                                </table>
                                            </fieldset>
                                        </div>
                                        <br>
                                        <ul class="list-inline pull-left">
                                            <li>
                                                <button type="button" class="btn btn-primary next-step">Guardar y continuar</button>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="tab-pane" role="tabpanel" id="step2">
                                        <fieldset>
                                            <legend>Proyecto Horario:</legend>
                                            <div id="cuerpo">
                                                <div class="box">
                                                    <div class="box-header">
                                                        <button type="button" class="btn btn-xs btn-success" id="mashorario" value="1"><i class="fa fa-plus"></i> Nuevo Horario</button>
                                                    </div>
                                                    <div class="box-body">
                                                        <table class="table table-bordered table-striped table-condensed" style="width:100%;">
                                                            <thead>
                                                                <tr>
                                                                    <th>Inicio</th>
                                                                    <th>Término</th>
                                                                    <th>L</th>
                                                                    <th>M</th>
                                                                    <th>Mi</th>
                                                                    <th>J</th>
                                                                    <th>V</th>
                                                                    <th>S</th>
                                                                    <th>D</th>
                                                                    <th></th>
                                                                </tr>
                                                            </thead>
                                                            <tbody id="tbody-horario">
                                                                <tr>
                                                                    <td>
                                                                        <input type="time" id="time_init" data-index="0" name="0">
                                                                    </td>
                                                                    <td>
                                                                        <input type="time" id="time_end" data-index="0" name="0">
                                                                    </td>
                                                                    <td><input type="checkbox" data-index="0" data-name="semana" name="L"></td>
                                                                    <td><input type="checkbox" data-index="0" data-name="semana" name="M"></td>
                                                                    <td><input type="checkbox" data-index="0" data-name="semana" name="Mi"></td>
                                                                    <td><input type="checkbox" data-index="0" data-name="semana" name="J"></td>
                                                                    <td><input type="checkbox" data-index="0" data-name="semana" name="V"></td>
                                                                    <td><input type="checkbox" data-index="0" data-name="semana" name="S"></td>
                                                                    <td><input type="checkbox" data-index="0" data-name="semana" name="D"></td>
                                                                    <td><button type="button" class="btn btn-xs btn-danger" disabled>Remover</button></td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <br>
                                        <ul class="list-inline pull-right">
                                            <li>
                                                <button type="button" class="btn btn-default prev-step">Anterior</button>
                                            </li>
                                            <li>
                                                <button type="button" class="btn btn-primary next-step">Guardar y continuar</button>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="tab-pane" role="tabpanel" id="step3">
                                        <fieldset>
                                            <legend>Encargado de faena</legend>
                                            <div class="col-md-5">
                                                <select name="internal" id="pinternal" class="form-control">
                                                <option value="">Seleccione una opción</option>
                                                <?php foreach ($peopleInternal as $k) { ?>
                                                    <option value="<?php echo $k->id; ?>">
                                                        <?php echo $k->name." ".$k->last_name; ?>
                                                    </option>
                                                <?php } ?>
                                            </select>
                                            </div>
                                        </fieldset>
                                        <br>
                                        <fieldset id="ubicacion">
                                            <legend>Ubicación:</legend>
                                            <div class="row">
                                                <div class="col-lg-4">
                                                  <div class="box no-border">
                                                    <div class="box-header"><label>Zonas</label></div>
                                                    <div class="box-body">
                                                        <?php foreach ($treeview['zona'] as $k) { ?>
                                                            <p><input type="checkbox" name="<?php echo $k['zid']; ?>" id="<?php echo $k['zid']; ?>" data-level="0" data-type="zone"> <?php echo $k['zone']; ?></p>
                                                        <?php } ?>
                                                    </div>
                                                  </div>
                                                </div>
                                                <div class="col-lg-4">
                                                  <div class="box no-border">
                                                    <div class="box-header"><label>Áreas</label></div>
                                                    <div class="box-body">
                                                        <?php foreach ($treeview['area'] as $k) { ?>
                                                            <p><input type="checkbox" name="<?php echo $k['area_parent']; ?>" id="<?php echo $k['aid']; ?>" data-level="1" data-type="area"> <?php echo $k['area']; ?></p>
                                                        <?php } ?>
                                                    </div>
                                                  </div>
                                                </div>
                                                <div class="col-lg-4">
                                                  <div class="box no-border">
                                                    <div class="box-header"><label>Departamentos</label></div>
                                                    <div class="box-body">
                                                        <?php foreach ($treeview['dpto'] as $k) { ?>
                                                            <p><input type="checkbox" name="<?php echo $k['department_parent']; ?>" id="<?php echo $k['did']; ?>" data-level="2" data-type="dpto"> <?php echo $k['department']; ?></p>
                                                        <?php }?>
                                                    </div>
                                                  </div>
                                                </div>
                                            </div>
                                        </fieldset>
                                        <br>
                                        <fieldset id="rutapuertas">
                                            <legend>Ruta:</legend>
                                                <?php for ($i=0; $i < count($doorlevel); $i++) { ?>
                                                <div class="row">
                                                    <?php foreach ($doorlevel as $k) { 
                                                        if ($k->level == $i) { ?>
                                                            <a href="#" style="color:#000" name="puertas" onMouseOver="this.style.color='#5cb85c'" onMouseOut="this.style.color='#000'" id="<?php echo $k->id; ?>" data-level="<?php echo $k->level; ?>">
                                                            <div class="col-md-3">
                                                                <div class="box box-solid box-default">
                                                                    <div class="box-header">
                                                                        <h5 class="box-title">Nivel <?php echo $k->level ?></h5>
                                                                    </div>
                                                                    <div class="box-body">
                                                                    <?php echo $k->door; ?> <i class="fa fa-sign-in fa-2x pull-right"></i>
                                                                        <div><label><small><?php echo $k->description; ?></small></label></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            </a>
                                                    <?php }} ?>
                                                </div>
                                            <?php } ?>
                                        </fieldset>
                                        <br>
                                        <ul class="list-inline pull-right">
                                            <li>
                                                <button type="button" class="btn btn-default prev-step">Anterior</button>
                                            </li>
                                            <li>
                                                <button type="button" class="btn btn-primary btn-info-full next-step">Guardar y continuar</button>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="tab-pane" role="tabpanel" id="complete">
                                        <fieldset>
                                            <legend>Requerimientos minimos: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <button class="btn btn-success btn-xs" type="button" onclick="openModalAddMRequirements();"><i class="fa fa-plus" aria-hidden="true"></i></button></legend>
                                            <div id="tre">
                                                <ul class="list list-group" id="ulrequirement">
                                                    <?php foreach ($minimum as $k) { ?>
                                                        <li class="list-group-item">
                                                            <div class="wrapepr">
                                                                <span>
                                                                    <label>
                                                                        <input type="checkbox" name="minimum" id="<?php echo $k->id; ?>"> &nbsp;&nbsp;
                                                                        <span> <?php echo $k->requirement; ?></span>
                                                                    </label>
                                                                </span>
                                                            </div>
                                                        </li>
                                                    <?php } ?>
                                                </ul>
                                            </div>
                                        </fieldset>
                                        <br>
                                        <fieldset>
                                            <legend>Adjuntar documentos:</legend>
                                            <div class="box">
                                                <div class="box-header">
                                                    <button type="button" class="btn btn-xs btn-success" onclick="addFormUpload()"><i class="fa fa-plus"></i> Subir archivos</button>
                                                </div>
                                                <div class="box-body" id="buploads">
                                                    
                                                </div>
                                                <div class="box-footer" id="fuploads">
                                                    
                                                </div>
                                            </div>
                                        </fieldset>
                                        <br>
                                        <div class="row">
                                            <div class="col-md-5 col-md-offset-3">
                                                <div id="mensaje-error" class="text text-danger"></div>
                                                <div class="box box-success">
                                                    <div class="box-header">
                                                        <h3>Procesar el registro del proyecto </h3></i>
                                                    </div>
                                                    <div class="box-body">
                                                        <div class="col-md-2 col-md-offset-4" id="div-question_loading">
                                                            <label class="text-primary"><i class="fa fa-question-circle-o fa-5x"></i></label>
                                                        </div>
                                                    </div>
                                                    <div class="box-footer">
                                                        <div class="col-md-8 col-md-offset-2">
                                                            <button type="submit" class="btn btn-success"">
                                                                <i class="fa fa-check"></i> Aceptar
                                                            </button>
                                                            <button type="button" class="btn btn-danger" onclick="btnCancelAdd()">
                                                                <i class="fa fa-times"></i> Cancelar
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<!-- MODAL -->
<div id="modal-add-people" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Contratista Adicional</h4>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <form id="formModal-add-people">
                    <table class="table table-condensed" style="width:100%;">
                        <tr>
                            <td><label>Rut</label></td>
                            <td>
                                <input type="text" class="form-control" name="rut" id="rut" maxlength="9" required disabled>
                            </td>
                        </tr>

                        <tr>
                            <td><label>Nombre</label></td>
                            <td>
                                <input type="text" class="form-control" name="name" id="name" required>
                            </td>
                        </tr>

                        <tr>
                            <td><label>Apellidos</label></td>
                            <td>
                                <input type="text" class="form-control" name="last_name" id="last_name" required>
                            </td>
                        </tr>

                        <tr>
                            <td><label>Dirección</label></td>
                            <td>
                                <input type="text" class="form-control" name="address" id="address" required>
                            </td>
                        </tr>

                        <tr>
                            <td><label>Email</label></td>
                            <td>
                                <input type="email" class="form-control" name="email" id="email" required>
                            </td>
                        </tr>

                        <tr>
                            <td><label>Teléfono</label></td>
                            <td>
                                <input type="number" class="form-control" name="phone" id="phone" required>
                            </td>
                        </tr>
                        
                        <tr>
                            <td><label>Perfil</label></td>
                            <td>
                                <select name="people_profiles" id="people_profiles" class="form-control" required>
                                    <?php foreach ($profiles as $k) { ?>
                                        <option value="<?php echo $k->id; ?>"><?php echo $k->profile; ?></option>
                                    <?php } ?>
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <td><label>Empresa</label></td>
                            <td>
                                <select name="companies" id="companies" class="form-control" required>
                                    <?php foreach ($companies as $k) { ?>
                                        <option value="<?php echo $k->id; ?>"><?php echo $k->company; ?></option>
                                    <?php } ?>
                                </select>
                            </td>
                        </tr>

                        <tr>
                            <td colspan="2">
                                <button type="submit" class="btn btn-primary" id="btnAgregar">Agregar</button>
                            </td>
                        </tr>
                    </table>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<div id="modal-add-vehicle" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Vehículo Contratista</h4>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <form id="formModal-add-vehicle">
                        <table class="table table-condensed" style="width:100%;">
                            <tr>
                                <td><label>Patente</label></td>
                                <td>
                                    <input type="text" class="form-control" name="patent" id="patent" maxlength="6" required placeholder="XXYY99">
                                </td>
                            </tr>

                            <tr>
                                <td><label>Modelo</label></td>
                                <td>
                                    <input type="text" class="form-control" name="model" id="model" disabled>
                                </td>
                            </tr>

                            <tr>
                                <td><label>Empresa</label></td>
                                <td>
                                    <select name="vcompanies" id="vcompanies" class="form-control" required disabled>
                                        <?php foreach ($companies as $k) { ?>
                                            <option value="<?php echo $k->id; ?>"><?php echo $k->company; ?></option>
                                        <?php } ?>
                                    </select>
                                </td>
                            </tr>

                            <tr>
                                <td><label>Responsable</label></td>
                                <td>
                                    <select name="vehicle_people" id="vehicle_people" class="form-control" required disabled>
                                        
                                    </select>
                                </td>
                            </tr>

                            <tr>
                                <td><label>Tipo vehículo</label></td>
                                <td>
                                    <select name="vechicle_type" id="vechicle_type" class="form-control" required disabled>
                                        <?php foreach ($vechicle_type as $k) { ?>
                                            <option value="<?php echo $k->id; ?>"><?php echo $k->type; ?></option>
                                        <?php } ?>
                                    </select>
                                </td>
                            </tr>

                            <tr>
                                <td><label>Vehículo perfil</label></td>
                                <td>
                                    <select name="vprofile" id="vprofile" class="form-control" required disabled>
                                        <?php foreach ($vehicle_profiles as $k) { ?>
                                            <option value="<?php echo $k->id; ?>"><?php echo $k->profile; ?></option>
                                        <?php } ?>
                                    </select>
                                </td>
                            </tr>

                            <tr>
                                <td colspan="2">
                                    <button type="submit" class="btn btn-primary" id="btnAgregarV">Agregar</button>
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<div id="modal-add-requirement" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Requerimientos minimos</h4>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <form id="formModal-add-requirement">
                        <table class="table table-condensed" style="width:100%;">
                            <tr>
                                <td><label>Requerimiento</label></td>
                                <td>
                                    <input type="text" class="form-control" name="requirement" id="requirement" required>
                                </td>
                            </tr>

                            <tr>
                                <td><label>Descripción</label></td>
                                <td>
                                    <input type="text" class="form-control" name="description" id="description">
                                </td>
                            </tr>

                            <tr>
                                <td colspan="2">
                                    <button type="submit" class="btn btn-primary" id="btnAgregarV">Agregar</button>
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<?php $this->view('footer'); ?>

<script>
    var requirem = [];
    var fecha = '<?php echo date('Y-m-d') ?>';

    var contratistas = [];
    var vehicles = [];
    var vueltas = 0;
    var horarios = [];
    
    var wpuertas = [];
    var wareas = [];
    var wdptos = [];
    var wzones = [];

    var accion_form = 0;
    var profiles = <?php echo json_encode($profiles); ?>;
    var companies = <?php echo json_encode($companies); ?>;
    var people = [];
    var index = 1;

    var up = 0;
    var patent;
    var control_init;
    var control_end;
    
    $(document).ready(function() {
        $('#li-contractors').addClass('menu-open');
        $('#ul-contractors').css('display', 'block');

        $.widget.bridge('uibutton', $.ui.button);

        //Initialize tooltips
        $('.nav-tabs > li a[title]').tooltip();
        
        //Wizard
        $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
            var $target = $(e.target);
            if ($target.parent().hasClass('disabled')) {
                return false;
            }
        });

        $(".next-step").click(function (e) {
            var stepid = $('.wizard .nav-tabs li.active a').attr('aria-controls');
            if (validateformstep(stepid)) {
                var $active = $('.wizard .nav-tabs li.active');
                $active.next().removeClass('disabled');
                nextTab($active);
            }
        });

        $(".prev-step").click(function (e) {
            var $active = $('.wizard .nav-tabs li.active');
            prevTab($active);
        });

        var rut = document.getElementById('input-add-rut1');
        $("#input-add-rut1").keydown(function(event) {
            if (event.which == 13) {
                event.preventDefault()
                if (checkRut(rut)) {
                    $.ajax({
                        url: site_url + '/cProjects/searchPeople/',
                        type: 'post',
                        data: {
                            rut: rut.value.slice(0, -2)
                        },
                        dataType: 'json',
                        success: function(data) {
                            if(data.mensaje == 1) {
                                if (data.people[0].internal == 2 && data.people[0].states_id == 1) {
                                    if (!contratistas.includes(rut.value.slice(0, -2))) {
                                        contratistas.push(rut.value.slice(0, -2))
                                        people.push({rut: rut.value, nombre: data.people[0].name+' '+data.people[0].last_name})
                                        $("#input-add-rut1").attr('readonly', true)
                                        $("#input-add-name").val(data.people[0].name)
                                        $("#input-add-last_name").val(data.people[0].last_name)
                                        $("#input-add-phone").val(data.people[0].phone)
                                        $("#input-add-email").val(data.people[0].email)
                                        $("#input-add-profile").val(data.people[0].profile)
                                        $("#input-add-company").val(data.people[0].company)
                                        $("#input-add-rut1").prop('disabled', true)
                                    }
                                    else {
                                        alert('Ya existe en la lista contratistas adicionales.')
                                        $("#input-add-rut1").val('')
                                        $("#input-add-rut1").attr('readonly', false)
                                    }
                                }
                                else if (data.people[0].internal == 1) {
                                    alert('Esta persona esta registrada como interno')
                                    $("#input-add-rut1").val('')
                                    $("#input-add-rut1").focus()
                                }
                                else if (data.people[0].internal == 0) {
                                    alert('Esta personsa esta registrada como visitante')
                                    $("#input-add-rut1").val('')
                                    $("#input-add-rut1").focus()
                                }
                                else if (data.people[0].states_id == 2) {
                                    alert('Esta personsa esta suspendida')
                                    $("#input-add-rut1").val('')
                                    $("#input-add-rut1").focus()
                                }
                                else if (data.people[0].states_id == 3) {
                                    alert('Esta personsa esta bloqueada')
                                    $("#input-add-rut1").val('')
                                    $("#input-add-rut1").focus()
                                }
                            }
                            else if (data.mensaje == 0) {
                                openModalAddAditionalPeople(0)//lamada desde la tabla wizard
                                $('#rut').val(rut.value);
                                $('#rut').attr('disabled', true);
                                $('.modal-title').text('Contratista Responsable');
                                $('#name').attr('disabled', false);
                                $('#last_name').attr('disabled', false);
                                $('#address').attr('disabled', false);
                                $('#email').attr('disabled', false);
                                $('#phone').attr('disabled', false);
                                $('#people_profiles').attr('disabled', true);
                                $('#people_profiles').val('7');
                                $('#companies').attr('disabled', false);
                            }
                        }
                    });
                }
                else {
                    alert('RUT Inválido.');
                }
            }
        });

        $("#formModal-add-people").submit(function(event) {
            event.preventDefault();

            if (accion_form == 0) {
                var rut         = $('#rut').val();
                var digit       = rut[rut.length-1];
                rut             = rut.slice(0, -2);
                var name        = $('#name').val();
                var last_name   = $('#last_name').val();
                var address     = $('#address').val();
                var email       = $('#email').val();
                var phone       = $('#phone').val();
                var profile     = $('#people_profiles option:selected').val();
                var company     = $('#companies option:selected').val();

                $.ajax({
                    url: site_url + '/cProjects/addPeople/',
                    type: 'post',
                    data: {
                        rut                 : rut, 
                        digit               : digit, 
                        name                : name, 
                        last_name           : last_name, 
                        address             : address, 
                        email               : email, 
                        phone               : phone,
                        allow_all           : 0, 
                        is_visited          : 0, 
                        internal            : 2, 
                        people_profiles_id  : profile, 
                        companies_id        : company
                    },
                    dataType: 'text',
                    success: function(data) {
                        if(data == 1) {
                            $('#rut').val(rut+'-'+digit);
                            $("#input-add-name").val(name)
                            $("#input-add-last_name").val(last_name)
                            $("#input-add-phone").val(phone)
                            $("#input-add-email").val(email)
                            data = $.grep(profiles, function(obj){return obj.id === profile;})[0]
                            $("#input-add-profile").val(data.profile)
                            data = $.grep(companies, function(obj){return obj.id === company;})[0]
                            $("#input-add-company").val(data.company)
                            contratistas.push(rut+'-'+digit)
                            people.push({rut: rut+'-'+digit, nombre: name+' '+last_name})
                            accion_form = 1;
                            $("#rut").val('')
                            $("#modal-add-people").modal('hide');

                        }
                        else {
                            alert("Error en el proceso...")
                            window.location.replace(site_url+"/cProjects/");
                        }
                    }
                });
            }
            else if(accion_form == 1){
                var rut = document.getElementById('rut');
                if (checkRut(rut)) {
                    $.ajax({
                        url: site_url + '/cProjects/searchPeople/',
                        type: 'post',
                        data: {
                            rut: rut.value.slice(0, -2)
                        },
                        dataType: 'json',
                        success: function(data) {
                            if(data.mensaje == 1) {
                                if (data.people[0].internal == 2 && data.people[0].states_id == 1) {
                                    if (!contratistas.includes(rut.value.slice(0, -2))) {
                                        contratistas.push(rut.value.slice(0, -2))
                                        people.push({rut: rut.value, nombre: data.people[0].name+' '+data.people[0].last_name})
                                        var html = ``;
                                        html += '<tr id="tr-'+rut.value.slice(0, -2)+'" name="aditionalpeople">';
                                        html += '<td>'+rut.value+'</td>';
                                        html += '<td>'+data.people[0].name+'</td>';
                                        html += '<td>'+data.people[0].last_name+'</td>';
                                        html += '<td><button type="button" class="btn btn-danger btn-xs" onclick="removeAditionalPeople('+rut.value.slice(0, -2)+');">remover</button></td>';
                                        html += '</tr>';
                                        $('#tbody-aditional').append(html);
                                        $("#rut").val('')
                                        $("#modal-add-people").modal('hide');
                                    }
                                    else {
                                        $("#rut").val('')
                                        $("#modal-add-people").modal('hide');
                                    }
                                }
                                else if (data.people[0].internal == 1) {
                                    alert('Esta personsa esta registrada como interno')
                                    $("#rut").val('')
                                    $("#rut").focus()
                                }
                                else if (data.people[0].internal == 0) {
                                    alert('Esta personsa esta registrada como visitante')
                                    $("#rut").val('')
                                    $("#rut").focus()
                                }
                                else if (data.people[0].states_id == 2) {
                                    alert('Esta personsa esta suspendida')
                                    $("#rut").val('')
                                    $("#rut").focus()
                                }
                                else if (data.people[0].states_id == 3) {
                                    alert('Esta personsa esta bloqueada')
                                    $("#rut").val('')
                                    $("#rut").focus()
                                }
                            }
                            else if (data.mensaje == 0) {
                                openModalAddAditionalPeople(0)
                                $('#rut').val(rut.value);
                                $('#rut').attr('disabled', true);
                                $('.modal-title').text('Contratista Responsable');
                                $('#name').attr('disabled', false);
                                $('#last_name').attr('disabled', false);
                                $('#address').attr('disabled', false);
                                $('#email').attr('disabled', false);
                                $('#phone').attr('disabled', false);
                                $('#people_profiles').attr('disabled', true);
                                $('#people_profiles').val('7');
                                $('#companies').attr('disabled', false);
                            }
                        }
                    });
                }
                else {
                    alert('Rut Inválido.');
                }
            }
        });
               
        $("#patent").keydown(function(event) {
            if (event.which == 13) {
                event.preventDefault();

                patent = $("#patent").val();

                $.ajax({
                    url: site_url + '/cProjects/SearchVehicle/',
                    type: 'post',
                    data: {
                        patent : $("#patent").val()
                    },
                    dataType: 'json',
                    success: function(data) {
                        if(data.mensaje == 1) {
                            if (data.vehicle[0].internal == 2 && data.vehicle[0].states_id == 1) {
                                if (!vehicles.includes(patent)) {
                                    vehicles.push(patent);
                                    var html = '';
                                    html += '<tr id="tr-'+patent+'" name="aditionalV">';
                                    html += '<td>'+patent+'</td>';
                                    html += '<td>'+data.vehicle[0].company+'</td>';
                                    html += '<td>'+data.vehicle[0].name+' '+data.vehicle[0].last_name+'</td>';
                                    html += '<td>'+data.vehicle[0].type+'</td>';
                                    html += '<td><input type="checkbox" id="init'+patent+'" name="autoinit"> Ingreso &nbsp;<input type="checkbox" id="end'+patent+'" name="autoend"> Salida</td>';
                                    html += '<td><button type="button" class="btn btn-danger btn-xs" onclick="removeVehicle('+patent+');">remover</button></td>';
                                    html += '</tr>';
                                    $("#tbody-vehicle").append(html);
                                    $("#modal-add-vehicle").modal('hide');
                                    $("#patent").val('');
                                }
                                else {
                                    alert('Ya existe en la lista de vehiculos de contratistas.');
                                    $("#patent").val('');
                                    $("#patent").focus();
                                }
                            }
                            else if(data.vehicle[0].internal == 1){
                                alert('Ya existe como vehículo interno.');
                                $("#patent").val('');
                                $("#patent").focus();
                            }
                            else if (data.vehicle[0].internal == 0) {
                                alert('Ya existe como vehículo visitante.');
                                $("#patent").val('');
                                $("#patent").focus();
                            }
                            else if (data.vehicle[0].states_id == 2) {
                                alert('Vehículo suspendido.');
                                $("#patent").val('');
                                $("#patent").focus();
                            }
                            else if (data.vehicle[0].states_id == 3) {
                                alert('Vehículo bloqueado.');
                                $("#patent").val('');
                                $("#patent").focus();
                            }
                        }
                        else {
                            $("#model").attr('disabled', false);
                            $("#vcompanies").attr('disabled', false);
                            $("#vehicle_people").attr('disabled', false);
                            $("#vechicle_type").attr('disabled', false);
                            $("#vprofile").attr('disabled', false);
                        }
                    }
                });
            }
        });

        $("#formModal-add-vehicle").submit(function(event) {
            event.preventDefault();

            var patent = $("#patent").val();
            var empresa = $("#vcompanies option:selected").text();
            var responsable = $("#vehicle_people option:selected").text();
            var tipo = $("#vechicle_type option:selected").text();

            $.ajax({
                url: site_url + '/cProjects/addVehicle/',
                type: 'post',
                data: {
                    patent                  : $("#patent").val(),
                    model                   : $("model").val(),
                    internal                : 2,
                    nfc_code                : 0,
                    companies_id            : $("#vcompanies").val(),
                    people_id               : $("#vehicle_people").val(),
                    vehicles_type_id        : $("#vechicle_type").val(),
                    vehicles_profiles_id    : $("#vprofile").val()
                },
                dataType: 'json',
                success: function(data) {
                    if (data == 1) {
                        vehicles.push($("#patent").val());
                        var html = '';
                        html += '<tr id="tr-'+patent+'" name="aditionalV">';
                        html += '<td>'+patent+'</td>';
                        html += '<td>'+empresa+'</td>';
                        html += '<td>'+responsable+'</td>';
                        html += '<td>'+tipo+'</td>';
                        html += '<td><input type="checkbox" id="init'+patent+'" name="autoinit"> Ingreso &nbsp;<input type="checkbox" id="end'+patent+'" name="autoend"> Salida</td>';
                        html += '<td><button type="button" class="btn btn-danger btn-xs" onclick="removeVehicle('+patent+');">remover</button></td>';
                        html += '</tr>';
                        $("#tbody-vehicle").append(html);
                        $("#modal-add-vehicle").modal('hide');
                        $("#patent").val('');
                    }
                    else if (data == 0) {
                        alert("Error en el proceso...")
                        window.location.replace(site_url+"/cProjects/");
                    }
                }
            });
        });

        $("#mashorario").click(function(event) {
            var html = '';
            html += '<tr id="'+index+'"><td><input type="time" id="time_init" data-index="'+index+'" name="'+index+'"></td>';
            html += '<td><input type="time" id="time_end" data-index="'+index+'" name="'+index+'"></td>';    
            html += '<td><input type="checkbox" data-index="'+index+'" data-name="semana" name="L"></td>';
            html += '<td><input type="checkbox" data-index="'+index+'" data-name="semana" name="M"></td>';
            html += '<td><input type="checkbox" data-index="'+index+'" data-name="semana" name="Mi"></td>';   
            html += '<td><input type="checkbox" data-index="'+index+'" data-name="semana" name="J"></td>';
            html += '<td><input type="checkbox" data-index="'+index+'" data-name="semana" name="V"></td>';
            html += '<td><input type="checkbox" data-index="'+index+'" data-name="semana" name="S"></td>';
            html += '<td><input type="checkbox" data-index="'+index+'" data-name="semana" name="D"></td>';
            html += '<td><button type="button" class="btn btn-xs btn-danger" onclick="removeMasHorario('+index+')">Remover</button></td></tr>';
            $('#tbody-horario').append(html);
            index++;
        });

        $("#formModal-add-requirement").submit(function(event) {
            event.preventDefault();
            $.ajax({
                url: site_url + '/cProjects/addRequirement/',
                type: 'post',
                data: {
                    requirement : $("#requirement").val(),
                    description : $("#description").val()
                },
                dataType: 'json',
                success: function(data) {
                    if (data.mensaje == 1) {
                        var html = '';
                        html += '<li class="list-group-item"><div class="wrapepr"><span><label>';
                        html += '<input type="checkbox" name="requir" id="'+data.id+'"> &nbsp;&nbsp;';
                        html += '<span> '+$("#requirement").val()+'</span>';
                        html += '</label></span></div></li>';
                        $("#ulrequirement").append(html);
                        $("#modal-add-requirement").modal('hide');
                        $("#requirement").val('');
                        $("#description").val('');
                    }
                    else if (data.mensaje == 0) {
                        alert("Error en el proceso...")
                        window.location.replace(site_url+"/cProjects/");
                    }
                }
            });
        });

        $("a[name='puertas']").click(function(event) {
            event.preventDefault();

            var id = $(this).attr('id')
            var level = $(this).data('level')
            
            if ($(this).find('.box-header').css("background-color") == "rgb(210, 214, 222)") {
                wpuertas.push($(this).attr('id'));
                $(this).find('.box-header').css("background-color", "#5cb85c");
                $(this).find('.box-header').css("background", "#5cb85c");
            }
            else {
                $(this).find('.box-header').css("background-color", "#d2d6de");
                $(this).find('.box-header').css("background", "#d2d6de");
                wpuertas.splice(wpuertas.indexOf(id), 1);
            }
        });

        var cont = 0; //usado en validaciones minimas
        var contador = 0;
        
        $("#formProjects").submit(function(event) {
            event.preventDefault();

            if (cont == 0) {//
                $("input:checkbox:checked[data-type='area']").each(function(index, el) {
                    wareas.push($(this).attr('id'));
                });
                $("input:checkbox:checked[data-type='dpto']").each(function(index, el) {
                    wdptos.push($(this).attr('id'));
                });
                $("input:checkbox:checked[data-type='zone']").each(function(index, el) {
                    wzones.push($(this).attr('id'));
                });
                $("input[data-index]").each(function(index, el) {
                    if ($(this).attr('type') == 'time') {
                        if ($(this).val() !=  '') {
                            horarios.push($(this).val());
                        }
                    }
                    else if ($(this).attr('type') == 'checkbox') {
                        if ($(this).prop('checked') == true) {
                            horarios.push('1')
                        }
                        else if ($(this).prop('checked') == false) {
                            horarios.push('0')
                        }
                    }
                });
                $("input:checkbox[name='minimum']").each(function(index, el) {
                    if ($(this).prop('checked') == true) {
                        requirem.push($(this).attr('id'))
                    } 
                });
                $("input:checkbox[name='autoinit']").each(function(index, el) {
                    if ($(this).prop('checked') == true) {
                        control_init = '1';
                    }
                    else if ($(this).prop('checked') == false) {
                        control_init = '0';
                    }
                });
                $("input:checkbox[name='autoend']").each(function(index, el) {
                    if ($(this).prop('checked') == true) {
                        control_end = '1';
                    }
                    else if ($(this).prop('checked') == false) {
                        control_end = '0';
                    }
                });
            }
            if (cont != 0) {
                if (wareas.length == 0) {
                    $("input:checkbox:checked[data-type='area']").each(function(index, el) {
                        wareas.push($(this).attr('id'));
                    });
                }
                if (wdptos.length == 0) {
                    $("input:checkbox:checked[data-type='dpto']").each(function(index, el) {
                        wdptos.push($(this).attr('id'));
                    });
                }
                if (wzones.length == 0) {
                    $("input:checkbox:checked[data-type='zone']").each(function(index, el) {
                        wzones.push($(this).attr('id'));
                    });
                }
                if (horarios.length == 0) {
                    $("input[data-index]").each(function(index, el) {
                        if ($(this).attr('type') == 'time') {
                            if ($(this).val() !=  '') {
                                horarios.push($(this).val());
                            }
                        }
                        else if ($(this).attr('type') == 'checkbox') {
                            if ($(this).prop('checked') == true) {
                                horarios.push('1')
                            }
                            else if ($(this).prop('checked') == false) {
                                horarios.push('0')
                            }
                        }
                    });
                }
            }

            /*
            $("input:checkbox[name='minimum']").each(function(index, el) {
                if ($(this).prop('checked') == true) {
                    requirem.push($(this).attr('id'))
                } 
            });
            */
            
            var archivoup = 0;
            if (up == 1) {
                var file_upload = document.getElementsByClassName('files-uploads');
                var elementos = new FormData();
                for(var j = 0; j < file_upload.length; j++){
                    var archivo = file_upload[j].files;
                    for(var i = 0; i < archivo.length; i++){
                        elementos.append('archivo'+archivoup, archivo[i]);
                        archivoup++;
                    }
                }
            }
            
            //validaciones
            if($("#input-add-rut1").val() == '') {
                $("#mensaje-error").text('Falta ingresar un encargado del proyecto');
                cont++;
            }
            else if($("#title").val() == '') {
                $("#mensaje-error").text('Falta ingresar el título del proyecto');
                cont++;
            }
            else if($("#pinternal").val() == '') {
                $("#mensaje-error").text('Falta definir un supervisor de proyecto');
                cont++;
            }
            else if($("#time_init").val() == '') {
                $("#mensaje-error").text('No se ingreso la hora de inicio de faenas');
                cont++;
            }
            else if($("#time_end").val() == '') {
                $("#mensaje-error").text('No se ingreso la hora de fin de faenas');
                cont++;
            }
            else if($("#init").val() == '') {
                $("#mensaje-error").text('No se ingreso la fecha de inicio del proyecto');
                cont++;
            }
            else if($("#end").val() == '') {
                $("#mensaje-error").text('No se ingreso la fecha de fin del proyecto');
                cont++;
            }
            else if(horarios.length <= 2) {
                $("#mensaje-error").text('Falta definir los días de la semana del proyecto');
                cont++;
            }
            else if(wzones.length == 0) {
                $("#mensaje-error").text('Falta definir las zonas del proyecto');
                cont++;
            }
            else if(wpuertas.length == 0) {
                $("#mensaje-error").text('Falta definir la ruta del proyecto');
                cont++;
            }
            else if (wdptos.length == 0) {
                $("#mensaje-error").text('Falta definir los departamentos del proyecto');
                cont++;
            }
            else if (wareas.length == 0) {
                $("#mensaje-error").text('Falta definir las areas del proyecto');
                cont++;
            }
            else if (wzones.length > 0 && wpuertas.length > 0 && wdptos.length > 0 && wareas.length > 0 && $("#input-add-rut1").val() != '' && $("#title").val() != '' && $("#pinternal").val() != '' && $("#init").val() != '' && $("#end").val() != '') {
                $.ajax({
                    url: site_url + '/cProjects/addProjects/',
                    type: 'post',
                    data: {
                        contratistas    : contratistas,
                        vehicles        : vehicles,
                        title           : $("#title").val(),
                        description     : $("#description").val(),
                        init            : $("#init").val(),
                        end             : $("#end").val(),
                        horarios        : horarios,
                        encargado       : $("#pinternal option:selected").val(),
                        puerta          : wpuertas,
                        areas           : wareas,
                        dptos           : wdptos,
                        zonas           : wzones,
                        requirements    : requirem,
                        control_init    : control_init,
                        control_end     : control_end
                    },
                    dataType: 'json',
                    success: function(data) {
                        if(data.mensaje == 1) {
                            $.ajax({
                                url: site_url + '/cProjects/addDocuments?id='+data.project,//upload
                                type: 'post',
                                contentType: false,
                                data: elementos,
                                processData: false,
                                cache: false,
                                success: function(dato) {
                                    window.location.replace(site_url+"/cProjects/");
                                }
                            });
                        }
                        else if (data.mensaje == 0) {
                            alert("Error en el proceso...");
                            window.location.replace(site_url+"/cProjects/");
                        }
                    }
                });
            }
        });
    });

    function nextTab(elem) {
        $(elem).next().find('a[data-toggle="tab"]').click();
    }

    function prevTab(elem) {
        $(elem).prev().find('a[data-toggle="tab"]').click();
    }

    var whorarios = [];
    var wwzones = [];
    var wwareas = [];
    var wwdptos = [];
    
    function validateformstep(stepid){
        $("input[data-index]").each(function(index, el) {
            if ($(this).attr('type') == 'checkbox') {
                if ($(this).prop('checked') == true) {
                    whorarios.push('1')
                }
            }
        });
        $("input:checkbox:checked[data-type='zone']").each(function(index, el) {
            wwzones.push($(this).attr('id'));
        });
        $("input:checkbox:checked[data-type='area']").each(function(index, el) {
            wwareas.push($(this).attr('id'));
        });
        $("input:checkbox:checked[data-type='dpto']").each(function(index, el) {
            wwdptos.push($(this).attr('id'));
        });

        switch(stepid) {
            case 'step1':
                if ($("#input-add-rut1").val() == '') {
                    alert('Falta ingresar un encargado del proyecto');
                    return false;
                }
                else if ($("#title").val() == '') {
                    alert('Falta ingresar el título del proyecto');
                    return false;
                }
                else if ($("#init").val() == '') {
                    alert('No se ingreso la fecha de inicio del proyecto');
                    return false;
                }
                else if ($("#end").val() == '') {
                    alert('No se ingreso la fecha de fin del proyecto');
                    return false;
                }
                else {
                    return true;
                }
                break;

            case 'step2':
                if ($("#time_init").val() == '') {
                    alert('No se ingreso la hora de inicio de faenas');
                    return false;
                }
                else if ($("#time_end").val() == '') {
                    alert('No se ingreso la hora de fin de faenas');
                    return false;
                }
                else if (whorarios.length == 0) {
                    alert('Falta definir los días de la semana del proyecto');
                    return false;
                }
                else {
                    return true;
                }
                break;

            case 'step3':
                if ($("#pinternal").val() == '') {
                    alert('Falta definir un supervisor de proyecto');
                    return false;
                }
                else if (wwzones.length == 0) {
                    alert('Falta definir las zonas del proyecto');
                    return false;
                }
                else if (wwareas.length == 0) {
                    alert('Falta definir las areas del proyecto');
                    return false;
                }
                else if (wwdptos.length == 0) {
                    alert('Falta definir los departamentos del proyecto');
                    return false;
                }
                else if (wpuertas.length == 0) {
                    alert('Falta definir la ruta del proyecto');
                    return false;
                }
                else {
                    return true;
                }
                break;
        }
    }

    function btnCancelAdd() {
        window.location.replace(site_url+"/cProjects/");
    }

    function checkRut(rut) {
        // Despejar Puntos
        var valor = rut.value.replace('.','');
        // Despejar Guión
        valor = valor.replace('-','');
        // Aislar Cuerpo y Dígito Verificador
        var cuerpo = valor.slice(0,-1);
        var dv = valor.slice(-1).toUpperCase();
        // Formatear RUN
        rut.value = cuerpo + '-'+ dv
        // Si no cumple con el mínimo ej. (n.nnn.nnn)
        if(cuerpo.length < 7) 
        {
            rut.setCustomValidity("RUT Incompleto");
            rut.setCustomValidity('');
            cuerpo = '';
            dv = '';
            rut.value = '';
            return false;
        }
        else
        {
            // Calcular Dígito Verificador
            suma = 0;
            multiplo = 2;
            
            // Para cada dígito del Cuerpo
            for(i=1;i<=cuerpo.length;i++) {
            
                // Obtener su Producto con el Múltiplo Correspondiente
                index = multiplo * valor.charAt(cuerpo.length - i);
                
                // Sumar al Contador General
                suma = suma + index;
                
                // Consolidar Múltiplo dentro del rango [2,7]
                if(multiplo < 7) { multiplo = multiplo + 1; } else { multiplo = 2; }
          
            }
            
            // Calcular Dígito Verificador en base al Módulo 11
            dvEsperado = 11 - (suma % 11);
            
            // Casos Especiales (0 y K)
            dv = (dv == 'K')?10:dv;
            dv = (dv == 0)?11:dv;
            
            // Validar que el Cuerpo coincide con su Dígito Verificador
            if(dvEsperado != dv) 
            { 
                rut.setCustomValidity("RUT Inválido");
                rut.setCustomValidity('');
                cuerpo = '';
                dv = '';
                rut.value = '';
                return false;
            }
            // Si todo sale bien, eliminar errores (decretar que es válido)
            rut.setCustomValidity('');
            return true;
        }
    }

    function openModalAddAditionalPeople(id) {
        accion_form = id
        if (id == 1) {
            $('#rut').val('');
            $('#name').val('');
            $('#last_name').val('');
            $('#address').val('');
            $('#email').val('');
            $('#phone').val('');
            $('#people_profiles').val('');
            $('#companies').val('');

            $('#rut').attr('disabled', false);
            $('#name').attr('disabled', true);
            $('#last_name').attr('disabled', true);
            $('#address').attr('disabled', true);
            $('#email').attr('disabled', true);
            $('#phone').attr('disabled', true);
            $('#people_profiles').attr('disabled', true);
            $('#companies').attr('disabled', true);

            $('#modal-add-people').modal('show');
            setTimeout(function(){ $('#rut').focus(); }, 500);
        }
        else if (id == 0) {
            $('#modal-add-people').modal('show');
            setTimeout(function(){ $('#rut').focus(); }, 500);
        }       
    }

    function removeAditionalPeople(id) {
        var i = myIndexOf(contratistas, id);

        if(i != -1){
            contratistas.splice(i, 1);
            $('#tr-'+id).remove();
        }
    }

    function myIndexOf(array, item) {
        var indice = -1;
        var i = 0;
        while(i < array.length) {
            if(array[i] == item)
                indice = i;
            i++;
        }

        return indice;
    }

    function openModalAddAditionalVehicle() {
        for (var i = 0; i < people.length; i++) {
            var html = ``;
            html += '<option value="'+people[i].rut+'">'+people[i].nombre+'</option>';
            $('#vehicle_people').append(html);
        }
        $('#modal-add-vehicle').modal('show');
        setTimeout(function(){ $('#patent').focus(); }, 500);
    }

    function removeVehicle(id) {
        var i = myIndexOf(vehicles, id);

        if(i != -1){
            vehicles.splice(i, 1);
            $('#tr-'+id).remove();
        }
    }

    function openModalAddMRequirements() {
        $('#modal-add-requirement').modal('show');
        setTimeout(function(){ $('#requirement').focus(); }, 500);
    }

    var contadorUp = 0;
    function addFormUpload() {
        var html = '<form enctype="multipart/form-data" id="form-upload'+contadorUp+'" method="post">';
        html += '<input class="files-uploads form-control" type="file" multiple="multiple" id="input-archivos'+contadorUp+'" name="archivo1"/><br><button class="btn btn-xs btn-danger" id="rem'+contadorUp+'" onclick="removeFormUpload('+contadorUp+')"><i class="fa fa-minus"></i> Remover</button><br><hr>';
       
        $('#buploads').append(html);
        // $("#fuploads").append('')
        up = 1;
        contadorUp++;
    }

    function removeFormUpload(id) {
        $("#form-upload"+id).remove();
        $("#rem"+id).remove();
        up = 0;
    }

    function removeMasHorario(id) {
        $("tr[id='"+id+"']").remove();
    }

    function searchbrother(level, name, id) {
        var mensaje = false
        $("#ubicacion input:checkbox:checked").each(function(index, el) {
            if ($(this).data('level') == level && $(this).attr('name') == name && $(this).attr('id') != id) {
                mensaje = true
            }
        });
        return mensaje
    }

    $("#ubicacion input:checkbox").change(function(event) {
        if ($(this).is(':checked')) {
            var name = $(this).attr('name');
            var id = $(this).attr('id');
            var level = $(this).data('level');

            var padres = [];
            if ($(this).data('level') == 1) { //si es area
                ruta($(this).attr('id'), 'add', 'area')
                $("#ubicacion input:checkbox").each(function(index, el) {
                    if ($(this).data('level') == 0 && $(this).attr('id') == name) { //marco zona padre
                        $(this).prop('checked', true)
                        ruta($(this).attr('id'), 'add', 'zona')
                    }
                    if ($(this).data('level') == 2 && $(this).attr('name') == id) { //marca dpto hijo
                        $(this).prop('checked', true)
                        ruta($(this).attr('id'), 'add', 'dpto')
                    }
                });
            }
            if ($(this).data('level') == 2) { //si es dpto
                ruta($(this).attr('id'), 'add', 'dpto')
                $("#ubicacion input:checkbox").each(function(index, el) {
                    if ($(this).data('level') == 1 && $(this).attr('id') == name) { //marco area padre
                        $(this).prop('checked', true)
                        padres.push($(this).attr('name'));
                        ruta($(this).attr('id'), 'add', 'area')
                    }
                });
                for (var j = 0; j < padres.length; j++) {
                    $("#ubicacion input:checkbox").each(function(index, el) {
                        if ($(this).data('level') == 0 && $(this).attr('id') == padres[j]) { //marco zona abuelo
                            $(this).prop('checked', true)
                            ruta($(this).attr('id'), 'add', 'zona')
                        }
                    });
                }
                padres = [];
            }
            if ($(this).data('level') == 0) { // si es zona
                ruta($(this).attr('id'), 'add', 'zona')
                $("#ubicacion input:checkbox").each(function(index, el) {
                    if ($(this).data('level') == 1 && $(this).attr('name') == id) { //marca areas hijos
                        $(this).prop('checked', true)
                        padres.push($(this).prop('id'));
                        ruta($(this).attr('id'), 'add', 'area')
                    }
                });
                for (var j = 0; j < padres.length; j++) {
                    $("#ubicacion input:checkbox").each(function(index, el) {
                        if ($(this).data('level') == 2 && $(this).attr('name') == padres[j]) { //marca dptos nietos
                            $(this).prop('checked', true)
                            ruta($(this).attr('id'), 'add', 'dpto')
                        }
                    });
                }
                padres = [];
            }
        }
        var check = $(this).is(':checked');
        if (!check) {
            var name = $(this).attr('name');
            var id = $(this).attr('id');
            var level = $(this).data('level');
            var padres = [];
            if ($(this).data('level') == 1) { //si es area
                $("#ubicacion input:checkbox").each(function(index, el) {
                    if ($(this).data('level') == 0 && $(this).attr('id') == name) { //marco zona padre
                        if (!searchbrother(level, name, id)) {
                            $(this).prop('checked', false)
                            ruta($(this).attr('id'), 'del', 'zona')
                        }
                    }
                    if ($(this).data('level') == 2 && $(this).attr('name') == id) { //marca dpto hijo
                        $(this).prop('checked', false)
                        ruta($(this).attr('id'), 'del', 'dpto')
                    }
                });
            }
            if ($(this).data('level') == 2) { //si es dpto
                $("#ubicacion input:checkbox").each(function(index, el) {
                    if ($(this).data('level') == 1 && $(this).attr('id') == name) { //marco area padre
                        if (!searchbrother(level, name, id)) {
                            $(this).prop('checked', false)//borro area
                            if (!searchbrother($(this).data('level'), $(this).attr('name'), $(this).attr('id'))) {
                                padres.push($(this).attr('name'));
                                ruta($(this).attr('id'), 'del', 'area')
                            }
                            
                        }
                    }
                });
                for (var j = 0; j < padres.length; j++) {
                    $("#ubicacion input:checkbox").each(function(index, el) {
                        if ($(this).data('level') == 0 && $(this).attr('id') == padres[j]) { //marco zona abuelo
                            $(this).prop('checked', false)
                            ruta($(this).attr('id'), 'del', 'zona')
                        }
                    });
                }
                padres = [];
            }
            if ($(this).data('level') == 0) { // si es zona
                $("#ubicacion input:checkbox").each(function(index, el) {
                    if ($(this).data('level') == 1 && $(this).attr('name') == id) { //marca areas hijos
                        $(this).prop('checked', false)
                        padres.push($(this).attr('id'));
                        ruta($(this).attr('id'), 'del', 'area')
                    }
                });
                for (var j = 0; j < padres.length; j++) {
                    $("#ubicacion input:checkbox").each(function(index, el) {
                        if ($(this).data('level') == 2 && $(this).attr('name') == padres[j]) { //marca dptos nietos
                            $(this).prop('checked', false)
                            ruta($(this).attr('id'), 'del', 'dpto')
                        }
                    });
                }
                padres = [];
            }
            check = null;
        }
    });

    var dzones = <?php echo json_encode($treeview['dzones']); ?>;
    var dareas = <?php echo json_encode($treeview['dareas']); ?>;
    var ddptos = <?php echo json_encode($treeview['ddepto']); ?>;
    var rdoor = [];

    function ruta(id, accion, ubicacion){
        if (ubicacion === 'zona') {
            if (accion === 'add') {
                for (var i = 0; i < dzones.length; i++) {
                    if (dzones[i].zones_id == id) {
                        rdoor.push(dzones[i].door_z)
                    }
                }
                $("a[name='puertas']").each(function(index, el) {
                    if(rdoor.includes($(this).attr('id'))){
                        //validar si se agrega o no la puerta
                        var i = myIndexOf(wpuertas, $(this).attr('id') );
                        if(i == -1)
                            wpuertas.push($(this).attr('id'));
                        //-------------------------------------------
                        $(this).find('.box-header').css("background-color", "#5cb85c");
                        $(this).find('.box-header').css("background", "#5cb85c");
                    } 
                });
            }
            if (accion === 'del') {
                $("a[name='puertas']").each(function(index, el) {
                    if(rdoor.includes($(this).attr('id'))){
                        $(this).find('.box-header').css("background-color", "#d2d6de");
                        $(this).find('.box-header').css("background", "#d2d6de");
                        //validar si se elimina o no la puerta
                        var i = myIndexOf(wpuertas, $(this).attr('id') );
                        if(i != -1)
                        {
                            wpuertas.splice(i, 1);
                            rdoor.splice(rdoor.indexOf($(this).attr('id')), 1);
                        }
                        //-------------------------------------------
                    } 
                });
            }
        }
        if (ubicacion === 'area') {
            if (accion === 'add') {
                for (var i = 0; i < dareas.length; i++) {
                    if (dareas[i].areas_id == id) {
                        rdoor.push(dareas[i].door_a)
                    }
                }
                $("a[name='puertas']").each(function(index, el) {
                    if(rdoor.includes($(this).attr('id'))){
                        //validar si se agrega o no la puerta
                        var i = myIndexOf(wpuertas, $(this).attr('id') );
                        if(i == -1)
                            wpuertas.push($(this).attr('id'));
                        //-------------------------------------------
                        $(this).find('.box-header').css("background-color", "#5cb85c");
                        $(this).find('.box-header').css("background", "#5cb85c");
                    } 
                });
            }
            if (accion === 'del') {
                $("a[name='puertas']").each(function(index, el) {
                    if(rdoor.includes($(this).attr('id'))){
                        $(this).find('.box-header').css("background-color", "#d2d6de");
                        $(this).find('.box-header').css("background", "#d2d6de");
                        //validar si se elimina o no la puerta
                        var i = myIndexOf(wpuertas, $(this).attr('id') );
                        if(i != -1)
                        {
                            wpuertas.splice(i, 1);
                            rdoor.splice(rdoor.indexOf($(this).attr('id')), 1);
                        }
                        //-------------------------------------------
                    } 
                });
            }
        }
        if (ubicacion === 'dpto') {
            if (accion === 'add') {
                for (var i = 0; i < ddptos.length; i++) {
                    if (ddptos[i].departments_id == id) {
                        rdoor.push(ddptos[i].door_d)
                    }
                }
                $("a[name='puertas']").each(function(index, el) {
                    if(rdoor.includes($(this).attr('id'))){
                        //validar si se agrega o no la puerta
                        var i = myIndexOf(wpuertas, $(this).attr('id') );
                        if(i == -1)
                            wpuertas.push($(this).attr('id'));
                        //-------------------------------------------
                        $(this).find('.box-header').css("background-color", "#5cb85c");
                        $(this).find('.box-header').css("background", "#5cb85c");
                    } 
                });
            }
            if (accion === 'del') {
                $("a[name='puertas']").each(function(index, el) {
                    if(rdoor.includes($(this).attr('id'))){
                        $(this).find('.box-header').css("background-color", "#d2d6de");
                        $(this).find('.box-header').css("background", "#d2d6de");
                        //validar si se elimina o no la puerta
                        var i = myIndexOf(wpuertas, $(this).attr('id') );
                        if(i != -1)
                        {
                            wpuertas.splice(i, 1);
                            rdoor.splice(rdoor.indexOf($(this).attr('id')), 1);
                        }
                        //-------------------------------------------
                    } 
                });
            }
        }
    }

</script>

</body>
</html>


