<style type="text/css">
  @import url('https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');

  main {
    min-width: 320px;
    max-width: 1100px;
    padding: 20px;
    margin: 0 auto;
    background: #fff;
  }

  .tab-pane {
    display: none;
    padding: 20px 0 0;
    border-top: 1px solid #ddd;
  }

  input {
    display: none;
  }

  label {
    display: inline-block;
    margin: 0 0 -1px;
    padding: 15px 25px;
    font-weight: 600;
    text-align: center;
    //color: #bbb;
    border: 1px solid transparent;
  }

  label:before {
    font-family: fontawesome;
    font-weight: normal;
    margin-right: 10px;
  }

  label:hover {
    color: #888;
    cursor: pointer;
  }

  input:checked + label {
    color: #555;
    border: 1px solid #ddd;
    border-top: 2px solid orange;
    border-bottom: 1px solid #fff;
  }

  #tab1:checked ~ #content1,
  #tab2:checked ~ #content2,
  #tab3:checked ~ #content3,
  #tab4:checked ~ #content4,
  #tab5:checked ~ #content5,
  #tab6:checked ~ #content6,
  #tab7:checked ~ #content7 {
    display: block;
  }
</style>

<div class="content-wrapper">
    <section class="content">
        <div class="row">
            <div class="col-sm-12">
                <div class="box box-success">
                    <div class="box-header ui-sortable-handle">
                        <i class="fa fa-sign-in"></i>
                        <h3 class="box-title">Detalle</h3>
                    </div>
                    
                    <div class="box-body">
                        <main>
                    
                            <input id="tab1" type="radio" name="tabs" checked>
                            <label for="tab1"><i class="fa fa-wrench" aria-hidden="true"></i>&nbsp;Proyecto</label>

                            <input id="tab2" type="radio" name="tabs">
                            <label for="tab2"><i class="fa fa-users" aria-hidden="true"></i>&nbsp;Personal</label>

                            <input id="tab3" type="radio" name="tabs">
                            <label for="tab3"><i class="fa fa-car" aria-hidden="true"></i>&nbsp;Vehículos</label>
                              
                            <input id="tab4" type="radio" name="tabs">
                            <label for="tab4"><i class="fa fa-map-marker" aria-hidden="true"></i>&nbsp;Ubicaciones</label>

                            <input id="tab5" type="radio" name="tabs">
                            <label for="tab5"><i class="fa fa-random" aria-hidden="true"></i>&nbsp;Movimientos</label>

                            <input id="tab6" type="radio" name="tabs">
                            <label for="tab6"><i class="fa fa-file-text" aria-hidden="true"></i>&nbsp;Requisitos</label>

                            <input id="tab7" type="radio" name="tabs">
                            <label for="tab7"><i class="fa fa-file-text" aria-hidden="true"></i>&nbsp;Controles</label>

                            <div id="content1" class="tab-pane">
                                <fieldset>
                                    <legend>Proyecto</legend>
                                    <table style="width: 100%;">
                                        <tbody>
                                        	<?php if (!empty($projects)) { ?>
			  		    		    		<tr>
				  		    		    		<td><label>Id</label></td>
				  			    				<td><?php echo $projects[0]['id'];?></td>
				  		    		    	</tr>
				  		    		    	<tr>
				  		    		    		<td><label>Título</label></td>
				  			    				<td><?php echo $projects[0]['title'];?></td>
				  		    		    	</tr>
				  		    		    	<tr>
				  		    		    		<td><label>Descripción</label></td>
				  			    				<td><?php echo $projects[0]['description'];?></td>
				  		    		    	</tr>
				  		    		    	<tr>
				  		    	    			<td><label>Inicio | Término</label></td>
				  		    	    			<td><?php echo $projects[0]['init'].' | '.$projects[0]['end'];?></td>
				  		    	    		</tr>
				  		    	    		<?php } ?>
											<?php if (!empty($horarios)) { ?>
				  		    	    		<tr>
				  		    	    			<td><label>Horario Inicio | Fin</label></td>
				  		    	    			<td><?php for ($i = 0; $i < count($horarios); $i++) {
                                                        echo $horarios[$i]['time_init'].' | '.$horarios[$i]['time_end'].' &nbsp-&nbsp ';
                                                    echo ($horarios[$i]['L'] == 1) ? '<i class="fa fa-check-square-o" aria-hidden="true"></i> Lunes &nbsp;' : '<i class="fa fa-square-o" aria-hidden="true"></i> Lunes &nbsp;';
                                                    echo ($horarios[$i]['M'] == 1) ? ' <i class="fa fa-check-square-o" aria-hidden="true"></i> Martes &nbsp;' : '<i class="fa fa-square-o" aria-hidden="true"></i> Martes &nbsp;';
                                                    echo ($horarios[$i]['MI'] == 1) ? ' <i class="fa fa-check-square-o" aria-hidden="true"></i> Miércoles &nbsp;' : '<i class="fa fa-square-o" aria-hidden="true"></i> Miércoles &nbsp;';
                                                    echo ($horarios[$i]['J'] == 1) ? ' <i class="fa fa-check-square-o" aria-hidden="true"></i> Jueves &nbsp;' : '<i class="fa fa-square-o" aria-hidden="true"></i> Jueves &nbsp;';
                                                    echo ($horarios[$i]['V'] == 1) ? ' <i class="fa fa-check-square-o" aria-hidden="true"></i> Viernes &nbsp;' : '<i class="fa fa-square-o" aria-hidden="true"></i> Viernes &nbsp;';
                                                    echo ($horarios[$i]['S'] == 1) ? ' <i class="fa fa-check-square-o" aria-hidden="true"></i> Sábado &nbsp;' : '<i class="fa fa-square-o" aria-hidden="true"></i> Sábado &nbsp;';
                                                    echo ($horarios[$i]['D'] == 1) ? ' <i class="fa fa-check-square-o" aria-hidden="true"></i> Domingo &nbsp;' : '<i class="fa fa-square-o" aria-hidden="true"></i> Domingo &nbsp;</br>';
                                                    }  ?>
                                                </td>
				  		    	    		</tr>
				  		    	    		<?php } ?>
                                            
                                        </tbody>
                                    </table>
                                </fieldset>
                            </div>
                            
                            <div id="content2" class="tab-pane">
                                <fieldset>
                                    <legend>Contratista Encargado</legend>
                                    <table style="width: 100%;">
                                        <tbody>
                                        	<tr>
                                                <td><label>Rut</label></td>
                                                <td id="td-rut">
                                                    <?php
                                                    
                                                        $rut = '';

                                                        if(!empty($projects[0]['rut']))
                                                        {
                                                            $rut .= $projects[0]['rut'];

                                                            if(!empty($projects[0]['digit']))
                                                            {
                                                                $rut .= '-'.$projects[0]['digit'];
                                                            }

                                                            echo $rut;
                                                        }
                                                    ?>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td><label>Nombre</label></td>
                                                <td id="td-name">
                                                    <?php
                                                    
                                                        $name = '';

                                                        if(!empty($projects[0]['name']))
                                                        {
                                                            $name .= $projects[0]['name'];

                                                            if(!empty($projects[0]['last_name']))
                                                            {
                                                                $name .= ' '.$projects[0]['last_name'];
                                                            }

                                                            echo $name;
                                                        }
                                                    ?>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td><label>Teléfono</label></td>
                                                <td id="td-phone">
                                                    <?php
                                                        if(!empty($projects[0]['phone']))
                                                        {
                                                            echo $projects[0]['phone'];
                                                        }
                                                    ?>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td><label>Email</label></td>
                                                <td id="td-email">
                                                    <?php
                                                        if(!empty($projects[0]['email']))
                                                        {
                                                            echo $projects[0]['email'];
                                                        }
                                                    ?>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td><label>Perfil</label></td>
                                                <td id="td-profile">
                                                    <?php
                                                        if(!empty($projects[0]['profile']))
                                                        {
                                                            echo $projects[0]['profile'];
                                                        }
                                                    ?>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td><label>Empresa</label></td>
                                                <td id="td-company">
                                                    <?php
                                                        if(!empty($projects[0]['company']))
                                                        {
                                                            echo $projects[0]['company'];
                                                        }
                                                    ?>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </fieldset>
                                <br>
                                <fieldset>
                                    <legend>Contraparte Empresa</legend>
                                    <table style="width: 100%;">
                                        <tbody>
                                        	<tr>
                                                <td><label>Rut</label></td>
                                                <td id="td-rut">
                                                    <?php
                                                    
                                                        $rut = '';

                                                        if(!empty($encargado[0]['rut']))
                                                        {
                                                            $rut .= $encargado[0]['rut'];

                                                            if(!empty($encargado[0]['digit']))
                                                            {
                                                                $rut .= '-'.$encargado[0]['digit'];
                                                            }

                                                            echo $rut;
                                                        }
                                                    ?>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td><label>Nombre</label></td>
                                                <td id="td-name">
                                                    <?php
                                                    
                                                        $name = '';

                                                        if(!empty($encargado[0]['name']))
                                                        {
                                                            $name .= $encargado[0]['name'];

                                                            if(!empty($encargado[0]['last_name']))
                                                            {
                                                                $name .= ' '.$encargado[0]['last_name'];
                                                            }

                                                            echo $name;
                                                        }
                                                    ?>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td><label>Teléfono</label></td>
                                                <td id="td-phone">
                                                    <?php
                                                        if(!empty($encargado[0]['phone']))
                                                        {
                                                            echo $encargado[0]['phone'];
                                                        }
                                                    ?>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td><label>Email</label></td>
                                                <td id="td-email">
                                                    <?php
                                                        if(!empty($encargado[0]['email']))
                                                        {
                                                            echo $encargado[0]['email'];
                                                        }
                                                    ?>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td><label>Perfil</label></td>
                                                <td id="td-profile">
                                                    <?php
                                                        if(!empty($encargado[0]['profile']))
                                                        {
                                                            echo $encargado[0]['profile'];
                                                        }
                                                    ?>
                                                </td>
                                            </tr>

                                            <tr>
                                                <td><label>Empresa</label></td>
                                                <td id="td-company">
                                                    <?php
                                                        if(!empty($encargado[0]['company']))
                                                        {
                                                            echo $encargado[0]['company'];
                                                        }
                                                    ?>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </fieldset>
                                <br>
                                <fieldset>
                                    <legend>Contratistas</legend>
                                    <table style="width: 100%;">
                                    	<tbody>
                                        	<tr>
                                                <th>Rut</th>
                                                <th>Nombre</th>
                                                <th>Teléfono</th>
                                                <th>Email</th>
                                                <th>Perfil</th>
                                                <th>Empresa</th>
											</tr>	
											<?php if (!empty($pexternal)) { ?>
                                            <?php
                                            foreach($pexternal as $pe) { ?>
											<tr>
					  							<td>
					  								<?php echo $pe['rut'].'-'.$pe['digit']; ?>
					  							</td>
					  							<td>
					  								<?php echo $pe['name'].' '.$pe['last_name']; ?>
					  							</td>
					  							<td>
					  								<?php echo $pe['phone']; ?>
					  							</td>
					  							<td>
					  								<?php echo $pe['email']; ?>
					  							</td>
					  							<td>
					  								<?php echo $pe['profile']; ?>
					  							</td>
					  							<td>
					  								<?php echo $pe['company']; ?>
					  							</td>
					  						<?php } ?>
                                            </tr>
                                            <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </fieldset>
                            </div>

                            <div id="content3" class="tab-pane">
                                <fieldset>
                                    <legend>Vehículos</legend>
                                    <table style="width: 100%;" id="table-zones">
                                        <tbody>
                                        	<tr>
                                        		<th>Patente</th>
                                        		<th>Responsable</th>
                                        		<th>Tipo</th>
                                        		<th>Empresa</th>
                                        		<th>Control</th>
                                        	</tr>
                                        	<tr>
                                        	<?php if (!empty($vehicles)) {
                                        		foreach ($vehicles as $k) { ?>
                                        			<td><?php echo $k->patent; ?></td>
                                        			<td><?php echo $k->rut.'-'.$k->digit.' | '.$k->name.' '.$k->last_name; ?></td>
                                        			<td><?php echo $k->type; ?></td>
                                        			<td><?php echo $k->company; ?></td>
                                        			<td><?php echo ($k->control_init == 1) ? '<i class="fa fa-check-square-o" aria-hidden="true"></i> Control entrada &nbsp;' : '<i class="fa fa-square-o" aria-hidden="true"></i> Control entrada &nbsp;'; ?><?php echo ($k->control_end == 1) ? '<i class="fa fa-check-square-o" aria-hidden="true"></i> Control salida &nbsp;' : '<i class="fa fa-square-o" aria-hidden="true"></i> Control salida &nbsp;' ?></td>
                                        	<?php }
                                        	} ?>
                                        	</tr>
                                        </tbody>
                                    </table>
                                </fieldset>
                            </div>
                              
                            <div id="content4" class="tab-pane">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-4">
                                            <fieldset>
                                                <legend>Zonas</legend>
                                                <table style="width: 100%;" id="table-zones">
                                                    <tbody>
                                                        <tr>
                                                            <td>
                                                                <?php
                                                                    if(!empty($zonas))
                                                                    {
                                                                        foreach($zonas as $apz)
                                                                        {
                                                                            echo  $apz['zone'].'<br>';
                                                                        }
                                                                    }
                                                                ?>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </fieldset>
                                        </div>

                                        <div class="col-md-4">
                                            <fieldset>
                                                <legend>Áreas</legend>
                                                <table style="width: 100%;" id="table-areas">
                                                    <tbody>
                                                        <tr>
                                                            <td>
                                                                <?php
                                                                    if(!empty($areas))
                                                                    {
                                                                        foreach($areas as $apa)
                                                                        {
                                                                            echo  $apa['area'].'<br>';
                                                                        }
                                                                    }
                                                                ?>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </fieldset>
                                        </div>

                                        <div class="col-md-4">
                                            <fieldset>
                                                <legend>Deptos.</legend>
                                                <table style="width: 100%;" id="table-department">
                                                    <tbody>
                                                        <tr>
                                                            <td>
                                                                <?php
                                                                    if(!empty($dptos))
                                                                    {
                                                                        foreach($dptos as $apd)
                                                                        {
                                                                            echo  $apd['department'].'<br>';
                                                                        }
                                                                    }
                                                                ?>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </fieldset>
                                        </div>
                                    </div>
                                </div>
                                <br>                          
                                <fieldset>
                                    <legend>Ruta de Puertas</legend>
                                    <br>

                                    <?php

                                    if(!empty($route))
                                    {

                                        for($i=0;$i<count($route);$i++)
                                        {
                                            if($i % 3 == 0)
                                            {
                                                echo '<div class="row"><div class="col-md-12">';
                                                echo '<div class="col-md-2">';
                                                echo '<div class="card" style="width: 18rem;">';
                                                echo '<img class="card-img-top" src="'.base_url().'/assets/img/door.png" alt="Card image cap">';
                                                echo '<div class="card-body">';
                                                echo '<h5 class="card-title">'.$route[$i]['door'].'</h5>';
                                                echo '<p class="card-text">'.$route[$i]['description'].'</p>';
                                                echo '</div>';
                                                echo '</div>';
                                                echo '</div>';

                                                if($i < count($route) -1)
                                                {
                                                    echo '<div class="col-md-2"><i class="fa fa-arrow-right fa-2x" aria-hidden="true"></i></div>';
                                                }

                                            }
                                            else if(($i + 1) % 3 == 0 )
                                            {
                                                echo '<div class="col-md-2">';
                                                echo '<div class="card" style="width: 18rem;">';
                                                echo '<img class="card-img-top" src="'.base_url().'/assets/img/door.png" alt="Card image cap">';
                                                echo '<div class="card-body">';
                                                echo '<h5 class="card-title">'.$route[$i]['door'].'</h5>';
                                                echo '<p class="card-text">'.$route[$i]['description'].'</p>';
                                                echo '</div>';
                                                echo '</div>';
                                                echo  '</div>';

                                                if($i < count($route) -1 )
                                                {
                                                echo  '<div class="col-md-2"><i class="fa fa-arrow-right fa-2x" aria-hidden="true"></i></div>';
                                                }

                                                echo  '</div></div><br><br>';
                                            }
                                            else
                                            {
                                                echo  '<div class="col-md-2">';
                                                echo '<div class="card" style="width: 18rem;">';
                                                echo '<img class="card-img-top" src="'.base_url().'/assets/img/door.png" alt="Card image cap">';
                                                echo '<div class="card-body">';
                                                echo '<h5 class="card-title">'.$route[$i]['door'].'</h5>';
                                                echo '<p class="card-text">'.$route[$i]['description'].'</p>';
                                                echo '</div>';
                                                echo '</div>';
                                                echo  '</div>';

                                                if($i < count($route) -1 )
                                                {
                                                    echo  '<div class="col-md-2"><i class="fa fa-arrow-right fa-2x" aria-hidden="true"></i></div>';
                                                }
                                            }
                                        }

                                    }

                                    ?>
                                </fieldset>
                            </div>

                            <div id="content5" class="tab-pane">
                                <div class="row">
                                    <div class="col-md-12">
                                        <fieldset>
                                            <legend>Movimientos: <small><span class="badge" style="background-color: #00a65a">Exito</span> <span class="badge" style="background-color: #dd4b39">Error</span> </small></legend>
                                            <br>
                                                <div class="table responsive">
                                                   <table id="table-intents" class="table table-striped table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th>N</th>
                                                                <th>Puerta</th>
                                                                <th>Persona</th>
                                                                <th>Movimiento</th>
                                                                <th>Tipo</th>
                                                                <th>Vehículo</th>
                                                                <th>Fecha</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        <?php
                                                            if(!empty($intent))
                                                            {
                                                                for($i=0;$i<count($intent);$i++)
                                                                {
                                                                    $entry = $intent[$i]['entry'];

                                                                    if($entry == 0)
                                                                        $entry = 'ENTRADA';
                                                                    else
                                                                        $entry = 'SALIDA';

                                                                    if($intent[$i]['success'] == 1)
                                                                    {
                                                                    	echo '<tr style="background-color: #00a65a;color:white;">';
                                                                        echo '<td>'.($i+1).'</td>';
                                                                        echo '<td>'.$intent[$i]['door'].'</td>';
                                                                        echo '<td>'.$intent[$i]['rut'].'-'.$intent[$i]['digit'].' | '.$intent[$i]['name'].' '.$intent[$i]['last_name'].'</td>';
                                                                        echo '<td>'.$entry.'</td>';
                                                                        if ($intent[$i]['flow'] == 0) {
                                                                        	echo "<td>Peatonal</td>";
                                                                        }
                                                                        else {
                                                                        	echo "<td>Vehícular</td>";
                                                                        }
                                                                        if ($intent[$i]['patent'] != '') {
                                                                        	echo "<td>".$intent[$i]['patent']."</td>";
                                                                        }
                                                                        else {
                                                                        	echo '<td>N/A</td>';
                                                                        }
                                                                        echo '<td>'.$intent[$i]['created'].'</td>';
                                                                        echo '</tr>';
                                                                    }
                                                                    else
                                                                    {
                                                                        echo '<tr style="background-color: #dd4b39;color:white;">';
                                                                        echo '<td>'.($i+1).'</td>';
                                                                        echo '<td>'.$intent[$i]['door'].'</td>';
                                                                        echo '<td>'.$intent[$i]['description'].'</td>';
                                                                        echo '<td>'.$entry.'</td>';
                                                                        if ($intent[$i]['flow'] == 0) {
                                                                        	echo "<td>Peatonal</td>";
                                                                        }
                                                                        else {
                                                                        	echo "<td>Vehícular</td>";
                                                                        }
                                                                        if ($intent[$i]['patent'] != '') {
                                                                        	echo "<td>".$intent[$i]['patent']."</td>";
                                                                        }
                                                                        else {
                                                                        	echo '<td>N/A</td>';
                                                                        }
                                                                        echo '<td>'.$intent[$i]['created'].'</td>';
                                                                        echo '</tr>';
                                                                    }
                                                                    
                                                                }
                                                            }
                                                        ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                        </fieldset>
                                    </div>
                                </div>
                                <br>
                            </div>

                            <div id="content6" class="tab-pane">
                                <fieldset>
                                    <legend>Requisitos mínimos</legend>
                                    <table style="width: 100%;" class="table" id="table-form-in">
                                        <tbody>
                                            <tr>
                                                <th>Requisito</th>
                                                <th>Descripción</th>
                                            </tr>
                                            <?php if (!empty($requirement)) { ?>
                                            <?php foreach ($requirement as $k) { ?>
                                            	<tr>
                                            		<td><?php echo $k->requirement; ?></td>
                                            		<td><?php echo $k->description; ?></td>
                                            	</tr>
                                            <?php } ?>
                                            <?php } ?>
                                        </tbody>                        
                                    </table>
                                </fieldset>
                                <br>
                                <fieldset>
                                    <legend>Documentos</legend>
                                    <div class="row">
                                    	<?php if (!empty($document)) {
                                    		for ($i=0; $i < count($document); $i++) { ?>
                                    			<div class="wrimagecard wrimagecard-topimage col-md-12">
								                    <a href="<?php echo base_url().$document[$i]; ?>" target="_blank">
								                        <span><?php $direc = strrpos($document[$i], '/'); echo substr($document[$i], ($direc+1)); ?></span>
								                    </a>
								                  </div>
                                    		<?php }
                                    	} ?>
                                    </div>
                                </fieldset>
                            </div>

                            <div id="content7" class="tab-pane">
                                <fieldset>
                                    <legend>Ingresos</legend>
                                    <table id="projectscontrolin" class="table table-striped table-bordered table-condensed" style="width: 100%">
                                        <thead>
                                          <tr>
                                            <th>Título</th>
                                            <th>Observación</th>
                                            <th>Patente</th>
                                            <th>Fecha</th>
                                            <th>Acción</th>
                                          </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </fieldset>
                                <br>
                                <fieldset>
                                    <legend>Salidas</legend>
                                    <table id="projectscontrolout" class="table table-striped table-bordered table-condensed" style="width: 100%">
                                        <thead>
                                          <tr>
                                            <th>Título</th>
                                            <th>Observación</th>
                                            <th>Patente</th>
                                            <th>Fecha</th>
                                            <th>Acción</th>
                                          </tr>
                                        </thead>
                                        <tbody></tbody>
                                    </table>
                                </fieldset>
                            </div>
                        </main>
                    </div>
                    <div class="box-footer">
                        <a href="<?php echo site_url(); ?>/cProjects/" class="btn btn-primary pull-right" role="button">
                            <i class='fa fa-undo'></i> Volver
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<div id="modal-detail-projectsform" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Detalle de control</h4>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table id="details" style="width: 100%" class="table table-bordered table-striped"></table>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<?php $this->view('footer'); ?>

<script src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>

<script>
    $(document).ready(function() {

        $('#projectscontrolin').DataTable({
          "lengthMenu": [[5, 10, 15], [5, 10, 15]],
          'responsive': true,
          'paging': true,
          'info': true,
          'filter': true,
          'ordering': true,
          // 'stateSave': true,
          'processing':true,
          'serverSide':true,
          'language': {
            "url": base_url + "assets/Spanish.json"
          },
          "order": [[0, "asc"]],
          'ajax': {
            "url": site_url + "/CProjects_Forms/datain",
            "data": { projects_id: <?php echo $projects[0]['id'];?> },
            "type":"POST",
          },
          "columns": [
            { "data": "Título"},
            { "data": "Observación"},
            { "data": "Patente" },
            { "data": "Fecha" },
            { "data": "Acción" }
          ],
          "columnDefs": [
            {
              "targets": [0],
              "orderable": true,
              "render": function(data, type, row) {
                return row.title
              }
            },
            {
              "targets": [1],
              "orderable": true,
              "render": function(data, type, row) {
                return row.observation
              }
            },
            {
              "targets": [2],
              "orderable": true,
              "render": function(data, type, row) {
                return row.patent
              }
            },
            {
              "targets": [3],
              "orderable": true,
              "render": function(data, type, row) {
                return row.created
              }
            },
            {
              "targets": [4],
              "orderable": false,
              "render": function(data, type, row) {
                return `
                  <a href="#" class="btn btn-primary btn-xs" role="button" onclick="modaldetailprojectsform('`+<?php echo $projects[0]['id'];?>+`', '`+0+`')">
                    <i class='fa fa-search'></i> Ver
                </a>`
              }
            }
           ],
        });

        $('#projectscontrolout').DataTable({
          "lengthMenu": [[5, 10, 15], [5, 10, 15]],
          'responsive': true,
          'paging': true,
          'info': true,
          'filter': true,
          'ordering': true,
          // 'stateSave': true,
          'processing':true,
          'serverSide':true,
          'language': {
            "url": base_url + "assets/Spanish.json"
          },
          "order": [[0, "asc"]],
          'ajax': {
            "url": site_url + "/CProjects_Forms/dataout",
            "data": { projects_id: <?php echo $projects[0]['id'];?> },
            "type":"POST",
          },
          "columns": [
            { "data": "Título"},
            { "data": "Observación"},
            { "data": "Patente" },
            { "data": "Fecha" },
            { "data": "Acción" }
          ],
          "columnDefs": [
            {
              "targets": [0],
              "orderable": true,
              "render": function(data, type, row) {
                return row.title
              }
            },
            {
              "targets": [1],
              "orderable": true,
              "render": function(data, type, row) {
                return row.observation
              }
            },
            {
              "targets": [2],
              "orderable": true,
              "render": function(data, type, row) {
                return row.patent
              }
            },
            {
              "targets": [3],
              "orderable": true,
              "render": function(data, type, row) {
                return row.created
              }
            },
            {
              "targets": [4],
              "orderable": false,
              "render": function(data, type, row) {
                return `
                  <a href="#" class="btn btn-primary btn-xs" role="button" onclick="modaldetailprojectsform('`+<?php echo $projects[0]['id'];?>+`', '`+1+`')">
                    <i class='fa fa-search'></i> Ver
                </a>`
              }
            }
           ],
        });

        $('#table-intents').DataTable({
            "searching": false,
            'language': {
                "url": base_url + "assets/Spanish.json"
              },
            dom: 'Bfrtip',
            buttons: [
                'excel'
            ],
          });

      	$('#li-contractors').addClass('menu-open');
        $('#ul-contractors').css('display', 'block');
    });

    function modaldetailprojectsform(projects, control){
        $.ajax({
            url: site_url + '/CProjects_Forms/details',
            type: 'POST',
            dataType: 'json',
            data: {
                projects_id: projects,
                control: control
            },
            success: function(data){
                if (data.mensaje != 0) {
                    var html = '';
                    html += '<tbody>';
                    html += '<tr><td><label>Título</label></td>';
                    html += '<td><label>'+data.det[0].title+'</label></td></tr>';
                    html += '<tr><td><label>Observación</label></td>';
                    html += '<td><label>'+data.det[0].observation+'</label></td></tr>';
                    html += '<tr><td><label>Patente</label></td>';
                    html += '<td><label>'+data.det[0].patent+'</label></td></tr>';
                    html += '<tr><td><label>Tipo</label></td>';
                    html += '<td><label>'+data.det[0].type+'</label></td></tr>';
                    for (var i = 0; i < data.det.length; i++) {
                        html += '<tr><td><label>'+data.det[i].question+'</label></td>';
                        html += '<td><label>'+data.det[i].answer+'</label></td></tr>';
                    }
                    html += '</tbody>';
                    $("#details").html(html);
                    $("#modal-detail-projectsform").modal('show');
                }
            }
        });
    }
</script>
</body>
</html>