<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-success">
					<div class="box-header ui-sortable-handle">
					    <i class="fa fa-briefcase"></i>
						<h3 class="box-title">Departamento</h3>
				  	</div>
					
					<div class="box-body">
			  			<section class="content">
			  			    <table id="department" class="table">
		  		    		    <tbody>
		  		    		    	<tr>
		  		    		    		<th>Id</th>
		  			    				<td><?php echo $department[0]->id;?></td>
		  		    		    	</tr>
		  		    		    	<tr>
		  		    		    		<th>Departamento</th>
		  			    				<td><?php echo $department[0]->department;?></td>
		  		    		    	</tr>
		  		    		    	<tr>
		  		    	    			<th>Área</th>
		  		    	    			<td><?php echo $department[0]->area;?></td>
		  		    	    		</tr>
		  		    	    		<tr>
		  		    	    			<th>Encargado</th>
		  		    	    			<td><?php echo $encargado[0]->name.' '.$encargado[0]->last_name;?></td>
		  		    	    		</tr>
		  		    	    		<tr>
		  		    	    			<th>Creado</th>
		  		    	    			<td><?php echo $department[0]->created;?></td>
		  		    	    		</tr>
		  		    	    		<tr>
		  		    	    			<th>Modificado</th>
		  		    	    			<td><?php echo $department[0]->modified;?></td>
		  		    	    		</tr>
		  		    		    </tbody>
		  			    	</table>
			  			</section>
				  	</div>
				  	<div class="box-footer">
				  		<a href="<?php echo site_url(); ?>/cDepartments/" class="btn btn-primary pull-right" role="button">
	                    <i class='fa fa-undo'></i> Volver
	                </a>
				  	</div>
				</div>
			</div>
		</div>
	</section>
	
</div>

<?php $this->view('footer'); ?>

<script>
    $(document).ready(function() {
      	$('#li-configuration').addClass('menu-open');
      	$('#ul-configuration').css('display', 'block');
      	
		$('#li-ubications').addClass('menu-open');
      	$('#ul-ubications').css('display', 'block');

		$('#li-departments').addClass('menu-open');
		$('#ul-departments').css('display', 'block');
    });
</script>
</body>
</html>