<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-success">
					<div class="box-header ui-sortable-handle">
					    <i class="fa fa-briefcase"></i>
				  	</div>
				  	
				  	<form class="form-horizontal" id="editDepartment">
			  			<div class="box-body">
			  				<div class="form-group">
			  					<label for="departments" class="col-sm-2 control-label">Departamento</label>
			  					<div class="col-sm-10">
			  						<input type="text" class="form-control" name="departments" id="departments" required value="<?php echo $department[0]->department; ?>">
			  					</div>
			  				</div>
			  				<div class="form-group">
			  					<label for="areas" class="col-sm-2 control-label">Área</label>
			  					<div class="col-sm-5">
			  						<select name="areas" id="areas" class="form-control" required>
			  							<?php 
			  								if (!empty($areas)) { ?>
			  								 	<option value="">Seleccione una opción</option>
			  								<?php 
			  									foreach ($areas as $key) { ?>
			  										<option value="<?php echo $key->id; ?>"><?php echo $key->area; ?></option>
	  											<?php }
										} ?>
			  						</select>
			  					</div>
			  				</div>
			  				<div class="form-group">
			  					<label for="in_charge" class="col-sm-2 control-label">Encargado</label>
			  					<div class="col-sm-5">
			  						<select name="in_charge" id="in_charge" class="form-control" required>
			  							<?php 
			  								if (!empty($encargado)) { ?>
			  								 	<option value="">Seleccione una opción</option>
			  								<?php 
			  									foreach ($encargado as $key) { ?>
			  										<option value="<?php echo $key->id; ?>"><?php echo $key->name.' '.$key->last_name.' / '.$key->profile; ?></option>
	  											<?php }
										} ?>
			  						</select>
			  					</div>
			  				</div>
			  			</div>
			  			<div class="box-footer">
			  				<button type="submit" class="btn btn-primary pull-right">Guardar</button>
			  			</div>
			  		</form>
				</div>
			</div>
		</div>
	</section>
	
</div>


<?php $this->view('footer'); ?>

<script>
	$(document).ready(function() {
		$("#areas").val(<?php echo $department[0]->areas_id; ?>);
		$("#in_charge").val(<?php echo $department[0]->in_charge; ?>);

		$("#editDepartment").submit(function(event) {
			event.preventDefault();

			$.post(
				site_url + "/cDepartments/editDepartments",{
					id 			: <?php echo $department[0]->id; ?>,
					department 	: $("#departments").val(),
					areas_id 	: $("#areas").val(),
					in_charge 	: $("#in_charge").val()
				},
				function(data){
					if (data == 1) {
						window.location.replace(site_url+"/cDepartments/");
					}
					else {
						alert("Error en el proceso...")
						window.location.replace(site_url+"/cDepartments/edit?id="+<?php echo $department[0]->id; ?>);
					}
				}
			);
		});

		
		$('#li-configuration').addClass('menu-open');
      	$('#ul-configuration').css('display', 'block');
      	
		$('#li-ubications').addClass('menu-open');
      	$('#ul-ubications').css('display', 'block');

		$('#li-departments').addClass('menu-open');
		$('#ul-departments').css('display', 'block');
	});
	
</script>
</body>
</html>
