<div class="content-wrapper">
	<br>
	<section class="content">
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-success">
					<div class="box-header ui-sortable-handle">
					    <i class="fa fa-briefcase"></i>
              			<i class="fa fa-sign-in"></i>
						<h3 class="box-title">Vincular Departamento - Puerta</h3>
				  	</div>

				  	<form class="form-horizontal" id="form-add-door_deparments">
			  			<div class="box-body">
			  				<div class="form-group">
			  					<label for="department" class="col-sm-2 control-label">Departamento</label>
			  					<div class="col-sm-10">
			  						<select required class="form-control" name="department" id="department" required>
			  							<?php 
			  								if(!empty($department)) { ?>
			  									<option value="">Seleccione una opción</option>
		  									<?php
			  									foreach($department as $a) {
			  										echo '<option value="'.$a['id'].'">'.$a['department'].'</option>';
			  									}
			  								} ?>
			  						</select>
			  					</div>
			  				</div>
			  				<div class="form-group">
			  					<label for="door" class="col-sm-2 control-label">Puerta</label>
			  					<div class="col-sm-10">
			  						<select required class="form-control" name="door" id="door" required>
			  							<?php 
			  								if(!empty($doors)) { ?>
												<option value="">Seleccione una opción</option> 
											<?php
			  									foreach($doors as $d) {
			  										echo '<option value="'.$d['id'].'">'.$d['door'].'</option>';
			  									}
			  								} ?>
			  						</select>
			  					</div>
			  				</div>
			  			</div>
			  			<div class="box-footer">
			  				<button type="submit" class="btn btn-primary pull-right">Guardar</button>
			  			</div>
			  		</form>

				</div>
			</div>
		</div>
	</section>
</div>

<?php $this->view('footer'); ?>

<script>
	$(document).ready(function() {
		$("#form-add-door_deparments").submit(function(event) {
			event.preventDefault();
			addArea_Department();
		});


		$('#li-configuration').addClass('menu-open');
      	$('#ul-configuration').css('display', 'block');
      	
		$('#li-ubications').addClass('menu-open');
      	$('#ul-ubications').css('display', 'block');

		$('#li-departments').addClass('menu-open');
		$('#ul-departments').css('display', 'block');
		
	});

	function addArea_Department()
	{
		$.post(
			site_url + "/cDoors_Departments/addDoor_Department",{
				department: $('#department').val(),
				door : 	$("#door").val()
			},
			function(data){
				if (data == 1) {
					window.location.replace(site_url+"/cDoors_Departments/");
				}
				else {
					alert('Se ha producido un error.\nVerifique que esta vinculación no exista.');
				}
			}
		);
	}
</script>
</body>
</html>
