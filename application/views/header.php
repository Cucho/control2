<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Control de Accesos</title>
  <link href="<?php echo base_url();?>assets/img/favicon.ico" type="image/x-icon" rel="icon"/><link href="/access_control/favicon.ico" type="image/x-icon" rel="shortcut icon"/>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

  <link rel="stylesheet" href="<?php echo base_url()?>assets/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo base_url()?>assets/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="<?php echo base_url()?>assets/Ionicons/css/ionicons.min.css">
  <link rel="stylesheet" href="<?php echo base_url()?>assets/css/AdminLTE.min.css">
  <link rel="stylesheet" href="<?php echo base_url()?>assets/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="<?php echo base_url()?>assets/morris.js/morris.css">
  <link rel="stylesheet" href="<?php echo base_url()?>assets/jvectormap/jquery-jvectormap.css">
  <link rel="stylesheet" href="<?php echo base_url()?>assets/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <link rel="stylesheet" href="<?php echo base_url()?>assets/bootstrap-daterangepicker/daterangepicker.css">
  <link rel="stylesheet" href="<?php echo base_url()?>assets/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

  <link rel="stylesheet" href="<?php echo base_url()?>assets/datatables.net-bs/css/dataTables.bootstrap.min.css">
  <link rel="stylesheet" href="<?php echo base_url()?>assets/responsive.dataTables.min.css">

  <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/treeview/bootstrap-theme.min.css" />
  <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/treeview/gijgo.min.css" />
  
  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

  <link rel="stylesheet" href="<?php echo base_url()?>assets/wizard.css">
  
  <!-- JS NODE -->
  <script src="<?php echo base_url();?>estadoActual/node_modules/socket.io-client/dist/socket.io.js"></script>
  <?php 
      //es porteria
       $this->db->select('pop_up');
       $this->db->from('main_access');
       //reemplazar ip fija por la que se rescata en session
       $this->db->where('ip_host', $this->session->userdata('ip'));
       $this->db->limit(1);
       $res = $this->db->get()->result_array();
       if(empty($res[0]['pop_up']) ||  $res[0]['pop_up'] == 0)
      { ?>
         <script src="<?php echo base_url();?>estadoActual/public/main_no_pop_up.js"></script> 
      <?php
      }
       else
      {?>
         <script src="<?php echo base_url();?>estadoActual/public/main.js"></script>
      <?php
      }
  ?>
 
  <!--  -->

  <script> var site_url = '<?php echo site_url() ?>'; </script>
  <script> var base_url = '<?php echo base_url() ?>'; </script>

  
  
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="<?php echo base_url();?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>C</b>A</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Control&nbsp;</b>Accesos</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          <!-- campana -->
          <li title="Estado instalación" onclick="redirectStateActual();" class="dropdown messages-menu">
            <a style="cursor: pointer;" target="_blank" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-university"></i>
              <span class="label label-danger"></span>
            </a>
            <!--
            <ul class="dropdown-menu" id="ul-bell-notification">
              <li class="header">You have 0 messages</li>
            </ul>
            -->
          </li>

          <li title="Listado ingresados" onclick="redirectStateActualList();" class="dropdown messages-menu">
            <a style="cursor: pointer;" target="_blank" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-list-alt"></i>
              <span class="label label-danger"></span>
            </a>
            <!--
            <ul class="dropdown-menu" id="ul-bell-notification">
              <li class="header">You have 0 messages</li>
            </ul>
            -->
          </li>

          <!-- campana -->
          <li title="Notificaciones" class="dropdown messages-menu" onclick="redirectNotification();">
            <a style="cursor: pointer;" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-bell-o"></i>
              <span class="label label-danger" id="label-bell-notification"></span>
            </a>
            <!--
            <ul class="dropdown-menu" id="ul-bell-notification">
              <li class="header">You have 0 messages</li>
            </ul>
            -->
          </li>

          <!-- qr code -->
          <li title="Generar QR" class="dropdown messages-menu" onclick="redirectGenerateQRCode();">
            <a style="cursor: pointer;" class="dropdown-toggle" data-toggle="dropdown">
              <i class="fa fa-qrcode"></i>
              <span class="label label-danger""></span>
            </a>
            <!--
            <ul class="dropdown-menu" id="ul-bell-notification">
              <li class="header">You have 0 messages</li>
            </ul>
            -->
          </li>
          
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <!--<img src="<?php //echo base_url()?>assets/img/user2-160x160.jpg" class="user-image" alt="User Image">-->
              <span class="hidden-xs">
                <?php
                    $name = ''; 
                    if($this->session->userdata('name'))
                    {
                      $name .= $this->session->userdata('name').' ';
                    }

                    if($this->session->userdata('last_name'))
                    {
                      $name .= $this->session->userdata('last_name');
                    }

                    echo $name;
                  ?>
              </span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <font color="white"><i class="fa fa-user fa-5x"></i></font>
                <p>
                  <?php
                    $name = ''; 
                    if($this->session->userdata('name'))
                    {
                      $name .= $this->session->userdata('name').' ';
                    }

                    if($this->session->userdata('last_name'))
                    {
                      $name .= $this->session->userdata('last_name');
                    }

                    echo $name;
                  ?>
                  <small> 
                  <?php
                    if($this->session->userdata('rol'))
                      echo $this->session->userdata('rol');
                  ?> </small>
                </p>
              </li>
              <!-- Menu Body -->
              <!--
              <li class="user-body">
                <div class="row">
                  <div class="col-xs-4 text-center">
                    <a href="#">Followers</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Sales</a>
                  </div>
                  <div class="col-xs-4 text-center">
                    <a href="#">Friends</a>
                  </div>
                </div>
              </li>
              -->
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="<?php echo site_url('Welcome/profile') ?>" class="btn btn-default btn-flat">Configuración</a>
                </div>
                <div class="pull-right">
                  <a href="<?php echo site_url("Welcome/Logout");?>" class="btn btn-default btn-flat">Salir</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>

  <!-- MODALS notificacion-->
<div  id="modal-wrong_event" class="modal fade" role="dialog">
  <div class="modal-dialog modal-md">
      <div class="modal-content">
          <div class="modal-header" style="background: red; color: white;">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title" id="modal-wrong_event-title"></h4> 
            
          </div>
          <div class="modal-body" id="modal-wrong_event-body">
           
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
          </div>
      </div>
  </div>
</div>
<!-- FIN MODALS -->

<audio id="audio" controls style="display: none">
<source type="audio/wav" src="<?php echo base_url();?>assets/alarma_notification.mp3">
</audio>

<script type="text/javascript">
  function redirectNotification()
  {
    $.ajax({
      url: 'http://<?php echo NODE_HOST;?>:<?php echo NODE_PORT;?>/clean_notification',
      data: {clean:1},
      type: 'get',
      dataType: 'text',
      success: function(response)
      { 
        window.location.href = "<?php echo site_url();?>/cNotifications/";
      }
    });
    
  }


  function redirectStateActual()
  {
    window.location.href = "<?php echo site_url('CAccess/State');?>";
  }

  function redirectStateActualList()
  {
    window.location.href = "<?php echo site_url('CAccess/Access_In');?>";
  }

  function redirectGenerateQRCode()
  {
    window.location.href = "<?php echo site_url('CAccess/GenerateQRCode');?>";
  }
</script>
