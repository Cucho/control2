<div class="content-wrapper">
	<section class="content">
		<div class="row">
			<div class="col-sm-12">
				<div class="box box-success">
					<div class="box-header ui-sortable-handle">
					    <i class="fa fa-sign-in"></i>
						<h3 class="box-title">Agregar tipo puerta</h3>
				  	</div>

				  	<form class="form-horizontal" id="addDoors_Type">
			  			<div class="box-body">
			  				<div class="form-group">
			  					<label for="doors_type" class="col-sm-2 control-label">Tipo puerta</label>
			  					<div class="col-sm-10">
			  						<input type="text" class="form-control" name="doors_type" id="doors_type" required>
			  					</div>
			  				</div>
			  			</div>
			  			<div class="box-footer">
			  				<button type="submit" class="btn btn-primary pull-right">Guardar</button>
			  			</div>
			  		</form>
				</div>
			</div>
		</div>
	</section>
	
</div>

<?php $this->view('footer'); ?>

<script>
	$(document).ready(function() {
		$("#addDoors_Type").submit(function(event) {
			event.preventDefault();

			$.post(
				site_url + "/cDoors_Type/addDoors_Type",{
					doors_type : 	$("#doors_type").val()
				},
				function(data){
					if (data == 1) {
						window.location.replace(site_url+"/cDoors_Type/");
					}
					else {
						alert("Error en el proceso...")
						window.location.replace(site_url+"/cDoors_Type/add");
					}
				}
			);
		});

		$('#li-doors').addClass('menu-open');
		$('#ul-doors').css('display', 'block');
	});
</script>
</body>
</html>
