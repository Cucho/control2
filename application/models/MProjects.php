<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MProjects extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function getProjects($start, $length, $search, $order, $by) {
		$retornar = array();
		if ($search) {
			$busca = $this->getSearchProject($search, $start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCountSearch($search, $start, $length, $order, $by);
			$retornar['data'] = $busca['datos'];
		}
		else {
			$todo = $this->getAllProjects($start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCount();
			$retornar['data'] = $todo['datos'];
		}

		$retornar['numDataTotal'] = $this->getCount();

		return $retornar;
	}

	public function getProject($id) {
		$this->db->select('projects.id as id, title, description, in_charge, init, end, DATE_FORMAT(projects.created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(projects.modified, "%d-%m-%Y %H:%i:%s") as modified, people.id as pid, rut, digit, name, last_name, people.address, people.email, people.phone, people_profiles_id, companies_id, profile, company');
		$this->db->from('projects');
		$this->db->join('people', 'people.id = in_charge');
		$this->db->join('people_profiles', 'people_profiles.id = people_profiles_id');
		$this->db->join('companies', 'companies.id = companies_id');
		$this->db->where('projects.id', $id);
		$this->db->limit(1);

		return $this->db->get()->result_array();
	}

	public function getPeopleExternal($id){
		$this->db->select('people.id as pid, rut, digit, name, last_name, people.address, people.email, people.phone, profile, company');
		$this->db->from('projects_people');
		$this->db->join('people', 'people.id = projects_people.people_id');
		$this->db->join('people_profiles', 'people_profiles.id = people.people_profiles_id');
		$this->db->join('companies', 'companies.id = people.companies_id');
		$this->db->where('projects_id', $id);
		$this->db->order_by("rut", "desc");
		return $this->db->get()->result_array();
	}

	public function getVehicles($id){
		$this->db->select('control_init, control_end, patent, company, type, people.id as pid, rut, digit, name, last_name, people.address, people.email, people.phone, profile, vehicles.id as vid');
		$this->db->from('projects_vehicles');
		$this->db->join('vehicles', 'vehicles.id = projects_vehicles.vehicles_id');
		$this->db->join('vehicles_type', 'vehicles_type.id = vehicles.vehicles_type_id');
		$this->db->join('people', 'people.id = vehicles.people_id');
		$this->db->join('people_profiles', 'people_profiles.id = people.people_profiles_id');
		$this->db->join('companies', 'companies.id = vehicles.companies_id');
		$this->db->where('projects_id', $id);
		return $this->db->get()->result();
	}

	public function getHorarios($id){
		$this->db->select('time_init, time_end, L, M, MI, J, V, S, D');
		$this->db->from('projects_schedules');
		$this->db->where('projects_id', $id);
		return $this->db->get()->result_array();
	}

	public function getEncargado($id){
		$this->db->select('people.id, rut, digit, name, last_name, people.address, people.email, people.phone, profile, company');
		$this->db->from('projects');
		$this->db->join('people', 'people.id = projects.in_charge_installation');
		$this->db->join('people_profiles', 'people_profiles.id = people.people_profiles_id');
		$this->db->join('companies', 'companies.id = people.companies_id');
		$this->db->where('projects.id', $id);
		return $this->db->get()->result_array();
	}

	public function getZones($id){
		$this->db->select('zone, id');
		$this->db->from('projects_zones');
		$this->db->join('zones', 'zones.id = projects_zones.zones_id');
		$this->db->where('projects_zones.projects_id', $id);
		return $this->db->get()->result_array();
	}

	public function getAreas($id){
		$this->db->select('area, id');
		$this->db->from('projects_areas');
		$this->db->join('areas', 'areas.id = projects_areas.areas_id');
		$this->db->where('projects_areas.projects_id', $id);
		return $this->db->get()->result_array();
	}

	public function getDptos($id){
		$this->db->select('department, id');
		$this->db->from('projects_departments');
		$this->db->join('departments', 'departments.id = projects_departments.departments_id');
		$this->db->where('projects_departments.projects_id', $id);
		return $this->db->get()->result_array();
	}

	public function getRoute($id){
		$this->db->select('door, description, id');
		$this->db->from('projects_route');
		$this->db->join('doors', 'doors.id = projects_route.doors_id');
		$this->db->where('projects_route.projects_id', $id);
		return $this->db->get()->result_array();
	}

	public function getIntent($id){
		$this->db->select('door, description, entry, success, DATE_FORMAT(projects_intents.created, "%d-%m-%Y %H:%i:%s") as created, flow, patent, rut, digit, name, last_name');
		$this->db->from('projects_intents');
		$this->db->join('doors', 'doors.id = projects_intents.doors_id');
		$this->db->join('vehicles', 'vehicles.id = projects_intents.vehicles_id', 'left');
		$this->db->join('people', 'people.id = projects_intents.people_id');
		$this->db->where('projects_id', $id);
		$this->db->order_by('created','asc');
		return $this->db->get()->result_array();
	}

	public function getRequeriment($id){
		$this->db->select('requirement, description, id');
		$this->db->from('projects_minimum_requirements');
		$this->db->join('minimum_requirements', 'minimum_requirements.id = projects_minimum_requirements.minimum_requirements');
		$this->db->where('projects_id', $id);
		$this->db->order_by('created','asc');
		return $this->db->get()->result();
	}

	// Funciones auxiliares datatable
	public function getAllProjects($start, $length, $order, $by) {
		$this->db->select('projects.id as id, title, description, in_charge, init, end, DATE_FORMAT(projects.created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(projects.modified, "%d-%m-%Y %H:%i:%s") as modified, people.id as pid, rut, digit, name, last_name, people.address, people.email, people.phone, people_profiles_id, companies_id, profile, company');
		$this->db->join('people', 'people.id = in_charge');
		$this->db->join('people_profiles', 'people_profiles.id = people_profiles_id');
		$this->db->join('companies', 'companies.id = companies_id');
		switch ($by) {
			case 0:
				$this->db->order_by('projects.id', $order);
				break;
			case 1:
				$this->db->order_by('title', $order);
				break;
			case 2:
				$this->db->order_by('in_charge', $order);
				break;
			case 3:
				$this->db->order_by('init', $order);
				break;
			case 4:
				$this->db->order_by('end', $order);
				break;
		}
		$this->db->limit($length, $start);
		$query = $this->db->get('projects');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getSearchProject($search, $start, $length, $order, $by) {
		$this->db->select('projects.id, title, description, in_charge, init, end, DATE_FORMAT(created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(modified, "%d-%m-%Y %H:%i:%s") as modified');
		$this->db->like('projects.id', $search);
		$this->db->or_like('title', $search);
		$this->db->or_like('in_charge', $search);
		$this->db->or_like('init', $search);
		$this->db->or_like('end', $search);
		switch ($by) {
			case 0:
				$this->db->order_by('id', $order);
				break;
			case 1:
				$this->db->order_by('title', $order);
				break;
			case 2:
				$this->db->order_by('in_charge', $order);
				break;
			case 3:
				$this->db->order_by('init', $order);
				break;
			case 4:
				$this->db->order_by('end', $order);
				break;
		}
		$this->db->limit($length, $start);
		$query = $this->db->get('projects');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getCount() {
		return $this->db->count_all('projects');
	}

	public function getCountSearch($search, $start, $length, $order, $by) {
		$this->db->select('projects.id');
		$this->db->like('id', $search);
		$this->db->or_like('title', $search);
		$this->db->or_like('in_charge', $search);
		$this->db->or_like('init', $search);
		$this->db->or_like('end', $search);
		$quer = $this->db->get('projects')->num_rows();
		return $quer;
	}
	// fin funciones auxiliares

	//Crud
	public function editProject($data, $id) {
		$this->db->where('id', $id);
		if($this->db->update('projects', $data))
			return true;
		else
			return false;
	}

	public function deleteProject($id) {
		$this->db->where('id', $id);
		if($this->db->delete('projects'))
			return true;
		else
			return false;
	}

	public function searchPeople($rut){
		$this->db->select('people.id, name, last_name, people.phone, people.email, people_profiles.profile, companies.company, people.internal, people.states_id');
		$this->db->from('people');
		$this->db->join('people_profiles', 'people_profiles.id = people.people_profiles_id');
		$this->db->join('companies', 'companies.id = people.companies_id');
		$this->db->where('people.rut', $rut);
		return $this->db->get()->result();
	}

	public function addPeople($data){
		if($this->db->insert('people', $data))
			return true;
		else
			return false;
	}

	public function SearchVehicle($patent){
		$this->db->select('patent, model, vehicles.internal, vehicles.nfc_code, vehicles.companies_id, company, people_id, people.name, people.last_name, vehicles_type_id, type, vehicles.states_id');
		$this->db->from('vehicles');
		$this->db->join('companies', 'companies.id = vehicles.companies_id');
		$this->db->join('people', 'people.id = vehicles.people_id');
		$this->db->join('vehicles_type', 'vehicles_type.id = vehicles.vehicles_type_id');
		$this->db->where('patent', $patent);
		return $this->db->get()->result();
	}

	public function addVehicle($data){
		if ($this->db->insert('vehicles', $data)) {
			return true;
		}
		else {
			return false;
		}
	}

	public function addRequirement($data){
		if($this->db->insert('minimum_requirements', $data))
			return $this->db->insert_id();
		else
			return false;
	}

	public function searchId($rut){
		$this->db->select('id');
		$this->db->where('rut', $rut);
		$q = $this->db->get('people')->result();
		foreach ($q as $k) {
			return $k->id;
		}
	}

	public function searchByPatent($patent){
		$this->db->select('id');
		$this->db->where('patent', $patent);
		$q = $this->db->get('vehicles')->result();
		foreach ($q as $k) {
			return $k->id;
		}
	}

	public function addProjects($data){
		if ($this->db->insert('projects', $data))
			return $this->db->insert_id();
		else
			return false;
	}

	public function editProjects($data){
		if ($this->db->replace('projects', $data))
			return true;
		else
			return false;
	}

	public function addProjectAreas($data){
		if ($this->db->insert('projects_areas', $data))
			return true;
		else
			return false;
	}

	public function editProjectAreas($id){
		$this->db->where('projects_id', $id);
		$this->db->delete('projects_areas');
	}

	public function addProjectDptos($data){
		if ($this->db->insert('projects_departments', $data))
			return true;
		else
			return false;
	}

	public function editProjectDptos($id){
		$this->db->where('projects_id', $id);
		$this->db->delete('projects_departments');
	}

	public function addProjectZones($data){
		if ($this->db->insert('projects_zones', $data))
			return true;
		else
			return false;
	}

	public function editProjectZones($id){
		$this->db->where('projects_id', $id);
		$this->db->delete('projects_zones');
	}

	public function addProjectRoute($data){
		if ($this->db->insert('projects_route', $data))
			return true;
		else
			return false;
	}

	public function editProjectRoute($id){
		$this->db->where('projects_id', $id);
		$this->db->delete('projects_route');
	}

	public function addProjectRequirement($data){
		if ($this->db->insert('projects_minimum_requirements', $data))
			return true;
		else
			return false;
	}

	public function editProjectRequirement($id){
		$this->db->where('projects_id', $id);
		$this->db->delete('projects_minimum_requirements');
	}

	public function addProjectPeople($data){
		if ($this->db->insert('projects_people', $data))
			return true;
		else
			return false;
	}

	public function editProjectPeople($id){
		$this->db->where('projects_id', $id);
		$this->db->delete('projects_people');
	}

	public function addProjectVehicles($data){
		if ($this->db->insert('projects_vehicles', $data))
			return true;
		else
			return false;
	}

	public function editProjectVehicles($id){
		$this->db->where('projects_id', $id);
		$this->db->delete('projects_vehicles');
	}

	public function addProjectSchedules($data){
		if ($this->db->insert('projects_schedules', $data))
			return true;
		else
			return false;
	}

	public function editProjectSchedules($id){
		$this->db->where('projects_id', $id);
		$this->db->delete('projects_schedules');
	}

	public function getAllProfiles(){
		$this->db->select('id, profile');
		$this->db->from('people_profiles');
		return $this->db->get()->result();
	}

	public function getAllCompanies(){
		$this->db->select('id, company');
		$this->db->from('companies');
		return $this->db->get()->result();
	}

	public function getAllVehicle_Type(){
		$this->db->select('id, type');
		$this->db->from('vehicles_type');
		return $this->db->get()->result();
	}

	public function getAllVehicleProfiles(){
		$this->db->select('id, profile');
		$this->db->from('vehicles_profiles');
		return $this->db->get()->result();
	}

	public function getAllPeopleInternal(){
		$this->db->select('id, name, last_name');
		$this->db->from('people');
		$this->db->where('internal', 1);
		return $this->db->get()->result();
	}

	public function GetTreeview() {
		$this->db->select('id, zone');
		$this->db->from('zones');
		$this->db->order_by('zone');
		$z = $this->db->get()->result();

		$this->db->select('id, area, zones_id');
		$this->db->from('areas');
		$this->db->order_by('area');
		$a = $this->db->get()->result();

		$this->db->select('id, department, areas_id');
		$this->db->from('departments');
		$this->db->order_by('department');
		$d = $this->db->get()->result();

		$this->db->select('zones_id, doors_id as dzid');
		$this->db->from('doors_zones');
		$dz = $this->db->get()->result();

		$this->db->select('areas_id, doors_id as daid');
		$this->db->from('doors_areas');
		$da = $this->db->get()->result();

		$this->db->select('departments_id, doors_id as ddid');
		$this->db->from('doors_departments');
		$dd = $this->db->get()->result();

		$object = array();
		
		$zone = array();
		foreach ($z as $k) {
			$zone[] = array(
				'zid' => $k->id,
				'zone' => $k->zone,
			);
		}
		$area = array();
		foreach ($a as $y) {
			$area[] = array(
				'aid' => $y->id,
				'area' => $y->area,
				'area_parent' => $y->zones_id,
			);
		}
		$dpto = array();
		foreach ($d as $e) {
			$dpto[] = array(
				'did' => $e->id,
				'department' => $e->department,
				'department_parent' => $e->areas_id,
			);
		}
		$dzones = array();
		foreach ($dz as $e) {
			$dzones[] = array(
				'door_z' => $e->dzid,
				'zones_id' => $e->zones_id
			);
		}
		$dareas = array();
		foreach ($da as $e) {
			$dareas[] = array(
				'door_a' => $e->daid,
				'areas_id' => $e->areas_id
			);
		}
		$ddepto = array();
		foreach ($dd as $e) {
			$ddepto[] = array(
				'door_d' => $e->ddid,
				'departments_id' => $e->departments_id
			);
		}

		return $object[] = array(
			'zona' => $zone,
			'area' => $area,
			'dpto' => $dpto,
			'dzones' => $dzones,
			'dareas' => $dareas,
			'ddepto' => $ddepto
		);
	}

	public function getDoorLevel() {
		$this->db->select('id, door, description, level');
		$this->db->from('doors');
		return $this->db->get()->result();
	}

	public function getAllMinimum() {
		$this->db->select('id, requirement, description');
		$this->db->from('minimum_requirements');
		return $this->db->get()->result();
	}
}
