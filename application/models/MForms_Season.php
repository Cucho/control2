<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MForms_Season extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function getForms_Season($start, $length, $search, $order, $by) {
		$retornar = array();
		if ($search) {
			$busca = $this->getSearchForms_Season($search, $start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCountSearch($search, $start, $length, $order, $by);
			$retornar['data'] = $busca['datos'];
		}
		else {
			$todo = $this->getAllForms_Season($start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCount();
			$retornar['data'] = $todo['datos'];
		}

		$retornar['numDataTotal'] = $this->getCount();

		return $retornar;
	}

	// Funciones auxiliares datatable
	public function getAllForms_Season($start, $length, $order, $by) {
		$this->db->select('vechiles_type_id, forms_id, seasons_id, year, vehicles_type.id, vehicles_type.type, forms.id, forms.title, seasons.id, season');
		$this->db->join('vehicles_type', 'vehicles_type.id = forms_season.vechiles_type_id');
		$this->db->join('forms', 'forms.id = forms_season.forms_id');
		$this->db->join('seasons', 'seasons.id = forms_season.seasons_id');
		switch ($by) {
			case 0:
				$this->db->order_by('vechiles_type_id', $order);
				break;
			case 1:
				$this->db->order_by('forms_id', $order);
				break;
			case 2:
				$this->db->order_by('seasons_id', $order);
				break;
			case 3:
				$this->db->order_by('year', $order);
				break;
		}
		$this->db->limit($length, $start);
		$query = $this->db->get('forms_season');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getSearchSpecial_Schedule($search, $start, $length, $order, $by) {
		$this->db->select('vechiles_type_id, forms_id, seasons_id, year, vehicles_type.id, vehicles_type.type, forms.id, forms.title');
		$this->db->join('vehicles_type', 'vehicles_type.id = forms_season.vechiles_type_id');
		$this->db->join('forms', 'forms.id = forms_season.forms_id');
		$this->db->like('vechiles_type_id', $search);
		$this->db->or_like('forms_id', $search);
		$this->db->or_like('seasons_id', $search);
		$this->db->or_like('year', $search);
		switch ($by) {
			case 0:
				$this->db->order_by('vechiles_type_id', $order);
				break;
			case 1:
				$this->db->order_by('forms_id', $order);
				break;
			case 2:
				$this->db->order_by('seasons_id', $order);
				break;
			case 3:
				$this->db->order_by('year', $order);
				break;
		}
		$this->db->limit($length, $start);
		$query = $this->db->get('forms_season');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getCount() {
		return $this->db->count_all('forms_season');
	}

	public function getCountSearch($search, $start, $length, $order, $by) {
		$this->db->select('vechiles_type_id');
		$this->db->join('vehicles_type', 'vehicles_type.id = forms_season.vechiles_type_id');
		$this->db->join('forms', 'forms.id = forms_season.forms_id');
		$this->db->like('vechiles_type_id', $search);
		$this->db->or_like('forms_id', $search);
		$this->db->or_like('seasons_id', $search);
		$this->db->or_like('year', $search);
		$quer = $this->db->get('forms_season')->num_rows();
		return $quer;
	}
	// fin funciones auxiliares

	//Crud
	public function addForm_Season($data) {
		if($this->db->insert('forms_season', $data))
			return true;
		else
			return false;
	}

	public function deleteForm_Season($id) {
		if($this->db->delete('forms_season', $id))
			return true;
		else
			return false;
	}

	public function getAllVehicles_Type() {
		$this->db->select('id, type');
		$this->db->from('vehicles_type');
		$this->db->order_by('type');

		return $this->db->get()->result();
	}

	public function getAllForms() {
		$this->db->select('id, title');
		$this->db->from('forms');
		$this->db->order_by('title');

		return $this->db->get()->result();
	}

	public function getAllSeasons() {
		$this->db->select('id, season');
		$this->db->from('seasons');
		$this->db->order_by('season');

		return $this->db->get()->result();
	}
}