<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MVehicles_Profiles extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function getVehicles_Profiles($start, $length, $search, $order, $by) {
		$retornar = array();
		if ($search) {
			$busca = $this->getSearchVehicles_Profiles($search, $start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCountSearch($search, $start, $length, $order, $by);
			$retornar['data'] = $busca['datos'];
		}
		else {
			$todo = $this->getAllVehicles_Profiles($start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCount();
			$retornar['data'] = $todo['datos'];
		}

		$retornar['numDataTotal'] = $this->getCount();

		return $retornar;
	}

	public function getVehicle_Profiles($id) {
		$this->db->select('id, profile, DATE_FORMAT(vehicles_profiles.created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(vehicles_profiles.modified, "%d-%m-%Y %H:%i:%s") as modified');
		$this->db->from('vehicles_profiles');
		$this->db->where('id', $id);
		$this->db->limit(1);

		return $this->db->get()->result_array();
	}

	// Funciones auxiliares datatable
	public function getAllVehicles_Profiles($start, $length, $order, $by) {
		$this->db->select('id, profile');
		if ($by == 0) {
			$this->db->order_by('id', $order);
		}
		else {
			$this->db->order_by('profile', $order);
		}
		$this->db->limit($length, $start);
		$query = $this->db->get('vehicles_profiles');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getSearchVehicles_Profiles($search, $start, $length, $order, $by) {
		$this->db->select('id, profile');
		$this->db->like('profile', $search);
		if ($by == 0) {
			$this->db->order_by('profile', $order);
		}
		$this->db->limit($length, $start);
		$query = $this->db->get('vehicles_profiles');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getCount() {
		return $this->db->count_all('vehicles_profiles');
	}

	public function getCountSearch($search, $start, $length, $order, $by) {
		$this->db->select('id');
		$this->db->like('profile', $search);
		$quer = $this->db->get('vehicles_profiles')->num_rows();
		return $quer;
	}
	// fin funciones auxiliares

	//Crud
	public function addVehicles_Profiles($data) {
		if($this->db->insert('vehicles_profiles', $data))
			return true;
		else
			return false;
	}

	public function editVehicles_Profiles($data, $id) {
		$this->db->where('id', $id);
		if($this->db->update('vehicles_profiles', $data))
			return true;
		else
			return false;
	}

	public function deleteVehicles_Profiles($id) {
		$this->db->where('id', $id);
		if($this->db->delete('vehicles_profiles'))
			return true;
		else
			return false;
	}
}