<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MProjects_Forms extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function searchPeopleProjects($rut, $date) {
		$this->db->select('people.id as people_id, projects_people.projects_id, projects_vehicles.vehicles_id, projects.title, projects.description, projects.init, projects.end');
		$this->db->from('people');
		$this->db->join('projects_people', 'projects_people.people_id = people.id');
		$this->db->join('projects', 'projects.id = projects_people.projects_id');
		$this->db->join('projects_vehicles', 'projects_vehicles.projects_id = projects_people.projects_id and projects_vehicles.control_init = 1');
		
		$this->db->where('people.rut', $rut);
		$this->db->where('projects.end >=', $date);

		return $this->db->get()->result();
	}

	public function searchPeopleProjects2($rut, $date) {
		$this->db->select('people.id as people_id, projects_people.projects_id, projects_vehicles.vehicles_id, projects.title, projects.description, projects.init, projects.end');
		$this->db->from('people');
		$this->db->join('projects_people', 'projects_people.people_id = people.id');
		$this->db->join('projects', 'projects.id = projects_people.projects_id');
		$this->db->join('projects_vehicles', 'projects_vehicles.projects_id = projects_people.projects_id and projects_vehicles.control_end = 1');
		
		$this->db->where('people.rut', $rut);
		$this->db->where('projects.end >=', $date);

		return $this->db->get()->result();
	}

	public function getForms($id){
		$this->db->select('forms.id as forms_id, forms.title, forms.description, forms_detail.order, question, placeholder, measures.measure, measures.acronimo, answers_type.id as ans');
		$this->db->from('forms');
		$this->db->join('forms_detail', 'forms_detail.forms_id = forms.id');
		$this->db->join('answers_type', 'answers_type.id = forms_detail.answers_type_id');
		$this->db->join('measures', 'measures.id = forms_detail.measures_id', 'left');
		$this->db->where('forms.id', $id);
		$this->db->order_by('forms_detail.order');
		return $this->db->get()->result();
	}

	public function searchForm(){
		$this->db->select('forms.id as forms_id, forms.title, forms.description');
		$this->db->from('forms');
		return $this->db->get()->result();
	}

	public function searchVehicle($project){
		$this->db->select('projects_vehicles.vehicles_id, vehicles.patent, vehicles_type.type');
		$this->db->from('projects_vehicles');
		$this->db->join('vehicles', 'vehicles.id = projects_vehicles.vehicles_id');
		$this->db->join('vehicles_type', 'vehicles_type.id = vehicles.vehicles_type_id');
		$this->db->where('projects_vehicles.projects_id', $project);

		return $this->db->get()->result();
	}

	public function addProjectsForm($data){
		if($this->db->insert('projects_forms', $data))
			return $this->db->insert_id();
		else
			return false;
	}

	public function addProjectsAnswers($data){
		if($this->db->insert('projects_answers', $data))
			return true;
		else
			return false;
	}

	public function getControlIn($start, $length, $search, $order, $by, $control, $project_id){
		$retornar = array();
		if ($search) {
			$busca = $this->getSearchControl($search, $start, $length, $order, $by, $control, $project_id);
			$retornar['numDataFilter'] = $this->getCountSearch($search, $start, $length, $order, $by, $control, $project_id);
			$retornar['data'] = $busca['datos'];
		}
		else {
			$todo = $this->getAllControl($start, $length, $order, $by, $control, $project_id);
			$retornar['numDataFilter'] = $this->getCount($control);
			$retornar['data'] = $todo['datos'];
		}

		$retornar['numDataTotal'] = $this->getCount($control);

		return $retornar;
	}

	public function getAllControl($start, $length, $order, $by, $control, $project_id) {
		$this->db->select('projects_forms.id, projects_forms.title, people.rut, people.digit, people.name, people.last_name, company, patent, DATE_FORMAT(projects_forms.created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(projects_forms.modified, "%d-%m-%Y %H:%i:%s") as modified');

		$this->db->join('vehicles', 'vehicles.id = projects_forms.vehicle_id');
		$this->db->join('people', 'people.id = projects_forms.people_id');
		$this->db->join('companies', 'companies.id = people.companies_id');
		$this->db->join('projects_answers', 'projects_answers.control_id = projects_forms.id');

		if ($by == 0) {
			$this->db->order_by('title', $order);
		}
		elseif($by == 1) {
			$this->db->order_by('rut', $order);
		}
		elseif ($by == 2) {
		 	$this->db->order_by('patent', $order);
		}
		elseif ($by == 3) {
			$this->db->order_by('created', $order);
		}
		elseif ($by == 4) {
			$this->db->order_by('modified', $order);
		}

		$this->db->where('projects_answers.control', $control);
		$this->db->where('projects_answers.order', 1);
		$this->db->where('projects_forms.projects_id', $project_id);

		$this->db->limit($length, $start);
		$query = $this->db->get('projects_forms');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getSearchControl($search, $start, $length, $order, $by, $control, $project_id) {
		$this->db->select('projects_forms.id, projects_forms.title, people.rut, people.digit, people.name, people.last_name, company, patent, DATE_FORMAT(projects_forms.created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(projects_forms.modified, "%d-%m-%Y %H:%i:%s") as modified');

		$this->db->join('vehicles', 'vehicles.id = projects_forms.vehicle_id');
		$this->db->join('people', 'people.id = projects_forms.people_id');
		$this->db->join('companies', 'companies.id = people.companies_id');
		$this->db->join('projects_answers', 'projects_answers.control_id = projects_forms.id');

		$this->db->like('projects_forms.title', $search);
		$this->db->or_like('rut', $search);
		$this->db->or_like('patent', $search);
		$this->db->or_like('projects_forms.created', $search);
		$this->db->or_like('projects_forms.modified', $search);

		if ($by == 0) {
			$this->db->order_by('title', $order);
		}
		elseif($by == 1) {
			$this->db->order_by('description', $order);
		}
		elseif ($by == 2) {
		 	$this->db->order_by('patent', $order);
		}
		elseif ($by == 3) {
			$this->db->order_by('created', $order);
		}
		elseif ($by == 4) {
			$this->db->order_by('modified', $order);
		}

		$this->db->where('projects_answers.control', $control);
		$this->db->where('projects_answers.order', 1);
		$this->db->where('projects_forms.projects_id', $project_id);

		$this->db->limit($length, $start);
		$query = $this->db->get('projects_forms');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getCount($control) {
		$this->db->select('projects_answers.control_id');

		$this->db->where('projects_answers.control', $control);

		$this->db->distinct('control_id');
		return $this->db->count_all_results('projects_answers');
	}

	public function getCountSearch($search, $start, $length, $order, $by, $control, $projects_id) {
		$this->db->select('projects_forms.projects_id');

		$this->db->join('vehicles', 'vehicles.id = projects_forms.vehicle_id');
		$this->db->join('people', 'people.id = projects_forms.people_id');
		$this->db->join('companies', 'companies.id = people.companies_id');
		$this->db->join('projects_answers', 'projects_answers.control_id = projects_forms.id');

		$this->db->like('projects_forms.title', $search);
		$this->db->or_like('people.rut', $search);
		$this->db->or_like('patent', $search);
		$this->db->or_like('projects_forms.created', $search);
		$this->db->or_like('projects_forms.modified', $search);

		$this->db->where('projects_answers.control', $control);
		$this->db->where('projects_answers.order', 1);
		$this->db->where('projects_forms.projects_id', $project_id);

		$this->db->limit($length, $start);
		$quer = $this->db->get('projects_forms')->num_rows();
		return $quer;
	}

	public function getDetailsProjectsForms($id, $control){
		$this->db->select('projects_forms.projects_id, projects_forms.title, projects_forms.observation, patent, type, forms_detail.order as qorder, forms_detail.question, projects_answers.order as aorder, projects_answers.answer, DATE_FORMAT(projects_forms.created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(projects_forms.modified, "%d-%m-%Y %H:%i:%s") as modified, projects_answers.control');

		$this->db->join('projects', 'projects.id = projects_forms.projects_id');
		$this->db->join('vehicles', 'vehicles.id = projects_forms.vehicle_id');
		$this->db->join('vehicles_type', 'vehicles_type.id = vehicles.vehicles_type_id');
		$this->db->join('projects_answers', 'projects_answers.control_id = projects_forms.id');
		$this->db->join('forms_detail', 'forms_detail.forms_id = projects_forms.forms_id');

		$this->db->where('projects_forms.id', $id);
		$this->db->where('projects_answers.control', $control);
		$this->db->where('projects_answers.order = forms_detail.order');

		return $this->db->get('projects_forms')->result();
	}

	/*-----*/
	public function getControlInB($start, $length, $search, $order, $by, $control){
		$retornar = array();
		if ($search) {
			$busca = $this->getSearchControlB($search, $start, $length, $order, $by, $control);
			$retornar['numDataFilter'] = $this->getCountSearchB($search, $start, $length, $order, $by, $control);
			$retornar['data'] = $busca['datos'];
		}
		else {
			$todo = $this->getAllControlB($start, $length, $order, $by, $control);
			$retornar['numDataFilter'] = $this->getCountB($control);
			$retornar['data'] = $todo['datos'];
		}

		$retornar['numDataTotal'] = $this->getCountB($control);

		return $retornar;
	}

	public function getAllControlB($start, $length, $order, $by, $control) {
		$this->db->select('projects_forms.id, projects_forms.title, people.rut, people.digit, people.name, people.last_name, company, patent, DATE_FORMAT(projects_forms.created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(projects_forms.modified, "%d-%m-%Y %H:%i:%s") as modified');

		$this->db->join('vehicles', 'vehicles.id = projects_forms.vehicle_id');
		$this->db->join('people', 'people.id = projects_forms.people_id');
		$this->db->join('companies', 'companies.id = people.companies_id');
		$this->db->join('projects_answers', 'projects_answers.control_id = projects_forms.id');

		if ($by == 0) {
			$this->db->order_by('title', $order);
		}
		elseif($by == 1) {
			$this->db->order_by('rut', $order);
		}
		elseif ($by == 2) {
		 	$this->db->order_by('patent', $order);
		}
		elseif ($by == 3) {
			$this->db->order_by('created', $order);
		}
		elseif ($by == 4) {
			$this->db->order_by('modified', $order);
		}

		$this->db->where('projects_answers.control', $control);
		$this->db->where('projects_answers.order', 1);
		
		$this->db->limit($length, $start);
		$query = $this->db->get('projects_forms');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getSearchControlB($search, $start, $length, $order, $by, $control) {
		$this->db->select('projects_forms.id, projects_forms.title, people.rut, people.digit, people.name, people.last_name, company, patent, DATE_FORMAT(projects_forms.created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(projects_forms.modified, "%d-%m-%Y %H:%i:%s") as modified');

		$this->db->join('vehicles', 'vehicles.id = projects_forms.vehicle_id');
		$this->db->join('people', 'people.id = projects_forms.people_id');
		$this->db->join('companies', 'companies.id = people.companies_id');
		$this->db->join('projects_answers', 'projects_answers.control_id = projects_forms.id');

		$this->db->group_start();
		$this->db->like('projects_forms.title', $search);
		$this->db->or_like('rut', $search);
		$this->db->or_like('patent', $search);
		$this->db->or_like('projects_forms.created', $search);
		$this->db->or_like('projects_forms.modified', $search);
		$this->db->group_end();

		if ($by == 0) {
			$this->db->order_by('title', $order);
		}
		elseif($by == 1) {
			$this->db->order_by('description', $order);
		}
		elseif ($by == 2) {
		 	$this->db->order_by('patent', $order);
		}
		elseif ($by == 3) {
			$this->db->order_by('created', $order);
		}
		elseif ($by == 4) {
			$this->db->order_by('modified', $order);
		}

		$this->db->where('projects_answers.control', $control);
		$this->db->where('projects_answers.order', 1);
		
		$this->db->limit($length, $start);
		$query = $this->db->get('projects_forms');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getCountB($control) {
		$this->db->select('projects_answers.control_id');

		$this->db->where('projects_answers.control', $control);

		$this->db->distinct('control_id');
		return $this->db->count_all_results('projects_answers');
	}

	public function getCountSearchB($search, $start, $length, $order, $by, $control) {
		$this->db->select('projects_forms.projects_id');

		$this->db->join('vehicles', 'vehicles.id = projects_forms.vehicle_id');
		$this->db->join('people', 'people.id = projects_forms.people_id');
		$this->db->join('companies', 'companies.id = people.companies_id');
		$this->db->join('projects_answers', 'projects_answers.control_id = projects_forms.id');

		$this->db->group_start();
		$this->db->like('projects_forms.title', $search);
		$this->db->or_like('rut', $search);
		$this->db->or_like('patent', $search);
		$this->db->or_like('projects_forms.created', $search);
		$this->db->or_like('projects_forms.modified', $search);
		$this->db->group_end();

		$this->db->where('projects_answers.control', $control);
		$this->db->where('projects_answers.order', 1);
		
		$this->db->limit($length, $start);
		$quer = $this->db->get('projects_forms')->num_rows();
		return $quer;
	}
}