<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MAreas extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function getAreas($start, $length, $search, $order, $by) {
		$retornar = array();
		if ($search) {
			$busca = $this->getSearchAreas($search, $start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCountSearch($search, $start, $length, $order, $by);
			$retornar['data'] = $busca['datos'];
		}
		else {
			$todo = $this->getAllAreas($start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCount();
			$retornar['data'] = $todo['datos'];
		}

		$retornar['numDataTotal'] = $this->getCount();

		return $retornar;
	}

	public function getArea($id) {
		$this->db->select('areas.id, area, areas.zones_id, DATE_FORMAT(areas.created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(areas.modified, "%d-%m-%Y %H:%i:%s") as modified, zone');
		$this->db->join('zones', 'areas.zones_id = zones.id');
		$this->db->from('areas');
		$this->db->where('areas.id', $id);
		$this->db->limit(1);

		return $this->db->get()->result_array();
	}

	// Funciones auxiliares datatable
	public function getAllAreas($start, $length, $order, $by) {
		$this->db->select('areas.id, area, areas.zones_id, DATE_FORMAT(areas.created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(areas.modified, "%d-%m-%Y %H:%i:%s") as modified, zone');
		$this->db->join('zones', 'areas.zones_id = zones.id');
		if ($by == 0) {
			$this->db->order_by('id', $order);
		}
		elseif($by == 1) {
			$this->db->order_by('area', $order);
		}
		else {
			$this->db->order_by('zone', $order);
		}
		$this->db->limit($length, $start);
		$query = $this->db->get('areas');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getSearchAreas($search, $start, $length, $order, $by) {
		$this->db->select('areas.id, area, areas.zones_id, DATE_FORMAT(areas.created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(areas.modified, "%d-%m-%Y %H:%i:%s") as modified, zone');
		$this->db->join('zones', 'areas.zones_id = zones.id');
		$this->db->like('areas.id', $search);
		$this->db->or_like('area', $search);
		$this->db->or_like('zone', $search);
		if ($by == 0) {
			$this->db->order_by('areas.id', $order);
		}
		elseif($by == 1) {
			$this->db->order_by('area', $order);
		}
		else {
			$this->db->order_by('zone', $order);
		}
		$this->db->limit($length, $start);
		$query = $this->db->get('areas');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getCount() {
		return $this->db->count_all('areas');
	}

	public function getCountSearch($search, $start, $length, $order, $by) {
		$this->db->select('areas.id');
		$this->db->join('zones', 'areas.zones_id = zones.id');
		$this->db->like('areas.id', $search);
		$this->db->or_like('area', $search);
		$this->db->or_like('zone', $search);
		$quer = $this->db->get('areas')->num_rows();
		return $quer;
	}
	// fin funciones auxiliares

	//Crud
	public function addAreas($data) {
		if($this->db->insert('areas', $data))
			return true;
		else
			return false;
	}

	public function editAreas($data, $id) {
		$this->db->where('id', $id);
		if($this->db->update('areas', $data))
			return true;
		else
			return false;
	}

	public function deleteAreas($id) {
		$this->db->where('id', $id);
		if($this->db->delete('areas'))
			return true;
		else
			return false;
	}
}