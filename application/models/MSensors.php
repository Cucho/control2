<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MSensors extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function getSensors($start, $length, $search, $order, $by) {
		$retornar = array();
		if ($search) {
			$busca = $this->getSearchSensor($search, $start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCountSearch($search, $start, $length, $order, $by);
			$retornar['data'] = $busca['datos'];
		}
		else {
			$todo = $this->getAllSensors($start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCount();
			$retornar['data'] = $todo['datos'];
		}

		$retornar['numDataTotal'] = $this->getCount();

		return $retornar;
	}

	public function getSensor($id) {
		$this->db->select('sensors.id, code, sensor, description, ip, sensors_type, entry,DATE_FORMAT(sensors.created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(sensors.modified, "%d-%m-%Y %H:%i:%s") as modified, type');
		$this->db->join('sensors_type','sensors_type.id = sensors.sensors_type');
		$this->db->from('sensors');
		$this->db->where('sensors.id', $id);
		$this->db->limit(1);

		return $this->db->get()->result_array();
	}

	// Funciones auxiliares datatable
	public function getAllSensors($start, $length, $order, $by) {
		$this->db->select('sensors.id, code, sensor, description, ip, sensors_type, entry,DATE_FORMAT(sensors.created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(sensors.modified, "%d-%m-%Y %H:%i:%s") as modified, type');
		$this->db->join('sensors_type','sensors_type.id = sensors.sensors_type');
		switch ($by) {
			case 0:
				$this->db->order_by('sensors.id', $order);
				break;
			case 1:
				$this->db->order_by('code', $order);
				break;
			case 2:
				$this->db->order_by('sensor', $order);
				break;
			case 3:
				$this->db->order_by('description', $order);
				break;
			case 4:
				$this->db->order_by('ip', $order);
				break;
			case 5:
				$this->db->order_by('type', $order);
				break;
		}
		$this->db->limit($length, $start);
		$query = $this->db->get('sensors');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getSearchSensor($search, $start, $length, $order, $by) {
		$this->db->select('sensors.id, code, sensor, description, ip, sensors_type, entry,DATE_FORMAT(sensors.created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(sensors.modified, "%d-%m-%Y %H:%i:%s") as modified, type');
		$this->db->like('sensors.id', $search);
		$this->db->or_like('code', $search);
		$this->db->or_like('sensor', $search);
		$this->db->or_like('description', $search);
		$this->db->or_like('ip', $search);
		$this->db->or_like('type', $search);
		switch ($by) {
			case 0:
				$this->db->order_by('sensors.id', $order);
				break;
			case 1:
				$this->db->order_by('code', $order);
				break;
			case 2:
				$this->db->order_by('sensor', $order);
				break;
			case 3:
				$this->db->order_by('description', $order);
				break;
			case 4:
				$this->db->order_by('ip', $order);
				break;
			case 5:
				$this->db->order_by('type', $order);
				break;
		}
		$this->db->join('sensors_type','sensors_type.id = sensors.sensors_type');
		$this->db->limit($length, $start);
		$query = $this->db->get('sensors');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getCount() {
		return $this->db->count_all('sensors');
	}

	public function getCountSearch($search, $start, $length, $order, $by) {
		$this->db->select('sensors.id');
		$this->db->like('sensors.id', $search);
		$this->db->or_like('code', $search);
		$this->db->or_like('sensor', $search);
		$this->db->or_like('description', $search);
		$this->db->or_like('ip', $search);
		$this->db->or_like('type', $search);
		$this->db->join('sensors_type','sensors_type.id = sensors.sensors_type');
		$quer = $this->db->get('sensors')->num_rows();
		return $quer;
	}
	// fin funciones auxiliares

	//Crud
	public function addSensor($data) {
		if($this->db->insert('sensors', $data))
			return true;
		else
			return false;
	}

	public function editSensor($data, $id) {
		$this->db->where('id', $id);
		if($this->db->update('sensors', $data))
			return true;
		else
			return false;
	}

	public function deleteSensor($id) {
		$this->db->where('id', $id);
		if($this->db->delete('sensors'))
			return true;
		else
			return false;
	}
	
	//
	public function getAllSensors_Type() {
		$this->db->select('id, type');
		$this->db->from('sensors_type');
		$this->db->order_by('type');

		return $this->db->get()->result();
	}

}