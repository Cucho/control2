<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MConfigurations extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function getConfiguration($id) {
		$this->db->select('configurations.id, cod_company, company, cod_installation, installation, configurations.address, configurations.email, configurations.phone, people_id, DATE_FORMAT(configurations.created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(configurations.modified, "%d-%m-%Y %H:%i:%s") as modified, name, last_name');
		$this->db->join('people','people.id = configurations.people_id');
		$this->db->from('configurations');
		$this->db->where('configurations.id', $id);
		$this->db->limit(1);

		return $this->db->get()->result_array();
	}

	//Crud
	public function addConfiguration($data) {
		if($this->db->insert('configurations', $data))
			return true;
		else
			return false;
	}

	public function editConfiguration($data, $id) {
		$this->db->where('id', $id);
		if($this->db->update('configurations', $data))
			return true;
		else
			return false;
	}

	public function getAllPeople() {
		$this->db->select('id, rut, name, last_name');
		$this->db->from('people');
		$this->db->order_by('name');

		return $this->db->get()->result();
	}

}