<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MReasons_Error extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function getReasons_Error($start, $length, $search, $order, $by) {
		$retornar = array();
		if ($search) {
			$busca = $this->getSearchReasons_Error($search, $start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCountSearch($search, $start, $length, $order, $by);
			$retornar['data'] = $busca['datos'];
		}
		else {
			$todo = $this->getAllReasons_Error($start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCount();
			$retornar['data'] = $todo['datos'];
		}

		$retornar['numDataTotal'] = $this->getCount();

		return $retornar;
	}

	public function getReason_Error($id) {
		$this->db->select('id, reason, DATE_FORMAT(created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(modified, "%d-%m-%Y %H:%i:%s") as modified');
		$this->db->from('reasons_error');
		$this->db->where('id', $id);
		$this->db->limit(1);

		return $this->db->get()->result_array();
	}

	// Funciones auxiliares datatable
	public function getAllReasons_Error($start, $length, $order, $by) {
		$this->db->select('id, reason, DATE_FORMAT(created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(modified, "%d-%m-%Y %H:%i:%s") as modified');
		if ($by == 0) {
			$this->db->order_by('id', $order);
		}
		else {
			$this->db->order_by('reason', $order);
		}
		$this->db->limit($length, $start);
		$query = $this->db->get('reasons_error');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getSearchReasons_Error($search, $start, $length, $order, $by) {
		$this->db->select('id, reason, DATE_FORMAT(created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(modified, "%d-%m-%Y %H:%i:%s") as modified');
		$this->db->like('id', $search);
		$this->db->or_like('reason', $search);
		if ($by == 0) {
			$this->db->order_by('id', $order);
		}
		else {
			$this->db->order_by('reason', $order);
		}
		$this->db->limit($length, $start);
		$query = $this->db->get('reasons_error');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getCount() {
		return $this->db->count_all('reasons_error');
	}

	public function getCountSearch($search, $start, $length, $order, $by) {
		$this->db->select('id');
		$this->db->like('id', $search);
		$this->db->or_like('reason', $search);
		$quer = $this->db->get('reasons_error')->num_rows();
		return $quer;
	}
	// fin funciones auxiliares

	//Crud
	public function addReason_Error($data) {
		if($this->db->insert('reasons_error', $data))
			return true;
		else
			return false;
	}

	public function editReason_Error($data, $id) {
		$this->db->where('id', $id);
		if($this->db->update('reasons_error', $data))
			return true;
		else
			return false;
	}

	public function delReason_Error($id) {
		$this->db->where('id', $id);
		if($this->db->delete('reasons_error'))
			return true;
		else
			return false;
	}

}