<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MJornadas extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function getJornadas($start, $length, $search, $order, $by) {
		$retornar = array();
		if ($search) {
			$busca = $this->getSearchJornadas($search, $start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCountSearch($search, $start, $length, $order, $by);
			$retornar['data'] = $busca['datos'];
		}
		else {
			$todo = $this->getAllJornadas($start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCount();
			$retornar['data'] = $todo['datos'];
		}

		$retornar['numDataTotal'] = $this->getCount();

		return $retornar;
	}

	public function getJornada($id) {
		$this->db->select('id, jornada, time_init, time_end');
		$this->db->from('jornada');
		$this->db->where('id', $id);
		$this->db->limit(1);

		return $this->db->get()->result_array();
	}

	// Funciones auxiliares datatable
	public function getAllJornadas($start, $length, $order, $by) {
		$this->db->select('id, jornada, time_init, time_end');
		if ($by == 0) {
			$this->db->order_by('id', $order);
		}
		else {
			$this->db->order_by('jornada', $order);
		}
		$this->db->limit($length, $start);
		$query = $this->db->get('jornada');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getSearchJornadas($search, $start, $length, $order, $by) {
		$this->db->select('id, jornada, time_init, time_end');
		$this->db->like('id', $search);
		$this->db->or_like('jornada', $search);
		if ($by == 0) {
			$this->db->order_by('id', $order);
		}
		else {
			$this->db->order_by('jornada', $order);
		}
		$this->db->limit($length, $start);
		$query = $this->db->get('jornada');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getCount() {
		return $this->db->count_all('jornada');
	}

	public function getCountSearch($search, $start, $length, $order, $by) {
		$this->db->select('id');
		$this->db->like('id', $search);
		$this->db->or_like('jornada', $search);
		$quer = $this->db->get('jornada')->num_rows();
		return $quer;
	}
	// fin funciones auxiliares

	//Crud
	public function addJornada($data) {
		if($this->db->insert('jornada', $data))
			return true;
		else
			return false;
	}

	public function editJornada($data, $id) {
		$this->db->where('id', $id);
		if($this->db->update('jornada', $data))
			return true;
		else
			return false;
	}

	public function deleteJornada($id) {
		$this->db->where('id', $id);
		if($this->db->delete('jornada'))
			return true;
		else
			return false;
	}
}