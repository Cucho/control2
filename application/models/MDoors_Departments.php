<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MDoors_Departments extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function getDoors_Departments($start, $length, $search, $order, $by) {
		$retornar = array();
		if ($search) {
			$busca = $this->getSearchDoor_Departments($search, $start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCountSearch($search, $start, $length, $order, $by);
			$retornar['data'] = $busca['datos'];
		}
		else {
			$todo = $this->getAllDoors_Departments($start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCount();
			$retornar['data'] = $todo['datos'];
		}

		$retornar['numDataTotal'] = $this->getCount();

		return $retornar;
	}

	public function getDoor_Department_byDoor($id) {
		$this->db->select('door, department, doors_id, departments_id');
		$this->db->from('doors_departments');
		$this->db->join('doors','doors.id = doors_departments.doors_id');
		$this->db->join('departments','departments.id = doors_departments.department_id');
		$this->db->where('doors_id', $id);

		return $this->db->get()->result_array();
	}

	public function getDoor_Department_byDepartment($id) {
		$this->db->select('door, department, doors_id, departments_id');
		$this->db->from('doors_departments');
		$this->db->join('doors','doors.id = doors_departments.doors_id');
		$this->db->join('departments','departments.id = doors_departments.zones_id');
		$this->db->where('departments_id', $id);

		return $this->db->get()->result_array();
	}

	// Funciones auxiliares datatable
	public function getAllDoors_Departments($start, $length, $order, $by) {
		$this->db->select('door, department, doors_id, departments_id');
		$this->db->join('doors','doors.id = doors_departments.doors_id');
		$this->db->join('departments','departments.id = doors_departments.departments_id');
		if ($by == 0) {
			$this->db->order_by('department', $order);
		}
		if ($by == 1) {
			$this->db->order_by('door', $order);
		}
		
		$this->db->limit($length, $start);
		$query = $this->db->get('doors_departments');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getSearchDoor_Departments($search, $start, $length, $order, $by) {
		$this->db->select('door, department, doors_id, departments_id');
		$this->db->like('door', $search);
		$this->db->or_like('department', $search);
		if ($by == 0) {
			$this->db->order_by('department', $order);
		}
		if ($by == 1) {
			$this->db->order_by('door', $order);
		}
		$this->db->join('doors','doors.id = doors_departments.doors_id');
		$this->db->join('departments','departments.id = doors_departments.departments_id');
		$this->db->limit($length, $start);
		$query = $this->db->get('doors_departments');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getCount() {
		return $this->db->count_all('doors_departments');
	}

	public function getCountSearch($search, $start, $length, $order, $by) {
		$this->db->select('door');
		$this->db->like('department', $search);
		$this->db->or_like('door', $search);
		$this->db->join('doors','doors.id = doors_departments.doors_id');
		$this->db->join('departments','departments.id = doors_departments.departments_id');
		$quer = $this->db->get('doors_departments')->num_rows();
		return $quer;
	}
	// fin funciones auxiliares

	//Crud
	public function addDoor_Department($data) {
		if($this->db->insert('doors_departments', $data))
			return true;
		else
			return false;
	}

	public function deleteDoor_Department($doors_id, $departments_id) {
		$this->db->where('doors_id', $doors_id);
		$this->db->where('departments_id', $departments_id);
		if($this->db->delete('doors_departments'))
			return true;
		else
			return false;
	}
	
	//
	public function getAllDoors() {
		$this->db->select('id, door');
		$this->db->from('doors');
		$this->db->order_by('door');

		return $this->db->get()->result_array();
	}

	public function getAllDepartments() {
		$this->db->select('id, department');
		$this->db->from('departments');
		$this->db->order_by('department');

		return $this->db->get()->result_array();
	}

}