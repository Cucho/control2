<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MNotifications extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function getNotifications($start, $length, $search, $order, $by) {
		$retornar = array();
		if ($search) {
			$busca = $this->getSearchNotifications($search, $start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCountSearch($search, $start, $length, $order, $by);
			$retornar['data'] = $busca['datos'];
		}
		else {
			$todo = $this->getAllNotifications($start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCount();
			$retornar['data'] = $todo['datos'];
		}

		$retornar['numDataTotal'] = $this->getCount();

		return $retornar;
	}

	// Funciones auxiliares datatable
	public function getAllNotifications($start, $length, $order, $by) {
		$this->db->select('notifications.id as id, notifications.notification as notification, doors.door as door,  sensors.sensor as sensor, notifications.entry as entry, DATE_FORMAT(notifications.created, "%d-%m-%Y %H:%i:%s") as created');
		$this->db->join('doors','doors.id = notifications.doors_id');
		$this->db->join('sensors','sensors.id = notifications.sensors_id');
		switch ($by) {
			case 0:
				$this->db->order_by('notifications.id', $order);
				break;
			case 1:
				$this->db->order_by('notifications.notification', $order);
				break;
			case 2:
				$this->db->order_by('door', $order);
				break;
			case 3:
				$this->db->order_by('sensor', $order);
				break;
			case 4:
				$this->db->order_by('entry', $order);
				break;
			case 5:
				$this->db->order_by('notifications.created', $order);
				break;
		}
		$this->db->limit($length, $start);
		$query = $this->db->get('notifications');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getSearchNotifications($search, $start, $length, $order, $by) {
		$this->db->select('notifications.id as id, notifications.notification as notification, doors.door as door,  sensors.sensor as sensor, notifications.entry as entry, DATE_FORMAT(notifications.created, "%d-%m-%Y %H:%i:%s") as created');
		$this->db->join('doors','doors.id = notifications.doors_id');
		$this->db->join('sensors','sensors.id = notifications.sensors_id');

		$this->db->like('notifications.id', $search);
		$this->db->or_like('notifications.notification', $search);
		$this->db->or_like('doors.door', $search);
		$this->db->or_like('sensors.sensor', $search);
		$this->db->or_like('notifications.created', $search);

		switch ($by) {
			case 0:
				$this->db->order_by('notifications.id', $order);
				break;
			case 1:
				$this->db->order_by('notifications.notification', $order);
				break;
			case 2:
				$this->db->order_by('door', $order);
				break;
			case 3:
				$this->db->order_by('sensor', $order);
				break;
			case 4:
				$this->db->order_by('entry', $order);
				break;
			case 5:
				$this->db->order_by('notifications.created', $order);
				break;
		}

		$this->db->limit($length, $start);
		$query = $this->db->get('notifications');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getCount() {
		return $this->db->count_all('notifications');
	}

	public function getCountSearch($search, $start, $length, $order, $by) {
		$this->db->select('notifications.id as id');
		$this->db->join('doors','doors.id = notifications.doors_id');
		$this->db->join('sensors','sensors.id = notifications.sensors_id');

		$this->db->like('notifications.id', $search);
		$this->db->or_like('notifications.notification', $search);
		$this->db->or_like('doors.door', $search);
		$this->db->or_like('sensors.sensor', $search);
		$this->db->or_like('notifications.created', $search);

		$quer = $this->db->get('notifications')->num_rows();
		return $quer;
	}

}