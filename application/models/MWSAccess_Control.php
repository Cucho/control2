<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MWSAccess_Control extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function getPeople_Nfc($nfc_code) {
		$this->db->select('people.id as id, allow_all, people.internal as internal, people.people_profiles_id, companies_id, name, last_name, company, rut, digit, states_id');
		$this->db->from('people');
		$this->db->join('companies','companies.id=people.companies_id');
		$this->db->where('nfc_code', $nfc_code);
		$this->db->limit(1);

		return $this->db->get()->result_array();
	}

	public function getPeople_Rut($rut) {
		$this->db->select('people.id as id, allow_all, people.internal as internal, people.people_profiles_id, companies_id, name, last_name, company, rut, digit, states_id');
		$this->db->from('people');
		$this->db->join('companies','companies.id=people.companies_id');
		$this->db->where('rut', $rut);
		$this->db->limit(1);

		return $this->db->get()->result_array();
	}

	public function getLast_Authorization_Access_People($people_id)
	{
		$this->db->select('id, end_time, TIMESTAMPDIFF(SECOND,NOW(), end_time) AS seconds, vehicles_id');
		$this->db->from('access_people');
		$this->db->where('entry', 0);
		$this->db->where('access_state_id', 2);
		$this->db->where('people_id', $people_id);
		$this->db->order_by('id', 'desc');
		$this->db->limit(1);

		return $this->db->get()->result_array();
	}

	public function getAuthorization_Door_Access_People($access_people_id, $doors_id)
	{
		$this->db->select('access_people_id, doors_id');
		$this->db->from('access_people_route');
		$this->db->where('access_people_id', $access_people_id);
		$this->db->where('doors_id', $doors_id);
		$res = $this->db->get()->result_array();

		if(!empty($res))
			return 1;
		else
			return 0;
	}

	public function getSpecial_Schedule($people_id, $doors_id, $date_time_now)
	{
		$this->db->select('date_end');
		$this->db->from('special_schedule');
		$this->db->where('people_id', $people_id);
		$this->db->where('doors_id', $doors_id);
		$this->db->where('date_init <= "'.$date_time_now.'"');
		$this->db->where('date_end >= "'.$date_time_now.'"');
		$this->db->limit(1);

		$res = $this->db->get()->result_array();
		if(!empty($res[0]['date_end']))
			return $res[0]['date_end'];
		else
			return 0;
	}

	//----------------------------------------------------------------------------------

	public function getLast_Project($people_id, $date_now)
	{
		$this->db->select('id, init, end');
		$this->db->from('projects');
		$this->db->join('projects_people','projects_people.projects_id=projects.id');
		$this->db->where('people_id', $people_id);
		$this->db->where('init <= "'.$date_now.'"');
		$this->db->where('end >= "'.$date_now.'"');
		$this->db->order_by('id', 'desc');
		$this->db->limit(1);

		return $this->db->get()->result_array();
	}

	public function getSchedule_Last_Project($projects_id, $time_now, $dow)
	{
		$this->db->select('projects_id, time_init, time_end');
		$this->db->from('projects_schedules');
		$this->db->where('projects_id', $projects_id);
		//$this->db->where('time_init <= "'.$time_now.'"');
		//$this->db->where('time_end >= "'.$time_now.'"');
		$this->db->where($dow, 1);
		$this->db->limit(1);

		$res = $this->db->get()->result_array();	
		if(!empty($res))
			return $res;
		else
			return 0;
	}

	public function getDoor_Last_Project($projects_id, $doors_id)
	{
		$this->db->select('projects_id, doors_id');
		$this->db->from('projects_route');
		$this->db->where('projects_id', $projects_id);
		$this->db->where('doors_id', $doors_id);
		$this->db->limit(1);

		$res = $this->db->get()->result_array();	
		if(!empty($res))
			return 1;
		else
			return 0;

	}

	public function getDoor_Profiles_Schedules($profiles_people_id, $time_now, $dow)
	{
		$this->db->select('doors_id, time_init, time_end');
		$this->db->from('profiles_doors_schedules');
		$this->db->where('profiles_people_id', $profiles_people_id);
		//$this->db->where('time_init <= "'.$time_now.'"');
		//$this->db->where('time_end >= "'.$time_now.'"');
		$this->db->where($dow, 1);
		$this->db->limit(1);

		return $this->db->get()->result_array();
	}

	//-------------------------------------------------------------
	//generar registros de accesos "internal people succes"

	public function add_internal_people($data)
	{
		if($this->db->insert('internal_people', $data))
			return true;
		else
			return false;
	}


	//generar registros de accesos "internal vehicles succes"

	public function add_internal_vehicles($data)
	{
		if($this->db->insert('internal_vehicles', $data))
			return true;
		else
			return false;
	}
	//-------------------------------------------------------------
	//generar registros de accesos "access_people_intents"

	public function add_access_people_intents($data)
	{
		if($this->db->insert('access_people_intents', $data))
			return true;
		else
			return false;
	}

	//generar registros de accesos "access_vehicles_intents"

	public function add_access_vehicles_intents($data)
	{
		if($this->db->insert('access_vehicles_intents', $data))
			return true;
		else
			return false;
	}
	//-------------------------------------------------------------
	//generar registros de accesos proyectos "projects_intents"

	public function add_projects_intents($data)
	{
		if($this->db->insert('projects_intents', $data))
			return true;
		else
			return false;
	}

	//-------------------------------------------------------------
	//generar registros de "current_internal_state"

	public function add_current_internal_state($data)
	{
		if($this->db->insert('current_internal_state', $data))
			return true;
		else
			return false;
	}
	//verificar si existe un registro al interior de la instalacion
	public function get_current_internal_state($cod, $internal)
	{
		$this->db->select('id');
		$this->db->from('current_internal_state');
		$this->db->where('cod', $cod);
		$this->db->where('internal', $internal);
		$this->db->limit(1);
		$res = $this->db->get()->result_array();

		if(!empty($res[0]['id']))
			return $res[0]['id'];
		else
			return 0;
	}

	public function edit_current_internal_state($data, $id)
	{
		$this->db->where('id', $id);
		if($this->db->update('current_internal_state', $data))
			return true;
		else
			return false;
	}

	public function delete_current_internal_state($id)
	{
		$this->db->where('id', $id);
		if($this->db->delete('current_internal_state'))
			return true;
		else
			return false;
	}

	public function delete_current_internal_state2($people_id)
	{
		$this->db->where('people_id', $people_id);
		if($this->db->delete('current_internal_state'))
			return true;
		else
			return false;
	}

	public function getControl_Out_Visit($cod)
	{
		$this->db->select('control_end');
		$this->db->from('access_people');
		$this->db->where('id', $cod);
		$this->db->limit(1);
		$res = $this->db->get()->result_array();
		if(!empty($res[0]['control_end']))
		{
			if($res[0]['control_end'] == 1)
				return true;
			else
				return false;
		}
		else
			return false;
	}

	public function generateAccessOut($id, $date_time_now, $observation){
		$this->db->set('entry', 1);
		$this->db->set('exit_time', $date_time_now);
		$this->db->set('observation', 'CONCAT(observation,"'.$observation.'")', FALSE);
		$this->db->where('entry', 0);
		$this->db->where('id', $id);
		if ($this->db->update('access_people')) {
			return true;
		}
		else {
			return false;
		}
	}


}