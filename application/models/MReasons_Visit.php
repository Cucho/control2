<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MReasons_Visit extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function getReasons_Visit($start, $length, $search, $order, $by) {
		$retornar = array();
		if ($search) {
			$busca = $this->getSearchReasons_Visit($search, $start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCountSearch($search, $start, $length, $order, $by);
			$retornar['data'] = $busca['datos'];
		}
		else {
			$todo = $this->getAllReasons_Visit($start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCount();
			$retornar['data'] = $todo['datos'];
		}

		$retornar['numDataTotal'] = $this->getCount();

		return $retornar;
	}

	public function getReason_Visit($id) {
		$this->db->select('id, reason, DATE_FORMAT(created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(modified, "%d-%m-%Y %H:%i:%s") as modified');
		$this->db->from('reasons_visit');
		$this->db->where('id', $id);
		$this->db->limit(1);

		return $this->db->get()->result_array();
	}

	// Funciones auxiliares datatable
	public function getAllReasons_Visit($start, $length, $order, $by) {
		$this->db->select('id, reason, DATE_FORMAT(created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(modified, "%d-%m-%Y %H:%i:%s") as modified');
		switch ($by) {
			case 0:
				$this->db->order_by('id', $order);
				break;
			case 1:
				$this->db->order_by('reason', $order);
				break;
		}
		$this->db->limit($length, $start);
		$query = $this->db->get('reasons_visit');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getSearchReasons_Visit($search, $start, $length, $order, $by) {
		$this->db->select('id, reason, DATE_FORMAT(created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(modified, "%d-%m-%Y %H:%i:%s") as modified');
		$this->db->like('id', $search);
		$this->db->or_like('reason', $search);
		switch ($by) {
			case 0:
				$this->db->order_by('id', $order);
				break;
			case 1:
				$this->db->order_by('reason', $order);
				break;
		}
		$this->db->limit($length, $start);
		$query = $this->db->get('reasons_visit');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getCount() {
		return $this->db->count_all('reasons_visit');
	}

	public function getCountSearch($search, $start, $length, $order, $by) {
		$this->db->select('id');
		$this->db->like('id', $search);
		$this->db->or_like('reason', $search);
		$quer = $this->db->get('reasons_visit')->num_rows();
		return $quer;
	}
	// fin funciones auxiliares

	//Crud
	public function addReasons_Visit($data) {
		if($this->db->insert('reasons_visit', $data))
			return true;
		else
			return false;
	}

	public function editReasons_Visit($data, $id) {
		$this->db->where('id', $id);
		if($this->db->update('reasons_visit', $data))
			return true;
		else
			return false;
	}

	public function deleteReasons_Visit($id) {
		$this->db->where('id', $id);
		if($this->db->delete('reasons_visit'))
			return true;
		else
			return false;
	}
}