<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MForms extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function getForms($start, $length, $search, $order, $by) {
		$retornar = array();
		if ($search) {
			$busca = $this->getSearchForms($search, $start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCountSearch($search, $start, $length, $order, $by);
			$retornar['data'] = $busca['datos'];
		}
		else {
			$todo = $this->getAllForms($start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCount();
			$retornar['data'] = $todo['datos'];
		}

		$retornar['numDataTotal'] = $this->getCount();

		return $retornar;
	}

	public function getForm($id) {
		$this->db->select('forms.id, title, DATE_FORMAT(forms.created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(forms.modified, "%d-%m-%Y %H:%i:%s") as modified, description, order, question, placeholder, forms_id, answers_type_id, measures_id, type');
		$this->db->join('forms_detail', 'forms_id = forms.id');
		$this->db->join('answers_type', 'answers_type_id = answers_type.id');
		$this->db->from('forms');
		$this->db->where('forms.id', $id);

		return $this->db->get()->result_array();
	}

	// Funciones auxiliares datatable
	public function getAllForms($start, $length, $order, $by) {
		$this->db->select('id, title, description, DATE_FORMAT(created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(modified, "%d-%m-%Y %H:%i:%s") as modified');
		if ($by == 0) {
			$this->db->order_by('id', $order);
		}
		elseif($by == 1) {
			$this->db->order_by('title', $order);
		}
		elseif($by == 2) {
			$this->db->order_by('description', $order);
		}
		$this->db->limit($length, $start);
		$query = $this->db->get('forms');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getSearchForms($search, $start, $length, $order, $by) {
		$this->db->select('id, title, description, DATE_FORMAT(created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(modified, "%d-%m-%Y %H:%i:%s") as modified');
		$this->db->like('id', $search);
		$this->db->or_like('title', $search);
		$this->db->or_like('description', $search);
		if ($by == 0) {
			$this->db->order_by('id', $order);
		}
		elseif($by == 1) {
			$this->db->order_by('title', $order);
		}
		elseif($by == 2) {
			$this->db->order_by('description', $order);
		}
		$this->db->limit($length, $start);
		$query = $this->db->get('forms');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getCount() {
		return $this->db->count_all('forms');
	}

	public function getCountSearch($search, $start, $length, $order, $by) {
		$this->db->select('id');
		$this->db->like('id', $search);
		$this->db->or_like('title', $search);
		$this->db->or_like('description', $search);
		$quer = $this->db->get('forms')->num_rows();
		return $quer;
	}
	// fin funciones auxiliares

	//Crud
	public function addForm($data) {
		$this->db->insert('forms', $data);
		$id = $this->db->insert_id();
		return $id;
	}

	public function addForms_Detail($data) {
		if($this->db->insert('forms_detail', $data)) {
			return true;
		}
		else {
			return false;
		}

	}

	public function editForm($data, $id) {
		$this->db->where('id', $id);
		if($this->db->update('forms', $data))
			return true;
		else
			return false;
	}

	public function editForms_Detail($order, $id, $questions, $placeholder, $answers_types, $measure) {
		$this->db->set('question', $questions);
		$this->db->set('placeholder', $placeholder);
		$this->db->set('answers_type_id', $answers_types);
		$this->db->set('measures_id', $measure);
		$this->db->where('order', $order);
		$this->db->where('forms_id', $id);
		if($this->db->update('forms_detail'))
			return true;
		else
			return false;
	}

	public function deleteForm($id) {
		$this->deleteForms_Detail($id);
		$this->db->where('id', $id);
		if($this->db->delete('forms'))
			return true;
		else
			return false;
	}

	public function deleteForms_Detail($id) {
		$this->db->select('order');
		$this->db->where('forms_id', $id);
		$result = $this->db->get('forms_detail')->result();
		foreach ($result as $key) {
			$this->db->where('order', $key->order);
			$this->db->where('forms_id', $id);
			$this->db->delete('forms_detail');
		}
	}

	public function getAllAnswers_Type() {
		$this->db->select('id, type,');
		return $this->db->get('answers_type')->result();
	}

	public function getAllMeasures() {
		$this->db->select('id, measure, acronimo');
		return $this->db->get('measures')->result();
	}
}
