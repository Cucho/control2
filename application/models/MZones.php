<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MZones extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function getZones($start, $length, $search, $order, $by) {
		$retornar = array();
		if ($search) {
			$busca = $this->getSearchZones($search, $start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCountSearch($search, $start, $length, $order, $by);
			$retornar['data'] = $busca['datos'];
		}
		else {
			$todo = $this->getAllZones($start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCount();
			$retornar['data'] = $todo['datos'];
		}

		$retornar['numDataTotal'] = $this->getCount();

		return $retornar;
	}

	public function getZone($id) {
		$this->db->select('id, zone, DATE_FORMAT(created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(modified, "%d-%m-%Y %H:%i:%s") as modified');
		$this->db->from('zones');
		$this->db->where('id', $id);
		$this->db->limit(1);

		return $this->db->get()->result_array();
	}

	// Funciones auxiliares datatable
	public function getAllZones($start, $length, $order, $by) {
		$this->db->select('id, zone');
		if ($by == 0) {
			$this->db->order_by('id', $order);
		}
		if ($by == 1) {
			$this->db->order_by('zone', $order);
		}
		$this->db->limit($length, $start);
		$query = $this->db->get('zones');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getSearchZones($search, $start, $length, $order, $by) {
		$this->db->select('id, zone');
		$this->db->like('zone', $search);
		$this->db->or_like('id', $search);
		if ($by == 0) {
			$this->db->order_by('zone', $order);
		}
		$this->db->limit($length, $start);
		$query = $this->db->get('zones');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getCount() {
		return $this->db->count_all('zones');
	}

	public function getCountSearch($search, $start, $length, $order, $by) {
		$this->db->select('id');
		$this->db->like('zone', $search);
		$this->db->or_like('id', $search);
		return $this->db->get('zones')->num_rows();
	}
	// fin funciones auxiliares

	//Crud
	public function addZone($data) {
		if($this->db->insert('zones', $data))
			return true;
		else
			return false;
	}

	public function editZone($data, $id) {
		$this->db->where('id', $id);
		if($this->db->update('zones', $data))
			return true;
		else
			return false;
	}

	public function deleteZone($id) {
		$this->db->where('id', $id);
		if($this->db->delete('zones'))
			return true;
		else
			return false;
	}
}