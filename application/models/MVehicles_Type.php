<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MVehicles_Type extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function getVehicles_Type($start, $length, $search, $order, $by) {
		$retornar = array();
		if ($search) {
			$busca = $this->getSearchVehicles_Type($search, $start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCountSearch($search, $start, $length, $order, $by);
			$retornar['data'] = $busca['datos'];
		}
		else {
			$todo = $this->getAllVehicles_Type($start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCount();
			$retornar['data'] = $todo['datos'];
		}

		$retornar['numDataTotal'] = $this->getCount();

		return $retornar;
	}

	public function getVehicle_Type($id) {
		$this->db->select('id, type, DATE_FORMAT(vehicles_type.created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(vehicles_type.modified, "%d-%m-%Y %H:%i:%s") as modified');
		$this->db->from('vehicles_type');
		$this->db->where('id', $id);
		$this->db->limit(1);

		return $this->db->get()->result_array();
	}

	// Funciones auxiliares datatable
	public function getAllVehicles_Type($start, $length, $order, $by) {
		$this->db->select('id, type');
		if ($by == 0) {
			$this->db->order_by('id', $order);
		}
		else {
			$this->db->order_by('type', $order);
		}
		$this->db->limit($length, $start);
		$query = $this->db->get('vehicles_type');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getSearchVehicles_Type($search, $start, $length, $order, $by) {
		$this->db->select('id, type');
		$this->db->like('type', $search);
		if ($by == 0) {
			$this->db->order_by('type', $order);
		}
		$this->db->limit($length, $start);
		$query = $this->db->get('vehicles_type');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getCount() {
		return $this->db->count_all('vehicles_type');
	}

	public function getCountSearch($search, $start, $length, $order, $by) {
		$this->db->select('id');
		$this->db->like('type', $search);
		$quer = $this->db->get('vehicles_type')->num_rows();
		return $quer;
	}
	// fin funciones auxiliares

	//Crud
	public function addVehicles_Type($data) {
		if($this->db->insert('vehicles_type', $data))
			return true;
		else
			return false;
	}

	public function editVehicles_Type($data, $id) {
		$this->db->where('id', $id);
		if($this->db->update('vehicles_type', $data))
			return true;
		else
			return false;
	}

	public function deleteVehicles_Type($id) {
		$this->db->where('id', $id);
		if($this->db->delete('vehicles_type'))
			return true;
		else
			return false;
	}
}