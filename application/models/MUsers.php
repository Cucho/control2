<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MUsers extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function getUsers($start, $length, $search, $order, $by) {
		$retornar = array();
		if ($search) {
			$busca = $this->getSearchUser($search, $start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCountSearch($search, $start, $length, $order, $by);
			$retornar['data'] = $busca['datos'];
		}
		else {
			$todo = $this->getAllUsers($start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCount();
			$retornar['data'] = $todo['datos'];
		}

		$retornar['numDataTotal'] = $this->getCount();

		return $retornar;
	}

	public function getUser($id) {
		$this->db->select('users.id, users.user, password, users.roles_id, users.people_id, users.users_state_id, DATE_FORMAT(users.created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(users.modified, "%d-%m-%Y %H:%i:%s") as modified, roles.rol, people.name, people.last_name, users_state.state, users.save, users.edit, users.del');
		$this->db->join('roles','roles.id = users.roles_id');
		$this->db->join('people','people.id = users.people_id');
		$this->db->join('users_state','users_state.id = users.users_state_id');
		$this->db->from('users');
		$this->db->where('users.id', $id);
		$this->db->limit(1);

		return $this->db->get()->result_array();
	}

	// Funciones auxiliares datatable
	public function getAllUsers($start, $length, $order, $by) {
		$this->db->select('users.id, users.user, password, users.roles_id, users.people_id, users.users_state_id, DATE_FORMAT(users.created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(users.modified, "%d-%m-%Y %H:%i:%s") as modified, roles.rol, people.name, people.last_name, users_state.state');
		$this->db->join('roles','roles.id = users.roles_id');
		$this->db->join('people','people.id = users.people_id');
		$this->db->join('users_state','users_state.id = users.users_state_id');
		switch ($by) {
			case 0:
				$this->db->order_by('users.id', $order);
				break;
			case 1:
				$this->db->order_by('users.user', $order);
				break;
			case 2:
				$this->db->order_by('users.roles_id', $order);
				break;
			case 3:
				$this->db->order_by('users.people_id', $order);
				break;
			case 4:
				$this->db->order_by('users.users_state_id', $order);
				break;
			case 5:
				$this->db->order_by('roles.rol', $order);
				break;
			case 6:
				$this->db->order_by('people.name', $order);
				break;
			case 7:
				$this->db->order_by('people.last_name', $order);
				break;
			case 8:
				$this->db->order_by('users_state.state', $order);
				break;
		}
		
		$this->db->limit($length, $start);
		$query = $this->db->get('users');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getSearchUser($search, $start, $length, $order, $by) {
		$this->db->select('users.id, users.user, password, users.roles_id, users.people_id, users.users_state_id, DATE_FORMAT(users.created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(users.modified, "%d-%m-%Y %H:%i:%s") as modified, roles.rol, people.name, people.last_name, users_state.state');
		$this->db->like('users.id', $search);
		$this->db->or_like('users.user', $search);
		$this->db->or_like('users.roles_id', $search);
		$this->db->or_like('users.people_id', $search);
		$this->db->or_like('users.users_state_id', $search);
		$this->db->or_like('roles.rol', $search);
		$this->db->or_like('people.name', $search);
		$this->db->or_like('people.last_name', $search);
		$this->db->or_like('users_state.state', $search);
		switch ($by) {
			case 0:
				$this->db->order_by('users.id', $order);
				break;
			case 1:
				$this->db->order_by('users.user', $order);
				break;
			case 2:
				$this->db->order_by('users.roles_id', $order);
				break;
			case 3:
				$this->db->order_by('users.people_id', $order);
				break;
			case 4:
				$this->db->order_by('users.users_state_id', $order);
				break;
			case 5:
				$this->db->order_by('roles.rol', $order);
				break;
			case 6:
				$this->db->order_by('people.name', $order);
				break;
			case 7:
				$this->db->order_by('people.last_name', $order);
				break;
			case 8:
				$this->db->order_by('users_state.state', $order);
				break;
		}
		$this->db->join('roles','roles.id = users.roles_id');
		$this->db->join('people','people.id = users.people_id');
		$this->db->join('users_state','users_state.id = users.users_state_id');
		$this->db->limit($length, $start);
		$query = $this->db->get('users');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getCount() {
		return $this->db->count_all('users');
	}

	public function getCountSearch($search, $start, $length, $order, $by) {
		$this->db->select('users.id');
		$this->db->like('users.id', $search);
		$this->db->or_like('users.user', $search);
		$this->db->or_like('users.roles_id', $search);
		$this->db->or_like('users.people_id', $search);
		$this->db->or_like('users.users_state_id', $search);
		$this->db->or_like('roles.rol', $search);
		$this->db->or_like('people.name', $search);
		$this->db->or_like('people.last_name', $search);
		$this->db->or_like('users_state.state', $search);
		$this->db->join('roles','roles.id = users.roles_id');
		$this->db->join('people','people.id = users.people_id');
		$this->db->join('users_state','users_state.id = users.users_state_id');
		$quer = $this->db->get('users')->num_rows();
		return $quer;
	}
	// fin funciones auxiliares

	//Crud
	public function addUser($data) {
		if($this->db->insert('users', $data))
			return true;
		else
			return false;
	}

	public function editUser($data, $id) {
		$this->db->where('id', $id);
		if($this->db->update('users', $data))
			return true;
		else
			return false;
	}

	public function deleteUser($id) {
		$this->db->where('id', $id);
		if($this->db->delete('users'))
			return true;
		else
			return false;
	}
	//Functions add door zone

	public function getAllPeople() {
		$this->db->select('id, name, last_name');
		$this->db->from('people');
		$this->db->where('internal', 1);
		$this->db->order_by('name');

		return $this->db->get()->result();
	}

	public function getAllUsers_state() {
		$this->db->select('id, state');
		$this->db->from('users_state');
		$this->db->order_by('state');

		return $this->db->get()->result();
	}

	public function getAllRoles() {
		$this->db->select('id, rol');
		$this->db->from('roles');
		$this->db->order_by('rol');

		return $this->db->get()->result();
	}

	//------------------- login

	public function getUserSession($user, $password)
	{
		$this->db->select('users.id as id, users.roles_id, roles.rol, users.people_id, people.name as name, people.last_name as last_name, users.users_state_id, users_state.state, DATE_FORMAT(users.created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(users.modified, "%d-%m-%Y %H:%i:%s") as modified, people.rut, people.digit, people.address, people.email, people.phone, people.allow_all, people.is_visited, people.internal, people.nfc_code, people.people_profiles_id, people.companies_id, people.departments_id, users.save, users.edit, users.del');
		$this->db->from('users');
		$this->db->join('roles', 'roles.id = users.roles_id');
		$this->db->join('people','people.id = users.people_id');
		$this->db->join('users_state','users_state.id = users.users_state_id');
		$this->db->where('users.user', $user);
		$this->db->where('users.password', $password);
		$this->db->limit(1);

		return $this->db->get()->result_array();

	}

	public function getOptions_Rol($roles_id)
	{
		$this->db->select('options.id as id, options.option as option, options.code as code');
		$this->db->from('options');
		$this->db->join('options_roles', 'options_roles.options_id = options.id');
		$this->db->where('options_roles.roles_id', $roles_id);

		return $this->db->get()->result_array();
	}

	public function getMainAccessIp() {
		$this->db->select('ip_host');
		$this->db->from('main_access');
		return $this->db->get()->result();
	}

	public function changePassword($id, $passv, $passn){
		$this->db->set('password', $passn);
		$this->db->where('users.id', $id);
		$this->db->where('users.password', $passv);
		if ($this->db->update('users')) {
			return true;
		}
		else {
			return false;
		}
	}

	public function checkAdmin()
	{
		$date_time_now = date('Y-m-d H:i:s');

		$this->db->select('id');
		$this->db->from('users');
		$this->db->where('user', 'admin');
		$this->db->limit(1);
		$user = $this->db->get()->result_array();

		if(empty($user[0]['id']))
		{
			//check people
			$this->db->select('id');
			$this->db->from('people');
			$this->db->where('rut', '11111111');
			$this->db->limit(1);
			$people = $this->db->get()->result_array();

			if(empty($people[0]['id']))
			{
				$data_people = array(
					'rut' => '11111111',
					'digit' => '1',
					'name' => 'ADMIN',
					'last_name' => 'SYS',
					'address' => '--',
					'email' => '--',
					'phone' => '--',
					'allow_all' => 1,
					'is_visited' => 0,
					'internal' => 1,
					'nfc_code' => 0,
					'people_profiles_id' => 1,
					'companies_id' => 1,
					'departments_id' => 0,
					'states_id' => 1,
					'created' => $date_time_now,
					'modified' => $date_time_now
				);

				$people_id = 0;

				if($this->db->insert('people', $data_people))
					$people_id = $this->db->insert_id();

				$data_user = array(
					'user' => 'admin',
					'password' => md5('admin'),
					'roles_id' => 1,
					'people_id' => $people_id,
					'users_state_id' => 1,
					'save' => 1,
					'edit' => 1,
					'del' => 1,
					'created' => $date_time_now,
					'modified' => $date_time_now
				);

				$this->db->insert('users', $data_user);
			}
			else
			{
				$people_id = $people[0]['id'];
				
				$data_user = array(
					'user' => 'admin',
					'password' => md5('admin'),
					'roles_id' => 1,
					'people_id' => $people_id,
					'users_state_id' => 1,
					'save' => 1,
					'edit' => 1,
					'del' => 1,
					'created' => $date_time_now,
					'modified' => $date_time_now
				);

				$this->db->insert('users', $data_user);
			}
		}
	}

}