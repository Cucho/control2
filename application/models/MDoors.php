<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MDoors extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function getDoors($start, $length, $search, $order, $by) {
		$retornar = array();
		if ($search) {
			$busca = $this->getSearchDoors($search, $start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCountSearch($search, $start, $length, $order, $by);
			$retornar['data'] = $busca['datos'];
		}
		else {
			$todo = $this->getAllDoors($start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCount();
			$retornar['data'] = $todo['datos'];
		}

		$retornar['numDataTotal'] = $this->getCount();

		return $retornar;
	}

	public function getDoor($id) {
		$this->db->select('doors.id, door, description, level, doors_type_id, DATE_FORMAT(doors.created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(doors.modified, "%d-%m-%Y %H:%i:%s") as modified, type');
		$this->db->join('doors_type', 'doors.doors_type_id = doors_type.id');
		$this->db->from('doors');
		$this->db->where('doors.id', $id);
		$this->db->limit(1);

		return $this->db->get()->result_array();
	}

	// Funciones auxiliares datatable
	public function getAllDoors($start, $length, $order, $by) {
		$this->db->select('doors.id, door, description, level, doors_type_id, DATE_FORMAT(doors.created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(doors.modified, "%d-%m-%Y %H:%i:%s") as modified, type');
		$this->db->join('doors_type', 'doors.doors_type_id = doors_type.id');
		if ($by == 0) {
			$this->db->order_by('id', $order);
		}
		elseif($by == 1) {
			$this->db->order_by('door', $order);
		}
		elseif($by == 2) {
			$this->db->order_by('description', $order);
		}
		elseif($by == 3) {
			$this->db->order_by('level', $order);
		}
		else {
			$this->db->order_by('type', $order);
		}
		$this->db->limit($length, $start);
		$query = $this->db->get('doors');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getSearchDoors($search, $start, $length, $order, $by) {
		$this->db->select('doors.id, door, description, level, doors_type_id, DATE_FORMAT(doors.created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(doors.modified, "%d-%m-%Y %H:%i:%s") as modified, type');
		$this->db->join('doors_type', 'doors.doors_type_id = doors_type.id');
		$this->db->like('doors.id', $search);
		$this->db->or_like('door', $search);
		$this->db->or_like('description', $search);
		$this->db->or_like('level', $search);
		$this->db->or_like('type', $search);
		if ($by == 0) {
			$this->db->order_by('id', $order);
		}
		elseif($by == 1) {
			$this->db->order_by('door', $order);
		}
		elseif($by == 2) {
			$this->db->order_by('description', $order);
		}
		elseif($by == 3) {
			$this->db->order_by('level', $order);
		}
		else {
			$this->db->order_by('type', $order);
		}
		$this->db->limit($length, $start);
		$query = $this->db->get('doors');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getCount() {
		return $this->db->count_all('doors');
	}

	public function getCountSearch($search, $start, $length, $order, $by) {
		$this->db->select('doors.id');
		$this->db->join('doors_type', 'doors.doors_type_id = doors_type.id');
		$this->db->like('doors.id', $search);
		$this->db->or_like('door', $search);
		$this->db->or_like('description', $search);
		$this->db->or_like('level', $search);
		$this->db->or_like('type', $search);
		$quer = $this->db->get('doors')->num_rows();
		return $quer;
	}
	// fin funciones auxiliares

	//Crud
	public function addDoors($data) {
		if($this->db->insert('doors', $data))
			return true;
		else
			return false;
	}

	public function editDoors($data, $id) {
		$this->db->where('id', $id);
		if($this->db->update('doors', $data))
			return true;
		else
			return false;
	}

	public function deleteDoors($id) {
		$this->db->where('id', $id);
		if($this->db->delete('doors'))
			return true;
		else
			return false;
	}

	//--------------------------------------------------------------------------

	public function getDoorsMonitoring()
	{
		$this->db->select('doors.id as id, doors.door as door, doors.description as description');
		$this->db->from('doors');
		$this->db->order_by('id');
		return $this->db->get()->result_array();
	}

	public function getSensorsInfo($doors_id)
	{
		$this->db->select('sensors.id as s_id,sensors.sensor as sensor, sensors.ip as ip');
		$this->db->from('sensors');
		$this->db->join('sensors_doors', 'sensors_doors.sensors_id = sensors.id');
		$this->db->where('sensors_doors.doors_id', $doors_id);
		return $this->db->get()->result_array();
	}


}