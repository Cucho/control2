<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MReports extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function getCompanies()
	{
		$this->db->select('id, company');
		$this->db->from('companies');
		$this->db->order_by('company', 'asc');
		return $this->db->get()->result_array();
	}

	public function getPeople()
	{
		$this->db->select('id, rut, digit, name, last_name');
		$this->db->from('people');
		$this->db->order_by('last_name', 'asc');
		return $this->db->get()->result_array();
	}

	public function getVehicles()
	{
		$this->db->select('id, patent');
		$this->db->from('vehicles');
		$this->db->order_by('patent', 'asc');
		return $this->db->get()->result_array();
	}

	public function getPeopleByCompany($companies_id)
	{
		if($companies_id > 0)
		{
			$this->db->select('id, rut, digit, name, last_name');
			$this->db->from('people');
			$this->db->where('companies_id', $companies_id);
			$this->db->order_by('last_name', 'asc');
			return $this->db->get()->result_array();
		}
		else
		{
			$this->db->select('id, rut, digit, name, last_name');
			$this->db->from('people');
			$this->db->order_by('last_name', 'asc');
			return $this->db->get()->result_array();
		}
		
	}

	public function getVehiclesByCompany($companies_id)
	{
		if($companies_id > 0)
		{
			$this->db->select('id, patent');
			$this->db->from('vehicles');
			$this->db->where('companies_id', $companies_id);
			$this->db->order_by('patent', 'asc');
			return $this->db->get()->result_array();
		}
		else
		{
			$this->db->select('id, patent');
			$this->db->from('vehicles');
			$this->db->order_by('patent', 'asc');
			return $this->db->get()->result_array();
		}
	}

	public function getReportDate($date, $type, $id)
	{
		if($type == 1)
		{
			//company
			$internal = $this->getInternal($type, $id);
			
			if($internal == 1 )
			{
				//pending vehicles :?;
				//internos
				$this->db->select('internal_people.id as id, people.rut as rut, people.digit as digit, people.name as name, people.last_name as last_name, people_profiles.profile as profile, doors.door as door, internal_people.entry as entry, DATE_FORMAT(internal_people.created, "%d-%m-%Y %H:%i:%s") as created');
				$this->db->from('internal_people');
				$this->db->join('people', 'people.id = internal_people.people_id');
				$this->db->join('people_profiles', 'people_profiles.id = people.people_profiles_id');
				$this->db->join('doors', 'doors.id = internal_people.doors_id');
				$this->db->where('people.companies_id', $id);
				$this->db->where('DATE(internal_people.created)', $date);
				$this->db->order_by('internal_people.created', 'asc');
				return $this->db->get()->result_array();
			}
			else if($internal == 0)
			{
				//visitas
				$this->db->select('access_people_intents.access_people_id as id, people.rut as rut, people.digit, people.name as name, people.last_name as last_name, people_profiles.profile as profile, doors.door as door, access_people_intents.entry as entry, access_people_intents.success as success,vehicles.patent as patent, access_people_intents.flow as flow, DATE_FORMAT(access_people_intents.created, "%d-%m-%Y %H:%i:%s") as created ');
				$this->db->from('access_people_intents');
				$this->db->join('access_people', 'access_people_intents.access_people_id = access_people.id');
				$this->db->join('vehicles', 'vehicles.id = access_people.vehicles_id', 'left');
				$this->db->join('people', 'people.id = access_people.people_id');
				$this->db->join('people_profiles', 'people_profiles.id = people.people_profiles_id');
				$this->db->join('doors', 'doors.id = access_people_intents.doors_id');
				$this->db->where('people.companies_id', $id);
				$this->db->where('DATE(access_people_intents.created)', $date);
				$this->db->order_by('access_people_intents.created', 'asc');
				return $this->db->get()->result_array();
			}
			else if($internal == 2)
			{
				//contratistas
				$this->db->select('projects_intents.projects_id as id, people.rut as rut, people.digit, people.name as name, people.last_name as last_name, people_profiles.profile as profile, doors.door as door, projects_intents.flow as flow, vehicles.patent as patent ,projects_intents.entry as entry, projects_intents.success as success,DATE_FORMAT(projects_intents.created, "%d-%m-%Y %H:%i:%s") as created ');
				$this->db->from('projects_intents');
				$this->db->join('people', 'people.id = projects_intents.people_id');
				$this->db->join('vehicles', 'vehicles.id = projects_intents.vehicles_id', 'left');
				$this->db->join('people_profiles', 'people_profiles.id = people.people_profiles_id');
				$this->db->join('doors', 'doors.id = projects_intents.doors_id');
				$this->db->where('people.companies_id', $id);
				$this->db->where('DATE(projects_intents.created)', $date);
				$this->db->order_by('projects_intents.created', 'asc');
				return $this->db->get()->result_array();
			}
		}
		else if($type == 2)
		{
			//vehicle
			$internal = $this->getInternal($type, $id);
			
			if($internal == 1 )
			{
				//internos
				$this->db->select('internal_vehicles.id as id, people.rut as rut, people.digit as digit, people.name as name, people.last_name as last_name, people_profiles.profile as profile, doors.door as door,vehicles.patent as patent, internal_vehicles.entry as entry, DATE_FORMAT(internal_vehicles.created, "%d-%m-%Y %H:%i:%s") as created');
				$this->db->from('internal_vehicles');
				$this->db->join('vehicles','vehicles.id = internal_vehicles.vehicles_id');
				$this->db->join('people', 'people.id = internal_vehicles.people_id');
				$this->db->join('people_profiles', 'people_profiles.id = people.people_profiles_id');
				$this->db->join('doors', 'doors.id = internal_vehicles.doors_id');
				$this->db->where('vehicles.id', $id);
				$this->db->where('DATE(internal_vehicles.created)', $date);
				$this->db->order_by('internal_vehicles.created', 'asc');
				return $this->db->get()->result_array();
			}
			else if($internal == 0)
			{
				//visitas
				$this->db->select('access_people_intents.access_people_id as id, people.rut as rut, people.digit, people.name as name, people.last_name as last_name, people_profiles.profile as profile, doors.door as door, access_people_intents.entry as entry, access_people_intents.success as success,vehicles.patent as patent, access_people_intents.flow as flow, DATE_FORMAT(access_people_intents.created, "%d-%m-%Y %H:%i:%s") as created ');
				$this->db->from('access_people_intents');
				$this->db->join('access_people', 'access_people_intents.access_people_id = access_people.id');
				$this->db->join('vehicles', 'vehicles.id = access_people.vehicles_id');
				$this->db->join('people', 'people.id = access_people.people_id');
				$this->db->join('people_profiles', 'people_profiles.id = people.people_profiles_id');
				$this->db->join('doors', 'doors.id = access_people_intents.doors_id');
				$this->db->where('vehicles.id', $id);
				$this->db->where('DATE(access_people_intents.created)', $date);
				$this->db->where('access_people_intents.flow',1);
				$this->db->order_by('access_people_intents.created', 'asc');
				return $this->db->get()->result_array();
				
			}
			else if($internal == 2)
			{
				//contratistas
				$this->db->select('projects_intents.projects_id as id, people.rut as rut, people.digit, people.name as name, people.last_name as last_name, people_profiles.profile as profile, doors.door as door, projects_intents.flow as flow, vehicles.patent as patent ,projects_intents.entry as entry, projects_intents.success as success,DATE_FORMAT(projects_intents.created, "%d-%m-%Y %H:%i:%s") as created ');
				$this->db->from('projects_intents');
				$this->db->join('people', 'people.id = projects_intents.people_id');
				$this->db->join('vehicles', 'vehicles.id = projects_intents.vehicles_id');
				$this->db->join('people_profiles', 'people_profiles.id = people.people_profiles_id');
				$this->db->join('doors', 'doors.id = projects_intents.doors_id');
				$this->db->where('vehicles.id', $id);
				$this->db->where('projects_intents.flow',1);
				$this->db->where('DATE(projects_intents.created)', $date);
				$this->db->order_by('projects_intents.created', 'asc');
				return $this->db->get()->result_array();
				
			}
		}
		else if($type == 0)
		{
			//people
			$internal = $this->getInternal($type, $id);
			
			if($internal == 1 )
			{
				//internos
				$this->db->select('internal_people.id as id, people.rut as rut, people.digit as digit, people.name as name, people.last_name as last_name, people_profiles.profile as profile, doors.door as door, internal_people.entry as entry, DATE_FORMAT(internal_people.created, "%d-%m-%Y %H:%i:%s") as created');
				$this->db->from('internal_people');
				$this->db->join('people', 'people.id = internal_people.people_id');
				$this->db->join('people_profiles', 'people_profiles.id = people.people_profiles_id');
				$this->db->join('doors', 'doors.id = internal_people.doors_id');
				$this->db->where('people.id', $id);
				$this->db->where('DATE(internal_people.created)', $date);
				$this->db->order_by('internal_people.created', 'asc');
				return $this->db->get()->result_array();
			}
			else if($internal == 0)
			{
				//visitas
				$this->db->select('access_people_intents.access_people_id as id, people.rut as rut, people.digit, people.name as name, people.last_name as last_name, people_profiles.profile as profile, doors.door as door, access_people_intents.entry as entry, access_people_intents.success as success, access_people_intents.flow as flow, DATE_FORMAT(access_people_intents.created, "%d-%m-%Y %H:%i:%s") as created ');
				$this->db->from('access_people_intents');
				$this->db->join('access_people', 'access_people_intents.access_people_id = access_people.id');
				$this->db->join('people', 'people.id = access_people.people_id');
				$this->db->join('people_profiles', 'people_profiles.id = people.people_profiles_id');
				$this->db->join('doors', 'doors.id = access_people_intents.doors_id');
				$this->db->where('people.id', $id);
				$this->db->where('DATE(access_people_intents.created)', $date);
				$this->db->where('access_people_intents.flow',0);
				$this->db->order_by('access_people_intents.created', 'asc');
				return $this->db->get()->result_array();
				
			}
			else if($internal == 2)
			{
				//contratistas
				$this->db->select('projects_intents.projects_id as id, people.rut as rut, people.digit, people.name as name, people.last_name as last_name, people_profiles.profile as profile, doors.door as door, projects_intents.flow as flow, projects_intents.entry as entry, projects_intents.success as success,DATE_FORMAT(projects_intents.created, "%d-%m-%Y %H:%i:%s") as created ');
				$this->db->from('projects_intents');
				$this->db->join('people', 'people.id = projects_intents.people_id');
				$this->db->join('people_profiles', 'people_profiles.id = people.people_profiles_id');
				$this->db->join('doors', 'doors.id = projects_intents.doors_id');
				$this->db->where('people.id', $id);
				$this->db->where('projects_intents.flow',0);
				$this->db->where('DATE(projects_intents.created)', $date);
				$this->db->order_by('projects_intents.created', 'asc');
				return $this->db->get()->result_array();
				
			}
		}
	}

	public function getReportInterval($init, $end, $type, $id)
	{
		if($type == 1)
		{
			//company
			$internal = $this->getInternal($type, $id);
			
			if($internal == 1 )
			{
				//pending vehicles :?;
				//internos
				$this->db->select('internal_people.id as id, people.rut as rut, people.digit as digit, people.name as name, people.last_name as last_name, people_profiles.profile as profile, doors.door as door, internal_people.entry as entry, DATE_FORMAT(internal_people.created, "%d-%m-%Y %H:%i:%s") as created');
				$this->db->from('internal_people');
				$this->db->join('people', 'people.id = internal_people.people_id');
				$this->db->join('people_profiles', 'people_profiles.id = people.people_profiles_id');
				$this->db->join('doors', 'doors.id = internal_people.doors_id');
				$this->db->where('people.companies_id', $id);
				$this->db->where('DATE(internal_people.created) >= ', $init);
				$this->db->where('DATE(internal_people.created) <= ', $end);
				$this->db->order_by('internal_people.created', 'asc');
				return $this->db->get()->result_array();
			}
			else if($internal == 0)
			{
				//visitas
				$this->db->select('access_people_intents.access_people_id as id, people.rut as rut, people.digit, people.name as name, people.last_name as last_name, people_profiles.profile as profile, doors.door as door, access_people_intents.entry as entry, access_people_intents.success as success,vehicles.patent as patent, access_people_intents.flow as flow, DATE_FORMAT(access_people_intents.created, "%d-%m-%Y %H:%i:%s") as created ');
				$this->db->from('access_people_intents');
				$this->db->join('access_people', 'access_people_intents.access_people_id = access_people.id');
				$this->db->join('vehicles', 'vehicles.id = access_people.vehicles_id', 'left');
				$this->db->join('people', 'people.id = access_people.people_id');
				$this->db->join('people_profiles', 'people_profiles.id = people.people_profiles_id');
				$this->db->join('doors', 'doors.id = access_people_intents.doors_id');
				$this->db->where('people.companies_id', $id);
				$this->db->where('DATE(access_people_intents.created) >= ', $init);
				$this->db->where('DATE(access_people_intents.created) <= ', $end);
				$this->db->order_by('access_people_intents.created', 'asc');
				return $this->db->get()->result_array();
			}
			else if($internal == 2)
			{
				//contratistas
				$this->db->select('projects_intents.projects_id as id, people.rut as rut, people.digit, people.name as name, people.last_name as last_name, people_profiles.profile as profile, doors.door as door, projects_intents.flow as flow, vehicles.patent as patent ,projects_intents.entry as entry, projects_intents.success as success,DATE_FORMAT(projects_intents.created, "%d-%m-%Y %H:%i:%s") as created ');
				$this->db->from('projects_intents');
				$this->db->join('people', 'people.id = projects_intents.people_id');
				$this->db->join('vehicles', 'vehicles.id = projects_intents.vehicles_id', 'left');
				$this->db->join('people_profiles', 'people_profiles.id = people.people_profiles_id');
				$this->db->join('doors', 'doors.id = projects_intents.doors_id');
				$this->db->where('people.companies_id', $id);
				$this->db->where('DATE(projects_intents.created) >= ', $init);
				$this->db->where('DATE(projects_intents.created) <= ', $end);
				$this->db->order_by('projects_intents.created', 'asc');
				return $this->db->get()->result_array();
			}
		}
		else if($type == 2)
		{
			//vehicle
			$internal = $this->getInternal($type, $id);
			
			if($internal == 1 )
			{
				//internos
				$this->db->select('internal_vehicles.id as id, people.rut as rut, people.digit as digit, people.name as name, people.last_name as last_name, people_profiles.profile as profile, doors.door as door,vehicles.patent as patent, internal_vehicles.entry as entry, DATE_FORMAT(internal_vehicles.created, "%d-%m-%Y %H:%i:%s") as created');
				$this->db->from('internal_vehicles');
				$this->db->join('vehicles','vehicles.id = internal_vehicles.vehicles_id');
				$this->db->join('people', 'people.id = internal_vehicles.people_id');
				$this->db->join('people_profiles', 'people_profiles.id = people.people_profiles_id');
				$this->db->join('doors', 'doors.id = internal_vehicles.doors_id');
				$this->db->where('vehicles.id', $id);
				$this->db->where('DATE(internal_vehicles.created) >= ', $init);
				$this->db->where('DATE(internal_vehicles.created) <= ', $end);
				$this->db->order_by('internal_vehicles.created', 'asc');
				return $this->db->get()->result_array();
			}
			else if($internal == 0)
			{
				//visitas
				$this->db->select('access_people_intents.access_people_id as id, people.rut as rut, people.digit, people.name as name, people.last_name as last_name, people_profiles.profile as profile, doors.door as door, access_people_intents.entry as entry, access_people_intents.success as success,vehicles.patent as patent, access_people_intents.flow as flow, DATE_FORMAT(access_people_intents.created, "%d-%m-%Y %H:%i:%s") as created ');
				$this->db->from('access_people_intents');
				$this->db->join('access_people', 'access_people_intents.access_people_id = access_people.id');
				$this->db->join('vehicles', 'vehicles.id = access_people.vehicles_id');
				$this->db->join('people', 'people.id = access_people.people_id');
				$this->db->join('people_profiles', 'people_profiles.id = people.people_profiles_id');
				$this->db->join('doors', 'doors.id = access_people_intents.doors_id');
				$this->db->where('vehicles.id', $id);
				$this->db->where('DATE(access_people_intents.created) >=', $init);
				$this->db->where('DATE(access_people_intents.created) <=', $end);
				$this->db->where('access_people_intents.flow',1);
				$this->db->order_by('access_people_intents.created', 'asc');
				return $this->db->get()->result_array();
				
			}
			else if($internal == 2)
			{
				//contratistas
				$this->db->select('projects_intents.projects_id as id, people.rut as rut, people.digit, people.name as name, people.last_name as last_name, people_profiles.profile as profile, doors.door as door, projects_intents.flow as flow, vehicles.patent as patent ,projects_intents.entry as entry, projects_intents.success as success,DATE_FORMAT(projects_intents.created, "%d-%m-%Y %H:%i:%s") as created ');
				$this->db->from('projects_intents');
				$this->db->join('people', 'people.id = projects_intents.people_id');
				$this->db->join('vehicles', 'vehicles.id = projects_intents.vehicles_id');
				$this->db->join('people_profiles', 'people_profiles.id = people.people_profiles_id');
				$this->db->join('doors', 'doors.id = projects_intents.doors_id');
				$this->db->where('vehicles.id', $id);
				$this->db->where('projects_intents.flow',1);
				$this->db->where('DATE(projects_intents.created) >= ', $init);
				$this->db->where('DATE(projects_intents.created) <= ', $end);
				$this->db->order_by('projects_intents.created', 'asc');
				return $this->db->get()->result_array();
				
			}
		}
		else if($type == 0)
		{
			//people
			$internal = $this->getInternal($type, $id);
			
			if($internal == 1 )
			{
				//internos
				$this->db->select('internal_people.id as id, people.rut as rut, people.digit as digit, people.name as name, people.last_name as last_name, people_profiles.profile as profile, doors.door as door, internal_people.entry as entry, DATE_FORMAT(internal_people.created, "%d-%m-%Y %H:%i:%s") as created');
				$this->db->from('internal_people');
				$this->db->join('people', 'people.id = internal_people.people_id');
				$this->db->join('people_profiles', 'people_profiles.id = people.people_profiles_id');
				$this->db->join('doors', 'doors.id = internal_people.doors_id');
				$this->db->where('people.id', $id);
				$this->db->where('DATE(internal_people.created) >= ', $init);
				$this->db->where('DATE(internal_people.created) <= ', $end);
				$this->db->order_by('internal_people.created', 'asc');
				return $this->db->get()->result_array();
			}
			else if($internal == 0)
			{
				//visitas
				$this->db->select('access_people_intents.access_people_id as id, people.rut as rut, people.digit, people.name as name, people.last_name as last_name, people_profiles.profile as profile, doors.door as door, access_people_intents.entry as entry, access_people_intents.success as success, access_people_intents.flow as flow, DATE_FORMAT(access_people_intents.created, "%d-%m-%Y %H:%i:%s") as created ');
				$this->db->from('access_people_intents');
				$this->db->join('access_people', 'access_people_intents.access_people_id = access_people.id');
				$this->db->join('people', 'people.id = access_people.people_id');
				$this->db->join('people_profiles', 'people_profiles.id = people.people_profiles_id');
				$this->db->join('doors', 'doors.id = access_people_intents.doors_id');
				$this->db->where('people.id', $id);
				$this->db->where('DATE(access_people_intents.created) >= ', $init);
				$this->db->where('DATE(access_people_intents.created) <= ', $end);
				$this->db->where('access_people_intents.flow',0);
				$this->db->order_by('access_people_intents.created', 'asc');
				return $this->db->get()->result_array();
				
			}
			else if($internal == 2)
			{
				//contratistas
				$this->db->select('projects_intents.projects_id as id, people.rut as rut, people.digit, people.name as name, people.last_name as last_name, people_profiles.profile as profile, doors.door as door, projects_intents.flow as flow, projects_intents.entry as entry, projects_intents.success as success,DATE_FORMAT(projects_intents.created, "%d-%m-%Y %H:%i:%s") as created ');
				$this->db->from('projects_intents');
				$this->db->join('people', 'people.id = projects_intents.people_id');
				$this->db->join('people_profiles', 'people_profiles.id = people.people_profiles_id');
				$this->db->join('doors', 'doors.id = projects_intents.doors_id');
				$this->db->where('people.id', $id);
				$this->db->where('projects_intents.flow',0); 
				$this->db->where('DATE(projects_intents.created) >= ', $init);
				$this->db->where('DATE(projects_intents.created) <= ', $end);
				$this->db->order_by('projects_intents.created', 'asc');
				return $this->db->get()->result_array();
				
			}
		}
	}

	public function getReportYearMonth($year, $month, $type, $id)
	{
		$init = $year.'-'.$month.'-01 00:00:00';
		$end = $year.'-'.$month.'-31 23:59:59';
		
		if($type == 1)
		{
			//company
			$internal = $this->getInternal($type, $id);
			
			if($internal == 1 )
			{
				//pending vehicles :?;
				//internos
				$this->db->select('internal_people.id as id, people.rut as rut, people.digit as digit, people.name as name, people.last_name as last_name, people_profiles.profile as profile, doors.door as door, internal_people.entry as entry, DATE_FORMAT(internal_people.created, "%d-%m-%Y %H:%i:%s") as created');
				$this->db->from('internal_people');
				$this->db->join('people', 'people.id = internal_people.people_id');
				$this->db->join('people_profiles', 'people_profiles.id = people.people_profiles_id');
				$this->db->join('doors', 'doors.id = internal_people.doors_id');
				$this->db->where('people.companies_id', $id);
				$this->db->where('DATE(internal_people.created) >= ', $init);
				$this->db->where('DATE(internal_people.created) <= ', $end);
				$this->db->order_by('internal_people.created', 'asc');
				return $this->db->get()->result_array();
			}
			else if($internal == 0)
			{
				//visitas
				$this->db->select('access_people_intents.access_people_id as id, people.rut as rut, people.digit, people.name as name, people.last_name as last_name, people_profiles.profile as profile, doors.door as door, access_people_intents.entry as entry, access_people_intents.success as success,vehicles.patent as patent, access_people_intents.flow as flow, DATE_FORMAT(access_people_intents.created, "%d-%m-%Y %H:%i:%s") as created ');
				$this->db->from('access_people_intents');
				$this->db->join('access_people', 'access_people_intents.access_people_id = access_people.id');
				$this->db->join('vehicles', 'vehicles.id = access_people.vehicles_id', 'left');
				$this->db->join('people', 'people.id = access_people.people_id');
				$this->db->join('people_profiles', 'people_profiles.id = people.people_profiles_id');
				$this->db->join('doors', 'doors.id = access_people_intents.doors_id');
				$this->db->where('people.companies_id', $id);
				$this->db->where('DATE(access_people_intents.created) >= ', $init);
				$this->db->where('DATE(access_people_intents.created) <= ', $end);
				$this->db->order_by('access_people_intents.created', 'asc');
				return $this->db->get()->result_array();
			}
			else if($internal == 2)
			{
				//contratistas
				$this->db->select('projects_intents.projects_id as id, people.rut as rut, people.digit, people.name as name, people.last_name as last_name, people_profiles.profile as profile, doors.door as door, projects_intents.flow as flow, vehicles.patent as patent ,projects_intents.entry as entry, projects_intents.success as success,DATE_FORMAT(projects_intents.created, "%d-%m-%Y %H:%i:%s") as created ');
				$this->db->from('projects_intents');
				$this->db->join('people', 'people.id = projects_intents.people_id');
				$this->db->join('vehicles', 'vehicles.id = projects_intents.vehicles_id', 'left');
				$this->db->join('people_profiles', 'people_profiles.id = people.people_profiles_id');
				$this->db->join('doors', 'doors.id = projects_intents.doors_id');
				$this->db->where('people.companies_id', $id);
				$this->db->where('DATE(projects_intents.created) >= ', $init);
				$this->db->where('DATE(projects_intents.created) <= ', $end);
				$this->db->order_by('projects_intents.created', 'asc');
				return $this->db->get()->result_array();
			}
		}
		else if($type == 2)
		{
			//vehicle
			$internal = $this->getInternal($type, $id);
			
			if($internal == 1 )
			{
				//internos
				$this->db->select('internal_vehicles.id as id, people.rut as rut, people.digit as digit, people.name as name, people.last_name as last_name, people_profiles.profile as profile, doors.door as door,vehicles.patent as patent, internal_vehicles.entry as entry, DATE_FORMAT(internal_vehicles.created, "%d-%m-%Y %H:%i:%s") as created');
				$this->db->from('internal_vehicles');
				$this->db->join('vehicles','vehicles.id = internal_vehicles.vehicles_id');
				$this->db->join('people', 'people.id = internal_vehicles.people_id');
				$this->db->join('people_profiles', 'people_profiles.id = people.people_profiles_id');
				$this->db->join('doors', 'doors.id = internal_vehicles.doors_id');
				$this->db->where('vehicles.id', $id);
				$this->db->where('DATE(internal_vehicles.created) >= ', $init);
				$this->db->where('DATE(internal_vehicles.created) <= ', $end);
				$this->db->order_by('internal_vehicles.created', 'asc');
				return $this->db->get()->result_array();
			}
			else if($internal == 0)
			{
				//visitas
				$this->db->select('access_people_intents.access_people_id as id, people.rut as rut, people.digit, people.name as name, people.last_name as last_name, people_profiles.profile as profile, doors.door as door, access_people_intents.entry as entry, access_people_intents.success as success,vehicles.patent as patent, access_people_intents.flow as flow, DATE_FORMAT(access_people_intents.created, "%d-%m-%Y %H:%i:%s") as created ');
				$this->db->from('access_people_intents');
				$this->db->join('access_people', 'access_people_intents.access_people_id = access_people.id');
				$this->db->join('vehicles', 'vehicles.id = access_people.vehicles_id');
				$this->db->join('people', 'people.id = access_people.people_id');
				$this->db->join('people_profiles', 'people_profiles.id = people.people_profiles_id');
				$this->db->join('doors', 'doors.id = access_people_intents.doors_id');
				$this->db->where('vehicles.id', $id);
				$this->db->where('DATE(access_people_intents.created) >=', $init);
				$this->db->where('DATE(access_people_intents.created) <=', $end);
				$this->db->where('access_people_intents.flow',1);
				$this->db->order_by('access_people_intents.created', 'asc');
				return $this->db->get()->result_array();
				
			}
			else if($internal == 2)
			{
				//contratistas
				$this->db->select('projects_intents.projects_id as id, people.rut as rut, people.digit, people.name as name, people.last_name as last_name, people_profiles.profile as profile, doors.door as door, projects_intents.flow as flow, vehicles.patent as patent ,projects_intents.entry as entry, projects_intents.success as success,DATE_FORMAT(projects_intents.created, "%d-%m-%Y %H:%i:%s") as created ');
				$this->db->from('projects_intents');
				$this->db->join('people', 'people.id = projects_intents.people_id');
				$this->db->join('vehicles', 'vehicles.id = projects_intents.vehicles_id');
				$this->db->join('people_profiles', 'people_profiles.id = people.people_profiles_id');
				$this->db->join('doors', 'doors.id = projects_intents.doors_id');
				$this->db->where('vehicles.id', $id);
				$this->db->where('projects_intents.flow',1);
				$this->db->where('DATE(projects_intents.created) >= ', $init);
				$this->db->where('DATE(projects_intents.created) <= ', $end);
				$this->db->order_by('projects_intents.created', 'asc');
				return $this->db->get()->result_array();
				
			}
		}
		else if($type == 0)
		{
			//people
			$internal = $this->getInternal($type, $id);
			
			if($internal == 1 )
			{
				//internos
				$this->db->select('internal_people.id as id, people.rut as rut, people.digit as digit, people.name as name, people.last_name as last_name, people_profiles.profile as profile, doors.door as door, internal_people.entry as entry, DATE_FORMAT(internal_people.created, "%d-%m-%Y %H:%i:%s") as created');
				$this->db->from('internal_people');
				$this->db->join('people', 'people.id = internal_people.people_id');
				$this->db->join('people_profiles', 'people_profiles.id = people.people_profiles_id');
				$this->db->join('doors', 'doors.id = internal_people.doors_id');
				$this->db->where('people.id', $id);
				$this->db->where('DATE(internal_people.created) >= ', $init);
				$this->db->where('DATE(internal_people.created) <= ', $end);
				$this->db->order_by('internal_people.created', 'asc');
				return $this->db->get()->result_array();
			}
			else if($internal == 0)
			{
				//visitas
				$this->db->select('access_people_intents.access_people_id as id, people.rut as rut, people.digit, people.name as name, people.last_name as last_name, people_profiles.profile as profile, doors.door as door, access_people_intents.entry as entry, access_people_intents.success as success, access_people_intents.flow as flow, DATE_FORMAT(access_people_intents.created, "%d-%m-%Y %H:%i:%s") as created ');
				$this->db->from('access_people_intents');
				$this->db->join('access_people', 'access_people_intents.access_people_id = access_people.id');
				$this->db->join('people', 'people.id = access_people.people_id');
				$this->db->join('people_profiles', 'people_profiles.id = people.people_profiles_id');
				$this->db->join('doors', 'doors.id = access_people_intents.doors_id');
				$this->db->where('people.id', $id);
				$this->db->where('DATE(access_people_intents.created) >= ', $init);
				$this->db->where('DATE(access_people_intents.created) <= ', $end);
				$this->db->where('access_people_intents.flow',0);
				$this->db->order_by('access_people_intents.created', 'asc');
				return $this->db->get()->result_array();
				
			}
			else if($internal == 2)
			{
				//contratistas
				$this->db->select('projects_intents.projects_id as id, people.rut as rut, people.digit, people.name as name, people.last_name as last_name, people_profiles.profile as profile, doors.door as door, projects_intents.flow as flow, projects_intents.entry as entry, projects_intents.success as success,DATE_FORMAT(projects_intents.created, "%d-%m-%Y %H:%i:%s") as created ');
				$this->db->from('projects_intents');
				$this->db->join('people', 'people.id = projects_intents.people_id');
				$this->db->join('people_profiles', 'people_profiles.id = people.people_profiles_id');
				$this->db->join('doors', 'doors.id = projects_intents.doors_id');
				$this->db->where('people.id', $id);
				$this->db->where('projects_intents.flow',0); 
				$this->db->where('DATE(projects_intents.created) >= ', $init);
				$this->db->where('DATE(projects_intents.created) <= ', $end);
				$this->db->order_by('projects_intents.created', 'asc');
				return $this->db->get()->result_array();
				
			}
		}
	}

	private function getInternal($type, $id)
	{
		if($type == 1)
		{
			//company
			$this->db->select('internal');
			$this->db->from('companies');
			$this->db->where('id', $id);
			$this->db->limit(1);
			$res = $this->db->get()->result_array();
			return $res[0]['internal'];
		}
		else if($type == 2)
		{
			//vehicle
			$this->db->select('internal');
			$this->db->from('vehicles');
			$this->db->where('id', $id);
			$this->db->limit(1);
			$res = $this->db->get()->result_array();
			return $res[0]['internal'];
		}
		else if($type == 0)
		{
			//people
			$this->db->select('internal');
			$this->db->from('people');
			$this->db->where('id', $id);
			$this->db->limit(1);
			$res = $this->db->get()->result_array();
			return $res[0]['internal'];
		}
	}

}