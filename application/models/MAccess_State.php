<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MAccess_State extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function getAccess_State($start, $length, $search, $order, $by) {
		$retornar = array();
		if ($search) {
			$busca = $this->getSearchAccess_State($search, $start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCountSearch($search, $start, $length, $order, $by);
			$retornar['data'] = $busca['datos'];
		}
		else {
			$todo = $this->getAllAccess_State($start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCount();
			$retornar['data'] = $todo['datos'];
		}

		$retornar['numDataTotal'] = $this->getCount();

		return $retornar;
	}

	public function getAccess($id) {
		$this->db->select('id, state, DATE_FORMAT(created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(modified, "%d-%m-%Y %H:%i:%s") as modified');
		$this->db->from('access_state');
		$this->db->where('id', $id);
		$this->db->limit(1);

		return $this->db->get()->result_array();
	}

	// Funciones auxiliares datatable
	public function getAllAccess_State($start, $length, $order, $by) {
		$this->db->select('id, state, DATE_FORMAT(created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(modified, "%d-%m-%Y %H:%i:%s") as modified');
		switch ($by) {
			case 0:
				$this->db->order_by('id', $order);
				break;
			case 1:
				$this->db->order_by('state', $order);
				break;
		}
		$this->db->limit($length, $start);
		$query = $this->db->get('access_state');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getSearchAccess_State($search, $start, $length, $order, $by) {
		$this->db->select('id, state, DATE_FORMAT(created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(modified, "%d-%m-%Y %H:%i:%s") as modified');
		$this->db->like('id', $search);
		$this->db->or_like('state', $search);
		switch ($by) {
			case 0:
				$this->db->order_by('id', $order);
				break;
			case 1:
				$this->db->order_by('state', $order);
				break;
		}
		$this->db->limit($length, $start);
		$query = $this->db->get('access_state');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getCount() {
		return $this->db->count_all('access_state');
	}

	public function getCountSearch($search, $start, $length, $order, $by) {
		$this->db->select('id');
		$this->db->like('id', $search);
		$this->db->or_like('state', $search);
		$quer = $this->db->get('access_state')->num_rows();
		return $quer;
	}
	// fin funciones auxiliares

	//Crud
	public function addAccess_State($data) {
		if($this->db->insert('access_state', $data))
			return true;
		else
			return false;
	}

	public function editAccess_State($data, $id) {
		$this->db->where('id', $id);
		if($this->db->update('access_state', $data))
			return true;
		else
			return false;
	}

	public function deleteAccess_State($id) {
		$this->db->where('id', $id);
		if($this->db->delete('access_state'))
			return true;
		else
			return false;
	}
}