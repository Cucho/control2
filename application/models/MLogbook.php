<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MLogbook extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	
	public function getLogbook($start, $length, $search, $order, $by) {
		$retornar = array();
		if ($search) {
			$busca = $this->getSearchLogbook($search, $start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCountSearch($search, $start, $length, $order, $by);
			$retornar['data'] = $busca['datos'];
		}
		else {
			$todo = $this->getAllLogbook($start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCount();
			$retornar['data'] = $todo['datos'];
		}

		$retornar['numDataTotal'] = $this->getCount();

		return $retornar;
	}

	public function getLogbook_($id) {
		$this->db->select('logbook.id, title, body, edited, people.id as people_id, people.rut as rut, people.digit as digit, people.name as name, people.last_name as last_name, DATE_FORMAT(logbook.created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(logbook.modified, "%d-%m-%Y %H:%i:%s") as modified, type_logbook.type as type, type_logbook_id');
		$this->db->from('logbook');
		$this->db->join('people', 'people.id = logbook.people_id');
		$this->db->join('type_logbook', 'type_logbook.id = logbook.type_logbook_id');
		$this->db->where('logbook.id', $id);
		$this->db->limit(1);

		return $this->db->get()->result_array();
	}

	// Funciones auxiliares datatable
	public function getAllLogbook($start, $length, $order, $by) {
		$this->db->select('logbook.id, title, body, edited, people.id as people_id, people.rut as rut, people.digit as digit, people.name as name, people.last_name as last_name, DATE_FORMAT(logbook.created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(logbook.modified, "%d-%m-%Y %H:%i:%s") as modified, type_logbook.type as type');
		$this->db->join('people', 'people.id = logbook.people_id');
		$this->db->join('type_logbook', 'type_logbook.id = logbook.type_logbook_id');
		if ($by == 0) {
			$this->db->order_by('logbook.id', $order);
		}
		elseif($by == 1) {
			$this->db->order_by('title', $order);
		}
		elseif($by == 2) {
			$this->db->order_by('people.rut', $order);
		}
		elseif($by == 3) {
			$this->db->order_by('type', $order);
		}
		elseif($by == 4) {
			$this->db->order_by('logbook.created', $order);
		}
		$this->db->limit($length, $start);
		$query = $this->db->get('logbook');
		
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getSearchLogbook($search, $start, $length, $order, $by) {
		$this->db->select('logbook.id, title, body, edited, people.id as people_id, people.rut as rut, people.digit as digit, people.name as name, people.last_name as last_name, DATE_FORMAT(logbook.created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(logbook.modified, "%d-%m-%Y %H:%i:%s") as modified, type_logbook.type as type');
		$this->db->join('people', 'people.id = logbook.people_id');
		$this->db->join('type_logbook', 'type_logbook.id = logbook.type_logbook_id');
		$this->db->like('logbook.id', $search);
		$this->db->or_like('title', $search);
		$this->db->or_like('people.rut', $search);
		$this->db->or_like('type', $search);
		if ($by == 0) {
			$this->db->order_by('logbook.id', $order);
		}
		elseif($by == 1) {
			$this->db->order_by('title', $order);
		}
		elseif($by == 2) {
			$this->db->order_by('people.rut', $order);
		}
		elseif($by == 3) {
			$this->db->order_by('type', $order);
		}
		elseif($by == 4) {
			$this->db->order_by('logbook.created', $order);
		}
		$this->db->limit($length, $start);
		$query = $this->db->get('logbook');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getCount() {
		return $this->db->count_all('logbook');
	}

	public function getCountSearch($search, $start, $length, $order, $by) {
		$this->db->select('logbook.id');

		$this->db->join('people', 'people.id = logbook.people_id');
		$this->db->join('type_logbook', 'type_logbook.id = logbook.type_logbook_id');

		$this->db->like('logbook.id', $search);
		$this->db->or_like('title', $search);
		$this->db->or_like('people.rut', $search);
		$this->db->or_like('type', $search);
		$quer = $this->db->get('logbook')->num_rows();
		return $quer;
	}
	// fin funciones auxiliares
	
	//Crud
	public function addLogbook($data) {
		if($this->db->insert('logbook', $data))
			return $this->db->insert_id();
		else
			return 0;
	}

	public function editLogbook($data, $id) {
		$this->db->where('id', $id);
		if($this->db->update('logbook', $data))
			return true;
		else
			return false;
	}

}