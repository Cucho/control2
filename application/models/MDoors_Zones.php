<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MDoors_Zones extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function getDoors_Zones($start, $length, $search, $order, $by) {
		$retornar = array();
		if ($search) {
			$busca = $this->getSearchDoor_Zones($search, $start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCountSearch($search, $start, $length, $order, $by);
			$retornar['data'] = $busca['datos'];
		}
		else {
			$todo = $this->getAllDoors_Zones($start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCount();
			$retornar['data'] = $todo['datos'];
		}

		$retornar['numDataTotal'] = $this->getCount();

		return $retornar;
	}

	public function getDoor_Zone_byDoor($id) {
		$this->db->select('door, zone, doors_id, zones_id');
		$this->db->from('doors_zones');
		$this->db->join('doors','doors.id=doors_zones.doors_id');
		$this->db->join('zones','zones.id=doors_zones.zones_id');
		$this->db->where('doors_id', $id);

		return $this->db->get()->result_array();
	}

	public function getDoor_Zone_byZone($id) {
		$this->db->select('door, zone, doors_id, zones_id');
		$this->db->from('doors_zones');
		$this->db->join('doors','doors.id=doors_zones.doors_id');
		$this->db->join('zones','zones.id=doors_zones.zones_id');
		$this->db->where('zones_id', $id);

		return $this->db->get()->result_array();
	}

	// Funciones auxiliares datatable
	public function getAllDoors_Zones($start, $length, $order, $by) {
		$this->db->select('door, zone, doors_id, zones_id');
		$this->db->join('doors','doors.id=doors_zones.doors_id');
		$this->db->join('zones','zones.id=doors_zones.zones_id');
		if ($by == 0) {
			$this->db->order_by('zone', $order);
		}
		if ($by == 1) {
			$this->db->order_by('door', $order);
		}
		
		$this->db->limit($length, $start);
		$query = $this->db->get('doors_zones');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getSearchDoor_Zones($search, $start, $length, $order, $by) {
		$this->db->select('door,zone, doors_id, zones_id');
		$this->db->like('door', $search);
		$this->db->or_like('zone', $search);
		if ($by == 0) {
			$this->db->order_by('zone', $order);
		}
		if ($by == 1) {
			$this->db->order_by('door', $order);
		}
		$this->db->join('doors','doors.id=doors_zones.doors_id');
		$this->db->join('zones','zones.id=doors_zones.zones_id');
		$this->db->limit($length, $start);
		$query = $this->db->get('doors_zones');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getCount() {
		return $this->db->count_all('doors_zones');
	}

	public function getCountSearch($search, $start, $length, $order, $by) {
		$this->db->select('door');
		$this->db->like('zone', $search);
		$this->db->or_like('door', $search);
		$this->db->join('doors','doors.id=doors_zones.doors_id');
		$this->db->join('zones','zones.id=doors_zones.zones_id');
		$quer = $this->db->get('doors_zones')->num_rows();
		return $quer;
	}
	// fin funciones auxiliares

	//Crud
	public function addDoor_Zone($data) {
		if($this->db->insert('doors_zones', $data))
			return true;
		else
			return false;
	}

	public function deleteDoor_Zone($doors_id, $zones_id) {
		$this->db->where('doors_id', $doors_id);
		$this->db->where('zones_id', $zones_id);
		if($this->db->delete('doors_zones'))
			return true;
		else
			return false;
	}
	//Functions add door zone

	public function getAllDoors()
	{
		$this->db->select('id, door');
		$this->db->from('doors');
		$this->db->order_by('door');

		return $this->db->get()->result_array();
	}

	public function getAllZones()
	{
		$this->db->select('id, zone');
		$this->db->from('zones');
		$this->db->order_by('zone');

		return $this->db->get()->result_array();
	}

}