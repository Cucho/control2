<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MInternal_People extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function getInternal_People($start, $length, $search, $order, $by) {
		$retornar = array();
		if ($search) {
			$busca = $this->getSearchInternal_People($search, $start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCountSearch($search, $start, $length, $order, $by);
			$retornar['data'] = $busca['datos'];
		}
		else {
			$todo = $this->getAllInternal_People($start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCount();
			$retornar['data'] = $todo['datos'];
		}

		$retornar['numDataTotal'] = $this->getCount();

		return $retornar;
	}

	// Funciones auxiliares datatable
	public function getAllInternal_People($start, $length, $order, $by) {
		$this->db->select('internal_people.id as id, people_id, DATE_FORMAT(internal_people.created, "%d-%m-%Y %H:%i:%s") as created, people.rut as rut, people.digit as digit, people.name as name, people.last_name as last_name, doors.door as door, internal_people.entry as entry, internal_people.success as success, internal_people.reasons_error_id as reasons_error_id, people_profiles.profile as profile, departments.department as department, reasons_error.reason as reason');
		$this->db->join('people', 'people.id = internal_people.people_id');
		$this->db->join('doors', 'doors.id = internal_people.doors_id');
		$this->db->join('people_profiles', 'people_profiles.id = people.people_profiles_id');
		$this->db->join('departments', 'departments.id=people.departments_id');
		$this->db->join('reasons_error', 'reasons_error.id = internal_people.reasons_error_id', 'left');

		if ($by == 0) {
			$this->db->order_by('id', $order);
		}
		else if ($by == 1) {
			$this->db->order_by('rut', $order);
		}
		else if($by == 2){
			$this->db->order_by('name', $order);
		}
		else if($by == 3){
			$this->db->order_by('department', $order);
		}
		else if($by == 4){
			$this->db->order_by('profile', $order);
		}
		else if($by == 5){
			$this->db->order_by('door', $order);
		}
		else if($by == 6){
			$this->db->order_by('entry', $order);
		}
		else if($by == 9){
			$this->db->order_by('created', $order);
		}

		$this->db->limit($length, $start);
		$query = $this->db->get('internal_people');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getSearchInternal_People($search, $start, $length, $order, $by) {
		$this->db->select('internal_people.id as id, people_id, DATE_FORMAT(internal_people.created, "%d-%m-%Y %H:%i:%s") as created, people.rut as rut, people.digit as digit, people.name as name, people.last_name as last_name, doors.door as door, internal_people.entry as entry, internal_people.success as success, internal_people.reasons_error_id as reasons_error_id, people_profiles.profile as profile, departments.department as department, reasons_error.reason as reason');
		$this->db->join('people', 'people.id = internal_people.people_id');
		$this->db->join('doors', 'doors.id = internal_people.doors_id');
		$this->db->join('people_profiles', 'people_profiles.id = people.people_profiles_id');
		$this->db->join('departments', 'departments.id=people.departments_id');
		$this->db->join('reasons_error', 'reasons_error.id = internal_people.reasons_error_id', 'left');

		$this->db->like('internal_people_success.id', $search);
		$this->db->or_like('people.rut', $search);
		$this->db->or_like('people.name', $search);
		$this->db->or_like('people.last_name', $search);
		$this->db->or_like('door', $search);

		if ($by == 0) {
			$this->db->order_by('id', $order);
		}
		else if ($by == 1) {
			$this->db->order_by('rut', $order);
		}
		else if($by == 2){
			$this->db->order_by('name', $order);
		}
		else if($by == 3){
			$this->db->order_by('department', $order);
		}
		else if($by == 4){
			$this->db->order_by('profile', $order);
		}
		else if($by == 5){
			$this->db->order_by('door', $order);
		}
		else if($by == 6){
			$this->db->order_by('entry', $order);
		}
		else if($by == 9){
			$this->db->order_by('created', $order);
		}
		
		$this->db->limit($length, $start);
		$query = $this->db->get('internal_people');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getCount() {
		return $this->db->count_all('internal_people');
	}

	public function getCountSearch($search, $start, $length, $order, $by) {
		$this->db->select('internal_people.id as id');
		$this->db->join('people', 'people.id = internal_people.people_id');
		$this->db->join('doors', 'doors.id = internal_people.doors_id');
		$this->db->join('people_profiles', 'people_profiles.id = people.people_profiles_id');
		$this->db->join('departments', 'departments.id=people.departments_id');
		$this->db->join('reasons_error', 'reasons_error.id = internal_people.reasons_error_id', 'left');

		$this->db->like('internal_people.id', $search);
		$this->db->or_like('people.rut', $search);
		$this->db->or_like('people.name', $search);
		$this->db->or_like('people.last_name', $search);
		$this->db->or_like('door', $search);

		$quer = $this->db->get('internal_people')->num_rows();
		return $quer;
	}
	// fin funciones auxiliares

}