<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MOptions extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function getOptions($start, $length, $search, $order, $by) {
		$retornar = array();
		if ($search) {
			$busca = $this->getSearchOptions($search, $start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCountSearch($search, $start, $length, $order, $by);
			$retornar['data'] = $busca['datos'];
		}
		else {
			$todo = $this->getAllOptions($start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCount();
			$retornar['data'] = $todo['datos'];
		}

		$retornar['numDataTotal'] = $this->getCount();

		return $retornar;
	}

	public function getOption($id) {
		$this->db->select('id, option, code, DATE_FORMAT(created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(modified, "%d-%m-%Y %H:%i:%s") as modified');
		$this->db->from('options');
		$this->db->where('id', $id);
		$this->db->limit(1);

		return $this->db->get()->result_array();
	}

	// Funciones auxiliares datatable
	public function getAllOptions($start, $length, $order, $by) {
		$this->db->select('id, option, code, DATE_FORMAT(created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(modified, "%d-%m-%Y %H:%i:%s") as modified');
		if ($by == 0) {
			$this->db->order_by('id', $order);
		}
		elseif ($by == 1) {
			$this->db->order_by('option', $order);
		}
		else {
			$this->db->order_by('code', $order);
		}
		$this->db->limit($length, $start);
		$query = $this->db->get('options');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getSearchOptions($search, $start, $length, $order, $by) {
		$this->db->select('id, option, code, DATE_FORMAT(created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(modified, "%d-%m-%Y %H:%i:%s") as modified');
		$this->db->like('id', $search);
		$this->db->or_like('option', $search);
		$this->db->or_like('code', $search);
		if ($by == 0) {
			$this->db->order_by('id', $order);
		}
		elseif ($by == 1) {
			$this->db->order_by('option', $order);
		}
		else {
			$this->db->order_by('code', $order);
		}
		$this->db->limit($length, $start);
		$query = $this->db->get('options');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getCount() {
		return $this->db->count_all('options');
	}

	public function getCountSearch($search, $start, $length, $order, $by) {
		$this->db->select('id');
		$this->db->like('id', $search);
		$this->db->or_like('option', $search);
		$this->db->or_like('code', $search);
		return $this->db->get('options')->num_rows();
	}
	// fin funciones auxiliares

	//Crud
	public function addOption($data) {
		if($this->db->insert('options', $data))
			return true;
		else
			return false;
	}

	public function editOption($data, $id) {
		$this->db->where('id', $id);
		if($this->db->update('options', $data))
			return true;
		else
			return false;
	}

	public function deleteOption($id) {
		$this->db->where('id', $id);
		if($this->db->delete('options'))
			return true;
		else
			return false;
	}
}