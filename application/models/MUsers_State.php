<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MUsers_State extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function getUsers_State($start, $length, $search, $order, $by) {
		$retornar = array();
		if ($search) {
			$busca = $this->getSearchUsers_State($search, $start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCountSearch($search, $start, $length, $order, $by);
			$retornar['data'] = $busca['datos'];
		}
		else {
			$todo = $this->getAllUsers_State($start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCount();
			$retornar['data'] = $todo['datos'];
		}

		$retornar['numDataTotal'] = $this->getCount();

		return $retornar;
	}

	public function getUser_State($id) {
		$this->db->select('id, state, DATE_FORMAT(created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(modified, "%d-%m-%Y %H:%i:%s") as modified');
		$this->db->from('users_state');
		$this->db->where('id', $id);
		$this->db->limit(1);

		return $this->db->get()->result_array();
	}

	// Funciones auxiliares datatable
	public function getAllUsers_State($start, $length, $order, $by) {
		$this->db->select('id, state, DATE_FORMAT(created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(modified, "%d-%m-%Y %H:%i:%s") as modified');
		if ($by == 0) {
			$this->db->order_by('id', $order);
		}
		else {
			$this->db->order_by('state', $order);
		}
		$this->db->limit($length, $start);
		$query = $this->db->get('users_state');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getSearchUsers_State($search, $start, $length, $order, $by) {
		$this->db->select('id, state, DATE_FORMAT(created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(modified, "%d-%m-%Y %H:%i:%s") as modified');
		$this->db->like('id', $search);
		$this->db->or_like('state', $search);
		if ($by == 0) {
			$this->db->order_by('id', $order);
		}
		else {
			$this->db->order_by('state', $order);
		}
		$this->db->limit($length, $start);
		$query = $this->db->get('users_state');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getCount() {
		return $this->db->count_all('users_state');
	}

	public function getCountSearch($search, $start, $length, $order, $by) {
		$this->db->select('id');
		$this->db->or_like('state', $search);
		$quer = $this->db->get('users_state')->num_rows();
		return $quer;
	}
	// fin funciones auxiliares

	//Crud
	public function addUser_State($data) {
		if($this->db->insert('users_state', $data))
			return true;
		else
			return false;
	}

	public function editUser_State($data, $id) {
		$this->db->where('id', $id);
		if($this->db->update('users_state', $data))
			return true;
		else
			return false;
	}

	public function deleteUser_State($id) {
		$this->db->where('id', $id);
		if($this->db->delete('users_state'))
			return true;
		else
			return false;
	}
}