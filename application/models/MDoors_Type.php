<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MDoors_Type extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function getDoors_Type($start, $length, $search, $order, $by) {
		$retornar = array();
		if ($search) {
			$busca = $this->getSearchDoors_Type($search, $start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCountSearch($search, $start, $length, $order, $by);
			$retornar['data'] = $busca['datos'];
		}
		else {
			$todo = $this->getAllDoors_Type($start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCount();
			$retornar['data'] = $todo['datos'];
		}

		$retornar['numDataTotal'] = $this->getCount();

		return $retornar;
	}

	public function getDoor_Type($id) {
		$this->db->select('id, type, DATE_FORMAT(doors_type.created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(doors_type.modified, "%d-%m-%Y %H:%i:%s") as modified');
		$this->db->from('doors_type');
		$this->db->where('id', $id);
		$this->db->limit(1);

		return $this->db->get()->result_array();
	}

	// Funciones auxiliares datatable
	public function getAllDoors_Type($start, $length, $order, $by) {
		$this->db->select('id, type');
		if ($by == 0) {
			$this->db->order_by('id', $order);
		}
		else {
			$this->db->order_by('type', $order);
		}
		$this->db->limit($length, $start);
		$query = $this->db->get('doors_type');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getSearchDoors_Type($search, $start, $length, $order, $by) {
		$this->db->select('id, type');
		$this->db->like('type', $search);
		if ($by == 0) {
			$this->db->order_by('type', $order);
		}
		$this->db->limit($length, $start);
		$query = $this->db->get('doors_type');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getCount() {
		return $this->db->count_all('doors_type');
	}

	public function getCountSearch($search, $start, $length, $order, $by) {
		$this->db->select('id');
		$this->db->like('type', $search);
		$quer = $this->db->get('doors_type')->num_rows();
		return $quer;
	}
	// fin funciones auxiliares

	//Crud
	public function addDoors_Type($data) {
		if($this->db->insert('doors_type', $data))
			return true;
		else
			return false;
	}

	public function editDoors_Type($data, $id) {
		$this->db->where('id', $id);
		if($this->db->update('doors_type', $data))
			return true;
		else
			return false;
	}

	public function deleteDoors_Type($id) {
		$this->db->where('id', $id);
		if($this->db->delete('doors_type'))
			return true;
		else
			return false;
	}
}