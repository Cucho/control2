<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MDoors_Parents extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function getDoors_Parents($start, $length, $search, $order, $by) {
		$retornar = array();
		if ($search) {
			$busca = $this->getSearchDoor_Parent($search, $start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCountSearch($search, $start, $length, $order, $by);
			$retornar['data'] = $busca['datos'];
		}
		else {
			$todo = $this->getAllDoors_Parents($start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCount();
			$retornar['data'] = $todo['datos'];
		}

		$retornar['numDataTotal'] = $this->getCount();

		return $retornar;
	}

	// Funciones auxiliares datatable
	public function getAllDoors_Parents($start, $length, $order, $by) {
		$this->db->select('doors_id, parent, door');
		$this->db->join('doors','doors.id = doors_parents.doors_id');
		switch ($by) {
			case 0:
				$this->db->order_by('doors_id', $order);
				break;
			case 1:
				$this->db->order_by('parent', $order);
				break;
			case 2:
				$this->db->order_by('doors.id', $order);
				break;
			case 3:
				$this->db->order_by('door', $order);
				break;
		}
		$this->db->limit($length, $start);

		$query = $this->db->get('doors_parents')->result();
		$this->db->select('door');
		$i = 0;
		$object = array();
		foreach ($query as $key) {
			$this->db->where('id', $key->parent);
			$q = $this->db->get('doors')->result();
			$object[] = (object) array(
				'doors_id' 	=> $query[$i]->doors_id,
				'doord'		=> $query[$i]->door,
				'parent_id'	=> $query[$i]->parent,
				'doorp'		=> $q[0]->door
			);
			$i++;
		}
		if (isset($object)) {
			$retornar = array(
				'datos' => ($object)
			);
		}
		else {
			$retornar = array(
				'datos' => ($object)
			);
		}
		
		return $retornar;
	}

	public function getSearchDoor_Parent($search, $start, $length, $order, $by) {
		$this->db->select('doors_id, parent, door');
		$this->db->like('doors_id', $search);
		$this->db->or_like('parent', $search);
		$this->db->or_like('doors.id', $search);
		$this->db->or_like('door', $search);
		switch ($by) {
			case 0:
				$this->db->order_by('doors_id', $order);
				break;
			case 1:
				$this->db->order_by('parent', $order);
				break;
			case 2:
				$this->db->order_by('doors.id', $order);
				break;
			case 3:
				$this->db->order_by('door', $order);
				break;
		}
		$this->db->join('doors','doors.id = doors_parents.doors_id');
		
		$query = $this->db->get('doors_parents')->result();
		$this->db->select('door');
		$i = 0;
		foreach ($query as $key) {
			$this->db->where('id', $key->parent);
			$q = $this->db->get('doors')->result();
			$object[] = (object) array(
				'doors_id' 	=> $query[$i]->doors_id,
				'doord'		=> $query[$i]->door,
				'parent_id'	=> $query[$i]->parent,
				'doorp'		=> $q[0]->door
			);
			$i++;
		}
		$retornar = array(
			'datos' => ($object)
		);
		return $retornar;
	}

	public function getCount() {
		return $this->db->count_all('doors_parents');
	}

	public function getCountSearch($search, $start, $length, $order, $by) {
		$this->db->select('doors_id');
		$this->db->like('doors_id', $search);
		$this->db->or_like('parent', $search);
		$this->db->or_like('doors.id', $search);
		$this->db->or_like('door', $search);
		$this->db->join('doors','doors.id = doors_parents.doors_id');
		$quer = $this->db->get('doors_parents')->num_rows();
		return $quer;
	}
	// fin funciones auxiliares

	//Crud
	public function addDoor_Parent($door, $parent) {
		$mensaje = false;
		for ($i=0; $i < count($parent); $i++) { 
			$this->db->set('doors_id', $door);
			$this->db->set('parent', $parent[$i]);
			if($this->db->insert('doors_parents')) {
				$mensaje = true;
			}
		}
		return $mensaje;		
	}

	public function deleteDoor_Parent($doors_id, $parent) {
		$this->db->where('doors_id', $doors_id);
		$this->db->where('parent', $parent);
		if($this->db->delete('doors_parents'))
			return true;
		else
			return false;
	}
	
	public function getAllDoors() {
		$this->db->select('id as did, door, level');
		$this->db->from('doors');
		$this->db->order_by('door');

		return $this->db->get()->result_array();
	}

	public function getParentsDoor($doors_id)
	{
		$this->db->select('parent');
		$this->db->from('doors_parents');
		$this->db->where('doors_id', $doors_id);

		return $this->db->get()->result_array();
	}

}