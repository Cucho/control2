<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MAreas_Doors extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function getAreas_Doors($start, $length, $search, $order, $by) {
		$retornar = array();
		if ($search) {
			$busca = $this->getSearchArea_Door($search, $start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCountSearch($search, $start, $length, $order, $by);
			$retornar['data'] = $busca['datos'];
		}
		else {
			$todo = $this->getAllAreas_Doors($start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCount();
			$retornar['data'] = $todo['datos'];
		}

		$retornar['numDataTotal'] = $this->getCount();

		return $retornar;
	}

	public function getArea_Door_byArea($id) {
		$this->db->select('area, door, doors_id, areas_id');
		$this->db->from('doors_areas');
		$this->db->join('doors','doors.id = doors_areas.doors_id');
		$this->db->join('areas','areas.id = doors_areas.areas_id');
		$this->db->where('areas_id', $id);

		return $this->db->get()->result_array();
	}

	public function getArea_Door_byDoor($id) {
		$this->db->select('area, door, doors_id, areas_id');
		$this->db->from('doors_areas');
		$this->db->join('doors','doors.id = doors_areas.doors_id');
		$this->db->join('areas','areas.id = doors_areas.areas_id');
		$this->db->where('doors_id', $id);

		return $this->db->get()->result_array();
	}

	// Funciones auxiliares datatable
	public function getAllAreas_Doors($start, $length, $order, $by) {
		$this->db->select('area, door, doors_id, areas_id');
		$this->db->join('doors','doors.id = doors_areas.doors_id');
		$this->db->join('areas','areas.id = doors_areas.areas_id');
		if ($by == 0) {
			$this->db->order_by('area', $order);
		}
		if ($by == 1) {
			$this->db->order_by('door', $order);
		}
		
		$this->db->limit($length, $start);
		$query = $this->db->get('doors_areas');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getSearchArea_Door($search, $start, $length, $order, $by) {
		$this->db->select('area, door, doors_id, areas_id');
		$this->db->like('area', $search);
		$this->db->or_like('door', $search);
		if ($by == 0) {
			$this->db->order_by('area', $order);
		}
		if ($by == 1) {
			$this->db->order_by('door', $order);
		}
		$this->db->join('doors','doors.id = doors_areas.doors_id');
		$this->db->join('areas','areas.id = doors_areas.areas_id');
		$this->db->limit($length, $start);
		$query = $this->db->get('doors_areas');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getCount() {
		return $this->db->count_all('doors_areas');
	}

	public function getCountSearch($search, $start, $length, $order, $by) {
		$this->db->select('area');
		$this->db->like('area', $search);
		$this->db->or_like('door', $search);
		$this->db->join('doors','doors.id = doors_areas.doors_id');
		$this->db->join('areas','areas.id = doors_areas.areas_id');
		$quer = $this->db->get('doors_areas')->num_rows();
		return $quer;
	}
	// fin funciones auxiliares

	//Crud
	public function addArea_Door($data) {
		if($this->db->insert('doors_areas', $data))
			return true;
		else
			return false;
	}

	public function deleteArea_Door($doors_id, $areas_id) {
		$this->db->where('doors_id', $doors_id);
		$this->db->where('areas_id', $areas_id);
		if($this->db->delete('doors_areas'))
			return true;
		else
			return false;
	}
	//Functions add door zone

	public function getAllAreas() {
		$this->db->select('id, area');
		$this->db->from('areas');
		$this->db->order_by('area');

		return $this->db->get()->result_array();
	}

	public function getAllDoors() {
		$this->db->select('id, door');
		$this->db->from('doors');
		$this->db->order_by('door');

		return $this->db->get()->result_array();
	}

}