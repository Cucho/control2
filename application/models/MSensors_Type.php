<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MSensors_Type extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function getSensors_Type($start, $length, $search, $order, $by) {
		$retornar = array();
		if ($search) {
			$busca = $this->getSearchSensors_Type($search, $start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCountSearch($search, $start, $length, $order, $by);
			$retornar['data'] = $busca['datos'];
		}
		else {
			$todo = $this->getAllSensors_Type($start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCount();
			$retornar['data'] = $todo['datos'];
		}

		$retornar['numDataTotal'] = $this->getCount();

		return $retornar;
	}

	public function getSensor_Type($id) {
		$this->db->select('id, type, DATE_FORMAT(created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(modified, "%d-%m-%Y %H:%i:%s") as modified');
		$this->db->from('sensors_type');
		$this->db->where('id', $id);
		$this->db->limit(1);

		return $this->db->get()->result_array();
	}

	// Funciones auxiliares datatable
	public function getAllSensors_Type($start, $length, $order, $by) {
		$this->db->select('id, type, DATE_FORMAT(created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(modified, "%d-%m-%Y %H:%i:%s") as modified');
		switch ($by) {
			case 0:
				$this->db->order_by('id', $order);
				break;
			case 1:
				$this->db->order_by('type', $order);
				break;
		}
		$this->db->limit($length, $start);
		$query = $this->db->get('sensors_type');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getSearchSensors_Type($search, $start, $length, $order, $by) {
		$this->db->select('id, type, DATE_FORMAT(created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(modified, "%d-%m-%Y %H:%i:%s") as modified');
		$this->db->like('id', $search);
		$this->db->or_like('type', $search);
		switch ($by) {
			case 0:
				$this->db->order_by('id', $order);
				break;
			case 1:
				$this->db->order_by('type', $order);
				break;
		}
		$this->db->limit($length, $start);
		$query = $this->db->get('sensors_type');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getCount() {
		return $this->db->count_all('sensors_type');
	}

	public function getCountSearch($search, $start, $length, $order, $by) {
		$this->db->select('id');
		$this->db->like('id', $search);
		$this->db->or_like('type', $search);
		$quer = $this->db->get('sensors_type')->num_rows();
		return $quer;
	}
	// fin funciones auxiliares

	//Crud
	public function addSensor_Type($data) {
		if($this->db->insert('sensors_type', $data))
			return true;
		else
			return false;
	}

	public function editSensor_Type($data, $id) {
		$this->db->where('id', $id);
		if($this->db->update('sensors_type', $data))
			return true;
		else
			return false;
	}

	public function deleteSensor_Type($id) {
		$this->db->where('id', $id);
		if($this->db->delete('sensors_type'))
			return true;
		else
			return false;
	}
}