<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MSpecial_Schedule extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function getSpecial_Schedule($start, $length, $search, $order, $by) {
		$retornar = array();
		if ($search) {
			$busca = $this->getSearchSpecial_Schedule($search, $start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCountSearch($search, $start, $length, $order, $by);
			$retornar['data'] = $busca['datos'];
		}
		else {
			$todo = $this->getAllSpecial_Schedule($start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCount();
			$retornar['data'] = $todo['datos'];
		}

		$retornar['numDataTotal'] = $this->getCount();

		return $retornar;
	}

	public function getSpecial_ScheduleId($id) {
		$this->db->select('special_schedule.id as scid, special_schedule.people_id, special_schedule.doors_id, DATE_FORMAT(date_init, "%d-%m-%Y %H:%i:%s") as date_init, DATE_FORMAT(date_end, "%d-%m-%Y %H:%i:%s") as date_end, special_schedule.reason, people.id as pid, people.rut, people.name, people.last_name, doors.id, doors.door, DATE_FORMAT(special_schedule.created, "%d-%m-%Y %H:%i:%s") as created');
		$this->db->join('people', 'people.id = special_schedule.people_id');
		$this->db->join('doors', 'doors.id = special_schedule.doors_id');
		$this->db->from('special_schedule');
		$this->db->where('special_schedule.id', $id);
		$this->db->limit(1);

		return $this->db->get()->result_array();
	}

	// Funciones auxiliares datatable
	public function getAllSpecial_Schedule($start, $length, $order, $by) {
		$this->db->select('special_schedule.id as scid, special_schedule.people_id, special_schedule.doors_id, DATE_FORMAT(date_init, "%d-%m-%Y %H:%i:%s") as date_init, DATE_FORMAT(date_end, "%d-%m-%Y %H:%i:%s") as date_end, special_schedule.reason, people.id as pid, people.rut, people.digit, people.name, people.last_name, doors.id, doors.door, DATE_FORMAT(special_schedule.created, "%d-%m-%Y %H:%i:%s") as created');
		$this->db->join('people', 'people.id = special_schedule.people_id');
		$this->db->join('doors', 'doors.id = special_schedule.doors_id');
		switch ($by) {
			case 0:
				$this->db->order_by('special_schedule.id', $order);
				break;
			case 1:
				$this->db->order_by('special_schedule.people_id', $order);
				break;
			case 2:
				$this->db->order_by('special_schedule.doors_id', $order);
				break;
			case 3:
				$this->db->order_by('date_init', $order);
				break;
			case 4:
				$this->db->order_by('date_end', $order);
				break;
			case 5:
				$this->db->order_by('people.rut', $order);
				break;
			case 6:
				$this->db->order_by('doors.id', $order);
				break;
		}
		$this->db->limit($length, $start);
		$query = $this->db->get('special_schedule');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getSearchSpecial_Schedule($search, $start, $length, $order, $by) {
		$this->db->select('special_schedule.id, special_schedule.people_id, special_schedule.doors_id, DATE_FORMAT(date_init, "%d-%m-%Y %H:%i:%s") as date_init, DATE_FORMAT(date_end, "%d-%m-%Y %H:%i:%s") as date_end,  special_schedule.reason, people.id, people.rut, people.name, people.last_name, doors.id, doors.door, DATE_FORMAT(special_schedule.created, "%d-%m-%Y %H:%i:%s") as created');
		$this->db->join('people', 'people.id = special_schedule.people_id');
		$this->db->join('doors', 'doors.id = special_schedule.doors_id');
		$this->db->like('special_schedule.id', $search);
		$this->db->or_like('special_schedule.people_id', $search);
		$this->db->or_like('special_schedule.doors_id', $search);
		$this->db->or_like('special_schedule.date_init', $search);
		$this->db->or_like('special_schedule.date_end', $search);
		$this->db->or_like('people.id', $search);
		$this->db->or_like('people.rut', $search);
		$this->db->or_like('people.name', $search);
		$this->db->or_like('people.last_name', $search);
		$this->db->or_like('doors.id', $search);
		$this->db->or_like('doors.door', $search);
		switch ($by) {
			case 0:
				$this->db->order_by('special_schedule.id', $order);
				break;
			case 1:
				$this->db->order_by('special_schedule.people_id', $order);
				break;
			case 2:
				$this->db->order_by('special_schedule.doors_id', $order);
				break;
			case 3:
				$this->db->order_by('date_init', $order);
				break;
			case 4:
				$this->db->order_by('date_end', $order);
				break;
			case 5:
				$this->db->order_by('people.rut', $order);
				break;
			case 6:
				$this->db->order_by('doors.id', $order);
				break;
		}
		$this->db->limit($length, $start);
		$query = $this->db->get('special_schedule');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getCount() {
		return $this->db->count_all('special_schedule');
	}

	public function getCountSearch($search, $start, $length, $order, $by) {
		$this->db->select('special_schedule.id');
		$this->db->join('people', 'people.id = special_schedule.people_id');
		$this->db->join('doors', 'doors.id = special_schedule.doors_id');
		$this->db->like('special_schedule.id', $search);
		$this->db->or_like('special_schedule.people_id', $search);
		$this->db->or_like('special_schedule.doors_id', $search);
		$this->db->or_like('special_schedule.date_init', $search);
		$this->db->or_like('special_schedule.date_end', $search);
		$this->db->or_like('people.id', $search);
		$this->db->or_like('people.rut', $search);
		$this->db->or_like('people.name', $search);
		$this->db->or_like('people.last_name', $search);
		$this->db->or_like('doors.id', $search);
		$this->db->or_like('doors.door', $search);
		$quer = $this->db->get('special_schedule')->num_rows();
		return $quer;
	}
	// fin funciones auxiliares

	//Crud
	public function addSpecial_Schedule($data) {
		if($this->db->insert('special_schedule', $data))
			return true;
		else
			return false;
	}

	public function editSpecial_Schedule($data, $id) {
		$this->db->where('id', $id);
		if($this->db->update('special_schedule', $data))
			return true;
		else
			return false;
	}

	public function deleteSpecial_Schedule($id) {
		$this->db->where('id', $id);
		if($this->db->delete('special_schedule'))
			return true;
		else
			return false;
	}

	public function getAllPeople() {
		$this->db->select('id, rut, digit, name, last_name');
		$this->db->from('people');
		$this->db->where('internal',1);
		$this->db->order_by('name');

		return $this->db->get()->result();
	}

	public function getAllDoors() {
		$this->db->select('id, door');
		$this->db->from('doors');
		$this->db->order_by('door');

		return $this->db->get()->result();
	}
}