<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MDepartments extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function getDepartments($start, $length, $search, $order, $by) {
		$retornar = array();
		if ($search) {
			$busca = $this->getSearchDepartments($search, $start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCountSearch($search, $start, $length, $order, $by);
			$retornar['data'] = $busca['datos'];
		}
		else {
			$todo = $this->getAllDepartments($start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCount();
			$retornar['data'] = $todo['datos'];
		}

		$retornar['numDataTotal'] = $this->getCount();

		return $retornar;
	}

	public function getDepartment($id) {
		$this->db->select('departments.id, department, in_charge, areas_id, DATE_FORMAT(departments.created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(departments.modified, "%d-%m-%Y %H:%i:%s") as modified, area, people.id as pid, people.name, people.last_name, profile');
		$this->db->join('areas', 'departments.areas_id = areas.id');
		$this->db->join('people', 'people.id = in_charge');
		$this->db->join('people_profiles', 'people_profiles.id = people.people_profiles_id');
		$this->db->from('departments');
		$this->db->where('departments.id', $id);
		$this->db->limit(1);

		return $this->db->get()->result();
	}

	// Funciones auxiliares datatable
	public function getAllDepartments($start, $length, $order, $by) {
		$this->db->select('departments.id, department, in_charge, areas_id, DATE_FORMAT(departments.created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(departments.modified, "%d-%m-%Y %H:%i:%s") as modified, area, people.id as pid, people.name, people.last_name, profile');
		$this->db->join('areas', 'departments.areas_id = areas.id');
		$this->db->join('people', 'people.id = in_charge');
		$this->db->join('people_profiles', 'people_profiles.id = people.people_profiles_id');
		if ($by == 0) {
			$this->db->order_by('departments.id', $order);
		}
		elseif($by == 1) {
			$this->db->order_by('department', $order);
		}
		else {
			$this->db->order_by('area', $order);
		}
		$this->db->limit($length, $start);
		$query = $this->db->get('departments');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getSearchDepartments($search, $start, $length, $order, $by) {
		$this->db->select('departments.id, department, in_charge, areas_id, DATE_FORMAT(departments.created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(departments.modified, "%d-%m-%Y %H:%i:%s") as modified, area, people.id as pid, people.name, people.last_name, profile');
		$this->db->join('areas', 'departments.areas_id = areas.id');
		$this->db->join('people', 'people.id = in_charge');
		$this->db->join('people_profiles', 'people_profiles.id = people.people_profiles_id');
		$this->db->like('departments.id', $search);
		$this->db->or_like('department', $search);
		$this->db->or_like('area', $search);
		if ($by == 0) {
			$this->db->order_by('departments.id', $order);
		}
		elseif($by == 1) {
			$this->db->order_by('department', $order);
		}
		else {
			$this->db->order_by('area', $order);
		}
		$this->db->limit($length, $start);
		$query = $this->db->get('departments');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getCount() {
		return $this->db->count_all('departments');
	}

	public function getCountSearch($search, $start, $length, $order, $by) {
		$this->db->select('departments.id');
		$this->db->join('areas', 'departments.areas_id = areas.id');
		$this->db->join('people', 'people.id = in_charge');
		$this->db->join('people_profiles', 'people_profiles.id = people.people_profiles_id');
		$this->db->like('departments.id', $search);
		$this->db->or_like('department', $search);
		$this->db->or_like('area', $search);
		$quer = $this->db->get('departments')->num_rows();
		return $quer;
	}
	// fin funciones auxiliares

	//Crud
	public function addDepartments($data) {
		if($this->db->insert('departments', $data))
			return true;
		else
			return false;
	}

	public function editDepartments($data, $id) {
		$this->db->where('id', $id);
		if($this->db->update('departments', $data))
			return true;
		else
			return false;
	}

	public function deleteDepartments($id) {
		$this->db->where('id', $id);
		if($this->db->delete('departments'))
			return true;
		else
			return false;
	}

	public function getPeople() {
		$this->db->select('people.id, name, last_name, profile');
		$this->db->join('people_profiles', 'people_profiles.id = people.people_profiles_id');
		$this->db->where('internal', 1);
		return $this->db->get('people')->result();
	}

	public function getAreas() {
		$this->db->select('id, area');
		return $this->db->get('areas')->result();
	}
}
