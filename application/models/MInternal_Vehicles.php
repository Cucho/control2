<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MInternal_Vehicles extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function getInternal_Vehicles($start, $length, $search, $order, $by) {
		$retornar = array();
		if ($search) {
			$busca = $this->getSearchInternal_Vehicles($search, $start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCountSearch($search, $start, $length, $order, $by);
			$retornar['data'] = $busca['datos'];
		}
		else {
			$todo = $this->getAllInternal_Vehicles($start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCount();
			$retornar['data'] = $todo['datos'];
		}

		$retornar['numDataTotal'] = $this->getCount();

		return $retornar;
	}

	// Funciones auxiliares datatable
	public function getAllInternal_Vehicles($start, $length, $order, $by) {
		$this->db->select('internal_vehicles.id as id, internal_vehicles.people_id as people_id, DATE_FORMAT(internal_vehicles.created, "%d-%m-%Y %H:%i:%s") as created, people.rut as rut, people.digit as digit, people.name as name, people.last_name as last_name, doors.door as door, internal_vehicles.entry as entry, internal_vehicles.success as success, internal_vehicles.reasons_error_id as reasons_error_id, people_profiles.profile as profile, departments.department as department, reasons_error.reason as reason, vehicles.patent as patent');
		$this->db->join('people', 'people.id = internal_vehicles.people_id');
		$this->db->join('doors', 'doors.id = internal_vehicles.doors_id');
		$this->db->join('people_profiles', 'people_profiles.id = people.people_profiles_id');
		$this->db->join('departments', 'departments.id=people.departments_id');
		$this->db->join('reasons_error', 'reasons_error.id = internal_vehicles.reasons_error_id', 'left');
		$this->db->join('vehicles', 'vehicles.id = internal_vehicles.vehicles_id');

		if ($by == 0) {
			$this->db->order_by('internal_vehicles.id', $order);
		}
		else if ($by == 1) {
			$this->db->order_by('rut', $order);
		}
		else if($by == 2){
			$this->db->order_by('name', $order);
		}
		else if($by == 3){
			$this->db->order_by('patent', $order);
		}
		else if($by == 4){
			$this->db->order_by('department', $order);
		}
		else if($by == 5){
			$this->db->order_by('profile', $order);
		}
		else if($by == 6){
			$this->db->order_by('door', $order);
		}
		else if($by == 10){
			$this->db->order_by('internal_vehicles.created', $order);
		}

		$this->db->limit($length, $start);
		$query = $this->db->get('internal_vehicles');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getSearchInternal_Vehicles($search, $start, $length, $order, $by) {
		$this->db->select('internal_vehicles.id as id, internal_vehicles.people_id as people_id, DATE_FORMAT(internal_vehicles.created, "%d-%m-%Y %H:%i:%s") as created, people.rut as rut, people.digit as digit, people.name as name, people.last_name as last_name, doors.door as door, internal_vehicles.entry as entry, internal_vehicles.success as success, internal_vehicles.reasons_error_id as reasons_error_id, people_profiles.profile as profile, departments.department as department, reasons_error.reason as reason, vehicles.patent as patent');
		$this->db->join('people', 'people.id = internal_vehicles.people_id');
		$this->db->join('doors', 'doors.id = internal_vehicles.doors_id');
		$this->db->join('people_profiles', 'people_profiles.id = people.people_profiles_id');
		$this->db->join('departments', 'departments.id=people.departments_id');
		$this->db->join('reasons_error', 'reasons_error.id = internal_vehicles.reasons_error_id', 'left');
		$this->db->join('vehicles', 'vehicles.id = internal_vehicles.vehicles_id');

		$this->db->like('people.rut', $search);
		$this->db->or_like('people.name', $search);
		$this->db->or_like('internal_vehicles.created', $search);
		$this->db->or_like('reason', $search);

		if ($by == 0) {
			$this->db->order_by('internal_vehicles.id', $order);
		}
		else if ($by == 1) {
			$this->db->order_by('rut', $order);
		}
		else if($by == 2){
			$this->db->order_by('name', $order);
		}
		else if($by == 3){
			$this->db->order_by('patent', $order);
		}
		else if($by == 4){
			$this->db->order_by('department', $order);
		}
		else if($by == 5){
			$this->db->order_by('profile', $order);
		}
		else if($by == 6){
			$this->db->order_by('door', $order);
		}
		else if($by == 10){
			$this->db->order_by('internal_vehicles.created', $order);
		}

		$this->db->limit($length, $start);
		$query = $this->db->get('internal_vehicles');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getCount() {
		return $this->db->count_all('internal_vehicles');
	}

	public function getCountSearch($search, $start, $length, $order, $by) {
		$this->db->select('internal_vehicles.id as id');
		$this->db->join('people', 'people.id = internal_vehicles.people_id');
		$this->db->join('doors', 'doors.id = internal_vehicles.doors_id');
		$this->db->join('people_profiles', 'people_profiles.id = people.people_profiles_id');
		$this->db->join('departments', 'departments.id=people.departments_id');
		$this->db->join('reasons_error', 'reasons_error.id = internal_vehicles.reasons_error_id', 'left');
		$this->db->join('vehicles', 'vehicles.id = internal_vehicles.vehicles_id');

		$this->db->like('people.rut', $search);
		$this->db->or_like('people.name', $search);
		$this->db->or_like('internal_vehicles_errors.created', $search);
		$this->db->or_like('reason', $search);

		$quer = $this->db->get('internal_vehicles')->num_rows();
		return $quer;
	}
	// fin funciones auxiliares


}