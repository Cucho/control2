<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MSensors_Doors extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function getSensors_Doors($start, $length, $search, $order, $by) {
		$retornar = array();
		if ($search) {
			$busca = $this->getSearchSensors_Doors($search, $start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCountSearch($search, $start, $length, $order, $by);
			$retornar['data'] = $busca['datos'];
		}
		else {
			$todo = $this->getAllSensors_Doors($start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCount();
			$retornar['data'] = $todo['datos'];
		}

		$retornar['numDataTotal'] = $this->getCount();

		return $retornar;
	}

	// Funciones auxiliares datatable
	public function getAllSensors_Doors($start, $length, $order, $by) {
		$this->db->select('door, sensor, doors_id, sensors_id');
		$this->db->join('doors','doors.id = sensors_doors.doors_id');
		$this->db->join('sensors','sensors.id = sensors_doors.sensors_id');
		if ($by == 0) {
			$this->db->order_by('door', $order);
		}
		else {
			$this->db->order_by('sensor', $order);
		}
		$this->db->limit($length, $start);
		$query = $this->db->get('sensors_doors');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getSearchSensors_Doors($search, $start, $length, $order, $by) {
		$this->db->select('door, sensor, doors_id, sensors_id');
		$this->db->like('door', $search);
		$this->db->or_like('sensor', $search);
		if ($by == 0) {
			$this->db->order_by('door', $order);
		}
		else {
			$this->db->order_by('sensor', $order);
		}
		$this->db->join('doors','doors.id = sensors_doors.doors_id');
		$this->db->join('sensors','sensors.id = sensors_doors.sensors_id');
		$this->db->limit($length, $start);
		$query = $this->db->get('sensors_doors');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getCount() {
		return $this->db->count_all('sensors_doors');
	}

	public function getCountSearch($search, $start, $length, $order, $by) {
		$this->db->select('door');
		$this->db->like('door', $search);
		$this->db->or_like('sensor', $search);
		$this->db->join('doors','doors.id = sensors_doors.doors_id');
		$this->db->join('sensors','sensors.id = sensors_doors.sensors_id');
		$quer = $this->db->get('sensors_doors')->num_rows();
		return $quer;
	}
	// fin funciones auxiliares

	//Crud
	public function addSensor_Door($data) {
		if($this->db->insert('sensors_doors', $data))
			return true;
		else
			return false;
	}

	public function deleteSensor_Door($sensor, $door) {
		$this->db->where('sensors_id', $sensor);
		$this->db->where('doors_id', $door);
		if($this->db->delete('sensors_doors'))
			return true;
		else
			return false;
	}

	public function getAllSensors() {
		$this->db->select('id, sensor, code');
		$this->db->from('sensors');
		$this->db->order_by('sensor');

		return $this->db->get()->result_array();
	}

	public function getAllDoors() {
		$this->db->select('id, door');
		$this->db->from('doors');
		$this->db->order_by('door');

		return $this->db->get()->result_array();
	}

	//---------------------------------------------------------

	
}