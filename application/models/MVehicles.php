<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MVehicles extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function getVehicles($start, $length, $search, $order, $by) {
		$retornar = array();
		if ($search) {
			$busca = $this->getSearchVehicle($search, $start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCountSearch($search, $start, $length, $order, $by);
			$retornar['data'] = $busca['datos'];
		}
		else {
			$todo = $this->getAllVehicles($start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCount();
			$retornar['data'] = $todo['datos'];
		}

		$retornar['numDataTotal'] = $this->getCount();

		return $retornar;
	}

	public function getVehicle($id) {
		$this->db->select('vehicles.id, patent, model, vehicles.internal as internal, vehicles.companies_id, vehicles.people_id, vehicles.vehicles_type_id, vehicles.vehicles_profiles_id, DATE_FORMAT(vehicles.created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(vehicles.modified, "%d-%m-%Y %H:%i:%s") as modified, companies.company as company, people.name as name, people.last_name as last_name,type, profile, state, vehicles.states_id');
		$this->db->join('companies','companies.id = vehicles.companies_id');
		$this->db->join('people','people.id = vehicles.people_id');
		$this->db->join('vehicles_type','vehicles_type.id = vehicles.vehicles_type_id');
		$this->db->join('vehicles_profiles','vehicles_profiles.id = vehicles.vehicles_profiles_id');
		$this->db->join('states', 'states.id = vehicles.states_id');
		$this->db->from('vehicles');
		$this->db->where('vehicles.id', $id);
		$this->db->limit(1);

		return $this->db->get()->result_array();
	}

	// Funciones auxiliares datatable
	public function getAllVehicles($start, $length, $order, $by) {
		$this->db->select('vehicles.id, patent, model, vehicles.internal as internal, vehicles.companies_id, vehicles.people_id, vehicles.vehicles_type_id, vehicles.vehicles_profiles_id, DATE_FORMAT(vehicles.created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(vehicles.modified, "%d-%m-%Y %H:%i:%s") as modified, companies.company as company, people.name as name, people.last_name as last_name, type, profile, state, vehicles.states_id');
		$this->db->join('companies','companies.id = vehicles.companies_id');
		$this->db->join('people','people.id = vehicles.people_id');
		$this->db->join('vehicles_type','vehicles_type.id = vehicles.vehicles_type_id');
		$this->db->join('vehicles_profiles','vehicles_profiles.id = vehicles.vehicles_profiles_id');
		$this->db->join('states', 'states.id = vehicles.states_id');
		switch ($by) {
			case 0:
				$this->db->order_by('vehicles.id', $order);
				break;
			case 1:
				$this->db->order_by('patent', $order);
				break;
			case 2:
				$this->db->order_by('model', $order);
				break;
			case 3:
				$this->db->order_by('vehicles.internal', $order);
				break;
			case 4:
				$this->db->order_by('company', $order);
				break;
			case 5:
				$this->db->order_by('name', $order);
				break;
			case 6:
				$this->db->order_by('type', $order);
				break;
			case 7:
				$this->db->order_by('profile', $order);
				break;
		}
		
		$this->db->limit($length, $start);
		$query = $this->db->get('vehicles');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getSearchVehicle($search, $start, $length, $order, $by) {
		$this->db->select('vehicles.id, patent, model, vehicles.internal as internal, vehicles.companies_id, vehicles.people_id, vehicles.vehicles_type_id, vehicles.vehicles_profiles_id, DATE_FORMAT(vehicles.created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(vehicles.modified, "%d-%m-%Y %H:%i:%s") as modified, companies.company as company, people.name as name, people.last_name as last_name, type, profile, state, vehicles.states_id');
		$this->db->like('vehicles.id', $search);
		$this->db->or_like('patent', $search);
		$this->db->or_like('model', $search);
		$this->db->or_like('vehicles.internal', $search);
		$this->db->or_like('company', $search);
		$this->db->or_like('name', $search);
		$this->db->or_like('type', $search);
		$this->db->or_like('profile', $search);
		switch ($by) {
			case 0:
				$this->db->order_by('vehicles.id', $order);
				break;
			case 1:
				$this->db->order_by('patent', $order);
				break;
			case 2:
				$this->db->order_by('model', $order);
				break;
			case 3:
				$this->db->order_by('internal', $order);
				break;
			case 4:
				$this->db->order_by('company', $order);
				break;
			case 5:
				$this->db->order_by('name', $order);
				break;
			case 6:
				$this->db->order_by('type', $order);
				break;
			case 7:
				$this->db->order_by('profile', $order);
				break;
		}
		$this->db->join('companies','companies.id = vehicles.companies_id');
		$this->db->join('people','people.id = vehicles.people_id');
		$this->db->join('vehicles_type','vehicles_type.id = vehicles.vehicles_type_id');
		$this->db->join('vehicles_profiles','vehicles_profiles.id = vehicles.vehicles_profiles_id');
		$this->db->join('states', 'states.id = vehicles.states_id');
		$this->db->limit($length, $start);
		$query = $this->db->get('vehicles');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getCount() {
		return $this->db->count_all('vehicles');
	}

	public function getCountSearch($search, $start, $length, $order, $by) {
		$this->db->select('vehicles.id');
		$this->db->like('vehicles.id', $search);
		$this->db->or_like('patent', $search);
		$this->db->or_like('model', $search);
		$this->db->or_like('vehicles.internal', $search);
		$this->db->or_like('company', $search);
		$this->db->or_like('name', $search);
		$this->db->or_like('type', $search);
		$this->db->or_like('profile', $search);
		$this->db->join('companies','companies.id = vehicles.companies_id');
		$this->db->join('people','people.id = vehicles.people_id');
		$this->db->join('vehicles_type','vehicles_type.id = vehicles.vehicles_type_id');
		$this->db->join('vehicles_profiles','vehicles_profiles.id = vehicles.vehicles_profiles_id');
		$this->db->join('states', 'states.id = vehicles.states_id');
		$quer = $this->db->get('vehicles')->num_rows();
		return $quer;
	}
	// fin funciones auxiliares

	//Crud
	public function addVehicle($data) {
		if($this->db->insert('vehicles', $data))
			return true;
		else
			return false;
	}

	public function editVehicle($data, $id) {
		$this->db->where('id', $id);
		if($this->db->update('vehicles', $data))
			return true;
		else
			return false;
	}

	public function deleteVehicle($id) {
		$this->db->where('id', $id);
		if($this->db->delete('vehicles'))
			return true;
		else
			return false;
	}
	//Functions add door zone

	public function getAllCompanies() {
		$this->db->select('id, company');
		$this->db->from('companies');
		$this->db->order_by('company');

		return $this->db->get()->result();
	}

	public function getAllPeople() {
		$this->db->select('id, name, last_name');
		$this->db->from('people');
		$this->db->order_by('name');

		return $this->db->get()->result();
	}

	public function getAllVehicles_Type() {
		$this->db->select('id, type');
		$this->db->from('vehicles_type');
		$this->db->order_by('type');

		return $this->db->get()->result();
	}

	public function getAllVehicles_Profiles() {
		$this->db->select('id, profile');
		$this->db->from('vehicles_profiles');
		$this->db->order_by('profile');

		return $this->db->get()->result();
	}

}