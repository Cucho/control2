<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MVehicles_Drivers extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function getVehicles_Drivers($start, $length, $search, $order, $by) {
		$retornar = array();
		if ($search) {
			$busca = $this->getSearchVehicle_Driver($search, $start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCountSearch($search, $start, $length, $order, $by);
			$retornar['data'] = $busca['datos'];
		}
		else {
			$todo = $this->getAllVehicles_Drivers($start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCount();
			$retornar['data'] = $todo['datos'];
		}

		$retornar['numDataTotal'] = $this->getCount();

		return $retornar;
	}

	public function getVehicle_Driver($id) {
		$this->db->select('vehicles.id as vid, patent, model, people.id as pid, rut, name, last_name');
		$this->db->join('people','people.id = vehicles_drivers.people_id');
		$this->db->join('vehicles','vehicles.id = vehicles_drivers.vehicles_id');
		$this->db->from('vehicles_drivers');
		$this->db->where('vehicles_drivers.vehicles_id', $id);
		$this->db->or_where('vehicles_drivers.people_id', $id);
		$this->db->limit(1);

		return $this->db->get()->result_array();
	}

	// Funciones auxiliares datatable
	public function getAllVehicles_Drivers($start, $length, $order, $by) {
		$this->db->select('vehicles.id as vid, patent, model, people.id as pid, rut, name, last_name');
		$this->db->join('people','people.id = vehicles_drivers.people_id');
		$this->db->join('vehicles','vehicles.id = vehicles_drivers.vehicles_id');
		switch ($by) {
			case 0:
				$this->db->order_by('vehicles.id', $order);
				break;
			case 1:
				$this->db->order_by('patent', $order);
				break;
			case 2:
				$this->db->order_by('model', $order);
				break;
			case 3:
				$this->db->order_by('people.id', $order);
				break;
			case 4:
				$this->db->order_by('rut', $order);
				break;
			case 5:
				$this->db->order_by('name', $order);
				break;
			case 6:
				$this->db->order_by('last_name', $order);
				break;
		}
		$this->db->limit($length, $start);
		$query = $this->db->get('vehicles_drivers');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getSearchVehicle_Driver($search, $start, $length, $order, $by) {
		$this->db->select('vehicles.id as vid, patent, model, people.id as pid, rut, name, last_name');
		$this->db->like('vehicles.id', $search);
		$this->db->or_like('patent', $search);
		$this->db->or_like('model', $search);
		$this->db->or_like('people.id', $search);
		$this->db->or_like('rut', $search);
		$this->db->or_like('name', $search);
		$this->db->or_like('last_name', $search);
		switch ($by) {
			case 0:
				$this->db->order_by('vehicles.id', $order);
				break;
			case 1:
				$this->db->order_by('patent', $order);
				break;
			case 2:
				$this->db->order_by('model', $order);
				break;
			case 3:
				$this->db->order_by('people.id', $order);
				break;
			case 4:
				$this->db->order_by('rut', $order);
				break;
			case 5:
				$this->db->order_by('name', $order);
				break;
			case 6:
				$this->db->order_by('last_name', $order);
				break;
		}
		$this->db->join('people','people.id = vehicles_drivers.people_id');
		$this->db->join('vehicles','vehicles.id = vehicles_drivers.vehicles_id');
		$this->db->limit($length, $start);
		$query = $this->db->get('vehicles_drivers');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getCount() {
		return $this->db->count_all('vehicles_drivers');
	}

	public function getCountSearch($search, $start, $length, $order, $by) {
		$this->db->select('vehicles_drivers.vehicles_id');
		$this->db->like('vehicles.id', $search);
		$this->db->or_like('patent', $search);
		$this->db->or_like('model', $search);
		$this->db->or_like('people.id', $search);
		$this->db->or_like('rut', $search);
		$this->db->or_like('name', $search);
		$this->db->or_like('last_name', $search);
		$this->db->join('people','people.id = vehicles_drivers.people_id');
		$this->db->join('vehicles','vehicles.id = vehicles_drivers.vehicles_id');
		$quer = $this->db->get('vehicles_drivers')->num_rows();
		return $quer;
	}
	// fin funciones auxiliares

	//Crud
	public function addVehicle_Driver($data) {
		if($this->db->insert('vehicles_drivers', $data))
			return true;
		else
			return false;
	}

	public function deletecVehicle_Driver($vehicles_id, $people_id) {
		$this->db->where('vehicles_id', $vehicles_id);
		$this->db->where('people_id', $people_id);
		if($this->db->delete('vehicles_drivers'))
			return true;
		else
			return false;
	}
	

	public function getAllPeople() {
		$this->db->select('id, rut, name, last_name');
		$this->db->from('people');
		$this->db->order_by('name');

		return $this->db->get()->result_array();
	}

	public function getAllVehicles() {
		$this->db->select('id, patent, model');
		$this->db->from('vehicles');
		$this->db->order_by('patent');

		return $this->db->get()->result_array();
	}

}