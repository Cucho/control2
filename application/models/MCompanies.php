<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MCompanies extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function getCompanies($start, $length, $search, $order, $by) {
		$retornar = array();
		if ($search) {
			$busca = $this->getSearchCompanies($search, $start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCountSearch($search, $start, $length, $order, $by);
			$retornar['data'] = $busca['datos'];
		}
		else {
			$todo = $this->getAllCompanies($start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCount();
			$retornar['data'] = $todo['datos'];
		}

		$retornar['numDataTotal'] = $this->getCount();

		return $retornar;
	}

	public function getCompany($id) { ///
		$this->db->select('id, company, address, phone, email, contact, DATE_FORMAT(companies.created, "%d-%m-%Y %H:%i:%s") as created, internal,DATE_FORMAT(companies.modified, "%d-%m-%Y %H:%i:%s") as modified');
		$this->db->from('companies');
		$this->db->where('id', $id);
		$this->db->limit(1);

		return $this->db->get()->result_array();
	}

	// Funciones auxiliares datatable
	public function getAllCompanies($start, $length, $order, $by) {
		$this->db->select('id, company, address, phone, email, contact');
		if ($by == 0) {
			$this->db->order_by('id', $order);
		}
		else {
			$this->db->order_by('company', $order);
		}
		$this->db->limit($length, $start);
		$query = $this->db->get('companies');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getSearchCompanies($search, $start, $length, $order, $by) {
		$this->db->select('id, company, address, phone, email, contact');
		$this->db->like('company', $search);
		$this->db->or_like('address', $search);
		$this->db->or_like('phone', $search);
		$this->db->or_like('email', $search);
		$this->db->or_like('contact', $search);
		if ($by == 0) {
			$this->db->order_by('company', $order);
		}
		$this->db->limit($length, $start);
		$query = $this->db->get('companies');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getCount() {
		return $this->db->count_all('companies');
	}

	public function getCountSearch($search, $start, $length, $order, $by) {
		$this->db->select('id');
		$this->db->like('company', $search);
		$this->db->or_like('address', $search);
		$this->db->or_like('phone', $search);
		$this->db->or_like('email', $search);
		$this->db->or_like('contact', $search);
		$quer = $this->db->get('companies')->num_rows();
		return $quer;
	}
	// fin funciones auxiliares

	//Crud
	public function addCompanies($data) {
		if($this->db->insert('companies', $data))
			return true;
		else
			return false;
	}

	public function editCompanies($data, $id) {
		$this->db->where('id', $id);
		if($this->db->update('companies', $data))
			return true;
		else
			return false;
	}

	public function deleteCompanies($id) {
		$this->db->where('id', $id);
		if($this->db->delete('companies'))
			return true;
		else
			return false;
	}
}