<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MAccess_People extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	////////////////////////////////////
	public function getPeople_Rut($rut) {
		$this->db->select('people.id, rut, digit, people.name, last_name, people.address, people.email, people.phone, allow_all, is_visited, people.internal as internal, people.people_profiles_id, profile, companies_id, company, DATE_FORMAT(people.created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(people.modified, "%d-%m-%Y %H:%i:%s") as modified, people.states_id');
		$this->db->from('people');
		$this->db->join('people_profiles','people_profiles.id = people.people_profiles_id');
		$this->db->join('companies','companies.id = people.companies_id');
		$this->db->where('rut', $rut);
		//$this->db->where('internal', 0);
		$this->db->limit(1);

		return $this->db->get()->result_array();
	}

	public function addPeople($data) {
		if($this->db->insert('people', $data))
			return true;
		else
			return false;
	}

	public function addReasons_Visit($data) {
		if($this->db->insert('reasons_visit', $data))
			return $this->db->insert_id();
		else
			return false;
	}

	public function GetTreeview() {
		$this->db->select('id, zone');
		$this->db->from('zones');
		$this->db->order_by('zone');
		$z = $this->db->get()->result();

		$this->db->select('id, area, zones_id');
		$this->db->from('areas');
		$this->db->order_by('area');
		$a = $this->db->get()->result();

		$this->db->select('id, department, areas_id');
		$this->db->from('departments');
		$this->db->order_by('department');
		$d = $this->db->get()->result();

		$this->db->select('zones_id, doors_id as dzid');
		$this->db->from('doors_zones');
		$dz = $this->db->get()->result();

		$this->db->select('areas_id, doors_id as daid');
		$this->db->from('doors_areas');
		$da = $this->db->get()->result();

		$this->db->select('departments_id, doors_id as ddid');
		$this->db->from('doors_departments');
		$dd = $this->db->get()->result();

		$object = array();
		$zone = array();
		foreach ($z as $k) {
			$zone[] = array(
				'zid' => $k->id,
				'zone' => $k->zone,
			);
		}
		$area = array();
		foreach ($a as $y) {
			$area[] = array(
				'aid' => $y->id,
				'area' => $y->area,
				'area_parent' => $y->zones_id,
			);
		}
		$dpto = array();
		foreach ($d as $e) {
			$dpto[] = array(
				'did' => $e->id,
				'department' => $e->department,
				'department_parent' => $e->areas_id,
			);
		}
		$dzones = array();
		foreach ($dz as $e) {
			$dzones[] = array(
				'door_z' => $e->dzid,
				'zones_id' => $e->zones_id
			);
		}
		$dareas = array();
		foreach ($da as $e) {
			$dareas[] = array(
				'door_a' => $e->daid,
				'areas_id' => $e->areas_id
			);
		}
		$ddepto = array();
		foreach ($dd as $e) {
			$ddepto[] = array(
				'door_d' => $e->ddid,
				'departments_id' => $e->departments_id
			);
		}

		return $object[] = array(
			'zona' => $zone, 
			'area' => $area, 
			'dpto' => $dpto, 
			'dzones' => $dzones,
			'dareas' => $dareas,
			'ddepto' => $ddepto
		);
	}

	public function getDoorLevel() {
		$this->db->select('id, door, description, level');
		$this->db->from('doors');
		return $this->db->get()->result();
	}

	/*----------------------------*/

	public function getControlIn($start, $length, $search, $order, $by, $control){
		$retornar = array();
		if ($search) {
			$busca = $this->getSearchControl($search, $start, $length, $order, $by, $control);
			$retornar['numDataFilter'] = $this->getCountSearchControl($search, $start, $length, $order, $by, $control);
			$retornar['data'] = $busca['datos'];
		}
		else {
			$todo = $this->getAllControl($start, $length, $order, $by, $control);
			$retornar['numDataFilter'] = $this->getCountControl($control);
			$retornar['data'] = $todo['datos'];
		}

		$retornar['numDataTotal'] = $this->getCountControl($control);

		return $retornar;
	}

	public function getAllControl($start, $length, $order, $by, $control) {
		$this->db->select('access_people_forms.id, access_people_forms.title, company, rut, digit, name, last_name, patent, DATE_FORMAT(access_people.created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(access_people.modified, "%d-%m-%Y %H:%i:%s") as modified');

		$this->db->join('access_people_forms', 'access_people_forms.access_people_id = access_people.id');
		$this->db->join('people', 'people.id = access_people.people_id');
		$this->db->join('companies', 'companies.id = people.companies_id');
		$this->db->join('vehicles', 'vehicles.id = access_people.vehicles_id');
		
		$this->db->where('access_people.control_init', 1);
		$this->db->where('access_people_forms.control', $control);
		
		if ($by == 0) {
			$this->db->order_by('title', $order);
		}
		elseif($by == 1) {
			$this->db->order_by('rut', $order);
		}
		elseif ($by == 2) {
		 	$this->db->order_by('patent', $order);
		}
		elseif ($by == 3) {
			$this->db->order_by('created', $order);
		}
		elseif ($by == 4) {
			$this->db->order_by('modified', $order);
		}

		$this->db->limit($length, $start);

		$query = $this->db->get('access_people');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getSearchControl($search, $start, $length, $order, $by, $control) {
		$this->db->select('access_people_forms.id, access_people_forms.title, company, rut, digit, name, last_name, patent, DATE_FORMAT(access_people.created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(access_people.modified, "%d-%m-%Y %H:%i:%s") as modified');

		$this->db->join('access_people_forms', 'access_people_forms.access_people_id = access_people.id');
		$this->db->join('people', 'people.id = access_people.people_id');
		$this->db->join('companies', 'companies.id = people.companies_id');
		$this->db->join('vehicles', 'vehicles.id = access_people.vehicles_id');

		$this->db->group_start();
		$this->db->like('access_people_forms.title', $search);
		$this->db->or_like('rut', $search);
		$this->db->or_like('patent', $search);
		$this->db->or_like('access_people.created', $search);
		$this->db->or_like('access_people.modified', $search);
		$this->db->group_end();
		
		if ($by == 0) {
			$this->db->order_by('title', $order);
		}
		elseif($by == 1) {
			$this->db->order_by('rut', $order);
		}
		elseif ($by == 2) {
		 	$this->db->order_by('patent', $order);
		}
		elseif ($by == 3) {
			$this->db->order_by('created', $order);
		}
		elseif ($by == 4) {
			$this->db->order_by('modified', $order);
		}

		$this->db->where('access_people.control_init', 1);
		$this->db->where('access_people_forms.control', $control);

		$this->db->limit($length, $start);

		$query = $this->db->get('access_people');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getCountControl($control) {
		$this->db->select('access_people_forms.id');

		$this->db->where('access_people.control_init', 1);
		$this->db->where('access_people_forms.control', $control);
		
		$this->db->join('access_people', 'access_people.id = access_people_forms.access_people_id');

		$this->db->distinct('access_people_forms.id');
		return $this->db->count_all_results('access_people_forms');
	}

	public function getCountSearchControl($search, $start, $length, $order, $by, $control) {
		$this->db->select('access_people_forms.id');

		$this->db->join('access_people', 'access_people.id = access_people_forms.access_people_id');
		$this->db->join('people', 'people.id = access_people.people_id');
		$this->db->join('vehicles', 'vehicles.id = access_people.vehicles_id');

		$this->db->group_start();
		$this->db->like('access_people_forms.title', $search);
		$this->db->or_like('rut', $search);
		$this->db->or_like('patent', $search);
		$this->db->or_like('access_people.created', $search);
		$this->db->or_like('access_people.modified', $search);
		$this->db->group_end();
		
		$this->db->where('access_people.control_init', 1);
		$this->db->where('access_people_forms.control', $control);

		$this->db->limit($length, $start);
		$quer = $this->db->get('access_people_forms')->num_rows();
		return $quer;
	}

	/**/

	public function getControlOut($start, $length, $search, $order, $by, $control){
		$retornar = array();
		if ($search) {
			$busca = $this->getSearchControlOut($search, $start, $length, $order, $by, $control);
			$retornar['numDataFilter'] = $this->getCountSearchControlOut($search, $start, $length, $order, $by, $control);
			$retornar['data'] = $busca['datos'];
		}
		else {
			$todo = $this->getAllControlOut($start, $length, $order, $by, $control);
			$retornar['numDataFilter'] = $this->getCountControlOut($control);
			$retornar['data'] = $todo['datos'];
		}

		$retornar['numDataTotal'] = $this->getCountControl($control);

		return $retornar;
	}

	public function getAllControlOut($start, $length, $order, $by, $control) {
		$this->db->select('access_people_forms.id, access_people_forms.title, company, rut, digit, name, last_name, patent, DATE_FORMAT(access_people.created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(access_people.modified, "%d-%m-%Y %H:%i:%s") as modified');

		$this->db->join('access_people_forms', 'access_people_forms.access_people_id = access_people.id');
		$this->db->join('people', 'people.id = access_people.people_id');
		$this->db->join('companies', 'companies.id = people.companies_id');
		$this->db->join('vehicles', 'vehicles.id = access_people.vehicles_id');

		if ($by == 0) {
			$this->db->order_by('title', $order);
		}
		elseif($by == 1) {
			$this->db->order_by('rut', $order);
		}
		elseif ($by == 2) {
		 	$this->db->order_by('patent', $order);
		}
		elseif ($by == 3) {
			$this->db->order_by('created', $order);
		}
		elseif ($by == 4) {
			$this->db->order_by('modified', $order);
		}

		$this->db->where('access_people.control_end', 1);
		$this->db->where('access_people_forms.control', $control);
		
		$this->db->limit($length, $start);

		$query = $this->db->get('access_people');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getSearchControlOut($search, $start, $length, $order, $by, $control) {
		$this->db->select('access_people_forms.id, access_people_forms.title, company, rut, digit, name, last_name, patent, DATE_FORMAT(access_people.created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(access_people.modified, "%d-%m-%Y %H:%i:%s") as modified');

		$this->db->join('access_people_forms', 'access_people_forms.access_people_id = access_people.id');
		$this->db->join('people', 'people.id = access_people.people_id');
		$this->db->join('companies', 'companies.id = people.companies_id');
		$this->db->join('vehicles', 'vehicles.id = access_people.vehicles_id');

		$this->db->group_start();
		$this->db->like('access_people_forms.title', $search);
		$this->db->or_like('rut', $search);
		$this->db->or_like('patent', $search);
		$this->db->or_like('access_people.created', $search);
		$this->db->or_like('access_people.modified', $search);
		$this->db->group_end();
		
		if ($by == 0) {
			$this->db->order_by('title', $order);
		}
		elseif($by == 1) {
			$this->db->order_by('rut', $order);
		}
		elseif ($by == 2) {
		 	$this->db->order_by('patent', $order);
		}
		elseif ($by == 3) {
			$this->db->order_by('created', $order);
		}
		elseif ($by == 4) {
			$this->db->order_by('modified', $order);
		}

		$this->db->where('access_people.control_end', 1);
		$this->db->where('access_people_forms.control', $control);

		$this->db->limit($length, $start);

		$query = $this->db->get('access_people');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getCountControlOut($control) {
		$this->db->select('access_people_forms.id');

		$this->db->join('access_people', 'access_people.id = access_people_forms.access_people_id');

		$this->db->where('access_people.control_end', 1);
		$this->db->where('access_people_forms.control', $control);

		$this->db->distinct('access_people_forms.id');
		return $this->db->count_all_results('access_people_forms');
	}

	public function getCountSearchControlOut($search, $start, $length, $order, $by, $control) {
		$this->db->select('access_people_forms.id');

		$this->db->join('access_people', 'access_people.id = access_people_forms.access_people_id');
		$this->db->join('people', 'people.id = access_people.people_id');
		$this->db->join('vehicles', 'vehicles.id = access_people.vehicles_id');

		$this->db->group_start();
		$this->db->like('access_people_forms.title', $search);
		$this->db->or_like('rut', $search);
		$this->db->or_like('patent', $search);
		$this->db->or_like('access_people.created', $search);
		$this->db->or_like('access_people.modified', $search);
		$this->db->group_end();
		
		$this->db->where('access_people.control_end', 1);
		$this->db->where('access_people_forms.control', $control);

		$this->db->limit($length, $start);
		$quer = $this->db->get('access_people_forms')->num_rows();
		return $quer;
	}

	/**/

	public function getDetailsInternalForms($apf_id, $control){
		$this->db->select('access_people_forms.title, access_people_forms.observation, company, rut, digit, name, last_name, patent, type, DATE_FORMAT(access_people.created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(access_people.modified, "%d-%m-%Y %H:%i:%s") as modified, forms_detail.order as qorder, forms_detail.question, access_people_answers.order as aorder, access_people_answers.answer, access_people_answers.control');

		$this->db->join('access_people', 'access_people.id = access_people_forms.access_people_id');
		$this->db->join('people', 'people.id = access_people.people_id');
		$this->db->join('companies', 'companies.id = people.companies_id');
		$this->db->join('vehicles', 'vehicles.id = access_people.vehicles_id');
		$this->db->join('access_people_answers', 'access_people_answers.access_people_id = access_people_forms.access_people_id');
		$this->db->join('vehicles_type', 'vehicles_type.id = vehicles.vehicles_type_id');
		$this->db->join('forms_detail', 'forms_detail.forms_id = access_people_forms.forms_id');
		
		$this->db->where('access_people_forms.id', $apf_id);
		$this->db->where('access_people_answers.control', $control);
		$this->db->where('access_people_forms.control', $control);
		$this->db->where('access_people_answers.order = forms_detail.order');
		$this->db->order_by('forms_detail.order');

		return $this->db->get('access_people_forms')->result();
	}

	/*----------------------------*/

	public function getAllEntry0() {
		$this->db->select('access_people.id as id, access_people.vehicles_id as vehicles_id, access_people.end_time as end_time, people.rut as rut, people.digit as digit, people.name as name, people.last_name as last_name, people_profiles.profile as profile, companies.company as company, DATE_FORMAT(access_people_intents.created, "%d-%m-%Y %H:%i:%s") as created');
		$this->db->from('access_people');
		$this->db->join('access_people_intents', 'access_people_intents.access_people_id = access_people.id');
		$this->db->join('access_people_state_history', 'access_people_state_history.access_people_id = access_people.id');
		$this->db->join('people', 'people.id = access_people.people_id');
		$this->db->join('people_profiles', 'people_profiles.id = people.people_profiles_id');
		$this->db->join('companies', 'companies.id = people.companies_id');
		$this->db->where('access_people.entry', 0);
		$this->db->where('access_people_state_history.access_state_id', 2);
		$this->db->where('access_people_intents.success', 1);
		$this->db->where('access_people_intents.entry', 0);
		$this->db->order_by('access_people.id', 'desc');
		$this->db->group_by('access_people.id');

		return $this->db->get()->result();
	}

	public function getAllEntry1() {
		$this->db->select('access_people.id, entry, people_id, exit_time, access_people.vehicles_id as vehicles_id, observation, people.rut, people.digit, people.name, people.last_name, people_profiles.profile, companies.company');
		$this->db->join('people', 'people.id = people_id');
		$this->db->join('people_profiles', 'people_profiles.id = people_profiles_id');
		$this->db->join('companies', 'companies.id = companies_id');
		$this->db->from('access_people');
		$this->db->where('entry', 1);
		date_default_timezone_set('America/Santiago');
		$this->db->where('exit_time >=', date('Y-m-d 00:00:00'));
		$this->db->where('exit_time <=', date('Y-m-d 23:59:59'));
		$this->db->order_by('exit_time', 'desc');
		return $this->db->get()->result();
	}

	public function generateAccessOut($id, $date, $observation){
		$this->db->set('entry', 1);
		$this->db->set('exit_time', $date);
		$this->db->set('observation', 'CONCAT(observation,"'.$observation.'")', FALSE);
		$this->db->where('entry', 0);
		$this->db->where('id', $id);
		if ($this->db->update('access_people')) {
			return true;
		}
		else {
			return false;
		}
	}

	public function generateAccessIn($data) {
		if($this->db->insert('access_people', $data))
			return $this->db->insert_id();
		else
			return 0;
	}
	//
	public function inAccessPeopleAreas($data) {
		if($this->db->insert('access_people_areas', $data))
			return true;
		else
			return false;
	}
	public function inAccessPeopleDepartment($data) {
		if($this->db->insert('access_people_department', $data))
			return true;
		else
			return false;
	}
	public function inAccessPeopleZones($data) {
		if($this->db->insert('access_people_zones', $data))
			return true;
		else
			return false;
	}
	public function inAccessPeopleReasonVisit($data) {
		if($this->db->insert('access_people_reasons_visit', $data))
			return true;
		else
			return false;
	}
	public function inAccessPeopleRoute($data) {
		if($this->db->insert('access_people_route', $data))
			return true;
		else
			return false;
	}
	public function inAccessPeopleVisit($data) {
		if($this->db->insert('access_people_visit', $data))
			return true;
		else
			return false;
	}

	public function generateInAccessStateHistory($data) {
		if($this->db->insert('access_people_state_history', $data))
			return true;
		else
			return false;
	}

	public function accessPeopleForms($data){
		$this->db->insert('access_people_forms', $data);
	}

	public function accessPeopleForms2($id, $forms_id, $title, $tobservation){
		$this->db->set('title', $title);
		$this->db->set('observation', $tobservation);
		$this->db->where('access_people_id', $id);
		$this->db->where('forms_id', $forms_id);
		$this->db->where('title', null);
		$this->db->where('observation', null);
		$this->db->update('access_people_forms');
	}

	public function accessPeopleAnswers($data){
		$this->db->insert('access_people_answers', $data);
	}

	
	//
	///////////////////////////////////
	public function getPeopleByAccessPeopleID($access_people_id){
		$this->db->select('rut, digit, name, last_name, profile, company');
		$this->db->from('people');
		$this->db->join('access_people','access_people.people_id=people.id');
		$this->db->join('people_profiles','people_profiles.id=people.people_profiles_id');
		$this->db->join('companies','companies.id=people.companies_id');
		$this->db->where('access_people.id', $access_people_id);
		$this->db->limit(1);

		return $this->db->get()->result_array();
	}

	public function getAllCompanies() {
		$this->db->select('id, company');
		$this->db->from('companies');
		$this->db->order_by('company');

		return $this->db->get()->result();
	}

	public function getAllPeople_Profile() {
		$this->db->select('id, profile');
		$this->db->from('people_profiles');
		$this->db->order_by('profile');

		return $this->db->get()->result();
	}

	public function getAllAccess_State() {
		$this->db->select('id, state');
		$this->db->from('access_state');
		$this->db->order_by('state');

		return $this->db->get()->result();
	}

	public function getAllReasons_Visit() {
		$this->db->select('id, reason');
		$this->db->from('reasons_visit');
		$this->db->order_by('reason');

		return $this->db->get()->result_array();
	}

	public function getAllPeople_Visit() {
		$this->db->select('id, rut, digit, name, last_name');
		$this->db->from('people');
		$this->db->where('is_visited', 1);
		$this->db->order_by('rut');

		return $this->db->get()->result();
	}

	public function getAllVehicle_Type(){
		$this->db->select('id, type');
		$this->db->from('vehicles_type');
		return $this->db->get()->result();
	}

	public function getAllVehicleProfiles(){
		$this->db->select('id, profile');
		$this->db->from('vehicles_profiles');
		return $this->db->get()->result();
	}

	public function getAllPeople(){
		$this->db->select('id, rut, digit, name, last_name, internal');
		$this->db->from('people');
		return $this->db->get()->result();
	}

	public function SearchVehicle($patent){
		$this->db->select('vehicles.id, patent, model, vehicles.internal, vehicles.nfc_code, vehicles.companies_id, company, people_id, people.name, people.last_name, vehicles_type_id, type, vehicles_profiles_id, vehicles_profiles.profile, vehicles.states_id');
		$this->db->from('vehicles');
		$this->db->join('companies', 'companies.id = vehicles.companies_id');
		$this->db->join('people', 'people.id = vehicles.people_id');
		$this->db->join('vehicles_type', 'vehicles_type.id = vehicles.vehicles_type_id');
		$this->db->join('vehicles_profiles', 'vehicles_profiles.id = vehicles.vehicles_profiles_id');
		$this->db->where('patent', $patent);
		return $this->db->get()->result();
	}

	public function addVehicle($data){
		if ($this->db->insert('vehicles', $data)) {
			return $this->db->insert_id();
		}
		else {
			return false;
		}
	}

	public function getAllForm(){
		$this->db->select('id, title, description');
		$this->db->from('forms');
		return $this->db->get()->result();
	}

	public function SearchFormDetail($form){
		$this->db->select('order, question, placeholder, answers_type_id, measures_id, answers_type.id as atid, type, measures.id as mid, measure, acronimo');
		$this->db->join('answers_type', 'answers_type.id = answers_type_id');
		$this->db->join('measures', 'measures.id = measures_id', 'left');
		$this->db->from('forms_detail');
		$this->db->where('forms_id', $form);
		$this->db->order_by('order');
		$q = $this->db->get()->result();
		return $q;
	}

	public function searchPeopleControl($rut){
		$this->db->select('access_people.id as access_people_id, access_people.end_time, access_people.control_end, access_people_forms.forms_id, access_people_forms.title, access_people_forms.observation, forms_detail.order, forms_detail.question, forms_detail.placeholder, forms_detail.answers_type_id, measures_id, answers_type.id as atid, answers_type.type, measures.id as mid, measure, acronimo');
		$this->db->from('people');
		$this->db->join('access_people', 'access_people.people_id = people.id');
		$this->db->join('access_people_forms', 'access_people_forms.access_people_id = access_people.id');
		$this->db->join('forms_detail', 'forms_detail.forms_id = access_people_forms.forms_id');
		$this->db->join('answers_type', 'answers_type.id = answers_type_id');
		$this->db->join('measures', 'measures.id = measures_id', 'left');
		$this->db->where('people.rut', $rut);
		$this->db->where('access_people.entry', 0);
		$this->db->where('access_people_forms.title', null);
		$this->db->order_by('forms_detail.order');
		return $this->db->get()->result();
	}

	public function SearchAccess_People($rut){
		$this->db->select('people.id as people_id, access_people.id as access_people_id, access_people.end_time');
		$this->db->from('people');
		$this->db->join('access_people', 'access_people.people_id = people.id');
		// $this->db->join('access_people_forms', 'access_people_forms.access_people_id = access_people.id');
		//$this->db->join('forms_detail', 'forms_detail.forms_id = access_people_forms.forms_id');
		//$this->db->join('answers_type', 'answers_type.id = answers_type_id');
		//$this->db->join('measures', 'measures.id = measures_id', 'left');
		$this->db->where('people.rut', $rut);
		$this->db->where('access_people.entry', 0);
		//$this->db->order_by('forms_detail.order');
		return $this->db->get()->result();
	}

	public function deleteCurrentInternalState($people_id){
		$this->db->where('people_id', $people_id);
		$this->db->delete('current_internal_state');
	}
	//--------------------------------------------------------------------

	public function getAccess_People($start, $length, $search, $order, $by) {
		$retornar = array();
		if ($search) {
			$busca = $this->getSearchAccess_People($search, $start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCountSearch($search, $start, $length, $order, $by);
			$retornar['data'] = $busca['datos'];
		}
		else {
			$todo = $this->getAllAccess_People($start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCount();
			$retornar['data'] = $todo['datos'];
		}

		$retornar['numDataTotal'] = $this->getCount();

		return $retornar;
	}

	public function getMainDoor($ip){
		//$this->db->select('doors_id');
		$this->db->select('id');
		$this->db->where('ip_host', $ip);
		return $this->db->get('main_access')->result();
	}

	public function getMainAccessId($id){
		$this->db->select('doors_id');
		$this->db->where('main_access.id', $id);
		return $this->db->get('main_access')->result();
	}

	public function generateAccesIntentOut($data){
		$this->db->insert('access_people_intents', $data);
	}

	// Funciones auxiliares datatable
	public function getAllAccess_People($start, $length, $order, $by) {
		$this->db->select('access_people.id as id, rut, digit, people.name as name, people.last_name as last_name, profile, main_access.name as access, company, access_people.entry as entry, access_state.state as state, access_state_id,DATE_FORMAT(access_people.exit_time, "%d-%m-%Y %H:%i:%s") as exit_time, DATE_FORMAT(access_people.created, "%d-%m-%Y %H:%i:%s") as created');
		$this->db->join('people', 'people.id=access_people.people_id');
		$this->db->join('people_profiles', 'people_profiles.id=people.people_profiles_id');
		$this->db->join('main_access', 'main_access.id=access_people.main_access_id');
		$this->db->join('companies', 'companies.id=people.companies_id');
		$this->db->join('access_state', 'access_state.id=access_people.access_state_id');

		if ($by == 0) {
			$this->db->order_by('id', $order);
		}
		elseif($by == 1) {
			$this->db->order_by('rut', $order);
		}
		elseif($by == 2) {
			$this->db->order_by('people.name', $order);
		}
		elseif($by == 3) {
			$this->db->order_by('last_name', $order);
		}
		else {
			$this->db->order_by('company', $order);
		}

		$this->db->limit($length, $start);
		$query = $this->db->get('access_people');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getSearchAccess_People($search, $start, $length, $order, $by) {
		$this->db->select('access_people.id as id, rut, digit, people.name as name, people.last_name as last_name, profile, main_access.name as access, company, access_people.entry as entry, access_state.state as state, access_state_id, DATE_FORMAT(access_people.exit_time, "%d-%m-%Y %H:%i:%s") as exit_time, DATE_FORMAT(access_people.created, "%d-%m-%Y %H:%i:%s") as created');
		$this->db->join('people', 'people.id=access_people.people_id');
		$this->db->join('people_profiles', 'people_profiles.id=people.people_profiles_id');
		$this->db->join('main_access', 'main_access.id=access_people.main_access_id');
		$this->db->join('companies', 'companies.id=people.companies_id');
		$this->db->join('access_state', 'access_state.id=access_people.access_state_id');
		$this->db->like('access_people.id', $search);
		$this->db->or_like('rut', $search);
		$this->db->or_like('people.name', $search);
		$this->db->or_like('last_name', $search);
		$this->db->or_like('company', $search);

		if ($by == 0) {
			$this->db->order_by('access_people.id', $order);
		}
		elseif($by == 1) {
			$this->db->order_by('rut', $order);
		}
		elseif($by == 2) {
			$this->db->order_by('people.name', $order);
		}
		elseif($by == 3) {
			$this->db->order_by('last_name', $order);
		}
		else {
			$this->db->order_by('company', $order);
		}
		$this->db->limit($length, $start);
		$query = $this->db->get('access_people');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getCount() {
		return $this->db->count_all('access_people');
	}

	public function getCountSearch($search, $start, $length, $order, $by) {
		$this->db->select('access_people.id as id');
		$this->db->join('people', 'people.id=access_people.people_id');
		$this->db->join('people_profiles', 'people_profiles.id=people.people_profiles_id');
		$this->db->join('main_access', 'main_access.id=access_people.main_access_id');
		$this->db->join('companies', 'companies.id=people.companies_id');
		$this->db->join('access_state', 'access_state.id=access_people.access_state_id');
		$this->db->like('access_people.id', $search);
		$this->db->or_like('rut', $search);
		$this->db->or_like('people.name', $search);
		$this->db->or_like('last_name', $search);
		$this->db->or_like('company', $search);

		$quer = $this->db->get('access_people')->num_rows();
		return $quer;
	}
	// fin funciones auxiliares

	public function getDetail_Access_People($access_people_id)
	{
		$this->db->select('access_people.id as id, access_people.entry as entry, DATE_FORMAT(access_people.exit_time, "%d-%m-%Y %H:%i:%s") as exit_time, hours, DATE_FORMAT(access_people.end_time, "%d-%m-%Y %H:%i:%s") as end_time, people_id, rut, digit, people.name as name, last_name, people.phone, people. email, profile, company, access_state_id, access_state.state, main_access_id, main_access.name as access, approved_by, observation, DATE_FORMAT(access_people.created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(access_people.modified, "%d-%m-%Y %H:%i:%s") as modified');
		$this->db->from('access_people');
		$this->db->join('people', 'people.id=access_people.people_id');
		$this->db->join('people_profiles', 'people_profiles.id=people.people_profiles_id');
		$this->db->join('main_access', 'main_access.id=access_people.main_access_id');
		$this->db->join('companies', 'companies.id=people.companies_id');
		$this->db->join('access_state', 'access_state.id=access_people.access_state_id');
		$this->db->where('access_people.id', $access_people_id);
		$this->db->limit(1);

		return $this->db->get()->result_array(); 
	}

	public function getDetail_Access_People_Zones($access_people_id)
	{
		$this->db->select('zone');
		$this->db->from('access_people_zones');
		$this->db->join('zones', 'zones.id=access_people_zones.zones_id');
		$this->db->where('access_people_id', $access_people_id);

		return $this->db->get()->result_array();

	}

	public function getDetail_Access_People_Areas($access_people_id)
	{
		$this->db->select('area');
		$this->db->from('access_people_areas');
		$this->db->join('areas', 'areas.id=access_people_areas.areas_id');
		$this->db->where('access_people_id', $access_people_id);

		return $this->db->get()->result_array();

	}

	public function getDetail_Access_People_Departments($access_people_id)
	{
		$this->db->select('department');
		$this->db->from('access_people_department');
		$this->db->join('departments', 'departments.id=access_people_department.departments_id');
		$this->db->where('access_people_id', $access_people_id);

		return $this->db->get()->result_array();

	}

	public function getDetail_Access_People_Intents($access_people_id)
	{
		$this->db->select('door, access_people_intents.flow as flow ,description, entry, success, DATE_FORMAT(access_people_intents.created, "%d-%m-%Y %H:%i:%s") as created');
		$this->db->from('access_people_intents');
		$this->db->join('doors', 'doors.id=access_people_intents.doors_id');
		$this->db->where('access_people_id', $access_people_id);
		$this->db->order_by('created','asc');

		return $this->db->get()->result_array();

	}

	public function getDetail_Access_People_Reason_Visit($access_people_id)
	{
		$this->db->select('reason');
		$this->db->from('access_people_reasons_visit');
		$this->db->join('reasons_visit', 'reasons_visit.id=access_people_reasons_visit.reasons_visit_id');
		$this->db->where('access_people_id', $access_people_id);

		return $this->db->get()->result_array();

	}

	public function getDetail_Access_People_Route($access_people_id)
	{
		$this->db->select('door, description');
		$this->db->from('access_people_route');
		$this->db->join('doors', 'doors.id=access_people_route.doors_id');
		$this->db->where('access_people_id', $access_people_id);

		return $this->db->get()->result_array();

	}

	public function getDetail_Access_People_State_History($access_people_id)
	{
		$this->db->select('access_state_id, state, description, DATE_FORMAT(access_people_state_history.created, "%d-%m-%Y %H:%i:%s") as created');
		$this->db->from('access_people_state_history');
		$this->db->join('access_state', 'access_state.id=access_people_state_history.access_state_id');
		$this->db->where('access_people_id', $access_people_id);

		return $this->db->get()->result_array();

	}

	public function getDetail_Access_People_Visit($access_people_id)
	{
		$this->db->select('rut, digit, name, last_name');
		$this->db->from('access_people_visit');
		$this->db->join('people', 'people.id=access_people_visit.people_id');
		$this->db->where('access_people_id', $access_people_id);

		return $this->db->get()->result_array();

	}

	public function getDetail_Access_People_Vehicle($access_people_id){
		$this->db->select('patent, type, profile');
		$this->db->from('access_people');
		$this->db->join('vehicles', 'vehicles.id = access_people.vehicles_id');
		$this->db->join('vehicles_type', 'vehicles_type.id = vehicles.vehicles_type_id');
		$this->db->join('vehicles_profiles', 'vehicles_profiles.id = vehicles.vehicles_profiles_id');
		$this->db->where('access_people.id', $access_people_id);
		return $this->db->get()->result_array();
	}

	public function getDetail_Access_People_Control($access_people_id){
		$this->db->select('control_init, control_end');
		$this->db->from('access_people');
		$this->db->where('access_people.id', $access_people_id);
		return $this->db->get()->result_array();
	}

	public function getDetail_Access_People_Form_in($access_people_id){
		$this->db->select('title, observation, forms_id, control');
		$this->db->from('access_people_forms');
		$this->db->where('access_people_id', $access_people_id);
		$this->db->order_by('control','asc');
		//$this->db->limit(1);
		return $this->db->get()->result();
	}

	public function getDetail_Access_People_Form_answers($access_people_id, $forms_id, $entry){
		$this->db->select('answer, order');
		$this->db->from('access_people_answers');
		$this->db->where('access_people_id', $access_people_id);
		$this->db->where('forms_id', $forms_id);
		$this->db->where('control', $entry);
		$this->db->order_by('order');
		return $this->db->get()->result();
	}

	public function getCreated($id){
		$this->db->select('DATE_FORMAT(created, "%d-%m-%Y %H:%i:%s") as created');
		$this->db->where('id', $id);
		return $this->db->get('access_people')->result();
	}

	public function getDetail_Access_People_Form_out($access_people_id){
		$this->db->select('access_people_forms.title, access_people_forms.observation, forms_detail.order, question, type, measure, acronimo, answer');
		$this->db->from('access_people_forms');
		$this->db->join('forms', 'forms.id = access_people_forms.forms_id');
		$this->db->join('forms_detail', 'forms_detail.forms_id = access_people_forms.forms_id');
		$this->db->join('answers_type', 'answers_type.id = forms_detail.answers_type_id');
		$this->db->join('measures', 'measures.id = forms_detail.measures_id', 'left');
		$this->db->join('access_people_answers', 'access_people_answers.id = forms_detail.measures_id');
		$this->db->join('access_people', 'access_people.id = access_people_forms.access_people_id');
		$this->db->where('access_people_forms.access_people_id', $access_people_id);
		$this->db->where('access_people.control_end', 1);
		return $this->db->get()->result_array();
	}

	//DELETE

	public function delete_Access_People_Zones($id)
	{
		$this->db->where('access_people_id', $id);
		if($this->db->delete('access_people_zones'))
			return true;
		else
			return false;
	}

	public function delete_Access_People_Areas($id)
	{
		$this->db->where('access_people_id', $id);
		if($this->db->delete('access_people_areas'))
			return true;
		else
			return false;
	}

	public function delete_Access_People_Departments($id)
	{
		$this->db->where('access_people_id', $id);
		if($this->db->delete('access_people_department'))
			return true;
		else
			return false;
	}

	public function delete_Access_People_Visit($id)
	{
		$this->db->where('access_people_id', $id);
		if($this->db->delete('access_people_visit'))
			return true;
		else
			return false;
	}

	public function delete_Access_People_State_History($id)
	{
		$this->db->where('access_people_id', $id);
		if($this->db->delete('access_people_state_history'))
			return true;
		else
			return false;
	}

	public function delete_Access_People_Route($id)
	{
		$this->db->where('access_people_id', $id);
		if($this->db->delete('access_people_route'))
			return true;
		else
			return false;
	}

	public function delete_Access_People_Reasons($id)
	{
		$this->db->where('access_people_id', $id);
		if($this->db->delete('access_people_reasons_visit'))
			return true;
		else
			return false;
	}

	public function delete_Access_People_Intents($id)
	{
		$this->db->where('access_people_id', $id);
		if($this->db->delete('access_people_intents'))
			return true;
		else
			return false;
	}

	public function delete_Access_People($id)
	{
		$this->db->where('id', $id);
		if($this->db->delete('access_people'))
			return true;
		else
			return false;
	}

	public function in_people_list(){
		$this->db->select('people.rut, people.digit, access_people.id');
		$this->db->join('people', 'people.id = access_people.people_id');
		$this->db->where('access_people.exit_time', '0000-00-00 00:00:00');
		return $this->db->get('access_people')->result();
	}

}