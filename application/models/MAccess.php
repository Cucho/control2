<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MAccess extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function getAccessPeopleByState($access_state_id)
	{
		$this->db->select('access_people.id as id, rut, digit, people.name as name, last_name, people_profiles.profile, main_access.name as access, company, access_people.entry as entry, access_state.state as state, DATE_FORMAT(access_people.created, "%d-%m-%Y %H:%i:%s") as created');
		$this->db->from('access_people');
		$this->db->join('people','people.id=access_people.people_id');
		$this->db->join('people_profiles','people_profiles.id=people.people_profiles_id');
		$this->db->join('main_access','main_access.id=access_people.main_access_id');
		$this->db->join('companies','companies.id=people.companies_id');
		$this->db->join('access_state','access_state.id=access_people.access_state_id');
		$this->db->where('access_state.id', $access_state_id);
		$this->db->order_by('access_people.id','desc');

		return $this->db->get()->result_array();
	}

	//ACCESS PEOPLE
	public function getAccessPeople($access_people_id)
	{
		$this->db->select('access_people.id as id, rut, digit, people.name as name, last_name, people.phone as phone, people.email as email, people_profiles.profile as profile, main_access.name as access, company, access_people.entry as entry, hours, DATE_FORMAT(end_time, "%d-%m-%Y %H:%i:%s") as end_time, access_state.state as state, access_people.observation,  DATE_FORMAT(access_people.created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(access_people.modified, "%d-%m-%Y %H:%i:%s") as modified, vehicles.patent, vehicles.model, access_people.control_init, access_people.control_end');
		$this->db->from('access_people');
		$this->db->join('people','people.id=access_people.people_id');
		$this->db->join('people_profiles','people_profiles.id=people.people_profiles_id');
		$this->db->join('main_access','main_access.id=access_people.main_access_id');
		$this->db->join('companies','companies.id=people.companies_id');
		$this->db->join('access_state','access_state.id=access_people.access_state_id');
		$this->db->join('vehicles','vehicles.id = access_people.vehicles_id', 'left');
		$this->db->where('access_people.id', $access_people_id);
		$this->db->limit(1);

		return $this->db->get()->result_array();
	}

	public function getDetail_Access_People_Form_in($access_people_id){
		$this->db->select('title, observation, forms_id');
		$this->db->from('access_people_forms');
		$this->db->where('access_people_id', $access_people_id);
		$this->db->where('control', 0);
		return $this->db->get()->result();
	}

	public function SearchFormDetail($form){
		$this->db->select('order, question, placeholder, answers_type_id, measures_id, answers_type.id as atid, type, measures.id as mid, measure, acronimo');
		$this->db->join('answers_type', 'answers_type.id = answers_type_id');
		$this->db->join('measures', 'measures.id = measures_id', 'left');
		$this->db->from('forms_detail');
		$this->db->where('forms_id', $form);
		$this->db->order_by('order');
		return $this->db->get()->result();
	}

	public function getDetail_Access_People_Form_answers($access_people_id, $forms_id, $entry){
		$this->db->select('answer, order');
		$this->db->from('access_people_answers');
		$this->db->where('access_people_id', $access_people_id);
		$this->db->where('forms_id', $forms_id);
		$this->db->where('control', $entry);
		$this->db->order_by('order');
		return $this->db->get()->result();
	}

	public function getAccessPeopleZones($access_people_id)
	{
		$this->db->select('zone');
		$this->db->from('access_people_zones');
		$this->db->join('zones','zones.id=access_people_zones.zones_id');
		$this->db->where('access_people_id', $access_people_id);

		return $this->db->get()->result_array();
	}

	public function getAccessPeopleAreas($access_people_id)
	{
		$this->db->select('area');
		$this->db->from('access_people_areas');
		$this->db->join('areas','areas.id=access_people_areas.areas_id');
		$this->db->where('access_people_id', $access_people_id);

		return $this->db->get()->result_array();
	}

	public function getAccessPeopleDepartment($access_people_id)
	{
		$this->db->select('department');
		$this->db->from('access_people_department');
		$this->db->join('departments','departments.id=access_people_department.departments_id');
		$this->db->where('access_people_id', $access_people_id);

		return $this->db->get()->result_array();
	}

	public function getAccessPeopleReasons($access_people_id)
	{
		$this->db->select('reason');
		$this->db->from('access_people_reasons_visit');
		$this->db->join('reasons_visit','reasons_visit.id=access_people_reasons_visit.reasons_visit_id');
		$this->db->where('access_people_id', $access_people_id);

		return $this->db->get()->result_array();
	}

	public function getAccessPeopleVisit($access_people_id)
	{
		$this->db->select('rut, digit, name, last_name');
		$this->db->from('access_people_visit');
		$this->db->join('people','people.id=access_people_visit.people_id');
		$this->db->where('access_people_id', $access_people_id);

		return $this->db->get()->result_array();
	}

	public function getAccessPeopleRoute($access_people_id)
	{
		$this->db->select('door, description, level, doors_type_id, type');
		$this->db->from('access_people_route');
		$this->db->join('doors','doors.id=access_people_route.doors_id');
		$this->db->join('doors_type', 'doors_type.id=doors.doors_type_id');
		$this->db->where('access_people_id', $access_people_id);
		$this->db->order_by('level','asc');

		return $this->db->get()->result_array();
	}

	public function getAccessPeopleIntents($access_people_id)
	{
		$this->db->select('door, description, level, doors_type_id, type, DATE_FORMAT(access_people_intents.created, "%d-%m-%Y %H:%i:%s") as created');
		$this->db->from('access_people_intents');
		$this->db->join('doors','doors.id=access_people_intents.doors_id');
		$this->db->join('doors_type', 'doors_type.id=doors.doors_type_id');
		$this->db->where('access_people_id', $access_people_id);
		$this->db->order_by('created', 'desc');

		return $this->db->get()->result_array();
	}

	public function getAccessPeopleStateHistory($access_people_id)
	{
		$this->db->select('access_people_state_history.id, access_state_id, state, description, DATE_FORMAT(access_people_state_history.created, "%d-%m-%Y %H:%i:%s") as created');
		$this->db->from('access_people_state_history');
		$this->db->join('access_state','access_state.id=access_people_state_history.access_state_id');
		$this->db->where('access_people_id', $access_people_id);
		$this->db->order_by('id', 'asc');

		return $this->db->get()->result_array();
	}

	public function changeStateAccessPeople($access_people_id, $access_state_id, $observation, $date_time, $end_time, $approved_by)
	{
		$query = 'UPDATE access_people SET observation = CONCAT(observation, "'.$observation.'"), access_state_id = '.$access_state_id.', modified = "'.$date_time.'", end_time = "'.$end_time.'", approved_by = '.$approved_by.' WHERE id = '.$access_people_id;

		if($this->db->query($query))
			return true;
		else
			return false;
	}

	public function addStateHistory($data)
	{
		if($this->db->insert('access_people_state_history',$data))
			return true;
		else
			return false;
	}

	//Estado Actual Interior
	/*
	public function getAccessPeoplePermitidos()
	{
		$this->db->select('access_people.id as id, rut, digit, people.name as name, last_name, profile,company,  DATE_FORMAT(access_people_state_history.created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(end_time, "%d-%m-%Y %H:%i:%s" ) as end_time, access_people_state_history.created as created_, end_time as end_time_');
		$this->db->from('access_people');
		$this->db->join('people','people.id=access_people.people_id');
		$this->db->join('people_profiles','people_profiles.id=people.people_profiles_id');
		$this->db->join('companies','companies.id=people.companies_id');
		$this->db->join('access_state','access_state.id=access_people.access_state_id');
		$this->db->join('access_people_state_history','access_people_state_history.access_people_id=access_people.id');
		$this->db->where('access_people.access_state_id', 2);
		$this->db->where('access_people_state_history.access_state_id', 2);
		$this->db->where('access_people.entry', 0);
		$this->db->group_by('access_people.id');
		
		return $this->db->get()->result_array();
	}	

	public function getCountPeoplePermitidos()
	{
		$this->db->select('COUNT(access_people.id) as total_permitidos');
		$this->db->from('access_people');
		$this->db->join('people','people.id=access_people.people_id');
		$this->db->join('people_profiles','people_profiles.id=people.people_profiles_id');
		$this->db->join('companies','companies.id=people.companies_id');
		$this->db->join('access_state','access_state.id=access_people.access_state_id');
		$this->db->where('access_state_id', 2);
		$this->db->where('access_people.entry', 0);

		$res = $this->db->get()->result_array();

		if(!empty($res[0]['total_permitidos']))
			$res = $res[0]['total_permitidos'];
		else
			$res = 0;

		return $res;
	}
	*/
	public function getPeopleByAccessPeopleID($access_people_id)
	{
		$this->db->select('rut, digit, name, last_name, profile, company');
		$this->db->from('people');
		$this->db->join('access_people','access_people.people_id=people.id');
		$this->db->join('people_profiles','people_profiles.id=people.people_profiles_id');
		$this->db->join('companies','companies.id=people.companies_id');
		$this->db->where('access_people.id', $access_people_id);
		$this->db->limit(1);

		return $this->db->get()->result_array();
	}

	//------------------------------------------------------------------------

	public function getCountVisitPeopleNormal()
	{
		$this->db->select('COUNT(current_internal_state.id) as total');
		$this->db->from('current_internal_state');
		$this->db->where('end_time >= NOW()');
		$this->db->where('flow', 0);
		$this->db->where('internal', 0);
		$this->db->limit(1);

		$res = $this->db->get()->result_array();

		if(!empty($res[0]['total']))
			$res = $res[0]['total'];
		else
			$res = 0;

		return $res;
	}

	public function getCountVisitPeopleExc()
	{
		$this->db->select('COUNT(current_internal_state.id) as total');
		$this->db->from('current_internal_state');
		$this->db->where('end_time < NOW()');
		$this->db->where('flow', 0);
		$this->db->where('internal', 0);
		$this->db->limit(1);

		$res = $this->db->get()->result_array();

		if(!empty($res[0]['total']))
			$res = $res[0]['total'];
		else
			$res = 0;

		return $res;
	}

	public function getCountVisitVehiclesNormal()
	{
		$this->db->select('COUNT(current_internal_state.id) as total');
		$this->db->from('current_internal_state');
		$this->db->where('end_time >= NOW()');
		$this->db->where('flow', 1);
		$this->db->where('internal', 0);
		$this->db->limit(1);

		$res = $this->db->get()->result_array();

		if(!empty($res[0]['total']))
			$res = $res[0]['total'];
		else
			$res = 0;

		return $res;
	}

	public function getCountVisitVehiclesExc()
	{
		$this->db->select('COUNT(current_internal_state.id) as total');
		$this->db->from('current_internal_state');
		$this->db->where('end_time < NOW()');
		$this->db->where('flow', 1);
		$this->db->where('internal', 0);
		$this->db->limit(1);

		$res = $this->db->get()->result_array();

		if(!empty($res[0]['total']))
			$res = $res[0]['total'];
		else
			$res = 0;

		return $res;
	}

	public function getCountContractorPeopleNormal()
	{
		$this->db->select('COUNT(current_internal_state.id) as total');
		$this->db->from('current_internal_state');
		$this->db->where('end_time >= NOW()');
		$this->db->where('flow', 0);
		$this->db->where('internal', 2);
		$this->db->limit(1);

		$res = $this->db->get()->result_array();

		if(!empty($res[0]['total']))
			$res = $res[0]['total'];
		else
			$res = 0;

		return $res;
	}

	public function getCountContractorPeopleExc()
	{
		$this->db->select('COUNT(current_internal_state.id) as total');
		$this->db->from('current_internal_state');
		$this->db->where('end_time < NOW()');
		$this->db->where('flow', 0);
		$this->db->where('internal', 2);
		$this->db->limit(1);

		$res = $this->db->get()->result_array();

		if(!empty($res[0]['total']))
			$res = $res[0]['total'];
		else
			$res = 0;

		return $res;
	}

	public function getCountContractorVehiclesNormal()
	{
		$this->db->select('COUNT(current_internal_state.id) as total');
		$this->db->from('current_internal_state');
		$this->db->where('end_time >= NOW()');
		$this->db->where('flow', 1);
		$this->db->where('internal', 2);
		$this->db->limit(1);

		$res = $this->db->get()->result_array();

		if(!empty($res[0]['total']))
			$res = $res[0]['total'];
		else
			$res = 0;

		return $res;
	}

	public function getCountContractorVehiclesExc()
	{
		$this->db->select('COUNT(current_internal_state.id) as total');
		$this->db->from('current_internal_state');
		$this->db->where('end_time < NOW()');
		$this->db->where('flow', 1);
		$this->db->where('internal', 2);
		$this->db->limit(1);

		$res = $this->db->get()->result_array();

		if(!empty($res[0]['total']))
			$res = $res[0]['total'];
		else
			$res = 0;

		return $res;
	}

	public function getCountInternalPeopleNormal()
	{
		$this->db->select('COUNT(current_internal_state.id) as total');
		$this->db->from('current_internal_state');
		$this->db->where('end_time >= NOW()');
		$this->db->where('flow', 0);
		$this->db->where('internal', 1);
		$this->db->limit(1);

		$res = $this->db->get()->result_array();

		if(!empty($res[0]['total']))
			$res = $res[0]['total'];
		else
			$res = 0;

		return $res;
	}

	public function getCountInternalPeopleExc()
	{
		$this->db->select('COUNT(current_internal_state.id) as total');
		$this->db->from('current_internal_state');
		$this->db->where('end_time < NOW()');
		$this->db->where('flow', 0);
		$this->db->where('internal', 1);
		$this->db->limit(1);

		$res = $this->db->get()->result_array();

		if(!empty($res[0]['total']))
			$res = $res[0]['total'];
		else
			$res = 0;

		return $res;
	}

	public function getCountInternalVehiclesNormal()
	{
		$this->db->select('COUNT(current_internal_state.id) as total');
		$this->db->from('current_internal_state');
		$this->db->where('end_time >= NOW()');
		$this->db->where('flow', 1);
		$this->db->where('internal', 1);
		$this->db->limit(1);

		$res = $this->db->get()->result_array();

		if(!empty($res[0]['total']))
			$res = $res[0]['total'];
		else
			$res = 0;

		return $res;
	}

	public function getCountInternalVehiclesExc()
	{
		$this->db->select('COUNT(current_internal_state.id) as total');
		$this->db->from('current_internal_state');
		$this->db->where('end_time < NOW()');
		$this->db->where('flow', 1);
		$this->db->where('internal', 1);
		$this->db->limit(1);

		$res = $this->db->get()->result_array();

		if(!empty($res[0]['total']))
			$res = $res[0]['total'];
		else
			$res = 0;

		return $res;
	}

	//------------------------------------------------------------------------
	public function getListAccessIn()
	{
		$this->db->select('current_internal_state.id as id, current_internal_state.flow as flow, current_internal_state.internal as internal, people.rut as rut, people.digit as digit, people.name as name, people.last_name as last_name, companies.company as company, vehicles.patent as patent, doors.door as door, current_internal_state.action as action, DATE_FORMAT(current_internal_state.end_time, "%d-%m-%Y %H:%i:%s") as end_time, DATE_FORMAT(current_internal_state.created, "%d-%m-%Y %H:%i:%s") as created, people_profiles.profile as profile');
		$this->db->from('current_internal_state');
		$this->db->join('people', 'people.id = current_internal_state.people_id');
		$this->db->join('people_profiles', 'people_profiles.id = people.people_profiles_id ');
		$this->db->join('companies', 'companies.id = people.companies_id');
		$this->db->join('doors', 'doors.id = current_internal_state.doors_id');
		$this->db->join('vehicles', 'vehicles.id = current_internal_state.vehicles_id', 'left');
		$this->db->order_by('current_internal_state.id');
		return $this->db->get()->result_array();
	}

	//30-10-2018
	public function getPeopleID_By_Rut($rut)
	{
		$this->db->select('id');
		$this->db->from('people');
		$this->db->where('rut', $rut);
		$this->db->limit(1);
		$res = $this->db->get()->result_array();

		if(!empty($res[0]['id']))
			return $res[0]['id'];
		else
			return 0;
	}

	public function getDoors_ID_By_Ip($ip)
	{
		$this->db->select('doors_id');
		$this->db->from('main_access');
		$this->db->where('ip_host', $ip);
		$this->db->limit(1);
		$res = $this->db->get()->result_array();

		if(!empty($res[0]['doors_id']))
			return $res[0]['doors_id'];
		else
			return 0;
	}

	//visitas---------

	public function getLastAccessPeople($people_id)
	{
		$this->db->select('id');
		$this->db->from('access_people');
		$this->db->where('people_id',$people_id);
		//$this->db->where('entry', 0);
		$this->db->order_by('created','desc');
		$this->db->limit(1);
		$res = $this->db->get()->result_array();

		if(!empty($res[0]['id']))
			return $res[0]['id'];
		else
			return 0;

	}

	public function getFirtsDoorAccess($access_people_id)
	{
		$this->db->select('doors_id');
		$this->db->from('access_people_intents');
		$this->db->where('access_people_id', $access_people_id);
		$this->db->where('entry', 0);
		$this->db->order_by('created', 'asc');
		$this->db->limit(1);
		$res = $this->db->get()->result_array();

		if(!empty($res[0]['doors_id']))
			return $res[0]['doors_id'];
		else
			return 0;

	}

	//----------------

	//internos ---------

	public function getFirtsDoorAccessInternal($people_id)
	{
		$this->db->select('doors_id');
		$this->db->from('internal_people');
		$this->db->where('people_id', $people_id);
		$this->db->where('entry', 0);
		$this->db->where('success', 1);
		$this->db->order_by('created', 'desc');
		$this->db->limit(1);
		$res = $this->db->get()->result_array();

		if(!empty($res[0]['doors_id']))
			return $res[0]['doors_id'];
		else
			return 0;
	}

	//------------------

	//contratistas ---------

	public function getLastProjectID_By_People($people_id, $date_time)
	{
		$dt = new DateTime($date_time);
		$date_now = $dt->format('Y-m-d');

		$this->db->select('id');
		$this->db->from('projects');
		$this->db->join('projects_people','projects_people.projects_id=projects.id');
		$this->db->where('people_id', $people_id);
		$this->db->where('init <= "'.$date_now.'"');
		$this->db->where('end >= "'.$date_now.'"');
		$this->db->order_by('id', 'desc');
		$this->db->limit(1);
		$res = $this->db->get()->result_array();

		if(!empty($res[0]['id']))
			return $res[0]['id'];
		else
			return 0;
	}

	//------------------

	public function save_logbook($data)
	{
		if($this->db->insert('logbook', $data))
			return true;
		else
			return false;
	}

	public function getInfoPeople($people_id)
	{
		$this->db->select('people.rut as rut, people.digit as digit, people.name as name, people.last_name as last_name, people.internal as internal, people_profiles.profile as profile, companies.company as company');
		$this->db->from('people');
		$this->db->join('people_profiles', 'people_profiles.id = people.people_profiles_id');
		$this->db->join('companies', 'companies.id = people.companies_id');
		$this->db->where('people.id', $people_id);
		$this->db->limit(1);
		return $this->db->get()->result_array();
	}
}