<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MAnswers_Type extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function getAnswers_Type($start, $length, $search, $order, $by) {
		$retornar = array();
		if ($search) {
			$busca = $this->getSearchAnswers_Type($search, $start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCountSearch($search, $start, $length, $order, $by);
			$retornar['data'] = $busca['datos'];
		}
		else {
			$todo = $this->getAllAnswers_Type($start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCount();
			$retornar['data'] = $todo['datos'];
		}

		$retornar['numDataTotal'] = $this->getCount();

		return $retornar;
	}

	public function getAnswer_Type($id) {
		$this->db->select('id, type, DATE_FORMAT(created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(modified, "%d-%m-%Y %H:%i:%s") as modified');
		$this->db->from('answers_type');
		$this->db->where('id', $id);
		$this->db->limit(1);

		return $this->db->get()->result_array();
	}

	// Funciones auxiliares datatable
	public function getAllAnswers_Type($start, $length, $order, $by) {
		$this->db->select('id, type, DATE_FORMAT(created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(modified, "%d-%m-%Y %H:%i:%s") as modified');
		if ($by == 0) {
			$this->db->order_by('id', $order);
		}
		elseif($by == 1) {
			$this->db->order_by('type', $order);
		}
		$this->db->limit($length, $start);
		$query = $this->db->get('answers_type');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getSearchAnswers_Type($search, $start, $length, $order, $by) {
		$this->db->select('id, type, DATE_FORMAT(created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(modified, "%d-%m-%Y %H:%i:%s") as modified');
		$this->db->like('id', $search);
		$this->db->or_like('type', $search);
		if ($by == 0) {
			$this->db->order_by('id', $order);
		}
		elseif($by == 1) {
			$this->db->order_by('type', $order);
		}
		$this->db->limit($length, $start);
		$query = $this->db->get('answers_type');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getCount() {
		return $this->db->count_all('answers_type');
	}

	public function getCountSearch($search, $start, $length, $order, $by) {
		$this->db->select('id');
		$this->db->like('id', $search);
		$this->db->or_like('type', $search);
		return $this->db->get('answers_type')->num_rows();
	}
	// fin funciones auxiliares

	//Crud
	public function addAnswer_Type($data) {
		if($this->db->insert('answers_type', $data))
			return true;
		else
			return false;
	}

	public function editAnswer_Type($data, $id) {
		$this->db->where('id', $id);
		if($this->db->update('answers_type', $data))
			return true;
		else
			return false;
	}

	public function deleteAnswer_Type($id) {
		$this->db->where('id', $id);
		if($this->db->delete('answers_type'))
			return true;
		else
			return false;
	}
}