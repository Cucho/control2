<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MType_Logbook extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	
	public function getType_Logbook($start, $length, $search, $order, $by) {
		$retornar = array();
		if ($search) {
			$busca = $this->getSearchType_Logbook($search, $start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCountSearch($search, $start, $length, $order, $by);
			$retornar['data'] = $busca['datos'];
		}
		else {
			$todo = $this->getAllType_Logbook($start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCount();
			$retornar['data'] = $todo['datos'];
		}

		$retornar['numDataTotal'] = $this->getCount();

		return $retornar;
	}

	public function getType_Logbook_($id) {
		$this->db->select('id,type, DATE_FORMAT(created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(modified, "%d-%m-%Y %H:%i:%s") as modified');
		$this->db->from('type_logbook');
		$this->db->where('id', $id);
		$this->db->limit(1);

		return $this->db->get()->result_array();
	}

	// Funciones auxiliares datatable
	public function getAllType_Logbook($start, $length, $order, $by) {
		$this->db->select('id, type, DATE_FORMAT(created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(modified, "%d-%m-%Y %H:%i:%s") as modified');
		if ($by == 0) {
			$this->db->order_by('id', $order);
		}
		elseif($by == 1) {
			$this->db->order_by('type', $order);
		}
		$this->db->limit($length, $start);
		$query = $this->db->get('type_logbook');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getSearchType_Logbook($search, $start, $length, $order, $by) {
		$this->db->select('id, type, DATE_FORMAT(created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(modified, "%d-%m-%Y %H:%i:%s") as modified');
		$this->db->like('id', $search);
		$this->db->or_like('type', $search);
		if ($by == 0) {
			$this->db->order_by('id', $order);
		}
		elseif($by == 1) {
			$this->db->order_by('type', $order);
		}
		$this->db->limit($length, $start);
		$query = $this->db->get('type_logbook');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getCount() {
		return $this->db->count_all('type_logbook');
	}

	public function getCountSearch($search, $start, $length, $order, $by) {
		$this->db->select('id');
		$this->db->like('id', $search);
		$this->db->or_like('type', $search);
		$quer = $this->db->get('type_logbook')->num_rows();
		return $quer;
	}
	// fin funciones auxiliares
	
	//Crud
	public function addType_Logbook($data) {
		if($this->db->insert('type_logbook', $data))
			return true;
		else
			return false;
	}

	public function editType_Logbook($data, $id) {
		$this->db->where('id', $id);
		if($this->db->update('type_logbook', $data))
			return true;
		else
			return false;
	}

	public function deleteType_Logbook($id) {
		$this->db->where('id', $id);
		if($this->db->delete('type_logbook'))
			return true;
		else
			return false;
	}

}