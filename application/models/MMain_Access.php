<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MMain_Access extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function getMain_Access($start, $length, $search, $order, $by) {
		$retornar = array();
		if ($search) {
			$busca = $this->getSearchMain_Access($search, $start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCountSearch($search, $start, $length, $order, $by);
			$retornar['data'] = $busca['datos'];
		}
		else {
			$todo = $this->getAllMain_Access($start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCount();
			$retornar['data'] = $todo['datos'];
		}

		$retornar['numDataTotal'] = $this->getCount();

		return $retornar;
	}

	public function getMain_AccessId($id) {
		$this->db->select('id, name, ubication, ip_host, name_host, entry, flow, internal, state, pop_up, doors_id, main, DATE_FORMAT(created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(modified, "%d-%m-%Y %H:%i:%s") as modified');
		$this->db->from('main_access');
		$this->db->where('id', $id);
		$this->db->limit(1);

		return $this->db->get()->result_array();
	}

	// Funciones auxiliares datatable
	public function getAllMain_Access($start, $length, $order, $by) {
		$this->db->select('main_access.id, name, ubication, ip_host, name_host, entry, flow, internal, state, pop_up, doors_id, door, main, DATE_FORMAT(main_access.created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(main_access.modified, "%d-%m-%Y %H:%i:%s") as modified');
		$this->db->join('doors', 'doors.id = doors_id');
		switch ($by) {
			case 0:
				$this->db->order_by('id', $order);
				break;
			case 1:
				$this->db->order_by('name', $order);
				break;
			case 2:
				$this->db->order_by('ubication', $order);
				break;
			case 3:
				$this->db->order_by('ip_host', $order);
				break;
			case 4:
				$this->db->order_by('name_host', $order);
				break;
			case 5:
				$this->db->order_by('entry', $order);
				break;
			case 6:
				$this->db->order_by('flow', $order);
				break;
			case 7:
				$this->db->order_by('internal', $order);
				break;
			case 8:
				$this->db->order_by('state', $order);
				break;
		}
		$this->db->limit($length, $start);
		$query = $this->db->get('main_access');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getSearchMain_Access($search, $start, $length, $order, $by) {
		$this->db->select('id, name, ubication, ip_host, name_host, entry, flow, internal, state, pop_up, DATE_FORMAT(created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(modified, "%d-%m-%Y %H:%i:%s") as modified');
		$this->db->like('id', $search);
		$this->db->or_like('name', $search);
		$this->db->or_like('ubication', $search);
		$this->db->or_like('ip_host', $search);
		$this->db->or_like('name_host', $search);
		$this->db->or_like('entry', $search);
		$this->db->or_like('flow', $search);
		$this->db->or_like('internal', $search);
		$this->db->or_like('state', $search);
		switch ($by) {
			case 0:
				$this->db->order_by('id', $order);
				break;
			case 1:
				$this->db->order_by('name', $order);
				break;
			case 2:
				$this->db->order_by('ubication', $order);
				break;
			case 3:
				$this->db->order_by('ip_host', $order);
				break;
			case 4:
				$this->db->order_by('name_host', $order);
				break;
			case 5:
				$this->db->order_by('entry', $order);
				break;
			case 6:
				$this->db->order_by('flow', $order);
				break;
			case 7:
				$this->db->order_by('internal', $order);
				break;
			case 8:
				$this->db->order_by('state', $order);
				break;
		}
		$this->db->limit($length, $start);
		$query = $this->db->get('main_access');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getCount() {
		return $this->db->count_all('main_access');
	}

	public function getCountSearch($search, $start, $length, $order, $by) {
		$this->db->select('id');
		$this->db->like('id', $search);
		$this->db->or_like('name', $search);
		$this->db->or_like('ubication', $search);
		$this->db->or_like('ip_host', $search);
		$this->db->or_like('name_host', $search);
		$this->db->or_like('entry', $search);
		$this->db->or_like('flow', $search);
		$this->db->or_like('internal', $search);
		$this->db->or_like('state', $search);
		$quer = $this->db->get('main_access')->num_rows();
		return $quer;
	}
	// fin funciones auxiliares

	//Crud
	public function addMain_Access($data) {
		if($this->db->insert('main_access', $data))
			return true;
		else
			return false;
	}

	public function editMain_Access($data, $id) {
		$this->db->where('id', $id);
		if($this->db->update('main_access', $data))
			return true;
		else
			return false;
	}

	public function deleteMain_Access($id) {
		$this->db->where('id', $id);
		if($this->db->delete('main_access'))
			return true;
		else
			return false;
	}

	public function getAllDoors(){
		$this->db->select('doors.id, door, description, level, type');
		$this->db->from('doors');
		$this->db->join('doors_type', 'doors_type.id = doors.doors_type_id');
		return $this->db->get()->result();
	}
}
