<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MProfiles_Doors_Schedules extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	//Crud
	public function addProfile_Door_Schedule($profiles_people_id, $jornada, $time_init, $time_end, $doors_id, $door) {
		$this->db->where('profiles_people_id', $profiles_people_id);
		$this->db->where('jornada_id', $jornada);
		$this->db->delete('profiles_doors_schedules');

		$mensaje = false;
		$j = 0;
		for ($i=0; $i < count($doors_id); $i++) { 
			$this->db->set('profiles_people_id', $profiles_people_id);
			$this->db->set('doors_id', $doors_id[$i]);
			$this->db->set('time_init', $time_init);
			$this->db->set('time_end', $time_end);
			$this->db->set('jornada_id', $jornada);

			if ($doors_id[$i] == $door[$j]['puerta']) {
				$this->db->set('L', $door[$j]['estado']);
				$this->db->set('M', $door[$j+1]['estado']);
				$this->db->set('Mi', $door[$j+2]['estado']);
				$this->db->set('J', $door[$j+3]['estado']);
				$this->db->set('V', $door[$j+4]['estado']);
				$this->db->set('S', $door[$j+5]['estado']);
				$this->db->set('D', $door[$j+6]['estado']);
			}
			$j += 7;
			
			if($this->db->insert('profiles_doors_schedules')) {
				$mensaje = true;
			}
		}
		return $mensaje;
	}

	public function getAllPeople_Profiles() {
		$this->db->select('id, profile');
		$this->db->from('people_profiles');
		$this->db->order_by('profile');

		return $this->db->get()->result();
	}

	public function getAllDoors() {
		$this->db->select('id, door');
		$this->db->from('doors');
		$this->db->order_by('door');

		return $this->db->get()->result();
	}

	public function getAllProfiles_Doors_Schedules() {
		$this->db->select('profiles_people_id, doors_id, time_init, time_end, jornada_id, L as D1, M as D2, Mi as D3, J as D4, V as D5, S as D6, D as D7');
		$this->db->from('profiles_doors_schedules');
		$this->db->order_by('profiles_people_id');

		return $this->db->get()->result();
	}

	public function getAllJornadas() {
		$this->db->select('id, jornada, time_init, time_end');
		$this->db->from('jornada');
		$this->db->order_by('id');

		return $this->db->get()->result();
	}
}