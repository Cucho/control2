<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MSeasons extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function getSeasons($start, $length, $search, $order, $by) {
		$retornar = array();
		if ($search) {
			$busca = $this->getSearchSeasons($search, $start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCountSearch($search, $start, $length, $order, $by);
			$retornar['data'] = $busca['datos'];
		}
		else {
			$todo = $this->getAllSeasons($start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCount();
			$retornar['data'] = $todo['datos'];
		}

		$retornar['numDataTotal'] = $this->getCount();

		return $retornar;
	}

	public function getSeason($id) {
		$this->db->select('id, season, DATE_FORMAT(created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(modified, "%d-%m-%Y %H:%i:%s") as modified');
		$this->db->from('seasons');
		$this->db->where('id', $id);
		$this->db->limit(1);

		return $this->db->get()->result_array();
	}

	// Funciones auxiliares datatable
	public function getAllSeasons($start, $length, $order, $by) {
		$this->db->select('id, season, DATE_FORMAT(created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(modified, "%d-%m-%Y %H:%i:%s") as modified');
		if ($by == 0) {
			$this->db->order_by('id', $order);
		}
		else {
			$this->db->order_by('season', $order);
		}
		$this->db->limit($length, $start);
		$query = $this->db->get('seasons');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getSearchSeasons($search, $start, $length, $order, $by) {
		$this->db->select('id, season, DATE_FORMAT(created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(modified, "%d-%m-%Y %H:%i:%s") as modified');
		$this->db->like('id', $search);
		$this->db->or_like('season', $search);
		if ($by == 0) {
			$this->db->order_by('id', $order);
		}
		else {
			$this->db->order_by('season', $order);
		}
		$this->db->limit($length, $start);
		$query = $this->db->get('seasons');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getCount() {
		return $this->db->count_all('seasons');
	}

	public function getCountSearch($search, $start, $length, $order, $by) {
		$this->db->select('id');
		$this->db->or_like('season', $search);
		$quer = $this->db->get('seasons')->num_rows();
		return $quer;
	}
	// fin funciones auxiliares

	//Crud
	public function addSeason($data) {
		if($this->db->insert('seasons', $data))
			return true;
		else
			return false;
	}

	public function editSeason($data, $id) {
		$this->db->where('id', $id);
		if($this->db->update('seasons', $data))
			return true;
		else
			return false;
	}

	public function deleteSeason($id) {
		$this->db->where('id', $id);
		if($this->db->delete('seasons'))
			return true;
		else
			return false;
	}
}