<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MOptions_Roles extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function getOptions_Roles($start, $length, $search, $order, $by) {
		$retornar = array();
		if ($search) {
			$busca = $this->getSearchOptions_Roles($search, $start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCountSearch($search, $start, $length, $order, $by);
			$retornar['data'] = $busca['datos'];
		}
		else {
			$todo = $this->getAllOptions_Roles($start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCount();
			$retornar['data'] = $todo['datos'];
		}

		$retornar['numDataTotal'] = $this->getCount();

		return $retornar;
	}

	// Funciones auxiliares datatable
	public function getAllOptions_Roles($start, $length, $order, $by) {
		$this->db->select('rol, option, roles_id, options_id');
		$this->db->join('roles','roles.id = options_roles.roles_id');
		$this->db->join('options','options.id = options_roles.options_id');
		if ($by == 0) {
			$this->db->order_by('option', $order);
		}
		else {
			$this->db->order_by('rol', $order);
		}
		$this->db->limit($length, $start);
		$query = $this->db->get('options_roles');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getSearchOptions_Roles($search, $start, $length, $order, $by) {
		$this->db->select('rol, option, roles_id, options_id');
		$this->db->like('option', $search);
		$this->db->or_like('rol', $search);
		if ($by == 0) {
			$this->db->order_by('option', $order);
		}
		else {
			$this->db->order_by('rol', $order);
		}
		$this->db->join('roles','roles.id = options_roles.roles_id');
		$this->db->join('options','options.id = options_roles.options_id');
		$this->db->limit($length, $start);
		$query = $this->db->get('options_roles');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getCount() {
		return $this->db->count_all('options_roles');
	}

	public function getCountSearch($search, $start, $length, $order, $by) {
		$this->db->select('rol');
		$this->db->like('option', $search);
		$this->db->or_like('rol', $search);
		$this->db->join('roles','roles.id = options_roles.roles_id');
		$this->db->join('options','options.id = options_roles.options_id');
		$quer = $this->db->get('options_roles')->num_rows();
		return $quer;
	}
	// fin funciones auxiliares

	//Crud
	public function addOption_Rol($data, $id) {
		$bandera = false;
		$this->db->where('roles_id', $id);
		$this->db->delete('options_roles');
		for ($i=0; $i < count($data); $i++) { 
			$this->db->set('roles_id', $id);
			$this->db->set('options_id', $data[$i]);
			if($this->db->insert('options_roles')) {
				$bandera = true;
			}
		}
		return $bandera;
	}

	public function deleteOption_Rol($option, $rol) {
		$this->db->where('options_id', $option);
		$this->db->where('roles_id', $rol);
		if($this->db->delete('options_roles'))
			return true;
		else
			return false;
	}

	public function getAllOptions() {
		$this->db->select('id, option, code');
		$this->db->from('options');
		$this->db->order_by('code');

		return $this->db->get()->result_array();
	}

	public function getAllRoles() {
		$this->db->select('id, rol');
		$this->db->from('roles');
		$this->db->order_by('rol');

		return $this->db->get()->result_array();
	}

	public function AllOptions_Roles() {
		$this->db->select('options_id, roles_id');
		$this->db->from('options_roles');
		$this->db->order_by('roles_id');

		return $this->db->get()->result_array();
	}
}