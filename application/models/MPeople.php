<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MPeople extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function getPeoples($start, $length, $search, $order, $by) {
		$retornar = array();
		if ($search) {
			$busca = $this->getSearchPeople($search, $start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCountSearch($search, $start, $length, $order, $by);
			$retornar['data'] = $busca['datos'];
		}
		else {
			$todo = $this->getAllPeoples($start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCount();
			$retornar['data'] = $todo['datos'];
		}

		$retornar['numDataTotal'] = $this->getCount();

		return $retornar;
	}

	public function getPeople($id) {
		$this->db->select('people.id, rut, digit, name, last_name, people.address, people.email, people.phone, allow_all, is_visited, people.internal as internal, people_profiles_id, companies_id, DATE_FORMAT(people.created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(people.modified, "%d-%m-%Y %H:%i:%s") as modified, companies.company, people_profiles.profile, departments_id, nfc_code, state, states_id');
		$this->db->join('companies','companies.id = people.companies_id');
		$this->db->join('people_profiles','people_profiles.id = people.people_profiles_id');
		$this->db->join('states','states.id = people.states_id');
		$this->db->from('people');
		$this->db->where('people.id', $id);
		$this->db->limit(1);

		return $this->db->get()->result_array();
	}

	// Funciones auxiliares datatable
	public function getAllPeoples($start, $length, $order, $by) {
		$this->db->select('people.id, rut, digit, name, last_name, people.address, people.email, people.phone, allow_all, is_visited, people.internal as internal, people_profiles_id, companies_id, DATE_FORMAT(people.created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(people.modified, "%d-%m-%Y %H:%i:%s") as modified, companies.company, people_profiles.profile, departments_id, state, states_id');
		$this->db->join('companies','companies.id = people.companies_id');
		$this->db->join('states','states.id = people.states_id');
		$this->db->join('people_profiles','people_profiles.id = people.people_profiles_id');
		switch ($by) {
			case 0:
				$this->db->order_by('people.id', $order);
				break;
			case 1:
				$this->db->order_by('people.rut', $order);
				break;
			case 2:
				$this->db->order_by('name', $order);
				break;
			case 3:
				$this->db->order_by('last_name', $order);
				break;
			case 4:
				$this->db->order_by('people.address', $order);
				break;
			case 5:
				$this->db->order_by('people.email', $order);
				break;
			case 6:
				$this->db->order_by('people.phone', $order);
				break;
			case 7:
				$this->db->order_by('allow_all', $order);
				break;
			case 8:
				$this->db->order_by('is_visited', $order);
				break;
			case 9:
				$this->db->order_by('people.internal', $order);
				break;
			case 10:
				$this->db->order_by('people_profiles_id', $order);
				break;
			case 11:
				$this->db->order_by('companies_id', $order);
				break;
		}
		$this->db->limit($length, $start);
		$query = $this->db->get('people');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getSearchPeople($search, $start, $length, $order, $by) {
		$this->db->select('people.id, rut, digit, name, last_name, people.address, people.email, people.phone, allow_all, is_visited, people.internal as internal, people_profiles_id, companies_id, DATE_FORMAT(people.created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(people.modified, "%d-%m-%Y %H:%i:%s") as modified, companies.company, people_profiles.profile, departments_id, state, states_id');
		$this->db->like('people.id', $search);
		$this->db->or_like('rut', $search);
		$this->db->or_like('name', $search);
		$this->db->or_like('last_name', $search);
		$this->db->or_like('people.address', $search);
		$this->db->or_like('people.email', $search);
		$this->db->or_like('people.phone', $search);
		$this->db->or_like('allow_all', $search);
		$this->db->or_like('is_visited', $search);
		$this->db->or_like('people_profiles_id', $search);
		$this->db->or_like('companies_id', $search);
		switch ($by) {
			case 0:
				$this->db->order_by('people.id', $order);
				break;
			case 1:
				$this->db->order_by('people.rut', $order);
				break;
			case 2:
				$this->db->order_by('name', $order);
				break;
			case 3:
				$this->db->order_by('last_name', $order);
				break;
			case 4:
				$this->db->order_by('people.address', $order);
				break;
			case 5:
				$this->db->order_by('people.email', $order);
				break;
			case 6:
				$this->db->order_by('people.phone', $order);
				break;
			case 7:
				$this->db->order_by('allow_all', $order);
				break;
			case 8:
				$this->db->order_by('is_visited', $order);
				break;
			case 9:
				$this->db->order_by('people.internal', $order);
				break;
			case 10:
				$this->db->order_by('people_profiles_id', $order);
				break;
			case 11:
				$this->db->order_by('companies_id', $order);
				break;
		}
		$this->db->join('states','states.id = people.states_id');
		$this->db->join('companies','companies.id = people.companies_id');
		$this->db->join('people_profiles','people_profiles.id = people.people_profiles_id');
		$this->db->limit($length, $start);
		$query = $this->db->get('people');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getCount() {
		return $this->db->count_all('people');
	}

	public function getCountSearch($search, $start, $length, $order, $by) {
		$this->db->select('people.id');
		$this->db->like('people.id', $search);
		$this->db->or_like('rut', $search);
		$this->db->or_like('name', $search);
		$this->db->or_like('last_name', $search);
		$this->db->or_like('people.address', $search);
		$this->db->or_like('people.email', $search);
		$this->db->or_like('people.phone', $search);
		$this->db->or_like('allow_all', $search);
		$this->db->or_like('is_visited', $search);
		$this->db->or_like('people.internal', $search);
		$this->db->or_like('people_profiles_id', $search);
		$this->db->or_like('companies_id', $search);

		$this->db->join('states','states.id = people.states_id');
		$this->db->join('companies','companies.id = people.companies_id');
		$this->db->join('people_profiles','people_profiles.id = people.people_profiles_id');
		$quer = $this->db->get('people')->num_rows();
		return $quer;
	}
	// fin funciones auxiliares

	//Crud
	public function addPeople($data) {
		if($this->db->insert('people', $data))
			return true;
		else
			return false;
	}

	public function editPeople($data, $id) {
		$this->db->where('id', $id);
		if($this->db->update('people', $data))
			return true;
		else
			return false;
	}

	public function deletePeople($id) {
		$this->db->where('id', $id);
		if($this->db->delete('people'))
			return true;
		else
			return false;
	}
	//Functions add door zone

	public function getAllCompanies() {
		$this->db->select('id, company');
		$this->db->from('companies');
		$this->db->order_by('company');

		return $this->db->get()->result();
	}

	public function getAllPeople_Profile() {
		$this->db->select('id, profile');
		$this->db->from('people_profiles');
		$this->db->order_by('profile');

		return $this->db->get()->result();
	}

	public function getAllDepartments(){
		$this->db->select('id, department');
		$this->db->from('departments');
		$this->db->order_by('department');

		return $this->db->get()->result();
	}

	public function getAllStates(){
		$this->db->select('id, state');
		$this->db->from('states');
		$this->db->order_by('state');

		return $this->db->get()->result();
	}

}