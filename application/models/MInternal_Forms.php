<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MInternal_Forms extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function searchPeople($rut) {
		$this->db->select('people.id as people_id, people.internal, vehicles_drivers.vehicles_id, vehicles.patent, vehicles_type.type');
		$this->db->from('people');
		$this->db->join('vehicles_drivers', 'vehicles_drivers.people_id = people.id');
		$this->db->join('vehicles', 'vehicles.id = vehicles_drivers.vehicles_id');
		$this->db->join('vehicles_type', 'vehicles_type.id = vehicles_drivers.vehicles_id');
		//$this->db->where('people.internal', 0);
		$this->db->where('people.rut', $rut);

		return $this->db->get()->result();
	}

	public function getForms($id){
		$this->db->select('forms.id as forms_id, forms.title, forms.description, forms_detail.order, question, placeholder, measures.measure, measures.acronimo, answers_type.id as ans');
		$this->db->from('forms');
		$this->db->join('forms_detail', 'forms_detail.forms_id = forms.id');
		$this->db->join('answers_type', 'answers_type.id = forms_detail.answers_type_id');
		$this->db->join('measures', 'measures.id = forms_detail.measures_id', 'left');
		$this->db->where('forms.id', $id);
		$this->db->order_by('forms_detail.order');
		return $this->db->get()->result();
	}

	public function searchForm(){
		$this->db->select('forms.id as forms_id, forms.title, forms.description');
		$this->db->from('forms');
		return $this->db->get()->result();
	}

	public function searchVehicle($project){
		$this->db->select('projects_vehicles.vehicles_id, vehicles.patent, vehicles_type.type');
		$this->db->from('projects_vehicles');
		$this->db->join('vehicles', 'vehicles.id = projects_vehicles.vehicles_id');
		$this->db->join('vehicles_type', 'vehicles_type.id = vehicles.vehicles_type_id');
		$this->db->where('projects_vehicles.projects_id', $project);

		return $this->db->get()->result();
	}

	public function addInternalForm($data){
		if($this->db->insert('internal_forms', $data))
			return $this->db->insert_id();
		else
			return false;
	}

	public function addInternalAnswers($data){
		if($this->db->insert('internal_answers', $data))
			return true;
		else
			return false;
	}

	public function getControlIn($start, $length, $search, $order, $by, $control){
		$retornar = array();
		if ($search) {
			$busca = $this->getSearchControl($search, $start, $length, $order, $by, $control);
			$retornar['numDataFilter'] = $this->getCountSearch($search, $start, $length, $order, $by, $control);
			$retornar['data'] = $busca['datos'];
		}
		else {
			$todo = $this->getAllControl($start, $length, $order, $by, $control);
			$retornar['numDataFilter'] = $this->getCount($control);
			$retornar['data'] = $todo['datos'];
		}

		$retornar['numDataTotal'] = $this->getCount($control);

		return $retornar;
	}

	public function getAllControl($start, $length, $order, $by, $control) {
		$this->db->select('internal_forms.id, internal_forms.title, people.rut, people.digit, people.name, people.last_name, companies.company, vehicles.patent, DATE_FORMAT(internal_forms.created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(internal_forms.modified, "%d-%m-%Y %H:%i:%s") as modified');
		$this->db->join('vehicles', 'vehicles.id = internal_forms.vehicle_id');
		$this->db->join('people', 'people.id = internal_forms.people_id');
		$this->db->join('companies', 'companies.id = people.companies_id');
		$this->db->join('internal_answers', 'internal_answers.control_id = internal_forms.id');

		if ($by == 0) {
			$this->db->order_by('title', $order);
		}
		elseif($by == 1) {
			$this->db->order_by('rut', $order);
		}
		elseif ($by == 2) {
		 	$this->db->order_by('patent', $order);
		}
		elseif ($by == 3) {
			$this->db->order_by('created', $order);
		}
		elseif ($by == 4) {
			$this->db->order_by('modified', $order);
		}

		$this->db->where('internal_answers.control', $control);
		$this->db->where('internal_answers.order', 1);
		$this->db->limit($length, $start);
		$query = $this->db->get('internal_forms');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getSearchControl($search, $start, $length, $order, $by, $control) {
		$this->db->distinct();
		$this->db->select('internal_forms.id, internal_forms.title, people.rut, people.digit, people.name, people.last_name, companies.company, vehicles.patent, DATE_FORMAT(internal_forms.created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(internal_forms.modified, "%d-%m-%Y %H:%i:%s") as modified');

		$this->db->join('vehicles', 'vehicles.id = internal_forms.vehicle_id');
		$this->db->join('people', 'people.id = internal_forms.people_id');
		$this->db->join('companies', 'companies.id = people.companies_id');
		$this->db->join('internal_answers', 'internal_answers.control_id = internal_forms.id');

		$this->db->where('internal_answers.control', $control);
		$this->db->where('internal_answers.order', 1);

		$this->db->like('internal_forms.title', $search);
		$this->db->or_like('people.rut', $search);
		$this->db->or_like('patent', $search);
		$this->db->or_like('internal_forms.created', $search);
		$this->db->or_like('internal_forms.modified', $search);

		if ($by == 0) {
			$this->db->order_by('title', $order);
		}
		elseif($by == 1) {
			$this->db->order_by('rut', $order);
		}
		elseif ($by == 2) {
		 	$this->db->order_by('patent', $order);
		}
		elseif ($by == 3) {
			$this->db->order_by('created', $order);
		}
		elseif ($by == 4) {
			$this->db->order_by('modified', $order);
		}
		
		$this->db->limit($length, $start);
		
		$query = $this->db->get('internal_forms');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getCount($control) {
		$this->db->select('internal_answers.control_id');
		$this->db->where('internal_answers.control', $control);
		$this->db->distinct('control_id');
		return $this->db->count_all_results('internal_answers');
	}

	public function getCountSearch($search, $start, $length, $order, $by, $control) {
		$this->db->select('internal_forms.id');

		$this->db->join('vehicles', 'vehicles.id = internal_forms.vehicle_id');
		$this->db->join('people', 'people.id = internal_forms.people_id');
		$this->db->join('companies', 'companies.id = people.companies_id');
		$this->db->join('internal_answers', 'internal_answers.control_id = internal_forms.id');
		$this->db->join('projects_answers', 'projects_answers.control_id = internal_forms.id');

		$this->db->like('internal_forms.title', $search);
		$this->db->or_like('people.rut', $search);
		$this->db->or_like('patent', $search);
		$this->db->or_like('internal_forms.created', $search);
		$this->db->or_like('internal_forms.modified', $search);

		$this->db->where('projects_answers.control', $control);
		$this->db->where('internal_answers.order', 1);
		$this->db->limit($length, $start);
		$quer = $this->db->get('internal_forms')->num_rows();
		return $quer;
	}

	public function getDetailsInternalForms($internal_id, $control){
		$this->db->select('internal_forms.title, internal_forms.observation, patent, type, forms_detail.order as qorder, forms_detail.question, internal_answers.order as aorder, internal_answers.answer, DATE_FORMAT(internal_forms.created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(internal_forms.modified, "%d-%m-%Y %H:%i:%s") as modified, internal_answers.control');

		// $this->db->join('projects', 'projects.id = '.$internal_id);
		$this->db->join('vehicles', 'vehicles.id = internal_forms.vehicle_id');
		$this->db->join('vehicles_type', 'vehicles_type.id = vehicles.vehicles_type_id');
		$this->db->join('internal_answers', 'internal_answers.control_id = internal_forms.id');
		$this->db->join('forms_detail', 'forms_detail.forms_id = internal_forms.forms_id');
		$this->db->where('internal_forms.id', $internal_id);
		$this->db->where('internal_answers.control', $control);
		$this->db->where('internal_answers.order = forms_detail.order');
		$this->db->order_by('forms_detail.order');

		return $this->db->get('internal_forms')->result();
	}
}