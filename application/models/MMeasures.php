<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MMeasures extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function getMeasures($start, $length, $search, $order, $by) {
		$retornar = array();
		if ($search) {
			$busca = $this->getSearchMeasures($search, $start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCountSearch($search, $start, $length, $order, $by);
			$retornar['data'] = $busca['datos'];
		}
		else {
			$todo = $this->getAllMeasures($start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCount();
			$retornar['data'] = $todo['datos'];
		}

		$retornar['numDataTotal'] = $this->getCount();

		return $retornar;
	}

	public function getMeasure($id) {
		$this->db->select('id, measure, acronimo, DATE_FORMAT(created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(modified, "%d-%m-%Y %H:%i:%s") as modified');
		$this->db->from('measures');
		$this->db->where('id', $id);
		$this->db->limit(1);

		return $this->db->get()->result_array();
	}

	// Funciones auxiliares datatable
	public function getAllMeasures($start, $length, $order, $by) {
		$this->db->select('id, measure, acronimo, DATE_FORMAT(created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(modified, "%d-%m-%Y %H:%i:%s") as modified');
		switch ($by) {
			case 0:
				$this->db->order_by('id', $order);
				break;
			case 1:
				$this->db->order_by('measure', $order);
				break;
			case 2:
				$this->db->order_by('acronimo', $order);
				break;
		}
		$this->db->limit($length, $start);
		$query = $this->db->get('measures');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getSearchMeasures($search, $start, $length, $order, $by) {
		$this->db->select('id, measure, acronimo, DATE_FORMAT(created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(modified, "%d-%m-%Y %H:%i:%s") as modified');
		$this->db->like('id', $search);
		$this->db->or_like('measure', $search);
		$this->db->or_like('acronimo', $search);
		switch ($by) {
			case 0:
				$this->db->order_by('id', $order);
				break;
			case 1:
				$this->db->order_by('measure', $order);
				break;
			case 2:
				$this->db->order_by('acronimo', $order);
				break;
		}
		$this->db->limit($length, $start);
		$query = $this->db->get('measures');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getCount() {
		return $this->db->count_all('measures');
	}

	public function getCountSearch($search, $start, $length, $order, $by) {
		$this->db->select('id');
		$this->db->like('id', $search);
		$this->db->or_like('measure', $search);
		$this->db->or_like('acronimo', $search);
		$quer = $this->db->get('measures')->num_rows();
		return $quer;
	}
	// fin funciones auxiliares

	//Crud
	public function addMeasure($data) {
		if($this->db->insert('measures', $data))
			return true;
		else
			return false;
	}

	public function editMeasure($data, $id) {
		$this->db->where('id', $id);
		if($this->db->update('measures', $data))
			return true;
		else
			return false;
	}

	public function deleteMeasure($id) {
		$this->db->where('id', $id);
		if($this->db->delete('measures'))
			return true;
		else
			return false;
	}
}