<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MMinimum_Requirements extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function getRequirements($start, $length, $search, $order, $by) {
		$retornar = array();
		if ($search) {
			$busca = $this->getSearchRequirement($search, $start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCountSearch($search, $start, $length, $order, $by);
			$retornar['data'] = $busca['datos'];
		}
		else {
			$todo = $this->getAllRequirements($start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCount();
			$retornar['data'] = $todo['datos'];
		}

		$retornar['numDataTotal'] = $this->getCount();

		return $retornar;
	}

	public function getRequirement($id) {
		$this->db->select('id, requirement, description, DATE_FORMAT(created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(modified, "%d-%m-%Y %H:%i:%s") as modified');
		$this->db->from('minimum_requirements');
		$this->db->where('id', $id);
		$this->db->limit(1);

		return $this->db->get()->result_array();
	}

	// Funciones auxiliares datatable
	public function getAllRequirements($start, $length, $order, $by) {
		$this->db->select('id, requirement, description, DATE_FORMAT(created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(modified, "%d-%m-%Y %H:%i:%s") as modified');
		switch ($by) {
			case 0:
				$this->db->order_by('id', $order);
				break;
			case 1:
				$this->db->order_by('requirement', $order);
				break;
			case 2:
				$this->db->order_by('description', $order);
				break;	
			case 3:
				$this->db->order_by('created', $order);
				break;
			case 4:
				$this->db->order_by('modified', $order);
				break;
		}
		$this->db->limit($length, $start);
		$query = $this->db->get('minimum_requirements');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getSearchRequirement($search, $start, $length, $order, $by) {
		$this->db->select('id, requirement, description, DATE_FORMAT(created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(modified, "%d-%m-%Y %H:%i:%s") as modified');
		$this->db->like('id', $search);
		$this->db->or_like('requirement', $search);
		$this->db->or_like('description', $search);
		switch ($by) {
			case 0:
				$this->db->order_by('id', $order);
				break;
			case 1:
				$this->db->order_by('requirement', $order);
				break;
			case 2:
				$this->db->order_by('description', $order);
				break;	
			case 3:
				$this->db->order_by('created', $order);
				break;
			case 4:
				$this->db->order_by('modified', $order);
				break;
		}
		$this->db->limit($length, $start);
		$query = $this->db->get('minimum_requirements');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getCount() {
		return $this->db->count_all('minimum_requirements');
	}

	public function getCountSearch($search, $start, $length, $order, $by) {
		$this->db->select('id');
		$this->db->like('id', $search);
		$this->db->or_like('requirement', $search);
		$this->db->or_like('description', $search);
		$quer = $this->db->get('minimum_requirements')->num_rows();
		return $quer;
	}
	// fin funciones auxiliares

	//Crud
	public function addRequirement($data) {
		if($this->db->insert('minimum_requirements', $data))
			return true;
		else
			return false;
	}

	public function editRequirement($data, $id) {
		$this->db->where('id', $id);
		if($this->db->update('minimum_requirements', $data))
			return true;
		else
			return false;
	}

	public function deleteRequirement($id) {
		$this->db->where('id', $id);
		if($this->db->delete('minimum_requirements'))
			return true;
		else
			return false;
	}

}