<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MPeople_Profiles extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	public function getPeople_Profiles($start, $length, $search, $order, $by) {
		$retornar = array();
		if ($search) {
			$busca = $this->getSearchPeople_Profiles($search, $start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCountSearch($search, $start, $length, $order, $by);
			$retornar['data'] = $busca['datos'];
		}
		else {
			$todo = $this->getAllPeople_Profiles($start, $length, $order, $by);
			$retornar['numDataFilter'] = $this->getCount();
			$retornar['data'] = $todo['datos'];
		}

		$retornar['numDataTotal'] = $this->getCount();

		return $retornar;
	}

	public function getPeople_Profile($id) {
		$this->db->select('id, profile, DATE_FORMAT(created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(modified, "%d-%m-%Y %H:%i:%s") as modified');
		$this->db->from('people_profiles');
		$this->db->where('id', $id);
		$this->db->limit(1);

		return $this->db->get()->result_array();
	}

	// Funciones auxiliares datatable
	public function getAllPeople_Profiles($start, $length, $order, $by) {
		$this->db->select('id, profile, DATE_FORMAT(created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(modified, "%d-%m-%Y %H:%i:%s") as modified');
		if ($by == 0) {
			$this->db->order_by('id', $order);
		}
		else {
			$this->db->order_by('profile', $order);
		}
		$this->db->limit($length, $start);
		$query = $this->db->get('people_profiles');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getSearchPeople_Profiles($search, $start, $length, $order, $by) {
		$this->db->select('id, profile, DATE_FORMAT(created, "%d-%m-%Y %H:%i:%s") as created, DATE_FORMAT(modified, "%d-%m-%Y %H:%i:%s") as modified');
		$this->db->like('id', $search);
		$this->db->or_like('profile', $search);
		if ($by == 0) {
			$this->db->order_by('id', $order);
		}
		else {
			$this->db->order_by('profile', $order);
		}
		$this->db->limit($length, $start);
		$query = $this->db->get('people_profiles');
		$retornar = array(
			'datos' => $query->result()
		);
		return $retornar;
	}

	public function getCount() {
		return $this->db->count_all('people_profiles');
	}

	public function getCountSearch($search, $start, $length, $order, $by) {
		$this->db->select('id');
		$this->db->or_like('profile', $search);
		$quer = $this->db->get('people_profiles')->num_rows();
		return $quer;
	}
	// fin funciones auxiliares

	//Crud
	public function addPeople_Profile($data) {
		if($this->db->insert('people_profiles', $data))
			return true;
		else
			return false;
	}

	public function editPeople_Profile($data, $id) {
		$this->db->where('id', $id);
		if($this->db->update('people_profiles', $data))
			return true;
		else
			return false;
	}

	public function deletePeople_Profile($id) {
		$this->db->where('id', $id);
		if($this->db->delete('people_profiles'))
			return true;
		else
			return false;
	}
}