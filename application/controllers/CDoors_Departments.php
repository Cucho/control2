<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CDoors_Departments extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('mDoors_Departments', 'modelo');
		if (empty($this->session->userdata('options'))) {
			redirect('welcome');
		}
		else if (in_array('01020302', $this->session->userdata('options'))) {
			redirect('welcome');
		}
	}

	public function index() {
		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('departments/list_doors_departments');
	}
	//Datatable
	public function datatable() {
		$start = $this->input->post('start');
		$length = $this->input->post('length');
		$search = $this->input->post('search')['value'];
		$by = $this->input->post('order')['0']['column'];
		$order = $this->input->post('order')['0']['dir'];

		$result = $this->modelo->getDoors_Departments($start, $length, $search, $order, $by);

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
            "recordsTotal"    => intval($result['numDataTotal']),
            "recordsFiltered" => intval($result['numDataFilter']),
            "data"            => $result['data']
            );

        echo json_encode($json_data);
	}

	//Vistas
	public function add() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$doors 		= $this->modelo->getAllDoors();
		$department = $this->modelo->getAllDepartments();

		$data = array(
			'doors' => $doors,
			'department' => $department
		);

		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('departments/add_door_department', $data);
	}

	//Crud
	public function addDoor_Department() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$door	= trim($this->input->post('door', TRUE));
		$department	= trim($this->input->post('department', TRUE));

		$date_time = date('Y-m-d H:i:s');

		$data = array(
			'doors_id' => $door,
			'departments_id' 	=> $department
		);

		if($this->modelo->addDoor_Department($data))
			echo '1';
		else
			echo '0';
	}


	public function deleteDoor_Department() {
		if (!$this->session->userdata('del')) {
			redirect('welcome');
		}
		$door = trim($this->input->post('door', TRUE)); 
		$department = trim($this->input->post('department', TRUE)); 

		if($this->modelo->deleteDoor_Department($door, $department))
			echo '1';
		else
			echo '0';
	}
}