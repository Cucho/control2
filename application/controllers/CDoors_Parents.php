<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CDoors_Parents extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('mDoors_Parents', 'modelo');
	}

	public function index() {
		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('doors/list_doors_parents');
	}
	//Datatable
	public function datatable() {
		$start = $this->input->post('start');
		$length = $this->input->post('length');
		$search = $this->input->post('search')['value'];
		$by = $this->input->post('order')['0']['column'];
		$order = $this->input->post('order')['0']['dir'];

		$result = $this->modelo->getDoors_Parents($start, $length, $search, $order, $by);

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
            "recordsTotal"    => intval($result['numDataTotal']),
            "recordsFiltered" => intval($result['numDataFilter']),
            "data"            => $result['data']
            );

        echo json_encode($json_data);
	}

	//Vistas
	public function add() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$doors = $this->modelo->getAllDoors();

		$data = array(
			'doors' => $doors
		);

		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('doors/add_doors_parents', $data);
	}

	//Crud
	public function addDoor_Parent() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$doors_id 	= 	trim($this->input->post('doors_id', TRUE));
		$parent 	= 	$this->input->post('parent', TRUE);
		
		if($this->modelo->addDoor_Parent($doors_id, $parent))
			echo '1';
		else
			echo '0';
	}

	public function deleteDoor_Parent() {
		if (!$this->session->userdata('del')) {
			redirect('welcome');
		}
		$doors_id = trim($this->input->post('doors_id', TRUE));
		$parent = trim($this->input->post('parent', TRUE));

		if($this->modelo->deleteDoor_Parent($doors_id, $parent))
			echo '1';
		else
			echo '0';
	}

	public function getParentsDoor()
	{
		$doors_id = trim($this->input->post('doors_id', TRUE));
		
		$parents = $this->modelo->getParentsDoor($doors_id);

		echo json_encode($parents);
	}
}