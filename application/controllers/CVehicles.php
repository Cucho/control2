<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CVehicles extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('mVehicles', 'modelo');
		if (empty($this->session->userdata('options'))) {
			redirect('welcome');
		}
		else if (in_array('10010000', $this->session->userdata('options'))) {
			redirect('welcome');
		}
	}

	public function index() {
		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('vehicles/index');
	}
	//Datatable
	public function datatable() {
		$start = $this->input->post('start');
		$length = $this->input->post('length');
		$search = $this->input->post('search')['value'];
		$by = $this->input->post('order')['0']['column'];
		$order = $this->input->post('order')['0']['dir'];

		$result = $this->modelo->getVehicles($start, $length, $search, $order, $by);

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
            "recordsTotal"    => intval($result['numDataTotal']),
            "recordsFiltered" => intval($result['numDataFilter']),
            "data"            => $result['data']
            );

        echo json_encode($json_data);
	}

	//Vistas
	public function add() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$companies = $this->modelo->getAllCompanies();
		$people = $this->modelo->getAllPeople();
		$type = $this->modelo->getAllVehicles_Type();
		$profile = $this->modelo->getAllVehicles_Profiles();

		$data = array(
			'companies' => $companies,
			'people' => $people,
			'type' => $type,
			'profiles' => $profile
		);

		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('vehicles/add', $data);
	}

	public function edit() {
		if (!$this->session->userdata('edit')) {
			redirect('welcome');
		}
		$id = trim($this->input->get('id', TRUE));

		$vehicle = $this->modelo->getVehicle($id);
		$companies = $this->modelo->getAllCompanies();
		$people = $this->modelo->getAllPeople();
		$type = $this->modelo->getAllVehicles_Type();
		$profile = $this->modelo->getAllVehicles_Profiles();

		$data = array(
			'vehicles' => $vehicle,
			'companies' => $companies,
			'people' => $people,
			'type' => $type,
			'profile' => $profile
		);

		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('vehicles/edit', $data);
	}

	public function view() {
		$id = trim($this->input->get('id', TRUE));

		$vehicle = $this->modelo->getVehicle($id);
		$companies = $this->modelo->getAllCompanies();
		$people = $this->modelo->getAllPeople();
		$type = $this->modelo->getAllVehicles_Type();
		$profile = $this->modelo->getAllVehicles_Profiles();

		$data = array(
			'vehicles' => $vehicle,
			'companies' => $companies,
			'people' => $people,
			'type' => $type,
			'profile' => $profile
		);

		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('vehicles/view', $data);
	}

	//Crud
	public function addVehicles() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$patents 	= 	trim($this->input->post('patents', TRUE));
		$model 	= 	trim($this->input->post('model', TRUE));
		$internal 	= 	trim($this->input->post('internal', TRUE));
		$companies 	= 	trim($this->input->post('companies', TRUE));
		$people 	= 	trim($this->input->post('people', TRUE));
		$type 		= 	trim($this->input->post('type', TRUE));
		$profile 	= 	trim($this->input->post('profile', TRUE));
		$states_id  =   trim($this->input->post('states_id', TRUE));

		$date_time = date('Y-m-d H:i:s');

		$data = array(
			'patent' 				=> $patents,
			'model' 				=> $model,
			'internal' 				=> $internal,
			'companies_id' 			=> $companies,
			'people_id' 			=> $people,
			'vehicles_type_id' 		=> $type,
			'vehicles_profiles_id' 	=> $profile,
			'states_id'				=> $states_id,
			'created'				=> $date_time,
			'modified'				=> $date_time
		);

		if($this->modelo->addVehicle($data))
			echo '1';
		else
			echo '0';
	}

	public function editVehicle() {
		if (!$this->session->userdata('edit')) {
			redirect('welcome');
		}
		$id = trim($this->input->post('id', TRUE));
		$patents 	= 	trim($this->input->post('patents', TRUE));
		$model 	= 	trim($this->input->post('model', TRUE));
		$internal 	= 	trim($this->input->post('internal', TRUE));
		$companies 	= 	trim($this->input->post('companies', TRUE));
		$people 	= 	trim($this->input->post('people', TRUE));
		$type 		= 	trim($this->input->post('type', TRUE));
		$profile 	= 	trim($this->input->post('profile', TRUE));
		$states_id  =   trim($this->input->post('states_id', TRUE));

		$date_time = date('Y-m-d H:i:s');

		$data = array(
			'patent' 				=> $patents,
			'model' 				=> $model,
			'internal' 				=> $internal,
			'companies_id' 			=> $companies,
			'people_id' 			=> $people,
			'vehicles_type_id' 		=> $type,
			'vehicles_profiles_id' 	=> $profile,
			'states_id'				=> $states_id,
			'modified'				=> $date_time
		);

		if($this->modelo->editVehicle($data, $id)) {
			echo '1';
		}
		else {
			echo '0';
		}
	}

	public function deleteVehicle() {
		if (!$this->session->userdata('del')) {
			redirect('welcome');
		}
		$id = trim($this->input->post('id', TRUE));

		if($this->modelo->deleteVehicle($id))
			echo '1';
		else
			echo '0';
	}
}