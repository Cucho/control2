<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CForms extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('mForms', 'modelo');
		if (empty($this->session->userdata('options'))) {
			redirect('welcome');
		}
		else if (in_array('09010000', $this->session->userdata('options'))) {
			redirect('welcome');
		}
	}

	public function index() {
		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('forms/index');
	}

	//Datatable
	public function datatable() {
		$start = $this->input->post('start');
		$length = $this->input->post('length');
		$search = $this->input->post('search')['value'];
		$by = $this->input->post('order')['0']['column'];
		$order = $this->input->post('order')['0']['dir'];

		$result = $this->modelo->getForms($start, $length, $search, $order, $by);

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
            "recordsTotal"    => intval($result['numDataTotal']),
            "recordsFiltered" => intval($result['numDataFilter']),
            "data"            => $result['data']
            );

        echo json_encode($json_data);
	}

	//Vistas
	public function add() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$this->load->view('header');
		$this->load->view('aside');
		$data = array(
			'answers' => $this->modelo->getAllAnswers_Type(),
			'measures' => $this->modelo->getAllMeasures()
		);

		$this->load->view('forms/add', $data);
	}

	public function edit() {
		if (!$this->session->userdata('edit')) {
			redirect('welcome');
		}
		$id = trim($this->input->get('id', TRUE));

		$forms = $this->modelo->getForm($id);

		$data = array(
			'forms' => $forms,
			'answers' => $this->modelo->getAllAnswers_Type(),
			'measures' => $this->modelo->getAllMeasures()
		);

		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('forms/edit', $data);
	}

	public function view() {
		$id = trim($this->input->get('id', TRUE));

		$forms = $this->modelo->getForm($id);

		$data = array(
			'forms' => $forms
		);

		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('forms/view', $data);
	}

	//Crud
	public function addForm() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$title 			= trim($this->input->post('title', TRUE));
		$description 	= trim($this->input->post('description', TRUE));

		$date_time = date('Y-m-d H:i:s');

		$data = array(
			'title' => $title,
			'description' => $description,
			'created' => $date_time,
			'modified' => $date_time
		);

		$id 			= $this->modelo->addForm($data);
		$questions 		= $this->input->post('questions', TRUE);
		$answers_types 	= $this->input->post('answers_types', TRUE);
		$placeholder 	= $this->input->post('answers_placeholders', TRUE);
		$measures 		= $this->input->post('measures', TRUE);
		$order 			= $this->input->post('orders', TRUE);

		if($id > 0) {
			for ($i=0; $i < count($questions); $i++) {
				$dato = array(
					'order' => $i+1,
					'question' => trim($questions[$i]),
					'placeholder' => trim($placeholder[$i]),
					'forms_id' => $id,
					'answers_type_id' => trim($answers_types[$i]),
					'measures_id' => trim($measures[$i])
				);
				$this->modelo->addForms_Detail($dato);
			}
			echo '1';
		}
		else {
			echo '0';
		}
	}

	public function editForm() {
		if (!$this->session->userdata('edit')) {
			redirect('welcome');
		}
		$id = trim($this->input->post('id', TRUE));

		$title 			= trim($this->input->post('title', TRUE));
		$description 	= trim($this->input->post('description', TRUE));

		$date_time = date('Y-m-d H:i:s');

		$data = array(
			'title' => $title,
			'description' => $description,
			'modified' => $date_time
		);

		$questions 		= $this->input->post('questions', TRUE);
		$answers_types 	= $this->input->post('answers_types', TRUE);
		$placeholder 	= $this->input->post('answers_placeholders', TRUE);
		$measures 		= $this->input->post('measures', TRUE);
		$order 			= $this->input->post('orders', TRUE);

		$origin 		= $this->input->post('origin', TRUE);


		if($this->modelo->editForm($data, $id)) {
			for ($i=0; $i < count($questions); $i++) {
				$order = $i+1;
				if ($i < $origin) {
					$this->modelo->editForms_Detail($order, $id, trim($questions[$i]), trim($placeholder[$i]), trim($answers_types[$i]), trim($measures[$i]));
				}
				else {
					$dato = array(
						'order' => $i+1,
						'question' => trim($questions[$i]),
						'placeholder' => trim($placeholder[$i]),
						'forms_id' => $id,
						'answers_type_id' => trim($answers_types[$i]),
						'measures_id' => trim($measures[$i])
					);
					$this->modelo->addForms_Detail($dato);
				}

			}
			echo '1';
		}
		else {
			echo '0';
		}
	}

	public function deleteForm() {
		if (!$this->session->userdata('del')) {
			redirect('welcome');
		}
		$id = trim($this->input->post('id', TRUE));

		if($this->modelo->deleteForm($id)) {
			echo '1';
		}
		else {
			echo '0';
		}
	}
}
