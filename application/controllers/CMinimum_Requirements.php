<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CMinimum_Requirements extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('mMinimum_Requirements', 'modelo');
		if (empty($this->session->userdata('options'))) {
			redirect('welcome');
		}
		else if (in_array('06020100', $this->session->userdata('options'))) {
			redirect('welcome');
		}
	}

	public function index() {
		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('minim_require/index');
	}

	//Datatable
	public function datatable() {
		$start = $this->input->post('start');
		$length = $this->input->post('length');
		$search = $this->input->post('search')['value'];
		$by = $this->input->post('order')['0']['column'];
		$order = $this->input->post('order')['0']['dir'];

		$result = $this->modelo->getRequirements($start, $length, $search, $order, $by);
		
		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
            "recordsTotal"    => intval($result['numDataTotal']),
            "recordsFiltered" => intval($result['numDataFilter']),
            "data"            => $result['data'],
            );

        echo json_encode($json_data);
	}

	//Vistas
	public function add() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('minim_require/add');
	}

	public function edit() {
		if (!$this->session->userdata('edit')) {
			redirect('welcome');
		}
		$id = trim($this->input->get('id', TRUE));

		$requirement = $this->modelo->getRequirement($id);
		
		$data = array('requirement' => $requirement);

		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('minim_require/edit', $data);
	}

	public function view() {
		$id = trim($this->input->get('id', TRUE));

		$requirement = $this->modelo->getRequirement($id);
		
		$data = array('requirement' => $requirement);

		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('minim_require/view', $data);

	}

	//Crud
	public function addRequirement() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$requirement 	= trim($this->input->post('requirement', TRUE));
		$description 	= trim($this->input->post('description', TRUE));
		
		$date_time = date('Y-m-d H:i:s');
		
		$data = array(
			'requirement' 	=> $requirement,
			'description' 	=> $description,
			'created' 		=> $date_time,
			'modified' 		=> $date_time
		);

		if($this->modelo->addRequirement($data)) {
			echo '1';
		}
		else {
			echo '0';
		}
	}

	public function editRequirement() {
		if (!$this->session->userdata('edit')) {
			redirect('welcome');
		}
		$id = trim($this->input->post('id', TRUE));

		$requirement 	= trim($this->input->post('requirement', TRUE));
		$description 	= trim($this->input->post('description', TRUE));

		$date_time = date('Y-m-d H:i:s');
		
		$data = array(
			'requirement' 	=> $requirement,
			'description' 	=> $description,
			'modified' 		=> $date_time
		);

		if($this->modelo->editRequirement($data, $id)) {
			echo '1';
		}
		else {
			echo '0';
		}
	}

	public function deleteRequirement() {
		if (!$this->session->userdata('del')) {
			redirect('welcome');
		}
		$id = trim($this->input->post('id', TRUE)); 

		if($this->modelo->deleteRequirement($id)) {
			echo '1';
		}
		else {
			echo '0';
		}
	}
}