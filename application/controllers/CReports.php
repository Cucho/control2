<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CReports extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('MReports', 'modelo');
		if (empty($this->session->userdata('options'))) {
			redirect('welcome');
		}
		else if (in_array('03010000', $this->session->userdata('options'))) {
			redirect('welcome');
		}
	}

	public function index() 
	{
		$companies = $this->modelo->getCompanies();
		$people = $this->modelo->getPeople();
		$vehicles = $this->modelo->getVehicles();

		$data = array(
			'companies' => $companies,
			'people'	=> $people,
			'vehicles'	=> $vehicles
		);

		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('reports/index', $data);
	}

	public function getPeopleByCompany()
	{
		$companies_id = trim($this->input->post('companies_id', TRUE));

		if($companies_id == 0)
			$companies_id = '';

		$people = $this->modelo->getPeopleByCompany($companies_id);
		echo  json_encode($people);
	}

	public function getVehiclesByCompany()
	{
		$companies_id = trim($this->input->post('companies_id', TRUE));

		if($companies_id == 0)
			$companies_id = '';

		$vehicles = $this->modelo->getVehiclesByCompany($companies_id);
		echo  json_encode($vehicles);
	}	

	public function generateReport()
	{
		$radio_date = trim($this->input->post('radio_date', TRUE));
		$type = trim($this->input->post('type', TRUE));

		if($radio_date == 1)
		{
			$date = trim($this->input->post('date', TRUE));

			if($type == 1)
			{
				$company = trim($this->input->post('company', TRUE));
				
				$data = $this->modelo->getReportDate($date, $type, $company);

				echo json_encode($data);
			}
			else if($type == 2)
			{
				$vehicle = trim($this->input->post('vehicle', TRUE));

				$data = $this->modelo->getReportDate($date, $type, $vehicle);

				echo json_encode($data);
				
			}
			else if($type == 0)
			{
				$people = trim($this->input->post('people', TRUE));

				$data = $this->modelo->getReportDate($date, $type, $people);

				echo json_encode($data);
				
			}
		}
		else if($radio_date == 2)
		{
			$date_init = trim($this->input->post('date_init', TRUE));
			$date_end = trim($this->input->post('date_end', TRUE));

			if($type == 1)
			{
				$company = trim($this->input->post('company', TRUE));

				$data = $this->modelo->getReportInterval($date_init, $date_end, $type, $company);

				echo json_encode($data);
				
			}
			else if($type == 2)
			{
				$vehicle = trim($this->input->post('vehicle', TRUE));

				$data = $this->modelo->getReportInterval($date_init, $date_end, $type, $vehicle);

				echo json_encode($data);
				
			}
			else if($type == 0)
			{
				$people = trim($this->input->post('people', TRUE));

				$data = $this->modelo->getReportInterval($date_init, $date_end, $type, $people);

				echo json_encode($data);
				
			}
		}
		else if($radio_date == 0)
		{
			$year = trim($this->input->post('year', TRUE));
			$month = trim($this->input->post('month', TRUE));

			if($type == 1)
			{
				$company = trim($this->input->post('company', TRUE));

				$data = $this->modelo->getReportYearMonth($year, $month, $type, $company);

				echo json_encode($data);
				
			}
			else if($type == 2)
			{
				$vehicle = trim($this->input->post('vehicle', TRUE));

				$data = $this->modelo->getReportYearMonth($year, $month, $type, $vehicle);

				echo json_encode($data);
				
			}
			else if($type == 0)
			{
				$people = trim($this->input->post('people', TRUE));

				$data = $this->modelo->getReportYearMonth($year, $month, $type, $people);

				echo json_encode($data);
				
			}
		}
	}

	// public function exportExcel(){
	// 	// $radio_date = trim($this->input->post('radio_date', TRUE));
	// 	// $type = trim($this->input->post('type', TRUE));

	// 	// if($radio_date == 1){
	// 	// 	$date = trim($this->input->post('date', TRUE));

	// 	// 	if($type == 1){
	// 	// 		$company = trim($this->input->post('company', TRUE));
				
	// 	// 		$data = $this->modelo->getReportDate($date, $type, $company);
	// 	// 	}
	// 	// 	else if($type == 2){
	// 	// 		$vehicle = trim($this->input->post('vehicle', TRUE));

	// 	// 		$data = $this->modelo->getReportDate($date, $type, $vehicle);
	// 	// 	}
	// 	// 	else if($type == 0){
	// 	// 		$people = trim($this->input->post('people', TRUE));

	// 	// 		$data = $this->modelo->getReportDate($date, $type, $people);
	// 	// 	}
	// 	// }
	// 	// else if($radio_date == 2){
	// 	// 	$date_init = trim($this->input->post('date_init', TRUE));
	// 	// 	$date_end = trim($this->input->post('date_end', TRUE));

	// 	// 	if($type == 1){
	// 	// 		$company = trim($this->input->post('company', TRUE));

	// 	// 		$data = $this->modelo->getReportInterval($date_init, $date_end, $type, $company);	
	// 	// 	}
	// 	// 	else if($type == 2){
	// 	// 		$vehicle = trim($this->input->post('vehicle', TRUE));

	// 	// 		$data = $this->modelo->getReportInterval($date_init, $date_end, $type, $vehicle);
	// 	// 	}
	// 	// 	else if($type == 0){
	// 	// 		$people = trim($this->input->post('people', TRUE));

	// 	// 		$data = $this->modelo->getReportInterval($date_init, $date_end, $type, $people);
	// 	// 	}
	// 	// }
	// 	// else if($radio_date == 0){
	// 	// 	$year = trim($this->input->post('year', TRUE));
	// 	// 	$month = trim($this->input->post('month', TRUE));

	// 	// 	if($type == 1){
	// 	// 		$company = trim($this->input->post('company', TRUE));

	// 	// 		$data = $this->modelo->getReportYearMonth($year, $month, $type, $company);
	// 	// 	}
	// 	// 	else if($type == 2){
	// 	// 		$vehicle = trim($this->input->post('vehicle', TRUE));

	// 	// 		$data = $this->modelo->getReportYearMonth($year, $month, $type, $vehicle);
	// 	// 	}
	// 	// 	else if($type == 0){
	// 	// 		$people = trim($this->input->post('people', TRUE));

	// 	// 		$data = $this->modelo->getReportYearMonth($year, $month, $type, $people);
	// 	// 	}
	// 	// }

	// 	$this->load->library('excel');
	// 	$this->excel->setActiveSheetIndex(0);
	// 	$this->excel->getActiveSheet()->setTitle('Informe');

	// 	$this->excel->getActiveSheet()->SetCellValue('A1', 'ID');
	// 	$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize('11');
	// 	$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
	// 	$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

	// 	$this->excel->getActiveSheet()->SetCellValue('B1', 'Rut');
	// 	$this->excel->getActiveSheet()->getStyle('B1')->getFont()->setSize('11');
	// 	$this->excel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
	// 	$this->excel->getActiveSheet()->getStyle('B1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

	// 	$this->excel->getActiveSheet()->SetCellValue('C1', 'Nombre');
	// 	$this->excel->getActiveSheet()->getStyle('C1')->getFont()->setSize('11');
	// 	$this->excel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
	// 	$this->excel->getActiveSheet()->getStyle('C1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

	// 	$this->excel->getActiveSheet()->SetCellValue('D1', 'Perfil');
	// 	$this->excel->getActiveSheet()->getStyle('D1')->getFont()->setSize('11');
	// 	$this->excel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);
	// 	$this->excel->getActiveSheet()->getStyle('D1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

	// 	$this->excel->getActiveSheet()->SetCellValue('E1', 'Puerta');
	// 	$this->excel->getActiveSheet()->getStyle('E1')->getFont()->setSize('11');
	// 	$this->excel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true);
	// 	$this->excel->getActiveSheet()->getStyle('E1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

	// 	$this->excel->getActiveSheet()->SetCellValue('F1', 'Estado');
	// 	$this->excel->getActiveSheet()->getStyle('F1')->getFont()->setSize('11');
	// 	$this->excel->getActiveSheet()->getStyle('F1')->getFont()->setBold(true);
	// 	$this->excel->getActiveSheet()->getStyle('F1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

	// 	$this->excel->getActiveSheet()->SetCellValue('G1', 'Movimiento');
	// 	$this->excel->getActiveSheet()->getStyle('G1')->getFont()->setSize('11');
	// 	$this->excel->getActiveSheet()->getStyle('G1')->getFont()->setBold(true);
	// 	$this->excel->getActiveSheet()->getStyle('G1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

	// 	$this->excel->getActiveSheet()->SetCellValue('H1', 'Tipo');
	// 	$this->excel->getActiveSheet()->getStyle('H1')->getFont()->setSize('11');
	// 	$this->excel->getActiveSheet()->getStyle('H1')->getFont()->setBold(true);
	// 	$this->excel->getActiveSheet()->getStyle('H1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

	// 	$this->excel->getActiveSheet()->SetCellValue('I1', 'Vehículo');
	// 	$this->excel->getActiveSheet()->getStyle('I1')->getFont()->setSize('11');
	// 	$this->excel->getActiveSheet()->getStyle('I1')->getFont()->setBold(true);
	// 	$this->excel->getActiveSheet()->getStyle('I1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

	// 	$this->excel->getActiveSheet()->SetCellValue('J1', 'Fecha');
	// 	$this->excel->getActiveSheet()->getStyle('J1')->getFont()->setSize('11');
	// 	$this->excel->getActiveSheet()->getStyle('J1')->getFont()->setBold(true);
	// 	$this->excel->getActiveSheet()->getStyle('J1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

	// 	$fila = 2;
	// 	foreach ($data as $k) {
	// 		$this->excel->getActiveSheet()->setCellValue('A'.$fila, trim($k['id']));
	// 		$this->excel->getActiveSheet()->setCellValue('B'.$fila, trim($k['rut'].'-'.$k['digit']));
	// 		$this->excel->getActiveSheet()->setCellValue('C'.$fila, trim($k['name'].' '.$k['last_name']));
	// 		$this->excel->getActiveSheet()->setCellValue('D'.$fila, trim($k['profile']));
	// 		$this->excel->getActiveSheet()->setCellValue('E'.$fila, trim($k['door']));
	// 		$estado = 'Rechazado';
	// 		if ($k['success'] == null) {
	// 			$estado = 'Permitido';
	// 		}
	// 		else if ($k['success'] == 1) {
	// 			$estado = 'Permitido';
	// 		}
	// 		$this->excel->getActiveSheet()->setCellValue('F'.$fila, $estado);

	// 		$entry = 'Salida';
	// 		if ($k['entry'] == 1) {
	// 			$entry = 'Entrada';
	// 		}
	// 		$this->excel->getActiveSheet()->setCellValue('G'.$fila, $entry);

	// 		$flow = '';
	// 		$patent = 'N/A';
	// 		if ($k['flow'] == null) {
	// 			$flow = 'Peatonal';
	// 		}
	// 		else { 
	// 			if ($k['flow'] == 0) {
	// 				$flow = 'Peatonal';
	// 			}
	// 			else{
	// 				$flow = 'Vehícular'; 
	// 				$patent = $k['patent'];
	// 			}
	// 		}
	// 		$this->excel->getActiveSheet()->setCellValue('H'.$fila, $flow);
	// 		$this->excel->getActiveSheet()->setCellValue('I'.$fila, $patent);

	// 		$this->excel->getActiveSheet()->setCellValue('J'.$fila, trim($k['created']));
	// 		$fila++;
	// 	}
	// 	for ($col='A'; $col !== 'K' ; $col++) { 
	// 		$this->excel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
	// 	}

	// 	$filename = 'Informe '.date('Y-m-d').'.xls';
	// 	header('Content-Type: application/vnd.ms-excel;charset=utf-8');
	// 	header('Content-Disposition: attachment;filename="'.$filename.'"');
	// 	header('Cache-Control: max-age=0');
	// 	$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
	// 	$objWriter->save('php://output');
	// }
}