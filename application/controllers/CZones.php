<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CZones extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('mZones', 'modelo');
		if (empty($this->session->userdata('options'))) {
			redirect('welcome');
		}
		else if (in_array('01020101', $this->session->userdata('options'))) {
			redirect('welcome');
		}
	}

	public function index() {
		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('zones/index');
	}
	//Datatable
	public function datatable() {
		$start = $this->input->post('start');
		$length = $this->input->post('length');
		$search = $this->input->post('search')['value'];
		$by = $this->input->post('order')['0']['column'];
		$order = $this->input->post('order')['0']['dir'];

		$result = $this->modelo->getZones($start, $length, $search, $order, $by);

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
            "recordsTotal"    => intval($result['numDataTotal']),
            "recordsFiltered" => intval($result['numDataFilter']),
            "data"            => $result['data']
            );

        echo json_encode($json_data);
	}

	//Vistas
	public function add() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('zones/add');
	}

	public function edit() {
		if (!$this->session->userdata('edit')) {
			redirect('welcome');
		}
		$id = trim($this->input->get('id', TRUE));

		$zone = $this->modelo->getZone($id);

		$data = array('zone' => $zone);

		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('zones/edit', $data);
	}

	public function view() {
		$id = trim($this->input->get('id', TRUE));

		$zone = $this->modelo->getZone($id);

		$data = array('zone' => $zone);

		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('zones/view', $data);
	}

	//Crud
	public function addZone() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$zone	= trim($this->input->post('zone', TRUE));

		$date_time = date('Y-m-d H:i:s');

		$data = array(
			'zone' 	=> $zone,
			'created' => $date_time,
			'modified' 	=> $date_time
		);

		if($this->modelo->addZone($data))
			echo '1';
		else
			echo '0';
	}

	public function editZone() {
		if (!$this->session->userdata('edit')) {
			redirect('welcome');
		}
		$id = trim($this->input->post('id', TRUE));

		$zone	= trim($this->input->post('zone', TRUE));

		$date_time = date('Y-m-d H:i:s');

		$data = array(
			'zone' 	=> $zone,
			'modified' 	=> $date_time
		);

		if($this->modelo->editZone($data, $id))
			echo '1';
		else
			echo '0';
	}

	public function deleteZone() {
		if (!$this->session->userdata('del')) {
			redirect('welcome');
		}
		$id = trim($this->input->post('id', TRUE)); 

		if($this->modelo->deleteZone($id))
			echo '1';
		else
			echo '0';
	}
}