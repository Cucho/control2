<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CUsers extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('mUsers', 'modelo');
		if (empty($this->session->userdata('options'))) {
			redirect('welcome');
		}
		else if (in_array('01090100', $this->session->userdata('options'))) {
			redirect('welcome');
		}
	}
	
	public function index() {
		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('users/index');
	}
	//Datatable
	public function datatable() {
		$start = $this->input->post('start');
		$length = $this->input->post('length');
		$search = $this->input->post('search')['value'];
		$by = $this->input->post('order')['0']['column'];
		$order = $this->input->post('order')['0']['dir'];

		$result = $this->modelo->getUsers($start, $length, $search, $order, $by);

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
            "recordsTotal"    => intval($result['numDataTotal']),
            "recordsFiltered" => intval($result['numDataFilter']),
            "data"            => $result['data']
            );

        echo json_encode($json_data);
	}

	//Vistas
	public function add() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$roles = $this->modelo->getAllRoles();
		$people = $this->modelo->getAllPeople();
		$users_state = $this->modelo->getAllUsers_State();

		$data = array(
			'roles' => $roles,
			'people' => $people,
			'users_state' => $users_state
		);

		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('users/add', $data);
	}

	public function edit() {
		if (!$this->session->userdata('edit')) {
			redirect('welcome');
		}
		$id = trim($this->input->get('id', TRUE));

		$users = $this->modelo->getUser($id);
		$roles = $this->modelo->getAllRoles();
		$people = $this->modelo->getAllPeople();
		$users_state = $this->modelo->getAllUsers_State();

		$data = array(
			'users' => $users,
			'roles' => $roles,
			'people' => $people,
			'users_state' => $users_state
		);

		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('users/edit', $data);
	}

	public function view() {
		$id = trim($this->input->get('id', TRUE));

		$users = $this->modelo->getUser($id);
		$roles = $this->modelo->getAllRoles();
		$people = $this->modelo->getAllPeople();
		$users_state = $this->modelo->getAllUsers_State();

		$data = array(
			'users' => $users,
			'roles' => $roles,
			'people' => $people,
			'users_state' => $users_state
		);

		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('users/view', $data);
	}

	
	//Crud
	public function addUser() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$user 			= 	trim($this->input->post('user', TRUE));
		$password 		= 	trim($this->input->post('password', TRUE));
		$roles_id 		= 	trim($this->input->post('roles_id', TRUE));
		$people_id 		= 	trim($this->input->post('people_id', TRUE));
		$users_state_id	= 	trim($this->input->post('users_state_id', TRUE));
		$users_save		= 	trim($this->input->post('users_save', TRUE));
		$users_edit		= 	trim($this->input->post('users_edit', TRUE));
		$users_del		= 	trim($this->input->post('users_del', TRUE));

		$date_time = date('Y-m-d H:i:s');

		$data = array(
			'user' 				=> $user,
			'password' 			=> md5($password),
			'roles_id' 			=> $roles_id,
			'people_id' 		=> $people_id,
			'users_state_id' 	=> $users_state_id,
			'save' 				=> $users_save,
			'edit' 				=> $users_edit,
			'del' 				=> $users_del,
			'created'			=> $date_time,
			'modified'			=> $date_time
		);

		if($this->modelo->addUser($data))
			echo '1';
		else
			echo '0';
	}

	public function editUser() {
		if (!$this->session->userdata('edit')) {
			redirect('welcome');
		}
		$id = trim($this->input->post('id', TRUE));

		$user 			= 	trim($this->input->post('user', TRUE));
		$password 		= 	trim($this->input->post('password', TRUE));
		$roles_id 		= 	trim($this->input->post('roles_id', TRUE));
		$people_id 		= 	trim($this->input->post('people_id', TRUE));
		$users_state_id	= 	trim($this->input->post('users_state_id', TRUE));
		$users_save		= 	trim($this->input->post('users_save', TRUE));
		$users_edit		= 	trim($this->input->post('users_edit', TRUE));
		$users_del		= 	trim($this->input->post('users_del', TRUE));

		$date_time = date('Y-m-d H:i:s');

		//validar recifrado password
		$this->db->select('password');
		$this->db->from('users');
		$this->db->where('id', $id);
		$this->db->limit(1);
		$res = $this->db->get()->result_array();
		if(!empty($res[0]['password']))
			$res = $res[0]['password'];
		else
			$res = 0;

		//si password es igual a res, no ha cambiado su contraseña
		$data = array();
		if($password == $res)
		{
			$data = array(
				'user' 				=> $user,
				'roles_id' 			=> $roles_id,
				'people_id' 		=> $people_id,
				'users_state_id' 	=> $users_state_id,
				'save' 				=> $users_save,
				'edit' 				=> $users_edit,
				'del' 				=> $users_del,
				'modified'			=> $date_time
			);
		}
		else
		{
			$data = array(
				'user' 				=> $user,
				'password' 			=> md5($password),
				'roles_id' 			=> $roles_id,
				'people_id' 		=> $people_id,
				'users_state_id' 	=> $users_state_id,
				'save' 				=> $users_save,
				'edit' 				=> $users_edit,
				'del' 				=> $users_del,
				'modified'			=> $date_time
			);
		}
		

		if($this->modelo->editUser($data, $id)) {
			echo '1';
		}
		else {
			echo '0';
		}
	}

	public function deleteUser() {
		if (!$this->session->userdata('del')) {
			redirect('welcome');
		}
		$id = trim($this->input->post('id', TRUE));

		if($this->modelo->deleteUser($id))
			echo '1';
		else
			echo '0';
	}

	//---------------------------------------------------------------------------------------------
	
    


}