<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CVehicles_Profiles extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('mVehicles_Profiles', 'modelo');
		if (empty($this->session->userdata('options'))) {
			redirect('welcome');
		}
		else if (in_array('10030000', $this->session->userdata('options'))) {
			redirect('welcome');
		}
	}

	public function index() {
		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('vehicles_profiles/index');
	}

	//Datatable
	public function datatable() {
		$start = $this->input->post('start');
		$length = $this->input->post('length');
		$search = $this->input->post('search')['value'];
		$by = $this->input->post('order')['0']['column'];
		$order = $this->input->post('order')['0']['dir'];

		$result = $this->modelo->getVehicles_Profiles($start, $length, $search, $order, $by);

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
            "recordsTotal"    => intval($result['numDataTotal']),
            "recordsFiltered" => intval($result['numDataFilter']),
            "data"            => $result['data']
            );

        echo json_encode($json_data);
	}

	//Vistas
	public function add() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('vehicles_profiles/add');
	}

	public function edit() {
		if (!$this->session->userdata('edit')) {
			redirect('welcome');
		}
		$id = trim($this->input->get('id', TRUE));

		$vehicles_profiles = $this->modelo->getVehicle_Profiles($id);

		$data = array('vehicles_profiles' => $vehicles_profiles);

		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('vehicles_profiles/edit', $data);
	}

	public function view() {
		$id = trim($this->input->get('id', TRUE));

		$vehicles_profiles = $this->modelo->getVehicle_Profiles($id);

		$data = array('vehicles_profiles' => $vehicles_profiles);

		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('vehicles_profiles/view', $data);
	}

	//Crud
	public function addVehicles_Profiles() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$profile 	= trim($this->input->post('vehicles_profiles', TRUE));
		
		$date_time = date('Y-m-d H:i:s');

		$data = array(
			'profile' => $profile,
			'created' => $date_time,
			'modified' => $date_time
		);

		if($this->modelo->addVehicles_Profiles($data)) {
			echo '1';
		}
		else {
			echo '0';
		}
	}

	public function editVehicles_Profiles() {
		if (!$this->session->userdata('edit')) {
			redirect('welcome');
		}
		$id = trim($this->input->post('id', TRUE));

		$profile 	= trim($this->input->post('vehicles_profiles', TRUE));
		
		$date_time = date('Y-m-d H:i:s');

		$data = array(
			'profile' 	=> $profile,
			'modified' 	=> $date_time
		);

		if($this->modelo->editVehicles_Profiles($data, $id))
		{
			echo '1';
		}
		else
		{
			echo '0';
		}
	}

	public function deleteVehicles_Profiles() {
		if (!$this->session->userdata('del')) {
			redirect('welcome');
		}
		$id = trim($this->input->post('id', TRUE)); 

		if($this->modelo->deleteVehicles_Profiles($id)) {
			echo '1';
		}
		else {
			echo '0';
		}
	}
}