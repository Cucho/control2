<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CReasons_Error extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('mReasons_Error', 'modelo');
	}

	public function index() {
		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('reasons_error/index');
	}

	//Datatable
	public function datatable() {
		$start = $this->input->post('start');
		$length = $this->input->post('length');
		$search = $this->input->post('search')['value'];
		$by = $this->input->post('order')['0']['column'];
		$order = $this->input->post('order')['0']['dir'];

		$result = $this->modelo->getReasons_Error($start, $length, $search, $order, $by);

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
            "recordsTotal"    => intval($result['numDataTotal']),
            "recordsFiltered" => intval($result['numDataFilter']),
            "data"            => $result['data']
            );

        echo json_encode($json_data);
	}

	//Vistas
	public function add() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('reasons_error/add');
	}

	public function edit() {
		if (!$this->session->userdata('edit')) {
			redirect('welcome');
		}
		$id = trim($this->input->get('id', TRUE));

		$reasons_error = $this->modelo->getReason_Error($id);
		
		$data = array('reasons_error' => $reasons_error);

		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('reasons_error/edit', $data);
	}

	public function view() {
		$id = trim($this->input->get('id', TRUE));

		$reasons_error = $this->modelo->getReason_Error($id);
		
		$data = array('reasons_error' => $reasons_error);

		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('reasons_error/view', $data);
	}

	//Crud
	public function addReason_Error() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$reason = trim($this->input->post('reason', TRUE));
		
		$date_time = date('Y-m-d H:i:s');

		$data = array(
			'reason' => $reason,
			'created' => $date_time,
			'modified' => $date_time
		);

		if($this->modelo->addReason_Error($data)) {
			echo '1';
		}
		else {
			echo '0';
		}
	}

	public function editReason_Error() {
		if (!$this->session->userdata('edit')) {
			redirect('welcome');
		}
		$id 	= trim($this->input->post('id', TRUE));

		$reason = trim($this->input->post('reason', TRUE));
		
		$date_time = date('Y-m-d H:i:s');

		$data = array(
			'reason' => $reason,
			'modified' => $date_time
		);

		if($this->modelo->editReason_Error($data, $id))
		{
			echo '1';
		}
		else
		{
			echo '0';
		}
	}

	public function delReason_Error() {
		if (!$this->session->userdata('del')) {
			redirect('welcome');
		}
		$id = trim($this->input->post('id', TRUE)); 

		if($this->modelo->delReason_Error($id)) {
			echo '1';
		}
		else {
			echo $this->db->error();
		}
	}

	
}