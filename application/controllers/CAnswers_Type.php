<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CAnswers_Type extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('mAnswers_Type', 'modelo');
	}

	public function index() {
		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('answers_type/index');
	}

	//Datatable
	public function datatable() {
		$start = $this->input->post('start');
		$length = $this->input->post('length');
		$search = $this->input->post('search')['value'];
		$by = $this->input->post('order')['0']['column'];
		$order = $this->input->post('order')['0']['dir'];

		$result = $this->modelo->getAnswers_Type($start, $length, $search, $order, $by);

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
            "recordsTotal"    => intval($result['numDataTotal']),
            "recordsFiltered" => intval($result['numDataFilter']),
            "data"            => $result['data']
            );

        echo json_encode($json_data);
	}

	//Vistas
	public function add() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('answers_type/add');
	}

	public function edit() {
		if (!$this->session->userdata('edit')) {
			redirect('welcome');
		}
		$id = trim($this->input->get('id', TRUE));

		$answers = $this->modelo->getAnswer_Type($id);

		$data = array(
			'answers' => $answers
		);

		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('answers_type/edit', $data);
	}

	public function view() {
		$id = trim($this->input->get('id', TRUE));

		$answers = $this->modelo->getAnswer_Type($id);

		$data = array(
			'answers' => $answers
		);

		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('answers_type/view', $data);
	}

	//Crud
	public function addAnswer_Type() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$type 	= trim($this->input->post('type', TRUE));
		
		$date_time = date('Y-m-d H:i:s');

		$data = array(
			'type' => $type,
			'created' => $date_time,
			'modified' => $date_time
		);

		if($this->modelo->addAnswer_Type($data)) {
			echo '1';
		}
		else {
			echo '0';
		}
	}

	public function editAnswer_Type() {
		if (!$this->session->userdata('edit')) {
			redirect('welcome');
		}
		$id 	= trim($this->input->post('id', TRUE));
		$type 	= trim($this->input->post('type', TRUE));
		
		$date_time = date('Y-m-d H:i:s');

		$data = array(
			'type' => $type,
			'modified' => $date_time
		);

		if($this->modelo->editAnswer_Type($data, $id))
		{
			echo '1';
		}
		else
		{
			echo '0';
		}
	}

	public function deleteAnswer_Type() {
		if (!$this->session->userdata('del')) {
			redirect('welcome');
		}
		$id = trim($this->input->post('id', TRUE)); 

		if($this->modelo->deleteAnswer_Type($id)) {
			echo '1';
		}
		else {
			echo '0';
		}
	}
}