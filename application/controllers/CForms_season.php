<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CForms_Season extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('mForms_Season', 'modelo');
	}

	public function index() {
		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('forms_season/index');
	}

	//Datatable
	public function datatable() {
		$start = $this->input->post('start');
		$length = $this->input->post('length');
		$search = $this->input->post('search')['value'];
		$by = $this->input->post('order')['0']['column'];
		$order = $this->input->post('order')['0']['dir'];

		$result = $this->modelo->getForms_Season($start, $length, $search, $order, $by);

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
            "recordsTotal"    => intval($result['numDataTotal']),
            "recordsFiltered" => intval($result['numDataFilter']),
            "data"            => $result['data']
            );

        echo json_encode($json_data);
	}

	//Vistas
	public function add() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$this->load->view('header');
		$this->load->view('aside');
		$vehicles_type = $this->modelo->getAllVehicles_Type();
		$forms = $this->modelo->getAllForms();
		$seasons = $this->modelo->getAllSeasons();
		$data = array(
			'vehicles_type' => $vehicles_type, 
			'forms' => $forms,
			'seasons' => $seasons
		);
		$this->load->view('forms_season/add', $data);
	}

	//Crud
	public function addForm_Season() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$vechiles_type_id 	= 	trim($this->input->post('vechiles_type_id', TRUE));
		$forms_id 			= 	trim($this->input->post('forms_id', TRUE));
		$seasons_id 		= 	trim($this->input->post('seasons_id', TRUE));
		$year 				= 	trim($this->input->post('year', TRUE));
		
		$data = array(
			'vechiles_type_id' 	=> $vechiles_type_id,
			'forms_id' 			=> $forms_id,
			'seasons_id'		=> $seasons_id,
			'year' 				=> $year
		);

		if($this->modelo->addForm_Season($data)) {
			echo '1';
		}
		else {
			echo '0';
		}
	}

	public function delForms_Season() {
		if (!$this->session->userdata('del')) {
			redirect('welcome');
		}
		$vechiles_type_id 	= trim($this->input->post('vechiles_type_id', TRUE));
		$forms_id			= trim($this->input->post('forms_id', TRUE));
		$seasons_id			= trim($this->input->post('seasons_id', TRUE));
		$year 				= trim($this->input->post('year', TRUE));

		$data = array(
			'vechiles_type_id' 	=> $vechiles_type_id,
			'forms_id' 			=> $forms_id,
			'seasons_id'		=> $seasons_id,
			'year' 				=> $year
		);

		if($this->modelo->deleteForm_Season($data)) {
			echo '1';
		}
		else {
			echo '0';
		}
	}
}