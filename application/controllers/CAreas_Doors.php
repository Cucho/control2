<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CAreas_Doors extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('mAreas_Doors', 'modelo');
		if (empty($this->session->userdata('options'))) {
			redirect('welcome');
		}
		else if (in_array('01020202', $this->session->userdata('options'))) {
			redirect('welcome');
		}
	}

	public function index() {
		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('areas/list_areas_doors');
	}
	//Datatable
	public function datatable() {
		$start = $this->input->post('start');
		$length = $this->input->post('length');
		$search = $this->input->post('search')['value'];
		$by = $this->input->post('order')['0']['column'];
		$order = $this->input->post('order')['0']['dir'];

		$result = $this->modelo->getAreas_Doors($start, $length, $search, $order, $by);

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
            "recordsTotal"    => intval($result['numDataTotal']),
            "recordsFiltered" => intval($result['numDataFilter']),
            "data"            => $result['data']
            );

        echo json_encode($json_data);
	}

	//Vistas
	public function add() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$areas = $this->modelo->getAllAreas();
		$doors = $this->modelo->getAllDoors();

		$data = array(
			'areas' => $areas,
			'doors' => $doors
		);

		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('areas/add_areas_door', $data);
	}

	//Crud
	public function addArea_Door() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$area	= trim($this->input->post('area', TRUE));
		$door	= trim($this->input->post('door', TRUE));

		$date_time = date('Y-m-d H:i:s');

		$data = array(
			'doors_id' 	=> $door,
			'areas_id' 	=> $door
		);

		if($this->modelo->addArea_Door($data))
			echo '1';
		else
			echo '0';
	}


	public function deleteArea_Door() {
		if (!$this->session->userdata('del')) {
			redirect('welcome');
		}
		$area	= trim($this->input->post('area', TRUE));
		$door	= trim($this->input->post('door', TRUE));

		if($this->modelo->deleteArea_Door($door, $area))
			echo '1';
		else
			echo '0';
	}
}