<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CJornadas extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('mJornadas', 'modelo');
		if (empty($this->session->userdata('options'))) {
			redirect('welcome');
		}
		else if (in_array('01060000', $this->session->userdata('options'))) {
			redirect('welcome');
		}
	}

	public function index() {
		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('jornadas/index');
	}

	//Datatable
	public function datatable() {
		$start = $this->input->post('start');
		$length = $this->input->post('length');
		$search = $this->input->post('search')['value'];
		$by = $this->input->post('order')['0']['column'];
		$order = $this->input->post('order')['0']['dir'];

		$result = $this->modelo->getJornadas($start, $length, $search, $order, $by);

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
            "recordsTotal"    => intval($result['numDataTotal']),
            "recordsFiltered" => intval($result['numDataFilter']),
            "data"            => $result['data']
            );

        echo json_encode($json_data);
	}

	//Vistas
	public function add() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('jornadas/add');
	}

	public function edit() {
		if (!$this->session->userdata('edit')) {
			redirect('welcome');
		}
		$id = trim($this->input->get('id', TRUE));

		$jornadas = $this->modelo->getJornada($id);

		$data = array('jornadas' => $jornadas);

		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('jornadas/edit', $data);
	}

	public function view() {
		$id = trim($this->input->get('id', TRUE));

		$jornadas = $this->modelo->getJornada($id);

		$data = array('jornadas' => $jornadas);

		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('jornadas/view', $data);
	}

	//Crud
	public function addJornada() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$jornada 	= trim($this->input->post('jornada', TRUE));
		$time_init 	= trim($this->input->post('time_init', TRUE));
		$time_end 	= trim($this->input->post('time_end', TRUE));
		
		$data = array(
			'jornada' 	=> $jornada,
			'time_init' => $time_init,
			'time_end' 	=> $time_end
		);

		if($this->modelo->addJornada($data)) {
			echo '1';
		}
		else {
			echo '0';
		}
	}

	public function editJornada() {
		if (!$this->session->userdata('edit')) {
			redirect('welcome');
		}
		$id = trim($this->input->post('id', TRUE));

		$jornada 	= trim($this->input->post('jornada', TRUE));
		$time_init 	= trim($this->input->post('time_init', TRUE));
		$time_end 	= trim($this->input->post('time_end', TRUE));
		
		$data = array(
			'jornada' 	=> $jornada,
			'time_init' => $time_init,
			'time_end' 	=> $time_end
		);

		if($this->modelo->editJornada($data, $id)) {
			echo '1';
		}
		else {
			echo '0';
		}
	}

	public function deleteJornada() {
		if (!$this->session->userdata('del')) {
			redirect('welcome');
		}
		$id = trim($this->input->post('id', TRUE)); 

		if($this->modelo->deleteJornada($id)) {
			echo '1';
		}
		else {
			echo '0';
		}
	}
}