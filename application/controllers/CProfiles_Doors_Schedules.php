<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CProfiles_Doors_Schedules extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('mProfiles_Doors_Schedules', 'modelo');
		if (empty($this->session->userdata('options'))) {
			redirect('welcome');
		}
		else if (in_array('08040000', $this->session->userdata('options'))) {
			redirect('welcome');
		}
	}

	// //Vistas
	public function add() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$this->load->view('header');
		$this->load->view('aside');

		$profiles 					= $this->modelo->getAllPeople_Profiles();
		$doors 						= $this->modelo->getAllDoors();
		$profiles_doors_schedules 	= $this->modelo->getAllProfiles_Doors_Schedules();
		$jornadas 					= $this->modelo->getAllJornadas();

		$data = array(
			'pro_doo_sche' 	=> $profiles_doors_schedules,
			'profiles' 		=> $profiles, 
			'doors' 		=> $doors,
			'jornadas'		=> $jornadas
		);

		$this->load->view('profiles_doors_schedules/add', $data);
	}

	//Crud
	public function addProfile_Door_Schedule() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$profiles_people_id 	= 	trim($this->input->post('profiles_people_id', TRUE));
		$jornada 				=	trim($this->input->post('jornada', TRUE));
		$time_init 				= 	trim($this->input->post('time_init', TRUE));
		$time_end 				= 	trim($this->input->post('time_end', TRUE));
		$doors_id 				= 	$this->input->post('doors_id', TRUE);

		$door 					= 	$this->input->post('door', true);

		if($this->modelo->addProfile_Door_Schedule($profiles_people_id, $jornada, $time_init, $time_end, $doors_id, $door)) {
			echo '1';
		}
		else {
			echo '0';
		}
	}
}