<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CAccess_People extends CI_Controller {

	private $description;
	
	function __construct() {
		parent::__construct();
		$this->load->model('mAccess_People', 'modelo');
		$this->description = $this->session->userdata('name').' '.$this->session->userdata('last_name');
		if (empty($this->session->userdata('options'))) {
			redirect('welcome');
		}
		else if (in_array('02000000', $this->session->userdata('options')) && in_array('05020100', $this->session->userdata('options')) ) {
			redirect('welcome');
		}
	}

	public function index() {
		$this->load->view('header');
		$this->load->view('aside');
		$data = array(
			'in' => $this->modelo->getAllEntry0(),
			'out' => $this->modelo->getAllEntry1(),
		);
		$this->load->view('access_people/authorization', $data);
	}

	public function PDF_authorization(){
		$in = $this->modelo->getAllEntry0();
		$out = $this->modelo->getAllEntry1();

		$this->load->library('excel');
		$this->excel->setActiveSheetIndex(0);
		$this->excel->getActiveSheet()->setTitle('Registros de ingresos diarios');

		$this->excel->getActiveSheet()->SetCellValue('A1', 'Rut');
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize('11');
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
		$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$this->excel->getActiveSheet()->SetCellValue('B1', 'Nombre');
		$this->excel->getActiveSheet()->getStyle('B1')->getFont()->setSize('11');
		$this->excel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
		$this->excel->getActiveSheet()->getStyle('B1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$this->excel->getActiveSheet()->SetCellValue('C1', 'Empresa');
		$this->excel->getActiveSheet()->getStyle('C1')->getFont()->setSize('11');
		$this->excel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
		$this->excel->getActiveSheet()->getStyle('C1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$this->excel->getActiveSheet()->SetCellValue('D1', 'Vehiculo');
		$this->excel->getActiveSheet()->getStyle('D1')->getFont()->setSize('11');
		$this->excel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);
		$this->excel->getActiveSheet()->getStyle('D1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$this->excel->getActiveSheet()->SetCellValue('E1', 'Fecha');
		$this->excel->getActiveSheet()->getStyle('E1')->getFont()->setSize('11');
		$this->excel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true);
		$this->excel->getActiveSheet()->getStyle('E1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$fila = 2;
		foreach ($in as $k) {
			$this->excel->getActiveSheet()->setCellValue('A'.$fila, trim($k->rut.'-'.$k->digit));
			$this->excel->getActiveSheet()->setCellValue('B'.$fila, trim($k->name.' '.$k->last_name));
			$this->excel->getActiveSheet()->setCellValue('C'.$fila, trim($k->company));
			$v_id = $k->vehicles_id;
			$patent = 'N/A';
            if( $v_id != 0){
                $this->db->select('patent');
                $this->db->from('vehicles');
                $this->db->where('id', $v_id);
                $res = $this->db->get()->result_array();
                if(!empty($res[0]['patent'])){
                    $patent = $res[0]['patent'];
                }
            }
			$this->excel->getActiveSheet()->setCellValue('D'.$fila, $patent);
			$this->excel->getActiveSheet()->setCellValue('E'.$fila, trim($k->created));
			$fila++;
		}
		for ($col='A'; $col !== 'F' ; $col++) { 
			$this->excel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
		}

		$this->excel->createSheet();
		$this->excel->setActiveSheetIndex(1);
		$this->excel->getActiveSheet()->setTitle('Registros de salidas diarias');

		$this->excel->getActiveSheet()->SetCellValue('A1', 'Rut');
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize('11');
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
		$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$this->excel->getActiveSheet()->SetCellValue('B1', 'Nombre');
		$this->excel->getActiveSheet()->getStyle('B1')->getFont()->setSize('11');
		$this->excel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
		$this->excel->getActiveSheet()->getStyle('B1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$this->excel->getActiveSheet()->SetCellValue('C1', 'Empresa');
		$this->excel->getActiveSheet()->getStyle('C1')->getFont()->setSize('11');
		$this->excel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
		$this->excel->getActiveSheet()->getStyle('C1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$this->excel->getActiveSheet()->SetCellValue('D1', 'Vehiculo');
		$this->excel->getActiveSheet()->getStyle('D1')->getFont()->setSize('11');
		$this->excel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);
		$this->excel->getActiveSheet()->getStyle('D1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$this->excel->getActiveSheet()->SetCellValue('E1', 'Fecha');
		$this->excel->getActiveSheet()->getStyle('E1')->getFont()->setSize('11');
		$this->excel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true);
		$this->excel->getActiveSheet()->getStyle('E1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$filab = 2;
		foreach ($out as $kk) {
			$this->excel->getActiveSheet()->setCellValue('A'.$filab, trim($kk->rut.'-'.$kk->digit));
			$this->excel->getActiveSheet()->setCellValue('B'.$filab, trim($kk->name.' '.$kk->last_name));
			$this->excel->getActiveSheet()->setCellValue('C'.$filab, trim($kk->company));
			$v_id = $kk->vehicles_id;
			$patent = 'N/A';
            if( $v_id != 0){
                $this->db->select('patent');
                $this->db->from('vehicles');
                $this->db->where('id', $v_id);
                $res = $this->db->get()->result_array();
                if(!empty($res[0]['patent'])){
                    $patent = $res[0]['patent'];
                }
            }
			$this->excel->getActiveSheet()->setCellValue('D'.$filab, $patent);
			//$this->excel->getActiveSheet()->setCellValue('E'.$filab, trim($kk->created));
			$this->excel->getActiveSheet()->setCellValue('E'.$filab, trim($kk->exit_time));
			$filab++;
		}
		for ($colb='A'; $colb !== 'F' ; $colb++) { 
			$this->excel->getActiveSheet()->getColumnDimension($colb)->setAutoSize(true);
		}

		$this->excel->setActiveSheetIndex(0);
		$filename = 'Registro movimientos diarios '.date('Y-m-d').'.xls';
		header('Content-Type: application/vnd.ms-excel;charset=utf-8');
		header('Content-Disposition: attachment;filename="'.$filename.'"');
		header('Cache-Control: max-age=0');
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
		$objWriter->save('php://output');
	}

	public function list(){
		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('access_people/list');
	}

	//--------------------------------------------------------

	public function generateAuthorization() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$rut = trim($this->input->post("rut", TRUE));
		$rutb = $rut;
		
		$digit = substr($rut, -1);
		$rut = substr($rut, 0, (strlen($rut)-2));
		
		$people = $this->modelo->getPeople_Rut($rut);
		$companies = $this->modelo->getAllCompanies();
		$people_profile = $this->modelo->getAllPeople_Profile();
		$access_state = $this->modelo->getAllAccess_State();
		$peopleVisited = $this->modelo->getAllPeople_Visit();
		

		if (!empty($people)) {
			//persona existente
			if($this->isPeopleIn($people[0]['id']) == false)
			{
				$data = array(
					'response' 	=> 1,
					'people' 	=> $people
				);
				echo json_encode($data);
			}
			else
			{
				$data = array(
					'response' 	=> 2,
					'people' 	=> $people
				);
				echo json_encode($data);
			}
			
		}
		else 
		{
			//persona no existe
			$data = array(
				'response' 			=> 0,
				'rut' 				=> $rutb,
				'companies' 		=> $companies,
				'people_profile' 	=> $people_profile,
				'access_state' 		=> $access_state,
				'peopleVisited' 	=> $peopleVisited,
			);
			echo json_encode($data);
		}
	}

	private function isPeopleIn($id)
	{
		$this->db->select('id');
		$this->db->from('access_people');
		$this->db->where('people_id', $id);
		$this->db->where('entry', 0);
		$this->db->limit(1);
		$res = $this->db->get()->result_array();

		if(!empty($res[0]['id']))
			return true;
		else
			return false;
	}

	//----------------------------------------------------------

	public function authorization() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$rut = trim($this->input->post("inputrut", TRUE));
		$rutb = $rut;
		
		$digit = substr($rut, -1);
		$rut = substr($rut, 0, (strlen($rut)-2));
		
		$people = $this->modelo->getPeople_Rut($rut);
		$companies = $this->modelo->getAllCompanies();
		$people_profile = $this->modelo->getAllPeople_Profile();
		$access_state = $this->modelo->getAllAccess_State();
		$peopleVisited = $this->modelo->getAllPeople_Visit();
		$treeview = $this->modelo->GetTreeview();
		$doorlevel = $this->modelo->getDoorLevel();

		$vehicle_type = $this->modelo->getAllVehicle_Type();
		$vehicle_profiles = $this->modelo->getAllVehicleProfiles();
		$peopleResponsable = $this->modelo->getAllPeople();
		$formularios = $this->modelo->getAllForm();
		

		if (!empty($people)) {
			$data = array(
				'people' => $people,
				'rut'	=> $rutb,
				'companies' => $companies,
				'people_profile' => $people_profile,
				'access_state' => $access_state,
				'peopleVisited' => $peopleVisited,
				'treeview' => $treeview,
				'doorlevel' => $doorlevel,
				'vehicle_type' => $vehicle_type,
				'vehicle_profiles' => $vehicle_profiles,
				'peopleResponsable' => $peopleResponsable,
				'form' => $formularios
			);

			$this->load->view('header');
			$this->load->view('aside');
			$this->load->view('access_people/add', $data);
		}
		else {
			
			$data = array(
				'rut' => $rutb,
				'companies' => $companies,
				'people_profile' => $people_profile,
				'access_state' => $access_state,
				'peopleVisited' => $peopleVisited,
				'treeview' => $treeview,
				'doorlevel' => $doorlevel,
				'vehicle_type' => $vehicle_type,
				'vehicle_profiles' => $vehicle_profiles,
				'peopleResponsable' => $peopleResponsable,
				'form' => $formularios
			);

			$this->load->view('header');
			$this->load->view('aside');
			$this->load->view('access_people/add', $data);
		}
	}

	public function authorizationtwo() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$rut = trim($this->input->get("inputrut", TRUE));
		$rutb = trim($this->input->get("inputrut", TRUE));
		
		$digit = substr($rut, -1);
		$rut = substr($rut, 0, (strlen($rut)-2));
		
		$people = $this->modelo->getPeople_Rut($rut);
		$companies = $this->modelo->getAllCompanies();
		$people_profile = $this->modelo->getAllPeople_Profile();
		$access_state = $this->modelo->getAllAccess_State();
		$peopleVisited = $this->modelo->getAllPeople_Visit();
		$treeview = $this->modelo->GetTreeview();
		$doorlevel = $this->modelo->getDoorLevel();
		

		if (count($people) > 0) {
			$data = array(
				'people' => $people,
				'rut'	=> $rutb,
				'companies' => $companies,
				'people_profile' => $people_profile,
				'access_state' => $access_state,
				'peopleVisited' => $peopleVisited,
				'treeview' => $treeview,
				'doorlevel' => $doorlevel
			);

			$this->load->view('header');
			$this->load->view('aside');
			$this->load->view('access_people/add', $data);
		}
		else {
			
			$data = array(
				'rut' => $rutb,
				'companies' => $companies,
				'people_profile' => $people_profile,
				'access_state' => $access_state,
				'peopleVisited' => $peopleVisited,
				'treeview' => $treeview,
				'doorlevel' => $doorlevel
			);

			$this->load->view('header');
			$this->load->view('aside');
			$this->load->view('access_people/add', $data);
		}
	}

	public function getPerson_rut() {
		$rut = trim($this->input->post("rut", TRUE));
		$rutb = trim($this->input->post("rut", TRUE));
		
		$digit = substr($rut, -1);
		$rut = substr($rut, 0, (strlen($rut)-2));
		
		$people = $this->modelo->getPeople_Rut($rut);
		$companies = $this->modelo->getAllCompanies();
		$people_profile = $this->modelo->getAllPeople_Profile();

		if (!empty($people)) {
			$data = array(
				'people' => $people,
				'rut'	=> $rutb,
				'companies' => $companies,
				'people_profile' => $people_profile
			);

			echo json_encode($data);
		}
		else {
			
			$data = array(
				'people' => '',
				'rut' => $rutb,
				'companies' => $companies,
				'people_profile' => $people_profile
			);

			echo json_encode($data);
		}
	}

	public function addPeople() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$rut 				= 	trim($this->input->post('rut', TRUE));
		$dv 				= 	trim($this->input->post('digit', TRUE));
		$name 				= 	trim($this->input->post('name', TRUE));
		$last_name 			= 	trim($this->input->post('last_name', TRUE));
		$address 			= 	trim($this->input->post('address', TRUE));
		$email 				= 	trim($this->input->post('email', TRUE));
		$phone 				= 	trim($this->input->post('phone', TRUE));
		$allow_all 			= 	trim($this->input->post('allow_all', TRUE));
		$is_visited 		= 	trim($this->input->post('is_visited', TRUE));
		$people_profiles_id = 	trim($this->input->post('people_profiles_id', TRUE));
		$companies_id 		= 	trim($this->input->post('companies_id', TRUE));
		$internal 			= 	trim($this->input->post('internal', TRUE));

		$date_time = date('Y-m-d H:i:s');

		$data = array(
			'rut' 				=> $rut,
			'digit' 			=> $dv,
			'name' 				=> $name,
			'last_name' 		=> $last_name,
			'address' 			=> $address,
			'email' 			=> $email,
			'phone' 			=> $phone,
			'allow_all' 		=> $allow_all,
			'is_visited' 		=> $is_visited,
			'people_profiles_id'=> $people_profiles_id,
			'companies_id' 		=> $companies_id,
			'internal' 			=> $internal,
			'created'			=> $date_time,
			'modified'			=> $date_time
		);

		if($this->modelo->addPeople($data))
			echo '1';
		else
			echo '0';
	}

	public function addPeople_getID() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$rut 				= 	trim($this->input->post('rut', TRUE));
		$dv 				= 	trim($this->input->post('digit', TRUE));
		$name 				= 	trim($this->input->post('name', TRUE));
		$last_name 			= 	trim($this->input->post('last_name', TRUE));
		$address 			= 	trim($this->input->post('address', TRUE));
		$email 				= 	trim($this->input->post('email', TRUE));
		$phone 				= 	trim($this->input->post('phone', TRUE));
		$allow_all 			= 	trim($this->input->post('allow_all', TRUE));
		$is_visited 		= 	trim($this->input->post('is_visited', TRUE));
		$people_profiles_id = 	trim($this->input->post('people_profiles_id', TRUE));
		$companies_id 		= 	trim($this->input->post('companies_id', TRUE));
		$internal 			= 	trim($this->input->post('internal', TRUE));

		$date_time = date('Y-m-d H:i:s');

		$data = array(
			'rut' 				=> $rut,
			'digit' 			=> $dv,
			'name' 				=> $name,
			'last_name' 		=> $last_name,
			'address' 			=> $address,
			'email' 			=> $email,
			'phone' 			=> $phone,
			'allow_all' 		=> $allow_all,
			'is_visited' 		=> $is_visited,
			'people_profiles_id'=> $people_profiles_id,
			'companies_id' 		=> $companies_id,
			'internal'			=> $internal,
			'created'			=> $date_time,
			'modified'			=> $date_time
		);

		if($this->modelo->addPeople($data))
			echo $this->db->insert_id();
		else
			echo '0';
	}
	//------------------------------------------------------------------------------

	public function getAllReasons_Visit() {
		$reason = $this->modelo->getAllReasons_Visit();
		
		echo json_encode($reason);
	}

	public function addReason_Visit() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$reason 		= 	trim($this->input->post('reason', TRUE));
		
		$date_time = date('Y-m-d H:i:s');

		$data = array(
			'reason' 	=> $reason,
			'created' 	=> $date_time,
			'modified' 	=> $date_time
		);

		echo $this->modelo->addReasons_Visit($data);
	}

	public function generateAccessOut() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$id = trim($this->input->post("id", TRUE));
		$observation = trim($this->input->post("observation", TRUE));

		$date_time = date('Y-m-d H:i:s');

		if($this->modelo->generateAccessOut($id, $date_time, $observation))
			echo '1';
		else
			echo '0';
	}

	public function generateAccessOut2() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$id = trim($this->input->post("access_people_id", TRUE));
		$tobservation = trim($this->input->post("tobservation", TRUE));

		$title = trim($this->input->post("title", TRUE));
		$observation = trim($this->input->post("observation", TRUE));
		$order = $this->input->post("order", TRUE);
		$answers = $this->input->post("answers", TRUE);
		$forms_id = trim($this->input->post("forms_id", TRUE));
		$rut = $this->input->post("rut", TRUE);

		$date_time = date('Y-m-d H:i:s');

		$idp = $this->modelo->SearchAccess_People($rut);
	    
	    if ($id == 0) {
	      	$id = $this->modelo->SearchAccess_People($rut);
	      	if ($this->modelo->generateAccessOut($id[0]->access_people_id, $date_time, $tobservation)) {
	      		$mid = $this->modelo->getMainDoor($this->session->userdata('ip'))[0]->id;
	      		$data =  array(
					'access_people_id' => $id[0]->access_people_id,
					'doors_id'	=> $this->modelo->getMainAccessId($mid)[0]->doors_id,
					'entry' => '1',
					'success' => '1',
					'flow' => '0',
					'created' => $date_time
				);
				$this->modelo->generateAccesIntentOut($data);/////
	        	$this->modelo->deleteCurrentInternalState($id[0]->people_id);
	        	echo '1';
	      	}
	    }
	    else if($this->modelo->generateAccessOut($id, $date_time, $tobservation)) {
	    	$mid = $this->modelo->getMainDoor($this->session->userdata('ip'))[0]->id;
	    	$data =  array(
					'access_people_id' => $id,
					'doors_id'	=> $this->modelo->getMainAccessId($mid)[0]->doors_id,
					'entry' => '1',
					'success' => '1',
					'flow' => '0',
					'created' => $date_time
				);
			$this->modelo->generateAccesIntentOut($data);/////
	      	$this->modelo->deleteCurrentInternalState($idp[0]->people_id);
			$this->modelo->accessPeopleForms2($id, $forms_id, $title, $observation);
			for ($y=0; $y < count($answers); $y++) { 
				$data = array(
					'access_people_id' 	=> $id,
					'forms_id' 			=> $forms_id,
					'order' 			=> $order[$y],
					'answer' 			=> $answers[$y],
					'control'			=> 1
				);
				$this->modelo->accessPeopleAnswers($data);
			}
			echo '1';
		}
		else
			echo '0';
	}

	public function generateAccessIn() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$mensaje = 0;
		
		$entry 				= 0;
		$exit_time 			= '0000-00-00 00:00:00';
		$access_state_id 	= $this->input->post('access_state_id');
		$date_time 			= date('Y-m-d H:i:s');
		$end_time 			= trim($this->input->post('end_time', true)); //esta hh:mm yyyy-mm-dd
		$end_time 			= explode(" ", $end_time);
		$people_id 			= $this->input->post('people_id');//array con persona responsable + adicionales
		$observation 		= $this->input->post('observation');
		$hours 				= $this->input->post('hours');

		$people_areas 		= $this->input->post('people_areas');
		$people_department 	= $this->input->post('people_department');
		$people_zones 		= $this->input->post('people_zones');
		$reasons_visit 		= $this->input->post('reasons_visit');
		$route 				= $this->input->post('route');
		$people_visit 		= $this->input->post('people_visit');

		$vehicle_id 	= $this->input->post('vehicle_id');
		$control_init 	= $this->input->post('control_init');
		$control_end 	= $this->input->post('control_end');

		$form_id 		= trim($this->input->post('form_id', TRUE));
		$form_id_end	= trim($this->input->post('form_id_end', TRUE));
		$ftitle 		= trim($this->input->post('ftitle', TRUE));
		$fobservation 	= trim($this->input->post('fobservation', TRUE));
		$forder 		= $this->input->post('forder');
		$answer 		= $this->input->post('answer');

		if ($access_state_id == 3) {
			$entry = 1;
			$exit_time = $date_time;
		}

		$datadata = array();

		for ($l=0; $l < count($people_id); $l++) { //esta recorriendo o grabando 1 sola vez
			$access_people_id;
			if ($l == 0) {
				$data = array(
					'entry' 			=> $entry,
					'exit_time' 		=> $exit_time,
					'hours' 			=> $hours,
					'end_time' 			=> $end_time[1]." ".$end_time[0],
					'people_id' 		=> $people_id[$l],
					'access_state_id' 	=> $access_state_id,
					'main_access_id' 	=> $this->modelo->getMainDoor($this->session->userdata('ip'))[0]->id,
					'approved_by' 		=> $this->session->userdata('people_id'),
					'observation' 		=> $observation,
					'vehicles_id'		=> $vehicle_id,
					'control_init'		=> $control_init,
					'control_end'		=> $control_end,
					'created' 			=> $date_time,
					'modified' 			=> $date_time
				);
				$access_people_id = $this->modelo->generateAccessIn($data);

				if ($control_init == 1) {
					$data = array(
						'access_people_id' 	=> $access_people_id,
						'forms_id' 			=> $form_id,
						'title' 			=> $ftitle,
						'observation' 		=> $fobservation,
						'control'			=> 0
					);
					$this->modelo->accessPeopleForms($data);
				}
				
				if ($control_end == 1) {
					$data = array(
						'access_people_id' 	=> $access_people_id,
						'forms_id'			=> $form_id_end,
						'control'			=> 1
					);
					$this->modelo->accessPeopleForms($data);
				}
				

				if (count($answer) > 0) {
					for ($y=0; $y < count($answer); $y++) { 
						$data = array(
							'access_people_id' 	=> $access_people_id,
							'forms_id' 			=> $form_id,
							'order' 			=> $forder[$y],
							'answer' 			=> $answer[$y],
							'control'			=> 0
						);
						$this->modelo->accessPeopleAnswers($data);
					}
				}
			}
			else {
				$data = array(
					'entry' 			=> $entry,
					'exit_time' 		=> $exit_time,
					'hours' 			=> $hours,
					'end_time' 			=> $end_time[1]." ".$end_time[0],
					'people_id' 		=> $people_id[$l],
					'access_state_id' 	=> $access_state_id,
					'main_access_id' 	=> $this->modelo->getMainDoor($this->session->userdata('ip'))[0]->id,
					'approved_by' 		=> $this->session->userdata('people_id'),
					'observation' 		=> $observation,
					'vehicles_id'		=> 0,
					'control_init'		=> 0,
					'control_end'		=> 0,
					'created' 			=> $date_time,
					'modified' 			=> $date_time
				);
				$access_people_id = $this->modelo->generateAccessIn($data);
			}
			
			for ($i=0; $i < count($people_areas); $i++) { 
				$data = array('access_people_id' => $access_people_id, 'areas_id' => $people_areas[$i]);
				$this->modelo->inAccessPeopleAreas($data);
			}
			for ($i=0; $i < count($people_department); $i++) { 
				$data = array('access_people_id' => $access_people_id, 'departments_id' => $people_department[$i]);
				$this->modelo->inAccessPeopleDepartment($data);
			}
			for ($i=0; $i < count($people_zones); $i++) { 
				$data = array('access_people_id' => $access_people_id, 'zones_id' => $people_zones[$i]);
				$this->modelo->inAccessPeopleZones($data);
			}
			for ($i=0; $i < count($reasons_visit); $i++) { 
				$data = array('access_people_id' => $access_people_id, 'reasons_visit_id' => $reasons_visit[$i]);
				$this->modelo->inAccessPeopleReasonVisit($data);
			}
			for ($i=0; $i < count($route); $i++) { 
				$data = array('access_people_id' => $access_people_id, 'doors_id' => $route[$i]);
				$this->modelo->inAccessPeopleRoute($data);
			}
			for ($i=0; $i < count($people_visit); $i++) { 
				$data = array('access_people_id' => $access_people_id, 'people_id' => $people_visit[$i]);
				$this->modelo->inAccessPeopleVisit($data);
			}

			$data = array(
				'access_people_id' 	=> $access_people_id,
				'access_state_id' 	=> $access_state_id,
				'description' 		=> $this->description,
				'created'			=> $date_time
			);
		
			if($this->modelo->generateInAccessStateHistory($data)) {
				if ($access_state_id == 2) {
					array_push($datadata, array(
						'mensaje' 			=> 1,
						'access_people_id' 	=> $access_people_id,
						'people' 			=> $this->modelo->getPeopleByAccessPeopleID($access_people_id),
						'time_entrada' 		=> $date_time,
						'time_end' 			=> $end_time[1]." ".$end_time[0]
					));
				}
				else {
					array_push($datadata,array('mensaje' => 1));
				}
				
			}
			else {
				array_push($datadata,array('mensaje' => 0));
			}
			
		}
		echo json_encode($datadata);	
	}

	public function getDetail() {

		$id = trim($this->input->get('id', true));
		
		$access_people = $this->modelo->getDetail_Access_People($id);
		$access_people_zones = $this->modelo->getDetail_Access_People_Zones($id);
		$access_people_areas = $this->modelo->getDetail_Access_People_Areas($id);
		$access_people_departments = $this->modelo->getDetail_Access_People_Departments($id);
		$access_people_intents = $this->modelo->getDetail_Access_People_Intents($id);
		$access_people_reasons = $this->modelo->getDetail_Access_People_Reason_Visit($id);
		$access_people_route = $this->modelo->getDetail_Access_People_Route($id);
		$access_people_state_history = $this->modelo->getDetail_Access_People_State_History($id);
		$access_people_visit = $this->modelo->getDetail_Access_People_Visit($id);
		$access_people_vehicle = $this->modelo->getDetail_Access_People_Vehicle($id);
		$access_people_control = $this->modelo->getDetail_Access_People_Control($id);
		$access_people_form_in = $this->modelo->getDetail_Access_People_Form_in($id);
		
		$access_people_form_inDetail = '';
		$access_people_form_outDetail =  '';

		if (!empty($access_people_form_in)) {
			$access_people_form_inDetail = $this->modelo->SearchFormDetail($access_people_form_in[0]->forms_id);
			$getDetail_Access_People_Form_in_answers = $this->modelo->getDetail_Access_People_Form_answers($id, $access_people_form_in[0]->forms_id, 0);

			$access_people_form_outDetail = $this->modelo->SearchFormDetail($access_people_form_in[1]->forms_id);
			$getDetail_Access_People_Form_out_answers = $this->modelo->getDetail_Access_People_Form_answers($id, $access_people_form_in[1]->forms_id, 1);
		}
		else {
			$access_people_form_inDetail = '';
			$getDetail_Access_People_Form_in_answers = '';
			$getDetail_Access_People_Form_out_answers = '';
		}
		//print_r($access_people_form_inDetail);
		$data = array(
			'myid' => $id,///////
			'access_people' => $access_people,
			'access_people_zones' => $access_people_zones,
			'access_people_areas' => $access_people_areas,
			'access_people_departments' => $access_people_departments,
			'access_people_intents' => $access_people_intents,
			'access_people_reasons' => $access_people_reasons,
			'access_people_route' => $access_people_route,
			'access_people_state_history' => $access_people_state_history,
			'access_people_visit' => $access_people_visit,
			'access_people_vehicle' => $access_people_vehicle,
			'access_people_control' => $access_people_control,
			'access_people_form_in' => $access_people_form_in,
			'access_people_form_inDetail' => $access_people_form_inDetail,
			'access_people_form_outDetail' => $access_people_form_outDetail,
			'getDetail_Access_People_Form_in_answers' => $getDetail_Access_People_Form_in_answers,
			'getDetail_Access_People_Form_out_answers' => $getDetail_Access_People_Form_out_answers
		);
		
		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('access_people/auth_view', $data);
	}

	public function getDetailPDF() {

		$id = trim($this->input->get('id', true));
		
		$access_people_form_in = $this->modelo->getDetail_Access_People_Form_in($id);
		
		if (!empty($access_people_form_in)) {
			$access_people_form_inDetail = $this->modelo->SearchFormDetail($access_people_form_in[0]->forms_id);
			$getDetail_Access_People_Form_in_answers = $this->modelo->getDetail_Access_People_Form_answers($id, $access_people_form_in[0]->forms_id, 0);

			$access_people_form_outDetail = $this->modelo->SearchFormDetail($access_people_form_in[1]->forms_id);
			$getDetail_Access_People_Form_out_answers = $this->modelo->getDetail_Access_People_Form_answers($id, $access_people_form_in[1]->forms_id, 1);
		}
		else {
			$access_people_form_inDetail = '';
			$getDetail_Access_People_Form_in_answers = '';
			$getDetail_Access_People_Form_out_answers = '';
		}
		//print_r($access_people_form_inDetail);
		$data = array(
			'access_people' => $this->modelo->getCreated($id),
			'access_people_form_in' => $access_people_form_in,
			'access_people_form_inDetail' => $access_people_form_inDetail,
			'access_people_form_outDetail' => $access_people_form_outDetail,
			'getDetail_Access_People_Form_in_answers' => $getDetail_Access_People_Form_in_answers,
			'getDetail_Access_People_Form_out_answers' => $getDetail_Access_People_Form_out_answers
		);

		$date_time = date('Y-m-d');
		
		$filename = "Registro de ingresos diarios ".$date_time.".pdf";
		//$html = $this->load->view('access_people/pdf2', $data, true);
		$html = $this->load->view('access_people/pdf', $data, true);
		$this->load->library('M_pdf');
		$this->m_pdf->pdf->WriteHTML($html);
		
		$this->m_pdf->pdf->Output($filename, "I");//D		
	}

	public function SearchVehicle(){
		$patent = trim($this->input->post('patent', TRUE));
		
		$vehicle = $this->modelo->SearchVehicle($patent);

		if (count($vehicle) > 0) {
			$data = array(
				'mensaje' => 1,
				'vehicle' => $vehicle
			);
		}
		else {
			$data = array(
				'mensaje' => 0
			);
		}
		echo json_encode($data);
	}

	public function addVehicle(){
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$patent             	= trim($this->input->post("patent", TRUE));
        $model              	= trim($this->input->post("model", TRUE));
        $internal           	= trim($this->input->post("internal", TRUE));
        $nfc_code           	= trim($this->input->post("nfc_code", TRUE));
        $companies_id       	= trim($this->input->post("companies_id", TRUE));
        $people_id           	= trim($this->input->post("people_id", TRUE));
        $vehicles_type_id		= trim($this->input->post("vehicles_type_id", TRUE));
        $vehicles_profiles_id 	= trim($this->input->post("vehicles_profiles_id", TRUE));

        // $people_id = '';
        // foreach ($this->modelo->searchPeople($rut) as $k) {
        // 	$people_id = $k->id;
        // }

        $date_time = date('Y-m-d H:i:s');

        $data = array(
        	'patent' 				=> $patent,
        	'model' 				=> $model,
        	'internal' 				=> $internal,
        	'nfc_code' 				=> $nfc_code,
        	'companies_id' 			=> $companies_id,
        	'people_id' 			=> $people_id,
        	'vehicles_type_id' 		=> $vehicles_type_id,
        	'vehicles_profiles_id' 	=> $vehicles_profiles_id,
        	'created'				=> $date_time,
        	'modified'				=> $date_time
        );
        $mensaje = $this->modelo->addVehicle($data);
        if ($mensaje != false) {
        	echo $mensaje;
        }
        else {
        	echo '0';
        }
    }

    public function LoadForm(){
    	$form = trim($this->input->post('form', TRUE));

    	$data = array('form' => $this->modelo->SearchFormDetail($form));
    	$this->load->view('access_people/form', $data);
    }

    public function searchPeopleControl(){//
    	$rut = $this->input->get('rut');
    	$rut = substr($rut, 0, -2);

    	$date_time = date('Y-m-d H:i:s');
    	
    	$a = $this->modelo->searchPeopleControl($rut);
    	
    	if (count($a) == 0) { //0 = persona sin auto
    		$p = $this->modelo->SearchAccess_People($rut);//people_id, access_people_id, end_time
    		$tobservation = '';
			if ($p[0]->end_time > $date_time) {
				$this->modelo->generateAccessOut($p[0]->access_people_id, $date_time, $tobservation);
				//revisar
				$mid = $this->modelo->getMainDoor($this->session->userdata('ip'))[0]->id;
				$data =  array(
					'access_people_id' => $p[0]->access_people_id,
					'doors_id'	=> $this->modelo->getMainAccessId($mid)[0]->doors_id,
					'entry' => '1',
					'success' => '1',
					'flow' => '0',
					'created' => $date_time
				);
				$this->modelo->generateAccesIntentOut($data);
				//
				$this->modelo->deleteCurrentInternalState($p[0]->people_id);
				redirect('cAccess_People/', 'refresh');
			}
			else if($p[0]->end_time < $date_time){
				$data = array(
					'excedido' 			=> true,
					'access_people_id' 	=> $p[0]->access_people_id,
					'control_end' 		=> 0,
					'forms_id' 			=> 0,
					'rut' 				=> $rut
				);
				
				$this->load->view('aside');
				$this->load->view('/access_people/salida', $data);
			}
    	}
    	else { // persona con auto
    		if ($a[0]->end_time > $date_time) {
				$data = array(
					'form' => $a,
					'excedido' => false,
					'control_end' => $a[0]->control_end,
					'access_people_id' => $a[0]->access_people_id,
					'forms_id' => $a[0]->forms_id,
					'rut' => $rut,
				);
				
				$this->load->view('aside');
				$this->load->view('/access_people/salida', $data);
			}
			else if($a[0]->end_time < $date_time){
				$excedido = true;
				$data = array(
					'form' => $a,
					'excedido' => true,
					'control_end' => $a[0]->control_end,
					'access_people_id' => $a[0]->access_people_id,
					'forms_id' => $a[0]->forms_id,
					'rut' => $rut,
				);
				
				$this->load->view('aside');
				$this->load->view('/access_people/salida', $data);
			}
    	}
    }

    public function datain(){
		$start = $this->input->post('start');
		$length = $this->input->post('length');
		$search = $this->input->post('search')['value'];
		$by = $this->input->post('order')['0']['column'];
		$order = $this->input->post('order')['0']['dir'];

		$result = $this->modelo->getControlIn($start, $length, $search, $order, $by, 0);

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
            "recordsTotal"    => intval($result['numDataTotal']),
            "recordsFiltered" => intval($result['numDataFilter']),
            "data"            => $result['data']
            );

		echo json_encode($json_data);
	}

	public function dataout(){
		$start = $this->input->post('start');
		$length = $this->input->post('length');
		$search = $this->input->post('search')['value'];
		$by = $this->input->post('order')['0']['column'];
		$order = $this->input->post('order')['0']['dir'];

		$result = $this->modelo->getControlOut($start, $length, $search, $order, $by, 1);

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
            "recordsTotal"    => intval($result['numDataTotal']),
            "recordsFiltered" => intval($result['numDataFilter']),
            "data"            => $result['data'],
            );

        echo json_encode($json_data);
	}

	public function details(){
		$apf_id = $this->input->post('internal_id');
		$control = $this->input->post('control');

		$details = $this->modelo->getDetailsInternalForms($apf_id, $control);

		$data = array(
			'mensaje' 	=> count($details),
			'det' 		=> $details
		);

		echo json_encode($data);
	}

	public function detailsPDF(){
		$apf_id = $this->input->get('id');
		$control = $this->input->get('mov');

		$details = $this->modelo->getDetailsInternalForms($apf_id, $control);
		$data = array('details' => $details);

		$mov = '';
		if ($control == 0) { $mov = 'Ingresos'; } else if($control == 1){ $mov = 'Salidas';}
		$filename = "Control interno de ".$mov.'.pdf';
		$html = $this->load->view('access_people/visitpdf', $data, true);
		$this->load->library('M_pdf');
		$this->m_pdf->pdf->WriteHTML($html);
		
		$this->m_pdf->pdf->Output($filaname, "I");//D
	}

	//--------------------------------------------------------------
	public function List_Access_People()
	{
		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('access_people/list_access_people');
	}

	//Datatable
	public function datatable() {
		$start = $this->input->post('start');
		$length = $this->input->post('length');
		$search = $this->input->post('search')['value'];
		$by = $this->input->post('order')['0']['column'];
		$order = $this->input->post('order')['0']['dir'];

		$result = $this->modelo->getAccess_People($start, $length, $search, $order, $by);

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
            "recordsTotal"    => intval($result['numDataTotal']),
            "recordsFiltered" => intval($result['numDataFilter']),
            "data"            => $result['data']
            );

        echo json_encode($json_data);
	}

	public function deleteAccessPeople()
	{
		if (!$this->session->userdata('del')) {
			redirect('welcome');
		}
		$id = trim($this->input->post('id', TRUE));

		if($this->modelo->delete_Access_People_Zones($id))
			if($this->modelo->delete_Access_People_Areas($id))
				if($this->modelo->delete_Access_People_Departments($id))
					if($this->modelo->delete_Access_People_Visit($id))
						if($this->modelo->delete_Access_People_State_History($id))
							if($this->modelo->delete_Access_People_Route($id))
								if($this->modelo->delete_Access_People_Reasons($id))
									if($this->modelo->delete_Access_People_Intents($id))
										if($this->modelo->delete_Access_People($id))
											echo 1;
										else
											echo 0;
									else
										echo 0;
								else
									echo 0;
							else
								echo 0;
						else
							echo 0;
					else
						echo 0;
				else
					echo 0;
			else
				echo 0;
		else
			echo 0;
	}

	public function in_people(){
		echo json_encode($this->modelo->in_people_list());
	}

}