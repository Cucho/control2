<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('mUsers', 'modelo');
	}

	public function index()
	{
		$this->modelo->checkAdmin();
		
		if (empty($this->session->userdata('options'))) {
			$this->vlogin();
		}
		else {
			$this->load->view('header');
			$this->load->view('aside');
			$this->load->view('wrapper');
			$this->load->view('footer');
		}
		// $this->load->view('control_aside');
	}

	public function index2()
	{	
		if (empty($this->session->userdata('options'))) {
			$this->vlogin();
		}
		else {
			$this->load->view('header');
			$this->load->view('aside');
			$this->load->view('wrapper2');
			$this->load->view('footer');
		}
		// $this->load->view('control_aside');
	}

	public function vlogin()
	{
		$this->load->view('login');
	}

	//login pendiente
	public function Login()
	{
		$user = trim($this->input->post('user', TRUE));
		$password = md5(trim($this->input->post('password', TRUE)));
		$ip = trim($this->input->post('ip', TRUE));

		$data = $this->modelo->getUserSession($user, $password);

		$asession_array = array();
		if(!empty($data))
		{
			if($data[0]['users_state_id'] == 1)
			{
				$session_array = array(
					'user' => $user,
					'password' => $password,
					'rol' => $data[0]['rol'],
					'roles_id' => $data[0]['roles_id'],
					'users_id' => $data[0]['id'],
					'name'=> $data[0]['name'],
					'last_name' => $data[0]['last_name'],
					'state' => $data[0]['state'],
					'created' => $data[0]['created'],
					'modified' => $data[0]['modified'],
					'ip' => $ip,
					'people_id' => $data[0]['people_id'],
					'rut' => $data[0]['rut'], 
					'digit' => $data[0]['digit'], 
					'address' => $data[0]['address'], 
					'email' => $data[0]['email'], 
					'phone' => $data[0]['rut'],
					'allow_all' => $data[0]['allow_all'],
					'is_visited' => $data[0]['is_visited'],
					'internal' => $data[0]['internal'],
					'nfc_code' => $data[0]['nfc_code'],
					'people_profiles_id' => $data[0]['people_profiles_id'],
					'companies_id' => $data[0]['companies_id'],
					'departments_id' => $data[0]['departments_id'],
					'save' => $data[0]['save'],
					'edit' => $data[0]['edit'],
					'del' => $data[0]['del'],
					'main' => $this->modelo->getMainAccessIp(''),//falla en aside si se cambia.
					'options' => $this->modelo->getOptions_Rol($data[0]['roles_id'])

				);

				//SESSION START
				$this->session->set_userdata($session_array);

				header('Location: '.base_url().'index.php/Welcome/index');
			}
			else
			{
				$mensaje = array('mensaje' => '<h6><font color="red">Tu cuenta esta temporalmente suspendida.<br>Acercate al Administrador para regularizar tu situación.</font></h6>');
				$this->load->view('login',$mensaje);
			}
			
		}
		else
		{	
			$mensaje = array('mensaje' => '<h6><font color="red">Usuario o Contraseña Incorrecto</font></h6>');
			$this->load->view('login',$mensaje);
		}		

	}

	public function Logout()
	{
		$this->session->userdata = array();
		$this->session->sess_destroy();
		header('Location: '.site_url("Welcome"));
	}

	public function profile(){
		if (!$this->session->userdata('users_id')) {
			redirect('welcome');
		}
		else {
			$this->load->view('header');
			$this->load->view('aside');
			$roles = $this->modelo->getAllRoles();
			$people = $this->modelo->getAllPeople();
			$users_state = $this->modelo->getAllUsers_State();

			$data = array(
				'roles' => $roles,
				'people' => $people,
				'users_state' => $users_state
			);
			$this->load->view('users/profile', $data);
		}
	}

	public function changePassword(){
		$passv = trim($this->input->post('passv', TRUE));
		$passn = trim($this->input->post('passn', TRUE));
		if ($this->modelo->changePassword($this->session->userdata('users_id'), md5($passv), md5($passn))) {
			$this->session->sess_destroy();
			echo '1';
		}
		else {
			echo '0';
		}
	}
}
