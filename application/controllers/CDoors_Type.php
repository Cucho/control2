<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CDoors_Type extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('mDoors_Type', 'modelo');
	}

	public function index() {
		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('doors_type/index');
	}

	//Datatable
	public function datatable() {
		$start = $this->input->post('start');
		$length = $this->input->post('length');
		$search = $this->input->post('search')['value'];
		$by = $this->input->post('order')['0']['column'];
		$order = $this->input->post('order')['0']['dir'];

		$result = $this->modelo->getDoors_Type($start, $length, $search, $order, $by);

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
            "recordsTotal"    => intval($result['numDataTotal']),
            "recordsFiltered" => intval($result['numDataFilter']),
            "data"            => $result['data']
            );

        echo json_encode($json_data);
	}

	//Vistas
	public function add() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('doors_type/add');
	}

	public function edit() {
		if (!$this->session->userdata('edit')) {
			redirect('welcome');
		}
		$id = trim($this->input->get('id', TRUE));

		$door_type = $this->modelo->getDoor_type($id);

		$data = array('doors_type' => $door_type);

		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('doors_type/edit', $data);
	}

	public function view() {
		$id = trim($this->input->get('id', TRUE));

		$doors_type = $this->modelo->getDoor_type($id);

		$data = array('doors_type' => $doors_type);

		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('doors_type/view', $data);
	}

	//Crud
	public function addDoors_Type() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$type 	= trim($this->input->post('doors_type', TRUE));
		
		$date_time = date('Y-m-d H:i:s');

		$data = array(
			'type' => $type,
			'created' => $date_time,
			'modified' => $date_time
		);

		if($this->modelo->addDoors_Type($data)) {
			echo '1';
		}
		else {
			echo '0';
		}
	}

	public function editDoors_Type() {
		if (!$this->session->userdata('edit')) {
			redirect('welcome');
		}
		$id = trim($this->input->post('id', TRUE));

		$type 	= trim($this->input->post('doors_type', TRUE));
		
		$date_time = date('Y-m-d H:i:s');

		$data = array(
			'type' 	=> $type,
			'modified' 	=> $date_time
		);

		if($this->modelo->editDoors_Type($data, $id))
		{
			echo '1';
		}
		else
		{
			echo '0';
		}
	}

	public function deleteDoors_Type() {
		if (!$this->session->userdata('del')) {
			redirect('welcome');
		}
		$id = trim($this->input->post('id', TRUE)); 

		if($this->modelo->deleteDoors_Type($id)) {
			echo '1';
		}
		else {
			echo '0';
		}
	}
}