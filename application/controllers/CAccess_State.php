<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CAccess_State extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('mAccess_State', 'modelo');
	}

	public function index() {
		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('access_state/index');
	}

	//Datatable
	public function datatable() {
		$start = $this->input->post('start');
		$length = $this->input->post('length');
		$search = $this->input->post('search')['value'];
		$by = $this->input->post('order')['0']['column'];
		$order = $this->input->post('order')['0']['dir'];

		$result = $this->modelo->getAccess_State($start, $length, $search, $order, $by);

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
            "recordsTotal"    => intval($result['numDataTotal']),
            "recordsFiltered" => intval($result['numDataFilter']),
            "data"            => $result['data']
            );

        echo json_encode($json_data);
	}

	//Vistas
	public function add() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('access_state/add');
	}

	public function edit() {
		if (!$this->session->userdata('edit')) {
			redirect('welcome');
		}
		$id = trim($this->input->get('id', TRUE));

		$access_state = $this->modelo->getAccess($id);
		
		$data = array('access_state' => $access_state);

		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('access_state/edit', $data);
	}

	public function view() {
		$id = trim($this->input->get('id', TRUE));

		$access_state = $this->modelo->getAccess($id);

		$data = array('access_state' => $access_state);

		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('access_state/view', $data);
	}

	//Crud
	public function addAccess_State() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$state 		= 	trim($this->input->post('state', TRUE));
		
		$date_time = date('Y-m-d H:i:s');

		$data = array(
			'state' 	=> $state,
			'created' 		=> $date_time,
			'modified' 		=> $date_time
		);

		if($this->modelo->addAccess_State($data)) {
			echo '1';
		}
		else {
			echo '0';
		}
	}

	public function editAccess_State() {
		if (!$this->session->userdata('edit')) {
			redirect('welcome');
		}
		$id 			= trim($this->input->post('id', TRUE));
		$state 	= 	trim($this->input->post('state', TRUE));
		
		$date_time = date('Y-m-d H:i:s');

		$data = array(
			'state' 	=> $state,
			'modified' 	=> $date_time
		);

		if($this->modelo->editAccess_State($data, $id))
		{
			echo '1';
		}
		else
		{
			echo '0';
		}
	}

	public function deleteAccess_State() {
		if (!$this->session->userdata('del')) {
			redirect('welcome');
		}
		$id = trim($this->input->post('id', TRUE)); 

		if($this->modelo->deleteAccess_State($id)) {
			echo '1';
		}
		else {
			echo $this->db->error();
		}
	}
}