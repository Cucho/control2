<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CAreas extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('mAreas', 'modelo');
		$this->load->model('mZones', 'model');
		if (empty($this->session->userdata('options'))) {
			redirect('welcome');
		}
		else if (in_array('01020201', $this->session->userdata('options'))) {
			redirect('welcome');
		}
	}

	public function index() {
		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('areas/index');
	}

	//Datatable
	public function datatable() {
		$start = $this->input->post('start');
		$length = $this->input->post('length');
		$search = $this->input->post('search')['value'];
		$by = $this->input->post('order')['0']['column'];
		$order = $this->input->post('order')['0']['dir'];

		$result = $this->modelo->getAreas($start, $length, $search, $order, $by);

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
            "recordsTotal"    => intval($result['numDataTotal']),
            "recordsFiltered" => intval($result['numDataFilter']),
            "data"            => $result['data']
            );

        echo json_encode($json_data);
	}

	//Vistas
	public function add() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$this->load->view('header');
		$this->load->view('aside');
		$data = $this->model->getZones(null, null, null, null, null);
		$this->load->view('areas/add', $data);
	}

	public function edit() {
		if (!$this->session->userdata('edit')) {
			redirect('welcome');
		}
		$id = trim($this->input->get('id', TRUE));

		$area = $this->modelo->getArea($id);
		$zone = $this->model->getZones(null, null, null, null, null);

		$data = array('area' => $area, 'zones' => $zone);

		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('areas/edit', $data);
	}

	public function view() {
		$id = trim($this->input->get('id', TRUE));

		$area = $this->modelo->getArea($id);
		$zone = $this->model->getZones(null, null, null, null, null);

		$data = array('area' => $area, 'zones' => $zone);

		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('areas/view', $data);
	}

	//Crud
	public function addAreas() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$area 			= trim($this->input->post('area', TRUE));
		$zones_id		= trim($this->input->post('zones_id', TRUE));
		
		$date_time = date('Y-m-d H:i:s');

		$data = array(
			'area' => $area,
			'zones_id' => $zones_id,
			'created' => $date_time,
			'modified' => $date_time
		);

		if($this->modelo->addAreas($data)) {
			echo '1';
		}
		else {
			echo '0';
		}
	}

	public function editAreas() {
		if (!$this->session->userdata('edit')) {
			redirect('welcome');
		}
		$id = trim($this->input->post('id', TRUE));
		$area 			= trim($this->input->post('area', TRUE));
		$zones_id		= trim($this->input->post('zones_id', TRUE));

		$date_time = date('Y-m-d H:i:s');

		$data = array(
			'area' => $area,
			'zones_id' => $zones_id,
			'modified' => $date_time
		);

		if($this->modelo->editAreas($data, $id))
		{
			echo '1';
		}
		else
		{
			echo '0';
		}
	}

	public function deleteAreas() {
		if (!$this->session->userdata('del')) {
			redirect('welcome');
		}
		$id = trim($this->input->post('id', TRUE)); 

		if($this->modelo->deleteAreas($id)) {
			echo '1';
		}
		else {
			echo $this->db->error();
		}
	}
}