<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CSensors_Doors extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('mSensors_Doors', 'modelo');
		if (empty($this->session->userdata('options'))) {
			redirect('welcome');
		}
		else if (in_array('01080300', $this->session->userdata('options'))) {
			redirect('welcome');
		}
	}

	public function index() {
		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('sensors/list_sensors_doors');
	}

	//Datatable
	public function datatable() {
		$start = $this->input->post('start');
		$length = $this->input->post('length');
		$search = $this->input->post('search')['value'];
		$by = $this->input->post('order')['0']['column'];
		$order = $this->input->post('order')['0']['dir'];

		$result = $this->modelo->getSensors_Doors($start, $length, $search, $order, $by);

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
            "recordsTotal"    => intval($result['numDataTotal']),
            "recordsFiltered" => intval($result['numDataFilter']),
            "data"            => $result['data']
            );

        echo json_encode($json_data);
	}

	//Vistas
	public function add() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$sensors = $this->modelo->getAllSensors();
		$doors = $this->modelo->getAllDoors();

		$data = array(
			'sensors' => $sensors,
			'doors' => $doors
		);

		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('sensors/add_sensors_doors', $data);
	}

	//Crud
	public function addSensor_Door() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$sensor = trim($this->input->post('sensors', TRUE));
		$door 	= trim($this->input->post('doors', TRUE));
		
		$data = array(
			'sensors_id' 	=> $sensor,
			'doors_id' 		=> $door
		);

		if($this->modelo->addSensor_Door($data)) {
			echo '1';
		}
		else {
			echo '0';
		}
	}

	public function deleteSensor_Door() {
		if (!$this->session->userdata('del')) {
			redirect('welcome');
		}
		$sensors = trim($this->input->post('sensors', TRUE)); 
		$doors = trim($this->input->post('doors', TRUE)); 

		if($this->modelo->deleteSensor_Door($sensors, $doors)) {
			echo '1';
		}
		else {
			echo '0';
		}
	}

}