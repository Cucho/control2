<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CDoors extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('mDoors', 'modelo');
		$this->load->model('mDoors_Type', 'model');
		if (empty($this->session->userdata('options'))) {
			redirect('welcome');
		}
		else if (in_array('01070100', $this->session->userdata('options')) && in_array('01070200', $this->session->userdata('options'))) {
			redirect('welcome');
		}
	}

	public function index() {
		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('doors/index');
	}

	//Datatable
	public function datatable() {
		$start = $this->input->post('start');
		$length = $this->input->post('length');
		$search = $this->input->post('search')['value'];
		$by = $this->input->post('order')['0']['column'];
		$order = $this->input->post('order')['0']['dir'];

		$result = $this->modelo->getDoors($start, $length, $search, $order, $by);

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
            "recordsTotal"    => intval($result['numDataTotal']),
            "recordsFiltered" => intval($result['numDataFilter']),
            "data"            => $result['data']
            );

        echo json_encode($json_data);
	}

	//Vistas
	public function add() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$this->load->view('header');
		$this->load->view('aside');
		$data = $this->model->getDoors_Type(null, null, null, null, null);
		$this->load->view('doors/add', $data);
	}

	public function edit() {
		if (!$this->session->userdata('edit')) {
			redirect('welcome');
		}
		$id = trim($this->input->get('id', TRUE));

		$door = $this->modelo->getDoor($id);
		$type = $this->model->getDoors_Type(null, null, null, null, null);

		$data = array('doors' => $door, 'types' => $type);

		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('doors/edit', $data);
	}

	public function view() {
		$id = trim($this->input->get('id', TRUE));

		$door = $this->modelo->getDoor($id);
		$type = $this->model->getDoors_Type(null, null, null, null, null);

		$data = array('doors' => $door, 'types' => $type);

		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('doors/view', $data);
	}

	//Crud
	public function addDoors() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$doors 			= trim($this->input->post('doors', TRUE));
		$description 	= trim($this->input->post('description', TRUE));
		$level 			= trim($this->input->post('level', TRUE));
		$type 			= trim($this->input->post('doors_type', TRUE));
		
		$date_time = date('Y-m-d H:i:s');

		$data = array(
			'door' 			=> $doors,
			'description' 	=> $description,
			'level' 		=> $level,
			'doors_type_id' => $type,
			'created' 		=> $date_time,
			'modified' 		=> $date_time
		);

		if($this->modelo->addDoors($data)) {
			echo '1';
		}
		else {
			echo '0';
		}
	}

	public function editDoors() {
		if (!$this->session->userdata('edit')) {
			redirect('welcome');
		}
		$id = trim($this->input->post('id', TRUE));
		$doors 			= trim($this->input->post('doors', TRUE));
		$description 	= trim($this->input->post('description', TRUE));
		$level 			= trim($this->input->post('level', TRUE));
		$type 			= trim($this->input->post('doors_type', TRUE));

		$date_time = date('Y-m-d H:i:s');

		$data = array(
			'door' 			=> $doors,
			'description' 	=> $description,
			'level' 		=> $level,
			'doors_type_id' => $type,
			'modified' 		=> $date_time
		);

		if($this->modelo->editDoors($data, $id))
		{
			echo '1';
		}
		else
		{
			echo '0';
		}
	}

	public function deleteDoors() {
		if (!$this->session->userdata('del')) {
			redirect('welcome');
		}
		$id = trim($this->input->post('id', TRUE)); 

		if($this->modelo->deleteDoors($id)) {
			echo '1';
		}
		else {
			echo '0';
		}
	}

	//--------------- monitoring
	public function Monitoring() {
		
		$doors = $this->modelo->getDoorsMonitoring();
		$doors_sensors = array();

		foreach($doors as $d)
		{
			$sensors_info = $this->modelo->getSensorsInfo($d['id']);
			array_push($doors_sensors, array('id' => $d['id'], 'door' => $d['door'], 'description' => $d['description'], 'sensors_info' => $sensors_info));
		}

		$data = array(
			'doors' => $doors_sensors
		);
		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('doors/monitoring', $data);
	}

	public function getStateHost()
	{
		
		$ip = trim($this->input->post('ip', TRUE)); 
		$data = array();

		$fp = @fsockopen($ip, 80, $errno, $errstr, 1);
        if (!$fp){
        	$ip = str_replace(".", "", $ip);
        	$data = array('ip' => $ip, 'resp' => '<label class="label-danger">Desconectado</label>', 'attr' => 'disabled', 'state' => '1');
        }
        else {
           	$ip = str_replace(".", "", $ip);
        	$data = array('ip' => $ip, 'resp' => '<label class="label-success">Conectado</label>', 'attr' => 'disabled', 'state' => '0');
            fclose($fp);
        }

        echo json_encode($data);
	}
}