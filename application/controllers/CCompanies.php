<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CCompanies extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('mCompanies', 'modelo');
		if (empty($this->session->userdata('options'))) {
			redirect('welcome');
		}
		else if (in_array('07010000', $this->session->userdata('options'))) {
			redirect('welcome');
		}
	}

	public function index() {
		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('companies/index');
	}
	//Datatable
	public function datatable() {
		$start = $this->input->post('start');
		$length = $this->input->post('length');
		$search = $this->input->post('search')['value'];
		$by = $this->input->post('order')['0']['column'];
		$order = $this->input->post('order')['0']['dir'];

		$result = $this->modelo->getCompanies($start, $length, $search, $order, $by);

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
            "recordsTotal"    => intval($result['numDataTotal']),
            "recordsFiltered" => intval($result['numDataFilter']),
            "data"            => $result['data']
            );

        echo json_encode($json_data);
	}

	//Vistas
	public function add() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('companies/add');
	}

	public function edit() {
		if (!$this->session->userdata('edit')) {
			redirect('welcome');
		}
		$id = trim($this->input->get('id', TRUE));

		$company = $this->modelo->getCompany($id);

		$data = array('company' => $company);

		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('companies/edit', $data);
	}

	public function view() {
		$id = trim($this->input->get('id', TRUE));

		$company = $this->modelo->getCompany($id);

		$data = array('company' => $company);

		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('companies/view', $data);
	}

	//Crud
	public function addCompanies() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$company 	= trim($this->input->post('company', TRUE));
		$address 	= trim($this->input->post('address', TRUE));
		$phone 		= trim($this->input->post('phone', TRUE));
		$email 		= trim($this->input->post('email', TRUE));
		$contact 	= trim($this->input->post('contact', TRUE));
		$internal 	= trim($this->input->post('internal', TRUE));

		$date_time = date('Y-m-d H:i:s');

		$data = array(
			'company' => $company,
			'address' => $address,
			'phone' => $phone,
			'email' => $email,
			'contact' => $contact,
			'internal'	=> $internal,
			'created' => $date_time,
			'modified' => $date_time
		);

		if($this->modelo->addCompanies($data)) {
			echo '1';
		}
		else {
			echo '0';
		}
	}

	public function editCompanies() {
		if (!$this->session->userdata('edit')) {
			redirect('welcome');
		}
		$id = trim($this->input->post('id', TRUE));

		$company 	= trim($this->input->post('company', TRUE));
		$address 	= trim($this->input->post('address', TRUE));
		$phone 		= trim($this->input->post('phone', TRUE));
		$email 		= trim($this->input->post('email', TRUE));
		$contact 	= trim($this->input->post('contact', TRUE));
		$internal 	= trim($this->input->post('internal', TRUE));

		$date_time = date('Y-m-d H:i:s');

		$data = array(
			'company' 	=> $company,
			'address' 	=> $address,
			'phone' 	=> $phone,
			'email' 	=> $email,
			'contact' 	=> $contact,
			'internal'	=> $internal,
			'modified' 	=> $date_time
		);

		if($this->modelo->editCompanies($data, $id))
		{
			echo '1';
		}
		else
		{
			echo '0';
		}
	}

	public function deleteCompanies() {
		if (!$this->session->userdata('del')) {
			redirect('welcome');
		}
		$id = trim($this->input->post('id', TRUE)); 

		if($this->modelo->deleteCompanies($id)) {
			echo '1';
		}
		else {
			echo '0';
		}
	}
}