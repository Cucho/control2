<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CConfigurations extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('mConfigurations', 'modelo');
		if (empty($this->session->userdata('options'))) {
			redirect('welcome');
		}
		else if (in_array('01000000', $this->session->userdata('options'))) {
			redirect('welcome');
		}
	}

	// public function index() {
	// 	$this->load->view('header');
	// 	$this->load->view('aside');
	// 	$this->load->view('people/index');
	// }
	//Datatable
	// public function datatable() {
	// 	$start = $this->input->post('start');
	// 	$length = $this->input->post('length');
	// 	$search = $this->input->post('search')['value'];
	// 	$by = $this->input->post('order')['0']['column'];
	// 	$order = $this->input->post('order')['0']['dir'];

	// 	$result = $this->modelo->getPeoples($start, $length, $search, $order, $by);

	// 	$json_data = array(
	// 		"draw"            => intval($this->input->post('draw')),
 //            "recordsTotal"    => intval($result['numDataTotal']),
 //            "recordsFiltered" => intval($result['numDataFilter']),
 //            "data"            => $result['data']
 //            );

 //        echo json_encode($json_data);
	// }

	//Vistas
	public function add() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$people = $this->modelo->getAllPeople();

		$data = array(
			'people' => $people,
		);

		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('configurations/add', $data);
	}

	public function edit() {
		if (!$this->session->userdata('edit')) {
			redirect('welcome');
		}
		$id = trim($this->input->get('id', TRUE));

		$people = $this->modelo->getAllPeople();
		$configurations = $this->modelo->getconfiguration($id);

		$data = array(
			'people' => $people,
			'configurations' => $configurations
		);

		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('configurations/edit', $data);
	}

	// public function view() {
	// 	$id = trim($this->input->get('id', TRUE));

	// 	$people = $this->modelo->getPeople($id);
	// 	$companies = $this->modelo->getAllCompanies();
	// 	$people_profile = $this->modelo->getAllPeople_Profile();

	// 	$data = array(
	// 		'companies' => $companies,
	// 		'people' => $people,
	// 		'people_profile' => $people_profile
	// 	);

	// 	$this->load->view('header');
	// 	$this->load->view('aside');
	// 	$this->load->view('people/view', $data);
	// }

	//Crud
	public function addConfiguration() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$cod_company		= 	trim($this->input->post("cod_company", TRUE));
		$company 			= 	trim($this->input->post("company", TRUE));
		$cod_installation	= 	trim($this->input->post("cod_installation", TRUE));
		$installation 		= 	trim($this->input->post("installation", TRUE));
		$address 			= 	trim($this->input->post("address", TRUE));
		$email 				= 	trim($this->input->post("email", TRUE));
		$phone 				= 	trim($this->input->post("phone", TRUE));
		$people_id 			= 	trim($this->input->post("people_id", TRUE));

		$date_time = date('Y-m-d H:i:s');

		$data = array(
			'cod_company' 		=> $cod_company,
			'company' 			=> $company,
			'cod_installation' 	=> $cod_installation,
			'installation' 		=> $installation,
			'address' 			=> $address,
			'email' 			=> $email,
			'phone' 			=> $phone,
			'people_id' 		=> $people_id,
			'created'			=> $date_time,
			'modified'			=> $date_time
		);

		if($this->modelo->addConfiguration($data))
			echo '1';
		else
			echo '0';
	}

	public function editConfiguration() {
		if (!$this->session->userdata('edit')) {
			redirect('welcome');
		}
		$id 				= 	trim($this->input->post('id', TRUE));
		$cod_company		= 	trim($this->input->post("cod_company", TRUE));
		$company 			= 	trim($this->input->post("company", TRUE));
		$cod_installation	= 	trim($this->input->post("cod_installation", TRUE));
		$installation 		= 	trim($this->input->post("installation", TRUE));
		$address 			= 	trim($this->input->post("address", TRUE));
		$email 				= 	trim($this->input->post("email", TRUE));
		$phone 				= 	trim($this->input->post("phone", TRUE));
		$people_id 			= 	trim($this->input->post("people_id", TRUE));

		$date_time = date('Y-m-d H:i:s');

		$data = array(
			'cod_company' 		=> $cod_company,
			'company' 			=> $company,
			'cod_installation' 	=> $cod_installation,
			'installation' 		=> $installation,
			'address' 			=> $address,
			'email' 			=> $email,
			'phone' 			=> $phone,
			'people_id' 		=> $people_id,
			'modified'			=> $date_time
		);

		if($this->modelo->editConfiguration($data, $id)) {
			echo '1';
		}
		else {
			echo '0';
		}
	}

	// public function deletePeople() {
	// 	$id = trim($this->input->post('id', TRUE));

	// 	if($this->modelo->deletePeople($id))
	// 		echo '1';
	// 	else
	// 		echo '0';
	// }
}