<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CSeasons extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('mSeasons', 'modelo');
	}

	public function index() {
		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('seasons/index');
	}

	//Datatable
	public function datatable() {
		$start = $this->input->post('start');
		$length = $this->input->post('length');
		$search = $this->input->post('search')['value'];
		$by = $this->input->post('order')['0']['column'];
		$order = $this->input->post('order')['0']['dir'];

		$result = $this->modelo->getSeasons($start, $length, $search, $order, $by);

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
            "recordsTotal"    => intval($result['numDataTotal']),
            "recordsFiltered" => intval($result['numDataFilter']),
            "data"            => $result['data']
            );

        echo json_encode($json_data);
	}

	//Vistas
	public function add() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('seasons/add');
	}

	public function edit() {
		if (!$this->session->userdata('edit')) {
			redirect('welcome');
		}
		$id = trim($this->input->get('id', TRUE));

		$seasons = $this->modelo->getSeason($id);

		$data = array('seasons' => $seasons);

		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('seasons/edit', $data);
	}

	public function view() {
		$id = trim($this->input->get('id', TRUE));

		$seasons = $this->modelo->getSeason($id);

		$data = array('seasons' => $seasons);

		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('seasons/view', $data);
	}

	//Crud
	public function addSeason() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$season 	= trim($this->input->post('seasons', TRUE));
		
		$date_time = date('Y-m-d H:i:s');

		$data = array(
			'season' => $season,
			'created' => $date_time,
			'modified' => $date_time
		);

		if($this->modelo->addSeason($data)) {
			echo '1';
		}
		else {
			echo '0';
		}
	}

	public function editSeason() {
		if (!$this->session->userdata('edit')) {
			redirect('welcome');
		}
		$id = trim($this->input->post('id', TRUE));

		$season 	= trim($this->input->post('seasons', TRUE));
		
		$date_time = date('Y-m-d H:i:s');

		$data = array(
			'season' 	=> $season,
			'modified' 	=> $date_time
		);

		if($this->modelo->editSeason($data, $id))
		{
			echo '1';
		}
		else
		{
			echo '0';
		}
	}

	public function deleteSeason() {
		if (!$this->session->userdata('del')) {
			redirect('welcome');
		}
		$id = trim($this->input->post('id', TRUE)); 

		if($this->modelo->deleteSeason($id)) {
			echo '1';
		}
		else {
			echo '0';
		}
	}
}