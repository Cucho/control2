<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CProjects_Forms extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('MProjects_Forms', 'modelo');
		if (empty($this->session->userdata('options'))) {
			redirect('welcome');
		}
		else if (in_array('06030100', $this->session->userdata('options')) && in_array('06030200', $this->session->userdata('options')) && in_array('06030300', $this->session->userdata('options'))) {
			redirect('welcome');
		}
	}

	public function in(){
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('projects_forms/in');
	}

	public function out(){
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('projects_forms/out');
	}

	public function searchPeople(){
		$rut 	= trim($this->input->post('rut', TRUE));
		$rut 	= substr($rut, 0, (strlen($rut)-2));
		$date 	= date('Y-m-d');
		$peopleProjects = $this->modelo->searchPeopleProjects($rut, $date);

		$data = array(
			'mensaje' 			=> count($peopleProjects),
			'peopleProjects' 	=> $peopleProjects
		);
		echo json_encode($data);
	}

	public function searchPeople2(){
		$rut 	= trim($this->input->post('rut', TRUE));
		$rut 	= substr($rut, 0, (strlen($rut)-2));
		$date 	= date('Y-m-d');
		$peopleProjects = $this->modelo->searchPeopleProjects2($rut, $date);

		$data = array(
			'mensaje' 			=> count($peopleProjects),
			'peopleProjects' 	=> $peopleProjects
		);
		echo json_encode($data);
	}

	public function searchForm(){
		$forms 	= $this->modelo->searchForm();

		$data = array(
			'mensaje' 	=> count($forms),
			'forms' 	=> $forms
		);
		echo json_encode($data);
	}

	public function getForms(){
		$id = $this->input->post('form');
		$form = $this->modelo->getForms($id);

		$data = array(
			'mensaje' 	=> count($form),
			'forms' 	=> $form
		);

		echo json_encode($data);
	}

	public function searchProjectsVehicles(){
		$project 	= $this->input->post('project');
		
		$vehicles = $this->modelo->searchVehicle($project);

		$data = array(
			'mensaje' 	=> count($vehicles),
			'vehicles' 	=> $vehicles
		);

		echo json_encode($data);
	}

	public function addFormIn(){
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$title 			= trim($this->input->post('title', TRUE));
		$observation 	= trim($this->input->post('observation', TRUE));
		$answers 		= $this->input->post('answers', TRUE);
		$order 			= $this->input->post('order', TRUE);
		$projects_id 	= trim($this->input->post('projects_id', TRUE));
		$people_id 		= trim($this->input->post('people_id', TRUE));
		$forms_id		= trim($this->input->post('forms_id', TRUE));
		$vehicles_id 	= trim($this->input->post('vehicles_id', TRUE));
		$date_time = date('Y-m-d H:i:s');

		$mensaje = '';

		$data = array(
			'title' 		=> $title,
			'forms_id' 		=> $forms_id,
			'people_id' 	=> $people_id,
			'vehicle_id' 	=> $vehicles_id,
			'observation' 	=> $observation,
			'projects_id' 	=> $projects_id,
			'created' 		=> $date_time,
			'modified' 		=> $date_time
		);
		$control_id = $this->modelo->addProjectsForm($data);
		if (!empty($control_id)) {
			for ($i = 0; $i < count($order); $i++) { 
				$data = array(
					'control_id'	=> $control_id,
					'projects_id' 	=> $projects_id,
					'forms_id' 		=> $forms_id,
					'order' 		=> $order[$i],
					'answer' 		=> $answers[$i],
					'control' 		=> '0'
				);
				if ($this->modelo->addProjectsAnswers($data)) {
					$mensaje = '1';
				}
				else {
					$mensaje = '0';
				}
			}
		}
		echo $mensaje;
	}
	
	public function addFormOut(){
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$title 			= trim($this->input->post('title', TRUE));
		$observation 	= trim($this->input->post('observation', TRUE));
		$answers 		= $this->input->post('answers', TRUE);
		$order 			= $this->input->post('order', TRUE);
		$projects_id 	= trim($this->input->post('projects_id', TRUE));
		$people_id 		= trim($this->input->post('people_id', TRUE));
		$forms_id		= trim($this->input->post('forms_id', TRUE));
		$vehicles_id 	= trim($this->input->post('vehicles_id', TRUE));
		$date_time = date('Y-m-d H:i:s');

		$mensaje = '';

		$data = array(
			'title' 		=> $title,
			'forms_id' 		=> $forms_id,
			'people_id' 	=> $people_id,
			'vehicle_id' 	=> $vehicles_id,
			'observation' 	=> $observation,
			'projects_id' 	=> $projects_id,
			'created' 		=> $date_time,
			'modified' 		=> $date_time
		);
		$control_id = $this->modelo->addProjectsForm($data);
		if (!empty($control_id)) {
			for ($i = 0; $i < count($order); $i++) { 
				$data = array(
					'control_id'	=> $control_id,
					'projects_id' 	=> $projects_id,
					'forms_id' 		=> $forms_id,
					'order' 		=> $order[$i],
					'answer' 		=> $answers[$i],
					'control' 		=> '1'
				);
				if ($this->modelo->addProjectsAnswers($data)) {
					$mensaje = '1';
				}
				else {
					$mensaje = '0';
				}
			}
		}
		echo $mensaje;
	}

	public function list(){
		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('projects_forms/list');
	}

	public function datain(){
		$start = $this->input->post('start');
		$length = $this->input->post('length');
		$search = $this->input->post('search')['value'];
		$by = $this->input->post('order')['0']['column'];
		$order = $this->input->post('order')['0']['dir'];

		$projects_id = $this->input->post('projects_id');

		$result = $this->modelo->getControlIn($start, $length, $search, $order, $by, 0, $projects_id);

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
            "recordsTotal"    => intval($result['numDataTotal']),
            "recordsFiltered" => intval($result['numDataFilter']),
            "data"            => $result['data']
        	);

        echo json_encode($json_data);
	}

	public function dataout(){
		$start = $this->input->post('start');
		$length = $this->input->post('length');
		$search = $this->input->post('search')['value'];
		$by = $this->input->post('order')['0']['column'];
		$order = $this->input->post('order')['0']['dir'];

		$projects_id = $this->input->post('projects_id');

		$result = $this->modelo->getControlIn($start, $length, $search, $order, $by, 1, $projects_id);

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
            "recordsTotal"    => intval($result['numDataTotal']),
            "recordsFiltered" => intval($result['numDataFilter']),
            "data"            => $result['data']
            );

        echo json_encode($json_data);
	}

	public function details(){
		$projects_id = $this->input->post('projects_id');
		$control = $this->input->post('control');

		$details = $this->modelo->getDetailsProjectsForms($projects_id, $control);

		$data = array(
			'mensaje' 	=> count($details),
			'det' 		=> $details
		);

		echo json_encode($data);
	}

	public function detailsPDF(){
		$projects_id = $this->input->get('id');
		$control = $this->input->get('mov');

		$details = $this->modelo->getDetailsProjectsForms($projects_id, $control);

		$data = array(
			'details' 		=> $details
		);

		$mov = '';
		if ($control == 0) { $mov = 'Ingresos'; } else if($control == 1){ $mov = 'Salidas';}
		$filename = "Control interno de ".$mov.'.pdf';
		$html = $this->load->view('projects_forms/pdf', $data, true);
		$this->load->library('M_pdf');
		$this->m_pdf->pdf->WriteHTML($html);
		
		$this->m_pdf->pdf->Output($filaname, "I");//D
	}

	public function datainB(){
		$start = $this->input->post('start');
		$length = $this->input->post('length');
		$search = $this->input->post('search')['value'];
		$by = $this->input->post('order')['0']['column'];
		$order = $this->input->post('order')['0']['dir'];

		$result = $this->modelo->getControlInB($start, $length, $search, $order, $by, 0);

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
            "recordsTotal"    => intval($result['numDataTotal']),
            "recordsFiltered" => intval($result['numDataFilter']),
            "data"            => $result['data']
        	);

        echo json_encode($json_data);
	}

	public function dataoutB(){
		$start = $this->input->post('start');
		$length = $this->input->post('length');
		$search = $this->input->post('search')['value'];
		$by = $this->input->post('order')['0']['column'];
		$order = $this->input->post('order')['0']['dir'];

		$result = $this->modelo->getControlInB($start, $length, $search, $order, $by, 1);

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
            "recordsTotal"    => intval($result['numDataTotal']),
            "recordsFiltered" => intval($result['numDataFilter']),
            "data"            => $result['data']
            );

        echo json_encode($json_data);
	}
}