<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CType_Logbook extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('MType_Logbook', 'modelo');
	}

	public function index() {
		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('type_logbook/index');
	}

	//Datatable
	public function datatable() {
		$start = $this->input->post('start');
		$length = $this->input->post('length');
		$search = $this->input->post('search')['value'];
		$by = $this->input->post('order')['0']['column'];
		$order = $this->input->post('order')['0']['dir'];

		$result = $this->modelo->getType_Logbook($start, $length, $search, $order, $by);

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
            "recordsTotal"    => intval($result['numDataTotal']),
            "recordsFiltered" => intval($result['numDataFilter']),
            "data"            => $result['data']
            );

        echo json_encode($json_data);
	}

	//Vistas
	public function add() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$this->load->view('header');
		$this->load->view('aside');

		$this->load->view('type_logbook/add');
	}

	public function edit() {
		if (!$this->session->userdata('edit')) {
			redirect('welcome');
		}
		$id = trim($this->input->get('id', TRUE));

		$type_logbook = $this->modelo->getType_Logbook_($id);

		$data = array(
			'type_logbook' => $type_logbook
		);

		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('type_logbook/edit', $data);
	}

	public function view() {
		$id = trim($this->input->get('id', TRUE));

		$type_logbook = $this->modelo->getType_Logbook_($id);

		$data = array(
			'type_logbook' => $type_logbook
		);

		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('type_logbook/view', $data);
	}

	//Crud
	public function addType_Logbook() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$date_time = date('Y-m-d H:i:s');
		$type 			= trim($this->input->post('type', TRUE));

		$data = array(
			'type' => $type,
			'created' => $date_time,
			'modified' => $date_time
		);

		if($this->modelo->addType_Logbook($data)) 
			echo '1';
		else 
			echo '0';
	}

	public function editType_Logbook() {
		if (!$this->session->userdata('edit')) {
			redirect('welcome');
		}
		$id = trim($this->input->post('id', TRUE));
		$type 			= trim($this->input->post('type', TRUE));
		$date_time = date('Y-m-d H:i:s');

		$data = array(
			'type' => $type,
			'modified' => $date_time
		);

		if($this->modelo->editType_Logbook($data, $id)) 
			echo '1';
		else 
			echo '0';
		
	}

	public function deleteType_Logbook() {
		if (!$this->session->userdata('del')) {
			redirect('welcome');
		}
		$id = trim($this->input->post('id', TRUE));

		if($this->modelo->deleteType_Logbook($id)) {
			echo '1';
		}
		else {
			echo '0';
		}
	}
}
