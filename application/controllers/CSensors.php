<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CSensors extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('mSensors', 'modelo');
		if (empty($this->session->userdata('options'))) {
			redirect('welcome');
		}
		else if (in_array('01080100', $this->session->userdata('options'))) {
			redirect('welcome');
		}
	}

	public function index() {
		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('sensors/index');
	}
	//Datatable
	public function datatable() {
		$start = $this->input->post('start');
		$length = $this->input->post('length');
		$search = $this->input->post('search')['value'];
		$by = $this->input->post('order')['0']['column'];
		$order = $this->input->post('order')['0']['dir'];

		$result = $this->modelo->getSensors($start, $length, $search, $order, $by);

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
            "recordsTotal"    => intval($result['numDataTotal']),
            "recordsFiltered" => intval($result['numDataFilter']),
            "data"            => $result['data']
            );

        echo json_encode($json_data);
	}

	//Vistas
	public function add() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$type = $this->modelo->getAllSensors_Type();
		
		$data = array(
			'type' => $type
		);

		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('sensors/add', $data);
	}

	public function edit() {
		if (!$this->session->userdata('edit')) {
			redirect('welcome');
		}
		$id = trim($this->input->get('id', TRUE));

		$sensors = $this->modelo->getSensor($id);
		$type = $this->modelo->getAllSensors_Type();

		$data = array(
			'sensors' => $sensors,
			'type' => $type
		);

		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('sensors/edit', $data);
	}

	public function view() {
		$id = trim($this->input->get('id', TRUE));

		$sensors = $this->modelo->getSensor($id);
		$type = $this->modelo->getAllSensors_Type();

		$data = array(
			'sensors' => $sensors,
			'type' => $type
		);

		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('sensors/view', $data);
	}

	//Crud
	public function addSensor() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$code 			= 	trim($this->input->post('code', TRUE));
		$sensor 		= 	trim($this->input->post('sensor', TRUE));
		$description 	= 	trim($this->input->post('description', TRUE));
		$ip 			= 	trim($this->input->post('ip', TRUE));
		$sensors_type 	= 	trim($this->input->post('sensors_type', TRUE));
		$entry 			= 	trim($this->input->post('entry', TRUE));
		
		$date_time = date('Y-m-d H:i:s');

		$data = array(
			'code' 			=> $code,
			'sensor' 		=> $sensor,
			'description' 	=> $description,
			'ip' 			=> $ip,
			'sensors_type' 	=> $sensors_type,
			'entry' 		=> $entry,
			'created'		=> $date_time,
			'modified'		=> $date_time
		);

		if($this->modelo->addSensor($data))
			echo '1';
		else
			echo '0';
	}

	public function editSensor() {
		if (!$this->session->userdata('edit')) {
			redirect('welcome');
		}
		$id = trim($this->input->post('id', TRUE));
		$code 			= 	trim($this->input->post('code', TRUE));
		$sensor 		= 	trim($this->input->post('sensor', TRUE));
		$description 	= 	trim($this->input->post('description', TRUE));
		$ip 			= 	trim($this->input->post('ip', TRUE));
		$sensors_type 	= 	trim($this->input->post('sensors_type', TRUE));
		$entry 			= 	trim($this->input->post('entry', TRUE));

		$date_time = date('Y-m-d H:i:s');

		$data = array(
			'code' 			=> $code,
			'sensor' 		=> $sensor,
			'description' 	=> $description,
			'ip' 			=> $ip,
			'sensors_type' 	=> $sensors_type,
			'entry'			=> $entry,
			'modified'		=> $date_time
		);

		if($this->modelo->editSensor($data, $id)) {
			echo '1';
		}
		else {
			echo '0';
		}
	}

	public function deleteSensor() {
		if (!$this->session->userdata('del')) {
			redirect('welcome');
		}
		$id = trim($this->input->post('id', TRUE));

		if($this->modelo->deleteSensor($id))
			echo '1';
		else
			echo '0';
	}
}