<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class COptions extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('mOptions', 'modelo');
		if (empty($this->session->userdata('options'))) {
			redirect('welcome');
		}
		else if (in_array('01040000', $this->session->userdata('options'))) {
			redirect('welcome');
		}
	}

	public function index() {
		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('options/index');
	}

	//Datatable
	public function datatable() {
		$start = $this->input->post('start');
		$length = $this->input->post('length');
		$search = $this->input->post('search')['value'];
		$by = $this->input->post('order')['0']['column'];
		$order = $this->input->post('order')['0']['dir'];

		$result = $this->modelo->getOptions($start, $length, $search, $order, $by);

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
            "recordsTotal"    => intval($result['numDataTotal']),
            "recordsFiltered" => intval($result['numDataFilter']),
            "data"            => $result['data']
            );

        echo json_encode($json_data);
	}

	//Vistas
	public function add() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('options/add');
	}

	public function edit() {
		if (!$this->session->userdata('edit')) {
			redirect('welcome');
		}
		$id = trim($this->input->get('id', TRUE));

		$options = $this->modelo->getOption($id);

		$data = array('options' => $options);

		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('options/edit', $data);
	}

	public function view() {
		$id = trim($this->input->get('id', TRUE));

		$options = $this->modelo->getOption($id);

		$data = array('options' => $options);

		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('options/view', $data);
	}

	//Crud
	public function addOption() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$option = trim($this->input->post('option', TRUE));
		$code 	= trim($this->input->post('code', TRUE));
		
		$date_time = date('Y-m-d H:i:s');

		$data = array(
			'option' 	=> $option,
			'code' 		=> $code,
			'created' 	=> $date_time,
			'modified'	=> $date_time
		);

		if($this->modelo->addOption($data)) {
			echo '1';
		}
		else {
			echo '0';
		}
	}

	public function editOption() {
		if (!$this->session->userdata('edit')) {
			redirect('welcome');
		}
		$id = trim($this->input->post('id', TRUE));

		$option = trim($this->input->post('option', TRUE));
		$code 	= trim($this->input->post('code', TRUE));
		
		$date_time = date('Y-m-d H:i:s');

		$data = array(
			'option' 	=> $option,
			'code' 		=> $code,
			'modified' 	=> $date_time
		);

		if($this->modelo->editOption($data, $id))
		{
			echo '1';
		}
		else
		{
			echo '0';
		}
	}

	public function deleteOption() {
		if (!$this->session->userdata('del')) {
			redirect('welcome');
		}
		$id = trim($this->input->post('id', TRUE)); 

		if($this->modelo->deleteOption($id)) {
			echo '1';
		}
		else {
			echo '0';
		}
	}
}