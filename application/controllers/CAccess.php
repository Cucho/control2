<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CAccess extends CI_Controller {

	private $description;
	private $approved_by;  

	function __construct() {
		parent::__construct();
		$this->load->model('mAccess', 'modelo');
		if (empty($this->session->userdata('options'))) {
			redirect('welcome');
		}
		else if (in_array('05020200', $this->session->userdata('options'))) {
			redirect('welcome');
		}

		$this->description  = 'Permitido por: '.$this->session->userdata('name').' '.$this->session->userdata('last_name');
		$this->approved_by  = $this->session->userdata('people_id'); 
	}

	public function Pending()
	{
		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('access/pending');
	}

	public function State()
	{
		$notifications = $this->getLastNotifications(10);
		$data = array('notifications' => $notifications);

		$this->load->view('access/state', $data);
	}

	private function getLastNotifications($number)
	{
		$this->db->select('notifications.notification as notification, doors.door as door, notifications.entry as entry, DATE_FORMAT(notifications.created, "%d-%m-%Y %H:%i:%s") as created');
		$this->db->from('notifications');
		$this->db->join('doors', 'doors.id = notifications.doors_id');
		$this->db->order_by('notifications.created', 'desc');
		$this->db->limit($number);
		return $this->db->get()->result_array();

	}

	public function getPending()
	{
		// 1 -> Pendiente
		// 2 -> Rechazado
		// 3 -> Permitido

		$access_people = $this->modelo->getAccessPeopleByState(1);

		echo json_encode($access_people);
	}

	public function getAccessPeople()
	{
		$id = trim($this->input->post('access_people_id', TRUE));

		$access_people 					= 	$this->modelo->getAccessPeople($id);//rescatar form_id
		$access_people_form_in			= 	$this->modelo->getDetail_Access_People_Form_in($id);
		if (!empty($access_people_form_in)) {
			$access_people_form_inDetail = $this->modelo->SearchFormDetail($access_people_form_in[0]->forms_id);
			$getDetail_Access_People_Form_in_answers = $this->modelo->getDetail_Access_People_Form_answers($id, $access_people_form_in[0]->forms_id, 0);
		}
		$access_people_zones 			= 	$this->modelo->getAccessPeopleZones($id);
		$access_people_areas 			= 	$this->modelo->getAccessPeopleAreas($id);
		$access_people_department 		= 	$this->modelo->getAccessPeopleDepartment($id);
		$access_people_reasons 			= 	$this->modelo->getAccessPeopleReasons($id);
		$access_people_visit 			= 	$this->modelo->getAccessPeopleVisit($id);
		$access_people_route 			= 	$this->modelo->getAccessPeopleRoute($id);
		//$access_people_intents 			= 	$this->modelo->getAccessPeopleIntents($id);
		//$access_people_state_history 	= 	$this->modelo->getAccessPeopleStateHistory($id);

		$data = array(
			'access_people'					=>		$access_people,
			'access_people_form_in'			=>		$access_people_form_in,
			'access_people_form_inDetail' 	=> 		$access_people_form_inDetail,
			'getDetail_Access_People_Form_in_answers' => $getDetail_Access_People_Form_in_answers,
			'access_people_zones'			=>		$access_people_zones,
			'access_people_areas'			=>		$access_people_areas,
			'access_people_department'		=>		$access_people_department,
			'access_people_reasons'			=>		$access_people_reasons,
			'access_people_visit'			=>		$access_people_visit,
			'access_people_route'			=>		$access_people_route
			//'access_people_intents'			=>		$access_people_intents,
			//'access_people_state_history'	=>		$access_people_state_history
		);

		echo json_encode($data);
	}

	public function changeStateAccessPeople()
	{
		$id = trim($this->input->post('access_people_id', TRUE));
		$access_state_id = trim($this->input->post('access_state_id', TRUE));
		$observation = $this->input->post('observation', TRUE);

		$date_time = date('Y-m-d H:i:s');
		$end_time = '0000-00-00 00:00:00';

		if($access_state_id == 2)
		{
			$hours = 0;

			$this->db->select('hours');
			$this->db->from('access_people');
			$this->db->where('id', $id);
			$res = $this->db->get()->result_array();
			if(!empty($res[0]['hours']))
				$hours = $res[0]['hours'];

			$end_time = strtotime('+'.$hours.' hour', strtotime($date_time));
			$end_time = date("Y-m-d H:i:s",$end_time);
		}
		else
		{
			//obtengo end_time
			$this->db->select('end_time');
			$this->db->from('access_people');
			$this->db->where('id', $id);
			$res = $this->db->get()->result_array();
			if(!empty($res[0]['end_time']))
				$end_time = $res[0]['end_time'];
			//------------------------------------------

			$data = array(
				'exit_time'	=> $date_time,
				'entry' => 1
			);

			$this->db->where('id', $id);
			$this->db->update('access_people', $data);
		}


		

		if(empty($observation))
			$observation = '';

		$data = array(
				'access_people_id'		=>		$id,
				'access_state_id'		=>		$access_state_id,
				'description'			=>		$this->description,
				'created'				=>		$date_time
		);

		if($this->modelo->changeStateAccessPeople($id, $access_state_id, $observation, $date_time, $end_time, $this->approved_by))
		{
			if($this->modelo->addStateHistory($data))
			{
				$people = $this->modelo->getPeopleByAccessPeopleID($id);
				$data = array(
					'response' => 1,
					'people' => $people,
					'time_entrance' => $date_time,
					'end_time' => $end_time
				);

				echo json_encode($data);
			}
			else
			{
				$data = array(
					'response' => 2
				);

				echo json_encode($data);
			}
		}
		else
		{
			$data = array(
					'response' => 0
				);

			echo json_encode($data);
		}
	}

	//--------- function state actual -------------------------------------

	public function getAllCounts()
	{
		$visit_people_normal = intval($this->modelo->getCountVisitPeopleNormal());
		$visit_people_exc    = intval($this->modelo->getCountVisitPeopleExc());
		$visit_vehicles_normal = intval($this->modelo->getCountVisitVehiclesNormal());
		$visit_vehicles_exc    = intval($this->modelo->getCountVisitVehiclesExc());

		$contractor_people_normal = intval($this->modelo->getCountContractorPeopleNormal());
		$contractor_people_exc    = intval($this->modelo->getCountContractorPeopleExc());
		$contractor_vehicles_normal = intval($this->modelo->getCountContractorVehiclesNormal());
		$contractor_vehicles_exc    = intval($this->modelo->getCountContractorVehiclesExc());

		$internal_people_normal = intval($this->modelo->getCountInternalPeopleNormal());
		$internal_people_exc    = intval($this->modelo->getCountInternalPeopleExc());
		$internal_vehicles_normal = intval($this->modelo->getCountInternalVehiclesNormal());
		$internal_vehicles_exc    = intval($this->modelo->getCountInternalVehiclesExc());

		$cont_external_people = $visit_people_normal + $contractor_people_normal;
        $cont_external_vehicles = $visit_vehicles_normal + $contractor_vehicles_normal;
        $cont_people_exc = $visit_people_exc + $contractor_people_exc + $internal_people_exc;
        $cont_vehicles_exc = $visit_vehicles_exc + $contractor_vehicles_exc + $internal_vehicles_exc;
        $cont_internal_people = $internal_people_normal;
        $cont_internal_vehicles = $internal_vehicles_normal;

        $sum_people = $cont_external_people + $cont_people_exc + $cont_internal_people;
        $sum_vehicles = $cont_external_vehicles + $cont_vehicles_exc + $cont_internal_vehicles;

		$data = array(
			'cont_external_people' 			=> $cont_external_people,
			'cont_external_vehicles'	  	=> $cont_external_vehicles,
			'cont_people_exc'				=> $cont_people_exc,
			'cont_vehicles_exc'				=> $cont_vehicles_exc,
			'cont_internal_people'			=> $cont_internal_people,
			'cont_internal_vehicles'		=> $cont_internal_vehicles,
			'sum_people'					=> $sum_people,
			'sum_vehicles'					=> $sum_vehicles
		);

		echo json_encode($data);
	}

	//----- end function state actual -------------------------------------

	public function Access_In()
	{
		$list_access_in = $this->modelo->getListAccessIn();
		$data = array(
			'list_access_in' => $list_access_in
		);
		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('access/list_access_in', $data);
	}

	public function PDF_Access_In(){
		$list_access_in = $this->modelo->getListAccessIn();
		
		$this->load->library('excel');
		$this->excel->setActiveSheetIndex(0);
		$this->excel->getActiveSheet()->setTitle('Listado de acceso al interior');

		$this->excel->getActiveSheet()->SetCellValue('A1', 'ID');
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setSize('11');
		$this->excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
		$this->excel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$this->excel->getActiveSheet()->SetCellValue('B1', 'Rut');
		$this->excel->getActiveSheet()->getStyle('B1')->getFont()->setSize('11');
		$this->excel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
		$this->excel->getActiveSheet()->getStyle('B1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$this->excel->getActiveSheet()->SetCellValue('C1', 'Nombre');
		$this->excel->getActiveSheet()->getStyle('C1')->getFont()->setSize('11');
		$this->excel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
		$this->excel->getActiveSheet()->getStyle('C1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$this->excel->getActiveSheet()->SetCellValue('D1', 'Perfil');
		$this->excel->getActiveSheet()->getStyle('D1')->getFont()->setSize('11');
		$this->excel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);
		$this->excel->getActiveSheet()->getStyle('D1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$this->excel->getActiveSheet()->SetCellValue('E1', 'Empresa');
		$this->excel->getActiveSheet()->getStyle('E1')->getFont()->setSize('11');
		$this->excel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true);
		$this->excel->getActiveSheet()->getStyle('E1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$this->excel->getActiveSheet()->SetCellValue('F1', 'Tipo');
		$this->excel->getActiveSheet()->getStyle('F1')->getFont()->setSize('11');
		$this->excel->getActiveSheet()->getStyle('F1')->getFont()->setBold(true);
		$this->excel->getActiveSheet()->getStyle('F1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$this->excel->getActiveSheet()->SetCellValue('G1', 'Puerta');
		$this->excel->getActiveSheet()->getStyle('G1')->getFont()->setSize('11');
		$this->excel->getActiveSheet()->getStyle('G1')->getFont()->setBold(true);
		$this->excel->getActiveSheet()->getStyle('G1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$this->excel->getActiveSheet()->SetCellValue('H1', 'Movimiento');
		$this->excel->getActiveSheet()->getStyle('H1')->getFont()->setSize('11');
		$this->excel->getActiveSheet()->getStyle('H1')->getFont()->setBold(true);
		$this->excel->getActiveSheet()->getStyle('H1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$this->excel->getActiveSheet()->SetCellValue('I1', 'Salida');
		$this->excel->getActiveSheet()->getStyle('I1')->getFont()->setSize('11');
		$this->excel->getActiveSheet()->getStyle('I1')->getFont()->setBold(true);
		$this->excel->getActiveSheet()->getStyle('I1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$this->excel->getActiveSheet()->SetCellValue('J1', 'Creado');
		$this->excel->getActiveSheet()->getStyle('J1')->getFont()->setSize('11');
		$this->excel->getActiveSheet()->getStyle('J1')->getFont()->setBold(true);
		$this->excel->getActiveSheet()->getStyle('J1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

		$fila = 2;
		foreach ($list_access_in as $k) {
			$this->excel->getActiveSheet()->setCellValue('A'.$fila, trim($k['id']));
			$this->excel->getActiveSheet()->setCellValue('B'.$fila, trim($k['rut'].'-'.$k['digit']));
			$this->excel->getActiveSheet()->setCellValue('C'.$fila, trim($k['name'].' '.$k['last_name']));
			$internal = $k['internal'];
            if($internal == 0){$internal = 'Visita';} 
            else if($internal == 1){$internal = 'Interno';}
            else {$internal = 'Contratista';}
			$this->excel->getActiveSheet()->setCellValue('D'.$fila, trim($internal.' | '.$k['profile']));
			$this->excel->getActiveSheet()->setCellValue('E'.$fila, trim($k['company']));
			$flow = $k['flow'];
            if($flow == 0){$flow = 'Peatonal';}
            else{$flow = 'Vehicular '.$k['patent'];}
			$this->excel->getActiveSheet()->setCellValue('F'.$fila, $flow);
			$this->excel->getActiveSheet()->setCellValue('G'.$fila, trim($k['door']));
			$action = $k['action'];
            if($action == 0){$action = 'Ingreso';}
            else{$action = 'Salida';}
			$this->excel->getActiveSheet()->setCellValue('H'.$fila, $action);
			$this->excel->getActiveSheet()->setCellValue('I'.$fila, trim($k['end_time']));
			$this->excel->getActiveSheet()->setCellValue('J'.$fila, trim($k['created']));
			$fila++;
		}
		for ($col='A'; $col !== 'K' ; $col++) { 
			$this->excel->getActiveSheet()->getColumnDimension($col)->setAutoSize(true);
		}
		$filename = 'Registro de acceso al interior '.date('Y-m-d').'.xls';
		header('Content-Type: application/vnd.ms-excel;charset=utf-8');
		header('Content-Disposition: attachment;filename="'.$filename.'"');
		header('Cache-Control: max-age=0');
		$objWriter = PHPExcel_IOFactory::createWriter($this->excel, 'Excel5');
		$objWriter->save('php://output');

	}

	//30-10-2018
	public function generateExit()
	{
		$rut = trim($this->input->post('rut', TRUE));
		$internal = trim($this->input->post('internal', TRUE));
		$motivo = trim($this->input->post('motivo', TRUE));
		$ip = $this->session->userdata('ip');

		$date_time = date('Y-m-d H:i:s');
		$people_id = $this->modelo->getPeopleID_By_Rut($rut);
		$doors_id = $this->modelo->getDoors_ID_By_Ip($ip);

		

		if(intval($doors_id) > 0)
		{

			$data_people = $this->modelo->getInfoPeople($people_id);
			if($data_people[0]['internal'] == 0)
				$data_people[0]['internal'] = 'Visita';
			else if($data_people[0]['internal'] == 1)
				$data_people[0]['internal'] = 'Interno';
			else
				$data_people[0]['internal'] = 'Contratista';

			$body = '<h4>SALIDA MANUAL GENERADA ('.$data_people[0]['rut'].'-'.$data_people[0]['digit'].')</h4>';
			$body .= '<hr>';
			$body .= '<p>'.$data_people[0]['name'].' '.$data_people[0]['last_name'].'</p>';
			$body .= '<p>'.$data_people[0]['internal'].'</p>';
			$body .= '<p>'.$data_people[0]['profile'].'</p>';
			$body .= '<p>'.$data_people[0]['company'].'</p>';
			$body .= '<hr>';
			$body .= '<p>Motivo: '.$motivo.'</p>';

			$data_logbook = array(
				'title' => 'SALIDA MANUAL PERSONA '.$data_people[0]['rut'].'-'.$data_people[0]['digit'],
				'body' => $body,
				'people_id' => $this->session->userdata('people_id'),
				'type_logbook_id' => 3,
				'edited' => 0,
				'created' => $date_time
			);


			if($internal == 0)
			{
				//visita
				$access_people_id = $this->modelo->getLastAccessPeople($people_id);
				
				$access_people = array(
					'entry' => 1,
					'exit_time' => $date_time
				);

				$access_people_intents = array(
					'access_people_id' => $access_people_id,
					'doors_id' => $doors_id,
					'entry' => 1,
					'success' => 1,
					'flow' => 0,
					'created' => $date_time
				);

				$this->db->where('id', $access_people_id);
				if($this->db->update('access_people', $access_people))
				{
					if($this->db->insert('access_people_intents', $access_people_intents))
					{
						$this->db->where('people_id', $people_id);
						if($this->db->delete('current_internal_state'))
						{
							echo 1;
						}
						else
							echo 0;
					}
					else
						echo 0;
				}
				else
					echo 0;


			}
			else if($internal == 1)
			{
				//interno
				$internal_people = array(
					'people_id' => $people_id,
					'doors_id' => $doors_id,
					'entry' => 1,
					'success' => 1,
					'reasons_error_id' => 0,
					'created' => $date_time 
				);

				if($this->db->insert('internal_people', $internal_people))
				{
					$this->db->where('people_id', $people_id);
					if($this->db->delete('current_internal_state'))
					{
						echo 1;
					}
					else
						echo 0;
				}
				else
					echo 0;
			}
			else
			{
				//contratista
				$projects_id = $this->modelo->getLastProjectID_By_People($people_id, $date_time);

				$projects_intents = array(
					'projects_id' => $projects_id,
					'people_id' => $people_id,
					'doors_id' => $doors_id,
					'entry' => 1,
					'success' => 1,
					'vehicles_id' => 0,
					'flow' => 0,
					'created' => $date_time
				);

				if($this->db->insert('projects_intents', $projects_intents))
				{
					$this->db->where('people_id', $people_id);
					if($this->db->delete('current_internal_state'))
					{
						echo 1;
					}
					else
						echo 0;
				}
				else
					echo 0;
			}

			$this->modelo->save_logbook($data_logbook);
		}
		else
		{
			//no se puede porque no es porteria
			echo 2;
		}
		
	}

	//26-11-2018
	public function GenerateQRCode()
	{
		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('generate_qrcode');
	}

	public function emitQRCode()
	{
		$rut = trim($this->input->post('rut', TRUE));
		$dv = trim($this->input->post('dv', TRUE));

		$this->load->library('ciqrcode');

		 //hacemos configuraciones
        $params['data'] = $rut.$dv;
        $params['level'] = 'H';
        $params['size'] = 10;
        //$params['framSize'] = 3; //tamaño en blanco

        //decimos el directorio a guardar el codigo qr, en este 
        //caso una carpeta en la raíz llamada qr_code
        $params['savename'] = FCPATH . "assets/qr_codes/qr_".$rut."_".$dv.".png";
        //generamos el código qr
        $this->ciqrcode->generate($params);

        echo "assets/qr_codes/qr_".$rut."_".$dv.".png";
	}


}