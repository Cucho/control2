<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CMain_Access extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('mMain_Access', 'modelo');
		if (empty($this->session->userdata('options'))) {
			redirect('welcome');
		}
		else if (in_array('01030000', $this->session->userdata('options'))) {
			redirect('welcome');
		}
	}

	public function index() {
		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('main_access/index');
	}

	//Datatable
	public function datatable() {
		$start = $this->input->post('start');
		$length = $this->input->post('length');
		$search = $this->input->post('search')['value'];
		$by = $this->input->post('order')['0']['column'];
		$order = $this->input->post('order')['0']['dir'];

		$result = $this->modelo->getMain_Access($start, $length, $search, $order, $by);

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
            "recordsTotal"    => intval($result['numDataTotal']),
            "recordsFiltered" => intval($result['numDataFilter']),
            "data"            => $result['data']
            );

        echo json_encode($json_data);
	}

	//Vistas
	public function add() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$this->load->view('header');
		$this->load->view('aside');
		$data = array('doors' => $this->modelo->getAllDoors());
		$this->load->view('main_access/add', $data);
	}

	public function edit() {
		if (!$this->session->userdata('edit')) {
			redirect('welcome');
		}
		$id = trim($this->input->get('id', TRUE));

		$main_access = $this->modelo->getMain_AccessId($id);

		$data = array(
			'main_access' => $main_access,
			'doors' => $this->modelo->getAllDoors()
		);

		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('main_access/edit', $data);
	}

	public function view() {
		$id = trim($this->input->get('id', TRUE));

		$main_access = $this->modelo->getMain_AccessId($id);

		$data = array(
			'main_access' => $main_access,
			'doors' => $this->modelo->getAllDoors()
		);

		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('main_access/view', $data);
	}

	//Crud
	public function addMain_Access() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$name 		= 	trim($this->input->post('name', TRUE));
		$ubication 	= 	trim($this->input->post('ubication', TRUE));
		$ip_host 	= 	trim($this->input->post('ip_host', TRUE));
		$name_host 	= 	trim($this->input->post('name_host', TRUE));
		$entry 		= 	trim($this->input->post('entry', TRUE));
		$flow 		= 	trim($this->input->post('flow', TRUE));
		$internal 	= 	trim($this->input->post('internal', TRUE));
		$state 		= 	trim($this->input->post('state', TRUE));
		$pop_up 	= 	trim($this->input->post('pop_up', TRUE));
		$doors_id 	= 	trim($this->input->post('doors', TRUE));
		$main 		= 	trim($this->input->post('main', TRUE));

		$date_time = date('Y-m-d H:i:s');

		$data = array(
			'name' 		=> $name,
			'ubication' => $ubication,
			'ip_host' 	=> $ip_host,
			'name_host' => $name_host,
			'entry' 	=> $entry,
			'flow' 		=> $flow,
			'internal' 	=> $internal,
			'state' 	=> $state,
			'pop_up' 	=> $pop_up,
			'doors_id' 	=> $doors_id,
			'main' 		=> $main,
			'created' 	=> $date_time,
			'modified' 	=> $date_time
		);

		if($this->modelo->addMain_Access($data)) {
			echo '1';
		}
		else {
			echo '0';
		}
	}

	public function editMain_Access() {
		if (!$this->session->userdata('edit')) {
			redirect('welcome');
		}
		$id = trim($this->input->post('id', TRUE));
		$name 		= 	trim($this->input->post('name', TRUE));
		$ubication 	= 	trim($this->input->post('ubication', TRUE));
		$ip_host 	= 	trim($this->input->post('ip_host', TRUE));
		$name_host 	= 	trim($this->input->post('name_host', TRUE));
		$entry 		= 	trim($this->input->post('entry', TRUE));
		$flow 		= 	trim($this->input->post('flow', TRUE));
		$internal 	= 	trim($this->input->post('internal', TRUE));
		$state 		= 	trim($this->input->post('state', TRUE));
		$pop_up 	= 	trim($this->input->post('pop_up', TRUE));
		$doors_id 	= 	trim($this->input->post('doors', TRUE));
		$main 		= 	trim($this->input->post('main', TRUE));

		$date_time = date('Y-m-d H:i:s');

		$data = array(
			'name' 		=> $name,
			'ubication' => $ubication,
			'ip_host' 	=> $ip_host,
			'name_host' => $name_host,
			'entry' 	=> $entry,
			'flow' 		=> $flow,
			'internal' 	=> $internal,
			'state' 	=> $state,
			'pop_up'	=> $pop_up,
			'doors_id' 	=> $doors_id,
			'main' 		=> $main,
			'modified' 	=> $date_time
		);

		if($this->modelo->editMain_Access($data, $id))
		{
			echo '1';
		}
		else
		{
			echo '0';
		}
	}

	public function deleteMain_Access() {
		if (!$this->session->userdata('del')) {
			redirect('welcome');
		}
		$id = trim($this->input->post('id', TRUE));

		if($this->modelo->deleteMain_Access($id)) {
			echo '1';
		}
		else {
			echo $this->db->error();
		}
	}
}
