<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CWSAccess_Control extends CI_Controller {

	private $node_url = 'http://'.NODE_HOST.':'.NODE_PORT.'/wrong_event';
	private $node_visit = 'http://'.NODE_HOST.':'.NODE_PORT.'/visit';

	function __construct() 
	{
		parent::__construct();
		$this->load->model('MWSAccess_Control', 'modelo');
	}


	public function getAccessPeopleReader()
	{
		//-------------------------------------------------------
		$date_time_now = date('Y-m-d H:i:s');
		$dt = new DateTime($date_time_now);
		$date_now = $dt->format('Y-m-d');
		$time_now = $dt->format('H:i:s');
		//Día de la semana [L-M-Mi-J-V-S-D]
		$dow = $this->getDayOfWeek();
		//-------------------------------------------------------
		$internal;
		$people_profiles_id;
		$companies_id;
		$people_id;
		//VARIABLES ENTRADA DESDE EL SENSOR RASPBERRY
		//------------------------------------------------------
		$rut 	=  trim($this->input->post('rut', TRUE));
		$rut_completo = $rut;
		$sensors_id = trim($this->input->post('sensors_id', TRUE));
		$sensors_name = $this->getSensorbyId($sensors_id);
		$doors_id = trim($this->input->post('doors_id', TRUE));
		$doors_name = $this->getDoorbyId($doors_id);
		$ubication = trim($this->input->post('ubication', TRUE));
		//ACTION [0 = INGRESO, 1 = SALIDA]
		$action = trim($this->input->post('action', TRUE));
		$level = trim($this->input->post('level', TRUE));
		//------------------------------------------------------
		$rut = substr($rut, 0, (strlen($rut)-1));
		$people = $this->modelo->getPeople_Rut($rut);
		//-------------------------------------------------------

		//si existe la persona
		if(!empty($people))
		{
			$state = $people[0]['states_id'];

			if($state == 1)
			{
				//activo
				$people_id = $people[0]['id'];
				$rut_completo 	=  $people[0]['rut'].'-'.$people[0]['digit'];
				$people_name = $people[0]['name'].' '.$people[0]['last_name'];
				$company = $people[0]['company'];
				$internal = $people[0]['internal'];
				$people_profiles_id = $people[0]['people_profiles_id'];
				$companies_id = $people[0]['companies_id'];
				$allow_all = $people[0]['allow_all'];

				if($internal == 0)
				{
					//VISITA
					//ULTIMA AUTORIZACION
					$last_authorization = $this->modelo->getLast_Authorization_Access_People($people_id);
					if(!empty($last_authorization))
					{
						$access_people_id = $last_authorization[0]['id'];
						$seconds = $last_authorization[0]['seconds'];
						$vehicles_id = $last_authorization[0]['vehicles_id'];

						if($seconds >= 0)
						{
							//ESTA DENTRO DEL TIEMPO PERMITIDO DE LA AUTORIZACION
							//VALIDAR PUERTA
							$isValidDoor = $this->modelo->getAuthorization_Door_Access_People($access_people_id, $doors_id);

							if($isValidDoor == 1)
							{
								//TIENE PERMISO POR ESTA PUERTA
								//datos intento
								
								//0 ERROR | 1 OK | 2 CONTROLAR AL SALIR VEHICULO | 3 SALIR SIN HABER INGRESADO| 4 intento ingreso inter sin pasar por externa
								$res = $this->add_current_internal_state($people_id, 0, $people_id, $vehicles_id, $doors_id, $level,$action, $date_time_now, $rut_completo, $people_name, $company, 0, $last_authorization[0]['end_time']);

								if($res == 1)
								{
									//ok
									$data = array(
										'access_people_id' => $access_people_id,
										'doors_id' => $doors_id,
										'entry' => $action,
										'success' => 1,
										'flow' => 0,
										'created' => $date_time_now
									);
									//registrar intento
									if($this->modelo->add_access_people_intents($data))
										echo 1;
									else
										echo 0;
								}
								else if($res == 0)
								{
									//error
									$title = 'ERROR INTERNO, INTENTO APERTURA: ';
										$people_ = $rut_completo.' | '.$people[0]['name'].' '.$people[0]['last_name'].' | '.$people[0]['company'];
									if($this->add_notification($title.$people_, $doors_id, $sensors_id, $action, $date_time_now))
									{
										$this->sendPop_up($title, $people_, $doors_name, $sensors_name, $action, $ubication, $date_time_now, 1);

										//datos intento
										$data = array(
											'access_people_id' => $access_people_id,
											'doors_id' => $doors_id,
											'entry' => $action,
											'success' => 0,
											'flow' => 0,
											'created' => $date_time_now
										);
										//registrar intento
										if($this->modelo->add_access_people_intents($data))
											echo 0;
										else
											echo 0;
									}

								}
								else if($res == 2)
								{
									//salida sin controlar vehiculo
									$title = 'SALIDA VISITA SIN CONTROLAR VEHICULO: ';
										$people_ = $rut_completo.' | '.$people[0]['name'].' '.$people[0]['last_name'].' | '.$people[0]['company'];
									if($this->add_notification($title.$people_, $doors_id, $sensors_id, $action, $date_time_now))
									{
										$this->sendPop_up($title, $people_, $doors_name, $sensors_name, $action, $ubication, $date_time_now, 1);

										//datos intento
										$data = array(
											'access_people_id' => $access_people_id,
											'doors_id' => $doors_id,
											'entry' => $action,
											'success' => 0,
											'flow' => 0,
											'created' => $date_time_now
										);
										//registrar intento
										if($this->modelo->add_access_people_intents($data))
											echo 0;
										else
											echo 0;
									}
								}
								else if($res == 3)
								{
									//salir sin haber entrado
									$title = 'SALIDA VISITA SIN PREVIA ENTRADA: ';
										$people_ = $rut_completo.' | '.$people[0]['name'].' '.$people[0]['last_name'].' | '.$people[0]['company'];
									if($this->add_notification($title.$people_, $doors_id, $sensors_id, $action, $date_time_now))
									{
										$this->sendPop_up($title, $people_, $doors_name, $sensors_name, $action, $ubication, $date_time_now, 1);

										//datos intento
										$data = array(
											'access_people_id' => $access_people_id,
											'doors_id' => $doors_id,
											'entry' => $action,
											'success' => 0,
											'flow' => 0,
											'created' => $date_time_now
										);
										//registrar intento
										if($this->modelo->add_access_people_intents($data))
											echo 0;
										else
											echo 0;
									}
								}
								else if($res == 4)
								{
									//ingreso p interna sin haber pasado por externa
									$title = 'INGRESO VISITA PUERTA INTERNA SIN PREVIAMENTE PASAR POR EXTERNA: ';
										$people_ = $rut_completo.' | '.$people[0]['name'].' '.$people[0]['last_name'].' | '.$people[0]['company'];
									if($this->add_notification($title.$people_, $doors_id, $sensors_id, $action, $date_time_now))
									{
										$this->sendPop_up($title, $people_, $doors_name, $sensors_name, $action, $ubication, $date_time_now, 1);

										//datos intento
										$data = array(
											'access_people_id' => $access_people_id,
											'doors_id' => $doors_id,
											'entry' => $action,
											'success' => 0,
											'flow' => 0,
											'created' => $date_time_now
										);
										//registrar intento
										if($this->modelo->add_access_people_intents($data))
											echo 0;
										else
											echo 0;
									}
								}
								
							}
							else
							{
								//NO TIENE PERMISO POR ESTA PUERTA
								//VALIDAR SI ES SALIDA
								if($action == 1)
								{
									//SALIDA
									if($level == 0)
									{
										//si es salida perimetral no puede
										//obligandolo a salir por porteria
										$title = 'SALIDA VISITA NO PERMITIDO POR PUERTA EQUIVOCADA: ';
										$people_ = $rut_completo.' | '.$people[0]['name'].' '.$people[0]['last_name'].' | '.$people[0]['company'];
										if($this->add_notification($title.$people_, $doors_id, $sensors_id, $action, $date_time_now))
										{
											$this->sendPop_up($title, $people_, $doors_name, $sensors_name, $action, $ubication, $date_time_now, 1);
											//datos intento
											$data = array(
												'access_people_id' => $access_people_id,
												'doors_id' => $doors_id,
												'entry' => $action,
												'success' => 0,
												'flow' => 0,
												'created' => $date_time_now
											);
											//registrar intento
											if($this->modelo->add_access_people_intents($data))
												echo 0;
											else
												echo 0;
										}
									}
									else
									{
										$res = $this->add_current_internal_state($people_id, 1, $people_id, 0, $doors_id, $level,$action, $date_time_now, $rut_completo, $people_name, $company, 0, '');

										//si es salida de una puerta interna si puede
										$title = 'APERTURA VISITA PUERTA EQUIVOCADA: ';
										$people_ = $rut_completo.' | '.$people[0]['name'].' '.$people[0]['last_name'].' | '.$people[0]['company'];
										if($this->add_notification($title.$people_, $doors_id, $sensors_id, $action, $date_time_now))
										{
											$this->sendPop_up($title, $people_, $doors_name, $sensors_name, $action, $ubication, $date_time_now, 1);
											
											//datos intento
											$data = array(
												'access_people_id' => $access_people_id,
												'doors_id' => $doors_id,
												'entry' => $action,
												'success' => 0,
												'flow' => 0,
												'created' => $date_time_now
											);
											//registrar intento
											if($this->modelo->add_access_people_intents($data))
												echo 1;
											else
												echo 0;
										}
									}
								}
								else
								{
									//ENTRADA
									//ACCESO VISITA NO PERMITIDO POR PUERTA EQUIVOCADA
									$title = 'ACCESO VISITA NO PERMITIDO POR PUERTA EQUIVOCADA: ';
									$people_ = $rut_completo.' | '.$people[0]['name'].' '.$people[0]['last_name'].' | '.$people[0]['company'];
									if($this->add_notification($title.$people_, $doors_id, $sensors_id, $action, $date_time_now))
									{
										$this->sendPop_up($title, $people_, $doors_name, $sensors_name, $action, $ubication, $date_time_now, 1);
										
										//datos intento
										$data = array(
											'access_people_id' => $access_people_id,
											'doors_id' => $doors_id,
											'entry' => $action,
											'success' => 0,
											'flow' => 0,
											'created' => $date_time_now
										);
										//registrar intento
										if($this->modelo->add_access_people_intents($data))
											echo 0;
										else
											echo 0;
									}
								}

							}
						}
						else
						{
							//AUTORIZACION EXCEDIDA, PERO NO SE HA CERRADO
							//VALIDAR SI ES SALIDA
							if($action == 1)
							{
								//SALIDA
								if($level == 0)
								{
									//si es salida perimetral no puede
									//obligandolo a salir por porteria
									$title = 'SALIDA VISITA NO PERMITIDO POR FUERA DE HORARIO: ';
									$people_ = $rut_completo.' | '.$people[0]['name'].' '.$people[0]['last_name'].' | '.$people[0]['company'];
									if($this->add_notification($title.$people_, $doors_id, $sensors_id, $action, $date_time_now))
									{
										$this->sendPop_up($title, $people_, $doors_name, $sensors_name, $action, $ubication, $date_time_now, 1);
										//datos intento
										$data = array(
											'access_people_id' => $access_people_id,
											'doors_id' => $doors_id,
											'entry' => $action,
											'success' => 0,
											'flow' => 0,
											'created' => $date_time_now
										);
										//registrar intento
										if($this->modelo->add_access_people_intents($data))
											echo 0;
										else
											echo 0;
									}
								}
								else
								{
									$res = $this->add_current_internal_state($people_id, 1, $people_id, 0, $doors_id, $level,$action, $date_time_now, $rut_completo, $people_name, $company, 0, '');

									//si es salida de una puerta interna si puede
									$title = 'APERTURA VISITA FUERA DE HORARIO: ';
									$people_ = $rut_completo.' | '.$people[0]['name'].' '.$people[0]['last_name'].' | '.$people[0]['company'];
									if($this->add_notification($title.$people_, $doors_id, $sensors_id, $action, $date_time_now))
									{
										$this->sendPop_up($title, $people_, $doors_name, $sensors_name, $action, $ubication, $date_time_now, 1);
										
										//datos intento
										$data = array(
											'access_people_id' => $access_people_id,
											'doors_id' => $doors_id,
											'entry' => $action,
											'success' => 0,
											'flow' => 0,
											'created' => $date_time_now
										);
										//registrar intento
										if($this->modelo->add_access_people_intents($data))
											echo 1;
										else
											echo 0;
									}
								}
							}
							else
							{
								//ENTRADA
								//ACCESO VISITA NO PERMITIDO POR FUERA DE HORARIO
								$title = 'ACCESO VISITA NO PERMITIDO POR FUERA DE HORARIO: ';
								$people_ = $rut_completo.' | '.$people[0]['name'].' '.$people[0]['last_name'].' | '.$people[0]['company'];
								if($this->add_notification($title.$people_, $doors_id, $sensors_id, $action, $date_time_now))
								{
									$this->sendPop_up($title, $people_, $doors_name, $sensors_name, $action, $ubication, $date_time_now, 1);
									
									//datos intento
									$data = array(
										'access_people_id' => $access_people_id,
										'doors_id' => $doors_id,
										'entry' => $action,
										'success' => 0,
										'flow' => 0,
										'created' => $date_time_now
									);
									//registrar intento
									if($this->modelo->add_access_people_intents($data))
										echo 0;
									else
										echo 0;
								}
							}
						}
					}
					else
					{
						if($action == 1)
						{
							//VISITA SIN AUTORIZACION
							$title = 'VISITA SIN AUTORIZACION: ';
							$people_ = $rut_completo.' | '.$people[0]['name'].' '.$people[0]['last_name'].' | '.$people[0]['company'];
							if($this->add_notification($title.$people_, $doors_id, $sensors_id, $action, $date_time_now))
							{
								$this->sendPop_up($title, $people_, $doors_name, $sensors_name, $action, $ubication, $date_time_now, 1);

								echo 1;
							}
						}
						else
						{
							//VISITA SIN AUTORIZACION
							$title = 'VISITA SIN AUTORIZACION: ';
							$people_ = $rut_completo.' | '.$people[0]['name'].' '.$people[0]['last_name'].' | '.$people[0]['company'];
							if($this->add_notification($title.$people_, $doors_id, $sensors_id, $action, $date_time_now))
							{
								$this->sendPop_up($title, $people_, $doors_name, $sensors_name, $action, $ubication, $date_time_now, 1);

								echo 0;
							}
						}
						
					}

				}
				else if($internal == 1)
				{
					//INTERNO
					//ACCESO TOTAL
					//aqui quede
					if($allow_all == 1)
					{
						$data = array(
							'people_id' => $people_id,
							'entry' => $action,
							'success' => 1,
							'reasons_error_id' => 0,
							'doors_id' => $doors_id,
							'created' => $date_time_now
						);


						//------------------------------------

						$data2 = array(
							'cod' => $people_id,
							'flow' => 0,
							'internal' => $internal,
							'people_id' => $people_id,
							'vehicles_id' => 0,
							'doors_id' => $doors_id,
							'action' => $action,
							'created' => $date_time_now
						);

						$id = $this->modelo->get_current_internal_state($people_id, $internal);

						if($id > 0)
						{
							if($action == 0)
							{
								//entrar
								if($this->modelo->edit_current_internal_state($data2, $id))
								{
									if($this->modelo->add_internal_people($data))
										echo 1;
									else
										echo 0;
								}
								else
									echo 0;
							}
							else
							{
								//salir
								//perimetral
								if($level == 0)
								{
									if($this->modelo->delete_current_internal_state2($people_id))
									{
										if($this->modelo->add_internal_people($data))
											echo 1;
										else
											echo 0;
									}
									else
										echo 0;
								}
								else
								{

									//interior
									if($this->modelo->edit_current_internal_state($data2, $id))
									{
										if($this->modelo->add_internal_people($data))
											echo 1;
										else
											echo 0;
									}
									else
										echo 0;
								}
							}
						}
						else
						{
							if($this->modelo->add_current_internal_state($data2))
							{
								if($this->modelo->add_internal_people($data))
									echo 1;
								else
									echo 0;
							}
							else
								echo 0;
						}
						//--------------------------------------
					}
					else
					{
						//verificar permiso especial
						$special_schedule = $this->modelo->getSpecial_Schedule($people_id, $doors_id, $date_time_now);
						if($special_schedule != 0)
						{
							//tiene permiso especial
							$res = $this->add_current_internal_state($people_id, 1, $people_id, 0, $doors_id, $level, $action, $date_time_now, $rut_completo, $people_name, $company, 0, $special_schedule);

							if($res == 1)
							{
								//ok
								$data = array(
									'people_id' => $people_id,
									'entry' => $action,
									'success' => 1,
									'reasons_error_id' => 0,
									'doors_id' => $doors_id,
									'created' => $date_time_now
								);
								if($this->modelo->add_internal_people($data))
									echo 1;
								else
									echo 0;
							}
							else if($res == 0)
							{
								//error
								$title = 'ERROR INTERNO, INTENTO APERTURA: ';
								$people_ = $rut_completo.' | '.$people[0]['name'].' '.$people[0]['last_name'].' | '.$people[0]['company'];
								if($this->add_notification($title.$people_, $doors_id, $sensors_id, $action, $date_time_now))
								{
									$this->sendPop_up($title, $people_, $doors_name, $sensors_name, $action, $ubication, $date_time_now, 1);

									//datos intento
									$data = array(
										'people_id' => $people_id,
										'entry' => $action,
										'success' => 0,
										'reasons_error_id' => 1,
										'doors_id' => $doors_id,
										'created' => $date_time_now
									);
									if($this->modelo->add_internal_people($data))
										echo 0;
									else
										echo 0;
								}

							}
							else if($res == 3)
							{
								$title = 'SALIDA INTERNO SIN PREVIA ENTRADA: ';
								$people_ = $rut_completo.' | '.$people[0]['name'].' '.$people[0]['last_name'].' | '.$people[0]['company'];
								if($this->add_notification($title.$people_, $doors_id, $sensors_id, $action, $date_time_now))
								{
									$this->sendPop_up($title, $people_, $doors_name, $sensors_name, $action, $ubication, $date_time_now, 1);

									//datos intento
									$data = array(
										'people_id' => $people_id,
										'entry' => $action,
										'success' => 0,
										'reasons_error_id' => 1,
										'doors_id' => $doors_id,
										'created' => $date_time_now
									);
									if($this->modelo->add_internal_people($data))
										echo 0;
									else
										echo 0;
								}
							}
							else if($res == 4)
							{
								$title = 'INGRESO INTERNO PUERTA INTERNA SIN PREVIAMENTE PASAR POR EXTERNA: ';
								$people_ = $rut_completo.' | '.$people[0]['name'].' '.$people[0]['last_name'].' | '.$people[0]['company'];
								if($this->add_notification($title.$people_, $doors_id, $sensors_id, $action, $date_time_now))
								{
									$this->sendPop_up($title, $people_, $doors_name, $sensors_name, $action, $ubication, $date_time_now, 1);

									//datos intento
									$data = array(
										'people_id' => $people_id,
										'entry' => $action,
										'success' => 0,
										'reasons_error_id' => 1,
										'doors_id' => $doors_id,
										'created' => $date_time_now
									);
									if($this->modelo->add_internal_people($data))
										echo 0;
									else
										echo 0;
								}
							}
						}
						else
						{
							//esquema de horarios-perfil
							$profiles_schedules = $this->modelo->getDoor_Profiles_Schedules($people_profiles_id,$time_now, $dow);

							if(!empty($profiles_schedules))
							{
								//posee esquema de horarios
								//validar esquema de horarios

								$time_init = $profiles_schedules[0]['time_init'];
								$time_end = $profiles_schedules[0]['time_end'];

								if($time_init < $time_end)
								{
									if(strtotime($time_init) <= strtotime($time_now) && strtotime($time_end) >= strtotime($time_now))
									{
										//esta dentro del rango
										if(in_array($doors_id,array_column($profiles_schedules,'doors_id')))
			    						{
			    							//posee puertas
			    							//aqui quede

			    							//return [0 = error;1 = ok;2 = controlar al salir vehiculo;3 = salida sin haber entrado; 4 = intento ingreso puerta interna sin pasar por externa]

			    							$res = $this->add_current_internal_state($people_id, 1, $people_id, 0, $doors_id, $level,$action, $date_time_now, $rut_completo, $people_name, $company, 0, $date_now.' '.$time_end);

			    							if($res == 1)
			    							{
			    								//ok
												$data = array(
													'people_id' => $people_id,
													'entry' => $action,
													'success' => 1,
													'reasons_error_id' => 0,
													'doors_id' => $doors_id,
													'created' => $date_time_now
												);
												if($this->modelo->add_internal_people($data))
													echo 1;
												else
													echo 0;
			    							}
			    							else if($res == 0)
			    							{
			    								//error
			    								$title = 'ERROR INTERNO, INTENTO APERTURA: ';
												$people_ = $rut_completo.' | '.$people[0]['name'].' '.$people[0]['last_name'].' | '.$people[0]['company'];
												if($this->add_notification($title.$people_, $doors_id, $sensors_id, $action, $date_time_now))
												{
													$this->sendPop_up($title, $people_, $doors_name, $sensors_name, $action, $ubication, $date_time_now, 1);

													//datos intento
													$data = array(
														'people_id' => $people_id,
														'entry' => $action,
														'success' => 0,
														'reasons_error_id' => 1,
														'doors_id' => $doors_id,
														'created' => $date_time_now
													);
													if($this->modelo->add_internal_people($data))
														echo 0;
													else
														echo 0;
												}

			    							}
			    							else if($res == 3)
			    							{
			    								//salida sin haber entrado
			    								$title = 'SALIDA INTERNO SIN PREVIA ENTRADA: ';
												$people_ = $rut_completo.' | '.$people[0]['name'].' '.$people[0]['last_name'].' | '.$people[0]['company'];
												if($this->add_notification($title.$people_, $doors_id, $sensors_id, $action, $date_time_now))
												{
													$this->sendPop_up($title, $people_, $doors_name, $sensors_name, $action, $ubication, $date_time_now, 1);

													//datos intento
													$data = array(
														'people_id' => $people_id,
														'entry' => $action,
														'success' => 0,
														'reasons_error_id' => 1,
														'doors_id' => $doors_id,
														'created' => $date_time_now
													);
													if($this->modelo->add_internal_people($data))
														echo 0;
													else
														echo 0;
												}
			    							}
			    							else if($res == 4)
			    							{
			    								//ingreso puerta interna sin haber entrado por externa
			    								$title = 'INGRESO INTERNO PUERTA INTERNA SIN PREVIAMENTE PASAR POR EXTERNA: ';
												$people_ = $rut_completo.' | '.$people[0]['name'].' '.$people[0]['last_name'].' | '.$people[0]['company'];
												if($this->add_notification($title.$people_, $doors_id, $sensors_id, $action, $date_time_now))
												{
													$this->sendPop_up($title, $people_, $doors_name, $sensors_name, $action, $ubication, $date_time_now, 1);

													//datos intento
													$data = array(
														'people_id' => $people_id,
														'entry' => $action,
														'success' => 0,
														'reasons_error_id' => 1,
														'doors_id' => $doors_id,
														'created' => $date_time_now
													);
													if($this->modelo->add_internal_people($data))
														echo 0;
													else
														echo 0;
												}
			    							}
			    						}
			    						else
			    						{
			    							//generar registro por error 1, PUERTA NO AUTORIZADA
			    							if($action == 1)
											{
												//salida
												//se abre

												//genera salida de la instalacion si es que es perimetral
												if($level == 0)
												{
													$res = $this->add_current_internal_state($people_id, 1, $people_id, 0, $doors_id, $level,$action, $date_time_now, $rut_completo, $people_name, $company, 0, '');
												}

												$title = 'APERTURA GENERADA, INTERNO PUERTA EQUIVOCADA: ';
													$people_ = $rut_completo.' | '.$people[0]['name'].' '.$people[0]['last_name'].' | '.$people[0]['company'];
												if($this->add_notification($title.$people_, $doors_id, $sensors_id, $action, $date_time_now))
												{
													$this->sendPop_up($title, $people_, $doors_name, $sensors_name, $action, $ubication, $date_time_now, 1);

													//datos intento
													$data = array(
														'people_id' => $people_id,
														'entry' => $action,
														'success' => 0,
														'reasons_error_id' => 1,
														'doors_id' => $doors_id,
														'created' => $date_time_now
													);
													if($this->modelo->add_internal_people($data))
														echo 1;
													else
														echo 0;
												}

											}
											else
											{
												//entrada
												$title = 'INTERNO PUERTA EQUIVOCADA: ';
													$people_ = $rut_completo.' | '.$people[0]['name'].' '.$people[0]['last_name'].' | '.$people[0]['company'];
												if($this->add_notification($title.$people_, $doors_id, $sensors_id, $action, $date_time_now))
												{
													$this->sendPop_up($title, $people_, $doors_name, $sensors_name, $action, $ubication, $date_time_now, 1);

													//datos intento
													$data = array(
														'people_id' => $people_id,
														'entry' => $action,
														'success' => 0,
														'reasons_error_id' => 1,
														'doors_id' => $doors_id,
														'created' => $date_time_now
													);
													if($this->modelo->add_internal_people($data))
														echo 0;
													else
														echo 0;
												}
											}
			    						}
									}
								}
								else
								{

									if(strtotime($time_end) < strtotime($time_now) && strtotime($time_init) > strtotime($time_now))
									{
										//no esta dentro del rango
										if($action == 1)
										{
											//salida
											if($level == 0)
											{
												//perimetral
												$title = 'INTERNO FUERA DE HORARIO: ';
												$people_ = $rut_completo.' | '.$people[0]['name'].' '.$people[0]['last_name'].' | '.$people[0]['company'];
												if($this->add_notification($title.$people_, $doors_id, $sensors_id, $action, $date_time_now))
												{
													$this->sendPop_up($title, $people_, $doors_name, $sensors_name, $action, $ubication, $date_time_now, 1);

													//datos intento
													$data = array(
														'people_id' => $people_id,
														'entry' => $action,
														'success' => 0,
														'reasons_error_id' => 2,
														'doors_id' => $doors_id,
														'created' => $date_time_now
													);
													if($this->modelo->add_internal_people($data))
														echo 0;
													else
														echo 0;
												}
											}
											else
											{
												//interna

												$res = $this->add_current_internal_state($people_id, 1, $people_id, 0, $doors_id, $level,$action, $date_time_now, $rut_completo, $people_name, $company, 0, '');

												$title = 'INTERNO FUERA DE HORARIO: ';
												$people_ = $rut_completo.' | '.$people[0]['name'].' '.$people[0]['last_name'].' | '.$people[0]['company'];
												if($this->add_notification($title.$people_, $doors_id, $sensors_id, $action, $date_time_now))
												{
													$this->sendPop_up($title, $people_, $doors_name, $sensors_name, $action, $ubication, $date_time_now, 1);

													//datos intento
													$data = array(
														'people_id' => $people_id,
														'entry' => $action,
														'success' => 0,
														'reasons_error_id' => 2,
														'doors_id' => $doors_id,
														'created' => $date_time_now
													);
													if($this->modelo->add_internal_people($data))
														echo 1;
													else
														echo 0;
												}
											}
											
										}
										else
										{
											//entrada
											$title = 'INTERNO FUERA DE HORARIO: ';
											$people_ = $rut_completo.' | '.$people[0]['name'].' '.$people[0]['last_name'].' | '.$people[0]['company'];
											if($this->add_notification($title.$people_, $doors_id, $sensors_id, $action, $date_time_now))
											{
												$this->sendPop_up($title, $people_, $doors_name, $sensors_name, $action, $ubication, $date_time_now, 1);

												//datos intento
												$data = array(
													'people_id' => $people_id,
													'entry' => $action,
													'success' => 0,
													'reasons_error_id' => 2,
													'doors_id' => $doors_id,
													'created' => $date_time_now
												);
												if($this->modelo->add_internal_people($data))
													echo 0;
												else
													echo 0;
											}
										}
										
									}
									else
									{
										//validar si la hora actual es menor a 23:59:59 sumar a date_now +1 dia
										//de lo contrario si es superior a 00:00:00 conservar date_now

										if(strtotime($time_now) <= strtotime('23:59:59'))
										{
											$date_now = strtotime ('+1 day' , strtotime($date_now)) ;
											$date_now = date ('Y-m-d' , $date_now );
										}

										//esta dentro del rango
										if(in_array($doors_id,array_column($profiles_schedules,'doors_id')))
			    						{
			    							//posee puertas
			    							//aqui quede

			    							//return [0 = error;1 = ok;2 = controlar al salir vehiculo;3 = salida sin haber entrado; 4 = intento ingreso puerta interna sin pasar por externa]

			    							$res = $this->add_current_internal_state($people_id, 1, $people_id, 0, $doors_id, $level,$action, $date_time_now, $rut_completo, $people_name, $company, 0, $date_now.' '.$time_end);

			    							if($res == 1)
			    							{
			    								//ok
												$data = array(
													'people_id' => $people_id,
													'entry' => $action,
													'success' => 1,
													'reasons_error_id' => 0,
													'doors_id' => $doors_id,
													'created' => $date_time_now
												);
												if($this->modelo->add_internal_people($data))
													echo 1;
												else
													echo 0;
			    							}
			    							else if($res == 0)
			    							{
			    								//error
			    								$title = 'ERROR INTERNO, INTENTO APERTURA: ';
												$people_ = $rut_completo.' | '.$people[0]['name'].' '.$people[0]['last_name'].' | '.$people[0]['company'];
												if($this->add_notification($title.$people_, $doors_id, $sensors_id, $action, $date_time_now))
												{
													$this->sendPop_up($title, $people_, $doors_name, $sensors_name, $action, $ubication, $date_time_now, 1);

													//datos intento
													$data = array(
														'people_id' => $people_id,
														'entry' => $action,
														'success' => 0,
														'reasons_error_id' => 1,
														'doors_id' => $doors_id,
														'created' => $date_time_now
													);
													if($this->modelo->add_internal_people($data))
														echo 0;
													else
														echo 0;
												}

			    							}
			    							else if($res == 3)
			    							{
			    								//salida sin haber entrado
			    								$title = 'SALIDA INTERNO SIN PREVIA ENTRADA: ';
												$people_ = $rut_completo.' | '.$people[0]['name'].' '.$people[0]['last_name'].' | '.$people[0]['company'];
												if($this->add_notification($title.$people_, $doors_id, $sensors_id, $action, $date_time_now))
												{
													$this->sendPop_up($title, $people_, $doors_name, $sensors_name, $action, $ubication, $date_time_now, 1);

													//datos intento
													$data = array(
														'people_id' => $people_id,
														'entry' => $action,
														'success' => 0,
														'reasons_error_id' => 1,
														'doors_id' => $doors_id,
														'created' => $date_time_now
													);
													if($this->modelo->add_internal_people($data))
														echo 0;
													else
														echo 0;
												}
			    							}
			    							else if($res == 4)
			    							{
			    								//ingreso puerta interna sin haber entrado por externa
			    								$title = 'INGRESO INTERNO PUERTA INTERNA SIN PREVIAMENTE PASAR POR EXTERNA: ';
												$people_ = $rut_completo.' | '.$people[0]['name'].' '.$people[0]['last_name'].' | '.$people[0]['company'];
												if($this->add_notification($title.$people_, $doors_id, $sensors_id, $action, $date_time_now))
												{
													$this->sendPop_up($title, $people_, $doors_name, $sensors_name, $action, $ubication, $date_time_now, 1);

													//datos intento
													$data = array(
														'people_id' => $people_id,
														'entry' => $action,
														'success' => 0,
														'reasons_error_id' => 1,
														'doors_id' => $doors_id,
														'created' => $date_time_now
													);
													if($this->modelo->add_internal_people($data))
														echo 0;
													else
														echo 0;
												}
			    							}
			    						}
			    						else
			    						{
			    							//generar registro por error 1, PUERTA NO AUTORIZADA
			    							if($action == 1)
											{
												//salida
												//se abre

												//genera salida de la instalacion si es que es perimetral
												if($level == 0)
												{
													$res = $this->add_current_internal_state($people_id, 1, $people_id, 0, $doors_id, $level,$action, $date_time_now, $rut_completo, $people_name, $company, 0, '');
												}

												$title = 'APERTURA GENERADA, INTERNO PUERTA EQUIVOCADA: ';
													$people_ = $rut_completo.' | '.$people[0]['name'].' '.$people[0]['last_name'].' | '.$people[0]['company'];
												if($this->add_notification($title.$people_, $doors_id, $sensors_id, $action, $date_time_now))
												{
													$this->sendPop_up($title, $people_, $doors_name, $sensors_name, $action, $ubication, $date_time_now, 1);

													//datos intento
													$data = array(
														'people_id' => $people_id,
														'entry' => $action,
														'success' => 0,
														'reasons_error_id' => 1,
														'doors_id' => $doors_id,
														'created' => $date_time_now
													);
													if($this->modelo->add_internal_people($data))
														echo 1;
													else
														echo 0;
												}

											}
											else
											{
												//entrada
												$title = 'INTERNO PUERTA EQUIVOCADA: ';
													$people_ = $rut_completo.' | '.$people[0]['name'].' '.$people[0]['last_name'].' | '.$people[0]['company'];
												if($this->add_notification($title.$people_, $doors_id, $sensors_id, $action, $date_time_now))
												{
													$this->sendPop_up($title, $people_, $doors_name, $sensors_name, $action, $ubication, $date_time_now, 1);

													//datos intento
													$data = array(
														'people_id' => $people_id,
														'entry' => $action,
														'success' => 0,
														'reasons_error_id' => 1,
														'doors_id' => $doors_id,
														'created' => $date_time_now
													);
													if($this->modelo->add_internal_people($data))
														echo 0;
													else
														echo 0;
												}
											}
			    						}
									}
								}

								
							}
							else
							{
								//no posee esquema de horarios ni horarios especial
								//generar registro por error 2, HORARIO
								if($action == 1)
								{
									//salida
									//se abre
									//genera salida de la instalacion si es que no es perimetral
									if($level == 1)
									{
										$res = $this->add_current_internal_state($people_id, 1, $people_id, 0, $doors_id, $level,$action, $date_time_now, $rut_completo, $people_name, $company, 0, '');

										$title = 'APERTURA GENERADA, INTERNO SIN ESQUEMA DE HORARIOS: ';
										$people_ = $rut_completo.' | '.$people[0]['name'].' '.$people[0]['last_name'].' | '.$people[0]['company'];
										if($this->add_notification($title.$people_, $doors_id, $sensors_id, $action, $date_time_now))
										{
											$this->sendPop_up($title, $people_, $doors_name, $sensors_name, $action, $ubication, $date_time_now, 1);

											//datos intento
											$data = array(
												'people_id' => $people_id,
												'entry' => $action,
												'success' => 0,
												'reasons_error_id' => 2,
												'doors_id' => $doors_id,
												'created' => $date_time_now
											);
											if($this->modelo->add_internal_people($data))
												echo 1;
											else
												echo 0;
										}
									}
									else
									{
										
										$title = 'APERTURA GENERADA, INTERNO SIN ESQUEMA DE HORARIOS: ';
										$people_ = $rut_completo.' | '.$people[0]['name'].' '.$people[0]['last_name'].' | '.$people[0]['company'];
										if($this->add_notification($title.$people_, $doors_id, $sensors_id, $action, $date_time_now))
										{
											$this->sendPop_up($title, $people_, $doors_name, $sensors_name, $action, $ubication, $date_time_now, 1);

											//datos intento
											$data = array(
												'people_id' => $people_id,
												'entry' => $action,
												'success' => 0,
												'reasons_error_id' => 2,
												'doors_id' => $doors_id,
												'created' => $date_time_now
											);
											if($this->modelo->add_internal_people($data))
												echo 0;
											else
												echo 0;
										}
									}
								}
								else
								{
									//entrada
									$title = 'INTERNO SIN ESQUEMA DE HORARIOS: ';
										$people_ = $rut_completo.' | '.$people[0]['name'].' '.$people[0]['last_name'].' | '.$people[0]['company'];
									if($this->add_notification($title.$people_, $doors_id, $sensors_id, $action, $date_time_now))
									{
										$this->sendPop_up($title, $people_, $doors_name, $sensors_name, $action, $ubication, $date_time_now, 1);

										//datos intento
										$data = array(
											'people_id' => $people_id,
											'entry' => $action,
											'success' => 0,
											'reasons_error_id' => 2,
											'doors_id' => $doors_id,
											'created' => $date_time_now
										);
										if($this->modelo->add_internal_people($data))
											echo 0;
										else
											echo 0;
									}
								}
							}
						}
					}
				}
				else if($internal == 2)
				{
					//CONTRATISTA
					//contratista debe poseer un proyecto asociado
					$project = $this->modelo->getLast_Project($people_id, $date_now);
					if(!empty($project))
					{
						$project_id = $project[0]['id'];
						$init = $project[0]['init'];
						$end = $project[0]['end'];

						//existe proyecto vigente entre fechas, ahora validación entre horarios y dia...
						$schedule = $this->modelo->getSchedule_Last_Project($project_id, $time_now, $dow);
						if($schedule != 0)
						{
							$time_init = $schedule[0]['time_init'];
							$time_end = $schedule[0]['time_end'];

							if($time_init < $time_end)
							{
								if(strtotime($time_init) <= strtotime($time_now) && strtotime($time_end) >= strtotime($time_now))
								{
									//esta dentro del rango
									$res = $this->add_current_internal_state($people_id, 2, $people_id, 0, $doors_id, $level,$action, $date_time_now, $rut_completo, $people_name, $company, 0,$date_now.' '.$time_end);

									if($res == 1)
	    							{
	    								//ok
										$data = array(
											'projects_id' => $project_id,
											'people_id' => $people_id,
											'doors_id' => $doors_id,
											'entry' => $action,
											'success' => 1,
											'created' => $date_time_now
										);

										if($this->modelo->add_projects_intents($data))
											echo 1;
										else
											echo 0;
	    							}
	    							else if($res == 0)
	    							{
	    								//error
	    								$title = 'ERROR INTERNO, INTENTO APERTURA: ';
										$people_ = $rut_completo.' | '.$people[0]['name'].' '.$people[0]['last_name'].' | '.$people[0]['company'];
										if($this->add_notification($title.$people_, $doors_id, $sensors_id, $action, $date_time_now))
										{
											$this->sendPop_up($title, $people_, $doors_name, $sensors_name, $action, $ubication, $date_time_now, 1);

											//datos intento
											$data = array(
												'projects_id' => $project_id,
												'people_id' => $people_id,
												'doors_id' => $doors_id,
												'entry' => $action,
												'success' => 0,
												'created' => $date_time_now
											);
											
											if($this->modelo->add_projects_intents($data))
												echo 0;
											else
												echo 0;
										}

	    							}
	    							else if($res == 3)
	    							{
	    								//salida sin haber entrado
	    								$title = 'SALIDA CONTRATISTA SIN PREVIA ENTRADA: ';
										$people_ = $rut_completo.' | '.$people[0]['name'].' '.$people[0]['last_name'].' | '.$people[0]['company'];
										if($this->add_notification($title.$people_, $doors_id, $sensors_id, $action, $date_time_now))
										{
											$this->sendPop_up($title, $people_, $doors_name, $sensors_name, $action, $ubication, $date_time_now, 1);

											//datos intento
											$data = array(
												'projects_id' => $project_id,
												'people_id' => $people_id,
												'doors_id' => $doors_id,
												'entry' => $action,
												'success' => 0,
												'created' => $date_time_now
											);
											
											if($this->modelo->add_projects_intents($data))
												echo 0;
											else
												echo 0;
										}
	    							}
	    							else if($res == 4)
	    							{
	    								//ingreso puerta interna sin haber entrado por externa
	    								$title = 'INGRESO CONTRATISTA PUERTA INTERNA SIN PREVIAMENTE PASAR POR EXTERNA: ';
										$people_ = $rut_completo.' | '.$people[0]['name'].' '.$people[0]['last_name'].' | '.$people[0]['company'];
										if($this->add_notification($title.$people_, $doors_id, $sensors_id, $action, $date_time_now))
										{
											$this->sendPop_up($title, $people_, $doors_name, $sensors_name, $action, $ubication, $date_time_now, 1);

											//datos intento
											$data = array(
												'projects_id' => $project_id,
												'people_id' => $people_id,
												'doors_id' => $doors_id,
												'entry' => $action,
												'success' => 0,
												'created' => $date_time_now
											);
											
											if($this->modelo->add_projects_intents($data))
												echo 1;
											else
												echo 0;
										}
	    							}
								}
							}
							else
							{

								if(strtotime($time_end) < strtotime($time_now) && strtotime($time_init) > strtotime($time_now))
								{
									//no esta dentro del rango
									if($action == 1)
									{
										//salida
										if($level == 1)
										{
											//puerta interna
											$title = 'CONTRATISTA FUERA DE HORARIO: ';
											$people_ = $rut_completo.' | '.$people[0]['name'].' '.$people[0]['last_name'].' | '.$people[0]['company'];
											if($this->add_notification($title.$people_, $doors_id, $sensors_id, $action, $date_time_now))
											{
												$this->sendPop_up($title, $people_, $doors_name, $sensors_name, $action, $ubication, $date_time_now, 1);

												//datos intento
												$data = array(
													'projects_id' => $project_id,
													'people_id' => $people_id,
													'doors_id' => $doors_id,
													'entry' => $action,
													'success' => 0,
													'created' => $date_time_now
												);
												
												if($this->modelo->add_projects_intents($data))
													echo 1;
												else
													echo 0;
											}
										}
										else
										{
											//puerta externa
											$title = 'CONTRATISTA FUERA DE HORARIO: ';
											$people_ = $rut_completo.' | '.$people[0]['name'].' '.$people[0]['last_name'].' | '.$people[0]['company'];
											if($this->add_notification($title.$people_, $doors_id, $sensors_id, $action, $date_time_now))
											{
												$this->sendPop_up($title, $people_, $doors_name, $sensors_name, $action, $ubication, $date_time_now, 1);

												//datos intento
												$data = array(
													'projects_id' => $project_id,
													'people_id' => $people_id,
													'doors_id' => $doors_id,
													'entry' => $action,
													'success' => 0,
													'created' => $date_time_now
												);
												
												if($this->modelo->add_projects_intents($data))
													echo 0;
												else
													echo 0;
											}
										}
									}
									else
									{
										//entrada
										$title = 'CONTRATISTA FUERA DE HORARIO: ';
										$people_ = $rut_completo.' | '.$people[0]['name'].' '.$people[0]['last_name'].' | '.$people[0]['company'];
										if($this->add_notification($title.$people_, $doors_id, $sensors_id, $action, $date_time_now))
										{
											$this->sendPop_up($title, $people_, $doors_name, $sensors_name, $action, $ubication, $date_time_now, 1);

											//datos intento
											$data = array(
												'projects_id' => $project_id,
												'people_id' => $people_id,
												'doors_id' => $doors_id,
												'entry' => $action,
												'success' => 0,
												'created' => $date_time_now
											);

											if($this->modelo->add_projects_intents($data))
												echo 0;
											else
												echo 0;
										}
									}
								}
								else
								{
									//esta dentro del rango
									if(strtotime($time_now) <= strtotime('23:59:59'))
									{
										$date_now = strtotime ('+1 day' , strtotime($date_now)) ;
										$date_now = date ('Y-m-d' , $date_now );
									}
									
									$res = $this->add_current_internal_state($people_id, 2, $people_id, 0, $doors_id, $level,$action, $date_time_now, $rut_completo, $people_name, $company, 0,$date_now.' '.$time_end);

									if($res == 1)
	    							{
	    								//ok
										$data = array(
											'projects_id' => $project_id,
											'people_id' => $people_id,
											'doors_id' => $doors_id,
											'entry' => $action,
											'success' => 1,
											'created' => $date_time_now
										);

										if($this->modelo->add_projects_intents($data))
											echo 1;
										else
											echo 0;
	    							}
	    							else if($res == 0)
	    							{
	    								//error
	    								$title = 'ERROR INTERNO, INTENTO APERTURA: ';
										$people_ = $rut_completo.' | '.$people[0]['name'].' '.$people[0]['last_name'].' | '.$people[0]['company'];
										if($this->add_notification($title.$people_, $doors_id, $sensors_id, $action, $date_time_now))
										{
											$this->sendPop_up($title, $people_, $doors_name, $sensors_name, $action, $ubication, $date_time_now, 1);

											//datos intento
											$data = array(
												'projects_id' => $project_id,
												'people_id' => $people_id,
												'doors_id' => $doors_id,
												'entry' => $action,
												'success' => 0,
												'created' => $date_time_now
											);
											
											if($this->modelo->add_projects_intents($data))
												echo 0;
											else
												echo 0;
										}

	    							}
	    							else if($res == 3)
	    							{
	    								//salida sin haber entrado
	    								$title = 'SALIDA CONTRATISTA SIN PREVIA ENTRADA: ';
										$people_ = $rut_completo.' | '.$people[0]['name'].' '.$people[0]['last_name'].' | '.$people[0]['company'];
										if($this->add_notification($title.$people_, $doors_id, $sensors_id, $action, $date_time_now))
										{
											$this->sendPop_up($title, $people_, $doors_name, $sensors_name, $action, $ubication, $date_time_now, 1);

											//datos intento
											$data = array(
												'projects_id' => $project_id,
												'people_id' => $people_id,
												'doors_id' => $doors_id,
												'entry' => $action,
												'success' => 0,
												'created' => $date_time_now
											);
											
											if($this->modelo->add_projects_intents($data))
												echo 0;
											else
												echo 0;
										}
	    							}
	    							else if($res == 4)
	    							{
	    								//ingreso puerta interna sin haber entrado por externa
	    								$title = 'INGRESO CONTRATISTA PUERTA INTERNA SIN PREVIAMENTE PASAR POR EXTERNA: ';
										$people_ = $rut_completo.' | '.$people[0]['name'].' '.$people[0]['last_name'].' | '.$people[0]['company'];
										if($this->add_notification($title.$people_, $doors_id, $sensors_id, $action, $date_time_now))
										{
											$this->sendPop_up($title, $people_, $doors_name, $sensors_name, $action, $ubication, $date_time_now, 1);

											//datos intento
											$data = array(
												'projects_id' => $project_id,
												'people_id' => $people_id,
												'doors_id' => $doors_id,
												'entry' => $action,
												'success' => 0,
												'created' => $date_time_now
											);
											
											if($this->modelo->add_projects_intents($data))
												echo 1;
											else
												echo 0;
										}
	    							}
								}
							}
						}
						else
						{
							//no existe validacion de horarios
							if($action == 1)
							{
								//salida
								if($level == 1)
								{
									//puerta interna
									$title = 'APERTURA GENERADA, CONTRATISTA SIN ESQUEMA DE HORARIOS: ';
									$people_ = $rut_completo.' | '.$people[0]['name'].' '.$people[0]['last_name'].' | '.$people[0]['company'];
									if($this->add_notification($title.$people_, $doors_id, $sensors_id, $action, $date_time_now))
									{
										$this->sendPop_up($title, $people_, $doors_name, $sensors_name, $action, $ubication, $date_time_now, 1);

										//datos intento
										$data = array(
											'projects_id' => $project_id,
											'people_id' => $people_id,
											'doors_id' => $doors_id,
											'entry' => $action,
											'success' => 0,
											'created' => $date_time_now
										);
										
										if($this->modelo->add_projects_intents($data))
											echo 1;
										else
											echo 0;
									}
								}
								else
								{
									//puerta externa
									$title = 'CONTRATISTA SIN ESQUEMA DE HORARIOS: ';
									$people_ = $rut_completo.' | '.$people[0]['name'].' '.$people[0]['last_name'].' | '.$people[0]['company'];
									if($this->add_notification($title.$people_, $doors_id, $sensors_id, $action, $date_time_now))
									{
										$this->sendPop_up($title, $people_, $doors_name, $sensors_name, $action, $ubication, $date_time_now, 1);

										//datos intento
										$data = array(
											'projects_id' => $project_id,
											'people_id' => $people_id,
											'doors_id' => $doors_id,
											'entry' => $action,
											'success' => 0,
											'created' => $date_time_now
										);
										
										if($this->modelo->add_projects_intents($data))
											echo 0;
										else
											echo 0;
									}
								}
							}
							else
							{
								//entrada
								$title = 'CONTRATISTA SIN ESQUEMA DE HORARIOS: ';
								$people_ = $rut_completo.' | '.$people[0]['name'].' '.$people[0]['last_name'].' | '.$people[0]['company'];
								if($this->add_notification($title.$people_, $doors_id, $sensors_id, $action, $date_time_now))
								{
									$this->sendPop_up($title, $people_, $doors_name, $sensors_name, $action, $ubication, $date_time_now, 1);

									//datos intento
									$data = array(
										'projects_id' => $project_id,
										'people_id' => $people_id,
										'doors_id' => $doors_id,
										'entry' => $action,
										'success' => 0,
										'created' => $date_time_now
									);

									if($this->modelo->add_projects_intents($data))
										echo 0;
									else
										echo 0;
								}
							}
						}

					}
					else
					{
						$title = 'ACCESO CONTRATISTA NO PERMITIDO NO POSEE PROYECTO VIGENTE: ';
						$people_ = $rut_completo.' | '.$people[0]['name'].' '.$people[0]['last_name'].' | '.$people[0]['company'];
						if($this->add_notification($title.$people_, $doors_id, $sensors_id, $action, $date_time_now))
						{
							$this->sendPop_up($title, $people_, $doors_name, $sensors_name, $action, $ubication, $date_time_now, 1);

							echo 0;
						}
					}
				}
				else
				{
					//PERFIL NO IDENTIFICADO
					$title = 'PERFIL NO IDENTIFICADO: ';
					$people_ = $rut_completo.' | '.$people[0]['name'].' '.$people[0]['last_name'].' | '.$people[0]['company'];
					if($this->add_notification($title.$people_, $doors_id, $sensors_id, $action, $date_time_now))
					{
						$this->sendPop_up($title, $people_, $doors_name, $sensors_name, $action, $ubication, $date_time_now, 1);

						echo 0;
					}
				}
			}
			else if($state == 2)
			{
				//suspendido
				$title = 'INGRESO DE PERSONA SUSPENDIDA: ';
				if($this->add_notification($title.$rut_completo, $doors_id, $sensors_id, $action, $date_time_now))
				{
					$this->sendPop_up($title, $rut_completo, $doors_name, $sensors_name, $action, $ubication, $date_time_now, 1);

					echo 0;
				}
			}
			else
			{
				//bloqueado
				$title = 'INGRESO DE PERSONA BLOQUEADA: ';
				if($this->add_notification($title.$rut_completo, $doors_id, $sensors_id, $action, $date_time_now))
				{
					$this->sendPop_up($title, $rut_completo, $doors_name, $sensors_name, $action, $ubication, $date_time_now, 1);

					echo 0;
				}
			}
		}
		else
		{
			$title = 'INGRESO NO IDENTIFICADO: ';
			if($this->add_notification($title.$rut_completo, $doors_id, $sensors_id, $action, $date_time_now))
			{
				$this->sendPop_up($title, $rut_completo, $doors_name, $sensors_name, $action, $ubication, $date_time_now, 1);

				echo 0;
			}
		}
	}

	private function sendPop_up($title, $people, $door, $sensor, $action, $ubication, $date_time_now, $pop_up)
	{
		if($action == 1)
			$action = 'SALIDA';
		else
			$action = 'ENTRADA';

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, str_replace(" ","%20",utf8_decode($this->node_url.'?title='.$title.'&people='.$people.'&door='.$door.'&sensor='.$sensor.'&entry='.$action.'&ubication='.$ubication.'&date_time_now='.$date_time_now.'&pop_up='.$pop_up)));
		curl_setopt($ch, CURLOPT_ENCODING, 'ISO-8859-1');
		curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$output = curl_exec ($ch);
		curl_close($ch);
	}

	private function getAccessPeopleID($people_id)
	{
		$this->db->select('id');
		$this->db->from('access_people');
		$this->db->where('people_id', $people_id);
		$this->db->order_by('id', 'desc');
		$this->db->limit(1);
		$res = $this->db->get()->result_array();
		if(!empty($res[0]['id']))
			return $res[0]['id'];
		else
			return 0;
	}



	private function generateAccessInOutVisits($rut, $name, $company, $vehicle, $date_entrance, $exit, $people_id)
	{
		$access_people_id = $this->getAccessPeopleID($people_id);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, str_replace(' ','%20', utf8_decode($this->node_visit.'?access_people_id='.$access_people_id.'&people_rut='.$rut.'&people_name='.$name.'&companies_name='.$company.'&vehicle_patent='.$vehicle.'&access_people_date='.$date_entrance.'&exit='.$exit)));
		curl_setopt($ch, CURLOPT_ENCODING, 'ISO-8859-1');
		curl_setopt($ch,CURLOPT_USERAGENT,'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$output = curl_exec ($ch);
		curl_close($ch);
	}


	public function getAccessPeople_Rut()
	{
		$rut 	=  trim($this->input->post('rut', TRUE));
		$rut = substr($rut, 0, (strlen($rut)-1));

		$people = $this->modelo->getPeople_Rut($rut);

		if(!empty($people))
			echo 1;
		else
			echo 0;

	}

	public function getDayOfWeek()
	{
		$day_of_week = array('D', 'L', 'M', 'Mi', 'J', 'V', 'S');
		$dow_numeric = date('w');
		return $day_of_week[$dow_numeric];
	}

	private function add_notification($notification, $doors_id, $sensors_id, $entry, $date_time_now)
	{
		$data = array(
			'notification'	=>	$notification,
			'doors_id'		=>	$doors_id,
			'sensors_id'	=>	$sensors_id,
			'entry'			=> 	$entry,
			'created'		=>	$date_time_now
		);

		if($this->db->insert('notifications', $data))
			return true;
		else
			return false;
	}

	private function add_current_internal_state($cod, $internal, $people_id, $vehicles_id, $doors_id, $level, $action, $date_time_now, $rut, $name, $company, $flow, $end_time)
	{
		$data;

		if(!empty($end_time))
		{
			$data = array(
				'cod' => $cod,
				'flow' => $flow,
				'internal' => $internal,
				'people_id' => $people_id,
				'vehicles_id' => $vehicles_id,
				'doors_id' => $doors_id,
				'action' => $action,
				'end_time' => $end_time,
				'created' => $date_time_now
			);
		}
		else
		{
			$data = array(
				'cod' => $cod,
				'flow' => $flow,
				'internal' => $internal,
				'people_id' => $people_id,
				'vehicles_id' => $vehicles_id,
				'doors_id' => $doors_id,
				'action' => $action,
				'created' => $date_time_now
			);
		}

		$id = $this->modelo->get_current_internal_state($cod, $internal);

		//return [0 = error;1 = ok;2 = controlar al salir vehiculo;3 = salida sin haber entrado; 4 = intento ingreso puerta interna sin pasar por externa]
		//existe registro
		if($id > 0)
		{
			if($action == 0)
			{
				//entrar
				if($this->modelo->edit_current_internal_state($data, $id))
					return 1;
				else
					return 0;
			}
			else
			{
				//salir
				//perimetral
				if($level == 0)
				{
					//visita
					if($internal == 0)
					{
						//generar salida por node
						//validar si el registro de acceso de visita tiene control de salida para
						//generar la salida
						$access_people_id = $this->getAccessPeopleID($people_id);
						if($this->modelo->getControl_Out_Visit($access_people_id) == false)
						{
							if($this->modelo->delete_current_internal_state2($people_id))
							{
								if($this->modelo->generateAccessOut($access_people_id, $date_time_now, ''))
								{
									$this->generateAccessInOutVisits($rut, $name, $company, $this->getPatentVehicle($vehicles_id), $date_time_now,1, $cod);
									return 1;
								}
								else
									return 0;
								
							}
							else
								return 0;
						}
						else
							return 2;
						
					}
					else
					{
						//no es visita
						if($this->modelo->delete_current_internal_state2($people_id))
						{
							return 1;
						}
						else
							return 0;
					}
					
				}
				else
				{
					//interior
					if($this->modelo->edit_current_internal_state($data, $id))
						return 1;
					else
						return 0;
				}
			}
		}
		else
		{
			//no existe
			if($action == 0)
			{
				//entrar
				if($level == 0)
				{
					//perimetral
					if($this->modelo->add_current_internal_state($data))
					{
						if($internal == 0)
						{
							//solo visita
							if($vehicles_id > 0)
							{
								$data = array(
									'cod' => $vehicles_id,
									'flow' => 1,
									'internal' => $internal,
									'people_id' => $people_id,
									'vehicles_id' => $vehicles_id,
									'doors_id' => $doors_id,
									'action' => $action,
									'end_time' => $end_time,
									'created' => $date_time_now
								);

								$this->modelo->add_current_internal_state($data);
							}

							//es visita, generar node para visitas
							$this->generateAccessInOutVisits($rut, $name, $company, $this->getPatentVehicle($vehicles_id), $date_time_now,0, $cod);

							return 1;
						}
						else
							return 1;
					}
					else
						return 0;
				}
				else
					return 4;
			}
			else
				return 3;
		}
	}

	private function getPatentVehicle($id)
	{
		if($id > 0)
		{
			$this->db->select('patent');
			$this->db->from('vehicles');
			$this->db->where('id', $id);
			$this->db->limit(1);

			$res = $this->db->get()->result_array();
			if(!empty($res[0]['patent']))
				return $res[0]['patent'];
			else
				return 'N/A';
		}
		else
			return 'N/A';
		
	}

	private function getDoorbyId($doors_id)
	{
		$this->db->select('door');
		$this->db->from('doors');
		$this->db->where('id', $doors_id);
		$this->db->limit(1);
		$res = $this->db->get()->result_array();
		if(!empty($res[0]['door']))
			return $res[0]['door'];
		else
			return 'PUERTA NO IDENTIFICADA';
	}

	private function getSensorbyId($sensors_id)
	{
		$this->db->select('sensor');
		$this->db->from('sensors');
		$this->db->where('id', $sensors_id);
		$this->db->limit(1);
		$res = $this->db->get()->result_array();
		if(!empty($res[0]['sensor']))
			return $res[0]['sensor'];
		else
			return 'SENSOR NO IDENTIFICADO';
	}


}