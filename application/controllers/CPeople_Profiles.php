<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CPeople_Profiles extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('mPeople_Profiles', 'modelo');
		if (empty($this->session->userdata('options'))) {
			redirect('welcome');
		}
		else if (in_array('08020000', $this->session->userdata('options'))) {
			redirect('welcome');
		}
	}

	public function index() {
		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('people_profiles/index');
	}

	//Datatable
	public function datatable() {
		$start = $this->input->post('start');
		$length = $this->input->post('length');
		$search = $this->input->post('search')['value'];
		$by = $this->input->post('order')['0']['column'];
		$order = $this->input->post('order')['0']['dir'];

		$result = $this->modelo->getPeople_Profiles($start, $length, $search, $order, $by);

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
            "recordsTotal"    => intval($result['numDataTotal']),
            "recordsFiltered" => intval($result['numDataFilter']),
            "data"            => $result['data']
            );

        echo json_encode($json_data);
	}

	//Vistas
	public function add() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('people_profiles/add');
	}

	public function edit() {
		if (!$this->session->userdata('edit')) {
			redirect('welcome');
		}
		$id = trim($this->input->get('id', TRUE));

		$people_profiles = $this->modelo->getPeople_Profile($id);

		$data = array('people_profiles' => $people_profiles);

		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('people_profiles/edit', $data);
	}

	public function view() {
		$id = trim($this->input->get('id', TRUE));

		$people_profiles = $this->modelo->getPeople_Profile($id);

		$data = array('people_profiles' => $people_profiles);

		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('people_profiles/view', $data);
	}

	//Crud
	public function addPeople_Profile() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$profile 	= trim($this->input->post('profile', TRUE));
		
		$date_time = date('Y-m-d H:i:s');

		$data = array(
			'profile' => $profile,
			'created' => $date_time,
			'modified' => $date_time
		);

		if($this->modelo->addPeople_Profile($data)) {
			echo '1';
		}
		else {
			echo '0';
		}
	}

	public function editPeople_Profile() {
		if (!$this->session->userdata('edit')) {
			redirect('welcome');
		}
		$id = trim($this->input->post('id', TRUE));

		$profile 	= trim($this->input->post('profile', TRUE));
		
		$date_time = date('Y-m-d H:i:s');

		$data = array(
			'profile' 	=> $profile,
			'modified' 	=> $date_time
		);

		if($this->modelo->editPeople_Profile($data, $id))
		{
			echo '1';
		}
		else
		{
			echo '0';
		}
	}

	public function deletePeople_Profile() {
		if (!$this->session->userdata('del')) {
			redirect('welcome');
		}
		$id = trim($this->input->post('id', TRUE)); 

		if($this->modelo->deletePeople_Profile($id)) {
			echo '1';
		}
		else {
			echo '0';
		}
	}
}