<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CDepartments extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('mDepartments', 'modelo');
		$this->load->model('mAreas', 'model');
		if (empty($this->session->userdata('options'))) {
			redirect('welcome');
		}
		else if (in_array('01020301', $this->session->userdata('options'))) {
			redirect('welcome');
		}
	}

	public function index() {
		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('departments/index');
	}

	//Datatable
	public function datatable() {
		$start = $this->input->post('start');
		$length = $this->input->post('length');
		$search = $this->input->post('search')['value'];
		$by = $this->input->post('order')['0']['column'];
		$order = $this->input->post('order')['0']['dir'];

		$result = $this->modelo->getDepartments($start, $length, $search, $order, $by);

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
            "recordsTotal"    => intval($result['numDataTotal']),
            "recordsFiltered" => intval($result['numDataFilter']),
            "data"            => $result['data']
            );

        echo json_encode($json_data);
	}

	//Vistas
	public function add() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$this->load->view('header');
		$this->load->view('aside');
		$areas = $this->modelo->getAreas();
		$encargado = $this->modelo->getPeople();
		$data = array(
			'areas' => $areas,
			'encargado' => $encargado
		);

		$this->load->view('departments/add', $data);
	}

	public function edit() {
		if (!$this->session->userdata('edit')) {
			redirect('welcome');
		}
		$id = trim($this->input->get('id', TRUE));

		$department = $this->modelo->getDepartment($id);
		$areas = $this->modelo->getAreas();
		$encargado = $this->modelo->getPeople();
		
		$data = array(
			'areas' => $areas,
			'encargado' => $encargado,
			'department' => $department
		);

		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('departments/edit', $data);
	}

	public function view() {
		$id = trim($this->input->get('id', TRUE));

		$department = $this->modelo->getDepartment($id);
		$areas = $this->modelo->getAreas();
		$encargado = $this->modelo->getPeople();
		
		$data = array(
			'areas' => $areas,
			'encargado' => $encargado,
			'department' => $department
		);

		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('departments/view', $data);
	}

	//Crud
	public function addDepartments() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$department 	= trim($this->input->post('department', TRUE));
		$areas_id		= trim($this->input->post('area_id', TRUE));
		$in_charge		= trim($this->input->post('in_charge', TRUE));
		
		$date_time = date('Y-m-d H:i:s');

		$data = array(
			'department' => $department,
			'areas_id' => $areas_id,
			'in_charge' => $in_charge,
			'created' => $date_time,
			'modified' => $date_time
		);

		if($this->modelo->addDepartments($data)) {
			echo '1';
		}
		else {
			echo '0';
		}
	}

	public function editDepartments() {
		if (!$this->session->userdata('edit')) {
			redirect('welcome');
		}
		$id 		= trim($this->input->post('id', TRUE));
		$department = trim($this->input->post('department', TRUE));
		$area 		= trim($this->input->post('areas_id', TRUE));
		$in_charge		= trim($this->input->post('in_charge', TRUE));
		
		$date_time = date('Y-m-d H:i:s');

		$data = array(
			'department' 	=> $department,
			'areas_id' 		=> $area,
			'in_charge' => $in_charge,
			'modified' 		=> $date_time
		);

		if($this->modelo->editDepartments($data, $id))
		{
			echo '1';
		}
		else
		{
			echo '0';
		}
	}

	public function deleteDepartment() {
		if (!$this->session->userdata('del')) {
			redirect('welcome');
		}
		$id = trim($this->input->post('id', TRUE)); 

		if($this->modelo->deleteDepartments($id)) {
			echo '1';
		}
		else {
			echo $this->db->error();
		}
	}
}