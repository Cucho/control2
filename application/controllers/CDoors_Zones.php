<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CDoors_Zones extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('mDoors_Zones', 'modelo');
		if (empty($this->session->userdata('options'))) {
			redirect('welcome');
		}
		else if (in_array('01020102', $this->session->userdata('options'))) {
			redirect('welcome');
		}
	}

	public function index() {
		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('zones/list_doors_zones');
	}
	//Datatable
	public function datatable() {
		$start = $this->input->post('start');
		$length = $this->input->post('length');
		$search = $this->input->post('search')['value'];
		$by = $this->input->post('order')['0']['column'];
		$order = $this->input->post('order')['0']['dir'];

		$result = $this->modelo->getDoors_Zones($start, $length, $search, $order, $by);

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
            "recordsTotal"    => intval($result['numDataTotal']),
            "recordsFiltered" => intval($result['numDataFilter']),
            "data"            => $result['data']
            );

        echo json_encode($json_data);
	}

	//Vistas
	public function add() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$doors = $this->modelo->getAllDoors();
		$zones = $this->modelo->getAllZones();

		$data = array(
			'doors' => $doors,
			'zones' => $zones
		);

		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('zones/add_door_zone', $data);
	}

	//Crud
	public function addDoor_Zone() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$door	= trim($this->input->post('door', TRUE));
		$zone	= trim($this->input->post('zone', TRUE));

		$data = array(
			'doors_id' => $door,
			'zones_id' 	=> $zone
		);

		if($this->modelo->addDoor_Zone($data))
			echo '1';
		else
			echo '0';
	}


	public function deleteDoor_Zone() {
		if (!$this->session->userdata('del')) {
			redirect('welcome');
		}
		$door = trim($this->input->post('door', TRUE)); 
		$zone = trim($this->input->post('zone', TRUE)); 

		if($this->modelo->deleteDoor_Zone($door, $zone))
			echo '1';
		else
			echo '0';
	}
}