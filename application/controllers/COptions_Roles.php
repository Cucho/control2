<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class COptions_Roles extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('mOptions_Roles', 'modelo');
		if (empty($this->session->userdata('options'))) {
			redirect('welcome');
		}
		else if (in_array('01050000', $this->session->userdata('options'))) {
			redirect('welcome');
		}
	}

	public function index() {
		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('options/list_options_roles');
	}

	//Datatable
	public function datatable() {
		$start = $this->input->post('start');
		$length = $this->input->post('length');
		$search = $this->input->post('search')['value'];
		$by = $this->input->post('order')['0']['column'];
		$order = $this->input->post('order')['0']['dir'];

		$result = $this->modelo->getOptions_Roles($start, $length, $search, $order, $by);

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
            "recordsTotal"    => intval($result['numDataTotal']),
            "recordsFiltered" => intval($result['numDataFilter']),
            "data"            => $result['data']
            );

        echo json_encode($json_data);
	}

	//Vistas
	public function add() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$options = $this->modelo->getAllOptions();
		$roles = $this->modelo->getAllRoles();
		$options_roles = $this->modelo->AllOptions_Roles();

		$data = array(
			'options' => $options,
			'roles' => $roles,
			'options_roles' => $options_roles
		);

		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('options/add_options_roles', $data);
	}

	//Crud
	public function addOption_Rol() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$option = $this->input->post('option', TRUE);
		$rol 	= trim($this->input->post('rol', TRUE));
		
		if($this->modelo->addOption_Rol($option, $rol)) {
			echo '1';
		}
		else {
			echo '0';
		}
	}

	public function deleteOption_Rol() {
		if (!$this->session->userdata('del')) {
			redirect('welcome');
		}
		$option = trim($this->input->post('option', TRUE)); 
		$rol = trim($this->input->post('rol', TRUE)); 

		if($this->modelo->deleteOption_Rol($option, $rol)) {
			echo '1';
		}
		else {
			echo '0';
		}
	}
}