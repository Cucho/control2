<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CMeasures extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('mMeasures', 'modelo');
		if (empty($this->session->userdata('options'))) {
			redirect('welcome');
		}
		else if (in_array('09020000', $this->session->userdata('options'))) {
			redirect('welcome');
		}
	}

	public function index() {
		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('measures/index');
	}

	//Datatable
	public function datatable() {
		$start = $this->input->post('start');
		$length = $this->input->post('length');
		$search = $this->input->post('search')['value'];
		$by = $this->input->post('order')['0']['column'];
		$order = $this->input->post('order')['0']['dir'];

		$result = $this->modelo->getMeasures($start, $length, $search, $order, $by);

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
            "recordsTotal"    => intval($result['numDataTotal']),
            "recordsFiltered" => intval($result['numDataFilter']),
            "data"            => $result['data']
            );

        echo json_encode($json_data);
	}

	//Vistas
	public function add() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('measures/add');
	}

	public function edit() {
		if (!$this->session->userdata('edit')) {
			redirect('welcome');
		}
		$id = trim($this->input->get('id', TRUE));

		$measures = $this->modelo->getMeasure($id);
		
		$data = array('measures' => $measures);

		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('measures/edit', $data);
	}

	public function view() {
		$id = trim($this->input->get('id', TRUE));

		$measures = $this->modelo->getMeasure($id);

		$data = array('measures' => $measures);

		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('measures/view', $data);
	}

	//Crud
	public function addMeasure() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$measure 		= 	trim($this->input->post('measure', TRUE));
		$acronimo 		= 	trim($this->input->post('acronimo', TRUE));
		
		$date_time = date('Y-m-d H:i:s');

		$data = array(
			'measure' 	=> $measure,
			'acronimo'	=> $acronimo,
			'created' 	=> $date_time,
			'modified' 	=> $date_time
		);

		if($this->modelo->addMeasure($data)) {
			echo '1';
		}
		else {
			echo '0';
		}
	}

	public function editMeasure() {
		if (!$this->session->userdata('edit')) {
			redirect('welcome');
		}
		$id = trim($this->input->post('id', TRUE));
		$measure 		= 	trim($this->input->post('measure', TRUE));
		$acronimo 		= 	trim($this->input->post('acronimo', TRUE));
		
		$date_time = date('Y-m-d H:i:s');

		$data = array(
			'measure' 		=> $measure,
			'acronimo'	=> $acronimo,
			'modified' 	=> $date_time
		);

		if($this->modelo->editMeasure($data, $id))
		{
			echo '1';
		}
		else
		{
			echo '0';
		}
	}

	public function deleteMeasure() {
		if (!$this->session->userdata('del')) {
			redirect('welcome');
		}
		$id = trim($this->input->post('id', TRUE)); 

		if($this->modelo->deleteMeasure($id)) {
			echo '1';
		}
		else {
			echo $this->db->error();
		}
	}
}