<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CVehicles_Drivers extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('mVehicles_Drivers', 'modelo');
		if (empty($this->session->userdata('options'))) {
			redirect('welcome');
		}
		else if (in_array('10040000', $this->session->userdata('options'))) {
			redirect('welcome');
		}
	}

	public function index() {
		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('vehicles/list_vehicles_drivers');
	}
	//Datatable
	public function datatable() {
		$start = $this->input->post('start');
		$length = $this->input->post('length');
		$search = $this->input->post('search')['value'];
		$by = $this->input->post('order')['0']['column'];
		$order = $this->input->post('order')['0']['dir'];

		$result = $this->modelo->getVehicles_Drivers($start, $length, $search, $order, $by);

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
            "recordsTotal"    => intval($result['numDataTotal']),
            "recordsFiltered" => intval($result['numDataFilter']),
            "data"            => $result['data']
            );

        echo json_encode($json_data);
	}

	//Vistas
	public function add() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$driver = $this->modelo->getAllPeople();
		$vehicle = $this->modelo->getAllVehicles();

		$data = array(
			'driver' => $driver,
			'vehicle' => $vehicle
		);

		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('vehicles/add_vehicles_drivers', $data);
	}

	//Crud
	public function addVehicle_Driver() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$vehicle 	= 	trim($this->input->post('vehicle', TRUE));
		$driver 	= 	trim($this->input->post('driver', TRUE));
		
		$data = array(
			'vehicles_id' 	=> $vehicle,
			'people_id' 	=> $driver
		);

		if($this->modelo->addVehicle_Driver($data))
			echo '1';
		else
			echo '0';
	}

	public function deletecVehicle_Driver() {
		if (!$this->session->userdata('del')) {
			redirect('welcome');
		}
		$vehicles_id = trim($this->input->post('vehicles_id', TRUE));
		$people_id = trim($this->input->post('people_id', TRUE));

		if($this->modelo->deletecVehicle_Driver($vehicles_id, $people_id))
			echo '1';
		else
			echo '0';
	}
}