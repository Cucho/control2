<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CReasons_Visit extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('mReasons_Visit', 'modelo');
		if (empty($this->session->userdata('options'))) {
			redirect('welcome');
		}
		else if (in_array('05010100', $this->session->userdata('options'))) {
			redirect('welcome');
		}
	}

	public function index() {
		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('reasons_visit/index');
	}

	//Datatable
	public function datatable() {
		$start = $this->input->post('start');
		$length = $this->input->post('length');
		$search = $this->input->post('search')['value'];
		$by = $this->input->post('order')['0']['column'];
		$order = $this->input->post('order')['0']['dir'];

		$result = $this->modelo->getReasons_Visit($start, $length, $search, $order, $by);

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
            "recordsTotal"    => intval($result['numDataTotal']),
            "recordsFiltered" => intval($result['numDataFilter']),
            "data"            => $result['data']
            );

        echo json_encode($json_data);
	}

	//Vistas
	public function add() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('reasons_visit/add');
	}

	public function edit() {
		if (!$this->session->userdata('edit')) {
			redirect('welcome');
		}
		$id = trim($this->input->get('id', TRUE));

		$reasons_visit = $this->modelo->getReason_Visit($id);
		
		$data = array('reasons_visit' => $reasons_visit);

		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('reasons_visit/edit', $data);
	}

	public function view() {
		$id = trim($this->input->get('id', TRUE));

		$reasons_visit = $this->modelo->getReason_Visit($id);

		$data = array('reasons_visit' => $reasons_visit);

		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('reasons_visit/view', $data);
	}

	//Crud
	public function addReason_Visit() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$reason 		= 	trim($this->input->post('reason', TRUE));
		
		$date_time = date('Y-m-d H:i:s');

		$data = array(
			'reason' 	=> $reason,
			'created' 		=> $date_time,
			'modified' 		=> $date_time
		);

		if($this->modelo->addReasons_Visit($data)) {
			echo '1';
		}
		else {
			echo '0';
		}
	}

	public function editReason_Visit() {
		if (!$this->session->userdata('edit')) {
			redirect('welcome');
		}
		$id 	= trim($this->input->post('id', TRUE));
		$reason = 	trim($this->input->post('reason', TRUE));
		
		$date_time = date('Y-m-d H:i:s');

		$data = array(
			'reason' 	=> $reason,
			'modified' 	=> $date_time
		);

		if($this->modelo->editReasons_Visit($data, $id))
		{
			echo '1';
		}
		else
		{
			echo '0';
		}
	}

	public function delReasons_Visit() {
		if (!$this->session->userdata('del')) {
			redirect('welcome');
		}
		$id = trim($this->input->post('id', TRUE)); 

		if($this->modelo->deleteReasons_Visit($id)) {
			echo '1';
		}
		else {
			echo $this->db->error();
		}
	}
}