<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CLogbook extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('MLogbook', 'modelo');
		if (empty($this->session->userdata('options'))) {
			redirect('welcome');
		}
		else if (in_array('11000000', $this->session->userdata('options'))) {
			redirect('welcome');
		}
	}

	public function index() {
		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('logbook/index');
	}

	//Datatable
	public function datatable() {
		$start = $this->input->post('start');
		$length = $this->input->post('length');
		$search = $this->input->post('search')['value'];
		$by = $this->input->post('order')['0']['column'];
		$order = $this->input->post('order')['0']['dir'];

		$result = $this->modelo->getLogbook($start, $length, $search, $order, $by);

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
            "recordsTotal"    => intval($result['numDataTotal']),
            "recordsFiltered" => intval($result['numDataFilter']),
            "data"            => $result['data']
            );

        echo json_encode($json_data);
	}

	//Vistas
	public function add() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$this->load->view('header');
		$this->load->view('aside');

		$this->load->view('logbook/add');
	}

	public function edit() {
		if (!$this->session->userdata('edit')) {
			redirect('welcome');
		}
		$id = trim($this->input->get('id', TRUE));

		$logbook = $this->modelo->getLogbook_($id);
		$documents = $this->listDocument($id);

		$data = array(
			'logbook' => $logbook,
			'documents' => $documents
		);

		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('logbook/edit', $data);
	}

	public function view() {
		$id = trim($this->input->get('id', TRUE));

		$logbook = $this->modelo->getLogbook_($id);
		$documents = $this->listDocument($id);

		$data = array(
			'logbook' => $logbook,
			'documents' => $documents
		);

		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('logbook/view', $data);
	}

	public function listDocument($id){
		$ruta = './assets/logbook/'.$id.'/';
		$list = [];
		// abrir un directorio y listarlo recursivo
		if (is_dir($ruta)){
			if ($dh = opendir($ruta)){
		 		//echo "<br /><strong>$ruta</strong>"; // Aqui se imprime el nombre de la carpeta o directorio
		 		while (($file = readdir($dh)) !== false){
		 			//if (is_dir($ruta . $file) && $file!="." && $file!="..") // Si se desea mostrar solo directorios
		 			if ($file != "." && $file != ".."){ // Si se desea mostrar directorios y archivos
		 				//solo si el archivo es un directorio, distinto que "." y ".."
		 				//echo "<br />$file"; // Aqui se imprime el nombre del Archivo con su ruta relativa
		 				array_push($list, base_url().str_replace('./','',$ruta).$file);
		 				//listDocument($ruta . $file . DIRECTORY_SEPARATOR); // Ahora volvemos a llamar la función
		 				//listDocument($id);
		 			}
		 		}
		 		closedir($dh);
		 	}
		}
		return $list;
	}

	//Crud
	public function addLogbook() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}

		$date_time 	= 	date('Y-m-d H:i:s');
		$title 		= 	trim($this->input->post('title', TRUE));
		$body 		= 	trim($this->input->post('body', TRUE));
		$type 		= 	trim($this->input->post('type', TRUE));

		$data = array(
			'title' => $title,
			'body' => $body,
			'people_id' => $this->session->userdata('people_id'),
			'type_logbook_id' => $type,
			'edited' => 1,
			'created' => $date_time,
			'modified' => $date_time
		);

		echo $this->modelo->addLogbook($data);
		
	}

	public function addDocuments() {
		
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		
		$id_logbook = trim($this->input->get('id_logbook', TRUE));
		
		$ruta = './assets/logbook/'.$id_logbook;
        if (!file_exists($ruta)) {
            mkdir($ruta, 0777, true);
        }

        $ruta .= '/';
        $mensaje = '';

        foreach($_FILES as $key) {
	        if($key['error'] == UPLOAD_ERR_OK) {
	            $nombreArchivo = $key['name'];
	            $temporal = $key['tmp_name'];
	            $destino = $ruta.$nombreArchivo;

	            move_uploaded_file($temporal, $destino);
	            $mensaje = '1';
	        }
	        else if($key['error'] == '') {
	            $mensaje = '1';
	        }
	        else if($key['error'] != '') {
	            $mensaje = '0';
	        }
	    }
	    
	    echo $mensaje;
	}

	public function deleteFile(){
		if (!$this->session->userdata('del')) {
			redirect('welcome');
		}
		
		$file 		= $this->input->post('file');
		$id 		= $this->input->post('id');

		$ruta = './assets/logbook/'.$id.'/'.$file;
		
		if(unlink($ruta)) 
			echo '1';
		else
			echo '0';
	}

	public function editLogbook() {
		if (!$this->session->userdata('edit')) {
			redirect('welcome');
		}
		$date_time 	= 	date('Y-m-d H:i:s');
		$id 		= 	trim($this->input->post('id', TRUE));
		$title 		= 	trim($this->input->post('title', TRUE));
		$body 		= 	trim($this->input->post('body', TRUE));
		$type 		= 	trim($this->input->post('type', TRUE));

		$data = array(
			'title' => $title,
			'body' => $body,
			'people_id' => $this->session->userdata('people_id'),
			'type_logbook_id' => $type,
			'modified' => $date_time
		);

		if($this->modelo->editLogbook($data, $id)) 
			echo '1';
		else 
			echo '0';
		
	}

}
