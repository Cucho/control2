<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CSpecial_Schedule extends CI_Controller {

	//id persona que genera el registro de horairo especial, puede ser guardia, recepcionista, etc...
	//despues llenar con variable de sesion
	//private $created_by = 2;
	private $created_by;

	function __construct() {
		parent::__construct();
		$this->load->model('mSpecial_Schedule', 'modelo');
		$this->created_by = $this->session->userdata('people_id');
		if (empty($this->session->userdata('options'))) {
			redirect('welcome');
		}
		else if (in_array('08030000', $this->session->userdata('options'))) {
			redirect('welcome');
		}
	}

	public function index() {
		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('special_schedule/index');
	}

	//Datatable
	public function datatable() {
		$start = $this->input->post('start');
		$length = $this->input->post('length');
		$search = $this->input->post('search')['value'];
		$by = $this->input->post('order')['0']['column'];
		$order = $this->input->post('order')['0']['dir'];

		$result = $this->modelo->getSpecial_Schedule($start, $length, $search, $order, $by);

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
            "recordsTotal"    => intval($result['numDataTotal']),
            "recordsFiltered" => intval($result['numDataFilter']),
            "data"            => $result['data']
            );

        echo json_encode($json_data);
	}

	//Vistas
	public function add() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$this->load->view('header');
		$this->load->view('aside');
		$people = $this->modelo->getAllPeople();
		$doors = $this->modelo->getAllDoors();
		$data = array('people' => $people, 'doors' => $doors);
		$this->load->view('special_schedule/add', $data);
	}

	public function edit() {
		if (!$this->session->userdata('edit')) {
			redirect('welcome');
		}
		$id = trim($this->input->get('id', TRUE));

		$special_schedule = $this->modelo->getSpecial_ScheduleId($id);
		$people = $this->modelo->getAllPeople();
		$doors = $this->modelo->getAllDoors();

		$data = array(
			'special_schedule' 	=> $special_schedule,
			'people'			=> $people,
			'doors' 			=> $doors
		);

		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('special_schedule/edit', $data);
	}

	public function view() {
		$id = trim($this->input->get('id', TRUE));

		$special_schedule = $this->modelo->getSpecial_ScheduleId($id);
		$people = $this->modelo->getAllPeople();
		$doors = $this->modelo->getAllDoors();

		$data = array(
			'special_schedule' 	=> $special_schedule,
			'people'			=> $people,
			'doors' 			=> $doors
		);

		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('special_schedule/view', $data);
	}

	//Crud
	public function addSpecial_Schedule() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$date_time_now = date('Y-m-d H:i:s');

		$people_id 	= 	trim($this->input->post('people_id', TRUE));
		$doors_id 	= 	trim($this->input->post('doors_id', TRUE));
		$date_init 	= 	trim($this->input->post('date_init', TRUE));
		$date_end 	= 	trim($this->input->post('date_end', TRUE));
		$reason 	= 	trim($this->input->post('reason', TRUE));
		
		$data = array(
			'people_id' 	=> $people_id,
			'doors_id' 		=> $doors_id,
			'created_by'	=> $this->created_by,
			'date_init' 	=> $date_init,
			'date_end' 		=> $date_end,
			'reason'		=> $reason,
			'created'		=> $date_time_now
		);

		if($this->modelo->addSpecial_Schedule($data)) {
			echo '1';
		}
		else {
			echo '0';
		}
	}

	public function editSpecial_Schedule() {
		if (!$this->session->userdata('edit')) {
			redirect('welcome');
		}
		$date_time_now = date('Y-m-d H:i:s');
		$id = trim($this->input->post('id', TRUE));

		$people_id 	= 	trim($this->input->post('people_id', TRUE));
		$doors_id 	= 	trim($this->input->post('doors_id', TRUE));
		$date_init 	= 	trim($this->input->post('date_init', TRUE));
		$date_end 	= 	trim($this->input->post('date_end', TRUE));
		$reason 	= 	trim($this->input->post('reason', TRUE));
		
		$data = array(
			'people_id' 	=> $people_id,
			'doors_id' 		=> $doors_id,
			'created_by'	=> $this->created_by,
			'date_init' 	=> $date_init,
			'date_end' 		=> $date_end,
			'reason'		=> $reason,
			'created'		=> $date_time_now
		);

		if($this->modelo->editSpecial_Schedule($data, $id))
		{
			echo '1';
		}
		else
		{
			echo '0';
		}
	}

	public function delSpecial_Schedule() {
		if (!$this->session->userdata('del')) {
			redirect('welcome');
		}
		$id = trim($this->input->post('id', TRUE)); 

		if($this->modelo->deleteSpecial_Schedule($id)) {
			echo '1';
		}
		else {
			echo '0';
		}
	}
}