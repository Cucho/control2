<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CPeople extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('mPeople', 'modelo');
		if (empty($this->session->userdata('options'))) {
			redirect('welcome');
		}
		else if (in_array('08010000', $this->session->userdata('options'))) {
			redirect('welcome');
		}
	}

	public function index() {
		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('people/index');
	}

	//Datatable
	public function datatable() {
		$start = $this->input->post('start');
		$length = $this->input->post('length');
		$search = $this->input->post('search')['value'];
		$by = $this->input->post('order')['0']['column'];
		$order = $this->input->post('order')['0']['dir'];

		$result = $this->modelo->getPeoples($start, $length, $search, $order, $by);

		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
            "recordsTotal"    => intval($result['numDataTotal']),
            "recordsFiltered" => intval($result['numDataFilter']),
            "data"            => $result['data']
            );

        echo json_encode($json_data);
	}

	//Vistas
	public function add() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$companies = $this->modelo->getAllCompanies();
		$people_profile = $this->modelo->getAllPeople_Profile();
		$departments = $this->modelo->getAllDepartments();
		$states = $this->modelo->getAllStates();

		$data = array(
			'companies' => $companies,
			'people_profile' => $people_profile,
			'departments' => $departments,
			'states' => $states
		);

		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('people/add', $data);
	}

	public function edit() {
		if (!$this->session->userdata('edit')) {
			redirect('welcome');
		}
		$id = trim($this->input->get('id', TRUE));

		$people = $this->modelo->getPeople($id);
		$companies = $this->modelo->getAllCompanies();
		$people_profile = $this->modelo->getAllPeople_Profile();

		$data = array(
			'companies' => $companies,
			'people' => $people,
			'people_profile' => $people_profile
		);

		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('people/edit', $data);
	}

	public function view() {
		$id = trim($this->input->get('id', TRUE));

		$people = $this->modelo->getPeople($id);
		$companies = $this->modelo->getAllCompanies();
		$people_profile = $this->modelo->getAllPeople_Profile();

		$data = array(
			'companies' => $companies,
			'people' => $people,
			'people_profile' => $people_profile
		);

		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('people/view', $data);
	}

	public function masiva(){
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$companies = $this->modelo->getAllCompanies();
		$people_profile = $this->modelo->getAllPeople_Profile();
		$departments = $this->modelo->getAllDepartments();
		$states = $this->modelo->getAllStates();

		$data = array(
			'companies' => $companies,
			'people_profile' => $people_profile,
			'departments' => $departments,
			'states' => $states
		);

		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('people/masiva', $data);
	}

	//Crud
	public function addPeople() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$rut 				= 	trim($this->input->post('rut', TRUE));
		$dv 				= 	trim($this->input->post('digit', TRUE));
		$name 				= 	trim($this->input->post('name', TRUE));
		$last_name 			= 	trim($this->input->post('last_name', TRUE));
		$address 			= 	trim($this->input->post('address', TRUE));
		$email 				= 	trim($this->input->post('email', TRUE));
		$phone 				= 	trim($this->input->post('phone', TRUE));
		$allow_all 			= 	trim($this->input->post('allow_all', TRUE));
		$is_visited 		= 	trim($this->input->post('is_visited', TRUE));
		$internal 			= 	trim($this->input->post('internal', TRUE));
		$people_profiles_id = 	trim($this->input->post('people_profiles_id', TRUE));
		$companies_id 		= 	trim($this->input->post('companies_id', TRUE));
		$departments_id 	= 	trim($this->input->post('departments_id', TRUE));
		$nfc_code 			= 	trim($this->input->post('nfc_code', TRUE));
		$states_id			=	trim($this->input->post('states_id', TRUE));

		if(empty($nfc_code))
			$nfc_code = 0;

		$date_time = date('Y-m-d H:i:s');

		$data = array(
			'rut' 				=> $rut,
			'digit' 			=> $dv,
			'name' 				=> $name,
			'last_name' 		=> $last_name,
			'address' 			=> $address,
			'email' 			=> $email,
			'phone' 			=> $phone,
			'allow_all' 		=> $allow_all,
			'is_visited' 		=> $is_visited,
			'internal' 			=> $internal,
			'people_profiles_id'=> $people_profiles_id,
			'companies_id' 		=> $companies_id,
			'departments_id' 	=> $departments_id,
			'nfc_code'			=> $nfc_code,
			'states_id'			=> $states_id,
			'created'			=> $date_time,
			'modified'			=> $date_time
		);

		if($this->modelo->addPeople($data))
			echo '1';
		else
			echo '0';
	}

	public function editPeople() {
		if (!$this->session->userdata('edit')) {
			redirect('welcome');
		}
		$id = trim($this->input->post('id', TRUE));
		$rut 				= 	trim($this->input->post('rut', TRUE));
		$dv 				= 	trim($this->input->post('digit', TRUE));
		$name 				= 	trim($this->input->post('name', TRUE));
		$last_name 			= 	trim($this->input->post('last_name', TRUE));
		$address 			= 	trim($this->input->post('address', TRUE));
		$email 				= 	trim($this->input->post('email', TRUE));
		$phone 				= 	trim($this->input->post('phone', TRUE));
		$allow_all 			= 	trim($this->input->post('allow_all', TRUE));
		$is_visited 		= 	trim($this->input->post('is_visited', TRUE));
		$internal 			= 	trim($this->input->post('internal', TRUE));
		$people_profiles_id = 	trim($this->input->post('people_profiles_id', TRUE));
		$companies_id 		= 	trim($this->input->post('companies_id', TRUE));
		$departments_id 	= 	trim($this->input->post('departments_id', TRUE));
		$nfc_code 			= 	trim($this->input->post('nfc_code', TRUE));
		$states_id			=	trim($this->input->post('states_id', TRUE));

		if(empty($nfc_code))
			$nfc_code = 0;

		$date_time = date('Y-m-d H:i:s');

		$data = array(
			'rut' 				=> $rut,
			'digit' 			=> $dv,
			'name' 				=> $name,
			'last_name' 		=> $last_name,
			'address' 			=> $address,
			'email' 			=> $email,
			'phone' 			=> $phone,
			'allow_all' 		=> $allow_all,
			'is_visited' 		=> $is_visited,
			'internal' 			=> $internal,
			'people_profiles_id'=> $people_profiles_id,
			'companies_id' 		=> $companies_id,
			'departments_id' 	=> $departments_id,
			'nfc_code'			=> $nfc_code,
			'states_id'			=> $states_id,
			'created'			=> $date_time,
			'modified'			=> $date_time
		);

		if($this->modelo->editPeople($data, $id)) {
			echo '1';
		}
		else {
			echo '0';
		}
	}

	public function deletePeople() {
		if (!$this->session->userdata('del')) {
			redirect('welcome');
		}
		$id = trim($this->input->post('id', TRUE));

		if($this->modelo->deletePeople($id))
			echo '1';
		else
			echo '0';
	}

	public function excelfile() {
		if (!$this->session->userdata('save')) {
			redirect('welcome');
		}
		$internal 			= trim($this->input->get('internal',TRUE));
		$people_profiles_id = trim($this->input->get('people_profiles_id',TRUE));
		$companies_id 		= trim($this->input->get('companies_id',TRUE));

		$this->load->library('excel');

		$ruta = './assets/excel/';
        if (!file_exists($ruta)) {
            mkdir($ruta, 0777, true);
        }

        $mensaje = '';
        $date_time = date('Y-m-d H:i:s');

        foreach($_FILES as $key) {
	        if($key['error'] == UPLOAD_ERR_OK) {
	            $nombreArchivo = $key['name'];
	            $temporal = $key['tmp_name'];
	            $destino = $ruta.$nombreArchivo;

	            move_uploaded_file($temporal, $destino);
	            
	            $objReader = PHPExcel_IOFactory::createReader('Excel2007');
	            $objReader->setReadDataOnly(true);

	            $objPHPExcel = $objReader->load($destino);
	            //$objWhorksheet = $objPHPExcel->getActiveSheet();

	            $filas = $objPHPExcel->getActiveSheet()->getHighestRow();

	            $letras = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L');

	            for ($i=2; $i <= $filas ; $i++) { 
	            	$data = array(
						'rut' 				=> ($objPHPExcel->getActiveSheet()->getCell($letras[0].$i)->getValue() != '' ? $objPHPExcel->getActiveSheet()->getCell($letras[0].$i)->getValue() : ''),
						'digit' 			=> ($objPHPExcel->getActiveSheet()->getCell($letras[1].$i)->getValue() != '' ? $objPHPExcel->getActiveSheet()->getCell($letras[1].$i)->getValue() : ''),
						'name' 				=> ($objPHPExcel->getActiveSheet()->getCell($letras[2].$i)->getValue() != '' ? $objPHPExcel->getActiveSheet()->getCell($letras[2].$i)->getValue() : ''),
						'last_name' 		=> ($objPHPExcel->getActiveSheet()->getCell($letras[3].$i)->getValue() != '' ? $objPHPExcel->getActiveSheet()->getCell($letras[3].$i)->getValue() : ''),
						'address' 			=> ($objPHPExcel->getActiveSheet()->getCell($letras[4].$i)->getValue() != '' ? $objPHPExcel->getActiveSheet()->getCell($letras[4].$i)->getValue() : ''),
						'email' 			=> ($objPHPExcel->getActiveSheet()->getCell($letras[5].$i)->getValue() != '' ? $objPHPExcel->getActiveSheet()->getCell($letras[5].$i)->getValue() : ''),
						'phone' 			=> ($objPHPExcel->getActiveSheet()->getCell($letras[6].$i)->getValue() != '' ? $objPHPExcel->getActiveSheet()->getCell($letras[6].$i)->getValue() : ''),
						'allow_all' 		=> ($objPHPExcel->getActiveSheet()->getCell($letras[7].$i)->getValue() != '' ? $objPHPExcel->getActiveSheet()->getCell($letras[7].$i)->getValue() : ''),
						'is_visited' 		=> ($objPHPExcel->getActiveSheet()->getCell($letras[8].$i)->getValue() != '' ? $objPHPExcel->getActiveSheet()->getCell($letras[8].$i)->getValue() : ''),
						'internal' 			=> $internal,
						'people_profiles_id'=> $people_profiles_id,
						'companies_id' 		=> $companies_id,
						'departments_id' 	=> ($objPHPExcel->getActiveSheet()->getCell($letras[9].$i)->getValue() != '' ? $objPHPExcel->getActiveSheet()->getCell($letras[9].$i)->getValue() : ''),
						'nfc_code'			=> ($objPHPExcel->getActiveSheet()->getCell($letras[10].$i)->getValue() != '' ? $objPHPExcel->getActiveSheet()->getCell($letras[10].$i)->getValue() : ''),
						'states_id'			=> ($objPHPExcel->getActiveSheet()->getCell($letras[11].$i)->getValue() != '' ? $objPHPExcel->getActiveSheet()->getCell($letras[11].$i)->getValue() : ''),
						'created'			=> $date_time,
						'modified'			=> $date_time
					);
					if($this->modelo->addPeople($data)){
						$mensaje = '1';
					}
					else{
						$mensaje = '0';
					}
	            }

				if (unlink($destino)) {
					$mensaje = '1';
				}
				else {
					$mensaje = '0';
				}

    		}
	        else if($key['error'] == '') {
	            $mensaje = '1';
	        }
	        else if($key['error'] != '') {
	            $mensaje = '0';
	        }
	    }
	    
	    echo $mensaje;
	}

}