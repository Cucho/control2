-- phpMyAdmin SQL Dump
-- version 4.7.5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:8889
-- Tiempo de generación: 11-06-2018 a las 10:56:21
-- Versión del servidor: 5.6.38
-- Versión de PHP: 7.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de datos: `access_control_3`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `access_people`
--

CREATE TABLE `access_people` (
  `id` int(11) NOT NULL,
  `entry` tinyint(1) NOT NULL,
  `exit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `hours` tinyint(2) DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `people_id` int(11) NOT NULL,
  `access_state_id` int(11) NOT NULL,
  `main_access_id` int(11) DEFAULT NULL,
  `approved_by` int(11) NOT NULL,
  `observation` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `vehicles_id` int(11) DEFAULT '0',
  `control_init` tinyint(4) DEFAULT '0',
  `control_end` tinyint(4) DEFAULT '0',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `access_people`
--

INSERT INTO `access_people` (`id`, `entry`, `exit_time`, `hours`, `end_time`, `people_id`, `access_state_id`, `main_access_id`, `approved_by`, `observation`, `vehicles_id`, `control_init`, `control_end`, `created`, `modified`) VALUES
(36, 1, '2018-04-29 09:47:22', 1, '2018-04-29 10:41:00', 43, 2, 2, 3, '<p>ingreso</p>', 15, 1, 1, '2018-04-29 09:42:13', '2018-04-29 09:42:13'),
(35, 1, '2018-04-28 14:43:45', 1, '2018-04-28 15:43:00', 44, 2, 2, 3, '', 0, 0, 0, '2018-04-28 14:43:16', '2018-04-28 14:43:16'),
(34, 1, '2018-04-28 14:42:06', 1, '2018-04-28 15:41:00', 44, 2, 2, 3, '', 0, 0, 0, '2018-04-28 14:41:37', '2018-04-28 14:41:37'),
(33, 1, '2018-04-28 14:34:44', 1, '2018-04-28 15:34:00', 43, 2, 2, 3, '', 0, 0, 0, '2018-04-28 14:34:16', '2018-04-28 14:34:16'),
(32, 1, '2018-04-28 14:33:19', 1, '2018-04-28 15:32:00', 43, 2, 2, 3, '<p>plpk</p>', 0, 0, 0, '2018-04-28 14:32:30', '2018-04-28 14:32:30'),
(31, 1, '2018-04-28 12:36:12', 1, '2018-04-26 18:52:00', 44, 2, 2, 3, 'saliendo fuera de plazo', 0, 0, 0, '2018-04-26 17:52:59', '2018-04-26 17:52:59'),
(30, 1, '2018-04-28 12:35:59', 1, '2018-04-25 19:23:00', 43, 2, 2, 3, 'saliendo fuera de plazo', 0, 0, 0, '2018-04-25 18:23:13', '2018-04-25 18:23:13'),
(28, 1, '2018-04-25 18:07:08', 1, '2018-04-25 18:03:00', 42, 2, 2, 3, '<p>entrada hasta 18:03</p>saliendo con auto', 15, 1, 1, '2018-04-25 17:04:53', '2018-04-25 17:04:53'),
(29, 1, '2018-04-25 18:22:29', 1, '2018-04-25 18:03:00', 43, 2, 2, 3, '<p>entrada hasta 18:03</p>salida acompañante en auto', 0, 0, 0, '2018-04-25 17:04:53', '2018-04-25 17:04:53'),
(37, 1, '2018-04-29 16:08:47', 1, '2018-04-29 10:41:00', 42, 2, 2, 3, '<p>ingreso</p>salida', 0, 0, 0, '2018-04-29 09:42:13', '2018-04-29 09:42:13'),
(38, 1, '2018-04-29 16:12:31', 1, '2018-04-29 17:08:00', 44, 2, 2, 3, '<p>observacion base entrada</p>', 15, 1, 1, '2018-04-29 16:10:33', '2018-04-29 16:10:33'),
(39, 1, '2018-05-04 20:54:38', 1, '2018-05-04 21:48:00', 44, 2, 2, 3, '<p>ascd</p>', 0, 0, 0, '2018-05-04 20:49:15', '2018-05-04 20:49:15'),
(40, 1, '2018-05-04 22:11:03', 1, '2018-05-04 21:56:00', 44, 2, 2, 3, '<p>asdc</p>prueba borrador', 15, 0, 1, '2018-05-04 20:57:02', '2018-05-04 20:57:02'),
(41, 1, '2018-05-04 22:27:11', 1, '2018-05-04 23:22:00', 44, 2, 2, 3, '', 15, 0, 1, '2018-05-04 22:23:17', '2018-05-04 22:23:17'),
(42, 1, '2018-05-04 22:32:22', 1, '2018-05-04 23:27:00', 44, 2, 2, 3, '<p>sd</p>', 15, 0, 1, '2018-05-04 22:28:12', '2018-05-04 22:28:12'),
(43, 1, '2018-05-23 18:31:22', 1, '2018-05-23 18:49:00', 44, 2, 2, 3, '<p>as</p>', 15, 1, 1, '2018-05-23 17:51:20', '2018-05-23 17:51:20'),
(44, 0, '0000-00-00 00:00:00', 1, '2018-05-23 18:49:00', 42, 2, 2, 3, '<p>as</p>', 0, 0, 0, '2018-05-23 17:51:20', '2018-05-23 17:51:20'),
(45, 1, '2018-05-23 19:35:58', 1, '2018-05-23 19:45:00', 44, 2, 2, 3, '<p>aw</p>', 15, 0, 1, '2018-05-23 18:46:24', '2018-05-23 18:46:24'),
(46, 1, '2018-05-23 19:47:36', 1, '2018-05-23 20:39:00', 44, 2, 2, 3, '<p>qa</p>', 15, 1, 1, '2018-05-23 19:40:45', '2018-05-23 19:40:45'),
(47, 1, '2018-05-24 23:33:48', 1, '2018-05-25 00:31:00', 44, 2, 2, 3, '<p>as</p>', 15, 0, 1, '2018-05-24 23:32:09', '2018-05-24 23:32:09'),
(48, 1, '2018-05-25 22:37:32', 1, '2018-05-25 23:35:00', 44, 2, 2, 3, '<p>qw</p>', 15, 0, 1, '2018-05-25 22:36:21', '2018-05-25 22:36:21'),
(49, 1, '2018-05-25 22:55:02', 1, '2018-05-25 23:52:00', 44, 2, 2, 3, '<p>observacion de ingreso</p>', 15, 1, 1, '2018-05-25 22:53:51', '2018-05-25 22:53:51'),
(52, 0, '0000-00-00 00:00:00', 1, '2018-06-08 01:20:00', 44, 2, 2, 3, '', 15, 1, 0, '2018-06-08 00:21:19', '2018-06-08 00:21:19');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `access_people_answers`
--

CREATE TABLE `access_people_answers` (
  `id` int(11) NOT NULL,
  `access_people_id` int(11) DEFAULT NULL,
  `forms_id` int(11) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `answer` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `control` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `access_people_answers`
--

INSERT INTO `access_people_answers` (`id`, `access_people_id`, `forms_id`, `order`, `answer`, `control`) VALUES
(84, 36, 32, 1, '1', 0),
(96, 41, 32, 2, '<p>wed</p>', 1),
(82, 36, 32, 3, '2', 0),
(81, 36, 32, 2, '<p>r2b</p>', 0),
(80, 36, 32, 1, 'r1b', 0),
(79, 36, 32, 1, '1', 0),
(95, 41, 32, 1, 'we', 1),
(77, 36, 32, 3, '3', 0),
(76, 36, 32, 2, '<p>r2</p>', 0),
(75, 36, 32, 1, 'r1', 0),
(74, 28, 32, 6, 'on', 0),
(73, 28, 32, 5, '2018-04-25', 0),
(72, 28, 32, 4, '1', 0),
(71, 28, 32, 3, '<p>q</p>', 0),
(70, 28, 32, 2, 'q', 0),
(69, 28, 32, 1, 'saliendo con auto', 0),
(67, 28, 32, 4, '2018-04-25', 0),
(68, 28, 32, 5, '1', 0),
(66, 28, 32, 3, '1', 0),
(65, 28, 32, 2, '<p>b</p>', 0),
(64, 28, 32, 1, 'a', 0),
(85, 38, 32, 1, 'respuesta corta pregunta 1', 0),
(86, 38, 32, 2, '<p>respuesta larga pregunta 2</p>', 0),
(87, 38, 32, 3, '10', 0),
(88, 38, 32, 4, '2018-04-29', 0),
(89, 38, 32, 5, '0', 0),
(90, 38, 32, 1, 'respuesta corta pregunta 1 salida', 1),
(91, 38, 32, 2, '<p>respuesta larga pregunta 2 salida</p>', 1),
(92, 38, 32, 3, '20', 1),
(93, 38, 32, 4, '2018-04-29', 1),
(94, 38, 32, 1, '1', 1),
(97, 41, 32, 3, '1', 1),
(98, 41, 32, 4, '2018-05-04', 1),
(99, 41, 32, 1, '1', 1),
(100, 41, 32, 1, 'we', 1),
(101, 41, 32, 2, '<p>wed</p>', 1),
(102, 41, 32, 3, '1', 1),
(103, 41, 32, 4, '2018-05-04', 1),
(104, 41, 32, 1, '1', 1),
(105, 41, 32, 1, 'we', 1),
(106, 41, 32, 2, '<p>wed</p>', 1),
(107, 41, 32, 3, '1', 1),
(108, 41, 32, 4, '2018-05-04', 1),
(109, 41, 32, 1, '1', 1),
(110, 42, 32, 1, 'as', 1),
(111, 42, 32, 2, '<p>ascx</p>', 1),
(112, 42, 32, 3, '1', 1),
(113, 42, 32, 4, '2018-05-04', 1),
(114, 42, 32, 1, '1', 1),
(115, 43, 32, 1, 'as', 0),
(116, 43, 32, 2, '<p>as</p>', 0),
(117, 43, 32, 3, '12', 0),
(118, 43, 32, 4, '2018-05-23', 0),
(119, 43, 32, 1, '1', 0),
(120, 43, 32, 1, '0', 1),
(121, 45, 32, 1, '12', 1),
(122, 45, 32, 2, '<p>12</p>', 1),
(123, 45, 32, 3, '12', 1),
(124, 45, 32, 4, '2018-05-23', 1),
(125, 45, 32, 1, '1', 1),
(126, 46, 32, 1, 'respuesta entrada 1', 0),
(127, 46, 32, 2, '<p>respuesta entrada 2</p>', 0),
(128, 46, 32, 3, '3', 0),
(129, 46, 32, 4, '2018-05-23', 0),
(130, 46, 32, 1, '1', 0),
(131, 46, 32, 1, 'respuesta salida 1', 1),
(132, 46, 32, 2, '<p>respuesta salida 2</p>', 1),
(133, 46, 32, 3, '3', 1),
(134, 46, 32, 4, '2018-05-23', 1),
(135, 46, 32, 1, '1', 1),
(136, 47, 32, 1, 'sal', 1),
(137, 47, 32, 2, '<p>sal</p>', 1),
(138, 47, 32, 3, '1', 1),
(139, 47, 32, 4, '2018-05-24', 1),
(140, 47, 32, 1, '1', 1),
(141, 48, 32, 1, 'r as 1', 1),
(142, 48, 32, 2, '<p>r as 2</p>', 1),
(143, 48, 32, 3, '3', 1),
(144, 48, 32, 4, '2018-05-25', 1),
(145, 48, 32, 1, '1', 1),
(146, 49, 32, 1, 'q1', 0),
(147, 49, 32, 2, '<p>q2</p>', 0),
(148, 49, 32, 3, '3', 0),
(149, 49, 32, 4, '2018-05-25', 0),
(150, 49, 32, 1, '1', 0),
(151, 49, 32, 1, 'o1', 1),
(152, 49, 32, 2, '<p>o2</p>', 1),
(153, 49, 32, 3, '3', 1),
(154, 49, 32, 4, '2018-05-25', 1),
(155, 49, 32, 1, '1', 1),
(156, 50, 32, 1, 'r1', 0),
(157, 50, 32, 2, '<p>r2</p>', 0),
(158, 50, 32, 3, '3', 0),
(159, 50, 32, 4, '2018-06-07', 0),
(160, 50, 32, 1, '1', 0),
(161, 51, 32, 1, 'd1', 0),
(162, 51, 32, 2, '<p>d2</p>', 0),
(163, 51, 32, 3, '3', 0),
(164, 51, 32, 4, '2018-06-08', 0),
(165, 51, 32, 4, '1', 0),
(166, 52, 32, 1, '1', 0),
(167, 52, 32, 2, '<p>2</p>', 0),
(168, 52, 32, 3, '3', 0),
(169, 52, 32, 4, '2018-06-08', 0),
(170, 52, 32, 5, '1', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `access_people_areas`
--

CREATE TABLE `access_people_areas` (
  `access_people_id` int(11) NOT NULL,
  `areas_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `access_people_areas`
--

INSERT INTO `access_people_areas` (`access_people_id`, `areas_id`) VALUES
(28, 1),
(28, 9),
(28, 10),
(29, 1),
(29, 9),
(29, 10),
(30, 1),
(30, 9),
(30, 10),
(31, 1),
(31, 9),
(31, 10),
(32, 1),
(32, 9),
(32, 10),
(33, 1),
(33, 9),
(33, 10),
(34, 1),
(34, 9),
(34, 10),
(35, 1),
(35, 9),
(35, 10),
(36, 1),
(36, 9),
(36, 10),
(37, 1),
(37, 9),
(37, 10),
(38, 1),
(38, 9),
(38, 10),
(39, 1),
(39, 9),
(39, 10),
(40, 1),
(40, 9),
(40, 10),
(41, 1),
(41, 9),
(41, 10),
(42, 1),
(42, 9),
(42, 10),
(43, 1),
(43, 9),
(43, 10),
(44, 1),
(44, 9),
(44, 10),
(45, 1),
(45, 9),
(45, 10),
(46, 1),
(46, 9),
(46, 10),
(47, 1),
(47, 9),
(47, 10),
(48, 1),
(48, 9),
(48, 10),
(49, 1),
(49, 9),
(49, 10),
(50, 1),
(50, 9),
(50, 10),
(51, 1),
(51, 9),
(51, 10),
(52, 1),
(52, 9),
(52, 10);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `access_people_department`
--

CREATE TABLE `access_people_department` (
  `access_people_id` int(11) NOT NULL,
  `departments_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `access_people_department`
--

INSERT INTO `access_people_department` (`access_people_id`, `departments_id`) VALUES
(28, 3),
(29, 3),
(30, 3),
(31, 3),
(32, 3),
(33, 3),
(34, 3),
(35, 3),
(36, 3),
(37, 3),
(38, 3),
(39, 3),
(40, 3),
(41, 3),
(42, 3),
(43, 3),
(44, 3),
(45, 3),
(46, 3),
(47, 3),
(48, 3),
(49, 3),
(50, 3),
(51, 3),
(52, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `access_people_forms`
--

CREATE TABLE `access_people_forms` (
  `id` int(11) NOT NULL,
  `access_people_id` int(11) DEFAULT NULL,
  `forms_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `observation` text COLLATE utf8mb4_unicode_ci,
  `control` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `access_people_forms`
--

INSERT INTO `access_people_forms` (`id`, `access_people_id`, `forms_id`, `title`, `observation`, `control`) VALUES
(57, 34, 0, NULL, NULL, 0),
(58, 35, 0, '', '', 0),
(55, 33, 0, NULL, NULL, 0),
(56, 34, 0, '', '', 0),
(53, 32, 0, NULL, NULL, 0),
(54, 33, 0, '', '', 0),
(52, 32, 0, '', '', 0),
(51, 31, 0, '', 'saliendo fuera de plazo', 0),
(50, 31, 0, '', 'saliendo fuera de plazo', 0),
(49, 30, 0, '', 'saliendo fuera de plazo', 0),
(48, 30, 0, '', 'saliendo fuera de plazo', 0),
(47, 28, 32, 'salida con auto', 'saliendo con auto', 0),
(46, 28, 32, NULL, NULL, 0),
(45, 28, 32, 'entrada con control inicio, fin', '<p>probando con doble control</p>', 0),
(59, 35, 0, NULL, NULL, 0),
(60, 36, 32, 'rellenando detalle salida', 'observacion', 0),
(61, 36, 32, 'rellenando detalle salida', 'observacion', 0),
(62, 38, 32, 'control de entrada de carga', 'observación entrada', 0),
(63, 38, 32, 'control de salida de carga', 'observación salida', 0),
(64, 39, 0, '', '', 0),
(65, 39, 0, NULL, NULL, 0),
(66, 40, 0, '', '', 0),
(67, 40, 32, NULL, NULL, 0),
(68, 41, 0, '', '', 0),
(69, 41, 32, 'sfcew', '', 0),
(70, 42, 0, '', '', 0),
(71, 42, 32, 'asc', '', 0),
(72, 43, 32, 'as', '<p>as</p>', 0),
(73, 43, 32, 'asdc', '', 0),
(74, 45, 0, '', '', 0),
(75, 45, 32, 'tt', '', 0),
(76, 46, 32, 'titulo entrada', '<p>observacion entrada</p>', 0),
(77, 46, 32, 'titulo salida', '', 0),
(78, 47, 0, '', '', 0),
(79, 47, 32, 'sal', '', 1),
(80, 48, 0, '', '', 0),
(81, 48, 32, 'salida as', '', 1),
(82, 49, 32, 'form in', '<p>observ form in</p>', 0),
(83, 49, 32, 'title form o', '', 1),
(84, 50, 32, 'tt', '<p>ed</p>', 0),
(85, 50, 0, NULL, NULL, 1),
(86, 51, 32, 'dd', '<p>dd</p>', 0),
(87, 51, 0, NULL, NULL, 1),
(88, 52, 32, 'wed', '<p>wed</p>', 0),
(89, 52, 0, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `access_people_intents`
--

CREATE TABLE `access_people_intents` (
  `access_people_id` int(11) NOT NULL,
  `doors_id` int(11) NOT NULL,
  `entry` tinyint(1) DEFAULT '0',
  `success` tinyint(1) DEFAULT '0',
  `vehicles_id` int(11) NOT NULL DEFAULT '0',
  `flow` tinyint(1) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `access_people_intents`
--

INSERT INTO `access_people_intents` (`access_people_id`, `doors_id`, `entry`, `success`, `vehicles_id`, `flow`, `created`) VALUES
(28, 1, 0, 1, 0, 0, '2018-04-25 17:07:02'),
(29, 1, 0, 1, 0, 0, '2018-04-25 17:07:02'),
(30, 1, 0, 1, 0, 0, '2018-04-25 18:23:43'),
(31, 1, 0, 1, 0, 0, '2018-04-26 17:54:23'),
(32, 1, 0, 1, 0, 0, '2018-04-28 14:33:11'),
(33, 1, 0, 1, 0, 0, '2018-04-28 14:34:38'),
(34, 1, 0, 1, 0, 0, '2018-04-28 14:41:58'),
(35, 1, 0, 1, 0, 0, '2018-04-28 14:43:38'),
(36, 1, 0, 1, 0, 0, '2018-04-29 09:42:51'),
(37, 1, 0, 1, 0, 0, '2018-04-29 09:43:05'),
(38, 1, 0, 1, 0, 0, '2018-04-29 16:11:04'),
(39, 1, 0, 1, 0, 0, '2018-05-04 20:50:44'),
(40, 1, 0, 1, 15, 0, '2018-05-04 21:09:23'),
(41, 1, 0, 1, 15, 0, '2018-05-04 22:24:09'),
(42, 1, 0, 1, 15, 0, '2018-05-04 22:29:38'),
(43, 1, 0, 1, 0, 0, '2018-05-23 17:54:06'),
(45, 1, 0, 1, 15, 0, '2018-05-23 18:47:26'),
(46, 1, 0, 1, 15, 0, '2018-05-23 19:42:27'),
(47, 1, 0, 1, 0, 0, '2018-05-24 23:32:59'),
(48, 1, 0, 1, 0, 0, '2018-05-25 22:36:52'),
(49, 1, 0, 1, 0, 0, '2018-05-25 22:54:18'),
(52, 1, 0, 1, 0, 0, '2018-06-08 00:22:14');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `access_people_reasons_visit`
--

CREATE TABLE `access_people_reasons_visit` (
  `access_people_id` int(11) NOT NULL,
  `reasons_visit_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `access_people_reasons_visit`
--

INSERT INTO `access_people_reasons_visit` (`access_people_id`, `reasons_visit_id`) VALUES
(28, 10),
(29, 10),
(30, 10),
(31, 11),
(32, 10),
(33, 10),
(34, 10),
(35, 10),
(36, 10),
(36, 12),
(37, 10),
(37, 12),
(38, 10),
(39, 5),
(40, 12),
(41, 10),
(42, 10),
(43, 10),
(44, 10),
(45, 11),
(45, 12),
(46, 12),
(47, 10),
(48, 11),
(49, 10),
(50, 10),
(51, 9),
(52, 11);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `access_people_route`
--

CREATE TABLE `access_people_route` (
  `access_people_id` int(11) NOT NULL,
  `doors_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `access_people_route`
--

INSERT INTO `access_people_route` (`access_people_id`, `doors_id`) VALUES
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1),
(37, 1),
(38, 1),
(39, 1),
(40, 1),
(41, 1),
(42, 1),
(43, 1),
(44, 1),
(45, 1),
(46, 1),
(47, 1),
(48, 1),
(49, 1),
(50, 1),
(51, 1),
(52, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `access_people_state_history`
--

CREATE TABLE `access_people_state_history` (
  `id` int(11) NOT NULL,
  `access_people_id` int(11) DEFAULT NULL,
  `access_state_id` int(11) DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `access_people_state_history`
--

INSERT INTO `access_people_state_history` (`id`, `access_people_id`, `access_state_id`, `description`, `created`) VALUES
(40, 41, 2, 'USUARIO USUARIO', '2018-05-04 22:23:17'),
(39, 40, 2, 'USUARIO USUARIO', '2018-05-04 20:57:02'),
(38, 39, 2, 'USUARIO USUARIO', '2018-05-04 20:49:15'),
(37, 38, 2, 'USUARIO USUARIO', '2018-04-29 16:10:33'),
(36, 37, 2, 'USUARIO USUARIO', '2018-04-29 09:42:13'),
(35, 36, 2, 'USUARIO USUARIO', '2018-04-29 09:42:13'),
(34, 35, 2, 'USUARIO USUARIO', '2018-04-28 14:43:16'),
(33, 34, 2, 'USUARIO USUARIO', '2018-04-28 14:41:37'),
(32, 33, 2, 'USUARIO USUARIO', '2018-04-28 14:34:16'),
(31, 32, 2, 'USUARIO USUARIO', '2018-04-28 14:32:30'),
(30, 31, 2, 'USUARIO USUARIO', '2018-04-26 17:52:59'),
(29, 30, 2, 'USUARIO USUARIO', '2018-04-25 18:23:13'),
(28, 29, 2, 'USUARIO USUARIO', '2018-04-25 17:04:53'),
(27, 28, 2, 'USUARIO USUARIO', '2018-04-25 17:04:53'),
(41, 42, 2, 'USUARIO USUARIO', '2018-05-04 22:28:12'),
(42, 43, 2, 'USUARIO USUARIO', '2018-05-23 17:51:20'),
(43, 44, 2, 'USUARIO USUARIO', '2018-05-23 17:51:20'),
(44, 45, 2, 'USUARIO USUARIO', '2018-05-23 18:46:24'),
(45, 46, 2, 'USUARIO USUARIO', '2018-05-23 19:40:45'),
(46, 47, 2, 'USUARIO USUARIO', '2018-05-24 23:32:09'),
(47, 48, 2, 'USUARIO USUARIO', '2018-05-25 22:36:21'),
(48, 49, 2, 'USUARIO USUARIO', '2018-05-25 22:53:51'),
(49, 50, 2, 'USUARIO USUARIO', '2018-06-07 23:44:14'),
(50, 51, 2, 'USUARIO USUARIO', '2018-06-08 00:19:16'),
(51, 52, 2, 'USUARIO USUARIO', '2018-06-08 00:21:19');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `access_people_visit`
--

CREATE TABLE `access_people_visit` (
  `access_people_id` int(11) NOT NULL,
  `people_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `access_people_visit`
--

INSERT INTO `access_people_visit` (`access_people_id`, `people_id`) VALUES
(28, 2),
(29, 2),
(30, 2),
(31, 2),
(32, 2),
(33, 2),
(34, 2),
(35, 2),
(36, 2),
(37, 2),
(38, 2),
(39, 2),
(40, 2),
(41, 2),
(42, 2),
(43, 2),
(44, 2),
(45, 2),
(46, 2),
(47, 2),
(48, 2),
(49, 2),
(50, 2),
(51, 2),
(52, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `access_people_zones`
--

CREATE TABLE `access_people_zones` (
  `access_people_id` int(11) NOT NULL,
  `zones_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `access_people_zones`
--

INSERT INTO `access_people_zones` (`access_people_id`, `zones_id`) VALUES
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1),
(37, 1),
(38, 1),
(39, 1),
(40, 1),
(41, 1),
(42, 1),
(43, 1),
(44, 1),
(45, 1),
(46, 1),
(47, 1),
(48, 1),
(49, 1),
(50, 1),
(51, 1),
(52, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `access_state`
--

CREATE TABLE `access_state` (
  `id` int(11) NOT NULL,
  `state` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `access_state`
--

INSERT INTO `access_state` (`id`, `state`, `created`, `modified`) VALUES
(6, 'RECHAZADO', '2018-04-29 20:06:01', '2018-04-29 20:06:01'),
(5, 'PERMITIDO', '2018-04-29 20:05:50', '2018-04-29 20:05:50'),
(4, 'PENDIENTE', '2018-04-29 20:05:50', '2018-04-29 20:05:50');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `answers_type`
--

CREATE TABLE `answers_type` (
  `id` int(11) NOT NULL,
  `type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `answers_type`
--

INSERT INTO `answers_type` (`id`, `type`, `created`, `modified`) VALUES
(1, 'Respuesta corta', '2018-02-14 04:42:23', '2018-02-14 11:33:59'),
(2, 'Párrafo', '2018-02-14 11:34:17', '2018-02-14 11:34:17'),
(3, 'Cantidad', '2018-02-14 11:34:28', '2018-02-14 11:34:28'),
(4, 'Fecha', '2018-02-14 11:34:42', '2018-02-14 11:34:42'),
(5, 'Binaria', '2018-02-14 11:34:53', '2018-02-14 11:34:53');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `areas`
--

CREATE TABLE `areas` (
  `id` int(11) NOT NULL,
  `area` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `zones_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `areas`
--

INSERT INTO `areas` (`id`, `area`, `zones_id`, `created`, `modified`) VALUES
(1, 'Área 1', 1, '2018-02-09 08:42:23', '2018-04-02 13:05:00'),
(9, 'Área 2', 1, '2018-04-02 13:04:40', '2018-04-02 13:04:40'),
(10, 'MONITOREO', 1, '2018-04-02 13:04:54', '2018-04-02 13:07:07');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `companies`
--

CREATE TABLE `companies` (
  `id` int(11) NOT NULL,
  `company` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `internal` tinyint(1) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `companies`
--

INSERT INTO `companies` (`id`, `company`, `address`, `phone`, `email`, `contact`, `internal`, `created`, `modified`) VALUES
(1, 'INTERNA', 'csCADS', 'CscdsC', 'sdcdsC@DD.CL', 'CscdCCCa', 0, '2018-02-08 11:47:48', '2018-02-20 07:56:31'),
(2, 'CONTRATISTA', 'Villa Pedro Nolasco Calle C #973', '945330884', 'aliro.ramirez02@inacapmail.cl', 'sxasxASX', 0, '2018-02-08 11:48:25', '2018-02-08 11:48:25'),
(3, 'VISITA', 'Villa Pedro Nolasco Calle C #973', '945330884', 'aliro.ramirez02@inacapmail.cl', 'tu', 0, '2018-02-09 12:56:09', '2018-02-09 12:56:09');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `current_internal_state`
--

CREATE TABLE `current_internal_state` (
  `id` int(11) NOT NULL,
  `cod` int(11) NOT NULL,
  `flow` tinyint(1) NOT NULL DEFAULT '0',
  `internal` tinyint(1) NOT NULL,
  `people_id` int(11) NOT NULL,
  `vehicles_id` int(11) NOT NULL,
  `doors_id` int(11) NOT NULL,
  `action` tinyint(1) NOT NULL,
  `end_time` datetime NOT NULL DEFAULT '2099-01-01 12:00:00',
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `departments`
--

CREATE TABLE `departments` (
  `id` int(11) NOT NULL,
  `department` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `areas_id` int(11) NOT NULL,
  `in_charge` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `departments`
--

INSERT INTO `departments` (`id`, `department`, `areas_id`, `in_charge`, `created`, `modified`) VALUES
(3, 'MONITOREO', 10, 54, '2018-02-10 02:51:09', '2018-04-02 13:05:31');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `doors`
--

CREATE TABLE `doors` (
  `id` int(11) NOT NULL,
  `door` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `level` tinyint(3) NOT NULL,
  `doors_type_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `doors`
--

INSERT INTO `doors` (`id`, `door`, `description`, `level`, `doors_type_id`, `created`, `modified`) VALUES
(1, 'SALA MONITOREO', 'ENTRADA A SALA DE MONITOREO', 0, 4, '2018-02-09 04:15:42', '2018-04-02 18:05:35');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `doors_areas`
--

CREATE TABLE `doors_areas` (
  `doors_id` int(11) NOT NULL,
  `areas_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `doors_departments`
--

CREATE TABLE `doors_departments` (
  `doors_id` int(11) NOT NULL,
  `departments_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `doors_departments`
--

INSERT INTO `doors_departments` (`doors_id`, `departments_id`) VALUES
(1, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `doors_parents`
--

CREATE TABLE `doors_parents` (
  `doors_id` int(11) NOT NULL,
  `parent` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `doors_type`
--

CREATE TABLE `doors_type` (
  `id` int(11) NOT NULL,
  `type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `doors_type`
--

INSERT INTO `doors_type` (`id`, `type`, `created`, `modified`) VALUES
(4, 'Ingreso', '2018-02-09 03:47:30', '2018-03-28 18:27:34'),
(5, 'Salida', '2018-02-09 03:47:38', '2018-03-28 18:27:41'),
(10, 'Ingreso / salida', '2018-03-28 18:27:51', '2018-03-28 18:27:51');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `doors_zones`
--

CREATE TABLE `doors_zones` (
  `doors_id` int(11) NOT NULL,
  `zones_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `forms`
--

CREATE TABLE `forms` (
  `id` int(11) NOT NULL,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `forms`
--

INSERT INTO `forms` (`id`, `title`, `description`, `created`, `modified`) VALUES
(32, 'Formulario prueba', 'prueba crear, editar', '2018-02-16 03:04:20', '2018-02-25 01:27:52'),
(33, 'algo', '<p>llll</p>', '2018-04-12 21:19:11', '2018-04-12 21:19:11');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `forms_detail`
--

CREATE TABLE `forms_detail` (
  `order` int(11) NOT NULL,
  `question` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `placeholder` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `forms_id` int(11) NOT NULL,
  `answers_type_id` int(11) NOT NULL,
  `measures_id` int(11) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `forms_detail`
--

INSERT INTO `forms_detail` (`order`, `question`, `placeholder`, `forms_id`, `answers_type_id`, `measures_id`) VALUES
(1, 'Pregunta 1', 'Respuesta 1', 32, 1, 0),
(2, 'Pregunta 2', 'Respuesta 2', 32, 2, 0),
(3, 'Pregunta 3', '12', 32, 3, 1),
(4, 'Pregunta 4', '2018-04-12 21:10:47', 32, 4, 0),
(5, 'Pregunta 5', '1', 32, 5, 0),
(1, '1', 'a', 33, 3, 1),
(2, '2', '33', 33, 2, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `forms_season`
--

CREATE TABLE `forms_season` (
  `vechiles_type_id` int(11) NOT NULL,
  `forms_id` int(11) NOT NULL,
  `seasons_id` int(11) NOT NULL,
  `year` int(4) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `forms_season`
--

INSERT INTO `forms_season` (`vechiles_type_id`, `forms_id`, `seasons_id`, `year`) VALUES
(1, 32, 1, 2018),
(3, 32, 4, 2019);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `internal_answers`
--

CREATE TABLE `internal_answers` (
  `control_id` int(11) NOT NULL,
  `internal_forms_id` int(11) NOT NULL,
  `order` int(11) NOT NULL,
  `answer` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `control` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `internal_answers`
--

INSERT INTO `internal_answers` (`control_id`, `internal_forms_id`, `order`, `answer`, `control`) VALUES
(7, 32, 3, '12', 0),
(7, 32, 2, '<p>eeee</p>', 0),
(7, 32, 1, 'eeee', 0),
(7, 32, 4, '2018-05-26', 0),
(7, 32, 5, '1', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `internal_forms`
--

CREATE TABLE `internal_forms` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `observation` text COLLATE utf8mb4_unicode_ci,
  `people_id` int(11) DEFAULT NULL,
  `vehicle_id` int(11) DEFAULT NULL,
  `forms_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `internal_forms`
--

INSERT INTO `internal_forms` (`id`, `title`, `observation`, `people_id`, `vehicle_id`, `forms_id`, `created`, `modified`) VALUES
(7, 'eeee', '<p>eeee</p>', 42, 2, 32, '2018-05-26 00:15:43', '2018-05-26 00:15:43');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `internal_people_errors`
--

CREATE TABLE `internal_people_errors` (
  `id` int(11) NOT NULL,
  `people_id` int(11) NOT NULL,
  `sensors_id` int(11) NOT NULL,
  `entry` tinyint(1) DEFAULT '0',
  `reasons_error_id` int(11) NOT NULL,
  `created` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `internal_people_errors`
--

INSERT INTO `internal_people_errors` (`id`, `people_id`, `sensors_id`, `entry`, `reasons_error_id`, `created`) VALUES
(1, 2, 1, 0, 1, '2018-02-18 12:02:06'),
(2, 30, 1, 0, 2, '2018-03-19 16:42:54'),
(3, 30, 1, 0, 2, '2018-03-19 16:44:37'),
(4, 30, 1, 0, 2, '2018-03-19 16:45:34'),
(5, 45, 1, 0, 2, '2018-03-25 16:36:03'),
(6, 45, 1, 0, 2, '2018-03-25 16:36:17'),
(7, 45, 1, 0, 1, '2018-03-27 15:05:49'),
(8, 45, 1, 0, 1, '2018-03-27 16:34:26'),
(9, 45, 1, 0, 1, '2018-03-27 16:34:40'),
(10, 45, 1, 0, 1, '2018-03-27 16:37:24'),
(11, 45, 1, 0, 1, '2018-03-27 16:38:58'),
(12, 45, 1, 0, 2, '2018-03-27 18:06:02'),
(13, 45, 1, 0, 2, '2018-03-27 18:08:20'),
(14, 45, 1, 0, 2, '2018-03-27 18:11:23'),
(15, 45, 1, 0, 2, '2018-03-27 18:11:33'),
(16, 45, 1, 0, 2, '2018-03-27 18:13:12'),
(17, 45, 1, 0, 2, '2018-03-27 18:13:19'),
(18, 45, 1, 0, 2, '2018-03-27 18:13:50');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `internal_people_success`
--

CREATE TABLE `internal_people_success` (
  `id` int(11) NOT NULL,
  `people_id` int(11) NOT NULL,
  `entry` tinyint(1) DEFAULT '0',
  `sensors_id` int(11) NOT NULL,
  `created` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `internal_people_success`
--

INSERT INTO `internal_people_success` (`id`, `people_id`, `entry`, `sensors_id`, `created`) VALUES
(1, 2, 0, 1, '2018-02-18 16:17:44'),
(2, 3, 0, 1, '2018-02-18 16:22:59'),
(3, 47, 0, 1, '2018-03-19 13:03:13'),
(4, 47, 0, 1, '2018-03-19 13:15:23'),
(5, 21, 0, 1, '2018-03-19 13:17:14'),
(6, 31, 0, 1, '2018-03-19 13:23:36'),
(7, 31, 0, 1, '2018-03-19 13:24:07'),
(8, 31, 0, 1, '2018-03-19 13:27:57'),
(9, 3, 0, 1, '2018-03-19 16:41:59'),
(10, 2, 0, 1, '2018-03-19 16:42:27'),
(11, 30, 0, 1, '2018-03-19 16:43:37'),
(12, 30, 0, 1, '2018-03-19 16:44:52'),
(13, 30, 0, 1, '2018-03-19 16:46:47'),
(14, 47, 0, 1, '2018-03-20 09:48:34'),
(15, 2, 0, 1, '2018-03-22 12:52:43'),
(16, 2, 0, 1, '2018-03-22 12:52:49'),
(17, 2, 0, 1, '2018-03-22 15:06:14'),
(18, 45, 0, 1, '2018-03-22 16:03:17'),
(19, 45, 0, 1, '2018-03-22 16:13:16'),
(20, 45, 0, 1, '2018-03-23 08:58:42'),
(21, 45, 0, 1, '2018-03-23 08:58:54'),
(22, 45, 0, 1, '2018-03-23 08:59:02'),
(23, 45, 0, 1, '2018-03-23 08:59:22'),
(24, 45, 0, 1, '2018-03-23 08:59:26'),
(25, 45, 0, 1, '2018-03-23 08:59:53'),
(26, 45, 0, 1, '2018-03-27 15:04:08'),
(27, 2, 0, 1, '2018-03-27 16:38:47'),
(28, 3, 0, 1, '2018-03-27 16:38:53'),
(29, 3, 0, 1, '2018-03-27 16:41:47'),
(30, 45, 0, 1, '2018-03-28 10:36:42'),
(31, 45, 0, 1, '2018-03-28 10:36:46'),
(32, 45, 0, 1, '2018-03-28 10:36:58'),
(33, 45, 0, 1, '2018-03-28 16:41:17'),
(34, 45, 0, 1, '2018-03-28 16:47:12'),
(35, 45, 0, 1, '2018-03-28 16:48:17'),
(36, 45, 0, 1, '2018-03-28 17:08:01'),
(37, 45, 0, 1, '2018-03-28 17:08:26'),
(38, 45, 0, 1, '2018-03-29 10:54:59'),
(39, 45, 0, 1, '2018-03-29 13:19:58'),
(40, 45, 0, 1, '2018-03-29 13:20:06'),
(41, 45, 0, 1, '2018-03-29 13:20:11'),
(42, 45, 0, 1, '2018-03-29 13:24:21'),
(43, 45, 0, 1, '2018-03-29 13:24:27'),
(44, 45, 0, 1, '2018-03-29 13:24:31'),
(45, 45, 0, 1, '2018-03-29 13:24:36'),
(46, 45, 0, 1, '2018-03-29 13:24:42'),
(47, 45, 0, 1, '2018-03-29 13:24:47'),
(48, 45, 0, 1, '2018-03-29 13:24:58'),
(49, 45, 0, 1, '2018-04-02 10:27:03'),
(50, 45, 0, 1, '2018-04-02 10:31:19'),
(51, 45, 0, 1, '2018-04-02 10:31:32'),
(52, 45, 0, 1, '2018-04-02 10:32:04'),
(53, 45, 0, 1, '2018-04-02 10:32:19'),
(54, 54, 0, 1, '2018-04-02 11:15:10'),
(55, 45, 0, 1, '2018-04-02 11:15:24'),
(56, 45, 0, 1, '2018-04-02 11:15:35'),
(57, 54, 0, 1, '2018-04-02 11:15:45'),
(58, 54, 0, 1, '2018-04-02 12:17:07'),
(59, 54, 0, 1, '2018-04-02 12:17:12'),
(60, 54, 0, 1, '2018-04-02 12:17:28'),
(61, 54, 0, 1, '2018-04-02 12:19:12'),
(62, 54, 0, 1, '2018-04-02 12:19:43'),
(63, 54, 0, 1, '2018-04-02 12:26:59'),
(64, 54, 0, 1, '2018-04-02 12:27:03'),
(65, 54, 0, 1, '2018-04-02 12:27:09'),
(66, 45, 0, 1, '2018-04-02 13:14:24'),
(67, 45, 0, 1, '2018-04-02 13:14:58'),
(68, 3, 0, 1, '2018-04-02 13:25:05'),
(69, 3, 0, 1, '2018-04-02 13:25:38'),
(70, 45, 0, 1, '2018-04-02 13:26:48'),
(71, 3, 0, 1, '2018-04-02 13:30:30'),
(72, 3, 0, 1, '2018-04-02 13:32:54'),
(73, 3, 0, 1, '2018-04-02 13:33:06'),
(74, 45, 0, 1, '2018-04-02 15:11:37'),
(75, 54, 0, 1, '2018-04-02 15:18:32'),
(76, 54, 0, 1, '2018-04-02 15:18:36'),
(77, 3, 0, 1, '2018-04-02 15:19:19'),
(78, 3, 0, 1, '2018-04-02 15:34:09');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `internal_vehicles_errors`
--

CREATE TABLE `internal_vehicles_errors` (
  `id` int(11) NOT NULL,
  `vehicles_id` int(11) NOT NULL,
  `people_id` int(11) NOT NULL DEFAULT '0',
  `sensors_id` int(11) DEFAULT NULL,
  `entry` tinyint(1) DEFAULT '0',
  `reasons_error_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `internal_vehicles_success`
--

CREATE TABLE `internal_vehicles_success` (
  `id` int(11) NOT NULL,
  `vehicles_id` int(11) NOT NULL,
  `people_id` int(11) NOT NULL DEFAULT '0',
  `entry` tinyint(4) DEFAULT NULL,
  `sensors_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `jornada`
--

CREATE TABLE `jornada` (
  `id` int(11) NOT NULL,
  `jornada` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time_init` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time_end` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `jornada`
--

INSERT INTO `jornada` (`id`, `jornada`, `time_init`, `time_end`) VALUES
(2, 'Jornada 1', '08:00', '18:00'),
(3, 'Jornada 2', '20:00', '08:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `local_information`
--

CREATE TABLE `local_information` (
  `id` int(11) NOT NULL,
  `cod_company` int(11) NOT NULL,
  `company` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cod_installation` int(11) NOT NULL,
  `installation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `people_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `local_information`
--

INSERT INTO `local_information` (`id`, `cod_company`, `company`, `cod_installation`, `installation`, `address`, `email`, `phone`, `people_id`, `created`, `modified`) VALUES
(1, 123456, 'gggg', 12, 'cco', 'casa', 'a@q.cl', '121212', 2, '2018-02-17 10:48:04', '2018-02-17 10:53:08');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `main_access`
--

CREATE TABLE `main_access` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ubication` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ip_host` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_host` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `entry` tinyint(1) NOT NULL,
  `flow` tinyint(1) NOT NULL,
  `internal` tinyint(1) NOT NULL,
  `state` tinyint(1) NOT NULL,
  `pop_up` tinyint(1) NOT NULL,
  `doors_id` int(11) DEFAULT NULL,
  `main` tinyint(1) DEFAULT '0',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `main_access`
--

INSERT INTO `main_access` (`id`, `name`, `ubication`, `ip_host`, `name_host`, `entry`, `flow`, `internal`, `state`, `pop_up`, `doors_id`, `main`, `created`, `modified`) VALUES
(2, 'Acceso principal', 'Teno', 'localhost', 'PAPC', 2, 0, 1, 1, 0, 1, 0, '2018-02-11 05:09:39', '2018-02-14 09:21:45'),
(3, 'Acceso secundario 1', 'Curicó', 'localhost', 'PASC1', 2, 2, 2, 1, 0, 0, 0, '2018-02-11 05:11:45', '2018-02-14 09:22:07'),
(4, 'Acceso secundario 2', 'Curicó', 'localhost', 'papap', 0, 2, 0, 1, 0, 0, 0, '2018-02-11 05:13:57', '2018-02-14 09:22:40'),
(5, 'Acceso principal3', 'Curicó', 'localhost', 'prprpp', 1, 1, 1, 1, 0, 0, 0, '2018-02-11 05:14:54', '2018-02-13 12:57:27'),
(6, 'Acceso principal 4', 'Curicó', 'localhost', 'papappapapa', 1, 1, 1, 0, 0, 0, 0, '2018-02-11 05:17:00', '2018-02-14 09:31:30');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `measures`
--

CREATE TABLE `measures` (
  `id` int(11) NOT NULL,
  `measure` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `acronimo` varchar(6) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `measures`
--

INSERT INTO `measures` (`id`, `measure`, `acronimo`, `created`, `modified`) VALUES
(1, 'Kilos', 'Kgs', '2018-02-14 23:26:17', '2018-02-24 09:47:44'),
(3, 'Litros', 'Lts', '2018-02-17 03:49:06', '2018-02-17 03:49:06'),
(4, 'Gramos', 'Grs', '2018-02-24 09:47:38', '2018-02-24 09:47:38');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `minimum_requirements`
--

CREATE TABLE `minimum_requirements` (
  `id` int(11) NOT NULL,
  `requirement` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `minimum_requirements`
--

INSERT INTO `minimum_requirements` (`id`, `requirement`, `description`, `created`, `modified`) VALUES
(1, 'prueba', '<p>a</p>', '2018-05-05 15:27:31', '2018-05-05 15:27:31'),
(2, 'prueba 2', '<p>as</p>', '2018-05-16 20:46:14', '2018-05-16 20:46:14'),
(3, 'prueba 3', '<p>as</p>', '2018-05-16 20:46:22', '2018-05-16 20:46:22');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `notifications`
--

CREATE TABLE `notifications` (
  `id` int(11) NOT NULL,
  `notification` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `doors_id` int(11) DEFAULT NULL,
  `sensors_id` int(11) DEFAULT NULL,
  `entry` tinyint(1) DEFAULT '0',
  `created` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `notifications`
--

INSERT INTO `notifications` (`id`, `notification`, `doors_id`, `sensors_id`, `entry`, `created`) VALUES
(1, 'INGRESO NO IDENTIFICADO RUT: 121', 1, 1, 0, '2018-03-27 18:05:55'),
(2, 'INGRESO NO PERMITIDO : 177956007 | MATIAS QUEZADA SANHUEZA | INTERNA', 1, 1, 0, '2018-03-27 18:06:02'),
(3, 'INGRESO NO PERMITIDO : 177956007 | MATIAS QUEZADA SANHUEZA | INTERNA', 1, 1, 0, '2018-03-27 18:08:20'),
(4, 'INGRESO NO IDENTIFICADO RUT: 121', 1, 1, 0, '2018-03-27 18:09:01'),
(5, 'INGRESO NO PERMITIDO : 177956007 | MATIAS QUEZADA SANHUEZA | INTERNA', 1, 1, 0, '2018-03-27 18:11:23'),
(6, 'INGRESO NO PERMITIDO : 177956007 | MATIAS QUEZADA SANHUEZA | INTERNA', 1, 1, 0, '2018-03-27 18:11:33'),
(7, 'INGRESO NO IDENTIFICADO RUT: 121', 1, 1, 0, '2018-03-27 18:11:59'),
(8, 'INGRESO NO PERMITIDO : 177956007 | MATIAS QUEZADA SANHUEZA | INTERNA', 1, 1, 0, '2018-03-27 18:13:12'),
(9, 'INGRESO NO IDENTIFICADO RUT: 121', 1, 1, 0, '2018-03-27 18:13:15'),
(10, 'INGRESO NO PERMITIDO : 177956007 | MATIAS QUEZADA SANHUEZA | INTERNA', 1, 1, 0, '2018-03-27 18:13:19'),
(11, 'INGRESO NO PERMITIDO : 177956007 | MATIAS QUEZADA SANHUEZA | INTERNA', 1, 1, 0, '2018-03-27 18:13:50'),
(12, 'INGRESO NO IDENTIFICADO RUT: 121', 1, 1, 0, '2018-03-27 18:14:02'),
(13, 'INGRESO NO IDENTIFICADO RUT: 121', 1, 1, 0, '2018-03-27 18:14:28'),
(14, 'INGRESO NO IDENTIFICADO RUT: 121', 1, 1, 0, '2018-03-28 09:22:38'),
(15, 'INGRESO NO IDENTIFICADO RUT: 121', 1, 1, 0, '2018-03-28 09:23:38'),
(16, 'INGRESO NO IDENTIFICADO RUT: 121', 1, 1, 0, '2018-03-28 09:24:30'),
(17, 'INGRESO NO IDENTIFICADO RUT: 121', 1, 1, 0, '2018-03-28 09:25:42'),
(18, 'INGRESO NO IDENTIFICADO RUT: 121', 1, 1, 0, '2018-03-28 09:29:16'),
(19, 'INGRESO NO IDENTIFICADO RUT: 121', 1, 1, 0, '2018-03-28 09:30:13'),
(20, 'INGRESO NO IDENTIFICADO RUT: 121', 1, 1, 0, '2018-03-28 09:36:18'),
(21, 'INGRESO NO IDENTIFICADO RUT: 121', 1, 1, 0, '2018-03-28 10:13:28'),
(22, 'INGRESO NO IDENTIFICADO RUT: 121', 1, 1, 0, '2018-03-28 10:14:38'),
(23, 'INGRESO NO IDENTIFICADO RUT: ', 1, 1, 0, '2018-03-28 10:34:38'),
(24, 'INGRESO NO IDENTIFICADO RUT: ', 1, 1, 0, '2018-03-28 10:35:01'),
(25, 'INGRESO NO IDENTIFICADO RUT: ', 1, 1, 0, '2018-03-28 10:35:16'),
(26, 'INGRESO NO IDENTIFICADO RUT: ', 1, 1, 0, '2018-03-28 10:35:42'),
(27, 'INGRESO NO IDENTIFICADO RUT: ', 1, 1, 0, '2018-03-28 10:36:38'),
(28, 'INGRESO NO IDENTIFICADO RUT: 121', 1, 1, 0, '2018-03-28 10:46:26'),
(29, 'INGRESO NO IDENTIFICADO RUT: 121', 1, 1, 0, '2018-03-28 10:46:32'),
(30, 'INGRESO NO IDENTIFICADO RUT: 121', 1, 1, 0, '2018-03-28 15:17:20'),
(31, 'INGRESO NO IDENTIFICADO RUT: ', 1, 1, 0, '2018-03-28 16:41:49'),
(32, 'INGRESO NO PERMITIDO : 70954362 | ENRIQUE QUEZADA MARTINEZ | VISITA', 1, 1, 0, '2018-03-28 16:47:04'),
(33, 'INGRESO NO PERMITIDO : 70954362 | ENRIQUE QUEZADA MARTINEZ | VISITA', 1, 1, 0, '2018-03-28 16:48:30'),
(34, 'INGRESO NO IDENTIFICADO RUT: ', 1, 1, 0, '2018-03-28 17:08:07'),
(35, 'INGRESO NO IDENTIFICADO RUT: ', 1, 1, 0, '2018-03-28 17:08:13'),
(36, 'INGRESO NO IDENTIFICADO RUT: ', 1, 1, 0, '2018-03-28 18:23:10'),
(37, 'INGRESO NO IDENTIFICADO RUT: ', 1, 1, 0, '2018-03-28 18:23:21'),
(38, 'INGRESO NO IDENTIFICADO RUT: ', 1, 1, 0, '2018-03-28 18:23:33'),
(39, 'INGRESO NO IDENTIFICADO RUT: ', 1, 1, 0, '2018-03-28 18:23:39'),
(40, 'INGRESO NO IDENTIFICADO RUT: ', 1, 1, 0, '2018-03-28 18:23:46'),
(41, 'INGRESO NO IDENTIFICADO RUT: ', 1, 1, 0, '2018-03-28 18:23:54'),
(42, 'INGRESO NO IDENTIFICADO RUT: ', 1, 1, 0, '2018-03-28 18:24:03'),
(43, 'INGRESO NO IDENTIFICADO RUT: 121', 1, 1, 0, '2018-03-29 10:41:02'),
(44, 'INGRESO NO IDENTIFICADO RUT: 121', 1, 1, 0, '2018-03-29 10:45:17'),
(45, 'INGRESO NO IDENTIFICADO RUT: 121', 1, 1, 0, '2018-03-29 10:54:20'),
(46, 'INGRESO NO IDENTIFICADO RUT: 121', 1, 1, 0, '2018-03-29 12:00:16'),
(47, 'INGRESO NO IDENTIFICADO RUT: ', 1, 1, 0, '2018-04-02 12:23:55');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `options`
--

CREATE TABLE `options` (
  `id` int(11) NOT NULL,
  `option` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `options`
--

INSERT INTO `options` (`id`, `option`, `code`, `created`, `modified`) VALUES
(1, 'Opción 1.', 'a123-k', '2018-02-13 10:26:11', '2018-02-13 10:31:32'),
(4, 'Opción 2.', 'a123-k', '2018-02-13 10:26:11', '2018-02-13 10:31:32'),
(5, 'Opción 3.', 'a123-k', '2018-02-13 10:26:11', '2018-02-13 10:31:32'),
(6, 'Opción 4.', 'a123-k', '2018-02-13 10:26:11', '2018-02-13 10:31:32'),
(7, 'Opción 5.', 'a123-k', '2018-02-13 10:26:11', '2018-02-13 10:31:32');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `options_roles`
--

CREATE TABLE `options_roles` (
  `options_id` int(11) NOT NULL,
  `roles_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `options_roles`
--

INSERT INTO `options_roles` (`options_id`, `roles_id`) VALUES
(1, 2),
(5, 1),
(6, 1),
(7, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `people`
--

CREATE TABLE `people` (
  `id` int(11) NOT NULL,
  `rut` int(15) NOT NULL,
  `digit` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `allow_all` tinyint(1) DEFAULT NULL,
  `is_visited` tinyint(1) DEFAULT NULL,
  `internal` tinyint(1) DEFAULT '0',
  `nfc_code` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `people_profiles_id` int(11) NOT NULL,
  `companies_id` int(11) NOT NULL,
  `departments_id` int(11) DEFAULT '0',
  `states_id` int(11) DEFAULT '1',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `people`
--

INSERT INTO `people` (`id`, `rut`, `digit`, `name`, `last_name`, `address`, `email`, `phone`, `allow_all`, `is_visited`, `internal`, `nfc_code`, `people_profiles_id`, `companies_id`, `departments_id`, `states_id`, `created`, `modified`) VALUES
(2, 33333333, '3', 'persona', 'soltera', 'su casa', 'no_tiene@sinmail.cl', '88776655', 1, 1, 1, '0', 1, 1, 3, 1, '2018-06-06 13:56:04', '2018-06-06 13:56:04'),
(3, 11111111, '1', 'OPERADOR', 'MONITOREO', 'RENE LEON 80', 'monitoreo@mdsg.cl', '752314360', 1, 0, 1, '156230101153', 4, 1, 3, 1, '2018-04-02 13:23:38', '2018-04-02 13:23:38'),
(21, 22222222, '2', 'Aliro', 'Nuñez', 'Villa Pedro Nolasco Calle C #973', 'aliro.ramirez02@inacapmail.cl', '945330884', 0, 0, 1, '0', 2, 1, 3, 1, '2018-03-19 13:17:26', '2018-03-19 13:17:26'),
(30, 44444444, '4', 'Aliro', 'Nuñez', 'Villa Pedro Nolasco Calle C #973', 'aliro.ramirez02@inacapmail.cl', '945330884', 0, 1, 2, '0', 5, 2, 3, 1, '2018-03-19 16:42:48', '2018-03-19 16:42:48'),
(31, 55555555, '5', 'Aliro operario', 'Nuñez', 'Villa Pedro Nolasco Calle C #973', 'aliro.ramirez02@inacapmail.cl', '945330884', 0, 0, 2, '0', 4, 1, 3, 1, '2018-03-19 13:19:56', '2018-03-19 13:19:56'),
(42, 66666666, '6', 'Aliro', 'Nuñez', 'Villa Pedro Nolasco Calle C #973', 'aliro.ramirez02@inacapmail.cl', '945330884', 0, 0, 0, '0', 2, 1, 0, 1, '2018-03-04 02:40:43', '2018-03-04 02:40:43'),
(43, 77777777, '7', 'Aliro', 'Nuñez', 'Villa Pedro Nolasco Calle C #973', 'aliro.ramirez02@inacapmail.cl', '945330884', 0, 0, 2, '0', 2, 2, 0, 1, '2018-03-04 02:49:53', '2018-03-04 02:49:53'),
(44, 88888888, '8', 'Aliro', 'Nuñez', 'Villa Pedro Nolasco Calle C #973', 'aliro.ramirez02@inacapmail.cl', '945330884', 0, 0, 0, '0', 6, 3, 0, 1, '2018-03-24 21:55:31', '2018-03-24 21:55:31'),
(57, 14325993, '5', 'Aliro', 'Ramírez Núñez', 'su casa', 'a@a.cl', '555', 0, 0, 2, '0', 7, 2, 0, 1, '2018-04-29 21:15:11', '2018-04-29 21:15:11'),
(58, 15811305, '8', 'Jose', 'Cares', 'su casita', 'a@a.cl', '5544', 0, NULL, 2, '0', 7, 2, 0, 1, '2018-04-29 21:19:00', '2018-04-29 21:19:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `people_profiles`
--

CREATE TABLE `people_profiles` (
  `id` int(11) NOT NULL,
  `profile` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `people_profiles`
--

INSERT INTO `people_profiles` (`id`, `profile`, `created`, `modified`) VALUES
(1, 'CHOFER', '2018-02-10 19:31:33', '2018-02-10 19:31:33'),
(2, 'ADMINISTRATIVO', '2018-02-12 11:08:47', '2018-02-12 11:11:10'),
(3, 'JORNAL', '2018-02-12 11:10:14', '2018-02-12 11:10:14'),
(4, 'OPERARIO', '2018-02-12 11:10:23', '2018-02-12 11:10:23'),
(5, 'GUARDIA', '2018-02-12 11:10:31', '2018-02-12 11:10:31'),
(6, 'VISITA', '2018-03-16 09:57:16', '2018-03-16 09:57:18'),
(7, 'CONTRATISTA', '2018-03-16 09:57:20', '2018-03-16 09:57:22');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `profiles_doors_schedules`
--

CREATE TABLE `profiles_doors_schedules` (
  `profiles_people_id` int(11) NOT NULL,
  `doors_id` int(11) NOT NULL,
  `time_init` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time_end` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jornada_id` int(11) NOT NULL,
  `L` tinyint(1) DEFAULT '0',
  `M` tinyint(1) DEFAULT '0',
  `Mi` tinyint(1) DEFAULT '0',
  `J` tinyint(1) DEFAULT '0',
  `V` tinyint(1) DEFAULT '0',
  `S` tinyint(1) DEFAULT '0',
  `D` tinyint(1) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `profiles_doors_schedules`
--

INSERT INTO `profiles_doors_schedules` (`profiles_people_id`, `doors_id`, `time_init`, `time_end`, `jornada_id`, `L`, `M`, `Mi`, `J`, `V`, `S`, `D`) VALUES
(2, 1, '08:00', '18:00', 2, 1, 0, 1, 1, 1, 0, 0),
(4, 1, '08:00', '18:00', 2, 0, 1, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `projects`
--

CREATE TABLE `projects` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `in_charge` int(11) DEFAULT NULL,
  `in_charge_installation` int(11) DEFAULT NULL,
  `init` date DEFAULT NULL,
  `end` date DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `projects`
--

INSERT INTO `projects` (`id`, `title`, `description`, `in_charge`, `in_charge_installation`, `init`, `end`, `created`, `modified`) VALUES
(32, 'as', '<p>as</p>', 31, 2, '2018-05-06', '2018-05-14', '2018-05-18 19:12:44', '2018-05-18 19:12:44'),
(30, 'prueba 1', '<p>hiio</p>', 58, 2, '2018-04-29', '2018-05-05', '2018-04-29 23:17:21', '2018-04-29 23:17:21'),
(31, 'aaaa', '<p>aaaa</p>', 58, 2, '2018-04-30', '2018-05-07', '2018-05-14 22:14:15', '2018-05-14 22:14:15');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `projects_answers`
--

CREATE TABLE `projects_answers` (
  `control_id` int(11) NOT NULL,
  `projects_id` int(11) DEFAULT NULL,
  `forms_id` int(11) DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `answer` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `control` tinyint(1) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `projects_answers`
--

INSERT INTO `projects_answers` (`control_id`, `projects_id`, `forms_id`, `order`, `answer`, `control`) VALUES
(12, 32, 32, 1, 'r1', 0),
(12, 32, 32, 2, '<p>r2</p>', 0),
(12, 32, 32, 3, '3', 0),
(12, 32, 32, 4, '2018-05-13', 0),
(12, 32, 32, 5, '1', 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `projects_areas`
--

CREATE TABLE `projects_areas` (
  `projects_id` int(11) NOT NULL,
  `areas_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `projects_areas`
--

INSERT INTO `projects_areas` (`projects_id`, `areas_id`) VALUES
(1, 1),
(1, 9),
(1, 10),
(2, 1),
(2, 9),
(2, 10),
(3, 1),
(3, 9),
(3, 10),
(4, 1),
(4, 9),
(4, 10),
(5, 1),
(5, 9),
(5, 10),
(6, 1),
(6, 9),
(6, 10),
(7, 1),
(7, 9),
(7, 10),
(8, 1),
(8, 9),
(8, 10),
(9, 1),
(9, 9),
(9, 10),
(10, 1),
(10, 9),
(10, 10),
(11, 1),
(11, 9),
(11, 10),
(12, 1),
(12, 9),
(12, 10),
(13, 1),
(13, 9),
(13, 10),
(14, 1),
(14, 9),
(14, 10),
(15, 1),
(15, 9),
(15, 10),
(16, 1),
(16, 9),
(16, 10),
(17, 1),
(17, 9),
(17, 10),
(18, 1),
(18, 9),
(18, 10),
(19, 1),
(19, 9),
(19, 10),
(20, 1),
(20, 9),
(20, 10),
(21, 1),
(21, 9),
(21, 10),
(22, 1),
(22, 9),
(22, 10),
(23, 1),
(23, 9),
(23, 10),
(24, 1),
(24, 9),
(24, 10),
(25, 1),
(25, 9),
(25, 10),
(26, 1),
(26, 9),
(26, 10),
(27, 1),
(27, 9),
(27, 10),
(28, 1),
(28, 9),
(28, 10),
(29, 1),
(29, 9),
(29, 10),
(30, 1),
(30, 9),
(30, 10),
(31, 10),
(32, 10),
(33, 10),
(34, 10),
(35, 10);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `projects_departments`
--

CREATE TABLE `projects_departments` (
  `projects_id` int(11) NOT NULL,
  `departments_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `projects_departments`
--

INSERT INTO `projects_departments` (`projects_id`, `departments_id`) VALUES
(1, 3),
(2, 3),
(3, 3),
(4, 3),
(5, 3),
(6, 3),
(7, 3),
(8, 3),
(9, 3),
(10, 3),
(11, 3),
(12, 3),
(13, 3),
(14, 3),
(15, 3),
(16, 3),
(17, 3),
(18, 3),
(19, 3),
(20, 3),
(21, 3),
(22, 3),
(23, 3),
(24, 3),
(26, 3),
(27, 3),
(28, 3),
(29, 3),
(30, 3),
(31, 3),
(32, 3),
(33, 3),
(34, 3),
(35, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `projects_forms`
--

CREATE TABLE `projects_forms` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `forms_id` int(11) DEFAULT NULL,
  `people_id` int(11) DEFAULT NULL,
  `vehicle_id` int(11) DEFAULT NULL,
  `observation` text COLLATE utf8mb4_unicode_ci,
  `projects_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `projects_forms`
--

INSERT INTO `projects_forms` (`id`, `title`, `forms_id`, `people_id`, `vehicle_id`, `observation`, `projects_id`, `created`, `modified`) VALUES
(12, 'cuadrando datos', 32, 57, 6, '<p>asd</p>', 32, '2018-05-13 17:12:58', '2018-05-13 17:12:58');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `projects_intents`
--

CREATE TABLE `projects_intents` (
  `projects_id` int(11) NOT NULL,
  `doors_id` int(11) NOT NULL,
  `entry` tinyint(1) DEFAULT '0',
  `success` tinyint(1) DEFAULT '0',
  `vehicles_id` int(11) NOT NULL DEFAULT '0',
  `flow` tinyint(4) NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `people_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `projects_intents`
--

INSERT INTO `projects_intents` (`projects_id`, `doors_id`, `entry`, `success`, `vehicles_id`, `flow`, `created`, `people_id`) VALUES
(31, 1, 0, 1, 6, 1, '2018-05-01 23:04:56', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `projects_minimum_requirements`
--

CREATE TABLE `projects_minimum_requirements` (
  `projects_id` int(11) NOT NULL,
  `minimum_requirements` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `projects_minimum_requirements`
--

INSERT INTO `projects_minimum_requirements` (`projects_id`, `minimum_requirements`) VALUES
(31, 1),
(32, 2),
(32, 3),
(33, 1),
(34, 1),
(35, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `projects_people`
--

CREATE TABLE `projects_people` (
  `projects_id` int(11) NOT NULL,
  `people_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `projects_people`
--

INSERT INTO `projects_people` (`projects_id`, `people_id`) VALUES
(31, 58),
(32, 30),
(32, 31),
(32, 57);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `projects_route`
--

CREATE TABLE `projects_route` (
  `projects_id` int(11) NOT NULL,
  `doors_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `projects_route`
--

INSERT INTO `projects_route` (`projects_id`, `doors_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `projects_schedules`
--

CREATE TABLE `projects_schedules` (
  `projects_id` int(11) DEFAULT NULL,
  `time_init` time DEFAULT NULL,
  `time_end` time DEFAULT NULL,
  `L` tinyint(1) DEFAULT '0',
  `M` tinyint(1) DEFAULT '0',
  `Mi` tinyint(1) DEFAULT '0',
  `J` tinyint(1) DEFAULT '0',
  `V` tinyint(1) DEFAULT '0',
  `S` tinyint(1) DEFAULT '0',
  `D` tinyint(1) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `projects_schedules`
--

INSERT INTO `projects_schedules` (`projects_id`, `time_init`, `time_end`, `L`, `M`, `Mi`, `J`, `V`, `S`, `D`) VALUES
(1, '00:00:00', '00:00:00', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(2, '00:00:00', '00:00:00', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(3, '00:00:00', '00:00:00', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(4, '00:00:00', '00:00:00', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(5, '00:00:00', '00:00:00', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(6, '00:00:00', '00:00:00', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(7, '00:00:00', '00:00:00', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(8, '00:00:00', '00:00:00', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(9, '00:00:00', '00:00:00', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(10, '00:00:00', '00:00:00', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(11, '00:00:00', '00:00:00', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(12, '00:00:00', '00:00:00', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(13, '08:00:00', '09:00:00', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(14, '00:00:00', '00:00:00', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(15, '00:00:00', '00:00:00', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(16, '00:00:00', '00:00:00', 1, NULL, NULL, NULL, NULL, NULL, NULL),
(17, '00:00:00', '00:00:00', 1, 1, 1, NULL, NULL, NULL, NULL),
(18, '00:00:00', '00:00:00', 1, 1, 1, NULL, NULL, NULL, NULL),
(19, '08:00:00', '07:00:00', 0, 0, 0, 0, 0, 0, 0),
(20, '08:00:00', '07:00:00', 0, 0, 0, 0, 0, 0, 0),
(21, '08:00:00', '07:00:00', 0, 0, 0, 0, 0, 0, 0),
(22, '08:00:00', '07:00:00', 1, 1, 1, 1, 1, 0, 0),
(23, '07:00:00', '06:00:00', 1, 1, 1, 0, 0, 0, 0),
(24, '07:00:00', '06:00:00', 1, 1, 1, 1, 0, 0, 0),
(26, '00:00:01', '00:00:01', 1, 0, 0, 0, 0, NULL, NULL),
(27, '00:00:01', '00:00:01', 1, 0, 0, 0, 0, NULL, NULL),
(28, '07:00:00', '08:00:00', 1, 1, 1, 0, 0, 0, 0),
(29, '00:00:01', '00:00:01', 1, 1, 1, 0, 0, NULL, NULL),
(30, '08:00:00', '09:00:00', 1, 1, 1, 0, 0, 0, 0),
(31, '08:00:00', '07:00:00', 1, 1, 1, 0, 0, 0, 0),
(32, '08:00:00', '09:00:00', 1, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `projects_vehicles`
--

CREATE TABLE `projects_vehicles` (
  `projects_id` int(11) NOT NULL,
  `vehicles_id` int(11) NOT NULL,
  `control_init` tinyint(1) DEFAULT '0',
  `control_end` tinyint(1) DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `projects_vehicles`
--

INSERT INTO `projects_vehicles` (`projects_id`, `vehicles_id`, `control_init`, `control_end`) VALUES
(1, 15, 0, 0),
(2, 15, 0, 0),
(3, 15, 0, 0),
(4, 15, 0, 0),
(5, 15, 0, 0),
(6, 15, 0, 0),
(7, 15, 0, 0),
(8, 15, 0, 0),
(9, 15, 0, 0),
(10, 15, 0, 0),
(11, 15, 0, 0),
(12, 15, 0, 0),
(13, 15, 0, 0),
(14, 17, 1, 1),
(15, 17, 1, 1),
(16, 17, 1, 1),
(17, 17, 1, 1),
(18, 17, 1, 1),
(19, 17, 1, 1),
(20, 17, 0, 0),
(21, 17, 1, 1),
(22, 17, 0, 0),
(32, 6, 1, 0),
(31, 17, 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `projects_zones`
--

CREATE TABLE `projects_zones` (
  `projects_id` int(11) NOT NULL,
  `zones_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `projects_zones`
--

INSERT INTO `projects_zones` (`projects_id`, `zones_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reasons_error`
--

CREATE TABLE `reasons_error` (
  `id` int(11) NOT NULL,
  `reason` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `reasons_error`
--

INSERT INTO `reasons_error` (`id`, `reason`, `created`, `modified`) VALUES
(1, 'Puerta no autorizada', '2018-02-18 12:01:53', '2018-02-18 12:01:53'),
(2, 'Fuera de horario permitido', '2018-02-18 09:29:04', '2018-02-18 09:29:04');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reasons_visit`
--

CREATE TABLE `reasons_visit` (
  `id` int(11) NOT NULL,
  `reason` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `reasons_visit`
--

INSERT INTO `reasons_visit` (`id`, `reason`, `created`, `modified`) VALUES
(1, 'Servicio de valija', '2018-03-04 01:41:37', '2018-04-06 15:52:04'),
(2, 'Servicio de encomienda', '2018-03-04 01:41:54', '2018-03-04 01:41:54'),
(3, 'Entrega de materia prima', '2018-03-04 01:44:29', '2018-03-04 01:44:29'),
(4, 'Despacho a puerto', '2018-03-04 01:44:46', '2018-03-04 01:44:46'),
(5, 'Cliente de productos', '2018-03-04 03:17:05', '2018-03-04 03:17:05'),
(7, 'Proveedor materia prima', '2018-03-04 03:18:59', '2018-03-04 03:18:59'),
(8, 'Retiro propietario', '2018-03-04 03:21:32', '2018-03-04 03:21:32'),
(9, 'test', '2018-03-08 07:22:35', '2018-03-08 07:22:35'),
(10, 'tset3', '2018-03-08 07:23:22', '2018-03-08 07:23:22'),
(11, 'TEST69', '2018-03-26 15:22:12', '2018-03-26 15:22:12'),
(12, 'TEST69CASCAS', '2018-03-26 15:22:41', '2018-03-26 15:22:41');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `rol` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id`, `rol`, `created`, `modified`) VALUES
(1, 'Administrador', '2018-02-12 05:34:39', '2018-02-12 05:34:39'),
(2, 'Funcionario', '2018-02-12 05:35:00', '2018-02-12 05:35:00'),
(3, 'Visitante', '2018-02-12 05:35:11', '2018-02-12 05:35:11'),
(4, 'Guardia', '2018-02-12 05:35:22', '2018-02-12 05:35:22'),
(5, 'Operario', '2018-02-12 05:35:51', '2018-02-12 05:35:51');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `seasons`
--

CREATE TABLE `seasons` (
  `id` int(11) NOT NULL,
  `season` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `seasons`
--

INSERT INTO `seasons` (`id`, `season`, `created`, `modified`) VALUES
(1, 'Temporada 20188', '2018-02-12 03:21:23', '2018-02-12 03:26:00'),
(2, 'Temporada 2019', '2018-02-12 03:21:53', '2018-02-12 03:21:53'),
(3, 'Temporada 2020', '2018-02-12 03:22:05', '2018-02-12 03:22:05'),
(4, 'Temporada 2018-2019', '2018-02-12 03:22:17', '2018-02-12 03:22:17'),
(5, 'Temporada 2019 - 2020', '2018-02-12 03:22:29', '2018-02-12 03:22:29');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sensors`
--

CREATE TABLE `sensors` (
  `id` int(11) NOT NULL,
  `code` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sensor` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ip` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sensors_type` int(11) NOT NULL,
  `entry` tinyint(1) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `sensors`
--

INSERT INTO `sensors` (`id`, `code`, `sensor`, `description`, `ip`, `sensors_type`, `entry`, `created`, `modified`) VALUES
(1, 'RASP-PI-3', 'NFC', '', '192.10.10.20', 2, 0, '2018-02-12 03:37:54', '2018-04-02 12:31:16');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sensors_doors`
--

CREATE TABLE `sensors_doors` (
  `sensors_id` int(11) NOT NULL,
  `doors_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `sensors_doors`
--

INSERT INTO `sensors_doors` (`sensors_id`, `doors_id`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sensors_type`
--

CREATE TABLE `sensors_type` (
  `id` int(11) NOT NULL,
  `type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `sensors_type`
--

INSERT INTO `sensors_type` (`id`, `type`, `created`, `modified`) VALUES
(1, 'Movimiento', '2018-02-11 11:40:36', '2018-02-11 11:41:38'),
(2, 'Presencia', '2018-02-11 11:41:56', '2018-02-11 11:41:56');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `special_schedule`
--

CREATE TABLE `special_schedule` (
  `id` int(11) NOT NULL,
  `reason` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `people_id` int(11) NOT NULL,
  `doors_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `date_init` datetime NOT NULL,
  `date_end` datetime NOT NULL,
  `created` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `states`
--

CREATE TABLE `states` (
  `id` int(11) NOT NULL,
  `state` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `states`
--

INSERT INTO `states` (`id`, `state`) VALUES
(1, 'Activo'),
(2, 'Suspendido'),
(3, 'Bloqueado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `user` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles_id` int(11) NOT NULL,
  `people_id` int(11) NOT NULL,
  `users_state_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `user`, `password`, `roles_id`, `people_id`, `users_state_id`, `created`, `modified`) VALUES
(1, 'onibas123', '827ccb0eea8a706c4c34a16891f84e7b', 1, 45, 1, '2018-02-14 08:23:20', '2018-04-01 17:17:06'),
(2, 'prueba', '827ccb0eea8a706c4c34a16891f84e7b', 2, 47, 1, '2018-04-01 17:12:56', '2018-04-01 17:16:59');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users_state`
--

CREATE TABLE `users_state` (
  `id` int(11) NOT NULL,
  `state` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users_state`
--

INSERT INTO `users_state` (`id`, `state`, `created`, `modified`) VALUES
(1, 'Activo', '2018-02-12 08:03:38', '2018-02-12 08:06:49'),
(2, 'Inactivo', '2018-02-12 08:03:47', '2018-02-12 08:03:47');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vehicles`
--

CREATE TABLE `vehicles` (
  `id` int(11) NOT NULL,
  `patent` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `internal` tinyint(1) NOT NULL,
  `nfc_code` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `companies_id` int(11) NOT NULL,
  `people_id` int(11) NOT NULL,
  `vehicles_type_id` int(11) NOT NULL,
  `vehicles_profiles_id` int(11) NOT NULL,
  `states_id` int(11) DEFAULT '1',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `vehicles`
--

INSERT INTO `vehicles` (`id`, `patent`, `model`, `internal`, `nfc_code`, `companies_id`, `people_id`, `vehicles_type_id`, `vehicles_profiles_id`, `states_id`, `created`, `modified`) VALUES
(1, '111111', 'charger', 1, '0', 2, 2, 1, 1, 1, '2018-02-11 10:15:23', '2018-02-11 10:15:23'),
(2, '222222', 'camaro ss', 0, '0', 1, 2, 1, 6, 1, '2018-02-11 04:38:05', '2018-02-11 04:38:05'),
(3, '333333', 'carreta', 1, '0', 2, 2, 2, 5, 1, '2018-02-11 02:16:48', '2018-02-11 02:16:48'),
(4, '444444', 'bici', 1, '0', 1, 2, 7, 3, 1, '2018-02-11 02:17:08', '2018-02-11 02:17:08'),
(5, '555555', 'z1', 1, '0', 2, 2, 3, 5, 1, '2018-02-11 02:18:48', '2018-02-11 02:18:48'),
(6, '666666', 'z4', 2, '0', 3, 2, 1, 2, 1, '2018-02-11 02:19:05', '2018-02-11 02:19:05'),
(15, '656565', 'camaro ss', 0, '0', 2, 2, 1, 1, 1, '2018-04-10 22:45:18', '2018-04-10 22:45:18'),
(16, '656565', '', 0, '0', 1, 0, 1, 1, 1, '2018-04-22 17:40:18', '2018-04-22 17:40:18'),
(17, '999999', '', 2, '0', 2, 57, 2, 3, 1, '2018-04-29 20:07:33', '2018-04-29 20:07:33'),
(19, '666666', '', 2, '0', 1, 58, 1, 1, 1, '2018-04-30 19:16:36', '2018-04-30 19:16:36'),
(20, '343434', '23e', 0, '0', 2, 42, 1, 1, 1, '2018-05-06 21:20:14', '2018-05-06 21:20:14'),
(21, '454545', 'q', 0, '0', 2, 42, 1, 1, 1, '2018-05-06 21:21:07', '2018-05-06 21:21:07');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vehicles_drivers`
--

CREATE TABLE `vehicles_drivers` (
  `vehicles_id` int(11) NOT NULL,
  `people_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `vehicles_drivers`
--

INSERT INTO `vehicles_drivers` (`vehicles_id`, `people_id`) VALUES
(2, 42);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vehicles_profiles`
--

CREATE TABLE `vehicles_profiles` (
  `id` int(11) NOT NULL,
  `profile` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `vehicles_profiles`
--

INSERT INTO `vehicles_profiles` (`id`, `profile`, `created`, `modified`) VALUES
(1, 'perfil 1', '2018-02-10 07:14:30', '2018-02-10 07:14:30'),
(2, 'perfil 2', '2018-02-10 07:14:39', '2018-02-10 07:14:39'),
(3, 'perfil 3', '2018-02-10 07:14:47', '2018-02-10 07:14:47'),
(4, 'perfil 4', '2018-02-10 07:14:55', '2018-02-10 07:14:55'),
(5, 'perfil 5', '2018-02-10 07:15:00', '2018-02-10 07:19:55'),
(6, 'perfil 6', '2018-02-10 07:15:05', '2018-02-10 07:15:05');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vehicles_type`
--

CREATE TABLE `vehicles_type` (
  `id` int(11) NOT NULL,
  `type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `vehicles_type`
--

INSERT INTO `vehicles_type` (`id`, `type`, `created`, `modified`) VALUES
(1, 'Automovil', '2018-02-10 06:33:37', '2018-02-10 06:39:44'),
(2, 'Camioneta', '2018-02-10 06:34:19', '2018-02-10 06:34:19'),
(3, 'Furgon', '2018-02-10 06:34:26', '2018-02-11 02:19:45'),
(4, 'Camión 3/4', '2018-02-10 06:35:10', '2018-02-10 06:35:10'),
(5, 'Camión acoplado', '2018-02-10 06:35:38', '2018-02-10 06:35:38'),
(6, 'Camión rampla', '2018-02-10 06:35:47', '2018-02-10 06:35:47'),
(7, 'Camión cisterna', '2018-02-10 06:36:11', '2018-02-10 06:36:11');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `zones`
--

CREATE TABLE `zones` (
  `id` int(11) NOT NULL,
  `zone` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `zones`
--

INSERT INTO `zones` (`id`, `zone`, `created`, `modified`) VALUES
(1, 'ZONA 1', '2018-02-09 01:49:41', '2018-04-02 12:29:22');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `access_people`
--
ALTER TABLE `access_people`
  ADD PRIMARY KEY (`id`),
  ADD KEY `people_id` (`people_id`),
  ADD KEY `access_state_id` (`access_state_id`),
  ADD KEY `main_access_id` (`main_access_id`);

--
-- Indices de la tabla `access_people_answers`
--
ALTER TABLE `access_people_answers`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `access_people_areas`
--
ALTER TABLE `access_people_areas`
  ADD PRIMARY KEY (`access_people_id`,`areas_id`),
  ADD KEY `areas_id` (`areas_id`);

--
-- Indices de la tabla `access_people_department`
--
ALTER TABLE `access_people_department`
  ADD PRIMARY KEY (`access_people_id`,`departments_id`),
  ADD KEY `departments_id` (`departments_id`);

--
-- Indices de la tabla `access_people_forms`
--
ALTER TABLE `access_people_forms`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `access_people_intents`
--
ALTER TABLE `access_people_intents`
  ADD KEY `doors_id` (`doors_id`);

--
-- Indices de la tabla `access_people_reasons_visit`
--
ALTER TABLE `access_people_reasons_visit`
  ADD PRIMARY KEY (`access_people_id`,`reasons_visit_id`),
  ADD KEY `reasons_visit_id` (`reasons_visit_id`);

--
-- Indices de la tabla `access_people_route`
--
ALTER TABLE `access_people_route`
  ADD PRIMARY KEY (`access_people_id`,`doors_id`),
  ADD KEY `doors_id` (`doors_id`);

--
-- Indices de la tabla `access_people_state_history`
--
ALTER TABLE `access_people_state_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `access_state_id` (`access_state_id`);

--
-- Indices de la tabla `access_people_visit`
--
ALTER TABLE `access_people_visit`
  ADD PRIMARY KEY (`access_people_id`,`people_id`),
  ADD KEY `people_id` (`people_id`);

--
-- Indices de la tabla `access_people_zones`
--
ALTER TABLE `access_people_zones`
  ADD PRIMARY KEY (`access_people_id`,`zones_id`),
  ADD KEY `zones_id` (`zones_id`);

--
-- Indices de la tabla `access_state`
--
ALTER TABLE `access_state`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `state` (`state`);

--
-- Indices de la tabla `answers_type`
--
ALTER TABLE `answers_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `type` (`type`);

--
-- Indices de la tabla `areas`
--
ALTER TABLE `areas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `area` (`area`),
  ADD KEY `zones_id` (`zones_id`);

--
-- Indices de la tabla `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `current_internal_state`
--
ALTER TABLE `current_internal_state`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `department` (`department`),
  ADD KEY `areas_id` (`areas_id`),
  ADD KEY `in_charge` (`in_charge`);

--
-- Indices de la tabla `doors`
--
ALTER TABLE `doors`
  ADD PRIMARY KEY (`id`),
  ADD KEY `doors_type_id` (`doors_type_id`);

--
-- Indices de la tabla `doors_areas`
--
ALTER TABLE `doors_areas`
  ADD PRIMARY KEY (`doors_id`,`areas_id`),
  ADD KEY `areas_id` (`areas_id`);

--
-- Indices de la tabla `doors_departments`
--
ALTER TABLE `doors_departments`
  ADD PRIMARY KEY (`doors_id`,`departments_id`),
  ADD KEY `departments_id` (`departments_id`);

--
-- Indices de la tabla `doors_parents`
--
ALTER TABLE `doors_parents`
  ADD PRIMARY KEY (`doors_id`,`parent`),
  ADD KEY `parent` (`parent`);

--
-- Indices de la tabla `doors_type`
--
ALTER TABLE `doors_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `type` (`type`);

--
-- Indices de la tabla `doors_zones`
--
ALTER TABLE `doors_zones`
  ADD PRIMARY KEY (`doors_id`,`zones_id`),
  ADD KEY `zones_id` (`zones_id`);

--
-- Indices de la tabla `forms`
--
ALTER TABLE `forms`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `forms_detail`
--
ALTER TABLE `forms_detail`
  ADD PRIMARY KEY (`order`,`forms_id`),
  ADD KEY `forms_id` (`forms_id`),
  ADD KEY `answers_type_id` (`answers_type_id`),
  ADD KEY `measures_id` (`measures_id`);

--
-- Indices de la tabla `forms_season`
--
ALTER TABLE `forms_season`
  ADD PRIMARY KEY (`vechiles_type_id`,`forms_id`,`seasons_id`,`year`),
  ADD KEY `vehicles_type_id` (`vechiles_type_id`),
  ADD KEY `forms_id` (`forms_id`),
  ADD KEY `seasons_id` (`seasons_id`);

--
-- Indices de la tabla `internal_answers`
--
ALTER TABLE `internal_answers`
  ADD PRIMARY KEY (`internal_forms_id`,`order`);

--
-- Indices de la tabla `internal_forms`
--
ALTER TABLE `internal_forms`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `internal_people_errors`
--
ALTER TABLE `internal_people_errors`
  ADD PRIMARY KEY (`id`),
  ADD KEY `people_id` (`people_id`),
  ADD KEY `sensors_id` (`sensors_id`),
  ADD KEY `reasons_error_id` (`reasons_error_id`);

--
-- Indices de la tabla `internal_people_success`
--
ALTER TABLE `internal_people_success`
  ADD PRIMARY KEY (`id`),
  ADD KEY `people_id` (`people_id`),
  ADD KEY `sensors_id` (`sensors_id`);

--
-- Indices de la tabla `internal_vehicles_errors`
--
ALTER TABLE `internal_vehicles_errors`
  ADD PRIMARY KEY (`id`),
  ADD KEY `vehicles_id` (`vehicles_id`),
  ADD KEY `sensors_id` (`sensors_id`),
  ADD KEY `reasons_error_id` (`reasons_error_id`);

--
-- Indices de la tabla `internal_vehicles_success`
--
ALTER TABLE `internal_vehicles_success`
  ADD PRIMARY KEY (`id`),
  ADD KEY `vehicles_id` (`vehicles_id`),
  ADD KEY `sensors_id` (`sensors_id`);

--
-- Indices de la tabla `jornada`
--
ALTER TABLE `jornada`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `local_information`
--
ALTER TABLE `local_information`
  ADD PRIMARY KEY (`id`),
  ADD KEY `people_id` (`people_id`);

--
-- Indices de la tabla `main_access`
--
ALTER TABLE `main_access`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `measures`
--
ALTER TABLE `measures`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `measure` (`measure`);

--
-- Indices de la tabla `minimum_requirements`
--
ALTER TABLE `minimum_requirements`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `requirement` (`requirement`);

--
-- Indices de la tabla `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `doors_id` (`doors_id`),
  ADD KEY `sensors_id` (`sensors_id`);

--
-- Indices de la tabla `options`
--
ALTER TABLE `options`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `option` (`option`);

--
-- Indices de la tabla `options_roles`
--
ALTER TABLE `options_roles`
  ADD PRIMARY KEY (`options_id`,`roles_id`),
  ADD KEY `roles_id` (`roles_id`);

--
-- Indices de la tabla `people`
--
ALTER TABLE `people`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `rut` (`rut`),
  ADD KEY `profiles_people_id` (`people_profiles_id`),
  ADD KEY `companies_id` (`companies_id`);

--
-- Indices de la tabla `people_profiles`
--
ALTER TABLE `people_profiles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `profile` (`profile`);

--
-- Indices de la tabla `profiles_doors_schedules`
--
ALTER TABLE `profiles_doors_schedules`
  ADD PRIMARY KEY (`profiles_people_id`,`doors_id`,`time_init`,`time_end`),
  ADD KEY `profiles_people_id` (`profiles_people_id`),
  ADD KEY `doors_id` (`doors_id`),
  ADD KEY `jornada_id` (`jornada_id`);

--
-- Indices de la tabla `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`),
  ADD KEY `in_charge` (`in_charge`),
  ADD KEY `in_charge_installation` (`in_charge_installation`);

--
-- Indices de la tabla `projects_areas`
--
ALTER TABLE `projects_areas`
  ADD PRIMARY KEY (`projects_id`,`areas_id`),
  ADD KEY `areas_id` (`areas_id`);

--
-- Indices de la tabla `projects_departments`
--
ALTER TABLE `projects_departments`
  ADD PRIMARY KEY (`projects_id`,`departments_id`),
  ADD KEY `departments_id` (`departments_id`);

--
-- Indices de la tabla `projects_forms`
--
ALTER TABLE `projects_forms`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `projects_intents`
--
ALTER TABLE `projects_intents`
  ADD PRIMARY KEY (`projects_id`,`doors_id`,`created`),
  ADD KEY `doors_id` (`doors_id`);

--
-- Indices de la tabla `projects_minimum_requirements`
--
ALTER TABLE `projects_minimum_requirements`
  ADD PRIMARY KEY (`projects_id`,`minimum_requirements`),
  ADD KEY `minimum_requirements` (`minimum_requirements`);

--
-- Indices de la tabla `projects_people`
--
ALTER TABLE `projects_people`
  ADD PRIMARY KEY (`projects_id`,`people_id`),
  ADD KEY `people_id` (`people_id`);

--
-- Indices de la tabla `projects_route`
--
ALTER TABLE `projects_route`
  ADD PRIMARY KEY (`projects_id`,`doors_id`),
  ADD KEY `doors_id` (`doors_id`);

--
-- Indices de la tabla `projects_schedules`
--
ALTER TABLE `projects_schedules`
  ADD KEY `projects_id` (`projects_id`);

--
-- Indices de la tabla `projects_vehicles`
--
ALTER TABLE `projects_vehicles`
  ADD PRIMARY KEY (`projects_id`,`vehicles_id`),
  ADD KEY `vehicles_id` (`vehicles_id`);

--
-- Indices de la tabla `projects_zones`
--
ALTER TABLE `projects_zones`
  ADD PRIMARY KEY (`projects_id`,`zones_id`),
  ADD KEY `zones_id` (`zones_id`);

--
-- Indices de la tabla `reasons_error`
--
ALTER TABLE `reasons_error`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `reason` (`reason`);

--
-- Indices de la tabla `reasons_visit`
--
ALTER TABLE `reasons_visit`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `reason` (`reason`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `rol` (`rol`);

--
-- Indices de la tabla `seasons`
--
ALTER TABLE `seasons`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `season` (`season`);

--
-- Indices de la tabla `sensors`
--
ALTER TABLE `sensors`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sensors_type` (`sensors_type`);

--
-- Indices de la tabla `sensors_doors`
--
ALTER TABLE `sensors_doors`
  ADD PRIMARY KEY (`sensors_id`,`doors_id`),
  ADD KEY `doors_id` (`doors_id`);

--
-- Indices de la tabla `sensors_type`
--
ALTER TABLE `sensors_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `type` (`type`);

--
-- Indices de la tabla `special_schedule`
--
ALTER TABLE `special_schedule`
  ADD PRIMARY KEY (`id`),
  ADD KEY `people_id` (`people_id`),
  ADD KEY `doors_id` (`doors_id`);

--
-- Indices de la tabla `states`
--
ALTER TABLE `states`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user` (`user`),
  ADD KEY `people_id` (`people_id`),
  ADD KEY `roles_id` (`roles_id`),
  ADD KEY `users_state_id` (`users_state_id`);

--
-- Indices de la tabla `users_state`
--
ALTER TABLE `users_state`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `state` (`state`);

--
-- Indices de la tabla `vehicles`
--
ALTER TABLE `vehicles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `people_id` (`people_id`),
  ADD KEY `vehicles_type_id` (`vehicles_type_id`),
  ADD KEY `vehicles_profiles_id` (`vehicles_profiles_id`),
  ADD KEY `companies_id` (`companies_id`);

--
-- Indices de la tabla `vehicles_drivers`
--
ALTER TABLE `vehicles_drivers`
  ADD PRIMARY KEY (`vehicles_id`,`people_id`),
  ADD KEY `people_id` (`people_id`);

--
-- Indices de la tabla `vehicles_profiles`
--
ALTER TABLE `vehicles_profiles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `profile` (`profile`);

--
-- Indices de la tabla `vehicles_type`
--
ALTER TABLE `vehicles_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `type` (`type`);

--
-- Indices de la tabla `zones`
--
ALTER TABLE `zones`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `zone` (`zone`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `access_people`
--
ALTER TABLE `access_people`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT de la tabla `access_people_answers`
--
ALTER TABLE `access_people_answers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=171;

--
-- AUTO_INCREMENT de la tabla `access_people_forms`
--
ALTER TABLE `access_people_forms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=90;

--
-- AUTO_INCREMENT de la tabla `access_people_state_history`
--
ALTER TABLE `access_people_state_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT de la tabla `access_state`
--
ALTER TABLE `access_state`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `answers_type`
--
ALTER TABLE `answers_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `areas`
--
ALTER TABLE `areas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `companies`
--
ALTER TABLE `companies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `current_internal_state`
--
ALTER TABLE `current_internal_state`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `doors`
--
ALTER TABLE `doors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `doors_type`
--
ALTER TABLE `doors_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `forms`
--
ALTER TABLE `forms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;

--
-- AUTO_INCREMENT de la tabla `internal_forms`
--
ALTER TABLE `internal_forms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `internal_people_errors`
--
ALTER TABLE `internal_people_errors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT de la tabla `internal_people_success`
--
ALTER TABLE `internal_people_success`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=79;

--
-- AUTO_INCREMENT de la tabla `internal_vehicles_errors`
--
ALTER TABLE `internal_vehicles_errors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `internal_vehicles_success`
--
ALTER TABLE `internal_vehicles_success`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `jornada`
--
ALTER TABLE `jornada`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `local_information`
--
ALTER TABLE `local_information`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `main_access`
--
ALTER TABLE `main_access`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `measures`
--
ALTER TABLE `measures`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `minimum_requirements`
--
ALTER TABLE `minimum_requirements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT de la tabla `options`
--
ALTER TABLE `options`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `people`
--
ALTER TABLE `people`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT de la tabla `people_profiles`
--
ALTER TABLE `people_profiles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `projects`
--
ALTER TABLE `projects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT de la tabla `projects_forms`
--
ALTER TABLE `projects_forms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `reasons_error`
--
ALTER TABLE `reasons_error`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `reasons_visit`
--
ALTER TABLE `reasons_visit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `seasons`
--
ALTER TABLE `seasons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `sensors`
--
ALTER TABLE `sensors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `sensors_type`
--
ALTER TABLE `sensors_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `special_schedule`
--
ALTER TABLE `special_schedule`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `states`
--
ALTER TABLE `states`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `users_state`
--
ALTER TABLE `users_state`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `vehicles`
--
ALTER TABLE `vehicles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT de la tabla `vehicles_profiles`
--
ALTER TABLE `vehicles_profiles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `vehicles_type`
--
ALTER TABLE `vehicles_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `zones`
--
ALTER TABLE `zones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
