/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.5.5-10.1.26-MariaDB : Database - access_control_3
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`access_control_3` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci */;

USE `access_control_3`;

/*Table structure for table `access_people` */

DROP TABLE IF EXISTS `access_people`;

CREATE TABLE `access_people` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entry` tinyint(1) NOT NULL,
  `exit_time` datetime DEFAULT NULL,
  `hours` tinyint(2) DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `people_id` int(11) NOT NULL,
  `access_state_id` int(11) NOT NULL,
  `main_access_id` int(11) DEFAULT NULL,
  `approved_by` int(11) DEFAULT NULL,
  `observation` text COLLATE utf8mb4_unicode_ci,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `people_id` (`people_id`),
  KEY `access_state_id` (`access_state_id`),
  KEY `main_access_id` (`main_access_id`),
  CONSTRAINT `access_people_ibfk_1` FOREIGN KEY (`people_id`) REFERENCES `people` (`id`),
  CONSTRAINT `access_people_ibfk_2` FOREIGN KEY (`access_state_id`) REFERENCES `access_state` (`id`),
  CONSTRAINT `access_people_ibfk_3` FOREIGN KEY (`main_access_id`) REFERENCES `main_access` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `access_people` */

insert  into `access_people`(`id`,`entry`,`exit_time`,`hours`,`end_time`,`people_id`,`access_state_id`,`main_access_id`,`approved_by`,`observation`,`created`,`modified`) values (3,1,'2018-03-12 16:20:08',2,'2018-03-12 18:18:29',33,2,2,1,'','2018-03-12 16:17:50','2018-03-12 16:18:29'),(4,1,'2018-03-12 16:26:44',2,'2018-03-12 18:23:48',2,2,2,1,'','2018-03-12 16:23:41','2018-03-12 16:23:48'),(5,1,'2018-03-12 16:26:48',2,'2018-03-12 18:23:50',33,2,2,1,'','2018-03-12 16:23:41','2018-03-12 16:23:50'),(6,1,'2018-03-12 16:26:51',2,'2018-03-12 18:23:52',37,2,2,1,'','2018-03-12 16:23:41','2018-03-12 16:23:52'),(7,1,'2018-03-12 16:58:18',5,'2018-03-12 21:58:13',43,2,2,1,'','2018-03-12 16:58:04','2018-03-12 16:58:13'),(8,1,'2018-03-12 17:00:27',2,'2018-03-12 19:00:09',43,2,2,1,'','2018-03-12 17:00:03','2018-03-12 17:00:09'),(9,1,'2018-03-13 15:23:45',2,'2018-03-13 17:23:27',2,2,2,1,'','2018-03-13 15:22:55','2018-03-13 15:23:27');

/*Table structure for table `access_people_areas` */

DROP TABLE IF EXISTS `access_people_areas`;

CREATE TABLE `access_people_areas` (
  `access_people_id` int(11) NOT NULL,
  `areas_id` int(11) NOT NULL,
  PRIMARY KEY (`access_people_id`,`areas_id`),
  KEY `areas_id` (`areas_id`),
  CONSTRAINT `access_people_areas_ibfk_1` FOREIGN KEY (`access_people_id`) REFERENCES `access_people` (`id`),
  CONSTRAINT `access_people_areas_ibfk_2` FOREIGN KEY (`areas_id`) REFERENCES `areas` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `access_people_areas` */

insert  into `access_people_areas`(`access_people_id`,`areas_id`) values (3,1),(3,2),(7,1),(7,2),(9,1);

/*Table structure for table `access_people_department` */

DROP TABLE IF EXISTS `access_people_department`;

CREATE TABLE `access_people_department` (
  `access_people_id` int(11) NOT NULL,
  `departments_id` int(11) NOT NULL,
  PRIMARY KEY (`access_people_id`,`departments_id`),
  KEY `departments_id` (`departments_id`),
  CONSTRAINT `access_people_department_ibfk_1` FOREIGN KEY (`access_people_id`) REFERENCES `access_people` (`id`),
  CONSTRAINT `access_people_department_ibfk_2` FOREIGN KEY (`departments_id`) REFERENCES `departments` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `access_people_department` */

insert  into `access_people_department`(`access_people_id`,`departments_id`) values (3,3),(3,5),(7,3),(7,5),(9,3),(9,5),(9,6);

/*Table structure for table `access_people_intents` */

DROP TABLE IF EXISTS `access_people_intents`;

CREATE TABLE `access_people_intents` (
  `access_people_id` int(11) DEFAULT NULL,
  `doors_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  KEY `access_people_id` (`access_people_id`),
  KEY `doors_id` (`doors_id`),
  CONSTRAINT `access_people_intents_ibfk_1` FOREIGN KEY (`access_people_id`) REFERENCES `access_people` (`id`),
  CONSTRAINT `access_people_intents_ibfk_2` FOREIGN KEY (`doors_id`) REFERENCES `doors` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `access_people_intents` */

/*Table structure for table `access_people_reasons_visit` */

DROP TABLE IF EXISTS `access_people_reasons_visit`;

CREATE TABLE `access_people_reasons_visit` (
  `access_people_id` int(11) NOT NULL,
  `reasons_visit_id` int(11) NOT NULL,
  PRIMARY KEY (`access_people_id`,`reasons_visit_id`),
  KEY `reasons_visit_id` (`reasons_visit_id`),
  CONSTRAINT `access_people_reasons_visit_ibfk_1` FOREIGN KEY (`access_people_id`) REFERENCES `access_people` (`id`),
  CONSTRAINT `access_people_reasons_visit_ibfk_2` FOREIGN KEY (`reasons_visit_id`) REFERENCES `reasons_visit` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `access_people_reasons_visit` */

insert  into `access_people_reasons_visit`(`access_people_id`,`reasons_visit_id`) values (3,3),(4,3),(7,3),(8,3),(9,3);

/*Table structure for table `access_people_route` */

DROP TABLE IF EXISTS `access_people_route`;

CREATE TABLE `access_people_route` (
  `access_people_id` int(11) NOT NULL,
  `doors_id` int(11) NOT NULL,
  PRIMARY KEY (`access_people_id`,`doors_id`),
  KEY `doors_id` (`doors_id`),
  CONSTRAINT `access_people_route_ibfk_1` FOREIGN KEY (`access_people_id`) REFERENCES `access_people` (`id`),
  CONSTRAINT `access_people_route_ibfk_2` FOREIGN KEY (`doors_id`) REFERENCES `doors` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `access_people_route` */

insert  into `access_people_route`(`access_people_id`,`doors_id`) values (3,1),(3,5),(4,5),(7,1),(7,5),(8,1),(8,5),(9,1),(9,5);

/*Table structure for table `access_people_state_history` */

DROP TABLE IF EXISTS `access_people_state_history`;

CREATE TABLE `access_people_state_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `access_people_id` int(11) DEFAULT NULL,
  `access_state_id` int(11) DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `access_people_id` (`access_people_id`),
  KEY `access_state_id` (`access_state_id`),
  CONSTRAINT `access_people_state_history_ibfk_1` FOREIGN KEY (`access_people_id`) REFERENCES `access_people` (`id`),
  CONSTRAINT `access_people_state_history_ibfk_2` FOREIGN KEY (`access_state_id`) REFERENCES `access_state` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `access_people_state_history` */

insert  into `access_people_state_history`(`id`,`access_people_id`,`access_state_id`,`description`,`created`) values (1,3,1,'USUARIO USUARIO','2018-03-12 16:17:50'),(2,3,2,'TEXTO A CONVENIR','2018-03-12 16:18:29'),(3,4,1,'USUARIO USUARIO','2018-03-12 16:23:41'),(4,4,2,'TEXTO A CONVENIR','2018-03-12 16:23:48'),(5,5,2,'TEXTO A CONVENIR','2018-03-12 16:23:50'),(6,6,2,'TEXTO A CONVENIR','2018-03-12 16:23:52'),(7,7,1,'USUARIO USUARIO','2018-03-12 16:58:04'),(8,7,2,'TEXTO A CONVENIR','2018-03-12 16:58:13'),(9,8,1,'USUARIO USUARIO','2018-03-12 17:00:03'),(10,8,2,'TEXTO A CONVENIR','2018-03-12 17:00:09'),(11,9,1,'USUARIO USUARIO','2018-03-13 15:22:56'),(12,9,2,'TEXTO A CONVENIR','2018-03-13 15:23:27');

/*Table structure for table `access_people_visit` */

DROP TABLE IF EXISTS `access_people_visit`;

CREATE TABLE `access_people_visit` (
  `access_people_id` int(11) NOT NULL,
  `people_id` int(11) NOT NULL,
  PRIMARY KEY (`access_people_id`,`people_id`),
  KEY `people_id` (`people_id`),
  CONSTRAINT `access_people_visit_ibfk_1` FOREIGN KEY (`access_people_id`) REFERENCES `access_people` (`id`),
  CONSTRAINT `access_people_visit_ibfk_2` FOREIGN KEY (`people_id`) REFERENCES `people` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `access_people_visit` */

insert  into `access_people_visit`(`access_people_id`,`people_id`) values (3,6),(3,25),(4,25),(7,25),(8,25),(9,25);

/*Table structure for table `access_people_zones` */

DROP TABLE IF EXISTS `access_people_zones`;

CREATE TABLE `access_people_zones` (
  `access_people_id` int(11) NOT NULL,
  `zones_id` int(11) NOT NULL,
  PRIMARY KEY (`access_people_id`,`zones_id`),
  KEY `zones_id` (`zones_id`),
  CONSTRAINT `access_people_zones_ibfk_1` FOREIGN KEY (`access_people_id`) REFERENCES `access_people` (`id`),
  CONSTRAINT `access_people_zones_ibfk_2` FOREIGN KEY (`zones_id`) REFERENCES `zones` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `access_people_zones` */

insert  into `access_people_zones`(`access_people_id`,`zones_id`) values (3,1),(7,1),(9,1),(9,2);

/*Table structure for table `access_state` */

DROP TABLE IF EXISTS `access_state`;

CREATE TABLE `access_state` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `state` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `state` (`state`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `access_state` */

insert  into `access_state`(`id`,`state`,`created`,`modified`) values (1,'Pendiente','2018-02-18 02:37:46','2018-02-18 02:40:59'),(2,'Permitido','2018-02-18 02:37:46','2018-02-18 02:37:46'),(3,'Rechazado','2018-02-18 02:37:46','2018-02-18 02:37:46');

/*Table structure for table `access_vehicles` */

DROP TABLE IF EXISTS `access_vehicles`;

CREATE TABLE `access_vehicles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entry` tinyint(1) NOT NULL,
  `exit_time` datetime DEFAULT NULL,
  `hours` tinyint(2) NOT NULL,
  `end_time` datetime NOT NULL,
  `vehicles_id` int(11) NOT NULL,
  `access_state_id` int(11) NOT NULL,
  `main_access_id` int(11) NOT NULL,
  `approved_by` int(11) DEFAULT NULL,
  `observation` text COLLATE utf8mb4_unicode_ci,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `vehicles_id` (`vehicles_id`),
  KEY `access_state_id` (`access_state_id`),
  KEY `main_access_id` (`main_access_id`),
  CONSTRAINT `access_vehicles_ibfk_1` FOREIGN KEY (`vehicles_id`) REFERENCES `vehicles` (`id`),
  CONSTRAINT `access_vehicles_ibfk_2` FOREIGN KEY (`access_state_id`) REFERENCES `access_state` (`id`),
  CONSTRAINT `access_vehicles_ibfk_3` FOREIGN KEY (`main_access_id`) REFERENCES `main_access` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `access_vehicles` */

/*Table structure for table `access_vehicles_answers` */

DROP TABLE IF EXISTS `access_vehicles_answers`;

CREATE TABLE `access_vehicles_answers` (
  `access_vehicles_id` int(11) NOT NULL,
  `forms_id` int(11) NOT NULL,
  `question` int(11) NOT NULL,
  `answer` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`access_vehicles_id`,`forms_id`,`question`),
  KEY `forms_id` (`forms_id`),
  CONSTRAINT `access_vehicles_answers_ibfk_1` FOREIGN KEY (`access_vehicles_id`) REFERENCES `access_vehicles` (`id`),
  CONSTRAINT `access_vehicles_answers_ibfk_2` FOREIGN KEY (`forms_id`) REFERENCES `forms` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `access_vehicles_answers` */

/*Table structure for table `access_vehicles_areas` */

DROP TABLE IF EXISTS `access_vehicles_areas`;

CREATE TABLE `access_vehicles_areas` (
  `access_vehicles_id` int(11) NOT NULL,
  `areas_id` int(11) NOT NULL,
  PRIMARY KEY (`access_vehicles_id`,`areas_id`),
  KEY `areas_id` (`areas_id`),
  CONSTRAINT `access_vehicles_areas_ibfk_1` FOREIGN KEY (`access_vehicles_id`) REFERENCES `access_vehicles` (`id`),
  CONSTRAINT `access_vehicles_areas_ibfk_2` FOREIGN KEY (`areas_id`) REFERENCES `areas` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `access_vehicles_areas` */

/*Table structure for table `access_vehicles_departments` */

DROP TABLE IF EXISTS `access_vehicles_departments`;

CREATE TABLE `access_vehicles_departments` (
  `access_vehicles_id` int(11) NOT NULL,
  `departments_id` int(11) NOT NULL,
  PRIMARY KEY (`access_vehicles_id`,`departments_id`),
  KEY `departments_id` (`departments_id`),
  CONSTRAINT `access_vehicles_departments_ibfk_1` FOREIGN KEY (`access_vehicles_id`) REFERENCES `access_vehicles` (`id`),
  CONSTRAINT `access_vehicles_departments_ibfk_2` FOREIGN KEY (`departments_id`) REFERENCES `departments` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `access_vehicles_departments` */

/*Table structure for table `access_vehicles_intents` */

DROP TABLE IF EXISTS `access_vehicles_intents`;

CREATE TABLE `access_vehicles_intents` (
  `access_vehicles_id` int(11) DEFAULT NULL,
  `doors_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  KEY `access_vehicles_id` (`access_vehicles_id`),
  KEY `doors_id` (`doors_id`),
  CONSTRAINT `access_vehicles_intents_ibfk_1` FOREIGN KEY (`access_vehicles_id`) REFERENCES `access_vehicles` (`id`),
  CONSTRAINT `access_vehicles_intents_ibfk_2` FOREIGN KEY (`doors_id`) REFERENCES `doors` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `access_vehicles_intents` */

/*Table structure for table `access_vehicles_reasons_visit` */

DROP TABLE IF EXISTS `access_vehicles_reasons_visit`;

CREATE TABLE `access_vehicles_reasons_visit` (
  `access_vehicles_id` int(11) NOT NULL,
  `reasons_visit_id` int(11) NOT NULL,
  PRIMARY KEY (`access_vehicles_id`,`reasons_visit_id`),
  KEY `reasons_visit_id` (`reasons_visit_id`),
  CONSTRAINT `access_vehicles_reasons_visit_ibfk_1` FOREIGN KEY (`access_vehicles_id`) REFERENCES `access_vehicles` (`id`),
  CONSTRAINT `access_vehicles_reasons_visit_ibfk_2` FOREIGN KEY (`reasons_visit_id`) REFERENCES `reasons_visit` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `access_vehicles_reasons_visit` */

/*Table structure for table `access_vehicles_route` */

DROP TABLE IF EXISTS `access_vehicles_route`;

CREATE TABLE `access_vehicles_route` (
  `access_vehicles_id` int(11) NOT NULL,
  `doors_id` int(11) NOT NULL,
  PRIMARY KEY (`access_vehicles_id`,`doors_id`),
  KEY `doors_id` (`doors_id`),
  CONSTRAINT `access_vehicles_route_ibfk_1` FOREIGN KEY (`access_vehicles_id`) REFERENCES `access_vehicles` (`id`),
  CONSTRAINT `access_vehicles_route_ibfk_2` FOREIGN KEY (`doors_id`) REFERENCES `doors` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `access_vehicles_route` */

/*Table structure for table `access_vehicles_state_history` */

DROP TABLE IF EXISTS `access_vehicles_state_history`;

CREATE TABLE `access_vehicles_state_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `access_vehicles_id` int(11) DEFAULT NULL,
  `access_state_id` int(11) DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `access_vehicles_id` (`access_vehicles_id`),
  KEY `access_state_id` (`access_state_id`),
  CONSTRAINT `access_vehicles_state_history_ibfk_1` FOREIGN KEY (`access_vehicles_id`) REFERENCES `access_vehicles` (`id`),
  CONSTRAINT `access_vehicles_state_history_ibfk_2` FOREIGN KEY (`access_state_id`) REFERENCES `access_state` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `access_vehicles_state_history` */

/*Table structure for table `access_vehicles_visit` */

DROP TABLE IF EXISTS `access_vehicles_visit`;

CREATE TABLE `access_vehicles_visit` (
  `access_vehicles_id` int(11) NOT NULL,
  `people_id` int(11) NOT NULL,
  PRIMARY KEY (`access_vehicles_id`,`people_id`),
  KEY `people_id` (`people_id`),
  CONSTRAINT `access_vehicles_visit_ibfk_1` FOREIGN KEY (`access_vehicles_id`) REFERENCES `access_vehicles` (`id`),
  CONSTRAINT `access_vehicles_visit_ibfk_2` FOREIGN KEY (`people_id`) REFERENCES `people` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `access_vehicles_visit` */

/*Table structure for table `access_vehicles_zones` */

DROP TABLE IF EXISTS `access_vehicles_zones`;

CREATE TABLE `access_vehicles_zones` (
  `access_vehicles_id` int(11) NOT NULL,
  `zones_id` int(11) NOT NULL,
  PRIMARY KEY (`access_vehicles_id`,`zones_id`),
  KEY `zones_id` (`zones_id`),
  CONSTRAINT `access_vehicles_zones_ibfk_1` FOREIGN KEY (`access_vehicles_id`) REFERENCES `access_vehicles` (`id`),
  CONSTRAINT `access_vehicles_zones_ibfk_2` FOREIGN KEY (`zones_id`) REFERENCES `zones` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `access_vehicles_zones` */

/*Table structure for table `answers_type` */

DROP TABLE IF EXISTS `answers_type`;

CREATE TABLE `answers_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `type` (`type`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `answers_type` */

insert  into `answers_type`(`id`,`type`,`created`,`modified`) values (1,'Respuesta corta','2018-02-14 04:42:23','2018-02-14 11:33:59'),(3,'Párrafo','2018-02-14 11:34:17','2018-02-14 11:34:17'),(4,'Cantidad','2018-02-14 11:34:28','2018-02-14 11:34:28'),(5,'Fecha','2018-02-14 11:34:42','2018-02-14 11:34:42'),(6,'Binaria','2018-02-14 11:34:53','2018-02-14 11:34:53');

/*Table structure for table `areas` */

DROP TABLE IF EXISTS `areas`;

CREATE TABLE `areas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `area` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `zones_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `area` (`area`),
  KEY `zones_id` (`zones_id`),
  CONSTRAINT `areas_ibfk_1` FOREIGN KEY (`zones_id`) REFERENCES `zones` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `areas` */

insert  into `areas`(`id`,`area`,`zones_id`,`created`,`modified`) values (1,'Área 1',1,'2018-02-09 08:42:23','2018-02-09 09:12:13'),(2,'Área 2',1,'2018-02-09 10:59:51','2018-02-09 10:59:51'),(3,'Área 3',2,'2018-02-10 02:59:25','2018-02-10 02:59:25'),(4,'Área 4',3,'2018-02-10 02:59:33','2018-02-10 02:59:33'),(5,'Área 5',3,'2018-02-10 02:59:42','2018-02-10 02:59:42'),(6,'Área 6',5,'2018-02-10 02:59:56','2018-02-10 02:59:56'),(7,'Área 7',7,'2018-02-10 03:00:03','2018-02-10 03:00:03'),(8,'c1',2,'2018-02-10 03:27:39','2018-02-10 03:27:39');

/*Table structure for table `companies` */

DROP TABLE IF EXISTS `companies`;

CREATE TABLE `companies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `companies` */

insert  into `companies`(`id`,`company`,`address`,`phone`,`email`,`contact`,`created`,`modified`) values (1,'VISITA','VISITA','123456','visita@gmail.com','VISITA','2018-02-08 11:47:48','2018-03-08 03:13:43'),(2,'EMPRESA CONTRATISTA','Villa Pedro Nolasco Calle C #973','945330884','aliro.ramirez02@inacapmail.cl','CONTRATISTA','2018-02-08 11:48:25','2018-03-08 03:14:02'),(3,'INTERNO','Villa Pedro Nolasco Calle C #973','945330884','aliro.ramirez02@inacapmail.cl','INTERNO','2018-02-09 12:56:09','2018-03-08 03:14:11');

/*Table structure for table `configurations` */

DROP TABLE IF EXISTS `configurations`;

CREATE TABLE `configurations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cod_company` int(11) NOT NULL,
  `company` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cod_installation` int(11) NOT NULL,
  `installation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `people_id` int(11) NOT NULL,
  `version` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `people_id` (`people_id`),
  CONSTRAINT `configurations_ibfk_1` FOREIGN KEY (`people_id`) REFERENCES `people` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `configurations` */

insert  into `configurations`(`id`,`cod_company`,`company`,`cod_installation`,`installation`,`address`,`email`,`phone`,`people_id`,`version`,`created`,`modified`) values (1,123456,'gggg',12,'cco','casa','a@q.cl','121212',2,'1.0.0','2018-02-17 10:48:04','2018-02-17 10:53:08');

/*Table structure for table `departments` */

DROP TABLE IF EXISTS `departments`;

CREATE TABLE `departments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `department` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `areas_id` int(11) NOT NULL,
  `in_charge` int(11) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `department` (`department`),
  KEY `areas_id` (`areas_id`),
  KEY `in_charge` (`in_charge`),
  CONSTRAINT `departments_ibfk_1` FOREIGN KEY (`areas_id`) REFERENCES `areas` (`id`),
  CONSTRAINT `departments_ibfk_2` FOREIGN KEY (`in_charge`) REFERENCES `people` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `departments` */

insert  into `departments`(`id`,`department`,`areas_id`,`in_charge`,`created`,`modified`) values (3,'Depto 3',1,3,'2018-02-10 02:51:09','2018-02-10 04:12:09'),(5,'Depto 1',2,3,'2018-02-10 03:43:45','2018-02-10 03:43:45'),(6,'Depto 4',7,3,'2018-02-10 03:29:39','2018-02-10 03:29:39'),(7,'Depto 5',6,3,'2018-02-10 03:29:47','2018-02-10 03:29:47'),(8,'Depto 6',5,3,'2018-02-10 03:29:54','2018-02-10 03:29:54'),(9,'Depto 7',3,3,'2018-02-10 03:30:03','2018-02-10 03:30:03');

/*Table structure for table `departments_people` */

DROP TABLE IF EXISTS `departments_people`;

CREATE TABLE `departments_people` (
  `departments_id` int(11) NOT NULL,
  `people_id` int(11) NOT NULL,
  PRIMARY KEY (`departments_id`,`people_id`),
  KEY `people_id` (`people_id`),
  CONSTRAINT `departments_people_ibfk_1` FOREIGN KEY (`departments_id`) REFERENCES `departments` (`id`),
  CONSTRAINT `departments_people_ibfk_2` FOREIGN KEY (`people_id`) REFERENCES `people` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `departments_people` */

/*Table structure for table `doors` */

DROP TABLE IF EXISTS `doors`;

CREATE TABLE `doors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `door` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `level` tinyint(3) NOT NULL,
  `doors_type_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `doors_type_id` (`doors_type_id`),
  CONSTRAINT `doors_ibfk_1` FOREIGN KEY (`doors_type_id`) REFERENCES `doors_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `doors` */

insert  into `doors`(`id`,`door`,`description`,`level`,`doors_type_id`,`created`,`modified`) values (1,'Portería 1','Puerta portería acceso sur',0,4,'2018-02-09 04:15:42','2018-02-09 06:21:53'),(2,'Portería 2','Puerta portería acceso norte',0,5,'2018-02-09 04:47:28','2018-02-09 04:47:28'),(3,'Portería 3','Puerta portería acceso suroriente',0,4,'2018-02-09 05:17:33','2018-02-09 05:17:33'),(4,'Portería 4','.',1,4,'2018-02-10 03:26:04','2018-03-02 02:56:23'),(5,'Portería 5','.',1,5,'2018-02-10 03:26:11','2018-02-18 09:59:33'),(6,'Portería 6','',0,4,'2018-02-10 03:26:17','2018-02-10 03:26:17'),(7,'Portería 7','',0,5,'2018-02-10 03:26:23','2018-02-10 03:26:23'),(8,'D1','',0,4,'2018-02-10 03:28:47','2018-02-10 03:28:47');

/*Table structure for table `doors_areas` */

DROP TABLE IF EXISTS `doors_areas`;

CREATE TABLE `doors_areas` (
  `doors_id` int(11) NOT NULL,
  `areas_id` int(11) NOT NULL,
  PRIMARY KEY (`doors_id`,`areas_id`),
  KEY `areas_id` (`areas_id`),
  CONSTRAINT `doors_areas_ibfk_1` FOREIGN KEY (`doors_id`) REFERENCES `doors` (`id`),
  CONSTRAINT `doors_areas_ibfk_2` FOREIGN KEY (`areas_id`) REFERENCES `areas` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `doors_areas` */

insert  into `doors_areas`(`doors_id`,`areas_id`) values (1,1),(2,2),(3,3),(4,4),(5,5),(6,6),(7,7),(8,8);

/*Table structure for table `doors_departments` */

DROP TABLE IF EXISTS `doors_departments`;

CREATE TABLE `doors_departments` (
  `doors_id` int(11) NOT NULL,
  `departments_id` int(11) NOT NULL,
  PRIMARY KEY (`doors_id`,`departments_id`),
  KEY `departments_id` (`departments_id`),
  CONSTRAINT `doors_departments_ibfk_1` FOREIGN KEY (`doors_id`) REFERENCES `doors` (`id`),
  CONSTRAINT `doors_departments_ibfk_2` FOREIGN KEY (`departments_id`) REFERENCES `departments` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `doors_departments` */

insert  into `doors_departments`(`doors_id`,`departments_id`) values (1,9),(2,8),(3,7),(4,6),(5,3),(6,5);

/*Table structure for table `doors_parents` */

DROP TABLE IF EXISTS `doors_parents`;

CREATE TABLE `doors_parents` (
  `doors_id` int(11) NOT NULL,
  `parent` int(11) NOT NULL,
  PRIMARY KEY (`doors_id`,`parent`),
  CONSTRAINT `doors_parents_ibfk_1` FOREIGN KEY (`doors_id`) REFERENCES `doors` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `doors_parents` */

insert  into `doors_parents`(`doors_id`,`parent`) values (4,1),(4,2),(4,7),(5,1),(5,2),(5,3),(5,4),(5,6),(5,7),(5,8),(8,1);

/*Table structure for table `doors_type` */

DROP TABLE IF EXISTS `doors_type`;

CREATE TABLE `doors_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `type` (`type`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `doors_type` */

insert  into `doors_type`(`id`,`type`,`created`,`modified`) values (4,'Ingreso','2018-02-09 03:47:30','2018-02-28 08:16:27'),(5,'Salida','2018-02-09 03:47:38','2018-02-28 08:16:48'),(6,'Ingreso / salida','2018-02-10 03:49:15','2018-02-28 08:16:57');

/*Table structure for table `doors_zones` */

DROP TABLE IF EXISTS `doors_zones`;

CREATE TABLE `doors_zones` (
  `doors_id` int(11) NOT NULL,
  `zones_id` int(11) NOT NULL,
  PRIMARY KEY (`doors_id`,`zones_id`),
  KEY `zones_id` (`zones_id`),
  CONSTRAINT `doors_zones_ibfk_1` FOREIGN KEY (`doors_id`) REFERENCES `doors` (`id`),
  CONSTRAINT `doors_zones_ibfk_2` FOREIGN KEY (`zones_id`) REFERENCES `zones` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `doors_zones` */

insert  into `doors_zones`(`doors_id`,`zones_id`) values (1,1),(1,2),(2,1),(3,1),(4,4),(8,1);

/*Table structure for table `forms` */

DROP TABLE IF EXISTS `forms`;

CREATE TABLE `forms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `forms` */

insert  into `forms`(`id`,`title`,`description`,`created`,`modified`) values (32,'Formulario prueba','prueba crear, editar','2018-02-16 03:04:20','2018-02-25 01:27:52');

/*Table structure for table `forms_detail` */

DROP TABLE IF EXISTS `forms_detail`;

CREATE TABLE `forms_detail` (
  `order` int(11) NOT NULL,
  `question` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `placeholder` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `forms_id` int(11) NOT NULL,
  `answers_type_id` int(11) NOT NULL,
  `measures_id` int(11) DEFAULT '0',
  PRIMARY KEY (`order`,`forms_id`),
  KEY `forms_id` (`forms_id`),
  KEY `answers_type_id` (`answers_type_id`),
  CONSTRAINT `forms_detail_ibfk_1` FOREIGN KEY (`forms_id`) REFERENCES `forms` (`id`),
  CONSTRAINT `forms_detail_ibfk_2` FOREIGN KEY (`answers_type_id`) REFERENCES `answers_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `forms_detail` */

insert  into `forms_detail`(`order`,`question`,`placeholder`,`forms_id`,`answers_type_id`,`measures_id`) values (1,'Pregunta 1','Respuesta 1',32,1,0),(2,'Pregunta 2','Respuesta 2',32,3,0),(3,'Pregunta 3','Respuesta 3b',32,6,0),(4,'Pregunta 4','hoy',32,5,0),(5,'Pregunta 5','123',32,4,1);

/*Table structure for table `forms_season` */

DROP TABLE IF EXISTS `forms_season`;

CREATE TABLE `forms_season` (
  `vechiles_type_id` int(11) NOT NULL,
  `forms_id` int(11) NOT NULL,
  `seasons_id` int(11) NOT NULL,
  `year` int(4) NOT NULL,
  PRIMARY KEY (`vechiles_type_id`,`forms_id`,`seasons_id`,`year`),
  KEY `forms_id` (`forms_id`),
  KEY `seasons_id` (`seasons_id`),
  CONSTRAINT `forms_season_ibfk_1` FOREIGN KEY (`vechiles_type_id`) REFERENCES `vehicles_type` (`id`),
  CONSTRAINT `forms_season_ibfk_2` FOREIGN KEY (`forms_id`) REFERENCES `forms` (`id`),
  CONSTRAINT `forms_season_ibfk_3` FOREIGN KEY (`seasons_id`) REFERENCES `seasons` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `forms_season` */

insert  into `forms_season`(`vechiles_type_id`,`forms_id`,`seasons_id`,`year`) values (1,32,1,2018),(3,32,4,2019);

/*Table structure for table `internal_errors` */

DROP TABLE IF EXISTS `internal_errors`;

CREATE TABLE `internal_errors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `people_id` int(11) NOT NULL,
  `sensors_id` int(11) NOT NULL,
  `reasons_error_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `people_id` (`people_id`),
  KEY `sensors_id` (`sensors_id`),
  KEY `reasons_error_id` (`reasons_error_id`),
  CONSTRAINT `internal_errors_ibfk_1` FOREIGN KEY (`people_id`) REFERENCES `people` (`id`),
  CONSTRAINT `internal_errors_ibfk_2` FOREIGN KEY (`sensors_id`) REFERENCES `sensors` (`id`),
  CONSTRAINT `internal_errors_ibfk_3` FOREIGN KEY (`reasons_error_id`) REFERENCES `reasons_error` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `internal_errors` */

insert  into `internal_errors`(`id`,`people_id`,`sensors_id`,`reasons_error_id`,`created`) values (1,2,1,1,'2018-02-18 12:02:06');

/*Table structure for table `internal_success` */

DROP TABLE IF EXISTS `internal_success`;

CREATE TABLE `internal_success` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `people_id` int(11) NOT NULL,
  `sensors_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `people_id` (`people_id`),
  KEY `sensors_id` (`sensors_id`),
  CONSTRAINT `internal_success_ibfk_1` FOREIGN KEY (`people_id`) REFERENCES `people` (`id`),
  CONSTRAINT `internal_success_ibfk_2` FOREIGN KEY (`sensors_id`) REFERENCES `sensors` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `internal_success` */

insert  into `internal_success`(`id`,`people_id`,`sensors_id`,`created`) values (1,2,4,'2018-02-18 16:17:44'),(2,3,2,'2018-02-18 16:22:59');

/*Table structure for table `jornada` */

DROP TABLE IF EXISTS `jornada`;

CREATE TABLE `jornada` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jornada` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time_init` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time_end` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `jornada` */

insert  into `jornada`(`id`,`jornada`,`time_init`,`time_end`) values (2,'Jornada 1','08:00','18:00'),(3,'Jornada 2','20:00','08:00');

/*Table structure for table `main_access` */

DROP TABLE IF EXISTS `main_access`;

CREATE TABLE `main_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ubication` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ip_host` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_host` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `entry` tinyint(1) NOT NULL,
  `flow` tinyint(1) NOT NULL,
  `internal` tinyint(1) NOT NULL,
  `state` tinyint(1) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `main_access` */

insert  into `main_access`(`id`,`name`,`ubication`,`ip_host`,`name_host`,`entry`,`flow`,`internal`,`state`,`created`,`modified`) values (2,'Acceso principal','Teno','10.10.1.1','PAPC',2,0,1,1,'2018-02-11 05:09:39','2018-02-14 09:21:45'),(3,'Acceso secundario 1','Curicó','10.10.1.2','PASC1',2,2,2,1,'2018-02-11 05:11:45','2018-02-14 09:22:07'),(4,'Acceso secundario 2','Curicó','10.10.1.3','papap',0,2,0,1,'2018-02-11 05:13:57','2018-02-14 09:22:40'),(5,'Acceso principal3','Curicó','10.10.1.4','prprpp',1,1,1,1,'2018-02-11 05:14:54','2018-02-13 12:57:27'),(6,'Acceso principal 4','Curicó','10.10.1.5','papappapapa',1,1,1,0,'2018-02-11 05:17:00','2018-02-14 09:31:30'),(7,'gg','gg','192.168.0.10','PC-PORTERIA',2,0,0,1,'2018-03-02 04:33:12','2018-03-02 04:34:01'),(8,'bbb','bbb','192.168.0.12','PC-PORTERIA2',0,1,2,1,'2018-03-02 04:50:32','2018-03-02 04:50:32');

/*Table structure for table `measures` */

DROP TABLE IF EXISTS `measures`;

CREATE TABLE `measures` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `measure` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `acronimo` varchar(6) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `measure` (`measure`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `measures` */

insert  into `measures`(`id`,`measure`,`acronimo`,`created`,`modified`) values (1,'Kilos','Kgs','2018-02-14 23:26:17','2018-02-24 09:47:44'),(3,'Litros','Lts','2018-02-17 03:49:06','2018-02-17 03:49:06'),(4,'Gramos','Grs','2018-02-24 09:47:38','2018-02-24 09:47:38');

/*Table structure for table `options` */

DROP TABLE IF EXISTS `options`;

CREATE TABLE `options` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `option` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `option` (`option`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `options` */

insert  into `options`(`id`,`option`,`code`,`created`,`modified`) values (1,'Opción 1.','a123-k','2018-02-13 10:26:11','2018-02-13 10:31:32'),(4,'Opción 2.','a123-k','2018-02-13 10:26:11','2018-02-13 10:31:32'),(5,'Opción 3.','a123-k','2018-02-13 10:26:11','2018-02-13 10:31:32'),(6,'Opción 4.','a123-k','2018-02-13 10:26:11','2018-02-13 10:31:32'),(7,'Opción 5.','a123-k','2018-02-13 10:26:11','2018-02-13 10:31:32');

/*Table structure for table `options_roles` */

DROP TABLE IF EXISTS `options_roles`;

CREATE TABLE `options_roles` (
  `options_id` int(11) NOT NULL,
  `roles_id` int(11) NOT NULL,
  PRIMARY KEY (`options_id`,`roles_id`),
  KEY `roles_id` (`roles_id`),
  CONSTRAINT `options_roles_ibfk_1` FOREIGN KEY (`options_id`) REFERENCES `options` (`id`),
  CONSTRAINT `options_roles_ibfk_2` FOREIGN KEY (`roles_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `options_roles` */

insert  into `options_roles`(`options_id`,`roles_id`) values (1,2),(5,1),(6,1),(7,1);

/*Table structure for table `people` */

DROP TABLE IF EXISTS `people`;

CREATE TABLE `people` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rut` int(15) NOT NULL,
  `digit` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `internal` tinyint(1) DEFAULT '0',
  `allow_all` tinyint(1) DEFAULT NULL,
  `is_visited` tinyint(1) DEFAULT NULL,
  `people_profiles_id` int(11) NOT NULL,
  `companies_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `rut` (`rut`),
  KEY `profiles_people_id` (`people_profiles_id`),
  KEY `companies_id` (`companies_id`),
  CONSTRAINT `people_ibfk_1` FOREIGN KEY (`people_profiles_id`) REFERENCES `people_profiles` (`id`),
  CONSTRAINT `people_ibfk_2` FOREIGN KEY (`companies_id`) REFERENCES `companies` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `people` */

insert  into `people`(`id`,`rut`,`digit`,`name`,`last_name`,`address`,`email`,`phone`,`internal`,`allow_all`,`is_visited`,`people_profiles_id`,`companies_id`,`created`,`modified`) values (2,33333333,'3','JUAN','PEREZ','CALLE FELIZ','no_tiene@sinmail.cl','88776655',0,0,0,7,1,'2018-03-09 03:01:10','2018-03-09 03:01:10'),(3,11111111,'1','MARCELO','RIOS','MI CASA','a@a.cl','4',1,0,0,2,3,'2018-03-09 07:56:31','2018-03-09 07:56:31'),(4,22222222,'2','MATIAS','Quezada','chile','el_mts@hotmail.com','12345',1,0,0,2,3,'2018-03-09 07:56:36','2018-03-09 07:56:36'),(6,44444444,'4','BENITO','VALDES','CALLE 123','el_mts@hotmail.com','4444',1,0,1,2,3,'2018-03-02 04:55:07','2018-03-02 04:55:07'),(25,16274962,'5','Valentín','Cofré Villalobos','Rene León 80, Curicó','vcofre@mdsg.cl','956572335',1,0,1,2,3,'2018-03-12 15:28:32','2018-03-12 15:28:32'),(33,17795600,'7','MATIAS','QUEZADA SANHUEZA','MANUEL CORREA 1','el_mts@hotmail.com','12345678',0,0,0,7,1,'2018-03-08 09:05:35','2018-03-08 09:05:35'),(34,15811305,'8','JOSE','CARES ACEVEDO','MANUEL CORREA 1','el_mts@hotmail.com','12345678',0,0,0,7,1,'2018-03-08 09:09:00','2018-03-08 09:09:00'),(36,9133911,'0','MONICA','SANHUEZA CARRASCO','MANUEL CORREA 1','el_mts@hotmail.com','1234567',0,0,0,7,1,'2018-03-09 03:26:18','2018-03-09 03:26:18'),(37,99999999,'9','Roger','Feder','su casa','sumail@atp.cl','1',0,0,0,1,2,'2018-03-09 02:26:16','2018-03-09 02:26:16'),(39,7095436,'2','ENRIQUE','QUEZADA SANHUEZA','MANUEL CORREA 1','el_mts@hotmail.com','123456789',0,0,0,7,1,'2018-03-12 11:15:20','2018-03-12 11:15:20'),(42,88888888,'8','prueba','prueba','MANUEL CORREA 1','el_mts@hotmail.com','12345678',0,0,0,7,1,'2018-03-12 15:32:42','2018-03-12 15:32:42'),(43,12783148,'3','ROBERTO','TORO','POR AHI','el_mts@hotmail.com','1234567',0,0,0,6,2,'2018-03-12 16:57:24','2018-03-12 16:57:24');

/*Table structure for table `people_profiles` */

DROP TABLE IF EXISTS `people_profiles`;

CREATE TABLE `people_profiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `profile` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `profile` (`profile`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `people_profiles` */

insert  into `people_profiles`(`id`,`profile`,`created`,`modified`) values (1,'CHOFER','2018-02-10 19:31:33','2018-03-09 02:59:10'),(2,'ADMINISTRATIVO','2018-02-12 11:08:47','2018-03-09 02:59:42'),(3,'JORNAL','2018-02-12 11:10:14','2018-03-09 02:59:58'),(4,'OPERARIO','2018-02-12 11:10:23','2018-03-09 03:00:03'),(5,'GUARDIA','2018-02-12 11:10:31','2018-03-09 03:00:07'),(6,'CONTRATISTA','2018-03-08 03:14:49','2018-03-08 03:14:49'),(7,'VISITA','2018-03-08 03:14:57','2018-03-08 03:14:57'),(8,'SUPERVISOR','2018-03-09 03:00:29','2018-03-09 03:00:29');

/*Table structure for table `profiles_doors_schedules` */

DROP TABLE IF EXISTS `profiles_doors_schedules`;

CREATE TABLE `profiles_doors_schedules` (
  `profiles_people_id` int(11) NOT NULL,
  `doors_id` int(11) NOT NULL,
  `time_init` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time_end` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jornada_id` int(11) NOT NULL,
  PRIMARY KEY (`profiles_people_id`,`doors_id`,`time_init`,`time_end`),
  KEY `profiles_people_id` (`profiles_people_id`),
  KEY `doors_id` (`doors_id`),
  KEY `jornada_id` (`jornada_id`),
  CONSTRAINT `profiles_doors_schedules_ibfk_1` FOREIGN KEY (`profiles_people_id`) REFERENCES `people_profiles` (`id`),
  CONSTRAINT `profiles_doors_schedules_ibfk_2` FOREIGN KEY (`doors_id`) REFERENCES `doors` (`id`),
  CONSTRAINT `profiles_doors_schedules_ibfk_3` FOREIGN KEY (`jornada_id`) REFERENCES `jornada` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `profiles_doors_schedules` */

insert  into `profiles_doors_schedules`(`profiles_people_id`,`doors_id`,`time_init`,`time_end`,`jornada_id`) values (2,1,'08:00','18:00',2),(2,2,'08:00','18:00',2),(2,3,'08:00','18:00',2),(2,4,'08:00','18:00',2),(2,5,'08:00','18:00',2),(2,6,'08:00','18:00',2),(2,7,'08:00','18:00',2),(2,8,'08:00','18:00',2),(2,1,'20:00','08:00',3),(2,2,'20:00','08:00',3),(2,3,'20:00','08:00',3),(2,4,'20:00','08:00',3),(2,5,'20:00','08:00',3),(2,6,'20:00','08:00',3),(2,7,'20:00','08:00',3),(2,8,'20:00','08:00',3);

/*Table structure for table `projects` */

DROP TABLE IF EXISTS `projects`;

CREATE TABLE `projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `in_charge` int(11) DEFAULT NULL,
  `init` date DEFAULT NULL,
  `end` date DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `in_charge` (`in_charge`),
  CONSTRAINT `projects_ibfk_1` FOREIGN KEY (`in_charge`) REFERENCES `people` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `projects` */

/*Table structure for table `projects_areas` */

DROP TABLE IF EXISTS `projects_areas`;

CREATE TABLE `projects_areas` (
  `projects_id` int(11) NOT NULL,
  `areas_id` int(11) NOT NULL,
  PRIMARY KEY (`projects_id`,`areas_id`),
  KEY `areas_id` (`areas_id`),
  CONSTRAINT `projects_areas_ibfk_1` FOREIGN KEY (`projects_id`) REFERENCES `projects` (`id`),
  CONSTRAINT `projects_areas_ibfk_2` FOREIGN KEY (`areas_id`) REFERENCES `areas` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `projects_areas` */

/*Table structure for table `projects_departments` */

DROP TABLE IF EXISTS `projects_departments`;

CREATE TABLE `projects_departments` (
  `projects_id` int(11) NOT NULL,
  `departments_id` int(11) NOT NULL,
  PRIMARY KEY (`projects_id`,`departments_id`),
  KEY `departments_id` (`departments_id`),
  CONSTRAINT `projects_departments_ibfk_1` FOREIGN KEY (`projects_id`) REFERENCES `projects` (`id`),
  CONSTRAINT `projects_departments_ibfk_2` FOREIGN KEY (`departments_id`) REFERENCES `departments` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `projects_departments` */

/*Table structure for table `projects_people` */

DROP TABLE IF EXISTS `projects_people`;

CREATE TABLE `projects_people` (
  `projects_id` int(11) NOT NULL,
  `people_id` int(11) NOT NULL,
  PRIMARY KEY (`projects_id`,`people_id`),
  KEY `people_id` (`people_id`),
  CONSTRAINT `projects_people_ibfk_1` FOREIGN KEY (`projects_id`) REFERENCES `projects` (`id`),
  CONSTRAINT `projects_people_ibfk_2` FOREIGN KEY (`people_id`) REFERENCES `people` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `projects_people` */

/*Table structure for table `projects_route` */

DROP TABLE IF EXISTS `projects_route`;

CREATE TABLE `projects_route` (
  `projects_id` int(11) NOT NULL,
  `doors_id` int(11) NOT NULL,
  PRIMARY KEY (`projects_id`,`doors_id`),
  KEY `doors_id` (`doors_id`),
  CONSTRAINT `projects_route_ibfk_1` FOREIGN KEY (`projects_id`) REFERENCES `projects` (`id`),
  CONSTRAINT `projects_route_ibfk_2` FOREIGN KEY (`doors_id`) REFERENCES `doors` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `projects_route` */

/*Table structure for table `projects_schedules` */

DROP TABLE IF EXISTS `projects_schedules`;

CREATE TABLE `projects_schedules` (
  `projects_id` int(11) DEFAULT NULL,
  `time_init` time DEFAULT NULL,
  `time_end` time DEFAULT NULL,
  `L` tinyint(1) DEFAULT '0',
  `M` tinyint(1) DEFAULT '0',
  `Mi` tinyint(1) DEFAULT '0',
  `J` tinyint(1) DEFAULT '0',
  `V` tinyint(1) DEFAULT '0',
  `S` tinyint(1) DEFAULT '0',
  `D` tinyint(1) DEFAULT '0',
  KEY `projects_id` (`projects_id`),
  CONSTRAINT `projects_schedules_ibfk_1` FOREIGN KEY (`projects_id`) REFERENCES `projects` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `projects_schedules` */

/*Table structure for table `projects_zones` */

DROP TABLE IF EXISTS `projects_zones`;

CREATE TABLE `projects_zones` (
  `projects_id` int(11) NOT NULL,
  `zones_id` int(11) NOT NULL,
  PRIMARY KEY (`projects_id`,`zones_id`),
  KEY `zones_id` (`zones_id`),
  CONSTRAINT `projects_zones_ibfk_1` FOREIGN KEY (`projects_id`) REFERENCES `projects` (`id`),
  CONSTRAINT `projects_zones_ibfk_2` FOREIGN KEY (`zones_id`) REFERENCES `zones` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `projects_zones` */

/*Table structure for table `reasons_error` */

DROP TABLE IF EXISTS `reasons_error`;

CREATE TABLE `reasons_error` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reason` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `reason` (`reason`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `reasons_error` */

insert  into `reasons_error`(`id`,`reason`,`created`,`modified`) values (1,'Puerta no autorizada','2018-02-18 12:01:53','2018-02-18 12:01:53'),(3,'Fuera de horario permitido','2018-02-18 09:29:04','2018-02-18 09:29:04');

/*Table structure for table `reasons_visit` */

DROP TABLE IF EXISTS `reasons_visit`;

CREATE TABLE `reasons_visit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reason` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `reason` (`reason`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `reasons_visit` */

insert  into `reasons_visit`(`id`,`reason`,`created`,`modified`) values (1,'Despacho a puerto','2018-02-18 04:37:34','2018-02-18 04:38:54'),(2,'Retiro propietario','2018-02-18 04:39:57','2018-02-18 04:39:57'),(3,'Visita a personal interno','2018-03-02 13:20:35','2018-03-02 13:20:35');

/*Table structure for table `roles` */

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rol` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `rol` (`rol`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `roles` */

insert  into `roles`(`id`,`rol`,`created`,`modified`) values (1,'Administrador','2018-02-12 05:34:39','2018-02-12 05:34:39'),(2,'Funcionario','2018-02-12 05:35:00','2018-02-12 05:35:00'),(3,'Visitante','2018-02-12 05:35:11','2018-02-12 05:35:11'),(4,'Guardia','2018-02-12 05:35:22','2018-02-12 05:35:22'),(5,'Operario','2018-02-12 05:35:51','2018-02-12 05:35:51');

/*Table structure for table `seasons` */

DROP TABLE IF EXISTS `seasons`;

CREATE TABLE `seasons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `season` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `season` (`season`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `seasons` */

insert  into `seasons`(`id`,`season`,`created`,`modified`) values (1,'Temporada 20188','2018-02-12 03:21:23','2018-02-12 03:26:00'),(2,'Temporada 2019','2018-02-12 03:21:53','2018-02-12 03:21:53'),(3,'Temporada 2020','2018-02-12 03:22:05','2018-02-12 03:22:05'),(4,'Temporada 2018-2019','2018-02-12 03:22:17','2018-02-12 03:22:17'),(5,'Temporada 2019 - 2020','2018-02-12 03:22:29','2018-02-12 03:22:29');

/*Table structure for table `sensors` */

DROP TABLE IF EXISTS `sensors`;

CREATE TABLE `sensors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sensor` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ip` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sensors_type` int(11) NOT NULL,
  `entry` tinyint(1) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `sensors_type` (`sensors_type`),
  CONSTRAINT `sensors_ibfk_1` FOREIGN KEY (`sensors_type`) REFERENCES `sensors_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `sensors` */

insert  into `sensors`(`id`,`code`,`sensor`,`description`,`ip`,`sensors_type`,`entry`,`created`,`modified`) values (1,'KK1','Ultrasonico 1','..','10.10.10.10',2,1,'2018-02-12 03:37:54','2018-03-02 05:12:31'),(2,'KK2','Ultrasonico 2','prueba de edición','10.10.10.11',1,0,'2018-02-12 03:39:55','2018-03-02 04:26:27'),(3,'KK3','Ultrasonico 3','','10.10.10.12',2,0,'2018-02-12 03:40:25','2018-02-12 03:40:25'),(4,'KK4','Ultrasonico 4','rango 1 metro','10.10.10.13',2,0,'2018-02-12 03:41:29','2018-02-12 03:41:29'),(5,'KK5','Ultrasonico 5','rango 1 metro','10.10.10.14',1,2,'2018-02-12 03:41:57','2018-02-12 03:41:57'),(6,'123456','ssss','sss','61.158.163.39',2,0,'2018-03-02 04:30:45','2018-03-02 04:31:32'),(7,'tttt','tt','dd','127.0.0.1',2,1,'2018-03-02 04:51:45','2018-03-02 04:56:08');

/*Table structure for table `sensors_doors` */

DROP TABLE IF EXISTS `sensors_doors`;

CREATE TABLE `sensors_doors` (
  `sensors_id` int(11) NOT NULL,
  `doors_id` int(11) NOT NULL,
  PRIMARY KEY (`sensors_id`,`doors_id`),
  KEY `doors_id` (`doors_id`),
  CONSTRAINT `sensors_doors_ibfk_1` FOREIGN KEY (`sensors_id`) REFERENCES `sensors` (`id`),
  CONSTRAINT `sensors_doors_ibfk_2` FOREIGN KEY (`doors_id`) REFERENCES `doors` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `sensors_doors` */

insert  into `sensors_doors`(`sensors_id`,`doors_id`) values (1,8),(2,7),(4,6);

/*Table structure for table `sensors_type` */

DROP TABLE IF EXISTS `sensors_type`;

CREATE TABLE `sensors_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `type` (`type`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `sensors_type` */

insert  into `sensors_type`(`id`,`type`,`created`,`modified`) values (1,'Movimiento','2018-02-11 11:40:36','2018-02-11 11:41:38'),(2,'Presencia','2018-02-11 11:41:56','2018-02-11 11:41:56');

/*Table structure for table `special_schedule` */

DROP TABLE IF EXISTS `special_schedule`;

CREATE TABLE `special_schedule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `people_id` int(11) NOT NULL,
  `doors_id` int(11) NOT NULL,
  `date_init` datetime NOT NULL,
  `date_end` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `people_id` (`people_id`),
  KEY `doors_id` (`doors_id`),
  CONSTRAINT `special_schedule_ibfk_1` FOREIGN KEY (`people_id`) REFERENCES `people` (`id`),
  CONSTRAINT `special_schedule_ibfk_2` FOREIGN KEY (`doors_id`) REFERENCES `doors` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `special_schedule` */

insert  into `special_schedule`(`id`,`people_id`,`doors_id`,`date_init`,`date_end`) values (1,2,1,'2018-02-18 21:58:00','2018-02-19 07:00:00'),(2,3,3,'2018-02-18 01:59:00','2018-02-19 23:00:00');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles_id` int(11) NOT NULL,
  `people_id` int(11) NOT NULL,
  `users_state_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user` (`user`),
  KEY `people_id` (`people_id`),
  KEY `roles_id` (`roles_id`),
  KEY `users_state_id` (`users_state_id`),
  CONSTRAINT `users_ibfk_1` FOREIGN KEY (`people_id`) REFERENCES `people` (`id`),
  CONSTRAINT `users_ibfk_2` FOREIGN KEY (`roles_id`) REFERENCES `roles` (`id`),
  CONSTRAINT `users_ibfk_3` FOREIGN KEY (`users_state_id`) REFERENCES `users_state` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`user`,`password`,`roles_id`,`people_id`,`users_state_id`,`created`,`modified`) values (1,'pedrito','c20ad4d76fe97759aa27a0c99bff6710',1,2,1,'2018-02-14 08:23:20','2018-02-14 08:23:20');

/*Table structure for table `users_state` */

DROP TABLE IF EXISTS `users_state`;

CREATE TABLE `users_state` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `state` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `state` (`state`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `users_state` */

insert  into `users_state`(`id`,`state`,`created`,`modified`) values (1,'Activo a','2018-02-12 08:03:38','2018-02-12 08:06:49'),(2,'Inactivo','2018-02-12 08:03:47','2018-02-12 08:03:47'),(3,'Vacaciones','2018-02-12 08:03:58','2018-02-12 08:03:58'),(4,'estado a','2018-02-12 08:04:21','2018-02-12 08:04:21'),(5,'estado b','2018-02-12 08:04:28','2018-02-12 08:04:28');

/*Table structure for table `vehicles` */

DROP TABLE IF EXISTS `vehicles`;

CREATE TABLE `vehicles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patent` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `internal` tinyint(1) NOT NULL,
  `companies_id` int(11) NOT NULL,
  `people_id` int(11) NOT NULL,
  `vehicles_type_id` int(11) NOT NULL,
  `vehicles_profiles_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `people_id` (`people_id`),
  KEY `vehicles_type_id` (`vehicles_type_id`),
  KEY `vehicles_profiles_id` (`vehicles_profiles_id`),
  KEY `companies_id` (`companies_id`),
  CONSTRAINT `vehicles_ibfk_1` FOREIGN KEY (`people_id`) REFERENCES `people` (`id`),
  CONSTRAINT `vehicles_ibfk_2` FOREIGN KEY (`vehicles_type_id`) REFERENCES `vehicles_type` (`id`),
  CONSTRAINT `vehicles_ibfk_3` FOREIGN KEY (`vehicles_profiles_id`) REFERENCES `vehicles_profiles` (`id`),
  CONSTRAINT `vehicles_ibfk_4` FOREIGN KEY (`companies_id`) REFERENCES `companies` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `vehicles` */

insert  into `vehicles`(`id`,`patent`,`model`,`internal`,`companies_id`,`people_id`,`vehicles_type_id`,`vehicles_profiles_id`,`created`,`modified`) values (1,'111111','charger',1,2,2,1,1,'2018-02-11 10:15:23','2018-02-11 10:15:23'),(2,'222222','camaro ss',0,1,2,1,6,'2018-02-11 04:38:05','2018-02-11 04:38:05'),(3,'333333','carreta',1,2,2,2,5,'2018-02-11 02:16:48','2018-02-11 02:16:48'),(4,'444444','bici',1,1,2,7,3,'2018-02-11 02:17:08','2018-02-11 02:17:08'),(5,'555555','z1',1,2,2,3,5,'2018-02-11 02:18:48','2018-02-11 02:18:48'),(6,'666666','z4',1,3,2,1,2,'2018-02-11 02:19:05','2018-02-11 02:19:05');

/*Table structure for table `vehicles_drivers` */

DROP TABLE IF EXISTS `vehicles_drivers`;

CREATE TABLE `vehicles_drivers` (
  `vehicles_id` int(11) NOT NULL,
  `people_id` int(11) NOT NULL,
  PRIMARY KEY (`vehicles_id`,`people_id`),
  KEY `people_id` (`people_id`),
  CONSTRAINT `vehicles_drivers_ibfk_1` FOREIGN KEY (`vehicles_id`) REFERENCES `vehicles` (`id`),
  CONSTRAINT `vehicles_drivers_ibfk_2` FOREIGN KEY (`people_id`) REFERENCES `people` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `vehicles_drivers` */

insert  into `vehicles_drivers`(`vehicles_id`,`people_id`) values (2,3);

/*Table structure for table `vehicles_profiles` */

DROP TABLE IF EXISTS `vehicles_profiles`;

CREATE TABLE `vehicles_profiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `profile` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `profile` (`profile`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `vehicles_profiles` */

insert  into `vehicles_profiles`(`id`,`profile`,`created`,`modified`) values (1,'perfil 1','2018-02-10 07:14:30','2018-02-10 07:14:30'),(2,'perfil 2','2018-02-10 07:14:39','2018-02-10 07:14:39'),(3,'perfil 3','2018-02-10 07:14:47','2018-02-10 07:14:47'),(4,'perfil 4','2018-02-10 07:14:55','2018-02-10 07:14:55'),(5,'perfil 5','2018-02-10 07:15:00','2018-02-10 07:19:55'),(6,'perfil 6','2018-02-10 07:15:05','2018-02-10 07:15:05');

/*Table structure for table `vehicles_type` */

DROP TABLE IF EXISTS `vehicles_type`;

CREATE TABLE `vehicles_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `type` (`type`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `vehicles_type` */

insert  into `vehicles_type`(`id`,`type`,`created`,`modified`) values (1,'Automovil','2018-02-10 06:33:37','2018-02-10 06:39:44'),(2,'Camioneta','2018-02-10 06:34:19','2018-02-10 06:34:19'),(3,'Furgon','2018-02-10 06:34:26','2018-02-11 02:19:45'),(4,'Camión 3/4','2018-02-10 06:35:10','2018-02-10 06:35:10'),(5,'Camión acoplado','2018-02-10 06:35:38','2018-02-10 06:35:38'),(6,'Camión rampla','2018-02-10 06:35:47','2018-02-10 06:35:47'),(7,'Camión cisterna','2018-02-10 06:36:11','2018-02-10 06:36:11');

/*Table structure for table `zones` */

DROP TABLE IF EXISTS `zones`;

CREATE TABLE `zones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `zone` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `zone` (`zone`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `zones` */

insert  into `zones`(`id`,`zone`,`created`,`modified`) values (1,'Z1','2018-02-09 01:49:41','2018-02-09 01:49:41'),(2,'Z2','2018-02-10 02:52:55','2018-02-10 02:52:55'),(3,'Z3','2018-02-10 02:53:03','2018-02-10 02:53:03'),(4,'z4','2018-02-10 02:53:08','2018-02-10 02:53:08'),(5,'z5','2018-02-10 02:53:14','2018-02-10 02:53:14'),(7,'z6','2018-02-10 02:53:44','2018-02-10 02:53:44');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
