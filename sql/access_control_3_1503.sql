-- phpMyAdmin SQL Dump
-- version 4.7.5
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost:8889
-- Tiempo de generación: 15-03-2018 a las 14:56:40
-- Versión del servidor: 5.6.38
-- Versión de PHP: 7.0.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Base de datos: `access_control_3`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `access_people`
--

CREATE TABLE `access_people` (
  `id` int(11) NOT NULL,
  `entry` tinyint(1) NOT NULL,
  `exit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `hours` tinyint(2) DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `people_id` int(11) NOT NULL,
  `access_state_id` int(11) NOT NULL,
  `main_access_id` int(11) DEFAULT NULL,
  `approved_by` int(11) NOT NULL,
  `observation` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `access_people`
--

INSERT INTO `access_people` (`id`, `entry`, `exit_time`, `hours`, `end_time`, `people_id`, `access_state_id`, `main_access_id`, `approved_by`, `observation`, `created`, `modified`) VALUES
(32, 1, '2018-03-12 03:45:24', 4, '2018-03-12 02:05:00', 44, 2, 2, 3, '..', '2018-03-12 02:05:43', '2018-03-12 02:05:43'),
(33, 1, '2018-03-12 03:45:30', 4, '2018-03-12 02:05:00', 42, 2, 2, 3, '..', '2018-03-12 02:05:44', '2018-03-12 02:05:44'),
(34, 1, '2018-03-12 03:45:34', 4, '2018-03-12 02:05:00', 43, 2, 2, 3, '..', '2018-03-12 02:05:44', '2018-03-12 02:05:44'),
(35, 1, '2018-03-12 03:52:29', 2, '2018-03-12 13:45:00', 44, 2, 2, 3, '...', '2018-03-12 03:46:02', '2018-03-12 03:46:02'),
(36, 1, '2018-03-12 03:52:21', 2, '2018-03-12 13:45:00', 43, 2, 2, 3, '...', '2018-03-12 03:46:03', '2018-03-12 03:46:03'),
(37, 1, '2018-03-12 03:52:15', 1, '2018-03-12 12:47:00', 42, 2, 2, 3, 'aa', '2018-03-12 03:48:50', '2018-03-12 03:48:50'),
(38, 1, '2018-03-12 03:52:20', 1, '2018-03-12 12:47:00', 43, 2, 2, 3, 'aa', '2018-03-12 03:48:50', '2018-03-12 03:48:50'),
(39, 1, '2018-03-12 10:38:22', 2, '2018-03-12 13:52:00', 44, 2, 2, 3, 'yy', '2018-03-12 03:53:03', '2018-03-12 03:53:03'),
(40, 1, '2018-03-14 00:57:49', 2, '2018-03-12 13:52:00', 43, 2, 2, 3, 'yy', '2018-03-12 03:53:03', '2018-03-12 03:53:03'),
(41, 1, '2018-03-14 02:18:05', 2, '2018-03-13 22:57:00', 44, 2, 2, 3, '....', '2018-03-14 00:58:23', '2018-03-14 00:58:23'),
(42, 1, '2018-03-14 02:16:48', 2, '2018-03-13 22:57:00', 43, 2, 2, 3, '....', '2018-03-14 00:58:23', '2018-03-14 00:58:23'),
(43, 1, '2018-03-14 02:23:01', 2, '0000-00-00 00:00:00', 44, 2, 2, 3, '..........', '2018-03-14 02:18:55', '2018-03-14 02:18:55'),
(44, 1, '2018-03-14 03:26:57', 2, '0000-00-00 00:00:00', 44, 2, 2, 3, 'ljljljlklhk', '2018-03-14 02:23:35', '2018-03-14 02:23:35'),
(45, 1, '2018-03-14 03:28:23', 2, '2018-03-14 01:27:00', 44, 2, 2, 3, 'jdjsdjsajdasjdlas', '2018-03-14 03:27:45', '2018-03-14 03:27:45'),
(46, 1, '2018-03-14 03:35:56', 2, '2018-03-14 01:28:00', 44, 2, 2, 3, 'ereg', '2018-03-14 03:29:26', '2018-03-14 03:29:26'),
(47, 1, '2018-03-14 13:35:04', 2, '2018-03-14 01:36:00', 43, 2, 2, 3, 'ggfgf', '2018-03-14 03:36:53', '2018-03-14 03:36:53'),
(48, 1, '2018-03-14 13:48:42', 2, '2018-03-14 11:36:00', 44, 2, 2, 3, 'ggg', '2018-03-14 13:37:01', '2018-03-14 13:37:01'),
(49, 1, '2018-03-14 13:48:18', 1, '2018-03-14 10:44:00', 43, 2, 2, 3, 'pkpkppk', '2018-03-14 13:45:59', '2018-03-14 13:45:59'),
(50, 1, '2018-03-14 13:48:25', 1, '2018-03-14 10:44:00', 43, 2, 2, 3, 'pkpkppk', '2018-03-14 13:46:48', '2018-03-14 13:46:48'),
(51, 1, '2018-03-14 13:48:32', 1, '2018-03-14 10:44:00', 43, 2, 2, 3, 'pkpkppk', '2018-03-14 13:47:24', '2018-03-14 13:47:24'),
(52, 1, '2018-03-14 13:48:38', 1, '2018-03-14 10:44:00', 43, 2, 2, 3, 'pkpkppk', '2018-03-14 13:47:57', '2018-03-14 13:47:57'),
(53, 1, '2018-03-14 13:51:03', 2, '2018-03-14 11:48:00', 44, 2, 2, 3, 'plpl', '2018-03-14 13:50:01', '2018-03-14 13:50:01'),
(54, 1, '2018-03-14 14:08:34', 1, '2018-03-14 11:07:00', 44, 2, 2, 3, 'hghm', '2018-03-14 14:08:21', '2018-03-14 14:08:21'),
(55, 1, '2018-03-14 16:23:59', 1, '2018-03-14 11:08:00', 44, 2, 2, 3, 'h', '2018-03-14 14:09:03', '2018-03-14 14:09:03'),
(56, 1, '2018-03-14 16:24:03', 1, '2018-03-14 11:08:00', 43, 2, 2, 3, 'h', '2018-03-14 14:09:03', '2018-03-14 14:09:03'),
(57, 1, '2018-03-14 18:12:13', 2, '2018-03-14 14:24:00', 44, 2, 2, 3, 'dfgd', '2018-03-14 16:24:30', '2018-03-14 16:24:30'),
(58, 1, '2018-03-14 18:12:17', 2, '2018-03-14 14:24:00', 43, 2, 2, 3, 'dfgd', '2018-03-14 16:24:30', '2018-03-14 16:24:30'),
(59, 1, '2018-03-15 01:39:09', 2, '2018-03-14 16:12:00', 44, 2, 2, 3, 'jj', '2018-03-14 18:12:47', '2018-03-14 18:12:47'),
(60, 1, '2018-03-15 01:39:13', 2, '2018-03-14 16:12:00', 43, 2, 2, 3, 'jj', '2018-03-14 18:12:47', '2018-03-14 18:12:47');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `access_people_areas`
--

CREATE TABLE `access_people_areas` (
  `access_people_id` int(11) NOT NULL,
  `areas_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `access_people_areas`
--

INSERT INTO `access_people_areas` (`access_people_id`, `areas_id`) VALUES
(37, 1),
(41, 1),
(41, 2),
(44, 2),
(53, 3),
(32, 4),
(35, 4),
(39, 4),
(47, 4),
(49, 4),
(50, 4),
(51, 4),
(52, 4),
(54, 4),
(32, 5),
(47, 5),
(54, 5),
(59, 5),
(60, 5),
(46, 6),
(48, 6),
(57, 6),
(58, 6),
(59, 6),
(60, 6),
(43, 7),
(55, 7),
(56, 7),
(45, 8);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `access_people_department`
--

CREATE TABLE `access_people_department` (
  `access_people_id` int(11) NOT NULL,
  `departments_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `access_people_department`
--

INSERT INTO `access_people_department` (`access_people_id`, `departments_id`) VALUES
(37, 3),
(41, 3),
(41, 5),
(44, 5),
(43, 6),
(55, 6),
(56, 6),
(46, 7),
(48, 7),
(57, 7),
(58, 7),
(59, 7),
(60, 7),
(47, 8),
(59, 8),
(60, 8),
(53, 9);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `access_people_intents`
--

CREATE TABLE `access_people_intents` (
  `access_people_id` int(11) NOT NULL,
  `doors_id` int(11) NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `access_people_reasons_visit`
--

CREATE TABLE `access_people_reasons_visit` (
  `access_people_id` int(11) NOT NULL,
  `reasons_visit_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `access_people_reasons_visit`
--

INSERT INTO `access_people_reasons_visit` (`access_people_id`, `reasons_visit_id`) VALUES
(39, 1),
(45, 1),
(37, 2),
(47, 2),
(35, 3),
(57, 3),
(58, 3),
(41, 4),
(59, 4),
(60, 4),
(46, 5),
(59, 5),
(60, 5),
(57, 7),
(58, 7),
(43, 8),
(44, 8),
(53, 8),
(55, 9),
(56, 9),
(48, 10),
(49, 10),
(56, 10);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `access_people_route`
--

CREATE TABLE `access_people_route` (
  `access_people_id` int(11) NOT NULL,
  `doors_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `access_people_route`
--

INSERT INTO `access_people_route` (`access_people_id`, `doors_id`) VALUES
(37, 1),
(39, 1),
(44, 1),
(46, 1),
(47, 1),
(49, 1),
(55, 1),
(56, 1),
(48, 2),
(59, 2),
(60, 2),
(35, 5),
(43, 5),
(53, 5),
(39, 7),
(41, 7),
(45, 7),
(57, 7),
(58, 7),
(59, 7),
(60, 7);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `access_people_state_history`
--

CREATE TABLE `access_people_state_history` (
  `id` int(11) NOT NULL,
  `access_people_id` int(11) DEFAULT NULL,
  `access_state_id` int(11) DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `access_people_state_history`
--

INSERT INTO `access_people_state_history` (`id`, `access_people_id`, `access_state_id`, `description`, `created`) VALUES
(1, 41, 2, 'USUARIO USUARIO', '2018-03-14 00:58:23'),
(2, 43, 2, 'USUARIO USUARIO', '2018-03-14 02:18:55'),
(3, 44, 2, 'USUARIO USUARIO', '2018-03-14 02:23:35'),
(4, 45, 2, 'USUARIO USUARIO', '2018-03-14 03:27:45'),
(5, 46, 2, 'USUARIO USUARIO', '2018-03-14 03:29:26'),
(6, 47, 2, 'USUARIO USUARIO', '2018-03-14 03:36:53'),
(7, 48, 2, 'USUARIO USUARIO', '2018-03-14 13:37:01'),
(8, 49, 2, 'USUARIO USUARIO', '2018-03-14 13:45:59'),
(9, 53, 2, 'USUARIO USUARIO', '2018-03-14 13:50:01'),
(10, 55, 2, 'USUARIO USUARIO', '2018-03-14 14:09:03'),
(11, 56, 2, 'USUARIO USUARIO', '2018-03-14 14:09:03'),
(12, 57, 2, 'USUARIO USUARIO', '2018-03-14 16:24:30'),
(13, 58, 2, 'USUARIO USUARIO', '2018-03-14 16:24:30'),
(14, 59, 2, 'USUARIO USUARIO', '2018-03-14 18:12:47'),
(15, 60, 2, 'USUARIO USUARIO', '2018-03-14 18:12:47');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `access_people_visit`
--

CREATE TABLE `access_people_visit` (
  `access_people_id` int(11) NOT NULL,
  `people_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `access_people_visit`
--

INSERT INTO `access_people_visit` (`access_people_id`, `people_id`) VALUES
(37, 2),
(39, 2),
(59, 2),
(60, 2),
(44, 3),
(45, 3),
(46, 3),
(47, 3),
(48, 3),
(49, 3),
(53, 3),
(55, 3),
(56, 3),
(57, 3),
(58, 3),
(59, 3),
(60, 3),
(35, 21),
(41, 21),
(43, 21);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `access_people_zones`
--

CREATE TABLE `access_people_zones` (
  `access_people_id` int(11) NOT NULL,
  `zones_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `access_people_zones`
--

INSERT INTO `access_people_zones` (`access_people_id`, `zones_id`) VALUES
(37, 1),
(41, 1),
(44, 1),
(45, 2),
(53, 2),
(35, 3),
(39, 3),
(47, 3),
(49, 3),
(59, 3),
(60, 3),
(46, 5),
(48, 5),
(57, 5),
(58, 5),
(59, 5),
(60, 5),
(43, 7),
(55, 7),
(56, 7);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `access_state`
--

CREATE TABLE `access_state` (
  `id` int(11) NOT NULL,
  `state` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `access_state`
--

INSERT INTO `access_state` (`id`, `state`, `created`, `modified`) VALUES
(1, 'Pendiente', '2018-02-18 02:37:46', '2018-02-18 02:40:59'),
(2, 'Permitido', '2018-03-04 09:56:52', '2018-03-04 09:56:52'),
(5, 'Rechazado', '2018-03-04 09:57:07', '2018-03-04 09:57:07');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `access_vehicles`
--

CREATE TABLE `access_vehicles` (
  `id` int(11) NOT NULL,
  `entry` tinyint(1) NOT NULL,
  `exit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `hours` tinyint(2) NOT NULL,
  `end_time` datetime NOT NULL,
  `vehicles_id` int(11) NOT NULL,
  `access_state_id` int(11) NOT NULL,
  `main_access_id` int(11) NOT NULL,
  `approved_by` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `access_vehicles_answers`
--

CREATE TABLE `access_vehicles_answers` (
  `access_vehicles_id` int(11) NOT NULL,
  `forms_id` int(11) NOT NULL,
  `question` int(11) NOT NULL,
  `answer` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `access_vehicles_areas`
--

CREATE TABLE `access_vehicles_areas` (
  `access_vehicles_id` int(11) NOT NULL,
  `areas_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `access_vehicles_departments`
--

CREATE TABLE `access_vehicles_departments` (
  `access_vehicles_id` int(11) NOT NULL,
  `departments_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `access_vehicles_reasons_visit`
--

CREATE TABLE `access_vehicles_reasons_visit` (
  `access_vehicles_id` int(11) NOT NULL,
  `reasons_visit_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `access_vehicles_route`
--

CREATE TABLE `access_vehicles_route` (
  `access_vehicles_id` int(11) NOT NULL,
  `doors_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `access_vehicles_state_history`
--

CREATE TABLE `access_vehicles_state_history` (
  `id` int(11) NOT NULL,
  `access_vehicles_id` int(11) DEFAULT NULL,
  `access_state_id` int(11) DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `access_vehicles_visit`
--

CREATE TABLE `access_vehicles_visit` (
  `access_vehicles_id` int(11) NOT NULL,
  `people_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `access_vehicles_zones`
--

CREATE TABLE `access_vehicles_zones` (
  `access_vehicles_id` int(11) NOT NULL,
  `zones_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `access_vehicle_intents`
--

CREATE TABLE `access_vehicle_intents` (
  `access_vehicle_id` int(11) NOT NULL,
  `doors_id` int(11) NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `answers_type`
--

CREATE TABLE `answers_type` (
  `id` int(11) NOT NULL,
  `type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `answers_type`
--

INSERT INTO `answers_type` (`id`, `type`, `created`, `modified`) VALUES
(1, 'Respuesta corta', '2018-02-14 04:42:23', '2018-02-14 11:33:59'),
(3, 'Párrafo', '2018-02-14 11:34:17', '2018-02-14 11:34:17'),
(4, 'Cantidad', '2018-02-14 11:34:28', '2018-02-14 11:34:28'),
(5, 'Fecha', '2018-02-14 11:34:42', '2018-02-14 11:34:42'),
(6, 'Binaria', '2018-02-14 11:34:53', '2018-02-14 11:34:53');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `areas`
--

CREATE TABLE `areas` (
  `id` int(11) NOT NULL,
  `area` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `zones_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `areas`
--

INSERT INTO `areas` (`id`, `area`, `zones_id`, `created`, `modified`) VALUES
(1, 'Área 1', 1, '2018-02-09 08:42:23', '2018-02-09 09:12:13'),
(2, 'Área 2', 1, '2018-02-09 10:59:51', '2018-02-09 10:59:51'),
(3, 'Área 3', 2, '2018-02-10 02:59:25', '2018-02-10 02:59:25'),
(4, 'Área 4', 3, '2018-02-10 02:59:33', '2018-02-10 02:59:33'),
(5, 'Área 5', 3, '2018-02-10 02:59:42', '2018-02-10 02:59:42'),
(6, 'Área 6', 5, '2018-02-10 02:59:56', '2018-02-10 02:59:56'),
(7, 'Área 7', 7, '2018-02-10 03:00:03', '2018-02-10 03:00:03'),
(8, 'Área 8', 2, '2018-02-10 03:27:39', '2018-02-10 03:27:39');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `companies`
--

CREATE TABLE `companies` (
  `id` int(11) NOT NULL,
  `company` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `companies`
--

INSERT INTO `companies` (`id`, `company`, `address`, `phone`, `email`, `contact`, `created`, `modified`) VALUES
(1, 'fscsdcsd', 'csCADS', 'CscdsC', 'sdcdsC@DD.CL', 'CscdCCCa', '2018-02-08 11:47:48', '2018-02-20 07:56:31'),
(2, 'modificada', 'Villa Pedro Nolasco Calle C #973', '945330884', 'aliro.ramirez02@inacapmail.cl', 'sxasxASX', '2018-02-08 11:48:25', '2018-02-08 11:48:25'),
(3, 'modificada', 'Villa Pedro Nolasco Calle C #973', '945330884', 'aliro.ramirez02@inacapmail.cl', 'tu', '2018-02-09 12:56:09', '2018-02-09 12:56:09');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `configurations`
--

CREATE TABLE `configurations` (
  `id` int(11) NOT NULL,
  `cod_company` int(11) NOT NULL,
  `company` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cod_installation` int(11) NOT NULL,
  `installation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `people_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `configurations`
--

INSERT INTO `configurations` (`id`, `cod_company`, `company`, `cod_installation`, `installation`, `address`, `email`, `phone`, `people_id`, `created`, `modified`) VALUES
(1, 123456, 'gggg', 12, 'cco', 'casa', 'a@q.cl', '121212', 2, '2018-02-17 10:48:04', '2018-02-17 10:53:08');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `departments`
--

CREATE TABLE `departments` (
  `id` int(11) NOT NULL,
  `department` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `areas_id` int(11) NOT NULL,
  `in_charge` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `departments`
--

INSERT INTO `departments` (`id`, `department`, `areas_id`, `in_charge`, `created`, `modified`) VALUES
(3, 'Depto 3', 1, 3, '2018-02-10 02:51:09', '2018-02-10 04:12:09'),
(5, 'Depto 5', 2, 3, '2018-02-10 03:43:45', '2018-02-10 03:43:45'),
(6, 'Depto 6', 7, 3, '2018-02-10 03:29:39', '2018-02-10 03:29:39'),
(7, 'Depto 7', 6, 3, '2018-02-10 03:29:47', '2018-02-10 03:29:47'),
(8, 'Depto 8', 5, 3, '2018-02-10 03:29:54', '2018-02-10 03:29:54'),
(9, 'Depto 9', 3, 3, '2018-02-10 03:30:03', '2018-02-10 03:30:03');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `doors`
--

CREATE TABLE `doors` (
  `id` int(11) NOT NULL,
  `door` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `level` tinyint(3) NOT NULL,
  `doors_type_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `doors`
--

INSERT INTO `doors` (`id`, `door`, `description`, `level`, `doors_type_id`, `created`, `modified`) VALUES
(1, 'Portería 1', 'Puerta portería acceso sur', 0, 4, '2018-02-09 04:15:42', '2018-02-09 06:21:53'),
(2, 'Portería 2', 'Puerta portería acceso norte', 1, 5, '2018-02-09 04:47:28', '2018-02-09 04:47:28'),
(3, 'Portería 3', 'Puerta portería acceso suroriente', 2, 4, '2018-02-09 05:17:33', '2018-02-09 05:17:33'),
(4, 'Portería 4', '.', 3, 4, '2018-02-10 03:26:04', '2018-02-18 09:54:44'),
(5, 'Portería 5', '.', 1, 5, '2018-02-10 03:26:11', '2018-02-18 09:59:33'),
(6, 'Portería 6', '', 4, 4, '2018-02-10 03:26:17', '2018-02-10 03:26:17'),
(7, 'Portería 7', '', 0, 5, '2018-02-10 03:26:23', '2018-02-10 03:26:23'),
(8, 'Portería 8', '', 5, 4, '2018-02-10 03:28:47', '2018-02-10 03:28:47');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `doors_areas`
--

CREATE TABLE `doors_areas` (
  `doors_id` int(11) NOT NULL,
  `areas_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `doors_areas`
--

INSERT INTO `doors_areas` (`doors_id`, `areas_id`) VALUES
(1, 1),
(2, 2),
(3, 3),
(4, 4),
(5, 5),
(6, 6),
(7, 7),
(8, 8);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `doors_departments`
--

CREATE TABLE `doors_departments` (
  `doors_id` int(11) NOT NULL,
  `departments_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `doors_departments`
--

INSERT INTO `doors_departments` (`doors_id`, `departments_id`) VALUES
(5, 3),
(6, 5),
(4, 6),
(3, 7),
(2, 8),
(1, 9);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `doors_parents`
--

CREATE TABLE `doors_parents` (
  `doors_id` int(11) NOT NULL,
  `parent` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `doors_type`
--

CREATE TABLE `doors_type` (
  `id` int(11) NOT NULL,
  `type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `doors_type`
--

INSERT INTO `doors_type` (`id`, `type`, `created`, `modified`) VALUES
(4, 'Corredera manual', '2018-02-09 03:47:30', '2018-02-09 03:47:30'),
(5, 'Corredera automatica', '2018-02-09 03:47:38', '2018-02-09 03:47:38'),
(6, 'Batiente manual', '2018-02-10 03:49:15', '2018-02-10 03:49:15'),
(7, 'Batiente automatica', '2018-02-10 03:49:27', '2018-02-10 03:49:27'),
(8, 'Levadiza manual', '2018-02-10 03:49:56', '2018-02-10 03:50:17'),
(9, 'Levadiza automatica', '2018-02-10 03:50:09', '2018-02-10 03:50:09');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `doors_zones`
--

CREATE TABLE `doors_zones` (
  `doors_id` int(11) NOT NULL,
  `zones_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `doors_zones`
--

INSERT INTO `doors_zones` (`doors_id`, `zones_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(8, 1),
(1, 2),
(4, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `forms`
--

CREATE TABLE `forms` (
  `id` int(11) NOT NULL,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `forms`
--

INSERT INTO `forms` (`id`, `title`, `description`, `created`, `modified`) VALUES
(32, 'Formulario prueba', 'prueba crear, editar', '2018-02-16 03:04:20', '2018-02-25 01:27:52');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `forms_detail`
--

CREATE TABLE `forms_detail` (
  `order` int(11) NOT NULL,
  `question` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `placeholder` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `forms_id` int(11) NOT NULL,
  `answers_type_id` int(11) NOT NULL,
  `measures_id` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `forms_detail`
--

INSERT INTO `forms_detail` (`order`, `question`, `placeholder`, `forms_id`, `answers_type_id`, `measures_id`) VALUES
(1, 'Pregunta 1', 'Respuesta 1', 32, 1, 0),
(2, 'Pregunta 2', 'Respuesta 2', 32, 3, 0),
(3, 'Pregunta 3', 'Respuesta 3b', 32, 6, 0),
(4, 'Pregunta 4', 'hoy', 32, 5, 0),
(5, 'Pregunta 5', '123', 32, 4, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `forms_season`
--

CREATE TABLE `forms_season` (
  `vechiles_type_id` int(11) NOT NULL,
  `forms_id` int(11) NOT NULL,
  `seasons_id` int(11) NOT NULL,
  `year` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `forms_season`
--

INSERT INTO `forms_season` (`vechiles_type_id`, `forms_id`, `seasons_id`, `year`) VALUES
(1, 32, 1, 2018),
(3, 32, 4, 2019);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `internal_errors`
--

CREATE TABLE `internal_errors` (
  `id` int(11) NOT NULL,
  `people_id` int(11) NOT NULL,
  `sensors_id` int(11) NOT NULL,
  `reasons_error_id` int(11) NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `internal_errors`
--

INSERT INTO `internal_errors` (`id`, `people_id`, `sensors_id`, `reasons_error_id`, `created`) VALUES
(1, 2, 1, 1, '2018-02-18 12:02:06');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `internal_success`
--

CREATE TABLE `internal_success` (
  `id` int(11) NOT NULL,
  `people_id` int(11) NOT NULL,
  `sensors_id` int(11) NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `internal_success`
--

INSERT INTO `internal_success` (`id`, `people_id`, `sensors_id`, `created`) VALUES
(1, 2, 4, '2018-02-18 16:17:44'),
(2, 3, 2, '2018-02-18 16:22:59');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `jornada`
--

CREATE TABLE `jornada` (
  `id` int(11) NOT NULL,
  `jornada` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `L` tinyint(1) DEFAULT '0',
  `M` tinyint(1) DEFAULT '0',
  `Mi` tinyint(1) DEFAULT '0',
  `J` tinyint(1) DEFAULT '0',
  `V` tinyint(1) DEFAULT '0',
  `S` tinyint(1) DEFAULT '0',
  `D` tinyint(1) DEFAULT '0',
  `time_init` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time_end` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `jornada`
--

INSERT INTO `jornada` (`id`, `jornada`, `L`, `M`, `Mi`, `J`, `V`, `S`, `D`, `time_init`, `time_end`) VALUES
(2, 'Jornada 1', 0, 0, 0, 0, 0, 0, 0, '08:00', '18:00'),
(3, 'Jornada 2', 0, 0, 0, 0, 0, 0, 0, '20:00', '08:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `main_access`
--

CREATE TABLE `main_access` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ubication` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ip_host` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_host` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `entry` tinyint(1) NOT NULL,
  `flow` tinyint(1) NOT NULL,
  `internal` tinyint(1) NOT NULL,
  `state` tinyint(1) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `main_access`
--

INSERT INTO `main_access` (`id`, `name`, `ubication`, `ip_host`, `name_host`, `entry`, `flow`, `internal`, `state`, `created`, `modified`) VALUES
(2, 'Acceso principal', 'Teno', '10.10.1.1', 'PAPC', 2, 0, 1, 1, '2018-02-11 05:09:39', '2018-02-14 09:21:45'),
(3, 'Acceso secundario 1', 'Curicó', '10.10.1.2', 'PASC1', 2, 2, 2, 1, '2018-02-11 05:11:45', '2018-02-14 09:22:07'),
(4, 'Acceso secundario 2', 'Curicó', '10.10.1.3', 'papap', 0, 2, 0, 1, '2018-02-11 05:13:57', '2018-02-14 09:22:40'),
(5, 'Acceso principal3', 'Curicó', '10.10.1.4', 'prprpp', 1, 1, 1, 1, '2018-02-11 05:14:54', '2018-02-13 12:57:27'),
(6, 'Acceso principal 4', 'Curicó', '10.10.1.5', 'papappapapa', 1, 1, 1, 0, '2018-02-11 05:17:00', '2018-02-14 09:31:30');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `measures`
--

CREATE TABLE `measures` (
  `id` int(11) NOT NULL,
  `measure` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `acronimo` varchar(6) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `measures`
--

INSERT INTO `measures` (`id`, `measure`, `acronimo`, `created`, `modified`) VALUES
(1, 'Kilos', 'Kgs', '2018-02-14 23:26:17', '2018-02-24 09:47:44'),
(3, 'Litros', 'Lts', '2018-02-17 03:49:06', '2018-02-17 03:49:06'),
(4, 'Gramos', 'Grs', '2018-02-24 09:47:38', '2018-02-24 09:47:38');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `options`
--

CREATE TABLE `options` (
  `id` int(11) NOT NULL,
  `option` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `options`
--

INSERT INTO `options` (`id`, `option`, `code`, `created`, `modified`) VALUES
(1, 'Opción 1.', 'a123-k', '2018-02-13 10:26:11', '2018-02-13 10:31:32'),
(4, 'Opción 2.', 'a123-k', '2018-02-13 10:26:11', '2018-02-13 10:31:32'),
(5, 'Opción 3.', 'a123-k', '2018-02-13 10:26:11', '2018-02-13 10:31:32'),
(6, 'Opción 4.', 'a123-k', '2018-02-13 10:26:11', '2018-02-13 10:31:32'),
(7, 'Opción 5.', 'a123-k', '2018-02-13 10:26:11', '2018-02-13 10:31:32');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `options_roles`
--

CREATE TABLE `options_roles` (
  `options_id` int(11) NOT NULL,
  `roles_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `options_roles`
--

INSERT INTO `options_roles` (`options_id`, `roles_id`) VALUES
(5, 1),
(6, 1),
(7, 1),
(1, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `people`
--

CREATE TABLE `people` (
  `id` int(11) NOT NULL,
  `rut` int(15) NOT NULL,
  `digit` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `allow_all` tinyint(1) DEFAULT NULL,
  `is_visited` tinyint(1) DEFAULT NULL,
  `internal` tinyint(1) DEFAULT '0',
  `people_profiles_id` int(11) NOT NULL,
  `companies_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `people`
--

INSERT INTO `people` (`id`, `rut`, `digit`, `name`, `last_name`, `address`, `email`, `phone`, `allow_all`, `is_visited`, `internal`, `people_profiles_id`, `companies_id`, `created`, `modified`) VALUES
(2, 33333333, '3', 'persona', 'soltera', 'su casa', 'no_tiene@sinmail.cl', '88776655', 1, 1, 1, 1, 1, '2018-02-17 03:34:12', '2018-02-17 03:34:12'),
(3, 11111111, '1', 'Usuario', 'usuario', 'casa', 'a@a.cl', '4', 1, 1, 1, 4, 1, '2018-02-18 08:24:35', '2018-02-18 08:24:35'),
(21, 22222222, '2', 'Aliro', 'Nuñez', 'Villa Pedro Nolasco Calle C #973', 'aliro.ramirez02@inacapmail.cl', '945330884', 1, 1, 1, 2, 1, '2018-03-02 10:55:11', '2018-03-02 10:55:11'),
(30, 44444444, '4', 'Aliro', 'Nuñez', 'Villa Pedro Nolasco Calle C #973', 'aliro.ramirez02@inacapmail.cl', '945330884', 1, 1, 1, 5, 2, '2018-03-03 10:20:33', '2018-03-03 10:20:33'),
(31, 55555555, '5', 'Aliro', 'Nuñez', 'Villa Pedro Nolasco Calle C #973', 'aliro.ramirez02@inacapmail.cl', '945330884', 0, 0, 1, 4, 1, '2018-03-03 10:27:52', '2018-03-03 10:27:52'),
(42, 66666666, '6', 'Aliro', 'Nuñez', 'Villa Pedro Nolasco Calle C #973', 'aliro.ramirez02@inacapmail.cl', '945330884', 0, 0, 0, 2, 1, '2018-03-04 02:40:43', '2018-03-04 02:40:43'),
(43, 77777777, '7', 'Aliro', 'Nuñez', 'Villa Pedro Nolasco Calle C #973', 'aliro.ramirez02@inacapmail.cl', '945330884', 0, 0, 0, 2, 2, '2018-03-04 02:49:53', '2018-03-04 02:49:53'),
(44, 88888888, '8', 'Aliro', 'Nuñez', 'Villa Pedro Nolasco Calle C #973', 'aliro.ramirez02@inacapmail.cl', '945330884', 0, 0, 0, 2, 1, '2018-03-04 02:51:07', '2018-03-04 02:51:07');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `people_profiles`
--

CREATE TABLE `people_profiles` (
  `id` int(11) NOT NULL,
  `profile` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `people_profiles`
--

INSERT INTO `people_profiles` (`id`, `profile`, `created`, `modified`) VALUES
(1, 'chofer', '2018-02-10 19:31:33', '2018-02-10 19:31:33'),
(2, 'administrativo', '2018-02-12 11:08:47', '2018-02-12 11:11:10'),
(3, 'jornal', '2018-02-12 11:10:14', '2018-02-12 11:10:14'),
(4, 'operario', '2018-02-12 11:10:23', '2018-02-12 11:10:23'),
(5, 'guardia', '2018-02-12 11:10:31', '2018-02-12 11:10:31');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `profiles_doors_schedules`
--

CREATE TABLE `profiles_doors_schedules` (
  `profiles_people_id` int(11) NOT NULL,
  `doors_id` int(11) NOT NULL,
  `time_init` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time_end` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jornada_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `profiles_doors_schedules`
--

INSERT INTO `profiles_doors_schedules` (`profiles_people_id`, `doors_id`, `time_init`, `time_end`, `jornada_id`) VALUES
(2, 1, '08:00', '18:00', 2),
(2, 5, '08:00', '18:00', 2),
(2, 7, '08:00', '18:00', 2),
(2, 6, '20:00', '08:00', 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `projects`
--

CREATE TABLE `projects` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `in_charge` int(11) DEFAULT NULL,
  `init` date DEFAULT NULL,
  `end` date DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `projects_areas`
--

CREATE TABLE `projects_areas` (
  `projects_id` int(11) NOT NULL,
  `areas_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `projects_departments`
--

CREATE TABLE `projects_departments` (
  `projects_id` int(11) NOT NULL,
  `departments_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `projects_intents`
--

CREATE TABLE `projects_intents` (
  `projects_id` int(11) NOT NULL,
  `doors_id` int(11) NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `projects_people`
--

CREATE TABLE `projects_people` (
  `projects_id` int(11) NOT NULL,
  `people_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `projects_route`
--

CREATE TABLE `projects_route` (
  `projects_id` int(11) NOT NULL,
  `doors_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `projects_schedules`
--

CREATE TABLE `projects_schedules` (
  `projects_id` int(11) DEFAULT NULL,
  `time_init` time DEFAULT NULL,
  `time_end` time DEFAULT NULL,
  `L` tinyint(1) DEFAULT '0',
  `M` tinyint(1) DEFAULT '0',
  `Mi` tinyint(1) DEFAULT '0',
  `J` tinyint(1) DEFAULT '0',
  `V` tinyint(1) DEFAULT '0',
  `S` tinyint(1) DEFAULT '0',
  `D` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `projects_zones`
--

CREATE TABLE `projects_zones` (
  `projects_id` int(11) NOT NULL,
  `zones_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reasons_error`
--

CREATE TABLE `reasons_error` (
  `id` int(11) NOT NULL,
  `reason` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `reasons_error`
--

INSERT INTO `reasons_error` (`id`, `reason`, `created`, `modified`) VALUES
(1, 'Puerta no autorizada', '2018-02-18 12:01:53', '2018-02-18 12:01:53'),
(3, 'Fuera de horario permitido', '2018-02-18 09:29:04', '2018-02-18 09:29:04');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `reasons_visit`
--

CREATE TABLE `reasons_visit` (
  `id` int(11) NOT NULL,
  `reason` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `reasons_visit`
--

INSERT INTO `reasons_visit` (`id`, `reason`, `created`, `modified`) VALUES
(1, 'Servicio de valija', '2018-03-04 01:41:37', '2018-03-04 01:41:37'),
(2, 'Servicio de encomienda', '2018-03-04 01:41:54', '2018-03-04 01:41:54'),
(3, 'Entrega de materia prima', '2018-03-04 01:44:29', '2018-03-04 01:44:29'),
(4, 'Despacho a puerto', '2018-03-04 01:44:46', '2018-03-04 01:44:46'),
(5, 'Cliente de productos', '2018-03-04 03:17:05', '2018-03-04 03:17:05'),
(7, 'Proveedor materia prima', '2018-03-04 03:18:59', '2018-03-04 03:18:59'),
(8, 'Retiro propietario', '2018-03-04 03:21:32', '2018-03-04 03:21:32'),
(9, 'test', '2018-03-08 07:22:35', '2018-03-08 07:22:35'),
(10, 'tset3', '2018-03-08 07:23:22', '2018-03-08 07:23:22');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `rol` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`id`, `rol`, `created`, `modified`) VALUES
(1, 'Administrador', '2018-02-12 05:34:39', '2018-02-12 05:34:39'),
(2, 'Funcionario', '2018-02-12 05:35:00', '2018-02-12 05:35:00'),
(3, 'Visitante', '2018-02-12 05:35:11', '2018-02-12 05:35:11'),
(4, 'Guardia', '2018-02-12 05:35:22', '2018-02-12 05:35:22'),
(5, 'Operario', '2018-02-12 05:35:51', '2018-02-12 05:35:51');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `seasons`
--

CREATE TABLE `seasons` (
  `id` int(11) NOT NULL,
  `season` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `seasons`
--

INSERT INTO `seasons` (`id`, `season`, `created`, `modified`) VALUES
(1, 'Temporada 20188', '2018-02-12 03:21:23', '2018-02-12 03:26:00'),
(2, 'Temporada 2019', '2018-02-12 03:21:53', '2018-02-12 03:21:53'),
(3, 'Temporada 2020', '2018-02-12 03:22:05', '2018-02-12 03:22:05'),
(4, 'Temporada 2018-2019', '2018-02-12 03:22:17', '2018-02-12 03:22:17'),
(5, 'Temporada 2019 - 2020', '2018-02-12 03:22:29', '2018-02-12 03:22:29');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sensors`
--

CREATE TABLE `sensors` (
  `id` int(11) NOT NULL,
  `code` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sensor` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ip` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sensors_type` int(11) NOT NULL,
  `entry` tinyint(1) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `sensors`
--

INSERT INTO `sensors` (`id`, `code`, `sensor`, `description`, `ip`, `sensors_type`, `entry`, `created`, `modified`) VALUES
(1, 'KK1', 'Ultrasonico 1', '', '10.10.10.10', 2, 0, '2018-02-12 03:37:54', '2018-02-12 03:37:54'),
(2, 'KK2', 'Ultrasonico 2', 'prueba de edición', '10.10.10.11', 2, 0, '2018-02-12 03:39:55', '2018-02-12 03:55:01'),
(3, 'KK3', 'Ultrasonico 3', '', '10.10.10.12', 2, 0, '2018-02-12 03:40:25', '2018-02-12 03:40:25'),
(4, 'KK4', 'Ultrasonico 4', 'rango 1 metro', '10.10.10.13', 2, 0, '2018-02-12 03:41:29', '2018-02-12 03:41:29'),
(5, 'KK5', 'Ultrasonico 5', 'rango 1 metro', '10.10.10.14', 1, 2, '2018-02-12 03:41:57', '2018-02-12 03:41:57');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sensors_doors`
--

CREATE TABLE `sensors_doors` (
  `sensors_id` int(11) NOT NULL,
  `doors_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `sensors_doors`
--

INSERT INTO `sensors_doors` (`sensors_id`, `doors_id`) VALUES
(4, 6),
(2, 7),
(1, 8);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sensors_type`
--

CREATE TABLE `sensors_type` (
  `id` int(11) NOT NULL,
  `type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `sensors_type`
--

INSERT INTO `sensors_type` (`id`, `type`, `created`, `modified`) VALUES
(1, 'Movimiento', '2018-02-11 11:40:36', '2018-02-11 11:41:38'),
(2, 'Presencia', '2018-02-11 11:41:56', '2018-02-11 11:41:56');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `special_schedule`
--

CREATE TABLE `special_schedule` (
  `id` int(11) NOT NULL,
  `people_id` int(11) NOT NULL,
  `doors_id` int(11) NOT NULL,
  `date_init` datetime NOT NULL,
  `date_end` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `special_schedule`
--

INSERT INTO `special_schedule` (`id`, `people_id`, `doors_id`, `date_init`, `date_end`) VALUES
(1, 2, 1, '2018-02-18 21:58:00', '2018-02-19 07:00:00'),
(2, 3, 3, '2018-02-18 01:59:00', '2018-02-19 23:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `user` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles_id` int(11) NOT NULL,
  `people_id` int(11) NOT NULL,
  `users_state_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `user`, `password`, `roles_id`, `people_id`, `users_state_id`, `created`, `modified`) VALUES
(1, 'pedrito', 'c20ad4d76fe97759aa27a0c99bff6710', 1, 2, 1, '2018-02-14 08:23:20', '2018-02-14 08:23:20');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users_state`
--

CREATE TABLE `users_state` (
  `id` int(11) NOT NULL,
  `state` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users_state`
--

INSERT INTO `users_state` (`id`, `state`, `created`, `modified`) VALUES
(1, 'Activo a', '2018-02-12 08:03:38', '2018-02-12 08:06:49'),
(2, 'Inactivo', '2018-02-12 08:03:47', '2018-02-12 08:03:47'),
(3, 'Vacaciones', '2018-02-12 08:03:58', '2018-02-12 08:03:58'),
(4, 'estado a', '2018-02-12 08:04:21', '2018-02-12 08:04:21'),
(5, 'estado b', '2018-02-12 08:04:28', '2018-02-12 08:04:28');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vehicles`
--

CREATE TABLE `vehicles` (
  `id` int(11) NOT NULL,
  `patent` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `internal` tinyint(1) NOT NULL,
  `companies_id` int(11) NOT NULL,
  `people_id` int(11) NOT NULL,
  `vehicles_type_id` int(11) NOT NULL,
  `vehicles_profiles_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `vehicles`
--

INSERT INTO `vehicles` (`id`, `patent`, `model`, `internal`, `companies_id`, `people_id`, `vehicles_type_id`, `vehicles_profiles_id`, `created`, `modified`) VALUES
(1, '111111', 'charger', 1, 2, 2, 1, 1, '2018-02-11 10:15:23', '2018-02-11 10:15:23'),
(2, '222222', 'camaro ss', 0, 1, 2, 1, 6, '2018-02-11 04:38:05', '2018-02-11 04:38:05'),
(3, '333333', 'carreta', 1, 2, 2, 2, 5, '2018-02-11 02:16:48', '2018-02-11 02:16:48'),
(4, '444444', 'bici', 1, 1, 2, 7, 3, '2018-02-11 02:17:08', '2018-02-11 02:17:08'),
(5, '555555', 'z1', 1, 2, 2, 3, 5, '2018-02-11 02:18:48', '2018-02-11 02:18:48'),
(6, '666666', 'z4', 1, 3, 2, 1, 2, '2018-02-11 02:19:05', '2018-02-11 02:19:05');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vehicles_drivers`
--

CREATE TABLE `vehicles_drivers` (
  `vehicles_id` int(11) NOT NULL,
  `people_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `vehicles_drivers`
--

INSERT INTO `vehicles_drivers` (`vehicles_id`, `people_id`) VALUES
(2, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vehicles_profiles`
--

CREATE TABLE `vehicles_profiles` (
  `id` int(11) NOT NULL,
  `profile` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `vehicles_profiles`
--

INSERT INTO `vehicles_profiles` (`id`, `profile`, `created`, `modified`) VALUES
(1, 'perfil 1', '2018-02-10 07:14:30', '2018-02-10 07:14:30'),
(2, 'perfil 2', '2018-02-10 07:14:39', '2018-02-10 07:14:39'),
(3, 'perfil 3', '2018-02-10 07:14:47', '2018-02-10 07:14:47'),
(4, 'perfil 4', '2018-02-10 07:14:55', '2018-02-10 07:14:55'),
(5, 'perfil 5', '2018-02-10 07:15:00', '2018-02-10 07:19:55'),
(6, 'perfil 6', '2018-02-10 07:15:05', '2018-02-10 07:15:05');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vehicles_type`
--

CREATE TABLE `vehicles_type` (
  `id` int(11) NOT NULL,
  `type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `vehicles_type`
--

INSERT INTO `vehicles_type` (`id`, `type`, `created`, `modified`) VALUES
(1, 'Automovil', '2018-02-10 06:33:37', '2018-02-10 06:39:44'),
(2, 'Camioneta', '2018-02-10 06:34:19', '2018-02-10 06:34:19'),
(3, 'Furgon', '2018-02-10 06:34:26', '2018-02-11 02:19:45'),
(4, 'Camión 3/4', '2018-02-10 06:35:10', '2018-02-10 06:35:10'),
(5, 'Camión acoplado', '2018-02-10 06:35:38', '2018-02-10 06:35:38'),
(6, 'Camión rampla', '2018-02-10 06:35:47', '2018-02-10 06:35:47'),
(7, 'Camión cisterna', '2018-02-10 06:36:11', '2018-02-10 06:36:11');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `zones`
--

CREATE TABLE `zones` (
  `id` int(11) NOT NULL,
  `zone` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `zones`
--

INSERT INTO `zones` (`id`, `zone`, `created`, `modified`) VALUES
(1, 'Zona 1', '2018-02-09 01:49:41', '2018-02-09 01:49:41'),
(2, 'Zona 2', '2018-02-10 02:52:55', '2018-02-10 02:52:55'),
(3, 'Zona 3', '2018-02-10 02:53:03', '2018-02-10 02:53:03'),
(4, 'Zona 4', '2018-02-10 02:53:08', '2018-02-10 02:53:08'),
(5, 'Zona 5', '2018-02-10 02:53:14', '2018-02-10 02:53:14'),
(7, 'Zona 7', '2018-02-10 02:53:44', '2018-02-10 02:53:44');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `access_people`
--
ALTER TABLE `access_people`
  ADD PRIMARY KEY (`id`),
  ADD KEY `people_id` (`people_id`),
  ADD KEY `access_state_id` (`access_state_id`),
  ADD KEY `main_access_id` (`main_access_id`),
  ADD KEY `approved_by` (`approved_by`);

--
-- Indices de la tabla `access_people_areas`
--
ALTER TABLE `access_people_areas`
  ADD PRIMARY KEY (`access_people_id`,`areas_id`),
  ADD KEY `areas_id` (`areas_id`);

--
-- Indices de la tabla `access_people_department`
--
ALTER TABLE `access_people_department`
  ADD PRIMARY KEY (`access_people_id`,`departments_id`),
  ADD KEY `departments_id` (`departments_id`);

--
-- Indices de la tabla `access_people_intents`
--
ALTER TABLE `access_people_intents`
  ADD KEY `access_people_id` (`access_people_id`),
  ADD KEY `doors_id` (`doors_id`);

--
-- Indices de la tabla `access_people_reasons_visit`
--
ALTER TABLE `access_people_reasons_visit`
  ADD PRIMARY KEY (`access_people_id`,`reasons_visit_id`),
  ADD KEY `reasons_visit_id` (`reasons_visit_id`);

--
-- Indices de la tabla `access_people_route`
--
ALTER TABLE `access_people_route`
  ADD PRIMARY KEY (`access_people_id`,`doors_id`),
  ADD KEY `doors_id` (`doors_id`);

--
-- Indices de la tabla `access_people_state_history`
--
ALTER TABLE `access_people_state_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `access_people_id` (`access_people_id`),
  ADD KEY `access_state_id` (`access_state_id`);

--
-- Indices de la tabla `access_people_visit`
--
ALTER TABLE `access_people_visit`
  ADD PRIMARY KEY (`access_people_id`,`people_id`),
  ADD KEY `people_id` (`people_id`);

--
-- Indices de la tabla `access_people_zones`
--
ALTER TABLE `access_people_zones`
  ADD PRIMARY KEY (`access_people_id`,`zones_id`),
  ADD KEY `zones_id` (`zones_id`);

--
-- Indices de la tabla `access_state`
--
ALTER TABLE `access_state`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `state` (`state`);

--
-- Indices de la tabla `access_vehicles`
--
ALTER TABLE `access_vehicles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `vehicles_id` (`vehicles_id`),
  ADD KEY `access_state_id` (`access_state_id`),
  ADD KEY `main_access_id` (`main_access_id`),
  ADD KEY `approved_by` (`approved_by`);

--
-- Indices de la tabla `access_vehicles_answers`
--
ALTER TABLE `access_vehicles_answers`
  ADD PRIMARY KEY (`access_vehicles_id`,`forms_id`,`question`),
  ADD KEY `forms_id` (`forms_id`);

--
-- Indices de la tabla `access_vehicles_areas`
--
ALTER TABLE `access_vehicles_areas`
  ADD PRIMARY KEY (`access_vehicles_id`,`areas_id`),
  ADD KEY `areas_id` (`areas_id`);

--
-- Indices de la tabla `access_vehicles_departments`
--
ALTER TABLE `access_vehicles_departments`
  ADD PRIMARY KEY (`access_vehicles_id`,`departments_id`),
  ADD KEY `departments_id` (`departments_id`);

--
-- Indices de la tabla `access_vehicles_reasons_visit`
--
ALTER TABLE `access_vehicles_reasons_visit`
  ADD PRIMARY KEY (`access_vehicles_id`,`reasons_visit_id`),
  ADD KEY `reasons_visit_id` (`reasons_visit_id`);

--
-- Indices de la tabla `access_vehicles_route`
--
ALTER TABLE `access_vehicles_route`
  ADD PRIMARY KEY (`access_vehicles_id`,`doors_id`),
  ADD KEY `doors_id` (`doors_id`);

--
-- Indices de la tabla `access_vehicles_state_history`
--
ALTER TABLE `access_vehicles_state_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `access_vehicles_id` (`access_vehicles_id`),
  ADD KEY `access_state_id` (`access_state_id`);

--
-- Indices de la tabla `access_vehicles_visit`
--
ALTER TABLE `access_vehicles_visit`
  ADD PRIMARY KEY (`access_vehicles_id`,`people_id`),
  ADD KEY `people_id` (`people_id`);

--
-- Indices de la tabla `access_vehicles_zones`
--
ALTER TABLE `access_vehicles_zones`
  ADD PRIMARY KEY (`access_vehicles_id`,`zones_id`),
  ADD KEY `zones_id` (`zones_id`);

--
-- Indices de la tabla `access_vehicle_intents`
--
ALTER TABLE `access_vehicle_intents`
  ADD KEY `access_vehicle_id` (`access_vehicle_id`),
  ADD KEY `doors_id` (`doors_id`);

--
-- Indices de la tabla `answers_type`
--
ALTER TABLE `answers_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `type` (`type`);

--
-- Indices de la tabla `areas`
--
ALTER TABLE `areas`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `area` (`area`),
  ADD KEY `zones_id` (`zones_id`);

--
-- Indices de la tabla `companies`
--
ALTER TABLE `companies`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `configurations`
--
ALTER TABLE `configurations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `people_id` (`people_id`);

--
-- Indices de la tabla `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `department` (`department`),
  ADD KEY `areas_id` (`areas_id`),
  ADD KEY `in_charge` (`in_charge`);

--
-- Indices de la tabla `doors`
--
ALTER TABLE `doors`
  ADD PRIMARY KEY (`id`),
  ADD KEY `doors_type_id` (`doors_type_id`);

--
-- Indices de la tabla `doors_areas`
--
ALTER TABLE `doors_areas`
  ADD PRIMARY KEY (`doors_id`,`areas_id`),
  ADD KEY `areas_id` (`areas_id`);

--
-- Indices de la tabla `doors_departments`
--
ALTER TABLE `doors_departments`
  ADD PRIMARY KEY (`doors_id`,`departments_id`),
  ADD KEY `departments_id` (`departments_id`);

--
-- Indices de la tabla `doors_parents`
--
ALTER TABLE `doors_parents`
  ADD PRIMARY KEY (`doors_id`,`parent`);

--
-- Indices de la tabla `doors_type`
--
ALTER TABLE `doors_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `type` (`type`);

--
-- Indices de la tabla `doors_zones`
--
ALTER TABLE `doors_zones`
  ADD PRIMARY KEY (`doors_id`,`zones_id`),
  ADD KEY `zones_id` (`zones_id`);

--
-- Indices de la tabla `forms`
--
ALTER TABLE `forms`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `forms_detail`
--
ALTER TABLE `forms_detail`
  ADD PRIMARY KEY (`order`,`forms_id`),
  ADD KEY `forms_id` (`forms_id`),
  ADD KEY `answers_type_id` (`answers_type_id`);

--
-- Indices de la tabla `forms_season`
--
ALTER TABLE `forms_season`
  ADD PRIMARY KEY (`vechiles_type_id`,`forms_id`,`seasons_id`,`year`),
  ADD KEY `forms_id` (`forms_id`),
  ADD KEY `seasons_id` (`seasons_id`);

--
-- Indices de la tabla `internal_errors`
--
ALTER TABLE `internal_errors`
  ADD PRIMARY KEY (`id`),
  ADD KEY `people_id` (`people_id`),
  ADD KEY `sensors_id` (`sensors_id`),
  ADD KEY `reasons_error_id` (`reasons_error_id`);

--
-- Indices de la tabla `internal_success`
--
ALTER TABLE `internal_success`
  ADD PRIMARY KEY (`id`),
  ADD KEY `people_id` (`people_id`),
  ADD KEY `sensors_id` (`sensors_id`);

--
-- Indices de la tabla `jornada`
--
ALTER TABLE `jornada`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `main_access`
--
ALTER TABLE `main_access`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `measures`
--
ALTER TABLE `measures`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `measure` (`measure`);

--
-- Indices de la tabla `options`
--
ALTER TABLE `options`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `option` (`option`);

--
-- Indices de la tabla `options_roles`
--
ALTER TABLE `options_roles`
  ADD PRIMARY KEY (`options_id`,`roles_id`),
  ADD KEY `roles_id` (`roles_id`);

--
-- Indices de la tabla `people`
--
ALTER TABLE `people`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `rut` (`rut`),
  ADD KEY `profiles_people_id` (`people_profiles_id`),
  ADD KEY `companies_id` (`companies_id`);

--
-- Indices de la tabla `people_profiles`
--
ALTER TABLE `people_profiles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `profile` (`profile`);

--
-- Indices de la tabla `profiles_doors_schedules`
--
ALTER TABLE `profiles_doors_schedules`
  ADD PRIMARY KEY (`profiles_people_id`,`doors_id`,`time_init`,`time_end`),
  ADD KEY `profiles_people_id` (`profiles_people_id`),
  ADD KEY `doors_id` (`doors_id`),
  ADD KEY `jornada_id` (`jornada_id`);

--
-- Indices de la tabla `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`),
  ADD KEY `in_charge` (`in_charge`);

--
-- Indices de la tabla `projects_areas`
--
ALTER TABLE `projects_areas`
  ADD PRIMARY KEY (`projects_id`,`areas_id`),
  ADD KEY `areas_id` (`areas_id`);

--
-- Indices de la tabla `projects_departments`
--
ALTER TABLE `projects_departments`
  ADD PRIMARY KEY (`projects_id`,`departments_id`),
  ADD KEY `departments_id` (`departments_id`);

--
-- Indices de la tabla `projects_intents`
--
ALTER TABLE `projects_intents`
  ADD PRIMARY KEY (`projects_id`,`doors_id`,`created`),
  ADD KEY `doors_id` (`doors_id`);

--
-- Indices de la tabla `projects_people`
--
ALTER TABLE `projects_people`
  ADD PRIMARY KEY (`projects_id`,`people_id`),
  ADD KEY `people_id` (`people_id`);

--
-- Indices de la tabla `projects_route`
--
ALTER TABLE `projects_route`
  ADD PRIMARY KEY (`projects_id`,`doors_id`),
  ADD KEY `doors_id` (`doors_id`);

--
-- Indices de la tabla `projects_schedules`
--
ALTER TABLE `projects_schedules`
  ADD KEY `projects_id` (`projects_id`);

--
-- Indices de la tabla `projects_zones`
--
ALTER TABLE `projects_zones`
  ADD PRIMARY KEY (`projects_id`,`zones_id`),
  ADD KEY `zones_id` (`zones_id`);

--
-- Indices de la tabla `reasons_error`
--
ALTER TABLE `reasons_error`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `reason` (`reason`);

--
-- Indices de la tabla `reasons_visit`
--
ALTER TABLE `reasons_visit`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `reason` (`reason`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `rol` (`rol`);

--
-- Indices de la tabla `seasons`
--
ALTER TABLE `seasons`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `season` (`season`);

--
-- Indices de la tabla `sensors`
--
ALTER TABLE `sensors`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sensors_type` (`sensors_type`);

--
-- Indices de la tabla `sensors_doors`
--
ALTER TABLE `sensors_doors`
  ADD PRIMARY KEY (`sensors_id`,`doors_id`),
  ADD KEY `doors_id` (`doors_id`);

--
-- Indices de la tabla `sensors_type`
--
ALTER TABLE `sensors_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `type` (`type`);

--
-- Indices de la tabla `special_schedule`
--
ALTER TABLE `special_schedule`
  ADD PRIMARY KEY (`id`),
  ADD KEY `people_id` (`people_id`),
  ADD KEY `doors_id` (`doors_id`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user` (`user`),
  ADD KEY `people_id` (`people_id`),
  ADD KEY `roles_id` (`roles_id`),
  ADD KEY `users_state_id` (`users_state_id`);

--
-- Indices de la tabla `users_state`
--
ALTER TABLE `users_state`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `state` (`state`);

--
-- Indices de la tabla `vehicles`
--
ALTER TABLE `vehicles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `people_id` (`people_id`),
  ADD KEY `vehicles_type_id` (`vehicles_type_id`),
  ADD KEY `vehicles_profiles_id` (`vehicles_profiles_id`),
  ADD KEY `companies_id` (`companies_id`);

--
-- Indices de la tabla `vehicles_drivers`
--
ALTER TABLE `vehicles_drivers`
  ADD PRIMARY KEY (`vehicles_id`,`people_id`),
  ADD KEY `people_id` (`people_id`);

--
-- Indices de la tabla `vehicles_profiles`
--
ALTER TABLE `vehicles_profiles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `profile` (`profile`);

--
-- Indices de la tabla `vehicles_type`
--
ALTER TABLE `vehicles_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `type` (`type`);

--
-- Indices de la tabla `zones`
--
ALTER TABLE `zones`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `zone` (`zone`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `access_people`
--
ALTER TABLE `access_people`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT de la tabla `access_people_state_history`
--
ALTER TABLE `access_people_state_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT de la tabla `access_state`
--
ALTER TABLE `access_state`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `access_vehicles`
--
ALTER TABLE `access_vehicles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `access_vehicles_state_history`
--
ALTER TABLE `access_vehicles_state_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `answers_type`
--
ALTER TABLE `answers_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `areas`
--
ALTER TABLE `areas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `companies`
--
ALTER TABLE `companies`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `configurations`
--
ALTER TABLE `configurations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `doors`
--
ALTER TABLE `doors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `doors_type`
--
ALTER TABLE `doors_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT de la tabla `forms`
--
ALTER TABLE `forms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT de la tabla `internal_errors`
--
ALTER TABLE `internal_errors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `internal_success`
--
ALTER TABLE `internal_success`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `jornada`
--
ALTER TABLE `jornada`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `main_access`
--
ALTER TABLE `main_access`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `measures`
--
ALTER TABLE `measures`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `options`
--
ALTER TABLE `options`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `people`
--
ALTER TABLE `people`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT de la tabla `people_profiles`
--
ALTER TABLE `people_profiles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `projects`
--
ALTER TABLE `projects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `reasons_error`
--
ALTER TABLE `reasons_error`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `reasons_visit`
--
ALTER TABLE `reasons_visit`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `seasons`
--
ALTER TABLE `seasons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `sensors`
--
ALTER TABLE `sensors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `sensors_type`
--
ALTER TABLE `sensors_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `special_schedule`
--
ALTER TABLE `special_schedule`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `users_state`
--
ALTER TABLE `users_state`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `vehicles`
--
ALTER TABLE `vehicles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `vehicles_profiles`
--
ALTER TABLE `vehicles_profiles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT de la tabla `vehicles_type`
--
ALTER TABLE `vehicles_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `zones`
--
ALTER TABLE `zones`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `access_people`
--
ALTER TABLE `access_people`
  ADD CONSTRAINT `access_people_ibfk_1` FOREIGN KEY (`people_id`) REFERENCES `people` (`id`),
  ADD CONSTRAINT `access_people_ibfk_2` FOREIGN KEY (`access_state_id`) REFERENCES `access_state` (`id`),
  ADD CONSTRAINT `access_people_ibfk_3` FOREIGN KEY (`main_access_id`) REFERENCES `main_access` (`id`),
  ADD CONSTRAINT `access_people_ibfk_4` FOREIGN KEY (`approved_by`) REFERENCES `people` (`id`);

--
-- Filtros para la tabla `access_people_areas`
--
ALTER TABLE `access_people_areas`
  ADD CONSTRAINT `access_people_areas_ibfk_1` FOREIGN KEY (`access_people_id`) REFERENCES `access_people` (`id`),
  ADD CONSTRAINT `access_people_areas_ibfk_2` FOREIGN KEY (`areas_id`) REFERENCES `areas` (`id`);

--
-- Filtros para la tabla `access_people_department`
--
ALTER TABLE `access_people_department`
  ADD CONSTRAINT `access_people_department_ibfk_1` FOREIGN KEY (`access_people_id`) REFERENCES `access_people` (`id`),
  ADD CONSTRAINT `access_people_department_ibfk_2` FOREIGN KEY (`departments_id`) REFERENCES `departments` (`id`);

--
-- Filtros para la tabla `access_people_intents`
--
ALTER TABLE `access_people_intents`
  ADD CONSTRAINT `access_people_intents_ibfk_1` FOREIGN KEY (`access_people_id`) REFERENCES `access_people` (`id`),
  ADD CONSTRAINT `access_people_intents_ibfk_2` FOREIGN KEY (`doors_id`) REFERENCES `doors` (`id`);

--
-- Filtros para la tabla `access_people_reasons_visit`
--
ALTER TABLE `access_people_reasons_visit`
  ADD CONSTRAINT `access_people_reasons_visit_ibfk_1` FOREIGN KEY (`access_people_id`) REFERENCES `access_people` (`id`),
  ADD CONSTRAINT `access_people_reasons_visit_ibfk_2` FOREIGN KEY (`reasons_visit_id`) REFERENCES `reasons_visit` (`id`);

--
-- Filtros para la tabla `access_people_route`
--
ALTER TABLE `access_people_route`
  ADD CONSTRAINT `access_people_route_ibfk_1` FOREIGN KEY (`access_people_id`) REFERENCES `access_people` (`id`),
  ADD CONSTRAINT `access_people_route_ibfk_2` FOREIGN KEY (`doors_id`) REFERENCES `doors` (`id`);

--
-- Filtros para la tabla `access_people_state_history`
--
ALTER TABLE `access_people_state_history`
  ADD CONSTRAINT `access_people_state_history_ibfk_1` FOREIGN KEY (`access_people_id`) REFERENCES `access_people` (`id`),
  ADD CONSTRAINT `access_people_state_history_ibfk_2` FOREIGN KEY (`access_state_id`) REFERENCES `access_state` (`id`);

--
-- Filtros para la tabla `access_people_visit`
--
ALTER TABLE `access_people_visit`
  ADD CONSTRAINT `access_people_visit_ibfk_1` FOREIGN KEY (`access_people_id`) REFERENCES `access_people` (`id`),
  ADD CONSTRAINT `access_people_visit_ibfk_2` FOREIGN KEY (`people_id`) REFERENCES `people` (`id`);

--
-- Filtros para la tabla `access_people_zones`
--
ALTER TABLE `access_people_zones`
  ADD CONSTRAINT `access_people_zones_ibfk_1` FOREIGN KEY (`access_people_id`) REFERENCES `access_people` (`id`),
  ADD CONSTRAINT `access_people_zones_ibfk_2` FOREIGN KEY (`zones_id`) REFERENCES `zones` (`id`);

--
-- Filtros para la tabla `access_vehicles`
--
ALTER TABLE `access_vehicles`
  ADD CONSTRAINT `access_vehicles_ibfk_1` FOREIGN KEY (`vehicles_id`) REFERENCES `vehicles` (`id`),
  ADD CONSTRAINT `access_vehicles_ibfk_2` FOREIGN KEY (`access_state_id`) REFERENCES `access_state` (`id`),
  ADD CONSTRAINT `access_vehicles_ibfk_3` FOREIGN KEY (`main_access_id`) REFERENCES `main_access` (`id`),
  ADD CONSTRAINT `access_vehicles_ibfk_4` FOREIGN KEY (`approved_by`) REFERENCES `people` (`id`);

--
-- Filtros para la tabla `access_vehicles_answers`
--
ALTER TABLE `access_vehicles_answers`
  ADD CONSTRAINT `access_vehicles_answers_ibfk_1` FOREIGN KEY (`access_vehicles_id`) REFERENCES `access_vehicles` (`id`),
  ADD CONSTRAINT `access_vehicles_answers_ibfk_2` FOREIGN KEY (`forms_id`) REFERENCES `forms` (`id`);

--
-- Filtros para la tabla `access_vehicles_areas`
--
ALTER TABLE `access_vehicles_areas`
  ADD CONSTRAINT `access_vehicles_areas_ibfk_1` FOREIGN KEY (`access_vehicles_id`) REFERENCES `access_vehicles` (`id`),
  ADD CONSTRAINT `access_vehicles_areas_ibfk_2` FOREIGN KEY (`areas_id`) REFERENCES `areas` (`id`);

--
-- Filtros para la tabla `access_vehicles_departments`
--
ALTER TABLE `access_vehicles_departments`
  ADD CONSTRAINT `access_vehicles_departments_ibfk_1` FOREIGN KEY (`access_vehicles_id`) REFERENCES `access_vehicles` (`id`),
  ADD CONSTRAINT `access_vehicles_departments_ibfk_2` FOREIGN KEY (`departments_id`) REFERENCES `departments` (`id`);

--
-- Filtros para la tabla `access_vehicles_reasons_visit`
--
ALTER TABLE `access_vehicles_reasons_visit`
  ADD CONSTRAINT `access_vehicles_reasons_visit_ibfk_1` FOREIGN KEY (`access_vehicles_id`) REFERENCES `access_vehicles` (`id`),
  ADD CONSTRAINT `access_vehicles_reasons_visit_ibfk_2` FOREIGN KEY (`reasons_visit_id`) REFERENCES `reasons_visit` (`id`);

--
-- Filtros para la tabla `access_vehicles_route`
--
ALTER TABLE `access_vehicles_route`
  ADD CONSTRAINT `access_vehicles_route_ibfk_1` FOREIGN KEY (`access_vehicles_id`) REFERENCES `access_vehicles` (`id`),
  ADD CONSTRAINT `access_vehicles_route_ibfk_2` FOREIGN KEY (`doors_id`) REFERENCES `doors` (`id`);

--
-- Filtros para la tabla `access_vehicles_state_history`
--
ALTER TABLE `access_vehicles_state_history`
  ADD CONSTRAINT `access_vehicles_state_history_ibfk_1` FOREIGN KEY (`access_vehicles_id`) REFERENCES `access_vehicles` (`id`),
  ADD CONSTRAINT `access_vehicles_state_history_ibfk_2` FOREIGN KEY (`access_state_id`) REFERENCES `access_state` (`id`);

--
-- Filtros para la tabla `access_vehicles_visit`
--
ALTER TABLE `access_vehicles_visit`
  ADD CONSTRAINT `access_vehicles_visit_ibfk_1` FOREIGN KEY (`access_vehicles_id`) REFERENCES `access_vehicles` (`id`),
  ADD CONSTRAINT `access_vehicles_visit_ibfk_2` FOREIGN KEY (`people_id`) REFERENCES `people` (`id`);

--
-- Filtros para la tabla `access_vehicles_zones`
--
ALTER TABLE `access_vehicles_zones`
  ADD CONSTRAINT `access_vehicles_zones_ibfk_1` FOREIGN KEY (`access_vehicles_id`) REFERENCES `access_vehicles` (`id`),
  ADD CONSTRAINT `access_vehicles_zones_ibfk_2` FOREIGN KEY (`zones_id`) REFERENCES `zones` (`id`);

--
-- Filtros para la tabla `access_vehicle_intents`
--
ALTER TABLE `access_vehicle_intents`
  ADD CONSTRAINT `access_vehicle_intents_ibfk_1` FOREIGN KEY (`access_vehicle_id`) REFERENCES `access_vehicles` (`id`),
  ADD CONSTRAINT `access_vehicle_intents_ibfk_2` FOREIGN KEY (`doors_id`) REFERENCES `doors` (`id`);

--
-- Filtros para la tabla `areas`
--
ALTER TABLE `areas`
  ADD CONSTRAINT `areas_ibfk_1` FOREIGN KEY (`zones_id`) REFERENCES `zones` (`id`);

--
-- Filtros para la tabla `configurations`
--
ALTER TABLE `configurations`
  ADD CONSTRAINT `configurations_ibfk_1` FOREIGN KEY (`people_id`) REFERENCES `people` (`id`);

--
-- Filtros para la tabla `departments`
--
ALTER TABLE `departments`
  ADD CONSTRAINT `departments_ibfk_1` FOREIGN KEY (`areas_id`) REFERENCES `areas` (`id`),
  ADD CONSTRAINT `departments_ibfk_2` FOREIGN KEY (`in_charge`) REFERENCES `people` (`id`);

--
-- Filtros para la tabla `doors`
--
ALTER TABLE `doors`
  ADD CONSTRAINT `doors_ibfk_1` FOREIGN KEY (`doors_type_id`) REFERENCES `doors_type` (`id`);

--
-- Filtros para la tabla `doors_areas`
--
ALTER TABLE `doors_areas`
  ADD CONSTRAINT `doors_areas_ibfk_1` FOREIGN KEY (`doors_id`) REFERENCES `doors` (`id`),
  ADD CONSTRAINT `doors_areas_ibfk_2` FOREIGN KEY (`areas_id`) REFERENCES `areas` (`id`);

--
-- Filtros para la tabla `doors_departments`
--
ALTER TABLE `doors_departments`
  ADD CONSTRAINT `doors_departments_ibfk_1` FOREIGN KEY (`doors_id`) REFERENCES `doors` (`id`),
  ADD CONSTRAINT `doors_departments_ibfk_2` FOREIGN KEY (`departments_id`) REFERENCES `departments` (`id`);

--
-- Filtros para la tabla `doors_parents`
--
ALTER TABLE `doors_parents`
  ADD CONSTRAINT `doors_parents_ibfk_1` FOREIGN KEY (`doors_id`) REFERENCES `doors` (`id`);

--
-- Filtros para la tabla `doors_zones`
--
ALTER TABLE `doors_zones`
  ADD CONSTRAINT `doors_zones_ibfk_1` FOREIGN KEY (`doors_id`) REFERENCES `doors` (`id`),
  ADD CONSTRAINT `doors_zones_ibfk_2` FOREIGN KEY (`zones_id`) REFERENCES `zones` (`id`);

--
-- Filtros para la tabla `forms_detail`
--
ALTER TABLE `forms_detail`
  ADD CONSTRAINT `forms_detail_ibfk_1` FOREIGN KEY (`forms_id`) REFERENCES `forms` (`id`),
  ADD CONSTRAINT `forms_detail_ibfk_2` FOREIGN KEY (`answers_type_id`) REFERENCES `answers_type` (`id`);

--
-- Filtros para la tabla `forms_season`
--
ALTER TABLE `forms_season`
  ADD CONSTRAINT `forms_season_ibfk_1` FOREIGN KEY (`vechiles_type_id`) REFERENCES `vehicles_type` (`id`),
  ADD CONSTRAINT `forms_season_ibfk_2` FOREIGN KEY (`forms_id`) REFERENCES `forms` (`id`),
  ADD CONSTRAINT `forms_season_ibfk_3` FOREIGN KEY (`seasons_id`) REFERENCES `seasons` (`id`);

--
-- Filtros para la tabla `internal_errors`
--
ALTER TABLE `internal_errors`
  ADD CONSTRAINT `internal_errors_ibfk_1` FOREIGN KEY (`people_id`) REFERENCES `people` (`id`),
  ADD CONSTRAINT `internal_errors_ibfk_2` FOREIGN KEY (`sensors_id`) REFERENCES `sensors` (`id`),
  ADD CONSTRAINT `internal_errors_ibfk_3` FOREIGN KEY (`reasons_error_id`) REFERENCES `reasons_error` (`id`);

--
-- Filtros para la tabla `internal_success`
--
ALTER TABLE `internal_success`
  ADD CONSTRAINT `internal_success_ibfk_1` FOREIGN KEY (`people_id`) REFERENCES `people` (`id`),
  ADD CONSTRAINT `internal_success_ibfk_2` FOREIGN KEY (`sensors_id`) REFERENCES `sensors` (`id`);

--
-- Filtros para la tabla `options_roles`
--
ALTER TABLE `options_roles`
  ADD CONSTRAINT `options_roles_ibfk_1` FOREIGN KEY (`options_id`) REFERENCES `options` (`id`),
  ADD CONSTRAINT `options_roles_ibfk_2` FOREIGN KEY (`roles_id`) REFERENCES `roles` (`id`);

--
-- Filtros para la tabla `people`
--
ALTER TABLE `people`
  ADD CONSTRAINT `people_ibfk_1` FOREIGN KEY (`people_profiles_id`) REFERENCES `people_profiles` (`id`),
  ADD CONSTRAINT `people_ibfk_2` FOREIGN KEY (`companies_id`) REFERENCES `companies` (`id`);

--
-- Filtros para la tabla `profiles_doors_schedules`
--
ALTER TABLE `profiles_doors_schedules`
  ADD CONSTRAINT `profiles_doors_schedules_ibfk_1` FOREIGN KEY (`profiles_people_id`) REFERENCES `people_profiles` (`id`),
  ADD CONSTRAINT `profiles_doors_schedules_ibfk_2` FOREIGN KEY (`doors_id`) REFERENCES `doors` (`id`),
  ADD CONSTRAINT `profiles_doors_schedules_ibfk_3` FOREIGN KEY (`jornada_id`) REFERENCES `jornada` (`id`);

--
-- Filtros para la tabla `projects`
--
ALTER TABLE `projects`
  ADD CONSTRAINT `projects_ibfk_1` FOREIGN KEY (`in_charge`) REFERENCES `people` (`id`);

--
-- Filtros para la tabla `projects_areas`
--
ALTER TABLE `projects_areas`
  ADD CONSTRAINT `projects_areas_ibfk_1` FOREIGN KEY (`projects_id`) REFERENCES `projects` (`id`),
  ADD CONSTRAINT `projects_areas_ibfk_2` FOREIGN KEY (`areas_id`) REFERENCES `areas` (`id`);

--
-- Filtros para la tabla `projects_departments`
--
ALTER TABLE `projects_departments`
  ADD CONSTRAINT `projects_departments_ibfk_1` FOREIGN KEY (`projects_id`) REFERENCES `projects` (`id`),
  ADD CONSTRAINT `projects_departments_ibfk_2` FOREIGN KEY (`departments_id`) REFERENCES `departments` (`id`);

--
-- Filtros para la tabla `projects_intents`
--
ALTER TABLE `projects_intents`
  ADD CONSTRAINT `projects_intents_ibfk_1` FOREIGN KEY (`projects_id`) REFERENCES `projects` (`id`),
  ADD CONSTRAINT `projects_intents_ibfk_2` FOREIGN KEY (`doors_id`) REFERENCES `doors` (`id`);

--
-- Filtros para la tabla `projects_people`
--
ALTER TABLE `projects_people`
  ADD CONSTRAINT `projects_people_ibfk_1` FOREIGN KEY (`projects_id`) REFERENCES `projects` (`id`),
  ADD CONSTRAINT `projects_people_ibfk_2` FOREIGN KEY (`people_id`) REFERENCES `people` (`id`);

--
-- Filtros para la tabla `projects_route`
--
ALTER TABLE `projects_route`
  ADD CONSTRAINT `projects_route_ibfk_1` FOREIGN KEY (`projects_id`) REFERENCES `projects` (`id`),
  ADD CONSTRAINT `projects_route_ibfk_2` FOREIGN KEY (`doors_id`) REFERENCES `doors` (`id`);

--
-- Filtros para la tabla `projects_schedules`
--
ALTER TABLE `projects_schedules`
  ADD CONSTRAINT `projects_schedules_ibfk_1` FOREIGN KEY (`projects_id`) REFERENCES `projects` (`id`);

--
-- Filtros para la tabla `projects_zones`
--
ALTER TABLE `projects_zones`
  ADD CONSTRAINT `projects_zones_ibfk_1` FOREIGN KEY (`projects_id`) REFERENCES `projects` (`id`),
  ADD CONSTRAINT `projects_zones_ibfk_2` FOREIGN KEY (`zones_id`) REFERENCES `zones` (`id`);

--
-- Filtros para la tabla `sensors`
--
ALTER TABLE `sensors`
  ADD CONSTRAINT `sensors_ibfk_1` FOREIGN KEY (`sensors_type`) REFERENCES `sensors_type` (`id`);

--
-- Filtros para la tabla `sensors_doors`
--
ALTER TABLE `sensors_doors`
  ADD CONSTRAINT `sensors_doors_ibfk_1` FOREIGN KEY (`sensors_id`) REFERENCES `sensors` (`id`),
  ADD CONSTRAINT `sensors_doors_ibfk_2` FOREIGN KEY (`doors_id`) REFERENCES `doors` (`id`);

--
-- Filtros para la tabla `special_schedule`
--
ALTER TABLE `special_schedule`
  ADD CONSTRAINT `special_schedule_ibfk_1` FOREIGN KEY (`people_id`) REFERENCES `people` (`id`),
  ADD CONSTRAINT `special_schedule_ibfk_2` FOREIGN KEY (`doors_id`) REFERENCES `doors` (`id`);

--
-- Filtros para la tabla `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`people_id`) REFERENCES `people` (`id`),
  ADD CONSTRAINT `users_ibfk_2` FOREIGN KEY (`roles_id`) REFERENCES `roles` (`id`),
  ADD CONSTRAINT `users_ibfk_3` FOREIGN KEY (`users_state_id`) REFERENCES `users_state` (`id`);

--
-- Filtros para la tabla `vehicles`
--
ALTER TABLE `vehicles`
  ADD CONSTRAINT `vehicles_ibfk_1` FOREIGN KEY (`people_id`) REFERENCES `people` (`id`),
  ADD CONSTRAINT `vehicles_ibfk_2` FOREIGN KEY (`vehicles_type_id`) REFERENCES `vehicles_type` (`id`),
  ADD CONSTRAINT `vehicles_ibfk_3` FOREIGN KEY (`vehicles_profiles_id`) REFERENCES `vehicles_profiles` (`id`),
  ADD CONSTRAINT `vehicles_ibfk_4` FOREIGN KEY (`companies_id`) REFERENCES `companies` (`id`);

--
-- Filtros para la tabla `vehicles_drivers`
--
ALTER TABLE `vehicles_drivers`
  ADD CONSTRAINT `vehicles_drivers_ibfk_1` FOREIGN KEY (`vehicles_id`) REFERENCES `vehicles` (`id`),
  ADD CONSTRAINT `vehicles_drivers_ibfk_2` FOREIGN KEY (`people_id`) REFERENCES `people` (`id`);
