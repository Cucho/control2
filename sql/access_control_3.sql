/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.5.5-10.1.26-MariaDB : Database - access_control_3
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`access_control_3` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci */;

USE `access_control_3`;

/*Table structure for table `access_people` */

DROP TABLE IF EXISTS `access_people`;

CREATE TABLE `access_people` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entry` tinyint(1) NOT NULL,
  `hours` tinyint(2) DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `people_id` int(11) NOT NULL,
  `access_state_id` int(11) NOT NULL,
  `main_access_id` int(11) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `people_id` (`people_id`),
  KEY `access_state_id` (`access_state_id`),
  KEY `main_access_id` (`main_access_id`),
  CONSTRAINT `access_people_ibfk_1` FOREIGN KEY (`people_id`) REFERENCES `people` (`id`),
  CONSTRAINT `access_people_ibfk_2` FOREIGN KEY (`access_state_id`) REFERENCES `access_state` (`id`),
  CONSTRAINT `access_people_ibfk_3` FOREIGN KEY (`main_access_id`) REFERENCES `main_access` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `access_people` */

/*Table structure for table `access_people_areas` */

DROP TABLE IF EXISTS `access_people_areas`;

CREATE TABLE `access_people_areas` (
  `access_people_id` int(11) NOT NULL,
  `areas_id` int(11) NOT NULL,
  PRIMARY KEY (`access_people_id`,`areas_id`),
  KEY `areas_id` (`areas_id`),
  CONSTRAINT `access_people_areas_ibfk_1` FOREIGN KEY (`access_people_id`) REFERENCES `access_people` (`id`),
  CONSTRAINT `access_people_areas_ibfk_2` FOREIGN KEY (`areas_id`) REFERENCES `areas` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `access_people_areas` */

/*Table structure for table `access_people_department` */

DROP TABLE IF EXISTS `access_people_department`;

CREATE TABLE `access_people_department` (
  `access_people_id` int(11) NOT NULL,
  `departments_id` int(11) NOT NULL,
  PRIMARY KEY (`access_people_id`,`departments_id`),
  KEY `departments_id` (`departments_id`),
  CONSTRAINT `access_people_department_ibfk_1` FOREIGN KEY (`access_people_id`) REFERENCES `access_people` (`id`),
  CONSTRAINT `access_people_department_ibfk_2` FOREIGN KEY (`departments_id`) REFERENCES `departments` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `access_people_department` */

/*Table structure for table `access_people_reasons_visit` */

DROP TABLE IF EXISTS `access_people_reasons_visit`;

CREATE TABLE `access_people_reasons_visit` (
  `access_people_id` int(11) NOT NULL,
  `reasons_visit_id` int(11) NOT NULL,
  PRIMARY KEY (`access_people_id`,`reasons_visit_id`),
  KEY `reasons_visit_id` (`reasons_visit_id`),
  CONSTRAINT `access_people_reasons_visit_ibfk_1` FOREIGN KEY (`access_people_id`) REFERENCES `access_people` (`id`),
  CONSTRAINT `access_people_reasons_visit_ibfk_2` FOREIGN KEY (`reasons_visit_id`) REFERENCES `reasons_visit` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `access_people_reasons_visit` */

/*Table structure for table `access_people_route` */

DROP TABLE IF EXISTS `access_people_route`;

CREATE TABLE `access_people_route` (
  `access_people_id` int(11) NOT NULL,
  `doors_id` int(11) NOT NULL,
  PRIMARY KEY (`access_people_id`,`doors_id`),
  KEY `doors_id` (`doors_id`),
  CONSTRAINT `access_people_route_ibfk_1` FOREIGN KEY (`access_people_id`) REFERENCES `access_people` (`id`),
  CONSTRAINT `access_people_route_ibfk_2` FOREIGN KEY (`doors_id`) REFERENCES `doors` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `access_people_route` */

/*Table structure for table `access_people_visit` */

DROP TABLE IF EXISTS `access_people_visit`;

CREATE TABLE `access_people_visit` (
  `access_people_id` int(11) NOT NULL,
  `people_id` int(11) NOT NULL,
  PRIMARY KEY (`access_people_id`,`people_id`),
  KEY `people_id` (`people_id`),
  CONSTRAINT `access_people_visit_ibfk_1` FOREIGN KEY (`access_people_id`) REFERENCES `access_people` (`id`),
  CONSTRAINT `access_people_visit_ibfk_2` FOREIGN KEY (`people_id`) REFERENCES `people` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `access_people_visit` */

/*Table structure for table `access_people_zones` */

DROP TABLE IF EXISTS `access_people_zones`;

CREATE TABLE `access_people_zones` (
  `access_people_id` int(11) NOT NULL,
  `zones_id` int(11) NOT NULL,
  PRIMARY KEY (`access_people_id`,`zones_id`),
  KEY `zones_id` (`zones_id`),
  CONSTRAINT `access_people_zones_ibfk_1` FOREIGN KEY (`access_people_id`) REFERENCES `access_people` (`id`),
  CONSTRAINT `access_people_zones_ibfk_2` FOREIGN KEY (`zones_id`) REFERENCES `zones` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `access_people_zones` */

/*Table structure for table `access_state` */

DROP TABLE IF EXISTS `access_state`;

CREATE TABLE `access_state` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `state` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `state` (`state`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `access_state` */

/*Table structure for table `access_vehicles` */

DROP TABLE IF EXISTS `access_vehicles`;

CREATE TABLE `access_vehicles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entry` tinyint(1) NOT NULL,
  `hours` tinyint(2) NOT NULL,
  `end_time` datetime NOT NULL,
  `vehicles_id` int(11) NOT NULL,
  `access_state_id` int(11) NOT NULL,
  `main_access_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `vehicles_id` (`vehicles_id`),
  KEY `access_state_id` (`access_state_id`),
  KEY `main_access_id` (`main_access_id`),
  CONSTRAINT `access_vehicles_ibfk_1` FOREIGN KEY (`vehicles_id`) REFERENCES `vehicles` (`id`),
  CONSTRAINT `access_vehicles_ibfk_2` FOREIGN KEY (`access_state_id`) REFERENCES `access_state` (`id`),
  CONSTRAINT `access_vehicles_ibfk_3` FOREIGN KEY (`main_access_id`) REFERENCES `main_access` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `access_vehicles` */

/*Table structure for table `access_vehicles_answers` */

DROP TABLE IF EXISTS `access_vehicles_answers`;

CREATE TABLE `access_vehicles_answers` (
  `access_vehicles_id` int(11) NOT NULL,
  `forms_id` int(11) NOT NULL,
  `question` int(11) NOT NULL,
  `answer` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`access_vehicles_id`,`forms_id`,`question`),
  KEY `forms_id` (`forms_id`),
  CONSTRAINT `access_vehicles_answers_ibfk_1` FOREIGN KEY (`access_vehicles_id`) REFERENCES `access_vehicles` (`id`),
  CONSTRAINT `access_vehicles_answers_ibfk_2` FOREIGN KEY (`forms_id`) REFERENCES `forms` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `access_vehicles_answers` */

/*Table structure for table `access_vehicles_areas` */

DROP TABLE IF EXISTS `access_vehicles_areas`;

CREATE TABLE `access_vehicles_areas` (
  `access_vehicles_id` int(11) NOT NULL,
  `areas_id` int(11) NOT NULL,
  PRIMARY KEY (`access_vehicles_id`,`areas_id`),
  KEY `areas_id` (`areas_id`),
  CONSTRAINT `access_vehicles_areas_ibfk_1` FOREIGN KEY (`access_vehicles_id`) REFERENCES `access_vehicles` (`id`),
  CONSTRAINT `access_vehicles_areas_ibfk_2` FOREIGN KEY (`areas_id`) REFERENCES `areas` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `access_vehicles_areas` */

/*Table structure for table `access_vehicles_departments` */

DROP TABLE IF EXISTS `access_vehicles_departments`;

CREATE TABLE `access_vehicles_departments` (
  `access_vehicles_id` int(11) NOT NULL,
  `departments_id` int(11) NOT NULL,
  PRIMARY KEY (`access_vehicles_id`,`departments_id`),
  KEY `departments_id` (`departments_id`),
  CONSTRAINT `access_vehicles_departments_ibfk_1` FOREIGN KEY (`access_vehicles_id`) REFERENCES `access_vehicles` (`id`),
  CONSTRAINT `access_vehicles_departments_ibfk_2` FOREIGN KEY (`departments_id`) REFERENCES `departments` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `access_vehicles_departments` */

/*Table structure for table `access_vehicles_reasons_visit` */

DROP TABLE IF EXISTS `access_vehicles_reasons_visit`;

CREATE TABLE `access_vehicles_reasons_visit` (
  `access_vehicles_id` int(11) NOT NULL,
  `reasons_visit_id` int(11) NOT NULL,
  PRIMARY KEY (`access_vehicles_id`,`reasons_visit_id`),
  KEY `reasons_visit_id` (`reasons_visit_id`),
  CONSTRAINT `access_vehicles_reasons_visit_ibfk_1` FOREIGN KEY (`access_vehicles_id`) REFERENCES `access_vehicles` (`id`),
  CONSTRAINT `access_vehicles_reasons_visit_ibfk_2` FOREIGN KEY (`reasons_visit_id`) REFERENCES `reasons_visit` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `access_vehicles_reasons_visit` */

/*Table structure for table `access_vehicles_route` */

DROP TABLE IF EXISTS `access_vehicles_route`;

CREATE TABLE `access_vehicles_route` (
  `access_vehicles_id` int(11) NOT NULL,
  `doors_id` int(11) NOT NULL,
  PRIMARY KEY (`access_vehicles_id`,`doors_id`),
  KEY `doors_id` (`doors_id`),
  CONSTRAINT `access_vehicles_route_ibfk_1` FOREIGN KEY (`access_vehicles_id`) REFERENCES `access_vehicles` (`id`),
  CONSTRAINT `access_vehicles_route_ibfk_2` FOREIGN KEY (`doors_id`) REFERENCES `doors` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `access_vehicles_route` */

/*Table structure for table `access_vehicles_visit` */

DROP TABLE IF EXISTS `access_vehicles_visit`;

CREATE TABLE `access_vehicles_visit` (
  `access_vehicles_id` int(11) NOT NULL,
  `people_id` int(11) NOT NULL,
  PRIMARY KEY (`access_vehicles_id`,`people_id`),
  KEY `people_id` (`people_id`),
  CONSTRAINT `access_vehicles_visit_ibfk_1` FOREIGN KEY (`access_vehicles_id`) REFERENCES `access_vehicles` (`id`),
  CONSTRAINT `access_vehicles_visit_ibfk_2` FOREIGN KEY (`people_id`) REFERENCES `people` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `access_vehicles_visit` */

/*Table structure for table `access_vehicles_zones` */

DROP TABLE IF EXISTS `access_vehicles_zones`;

CREATE TABLE `access_vehicles_zones` (
  `access_vehicles_id` int(11) NOT NULL,
  `zones_id` int(11) NOT NULL,
  PRIMARY KEY (`access_vehicles_id`,`zones_id`),
  KEY `zones_id` (`zones_id`),
  CONSTRAINT `access_vehicles_zones_ibfk_1` FOREIGN KEY (`access_vehicles_id`) REFERENCES `access_vehicles` (`id`),
  CONSTRAINT `access_vehicles_zones_ibfk_2` FOREIGN KEY (`zones_id`) REFERENCES `zones` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `access_vehicles_zones` */

/*Table structure for table `answers_type` */

DROP TABLE IF EXISTS `answers_type`;

CREATE TABLE `answers_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `type` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `answers_type` */

/*Table structure for table `areas` */

DROP TABLE IF EXISTS `areas`;

CREATE TABLE `areas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `area` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `zones_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `area` (`area`),
  KEY `zones_id` (`zones_id`),
  CONSTRAINT `areas_ibfk_1` FOREIGN KEY (`zones_id`) REFERENCES `zones` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `areas` */

/*Table structure for table `companies` */

DROP TABLE IF EXISTS `companies`;

CREATE TABLE `companies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `companies` */

/*Table structure for table `configurations` */

DROP TABLE IF EXISTS `configurations`;

CREATE TABLE `configurations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cod_company` int(11) NOT NULL,
  `company` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cod_installation` int(11) NOT NULL,
  `installation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `people_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `people_id` (`people_id`),
  CONSTRAINT `configurations_ibfk_1` FOREIGN KEY (`people_id`) REFERENCES `people` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `configurations` */

/*Table structure for table `departments` */

DROP TABLE IF EXISTS `departments`;

CREATE TABLE `departments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `department` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `areas_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `department` (`department`),
  KEY `areas_id` (`areas_id`),
  CONSTRAINT `departments_ibfk_1` FOREIGN KEY (`areas_id`) REFERENCES `areas` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `departments` */

/*Table structure for table `doors` */

DROP TABLE IF EXISTS `doors`;

CREATE TABLE `doors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `door` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `doors_type_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `doors_type_id` (`doors_type_id`),
  CONSTRAINT `doors_ibfk_1` FOREIGN KEY (`doors_type_id`) REFERENCES `doors_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `doors` */

/*Table structure for table `doors_areas` */

DROP TABLE IF EXISTS `doors_areas`;

CREATE TABLE `doors_areas` (
  `doors_id` int(11) NOT NULL,
  `areas_id` int(11) NOT NULL,
  PRIMARY KEY (`doors_id`,`areas_id`),
  KEY `areas_id` (`areas_id`),
  CONSTRAINT `doors_areas_ibfk_1` FOREIGN KEY (`doors_id`) REFERENCES `doors` (`id`),
  CONSTRAINT `doors_areas_ibfk_2` FOREIGN KEY (`areas_id`) REFERENCES `areas` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `doors_areas` */

/*Table structure for table `doors_departments` */

DROP TABLE IF EXISTS `doors_departments`;

CREATE TABLE `doors_departments` (
  `doors_id` int(11) NOT NULL,
  `departments_id` int(11) NOT NULL,
  PRIMARY KEY (`doors_id`,`departments_id`),
  KEY `departments_id` (`departments_id`),
  CONSTRAINT `doors_departments_ibfk_1` FOREIGN KEY (`doors_id`) REFERENCES `doors` (`id`),
  CONSTRAINT `doors_departments_ibfk_2` FOREIGN KEY (`departments_id`) REFERENCES `departments` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `doors_departments` */

/*Table structure for table `doors_type` */

DROP TABLE IF EXISTS `doors_type`;

CREATE TABLE `doors_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `type` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `doors_type` */

/*Table structure for table `doors_zones` */

DROP TABLE IF EXISTS `doors_zones`;

CREATE TABLE `doors_zones` (
  `doors_id` int(11) NOT NULL,
  `zones_id` int(11) NOT NULL,
  PRIMARY KEY (`doors_id`,`zones_id`),
  KEY `zones_id` (`zones_id`),
  CONSTRAINT `doors_zones_ibfk_1` FOREIGN KEY (`doors_id`) REFERENCES `doors` (`id`),
  CONSTRAINT `doors_zones_ibfk_2` FOREIGN KEY (`zones_id`) REFERENCES `zones` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `doors_zones` */

/*Table structure for table `forms` */

DROP TABLE IF EXISTS `forms`;

CREATE TABLE `forms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `forms` */

/*Table structure for table `forms_detail` */

DROP TABLE IF EXISTS `forms_detail`;

CREATE TABLE `forms_detail` (
  `order` int(11) NOT NULL,
  `question` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `forms_id` int(11) NOT NULL,
  `answers_type_id` int(11) NOT NULL,
  PRIMARY KEY (`order`,`forms_id`),
  KEY `forms_id` (`forms_id`),
  KEY `answers_type_id` (`answers_type_id`),
  CONSTRAINT `forms_detail_ibfk_1` FOREIGN KEY (`forms_id`) REFERENCES `forms` (`id`),
  CONSTRAINT `forms_detail_ibfk_2` FOREIGN KEY (`answers_type_id`) REFERENCES `answers_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `forms_detail` */

/*Table structure for table `forms_season` */

DROP TABLE IF EXISTS `forms_season`;

CREATE TABLE `forms_season` (
  `vechiles_type_id` int(11) NOT NULL,
  `forms_id` int(11) NOT NULL,
  `seasons_id` int(11) NOT NULL,
  `year` tinyint(4) NOT NULL,
  PRIMARY KEY (`vechiles_type_id`,`forms_id`,`seasons_id`,`year`),
  KEY `forms_id` (`forms_id`),
  KEY `seasons_id` (`seasons_id`),
  CONSTRAINT `forms_season_ibfk_1` FOREIGN KEY (`vechiles_type_id`) REFERENCES `vehicles_type` (`id`),
  CONSTRAINT `forms_season_ibfk_2` FOREIGN KEY (`forms_id`) REFERENCES `forms` (`id`),
  CONSTRAINT `forms_season_ibfk_3` FOREIGN KEY (`seasons_id`) REFERENCES `seasons` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `forms_season` */

/*Table structure for table `internal_errors` */

DROP TABLE IF EXISTS `internal_errors`;

CREATE TABLE `internal_errors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `people_id` int(11) NOT NULL,
  `sensors_id` int(11) NOT NULL,
  `reasons_error_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `people_id` (`people_id`),
  KEY `sensors_id` (`sensors_id`),
  KEY `reasons_error_id` (`reasons_error_id`),
  CONSTRAINT `internal_errors_ibfk_1` FOREIGN KEY (`people_id`) REFERENCES `people` (`id`),
  CONSTRAINT `internal_errors_ibfk_2` FOREIGN KEY (`sensors_id`) REFERENCES `sensors` (`id`),
  CONSTRAINT `internal_errors_ibfk_3` FOREIGN KEY (`reasons_error_id`) REFERENCES `reasons_error` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `internal_errors` */

/*Table structure for table `internal_success` */

DROP TABLE IF EXISTS `internal_success`;

CREATE TABLE `internal_success` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `people_id` int(11) NOT NULL,
  `sensors_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `people_id` (`people_id`),
  KEY `sensors_id` (`sensors_id`),
  CONSTRAINT `internal_success_ibfk_1` FOREIGN KEY (`people_id`) REFERENCES `people` (`id`),
  CONSTRAINT `internal_success_ibfk_2` FOREIGN KEY (`sensors_id`) REFERENCES `seasons` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `internal_success` */

/*Table structure for table `main_access` */

DROP TABLE IF EXISTS `main_access`;

CREATE TABLE `main_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ubication` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ip_host` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_host` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `entry` tinyint(1) NOT NULL,
  `flow` tinyint(1) NOT NULL,
  `internal` tinyint(1) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `main_access` */

/*Table structure for table `measures` */

DROP TABLE IF EXISTS `measures`;

CREATE TABLE `measures` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `measure` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `measure` (`measure`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `measures` */

/*Table structure for table `options` */

DROP TABLE IF EXISTS `options`;

CREATE TABLE `options` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `option` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `option` (`option`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `options` */

/*Table structure for table `options_roles` */

DROP TABLE IF EXISTS `options_roles`;

CREATE TABLE `options_roles` (
  `options_id` int(11) NOT NULL,
  `roles_id` int(11) NOT NULL,
  PRIMARY KEY (`options_id`,`roles_id`),
  KEY `roles_id` (`roles_id`),
  CONSTRAINT `options_roles_ibfk_1` FOREIGN KEY (`options_id`) REFERENCES `options` (`id`),
  CONSTRAINT `options_roles_ibfk_2` FOREIGN KEY (`roles_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `options_roles` */

/*Table structure for table `people` */

DROP TABLE IF EXISTS `people`;

CREATE TABLE `people` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rut` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `allow_all` tinyint(1) DEFAULT NULL,
  `is_visited` tinyint(1) DEFAULT NULL,
  `people_profiles_id` int(11) NOT NULL,
  `companies_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `rut` (`rut`),
  KEY `profiles_people_id` (`people_profiles_id`),
  KEY `companies_id` (`companies_id`),
  CONSTRAINT `people_ibfk_1` FOREIGN KEY (`people_profiles_id`) REFERENCES `people_profiles` (`id`),
  CONSTRAINT `people_ibfk_2` FOREIGN KEY (`companies_id`) REFERENCES `companies` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `people` */

/*Table structure for table `people_profiles` */

DROP TABLE IF EXISTS `people_profiles`;

CREATE TABLE `people_profiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `profile` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `profile` (`profile`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `people_profiles` */

/*Table structure for table `profiles_doors_schedules` */

DROP TABLE IF EXISTS `profiles_doors_schedules`;

CREATE TABLE `profiles_doors_schedules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `profiles_people_id` int(11) NOT NULL,
  `doors_id` int(11) NOT NULL,
  `time_init` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time_end` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `profiles_people_id` (`profiles_people_id`),
  KEY `doors_id` (`doors_id`),
  CONSTRAINT `profiles_doors_schedules_ibfk_1` FOREIGN KEY (`profiles_people_id`) REFERENCES `people_profiles` (`id`),
  CONSTRAINT `profiles_doors_schedules_ibfk_2` FOREIGN KEY (`doors_id`) REFERENCES `doors` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `profiles_doors_schedules` */

/*Table structure for table `reasons_error` */

DROP TABLE IF EXISTS `reasons_error`;

CREATE TABLE `reasons_error` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reason` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `reason` (`reason`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `reasons_error` */

/*Table structure for table `reasons_visit` */

DROP TABLE IF EXISTS `reasons_visit`;

CREATE TABLE `reasons_visit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reason` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `reason` (`reason`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `reasons_visit` */

/*Table structure for table `roles` */

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rol` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `rol` (`rol`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `roles` */

/*Table structure for table `seasons` */

DROP TABLE IF EXISTS `seasons`;

CREATE TABLE `seasons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `season` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `season` (`season`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `seasons` */

/*Table structure for table `sensors` */

DROP TABLE IF EXISTS `sensors`;

CREATE TABLE `sensors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sensor` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ip` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sensors_type` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `sensors_type` (`sensors_type`),
  CONSTRAINT `sensors_ibfk_1` FOREIGN KEY (`sensors_type`) REFERENCES `sensors_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `sensors` */

/*Table structure for table `sensors_doors` */

DROP TABLE IF EXISTS `sensors_doors`;

CREATE TABLE `sensors_doors` (
  `sensors_id` int(11) NOT NULL,
  `doors_id` int(11) NOT NULL,
  PRIMARY KEY (`sensors_id`,`doors_id`),
  KEY `doors_id` (`doors_id`),
  CONSTRAINT `sensors_doors_ibfk_1` FOREIGN KEY (`sensors_id`) REFERENCES `sensors` (`id`),
  CONSTRAINT `sensors_doors_ibfk_2` FOREIGN KEY (`doors_id`) REFERENCES `doors` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `sensors_doors` */

/*Table structure for table `sensors_type` */

DROP TABLE IF EXISTS `sensors_type`;

CREATE TABLE `sensors_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `type` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `sensors_type` */

/*Table structure for table `special_schedule` */

DROP TABLE IF EXISTS `special_schedule`;

CREATE TABLE `special_schedule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `people_id` int(11) NOT NULL,
  `doors_id` int(11) NOT NULL,
  `date_init` datetime NOT NULL,
  `date_end` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `people_id` (`people_id`),
  KEY `doors_id` (`doors_id`),
  CONSTRAINT `special_schedule_ibfk_1` FOREIGN KEY (`people_id`) REFERENCES `people` (`id`),
  CONSTRAINT `special_schedule_ibfk_2` FOREIGN KEY (`doors_id`) REFERENCES `doors` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `special_schedule` */

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles_id` int(11) NOT NULL,
  `people_id` int(11) NOT NULL,
  `users_state_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user` (`user`),
  KEY `people_id` (`people_id`),
  KEY `roles_id` (`roles_id`),
  KEY `users_state_id` (`users_state_id`),
  CONSTRAINT `users_ibfk_1` FOREIGN KEY (`people_id`) REFERENCES `people` (`id`),
  CONSTRAINT `users_ibfk_2` FOREIGN KEY (`roles_id`) REFERENCES `roles` (`id`),
  CONSTRAINT `users_ibfk_3` FOREIGN KEY (`users_state_id`) REFERENCES `users_state` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `users` */

/*Table structure for table `users_state` */

DROP TABLE IF EXISTS `users_state`;

CREATE TABLE `users_state` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `state` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `state` (`state`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `users_state` */

/*Table structure for table `vehicles` */

DROP TABLE IF EXISTS `vehicles`;

CREATE TABLE `vehicles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patent` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `internal` tinyint(1) NOT NULL,
  `companies_id` int(11) NOT NULL,
  `people_id` int(11) NOT NULL,
  `vehicles_type_id` int(11) NOT NULL,
  `vehicles_profiles_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `people_id` (`people_id`),
  KEY `vehicles_type_id` (`vehicles_type_id`),
  KEY `vehicles_profiles_id` (`vehicles_profiles_id`),
  KEY `companies_id` (`companies_id`),
  CONSTRAINT `vehicles_ibfk_1` FOREIGN KEY (`people_id`) REFERENCES `people` (`id`),
  CONSTRAINT `vehicles_ibfk_2` FOREIGN KEY (`vehicles_type_id`) REFERENCES `vehicles_type` (`id`),
  CONSTRAINT `vehicles_ibfk_3` FOREIGN KEY (`vehicles_profiles_id`) REFERENCES `vehicles_profiles` (`id`),
  CONSTRAINT `vehicles_ibfk_4` FOREIGN KEY (`companies_id`) REFERENCES `companies` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `vehicles` */

/*Table structure for table `vehicles_profiles` */

DROP TABLE IF EXISTS `vehicles_profiles`;

CREATE TABLE `vehicles_profiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `profile` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `profile` (`profile`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `vehicles_profiles` */

/*Table structure for table `vehicles_type` */

DROP TABLE IF EXISTS `vehicles_type`;

CREATE TABLE `vehicles_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `type` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `vehicles_type` */

/*Table structure for table `zones` */

DROP TABLE IF EXISTS `zones`;

CREATE TABLE `zones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `zone` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `zone` (`zone`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `zones` */

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
