<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class CProjects extends CI_Controller {

	function __construct() {
		parent::__construct();
		$this->load->model('mProjects', 'modelo');
	}

	public function index() {
		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('projects/index');
	}

	//Datatable
	public function datatable() {
		$start = $this->input->post('start');
		$length = $this->input->post('length');
		$search = $this->input->post('search')['value'];
		$by = $this->input->post('order')['0']['column'];
		$order = $this->input->post('order')['0']['dir'];

		$result = $this->modelo->getProjects($start, $length, $search, $order, $by);
		
		$json_data = array(
			"draw"            => intval($this->input->post('draw')),
            "recordsTotal"    => intval($result['numDataTotal']),
            "recordsFiltered" => intval($result['numDataFilter']),
            "data"            => $result['data'],
            );

        echo json_encode($json_data);
	}

	//Vistas
	public function add() {
		$profile = $this->modelo->getAllProfiles();
		$companies = $this->modelo->getAllCompanies();
		$vehicle_type = $this->modelo->getAllVehicle_Type();
		$vehicle_profiles = $this->modelo->getAllVehicleProfiles();
		$peopleInternal = $this->modelo->getAllPeopleInternal();
		$treeview = $this->modelo->GetTreeview();
		$doorlevel = $this->modelo->getDoorLevel();
		$minimum = $this->modelo->getAllMinimum();

		$data = array(
			'profiles' => $profile,
			'companies' => $companies,
			'vechicle_type' => $vehicle_type,
			'vehicle_profiles' => $vehicle_profiles,
			'peopleInternal' => $peopleInternal,
			'treeview' => $treeview,
			'doorlevel' => $doorlevel,
			'minimum' => $minimum
		);

		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('projects/add', $data);
	}

	public function edit() {
		$profile = $this->modelo->getAllProfiles();
		$companies = $this->modelo->getAllCompanies();
		$vehicle_type = $this->modelo->getAllVehicle_Type();
		$vehicle_profiles = $this->modelo->getAllVehicleProfiles();
		$peopleInternal = $this->modelo->getAllPeopleInternal();
		$treeview = $this->modelo->GetTreeview();
		$doorlevel = $this->modelo->getDoorLevel();
		$minimum = $this->modelo->getAllMinimum();

		$id = trim($this->input->get('id', TRUE));

		$projects = $this->modelo->getProject($id);
		$pexternal = $this->modelo->getPeopleExternal($id);
		$vehicles = $this->modelo->getVehicles($id);
		$horarios = $this->modelo->getHorarios($id);
		$encargado = $this->modelo->getEncargado($id);
		$zonas = $this->modelo->getZones($id);
		$areas = $this->modelo->getAreas($id);
		$dptos = $this->modelo->getDptos($id);
		$route = $this->modelo->getRoute($id);
		$intent = $this->modelo->getIntent($id);
		$requirement = $this->modelo->getRequeriment($id);
		$document = $this->listDocument($id);

		$data = array(
			'projects' => $projects,
			'pexternal' => $pexternal,
			'vehicles' => $vehicles,
			'horarios' => $horarios,
			'encargado' => $encargado,
			'zonas' => $zonas,
			'areas' => $areas,
			'dptos' => $dptos,
			'route' => $route,
			'intent' => $intent,
			'requirement' => $requirement,
			'document' => $document,

			'profiles' => $profile,
			'companies' => $companies,
			'vechicle_type' => $vehicle_type,
			'vehicle_profiles' => $vehicle_profiles,
			'peopleInternal' => $peopleInternal,
			'treeview' => $treeview,
			'doorlevel' => $doorlevel,
			'minimum' => $minimum
		);

		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('projects/edit', $data);
	}

	public function view() {
		$id = trim($this->input->get('id', TRUE));

		$projects = $this->modelo->getProject($id);
		$pexternal = $this->modelo->getPeopleExternal($id);
		$vehicles = $this->modelo->getVehicles($id);
		$horarios = $this->modelo->getHorarios($id);
		$encargado = $this->modelo->getEncargado($id);
		$zonas = $this->modelo->getZones($id);
		$areas = $this->modelo->getAreas($id);
		$dptos = $this->modelo->getDptos($id);
		$route = $this->modelo->getRoute($id);
		$intent = $this->modelo->getIntent($id);
		$requirement = $this->modelo->getRequeriment($id);
		$document = $this->listDocument($id);

		$data = array(
			'projects' => $projects,
			'pexternal' => $pexternal,
			'vehicles' => $vehicles,
			'horarios' => $horarios,
			'encargado' => $encargado,
			'zonas' => $zonas,
			'areas' => $areas,
			'dptos' => $dptos,
			'route' => $route,
			'intent' => $intent,
			'requirement' => $requirement,
			'document' => $document
		);

		$this->load->view('header');
		$this->load->view('aside');
		$this->load->view('projects/view', $data);
	}

	public function SearchPeople(){
		$rut = trim($this->input->post('rut', TRUE));
		$people = $this->modelo->searchPeople($rut);
		
		if (!empty($people)) {
			$data = array(
				'mensaje' 	=> 1,
				'people' 	=> $people
			);
		}
		else {
			$data = array(
				'mensaje' => 0
			);
		}
		echo json_encode($data);
	}

	public function addPeople(){
		$rut                 = trim($this->input->post('rut', TRUE)); 
        $digit               = trim($this->input->post('digit', TRUE)); 
        $name                = trim($this->input->post('name', TRUE)); 
        $last_name           = trim($this->input->post('last_name', TRUE)); 
        $address             = trim($this->input->post('address', TRUE)); 
        $email               = trim($this->input->post('email', TRUE)); 
        $phone               = trim($this->input->post('phone', TRUE)); 
        $allow_all           = trim($this->input->post('allow_all', TRUE)); 
        $is_visited          = trim($this->input->post('is_visited', TRUE)); 
        $internal            = trim($this->input->post('internal', TRUE)); 
        $people_profiles_id  = trim($this->input->post('people_profiles_id', TRUE)); 
        $companies_id        = trim($this->input->post('companies_id', TRUE));

        $date_time = date('Y-m-d H:i:s');

        $data = array(
        	'rut' 					=> $rut,
        	'digit' 				=> $digit,
        	'name' 					=> $name,
        	'last_name' 			=> $last_name,
        	'address' 				=> $address,
        	'email' 				=> $email,
        	'phone' 				=> $phone,
        	'allow_all' 			=> $allow_all,
        	'internal' 				=> $internal,
        	'people_profiles_id' 	=> $people_profiles_id,
        	'companies_id' 			=> $companies_id,
        	'created'				=> $date_time,
        	'modified'				=> $date_time
        );
        if ($this->modelo->addPeople($data)) {
        	echo '1';
        }
        else {
        	echo '0';
        }        
	}

	public function SearchVehicle(){
		$patent = trim($this->input->post('patent', TRUE));
		$vehicle = $this->modelo->SearchVehicle($patent);
		if (!empty($vehicle)) {
			$data = array(
				'mensaje' => 1,
				'vehicle' => $vehicle
			);
		}
		else {
			$data = array(
				'mensaje' => 0
			);
		}
		echo json_encode($data);
	}

	public function addVehicle(){
		$patent             	= trim($this->input->post("patent", TRUE));
        $model              	= trim($this->input->post("model", TRUE));
        $internal           	= trim($this->input->post("internal", TRUE));
        $nfc_code           	= trim($this->input->post("nfc_code", TRUE));
        $companies_id       	= trim($this->input->post("companies_id", TRUE));
        $rut           			= trim($this->input->post("people_id", TRUE));
        $vehicles_type_id		= trim($this->input->post("vehicles_type_id", TRUE));
        $vehicles_profiles_id 	= trim($this->input->post("vehicles_profiles_id", TRUE));

        $people_id = '';
        foreach ($this->modelo->searchPeople($rut) as $k) {
        	$people_id = $k->id;
        }

        $date_time = date('Y-m-d H:i:s');

        $data = array(
        	'patent' 				=> $patent,
        	'model' 				=> $model,
        	'internal' 				=> $internal,
        	'nfc_code' 				=> $nfc_code,
        	'companies_id' 			=> $companies_id,
        	'people_id' 			=> $people_id,
        	'vehicles_type_id' 		=> $vehicles_type_id,
        	'vehicles_profiles_id' 	=> $vehicles_profiles_id,
        	'created'				=> $date_time,
        	'modified'				=> $date_time
        );
        if ($this->modelo->addVehicle($data)) {
        	echo '1';
        }
        else {
        	echo '0';
        }
    }

    public function addRequirement(){
    	$requirement = trim($this->input->post('requirement', TRUE));
    	$description = trim($this->input->post('description', TRUE));

		$date_time = date('Y-m-d H:i:s');

		$dato = array(
			'requirement' 	=> $requirement,
			'description' 	=> $description,
			'created' 		=> $date_time,
			'modified' 		=> $date_time
		);

		$id = $this->modelo->addRequirement($dato);

		if ($id != false) {
			$data = array('mensaje' => 1, 'id' => $id);
    	}
    	else {
    		$data = array('mensaje' => 0);
    	}
    	echo json_encode($data);    	
    }

	//Crud
	public function addProjects() {
		$title 			= trim($this->input->post('title', TRUE));
		$description 	= trim($this->input->post('description', TRUE));
		$date_init		= trim($this->input->post('init', TRUE));
		$date_end 		= trim($this->input->post('end', TRUE));

		$contratistas 	= $this->input->post('contratistas');

		$vehicles 		= $this->input->post('vehicles');
		$horarios 		= $this->input->post('horarios');
		$encargado 		= trim($this->input->post('encargado', TRUE));
		$puertas 		= $this->input->post('puerta');
		$requirements 	= $this->input->post('requirements');
		$areas 			= $this->input->post('areas');
		$dptos 			= $this->input->post('dptos');
		$zonas 			= $this->input->post('zonas');

		$vueltas 		= $this->input->post('vueltas');

		$control_init 	= $this->input->post('control_init');
		$control_end 	= $this->input->post('control_end');

		$date_time 		= date('Y-m-d H:i:s');

		$data = array(
			'title' 					=> $title,
			'description' 				=> $description,
			'in_charge' 				=> $this->modelo->searchId($contratistas[0]),
			'in_charge_installation' 	=> $encargado,
			'init' 						=> $date_init,
			'end'	 					=> $date_end,
			'created' 					=> $date_time,
			'modified' 					=> $date_time
		);

		$projects_id = $this->modelo->addProjects($data);
		if ($projects_id != false) {
			for ($i=0; $i < count($areas); $i++) { 
				$data = array('projects_id' => $projects_id, 'areas_id' => $areas[$i]);
				$this->modelo->addProjectAreas($data);
			}
			for ($i=0; $i < count($dptos); $i++) { 
				$data = array('projects_id' => $projects_id, 'departments_id' => $dptos[$i]);
				$this->modelo->addProjectDptos($data);
			}
			for ($i=0; $i < count($zonas); $i++) { 
				$data = array('projects_id' => $projects_id, 'zones_id' => $zonas[$i]);
				$this->modelo->addProjectZones($data);
			}
			for ($i=0; $i < count($puertas); $i++) { 
				$data = array('projects_id' => $projects_id, 'doors_id' => $puertas[$i]);
				$this->modelo->addProjectRoute($data);
			}
			for ($i=0; $i < count($requirements); $i++) { 
				$data = array('projects_id' => $projects_id, 'minimum_requirements' => $requirements[$i]);
				$this->modelo->addProjectRequirement($data);
			}
			for ($i=0; $i < count($contratistas); $i++) { 
				$data = array(
					'projects_id' => $projects_id, 
					'people_id' => $this->modelo->searchId($contratistas[$i])
				);
				$this->modelo->addProjectPeople($data);
			}
			for ($i=0; $i < count($vehicles); $i++) { 
				$data = array(
					'projects_id' 	=> $projects_id, 
					'vehicles_id' 	=> $this->modelo->searchByPatent($vehicles[$i]),
					'control_init' 	=> $control_init[$i],
					'control_end' 	=> $control_end[$i]
				);
				$this->modelo->addProjectVehicles($data);
			}
			$vueltas = count($horarios)/9;
			$j = 0;
			for ($i=0; $i < $vueltas; $i++) { 
				$data = array(
					'projects_id' 	=> $projects_id,
					'time_init' 	=> $horarios[$j],
					'time_end' 		=> $horarios[$j+1],
					'L' 			=> $horarios[$j+2],
					'M' 			=> $horarios[$j+3],
					'Mi' 			=> $horarios[$j+4],
					'J' 			=> $horarios[$j+5],
					'V' 			=> $horarios[$j+6],
					'S' 			=> $horarios[$j+7],
					'D' 			=> $horarios[$j+8]
				);
				$this->modelo->addProjectSchedules($data);
				$j += 9;
			}
			$data = array('mensaje' => '1', 'project' => $projects_id);
			echo json_encode($data);
		}
		else {
			$data = array('mensaje' => '0');
			echo json_encode($data);
		}
	}

	public function editProjects(){
		$id 			= $this->input->post('projects');
		$title 			= trim($this->input->post('title', TRUE));
		$description 	= trim($this->input->post('description', TRUE));
		$date_init		= trim($this->input->post('init', TRUE));
		$date_end 		= trim($this->input->post('end', TRUE));

		$contratistas 	= $this->input->post('contratistas');

		$vehicles 		= $this->input->post('vehicles');
		$horarios 		= $this->input->post('horarios');
		$encargado 		= trim($this->input->post('encargado', TRUE));
		$puertas 		= $this->input->post('puerta');
		$requirements 	= $this->input->post('requirements');
		$areas 			= $this->input->post('areas');
		$dptos 			= $this->input->post('dptos');
		$zonas 			= $this->input->post('zonas');

		$vueltas 		= $this->input->post('vueltas');

		$control_init 	= $this->input->post('control_init');
		$control_end 	= $this->input->post('control_end');

		$date_time 		= date('Y-m-d H:i:s');

		///////
		$data = array(
			'id'						=> $id,
			'title' 					=> $title,
			'description' 				=> $description,
			'in_charge' 				=> $this->modelo->searchId($contratistas[0]),
			'in_charge_installation' 	=> $encargado,
			'init' 						=> $date_init,
			'end'	 					=> $date_end,
			'created' 					=> $date_time,
			'modified' 					=> $date_time
		);

		$projects_id = $this->modelo->editProjects($data);

		if ($projects_id != false) {
			$this->modelo->editProjectAreas($id);
			for ($i=0; $i < count($areas); $i++) { 
				$data = array('projects_id' => $id, 'areas_id' => $areas[$i]);
				$this->modelo->addProjectAreas($data);
			}
			$this->modelo->editProjectDptos($id);
			for ($i=0; $i < count($dptos); $i++) { 
				$data = array('projects_id' => $id, 'departments_id' => $dptos[$i]);
				$this->modelo->addProjectDptos($data);
			}
			$this->modelo->editProjectZones($id);
			for ($i=0; $i < count($zonas); $i++) { 
				$data = array('projects_id' => $id, 'zones_id' => $zonas[$i]);
				$this->modelo->addProjectZones($data);
			}
			$this->modelo->editProjectRoute($id);
			for ($i=0; $i < count($puertas); $i++) { 
				$data = array('projects_id' => $id, 'doors_id' => $puertas[$i]);
				$this->modelo->addProjectRoute($data);
			}
			$this->modelo->editProjectRequirement($id);
			for ($i=0; $i < count($requirements); $i++) { 
				$data = array('projects_id' => $id, 'minimum_requirements' => $requirements[$i]);
				$this->modelo->addProjectRequirement($data);
			}
			///////
			$this->modelo->editProjectPeople($id);
			for ($i=0; $i < count($contratistas); $i++) { 
				$data = array(
					'projects_id' => $id, 
					'people_id' => $this->modelo->searchId($contratistas[$i])
				);
				$this->modelo->addProjectPeople($data);
			}
			$this->modelo->editProjectVehicles($id);
			for ($i=0; $i < count($vehicles); $i++) { 
				$data = array(
					'projects_id' 	=> $id, 
					'vehicles_id' 	=> $this->modelo->searchByPatent($vehicles[$i]),
					'control_init' 	=> $control_init[$i],
					'control_end' 	=> $control_end[$i]
				);
				$this->modelo->addProjectVehicles($data);
			}
			$vueltas = count($horarios)/9;
			$j = 0;
			$this->modelo->editProjectSchedules($id);
			for ($i=0; $i < $vueltas; $i++) { 
				$data = array(
					'projects_id' 	=> $id,
					'time_init' 	=> $horarios[$j],
					'time_end' 		=> $horarios[$j+1],
					'L' 			=> $horarios[$j+2],
					'M' 			=> $horarios[$j+3],
					'Mi' 			=> $horarios[$j+4],
					'J' 			=> $horarios[$j+5],
					'V' 			=> $horarios[$j+6],
					'S' 			=> $horarios[$j+7],
					'D' 			=> $horarios[$j+8]
				);
				$this->modelo->addProjectSchedules($data);
				$j += 9;
			}
			$data = array('mensaje' => '1', 'project' => $id);
			echo json_encode($data);
		}
		else {
			$data = array('mensaje' => '0');
			echo json_encode($data);
		}
	}

	public function addDocuments() {
		$idProject = trim($this->input->get('id',TRUE));

		$ruta = './assets/document/'.$idProject;
        if (!file_exists($ruta)) {
            mkdir($ruta, 0777, true);
        }

        $ruta .= '/';
        $mensaje = '';

        foreach($_FILES as $key) {
	        if($key['error'] == UPLOAD_ERR_OK) {
	            $nombreArchivo = $key['name'];
	            $temporal = $key['tmp_name'];
	            $destino = $ruta.$nombreArchivo;

	            move_uploaded_file($temporal, $destino);
	            $mensaje = '1';
	        }
	        else if($key['error'] == '') {
	            $mensaje = '1';
	        }
	        else if($key['error'] != '') {
	            $mensaje = '0';
	        }
	    }
	    
	    echo $mensaje;
	}

	public function listDocument($id){
		$ruta = './assets/document/'.$id.'/';
		$list = [];
		// abrir un directorio y listarlo recursivo
		if (is_dir($ruta)){
			if ($dh = opendir($ruta)){
		 		//echo "<br /><strong>$ruta</strong>"; // Aqui se imprime el nombre de la carpeta o directorio
		 		while (($file = readdir($dh)) !== false){
		 			//if (is_dir($ruta . $file) && $file!="." && $file!="..") // Si se desea mostrar solo directorios
		 			if ($file != "." && $file != ".."){ // Si se desea mostrar directorios y archivos
		 				//solo si el archivo es un directorio, distinto que "." y ".."
		 				//echo "<br />$file"; // Aqui se imprime el nombre del Archivo con su ruta relativa
		 				array_push($list, $ruta.$file);
		 				//listDocument($ruta . $file . DIRECTORY_SEPARATOR); // Ahora volvemos a llamar la función
		 				//listDocument($id);
		 			}
		 		}
		 		closedir($dh);
		 	}
		}
		return $list;
	}

	public function editProject() {
		$id = trim($this->input->post('id', TRUE));

		$title 			= trim($this->input->post('title', TRUE));
		$description 	= trim($this->input->post('description', TRUE));
		$in_charge 		= trim($this->input->post('in_charge', TRUE));
		$init 			= trim($this->input->post('init', TRUE));
		$end 			= trim($this->input->post('end', TRUE));

		$date_time = date('Y-m-d H:i:s');
		
		$data = array(
			'title' 		=> $title,
			'description' 	=> $description,
			'in_charge' 	=> $in_charge,
			'init' 			=> $init,
			'end' 			=> $end,
			'modified' 		=> $date_time
		);

		if($this->modelo->editProject($data, $id)) {
			echo '1';
		}
		else {
			echo '0';
		}
	}

	public function deleteProject() {
		$id = trim($this->input->post('id', TRUE)); 

		if($this->modelo->deleteProject($id)) {
			echo '1';
		}
		else {
			echo '0';
		}
	}
}
