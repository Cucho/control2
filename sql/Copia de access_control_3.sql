/*
SQLyog Ultimate v11.11 (64 bit)
MySQL - 5.5.5-10.1.26-MariaDB : Database - access_control_3
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`access_control_3` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci */;

USE `access_control_3`;

/*Table structure for table `access_people` */

DROP TABLE IF EXISTS `access_people`;

CREATE TABLE `access_people` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entry` tinyint(1) NOT NULL,
  `exit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `hours` tinyint(2) DEFAULT NULL,
  `end_time` datetime DEFAULT NULL,
  `people_id` int(11) NOT NULL,
  `access_state_id` int(11) NOT NULL,
  `main_access_id` int(11) DEFAULT NULL,
  `approved_by` int(11) NOT NULL,
  `observation` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `people_id` (`people_id`),
  KEY `access_state_id` (`access_state_id`),
  KEY `main_access_id` (`main_access_id`),
  KEY `approved_by` (`approved_by`),
  CONSTRAINT `access_people_ibfk_1` FOREIGN KEY (`people_id`) REFERENCES `people` (`id`),
  CONSTRAINT `access_people_ibfk_2` FOREIGN KEY (`access_state_id`) REFERENCES `access_state` (`id`),
  CONSTRAINT `access_people_ibfk_3` FOREIGN KEY (`main_access_id`) REFERENCES `main_access` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `access_people` */

insert  into `access_people`(`id`,`entry`,`exit_time`,`hours`,`end_time`,`people_id`,`access_state_id`,`main_access_id`,`approved_by`,`observation`,`created`,`modified`) values (32,1,'2018-03-12 03:45:24',4,'2018-03-12 02:05:00',44,2,2,3,'..','2018-03-12 02:05:43','2018-03-12 02:05:43'),(33,1,'2018-03-12 03:45:30',4,'2018-03-12 02:05:00',42,2,2,3,'..','2018-03-12 02:05:44','2018-03-12 02:05:44'),(34,1,'2018-03-12 03:45:34',4,'2018-03-12 02:05:00',43,2,2,3,'..','2018-03-12 02:05:44','2018-03-12 02:05:44'),(35,1,'2018-03-12 03:52:29',2,'2018-03-12 13:45:00',44,2,2,3,'...','2018-03-12 03:46:02','2018-03-12 03:46:02'),(36,1,'2018-03-12 03:52:21',2,'2018-03-12 13:45:00',43,2,2,3,'...','2018-03-12 03:46:03','2018-03-12 03:46:03'),(37,1,'2018-03-12 03:52:15',1,'2018-03-12 12:47:00',42,2,2,3,'aa','2018-03-12 03:48:50','2018-03-12 03:48:50'),(38,1,'2018-03-12 03:52:20',1,'2018-03-12 12:47:00',43,2,2,3,'aa','2018-03-12 03:48:50','2018-03-12 03:48:50'),(39,1,'2018-03-12 10:38:22',2,'2018-03-12 13:52:00',44,2,2,3,'yy','2018-03-12 03:53:03','2018-03-12 03:53:03'),(40,1,'2018-03-14 00:57:49',2,'2018-03-12 13:52:00',43,2,2,3,'yy','2018-03-12 03:53:03','2018-03-12 03:53:03'),(41,1,'2018-03-14 02:18:05',2,'2018-03-13 22:57:00',44,2,2,3,'....','2018-03-14 00:58:23','2018-03-14 00:58:23'),(42,1,'2018-03-14 02:16:48',2,'2018-03-13 22:57:00',43,2,2,3,'....','2018-03-14 00:58:23','2018-03-14 00:58:23'),(43,1,'2018-03-14 02:23:01',2,'0000-00-00 00:00:00',44,2,2,3,'..........','2018-03-14 02:18:55','2018-03-14 02:18:55'),(44,1,'2018-03-14 03:26:57',2,'0000-00-00 00:00:00',44,2,2,3,'ljljljlklhk','2018-03-14 02:23:35','2018-03-14 02:23:35'),(45,1,'2018-03-14 03:28:23',2,'2018-03-14 01:27:00',44,2,2,3,'jdjsdjsajdasjdlas','2018-03-14 03:27:45','2018-03-14 03:27:45'),(46,1,'2018-03-14 03:35:56',2,'2018-03-14 01:28:00',44,2,2,3,'ereg','2018-03-14 03:29:26','2018-03-14 03:29:26'),(47,1,'2018-03-14 13:35:04',2,'2018-03-14 01:36:00',43,2,2,3,'ggfgf','2018-03-14 03:36:53','2018-03-14 03:36:53'),(48,1,'2018-03-14 13:48:42',2,'2018-03-14 11:36:00',44,2,2,3,'ggg','2018-03-14 13:37:01','2018-03-14 13:37:01'),(49,1,'2018-03-14 13:48:18',1,'2018-03-14 10:44:00',43,2,2,3,'pkpkppk','2018-03-14 13:45:59','2018-03-14 13:45:59'),(50,1,'2018-03-14 13:48:25',1,'2018-03-14 10:44:00',43,2,2,3,'pkpkppk','2018-03-14 13:46:48','2018-03-14 13:46:48'),(51,1,'2018-03-14 13:48:32',1,'2018-03-14 10:44:00',43,2,2,3,'pkpkppk','2018-03-14 13:47:24','2018-03-14 13:47:24'),(52,1,'2018-03-14 13:48:38',1,'2018-03-14 10:44:00',43,2,2,3,'pkpkppk','2018-03-14 13:47:57','2018-03-14 13:47:57'),(53,1,'2018-03-14 13:51:03',2,'2018-03-14 11:48:00',44,2,2,3,'plpl','2018-03-14 13:50:01','2018-03-14 13:50:01'),(54,1,'2018-03-14 14:08:34',1,'2018-03-14 11:07:00',44,2,2,3,'hghm','2018-03-14 14:08:21','2018-03-14 14:08:21'),(55,1,'2018-03-14 16:23:59',1,'2018-03-14 11:08:00',44,2,2,3,'h','2018-03-14 14:09:03','2018-03-14 14:09:03'),(56,1,'2018-03-14 16:24:03',1,'2018-03-14 11:08:00',43,2,2,3,'h','2018-03-14 14:09:03','2018-03-14 14:09:03'),(57,1,'2018-03-14 18:12:13',2,'2018-03-14 14:24:00',44,2,2,3,'dfgd','2018-03-14 16:24:30','2018-03-14 16:24:30'),(58,1,'2018-03-14 18:12:17',2,'2018-03-14 14:24:00',43,2,2,3,'dfgd','2018-03-14 16:24:30','2018-03-14 16:24:30'),(59,1,'2018-03-15 01:39:09',2,'2018-03-14 16:12:00',44,2,2,3,'jj','2018-03-14 18:12:47','2018-03-14 18:12:47'),(60,1,'2018-03-15 01:39:13',2,'2018-03-14 16:12:00',43,2,2,3,'jj','2018-03-14 18:12:47','2018-03-14 18:12:47'),(61,1,'2018-03-16 10:42:56',2,'2018-03-16 12:36:23',45,2,2,1,'hola','2018-03-16 10:30:35','2018-03-16 10:36:23'),(62,1,'2018-03-16 10:45:51',1,'2018-03-16 11:45:05',45,2,2,1,'<p><b><i><u>cascsa</u></i></b><b><i></i></b></p>','2018-03-16 10:44:44','2018-03-16 10:45:05'),(63,1,'2018-03-16 15:28:39',2,'2018-03-16 14:15:28',45,2,2,1,'','2018-03-16 10:49:13','2018-03-16 10:50:28'),(64,1,'2018-03-16 15:57:02',2,'2018-03-16 17:34:37',42,2,2,1,'','2018-03-16 15:34:14','2018-03-16 15:34:37'),(65,1,'2018-03-19 16:48:10',2,'2018-03-25 18:53:30',45,2,2,1,'','2018-03-16 15:53:23','2018-03-16 15:53:30'),(66,1,'2018-03-19 17:34:42',1,'2018-03-19 17:50:00',45,2,2,3,'','2018-03-19 16:49:41','2018-03-19 16:49:41'),(68,1,'2018-03-19 18:01:20',2,'2018-03-19 19:55:56',42,2,2,1,'','2018-03-19 17:55:49','2018-03-19 17:55:56'),(69,1,'2018-03-19 18:48:26',2,'2018-03-19 20:46:27',45,2,2,1,'','2018-03-19 18:46:15','2018-03-19 18:46:27'),(70,1,'2018-03-19 18:48:17',2,'2018-03-19 20:46:29',42,2,2,1,'','2018-03-19 18:46:15','2018-03-19 18:46:29'),(71,1,'2018-03-19 18:48:21',2,'2018-03-19 20:46:31',43,2,2,1,'','2018-03-19 18:46:15','2018-03-19 18:46:31'),(72,1,'2018-03-19 20:09:59',2,'2018-03-19 20:09:00',45,2,2,1,'<p><b><u><small>hola</small></u></b></p>','2018-03-19 20:01:16','2018-03-19 20:04:00'),(73,1,'2018-03-19 20:10:03',2,'2018-03-19 22:04:03',43,2,2,1,'<p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p>\r\n\r\n<p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p>\r\n<p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p>\r\n<p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p>\r\n<p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p>\r\n<p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p>\r\n<p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p>\r\n<p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p>\r\n<p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p>\r\n<p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p><p><b><u><small>hola</small></u></b></p>\r\n','2018-03-19 20:01:16','2018-03-19 20:04:03'),(74,1,'2018-03-20 11:49:00',2,'2018-03-20 13:46:10',45,2,2,1,'<p>dqwdwq</p>','2018-03-20 11:46:04','2018-03-20 11:46:10'),(75,1,'2018-03-20 11:54:53',2,'2018-03-20 13:46:14',43,2,2,1,'<p>dqwdwq</p>','2018-03-20 11:46:04','2018-03-20 11:46:14'),(76,1,'2018-03-20 18:04:55',1,'2018-03-21 12:40:58',45,2,2,3,'','2018-03-20 11:57:11','2018-03-20 11:57:11'),(78,0,'0000-00-00 00:00:00',1,'2018-03-20 12:58:00',44,3,2,3,'','2018-03-20 12:01:40','2018-03-20 12:01:40'),(79,0,'0000-00-00 00:00:00',1,'2018-03-20 13:02:00',44,3,2,3,'<p><b>por motivos personales</b></p>','2018-03-20 12:02:40','2018-03-20 12:02:40');

/*Table structure for table `access_people_areas` */

DROP TABLE IF EXISTS `access_people_areas`;

CREATE TABLE `access_people_areas` (
  `access_people_id` int(11) NOT NULL,
  `areas_id` int(11) NOT NULL,
  PRIMARY KEY (`access_people_id`,`areas_id`),
  KEY `areas_id` (`areas_id`),
  CONSTRAINT `access_people_areas_ibfk_1` FOREIGN KEY (`access_people_id`) REFERENCES `access_people` (`id`),
  CONSTRAINT `access_people_areas_ibfk_2` FOREIGN KEY (`areas_id`) REFERENCES `areas` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `access_people_areas` */

insert  into `access_people_areas`(`access_people_id`,`areas_id`) values (32,4),(32,5),(35,4),(37,1),(39,4),(41,1),(41,2),(43,7),(44,2),(45,8),(46,6),(47,4),(47,5),(48,6),(49,4),(50,4),(51,4),(52,4),(53,3),(54,4),(54,5),(55,7),(56,7),(57,6),(58,6),(59,5),(59,6),(60,5),(60,6),(61,1),(61,2),(62,1),(62,2),(63,1),(63,2),(64,1),(64,2),(65,1),(65,2),(66,1),(66,2),(68,1),(68,2),(69,1),(69,2),(70,1),(70,2),(71,1),(71,2),(72,1),(72,2),(72,3),(72,8),(73,1),(73,2),(73,3),(73,8),(74,1),(74,2),(75,1),(75,2),(76,1),(76,2),(78,3),(78,8),(79,1),(79,2);

/*Table structure for table `access_people_department` */

DROP TABLE IF EXISTS `access_people_department`;

CREATE TABLE `access_people_department` (
  `access_people_id` int(11) NOT NULL,
  `departments_id` int(11) NOT NULL,
  PRIMARY KEY (`access_people_id`,`departments_id`),
  KEY `departments_id` (`departments_id`),
  CONSTRAINT `access_people_department_ibfk_1` FOREIGN KEY (`access_people_id`) REFERENCES `access_people` (`id`),
  CONSTRAINT `access_people_department_ibfk_2` FOREIGN KEY (`departments_id`) REFERENCES `departments` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `access_people_department` */

insert  into `access_people_department`(`access_people_id`,`departments_id`) values (37,3),(41,3),(41,5),(43,6),(44,5),(46,7),(47,8),(48,7),(53,9),(55,6),(56,6),(57,7),(58,7),(59,7),(59,8),(60,7),(60,8),(61,3),(61,5),(62,3),(62,5),(63,3),(63,5),(64,3),(64,5),(65,3),(65,5),(66,3),(66,5),(68,3),(68,5),(69,3),(69,5),(70,3),(70,5),(71,3),(71,5),(72,3),(72,5),(72,9),(73,3),(73,5),(73,9),(74,3),(74,5),(75,3),(75,5),(76,3),(76,5),(79,3),(79,5);

/*Table structure for table `access_people_intents` */

DROP TABLE IF EXISTS `access_people_intents`;

CREATE TABLE `access_people_intents` (
  `access_people_id` int(11) NOT NULL,
  `doors_id` int(11) NOT NULL,
  `entry` tinyint(1) DEFAULT '0',
  `success` tinyint(1) DEFAULT '0',
  `created` datetime NOT NULL,
  KEY `access_people_id` (`access_people_id`),
  KEY `doors_id` (`doors_id`),
  CONSTRAINT `access_people_intents_ibfk_1` FOREIGN KEY (`access_people_id`) REFERENCES `access_people` (`id`),
  CONSTRAINT `access_people_intents_ibfk_2` FOREIGN KEY (`doors_id`) REFERENCES `doors` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `access_people_intents` */

insert  into `access_people_intents`(`access_people_id`,`doors_id`,`entry`,`success`,`created`) values (65,1,0,1,'2018-03-19 16:47:43'),(66,1,0,1,'2018-03-19 16:49:51'),(66,1,0,0,'2018-03-19 16:50:36'),(66,1,0,1,'2018-03-19 16:51:40'),(66,1,0,1,'2018-03-19 17:19:45'),(66,1,0,1,'2018-03-19 17:21:15'),(66,1,0,1,'2018-03-19 17:21:19'),(66,1,0,1,'2018-03-19 17:21:24'),(66,1,0,1,'2018-03-19 17:21:52'),(66,1,0,1,'2018-03-19 17:21:57'),(66,1,0,1,'2018-03-19 17:22:01'),(66,1,0,1,'2018-03-19 17:22:06'),(66,1,0,1,'2018-03-19 17:22:38'),(66,1,0,1,'2018-03-19 17:23:12'),(66,1,0,1,'2018-03-19 17:23:17'),(66,1,0,1,'2018-03-19 17:23:34'),(66,1,0,1,'2018-03-19 17:23:46'),(66,1,0,1,'2018-03-19 17:23:52'),(66,1,0,1,'2018-03-19 17:23:58'),(72,1,0,1,'2018-03-19 17:52:20'),(76,1,0,0,'2018-03-20 12:23:37'),(76,1,0,0,'2018-03-20 12:23:40'),(76,1,0,0,'2018-03-20 12:24:40'),(76,1,0,0,'2018-03-20 12:25:05'),(76,1,0,0,'2018-03-20 12:25:31'),(76,1,0,0,'2018-03-20 12:25:45'),(76,1,0,0,'2018-03-20 12:26:27'),(76,1,0,0,'2018-03-20 12:27:02'),(76,1,0,0,'2018-03-20 12:27:50'),(76,1,0,0,'2018-03-20 12:28:16'),(76,1,0,1,'2018-03-20 12:29:50'),(76,1,0,1,'2018-03-20 12:30:00'),(76,1,0,1,'2018-03-20 12:33:15'),(76,1,0,1,'2018-03-20 12:34:06'),(76,1,0,1,'2018-03-20 12:34:19'),(76,1,0,1,'2018-03-20 12:34:30'),(76,1,0,1,'2018-03-20 12:34:49'),(76,1,0,1,'2018-03-20 12:34:55'),(76,1,0,1,'2018-03-20 12:35:00'),(76,1,0,1,'2018-03-20 12:35:05'),(76,1,0,1,'2018-03-20 12:35:14'),(76,1,0,1,'2018-03-20 12:35:23'),(76,1,0,1,'2018-03-20 12:35:29'),(76,1,0,1,'2018-03-20 12:35:34'),(76,1,0,1,'2018-03-20 12:36:18'),(76,1,0,1,'2018-03-20 12:36:27'),(76,1,0,1,'2018-03-20 12:38:28'),(76,1,0,1,'2018-03-20 12:38:45'),(76,1,0,1,'2018-03-20 12:39:13'),(76,1,0,1,'2018-03-20 12:39:30'),(76,1,0,1,'2018-03-20 12:40:53'),(76,1,0,1,'2018-03-20 12:41:12'),(76,1,0,1,'2018-03-20 12:41:29'),(76,1,0,1,'2018-03-20 13:08:34'),(76,1,0,1,'2018-03-20 13:08:44'),(76,1,0,1,'2018-03-20 13:08:48'),(76,1,0,1,'2018-03-20 13:08:57'),(76,1,0,1,'2018-03-20 13:09:07'),(76,1,0,1,'2018-03-20 13:09:26'),(76,1,0,1,'2018-03-20 13:11:32'),(76,1,0,1,'2018-03-20 13:11:55'),(76,1,0,1,'2018-03-20 13:17:03'),(76,1,0,1,'2018-03-20 13:17:17'),(76,1,0,1,'2018-03-20 13:17:33'),(76,1,0,1,'2018-03-20 13:17:41'),(76,1,0,1,'2018-03-20 13:17:59'),(76,1,0,1,'2018-03-20 13:18:12'),(76,1,0,1,'2018-03-20 13:19:21'),(76,1,0,1,'2018-03-20 13:20:29'),(76,1,0,1,'2018-03-20 13:21:32'),(76,1,0,1,'2018-03-20 13:21:38'),(76,1,0,1,'2018-03-20 13:21:54'),(76,1,0,1,'2018-03-20 13:22:20'),(76,1,0,1,'2018-03-20 13:22:40'),(76,1,0,1,'2018-03-20 13:22:46'),(76,1,0,1,'2018-03-20 13:23:42'),(76,1,0,1,'2018-03-20 13:24:20');

/*Table structure for table `access_people_reasons_visit` */

DROP TABLE IF EXISTS `access_people_reasons_visit`;

CREATE TABLE `access_people_reasons_visit` (
  `access_people_id` int(11) NOT NULL,
  `reasons_visit_id` int(11) NOT NULL,
  PRIMARY KEY (`access_people_id`,`reasons_visit_id`),
  KEY `reasons_visit_id` (`reasons_visit_id`),
  CONSTRAINT `access_people_reasons_visit_ibfk_1` FOREIGN KEY (`access_people_id`) REFERENCES `access_people` (`id`),
  CONSTRAINT `access_people_reasons_visit_ibfk_2` FOREIGN KEY (`reasons_visit_id`) REFERENCES `reasons_visit` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `access_people_reasons_visit` */

insert  into `access_people_reasons_visit`(`access_people_id`,`reasons_visit_id`) values (35,3),(37,2),(39,1),(41,4),(43,8),(44,8),(45,1),(46,5),(47,2),(48,10),(49,10),(53,8),(55,9),(56,9),(56,10),(57,3),(57,7),(58,3),(58,7),(59,4),(59,5),(60,4),(60,5),(61,2),(62,10),(63,9),(64,10),(65,10),(66,10),(68,10),(69,10),(70,10),(71,10),(72,9),(72,10),(73,9),(73,10),(74,10),(75,10),(76,10),(79,10);

/*Table structure for table `access_people_route` */

DROP TABLE IF EXISTS `access_people_route`;

CREATE TABLE `access_people_route` (
  `access_people_id` int(11) NOT NULL,
  `doors_id` int(11) NOT NULL,
  PRIMARY KEY (`access_people_id`,`doors_id`),
  KEY `doors_id` (`doors_id`),
  CONSTRAINT `access_people_route_ibfk_1` FOREIGN KEY (`access_people_id`) REFERENCES `access_people` (`id`),
  CONSTRAINT `access_people_route_ibfk_2` FOREIGN KEY (`doors_id`) REFERENCES `doors` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `access_people_route` */

insert  into `access_people_route`(`access_people_id`,`doors_id`) values (35,5),(37,1),(39,1),(39,7),(41,7),(43,5),(44,1),(45,7),(46,1),(47,1),(48,2),(49,1),(53,5),(55,1),(56,1),(57,7),(58,7),(59,2),(59,7),(60,2),(60,7),(61,1),(62,2),(63,1),(63,8),(64,1),(64,8),(65,1),(65,8),(66,1),(68,1),(68,2),(72,1),(72,8),(73,1),(73,8),(74,1),(74,7),(74,8),(75,1),(75,7),(75,8),(76,1),(76,2),(76,8),(79,1),(79,2);

/*Table structure for table `access_people_state_history` */

DROP TABLE IF EXISTS `access_people_state_history`;

CREATE TABLE `access_people_state_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `access_people_id` int(11) DEFAULT NULL,
  `access_state_id` int(11) DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `access_people_id` (`access_people_id`),
  KEY `access_state_id` (`access_state_id`),
  CONSTRAINT `access_people_state_history_ibfk_1` FOREIGN KEY (`access_people_id`) REFERENCES `access_people` (`id`),
  CONSTRAINT `access_people_state_history_ibfk_2` FOREIGN KEY (`access_state_id`) REFERENCES `access_state` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `access_people_state_history` */

insert  into `access_people_state_history`(`id`,`access_people_id`,`access_state_id`,`description`,`created`) values (1,41,2,'USUARIO USUARIO','2018-03-14 00:58:23'),(2,43,2,'USUARIO USUARIO','2018-03-14 02:18:55'),(3,44,2,'USUARIO USUARIO','2018-03-14 02:23:35'),(4,45,2,'USUARIO USUARIO','2018-03-14 03:27:45'),(5,46,2,'USUARIO USUARIO','2018-03-14 03:29:26'),(6,47,2,'USUARIO USUARIO','2018-03-14 03:36:53'),(7,48,2,'USUARIO USUARIO','2018-03-14 13:37:01'),(8,49,2,'USUARIO USUARIO','2018-03-14 13:45:59'),(9,53,2,'USUARIO USUARIO','2018-03-14 13:50:01'),(10,55,2,'USUARIO USUARIO','2018-03-14 14:09:03'),(11,56,2,'USUARIO USUARIO','2018-03-14 14:09:03'),(12,57,2,'USUARIO USUARIO','2018-03-14 16:24:30'),(13,58,2,'USUARIO USUARIO','2018-03-14 16:24:30'),(14,59,2,'USUARIO USUARIO','2018-03-14 18:12:47'),(15,60,2,'USUARIO USUARIO','2018-03-14 18:12:47'),(16,61,1,'USUARIO USUARIO','2018-03-16 10:30:35'),(17,61,2,'TEXTO A CONVENIR','2018-03-16 10:36:23'),(18,62,1,'USUARIO USUARIO','2018-03-16 10:44:44'),(19,62,2,'TEXTO A CONVENIR','2018-03-16 10:45:05'),(20,63,1,'USUARIO USUARIO','2018-03-16 10:49:13'),(21,63,2,'TEXTO A CONVENIR','2018-03-16 10:50:28'),(22,64,1,'USUARIO USUARIO','2018-03-16 15:34:14'),(23,64,2,'TEXTO A CONVENIR','2018-03-16 15:34:37'),(24,65,1,'USUARIO USUARIO','2018-03-16 15:53:23'),(25,65,2,'TEXTO A CONVENIR','2018-03-16 15:53:30'),(26,66,2,'USUARIO USUARIO','2018-03-19 16:49:41'),(29,68,1,'USUARIO USUARIO','2018-03-19 17:55:49'),(30,68,2,'TEXTO A CONVENIR','2018-03-19 17:55:56'),(31,69,1,'USUARIO USUARIO','2018-03-19 18:46:15'),(32,70,1,'USUARIO USUARIO','2018-03-19 18:46:15'),(33,71,1,'USUARIO USUARIO','2018-03-19 18:46:15'),(34,69,2,'TEXTO A CONVENIR','2018-03-19 18:46:27'),(35,70,2,'TEXTO A CONVENIR','2018-03-19 18:46:29'),(36,71,2,'TEXTO A CONVENIR','2018-03-19 18:46:31'),(37,72,1,'USUARIO USUARIO','2018-03-19 20:01:16'),(38,73,1,'USUARIO USUARIO','2018-03-19 20:01:16'),(39,72,2,'TEXTO A CONVENIR','2018-03-19 20:04:00'),(40,73,2,'TEXTO A CONVENIR','2018-03-19 20:04:03'),(41,74,1,'USUARIO USUARIO','2018-03-20 11:46:04'),(42,75,1,'USUARIO USUARIO','2018-03-20 11:46:04'),(43,74,2,'TEXTO A CONVENIR','2018-03-20 11:46:10'),(44,75,2,'TEXTO A CONVENIR','2018-03-20 11:46:14'),(45,76,2,'USUARIO USUARIO','2018-03-20 11:57:11'),(46,79,3,'USUARIO USUARIO','2018-03-20 12:02:40');

/*Table structure for table `access_people_visit` */

DROP TABLE IF EXISTS `access_people_visit`;

CREATE TABLE `access_people_visit` (
  `access_people_id` int(11) NOT NULL,
  `people_id` int(11) NOT NULL,
  PRIMARY KEY (`access_people_id`,`people_id`),
  KEY `people_id` (`people_id`),
  CONSTRAINT `access_people_visit_ibfk_1` FOREIGN KEY (`access_people_id`) REFERENCES `access_people` (`id`),
  CONSTRAINT `access_people_visit_ibfk_2` FOREIGN KEY (`people_id`) REFERENCES `people` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `access_people_visit` */

insert  into `access_people_visit`(`access_people_id`,`people_id`) values (35,21),(37,2),(39,2),(41,21),(43,21),(44,3),(45,3),(46,3),(47,3),(48,3),(49,3),(53,3),(55,3),(56,3),(57,3),(58,3),(59,2),(59,3),(60,2),(60,3),(61,3),(62,3),(63,3),(64,3),(65,21),(66,3),(68,3),(69,3),(70,3),(71,3),(72,2),(72,3),(72,30),(73,2),(73,3),(73,30),(74,3),(75,3),(76,3),(79,3);

/*Table structure for table `access_people_zones` */

DROP TABLE IF EXISTS `access_people_zones`;

CREATE TABLE `access_people_zones` (
  `access_people_id` int(11) NOT NULL,
  `zones_id` int(11) NOT NULL,
  PRIMARY KEY (`access_people_id`,`zones_id`),
  KEY `zones_id` (`zones_id`),
  CONSTRAINT `access_people_zones_ibfk_1` FOREIGN KEY (`access_people_id`) REFERENCES `access_people` (`id`),
  CONSTRAINT `access_people_zones_ibfk_2` FOREIGN KEY (`zones_id`) REFERENCES `zones` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `access_people_zones` */

insert  into `access_people_zones`(`access_people_id`,`zones_id`) values (35,3),(37,1),(39,3),(41,1),(43,7),(44,1),(45,2),(46,5),(47,3),(48,5),(49,3),(53,2),(55,7),(56,7),(57,5),(58,5),(59,3),(59,5),(60,3),(60,5),(61,1),(62,1),(63,1),(64,1),(65,1),(66,1),(68,1),(69,1),(70,1),(71,1),(72,1),(72,2),(73,1),(73,2),(74,1),(75,1),(76,1),(79,1);

/*Table structure for table `access_state` */

DROP TABLE IF EXISTS `access_state`;

CREATE TABLE `access_state` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `state` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `state` (`state`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `access_state` */

insert  into `access_state`(`id`,`state`,`created`,`modified`) values (1,'Pendiente','2018-02-18 02:37:46','2018-02-18 02:40:59'),(2,'Permitido','2018-03-04 09:56:52','2018-03-04 09:56:52'),(3,'Rechazado','2018-03-04 09:57:07','2018-03-04 09:57:07');

/*Table structure for table `access_vehicle_intents` */

DROP TABLE IF EXISTS `access_vehicle_intents`;

CREATE TABLE `access_vehicle_intents` (
  `access_vehicle_id` int(11) NOT NULL,
  `doors_id` int(11) NOT NULL,
  `entry` tinyint(1) DEFAULT '0',
  `success` tinyint(1) DEFAULT '0',
  `created` datetime NOT NULL,
  KEY `access_vehicle_id` (`access_vehicle_id`),
  KEY `doors_id` (`doors_id`),
  CONSTRAINT `access_vehicle_intents_ibfk_1` FOREIGN KEY (`access_vehicle_id`) REFERENCES `access_vehicles` (`id`),
  CONSTRAINT `access_vehicle_intents_ibfk_2` FOREIGN KEY (`doors_id`) REFERENCES `doors` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `access_vehicle_intents` */

/*Table structure for table `access_vehicles` */

DROP TABLE IF EXISTS `access_vehicles`;

CREATE TABLE `access_vehicles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `entry` tinyint(1) NOT NULL,
  `exit_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `hours` tinyint(2) NOT NULL,
  `end_time` datetime NOT NULL,
  `vehicles_id` int(11) NOT NULL,
  `access_state_id` int(11) NOT NULL,
  `main_access_id` int(11) NOT NULL,
  `approved_by` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `vehicles_id` (`vehicles_id`),
  KEY `access_state_id` (`access_state_id`),
  KEY `main_access_id` (`main_access_id`),
  KEY `approved_by` (`approved_by`),
  CONSTRAINT `access_vehicles_ibfk_1` FOREIGN KEY (`vehicles_id`) REFERENCES `vehicles` (`id`),
  CONSTRAINT `access_vehicles_ibfk_2` FOREIGN KEY (`access_state_id`) REFERENCES `access_state` (`id`),
  CONSTRAINT `access_vehicles_ibfk_3` FOREIGN KEY (`main_access_id`) REFERENCES `main_access` (`id`),
  CONSTRAINT `access_vehicles_ibfk_4` FOREIGN KEY (`approved_by`) REFERENCES `people` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `access_vehicles` */

insert  into `access_vehicles`(`id`,`entry`,`exit_time`,`hours`,`end_time`,`vehicles_id`,`access_state_id`,`main_access_id`,`approved_by`,`created`,`modified`) values (1,1,'2018-03-16 08:36:00',2,'2018-03-16 09:35:00',1,2,2,3,'2018-03-16 08:18:52','2018-03-16 08:18:52');

/*Table structure for table `access_vehicles_answers` */

DROP TABLE IF EXISTS `access_vehicles_answers`;

CREATE TABLE `access_vehicles_answers` (
  `access_vehicles_id` int(11) NOT NULL,
  `forms_id` int(11) NOT NULL,
  `question` int(11) NOT NULL,
  `answer` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`access_vehicles_id`,`forms_id`,`question`),
  KEY `forms_id` (`forms_id`),
  CONSTRAINT `access_vehicles_answers_ibfk_1` FOREIGN KEY (`access_vehicles_id`) REFERENCES `access_vehicles` (`id`),
  CONSTRAINT `access_vehicles_answers_ibfk_2` FOREIGN KEY (`forms_id`) REFERENCES `forms` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `access_vehicles_answers` */

/*Table structure for table `access_vehicles_areas` */

DROP TABLE IF EXISTS `access_vehicles_areas`;

CREATE TABLE `access_vehicles_areas` (
  `access_vehicles_id` int(11) NOT NULL,
  `areas_id` int(11) NOT NULL,
  PRIMARY KEY (`access_vehicles_id`,`areas_id`),
  KEY `areas_id` (`areas_id`),
  CONSTRAINT `access_vehicles_areas_ibfk_1` FOREIGN KEY (`access_vehicles_id`) REFERENCES `access_vehicles` (`id`),
  CONSTRAINT `access_vehicles_areas_ibfk_2` FOREIGN KEY (`areas_id`) REFERENCES `areas` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `access_vehicles_areas` */

/*Table structure for table `access_vehicles_departments` */

DROP TABLE IF EXISTS `access_vehicles_departments`;

CREATE TABLE `access_vehicles_departments` (
  `access_vehicles_id` int(11) NOT NULL,
  `departments_id` int(11) NOT NULL,
  PRIMARY KEY (`access_vehicles_id`,`departments_id`),
  KEY `departments_id` (`departments_id`),
  CONSTRAINT `access_vehicles_departments_ibfk_1` FOREIGN KEY (`access_vehicles_id`) REFERENCES `access_vehicles` (`id`),
  CONSTRAINT `access_vehicles_departments_ibfk_2` FOREIGN KEY (`departments_id`) REFERENCES `departments` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `access_vehicles_departments` */

/*Table structure for table `access_vehicles_reasons_visit` */

DROP TABLE IF EXISTS `access_vehicles_reasons_visit`;

CREATE TABLE `access_vehicles_reasons_visit` (
  `access_vehicles_id` int(11) NOT NULL,
  `reasons_visit_id` int(11) NOT NULL,
  PRIMARY KEY (`access_vehicles_id`,`reasons_visit_id`),
  KEY `reasons_visit_id` (`reasons_visit_id`),
  CONSTRAINT `access_vehicles_reasons_visit_ibfk_1` FOREIGN KEY (`access_vehicles_id`) REFERENCES `access_vehicles` (`id`),
  CONSTRAINT `access_vehicles_reasons_visit_ibfk_2` FOREIGN KEY (`reasons_visit_id`) REFERENCES `reasons_visit` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `access_vehicles_reasons_visit` */

/*Table structure for table `access_vehicles_route` */

DROP TABLE IF EXISTS `access_vehicles_route`;

CREATE TABLE `access_vehicles_route` (
  `access_vehicles_id` int(11) NOT NULL,
  `doors_id` int(11) NOT NULL,
  PRIMARY KEY (`access_vehicles_id`,`doors_id`),
  KEY `doors_id` (`doors_id`),
  CONSTRAINT `access_vehicles_route_ibfk_1` FOREIGN KEY (`access_vehicles_id`) REFERENCES `access_vehicles` (`id`),
  CONSTRAINT `access_vehicles_route_ibfk_2` FOREIGN KEY (`doors_id`) REFERENCES `doors` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `access_vehicles_route` */

/*Table structure for table `access_vehicles_state_history` */

DROP TABLE IF EXISTS `access_vehicles_state_history`;

CREATE TABLE `access_vehicles_state_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `access_vehicles_id` int(11) DEFAULT NULL,
  `access_state_id` int(11) DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `access_vehicles_id` (`access_vehicles_id`),
  KEY `access_state_id` (`access_state_id`),
  CONSTRAINT `access_vehicles_state_history_ibfk_1` FOREIGN KEY (`access_vehicles_id`) REFERENCES `access_vehicles` (`id`),
  CONSTRAINT `access_vehicles_state_history_ibfk_2` FOREIGN KEY (`access_state_id`) REFERENCES `access_state` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `access_vehicles_state_history` */

/*Table structure for table `access_vehicles_visit` */

DROP TABLE IF EXISTS `access_vehicles_visit`;

CREATE TABLE `access_vehicles_visit` (
  `access_vehicles_id` int(11) NOT NULL,
  `people_id` int(11) NOT NULL,
  PRIMARY KEY (`access_vehicles_id`,`people_id`),
  KEY `people_id` (`people_id`),
  CONSTRAINT `access_vehicles_visit_ibfk_1` FOREIGN KEY (`access_vehicles_id`) REFERENCES `access_vehicles` (`id`),
  CONSTRAINT `access_vehicles_visit_ibfk_2` FOREIGN KEY (`people_id`) REFERENCES `people` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `access_vehicles_visit` */

/*Table structure for table `access_vehicles_zones` */

DROP TABLE IF EXISTS `access_vehicles_zones`;

CREATE TABLE `access_vehicles_zones` (
  `access_vehicles_id` int(11) NOT NULL,
  `zones_id` int(11) NOT NULL,
  PRIMARY KEY (`access_vehicles_id`,`zones_id`),
  KEY `zones_id` (`zones_id`),
  CONSTRAINT `access_vehicles_zones_ibfk_1` FOREIGN KEY (`access_vehicles_id`) REFERENCES `access_vehicles` (`id`),
  CONSTRAINT `access_vehicles_zones_ibfk_2` FOREIGN KEY (`zones_id`) REFERENCES `zones` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `access_vehicles_zones` */

/*Table structure for table `answers_type` */

DROP TABLE IF EXISTS `answers_type`;

CREATE TABLE `answers_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `type` (`type`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `answers_type` */

insert  into `answers_type`(`id`,`type`,`created`,`modified`) values (1,'Respuesta corta','2018-02-14 04:42:23','2018-02-14 11:33:59'),(3,'Párrafo','2018-02-14 11:34:17','2018-02-14 11:34:17'),(4,'Cantidad','2018-02-14 11:34:28','2018-02-14 11:34:28'),(5,'Fecha','2018-02-14 11:34:42','2018-02-14 11:34:42'),(6,'Binaria','2018-02-14 11:34:53','2018-02-14 11:34:53');

/*Table structure for table `areas` */

DROP TABLE IF EXISTS `areas`;

CREATE TABLE `areas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `area` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `zones_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `area` (`area`),
  KEY `zones_id` (`zones_id`),
  CONSTRAINT `areas_ibfk_1` FOREIGN KEY (`zones_id`) REFERENCES `zones` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `areas` */

insert  into `areas`(`id`,`area`,`zones_id`,`created`,`modified`) values (1,'Área 1',1,'2018-02-09 08:42:23','2018-02-09 09:12:13'),(2,'Área 2',1,'2018-02-09 10:59:51','2018-02-09 10:59:51'),(3,'Área 3',2,'2018-02-10 02:59:25','2018-02-10 02:59:25'),(4,'Área 4',3,'2018-02-10 02:59:33','2018-02-10 02:59:33'),(5,'Área 5',3,'2018-02-10 02:59:42','2018-02-10 02:59:42'),(6,'Área 6',5,'2018-02-10 02:59:56','2018-02-10 02:59:56'),(7,'Área 7',7,'2018-02-10 03:00:03','2018-02-10 03:00:03'),(8,'Área 8',2,'2018-02-10 03:27:39','2018-02-10 03:27:39');

/*Table structure for table `companies` */

DROP TABLE IF EXISTS `companies`;

CREATE TABLE `companies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `contact` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `companies` */

insert  into `companies`(`id`,`company`,`address`,`phone`,`email`,`contact`,`created`,`modified`) values (1,'INTERNA','csCADS','CscdsC','sdcdsC@DD.CL','CscdCCCa','2018-02-08 11:47:48','2018-02-20 07:56:31'),(2,'CONTRATISTA','Villa Pedro Nolasco Calle C #973','945330884','aliro.ramirez02@inacapmail.cl','sxasxASX','2018-02-08 11:48:25','2018-02-08 11:48:25'),(3,'VISITA','Villa Pedro Nolasco Calle C #973','945330884','aliro.ramirez02@inacapmail.cl','tu','2018-02-09 12:56:09','2018-02-09 12:56:09');

/*Table structure for table `configurations` */

DROP TABLE IF EXISTS `configurations`;

CREATE TABLE `configurations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cod_company` int(11) NOT NULL,
  `company` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cod_installation` int(11) NOT NULL,
  `installation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `people_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `people_id` (`people_id`),
  CONSTRAINT `configurations_ibfk_1` FOREIGN KEY (`people_id`) REFERENCES `people` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `configurations` */

insert  into `configurations`(`id`,`cod_company`,`company`,`cod_installation`,`installation`,`address`,`email`,`phone`,`people_id`,`created`,`modified`) values (1,123456,'gggg',12,'cco','casa','a@q.cl','121212',2,'2018-02-17 10:48:04','2018-02-17 10:53:08');

/*Table structure for table `departments` */

DROP TABLE IF EXISTS `departments`;

CREATE TABLE `departments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `department` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `areas_id` int(11) NOT NULL,
  `in_charge` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `department` (`department`),
  KEY `areas_id` (`areas_id`),
  KEY `in_charge` (`in_charge`),
  CONSTRAINT `departments_ibfk_1` FOREIGN KEY (`areas_id`) REFERENCES `areas` (`id`),
  CONSTRAINT `departments_ibfk_2` FOREIGN KEY (`in_charge`) REFERENCES `people` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `departments` */

insert  into `departments`(`id`,`department`,`areas_id`,`in_charge`,`created`,`modified`) values (3,'Depto 3',1,3,'2018-02-10 02:51:09','2018-02-10 04:12:09'),(5,'Depto 5',2,3,'2018-02-10 03:43:45','2018-02-10 03:43:45'),(6,'Depto 6',7,3,'2018-02-10 03:29:39','2018-02-10 03:29:39'),(7,'Depto 7',6,3,'2018-02-10 03:29:47','2018-02-10 03:29:47'),(8,'Depto 8',5,3,'2018-02-10 03:29:54','2018-02-10 03:29:54'),(9,'Depto 9',3,3,'2018-02-10 03:30:03','2018-02-10 03:30:03');

/*Table structure for table `doors` */

DROP TABLE IF EXISTS `doors`;

CREATE TABLE `doors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `door` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `level` tinyint(3) NOT NULL,
  `doors_type_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `doors_type_id` (`doors_type_id`),
  CONSTRAINT `doors_ibfk_1` FOREIGN KEY (`doors_type_id`) REFERENCES `doors_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `doors` */

insert  into `doors`(`id`,`door`,`description`,`level`,`doors_type_id`,`created`,`modified`) values (1,'Portería 1','Puerta portería acceso sur',0,4,'2018-02-09 04:15:42','2018-02-09 06:21:53'),(2,'Portería 2','Puerta portería acceso norte',1,5,'2018-02-09 04:47:28','2018-02-09 04:47:28'),(3,'Portería 3','Puerta portería acceso suroriente',2,4,'2018-02-09 05:17:33','2018-02-09 05:17:33'),(4,'Portería 4','.',3,4,'2018-02-10 03:26:04','2018-02-18 09:54:44'),(5,'Portería 5','.',1,5,'2018-02-10 03:26:11','2018-02-18 09:59:33'),(6,'Portería 6','',4,4,'2018-02-10 03:26:17','2018-02-10 03:26:17'),(7,'Portería 7','',0,5,'2018-02-10 03:26:23','2018-02-10 03:26:23'),(8,'Portería 8','',5,4,'2018-02-10 03:28:47','2018-02-10 03:28:47');

/*Table structure for table `doors_areas` */

DROP TABLE IF EXISTS `doors_areas`;

CREATE TABLE `doors_areas` (
  `doors_id` int(11) NOT NULL,
  `areas_id` int(11) NOT NULL,
  PRIMARY KEY (`doors_id`,`areas_id`),
  KEY `areas_id` (`areas_id`),
  CONSTRAINT `doors_areas_ibfk_1` FOREIGN KEY (`doors_id`) REFERENCES `doors` (`id`),
  CONSTRAINT `doors_areas_ibfk_2` FOREIGN KEY (`areas_id`) REFERENCES `areas` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `doors_areas` */

insert  into `doors_areas`(`doors_id`,`areas_id`) values (1,1),(2,2),(3,3),(4,4),(5,5),(6,6),(7,7),(8,8);

/*Table structure for table `doors_departments` */

DROP TABLE IF EXISTS `doors_departments`;

CREATE TABLE `doors_departments` (
  `doors_id` int(11) NOT NULL,
  `departments_id` int(11) NOT NULL,
  PRIMARY KEY (`doors_id`,`departments_id`),
  KEY `departments_id` (`departments_id`),
  CONSTRAINT `doors_departments_ibfk_1` FOREIGN KEY (`doors_id`) REFERENCES `doors` (`id`),
  CONSTRAINT `doors_departments_ibfk_2` FOREIGN KEY (`departments_id`) REFERENCES `departments` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `doors_departments` */

insert  into `doors_departments`(`doors_id`,`departments_id`) values (1,9),(2,8),(3,7),(4,6),(5,3),(6,5);

/*Table structure for table `doors_parents` */

DROP TABLE IF EXISTS `doors_parents`;

CREATE TABLE `doors_parents` (
  `doors_id` int(11) NOT NULL,
  `parent` int(11) NOT NULL,
  PRIMARY KEY (`doors_id`,`parent`),
  CONSTRAINT `doors_parents_ibfk_1` FOREIGN KEY (`doors_id`) REFERENCES `doors` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `doors_parents` */

/*Table structure for table `doors_type` */

DROP TABLE IF EXISTS `doors_type`;

CREATE TABLE `doors_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `type` (`type`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `doors_type` */

insert  into `doors_type`(`id`,`type`,`created`,`modified`) values (4,'Corredera manual','2018-02-09 03:47:30','2018-02-09 03:47:30'),(5,'Corredera automatica','2018-02-09 03:47:38','2018-02-09 03:47:38'),(6,'Batiente manual','2018-02-10 03:49:15','2018-02-10 03:49:15'),(7,'Batiente automatica','2018-02-10 03:49:27','2018-02-10 03:49:27'),(8,'Levadiza manual','2018-02-10 03:49:56','2018-02-10 03:50:17'),(9,'Levadiza automatica','2018-02-10 03:50:09','2018-02-10 03:50:09');

/*Table structure for table `doors_zones` */

DROP TABLE IF EXISTS `doors_zones`;

CREATE TABLE `doors_zones` (
  `doors_id` int(11) NOT NULL,
  `zones_id` int(11) NOT NULL,
  PRIMARY KEY (`doors_id`,`zones_id`),
  KEY `zones_id` (`zones_id`),
  CONSTRAINT `doors_zones_ibfk_1` FOREIGN KEY (`doors_id`) REFERENCES `doors` (`id`),
  CONSTRAINT `doors_zones_ibfk_2` FOREIGN KEY (`zones_id`) REFERENCES `zones` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `doors_zones` */

insert  into `doors_zones`(`doors_id`,`zones_id`) values (1,1),(1,2),(2,1),(3,1),(4,4),(8,1);

/*Table structure for table `forms` */

DROP TABLE IF EXISTS `forms`;

CREATE TABLE `forms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `forms` */

insert  into `forms`(`id`,`title`,`description`,`created`,`modified`) values (32,'Formulario prueba','prueba crear, editar','2018-02-16 03:04:20','2018-02-25 01:27:52');

/*Table structure for table `forms_detail` */

DROP TABLE IF EXISTS `forms_detail`;

CREATE TABLE `forms_detail` (
  `order` int(11) NOT NULL,
  `question` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `placeholder` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `forms_id` int(11) NOT NULL,
  `answers_type_id` int(11) NOT NULL,
  `measures_id` int(11) DEFAULT '0',
  PRIMARY KEY (`order`,`forms_id`),
  KEY `forms_id` (`forms_id`),
  KEY `answers_type_id` (`answers_type_id`),
  CONSTRAINT `forms_detail_ibfk_1` FOREIGN KEY (`forms_id`) REFERENCES `forms` (`id`),
  CONSTRAINT `forms_detail_ibfk_2` FOREIGN KEY (`answers_type_id`) REFERENCES `answers_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `forms_detail` */

insert  into `forms_detail`(`order`,`question`,`placeholder`,`forms_id`,`answers_type_id`,`measures_id`) values (1,'Pregunta 1','Respuesta 1',32,1,0),(2,'Pregunta 2','Respuesta 2',32,3,0),(3,'Pregunta 3','Respuesta 3b',32,6,0),(4,'Pregunta 4','hoy',32,5,0),(5,'Pregunta 5','123',32,4,1);

/*Table structure for table `forms_season` */

DROP TABLE IF EXISTS `forms_season`;

CREATE TABLE `forms_season` (
  `vechiles_type_id` int(11) NOT NULL,
  `forms_id` int(11) NOT NULL,
  `seasons_id` int(11) NOT NULL,
  `year` int(4) NOT NULL,
  PRIMARY KEY (`vechiles_type_id`,`forms_id`,`seasons_id`,`year`),
  KEY `forms_id` (`forms_id`),
  KEY `seasons_id` (`seasons_id`),
  CONSTRAINT `forms_season_ibfk_1` FOREIGN KEY (`vechiles_type_id`) REFERENCES `vehicles_type` (`id`),
  CONSTRAINT `forms_season_ibfk_2` FOREIGN KEY (`forms_id`) REFERENCES `forms` (`id`),
  CONSTRAINT `forms_season_ibfk_3` FOREIGN KEY (`seasons_id`) REFERENCES `seasons` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `forms_season` */

insert  into `forms_season`(`vechiles_type_id`,`forms_id`,`seasons_id`,`year`) values (1,32,1,2018),(3,32,4,2019);

/*Table structure for table `internal_people_errors` */

DROP TABLE IF EXISTS `internal_people_errors`;

CREATE TABLE `internal_people_errors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `people_id` int(11) NOT NULL,
  `sensors_id` int(11) NOT NULL,
  `entry` tinyint(1) DEFAULT '0',
  `reasons_error_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `people_id` (`people_id`),
  KEY `sensors_id` (`sensors_id`),
  KEY `reasons_error_id` (`reasons_error_id`),
  CONSTRAINT `internal_people_errors_ibfk_1` FOREIGN KEY (`people_id`) REFERENCES `people` (`id`),
  CONSTRAINT `internal_people_errors_ibfk_2` FOREIGN KEY (`sensors_id`) REFERENCES `sensors` (`id`),
  CONSTRAINT `internal_people_errors_ibfk_3` FOREIGN KEY (`reasons_error_id`) REFERENCES `reasons_error` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `internal_people_errors` */

insert  into `internal_people_errors`(`id`,`people_id`,`sensors_id`,`entry`,`reasons_error_id`,`created`) values (1,2,1,0,1,'2018-02-18 12:02:06'),(2,30,1,0,2,'2018-03-19 16:42:54'),(3,30,1,0,2,'2018-03-19 16:44:37'),(4,30,1,0,2,'2018-03-19 16:45:34');

/*Table structure for table `internal_people_success` */

DROP TABLE IF EXISTS `internal_people_success`;

CREATE TABLE `internal_people_success` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `people_id` int(11) NOT NULL,
  `entry` tinyint(1) DEFAULT '0',
  `sensors_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `people_id` (`people_id`),
  KEY `sensors_id` (`sensors_id`),
  CONSTRAINT `internal_people_success_ibfk_1` FOREIGN KEY (`people_id`) REFERENCES `people` (`id`),
  CONSTRAINT `internal_people_success_ibfk_2` FOREIGN KEY (`sensors_id`) REFERENCES `sensors` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `internal_people_success` */

insert  into `internal_people_success`(`id`,`people_id`,`entry`,`sensors_id`,`created`) values (1,2,0,1,'2018-02-18 16:17:44'),(2,3,0,1,'2018-02-18 16:22:59'),(3,47,0,1,'2018-03-19 13:03:13'),(4,47,0,1,'2018-03-19 13:15:23'),(5,21,0,1,'2018-03-19 13:17:14'),(6,31,0,1,'2018-03-19 13:23:36'),(7,31,0,1,'2018-03-19 13:24:07'),(8,31,0,1,'2018-03-19 13:27:57'),(9,3,0,1,'2018-03-19 16:41:59'),(10,2,0,1,'2018-03-19 16:42:27'),(11,30,0,1,'2018-03-19 16:43:37'),(12,30,0,1,'2018-03-19 16:44:52'),(13,30,0,1,'2018-03-19 16:46:47'),(14,47,0,1,'2018-03-20 09:48:34');

/*Table structure for table `internal_vehicles_errors` */

DROP TABLE IF EXISTS `internal_vehicles_errors`;

CREATE TABLE `internal_vehicles_errors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vehicles_id` int(11) NOT NULL,
  `sensors_id` int(11) DEFAULT NULL,
  `entry` tinyint(1) DEFAULT '0',
  `reasons_error_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `vehicles_id` (`vehicles_id`),
  KEY `sensors_id` (`sensors_id`),
  KEY `reasons_error_id` (`reasons_error_id`),
  CONSTRAINT `internal_vehicles_errors_ibfk_1` FOREIGN KEY (`vehicles_id`) REFERENCES `vehicles` (`id`),
  CONSTRAINT `internal_vehicles_errors_ibfk_2` FOREIGN KEY (`sensors_id`) REFERENCES `sensors` (`id`),
  CONSTRAINT `internal_vehicles_errors_ibfk_3` FOREIGN KEY (`reasons_error_id`) REFERENCES `reasons_error` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `internal_vehicles_errors` */

/*Table structure for table `internal_vehicles_success` */

DROP TABLE IF EXISTS `internal_vehicles_success`;

CREATE TABLE `internal_vehicles_success` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `vehicles_id` int(11) NOT NULL,
  `entry` tinyint(4) DEFAULT NULL,
  `sensors_id` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `vehicles_id` (`vehicles_id`),
  KEY `sensors_id` (`sensors_id`),
  CONSTRAINT `internal_vehicles_success_ibfk_1` FOREIGN KEY (`vehicles_id`) REFERENCES `vehicles` (`id`),
  CONSTRAINT `internal_vehicles_success_ibfk_2` FOREIGN KEY (`sensors_id`) REFERENCES `sensors` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `internal_vehicles_success` */

/*Table structure for table `jornada` */

DROP TABLE IF EXISTS `jornada`;

CREATE TABLE `jornada` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `jornada` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time_init` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time_end` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `jornada` */

insert  into `jornada`(`id`,`jornada`,`time_init`,`time_end`) values (2,'Jornada 1','08:00','18:00'),(3,'Jornada 2','20:00','08:00');

/*Table structure for table `main_access` */

DROP TABLE IF EXISTS `main_access`;

CREATE TABLE `main_access` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ubication` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ip_host` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name_host` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `entry` tinyint(1) NOT NULL,
  `flow` tinyint(1) NOT NULL,
  `internal` tinyint(1) NOT NULL,
  `state` tinyint(1) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `main_access` */

insert  into `main_access`(`id`,`name`,`ubication`,`ip_host`,`name_host`,`entry`,`flow`,`internal`,`state`,`created`,`modified`) values (2,'Acceso principal','Teno','10.10.1.1','PAPC',2,0,1,1,'2018-02-11 05:09:39','2018-02-14 09:21:45'),(3,'Acceso secundario 1','Curicó','10.10.1.2','PASC1',2,2,2,1,'2018-02-11 05:11:45','2018-02-14 09:22:07'),(4,'Acceso secundario 2','Curicó','10.10.1.3','papap',0,2,0,1,'2018-02-11 05:13:57','2018-02-14 09:22:40'),(5,'Acceso principal3','Curicó','10.10.1.4','prprpp',1,1,1,1,'2018-02-11 05:14:54','2018-02-13 12:57:27'),(6,'Acceso principal 4','Curicó','10.10.1.5','papappapapa',1,1,1,0,'2018-02-11 05:17:00','2018-02-14 09:31:30');

/*Table structure for table `measures` */

DROP TABLE IF EXISTS `measures`;

CREATE TABLE `measures` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `measure` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `acronimo` varchar(6) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `measure` (`measure`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `measures` */

insert  into `measures`(`id`,`measure`,`acronimo`,`created`,`modified`) values (1,'Kilos','Kgs','2018-02-14 23:26:17','2018-02-24 09:47:44'),(3,'Litros','Lts','2018-02-17 03:49:06','2018-02-17 03:49:06'),(4,'Gramos','Grs','2018-02-24 09:47:38','2018-02-24 09:47:38');

/*Table structure for table `minimum_requirements` */

DROP TABLE IF EXISTS `minimum_requirements`;

CREATE TABLE `minimum_requirements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `requirement` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `requirement` (`requirement`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `minimum_requirements` */

/*Table structure for table `options` */

DROP TABLE IF EXISTS `options`;

CREATE TABLE `options` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `option` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `option` (`option`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `options` */

insert  into `options`(`id`,`option`,`code`,`created`,`modified`) values (1,'Opción 1.','a123-k','2018-02-13 10:26:11','2018-02-13 10:31:32'),(4,'Opción 2.','a123-k','2018-02-13 10:26:11','2018-02-13 10:31:32'),(5,'Opción 3.','a123-k','2018-02-13 10:26:11','2018-02-13 10:31:32'),(6,'Opción 4.','a123-k','2018-02-13 10:26:11','2018-02-13 10:31:32'),(7,'Opción 5.','a123-k','2018-02-13 10:26:11','2018-02-13 10:31:32');

/*Table structure for table `options_roles` */

DROP TABLE IF EXISTS `options_roles`;

CREATE TABLE `options_roles` (
  `options_id` int(11) NOT NULL,
  `roles_id` int(11) NOT NULL,
  PRIMARY KEY (`options_id`,`roles_id`),
  KEY `roles_id` (`roles_id`),
  CONSTRAINT `options_roles_ibfk_1` FOREIGN KEY (`options_id`) REFERENCES `options` (`id`),
  CONSTRAINT `options_roles_ibfk_2` FOREIGN KEY (`roles_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `options_roles` */

insert  into `options_roles`(`options_id`,`roles_id`) values (1,2),(5,1),(6,1),(7,1);

/*Table structure for table `people` */

DROP TABLE IF EXISTS `people`;

CREATE TABLE `people` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rut` int(15) NOT NULL,
  `digit` varchar(1) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `allow_all` tinyint(1) DEFAULT NULL,
  `is_visited` tinyint(1) DEFAULT NULL,
  `internal` tinyint(1) DEFAULT '0',
  `nfc_code` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `people_profiles_id` int(11) NOT NULL,
  `companies_id` int(11) NOT NULL,
  `departments_id` int(11) DEFAULT '0',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `rut` (`rut`),
  KEY `profiles_people_id` (`people_profiles_id`),
  KEY `companies_id` (`companies_id`),
  CONSTRAINT `people_ibfk_1` FOREIGN KEY (`people_profiles_id`) REFERENCES `people_profiles` (`id`),
  CONSTRAINT `people_ibfk_2` FOREIGN KEY (`companies_id`) REFERENCES `companies` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `people` */

insert  into `people`(`id`,`rut`,`digit`,`name`,`last_name`,`address`,`email`,`phone`,`allow_all`,`is_visited`,`internal`,`nfc_code`,`people_profiles_id`,`companies_id`,`departments_id`,`created`,`modified`) values (2,33333333,'3','persona','soltera','su casa','no_tiene@sinmail.cl','88776655',1,1,1,'0',1,1,3,'2018-02-17 03:34:12','2018-02-17 03:34:12'),(3,11111111,'1','Usuario','usuario','casa','a@a.cl','4',1,1,1,'0',4,1,3,'2018-02-18 08:24:35','2018-02-18 08:24:35'),(21,22222222,'2','Aliro','Nuñez','Villa Pedro Nolasco Calle C #973','aliro.ramirez02@inacapmail.cl','945330884',0,0,1,'0',2,1,3,'2018-03-19 13:17:26','2018-03-19 13:17:26'),(30,44444444,'4','Aliro','Nuñez','Villa Pedro Nolasco Calle C #973','aliro.ramirez02@inacapmail.cl','945330884',0,1,1,'0',5,2,3,'2018-03-19 16:42:48','2018-03-19 16:42:48'),(31,55555555,'5','Aliro operario','Nuñez','Villa Pedro Nolasco Calle C #973','aliro.ramirez02@inacapmail.cl','945330884',0,0,1,'0',4,1,3,'2018-03-19 13:19:56','2018-03-19 13:19:56'),(42,66666666,'6','Aliro','Nuñez','Villa Pedro Nolasco Calle C #973','aliro.ramirez02@inacapmail.cl','945330884',0,0,0,'0',2,1,0,'2018-03-04 02:40:43','2018-03-04 02:40:43'),(43,77777777,'7','Aliro','Nuñez','Villa Pedro Nolasco Calle C #973','aliro.ramirez02@inacapmail.cl','945330884',0,0,0,'0',2,2,0,'2018-03-04 02:49:53','2018-03-04 02:49:53'),(44,88888888,'8','Aliro','Nuñez','Villa Pedro Nolasco Calle C #973','aliro.ramirez02@inacapmail.cl','945330884',0,0,0,'0',2,1,0,'2018-03-04 02:51:07','2018-03-04 02:51:07'),(45,17795600,'7','MATIAS','QUEZADA SANHUEZA','MANUEL CORREA 1','el_mts@hotmail.com','123456789',0,0,0,'1181362197',7,3,0,'2018-03-20 16:37:20','2018-03-20 16:37:20'),(46,99999999,'9','EJEMPLO','EJEMPLO','EJEMPLO','EJEMPLO@GMAIL.COM','12345678',0,0,0,'0',6,3,0,'2018-03-16 15:30:13','2018-03-16 15:30:13'),(47,18577245,'4','JOSE','BUSTAMANTE BOBADILLA','CURICO','JBUSTAMANTE@MDSG.CL','1234567',1,0,1,'0',2,1,3,'2018-03-20 17:25:18','2018-03-20 17:25:18');

/*Table structure for table `people_profiles` */

DROP TABLE IF EXISTS `people_profiles`;

CREATE TABLE `people_profiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `profile` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `profile` (`profile`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `people_profiles` */

insert  into `people_profiles`(`id`,`profile`,`created`,`modified`) values (1,'CHOFER','2018-02-10 19:31:33','2018-02-10 19:31:33'),(2,'ADMINISTRATIVO','2018-02-12 11:08:47','2018-02-12 11:11:10'),(3,'JORNAL','2018-02-12 11:10:14','2018-02-12 11:10:14'),(4,'OPERARIO','2018-02-12 11:10:23','2018-02-12 11:10:23'),(5,'GUARDIA','2018-02-12 11:10:31','2018-02-12 11:10:31'),(6,'VISITA','2018-03-16 09:57:16','2018-03-16 09:57:18'),(7,'CONTRATISTA','2018-03-16 09:57:20','2018-03-16 09:57:22');

/*Table structure for table `profiles_doors_schedules` */

DROP TABLE IF EXISTS `profiles_doors_schedules`;

CREATE TABLE `profiles_doors_schedules` (
  `profiles_people_id` int(11) NOT NULL,
  `doors_id` int(11) NOT NULL,
  `time_init` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `time_end` varchar(5) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jornada_id` int(11) NOT NULL,
  `L` tinyint(1) DEFAULT '0',
  `M` tinyint(1) DEFAULT '0',
  `Mi` tinyint(1) DEFAULT '0',
  `J` tinyint(1) DEFAULT '0',
  `V` tinyint(1) DEFAULT '0',
  `S` tinyint(1) DEFAULT '0',
  `D` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`profiles_people_id`,`doors_id`,`time_init`,`time_end`),
  KEY `profiles_people_id` (`profiles_people_id`),
  KEY `doors_id` (`doors_id`),
  KEY `jornada_id` (`jornada_id`),
  CONSTRAINT `profiles_doors_schedules_ibfk_1` FOREIGN KEY (`profiles_people_id`) REFERENCES `people_profiles` (`id`),
  CONSTRAINT `profiles_doors_schedules_ibfk_2` FOREIGN KEY (`doors_id`) REFERENCES `doors` (`id`),
  CONSTRAINT `profiles_doors_schedules_ibfk_3` FOREIGN KEY (`jornada_id`) REFERENCES `jornada` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `profiles_doors_schedules` */

insert  into `profiles_doors_schedules`(`profiles_people_id`,`doors_id`,`time_init`,`time_end`,`jornada_id`,`L`,`M`,`Mi`,`J`,`V`,`S`,`D`) values (2,1,'08:00','18:00',2,1,1,1,1,1,0,0),(2,2,'08:00','18:00',2,1,1,1,1,1,0,0),(2,3,'08:00','18:00',2,1,1,1,1,1,1,0),(2,5,'08:00','18:00',2,1,0,0,0,0,0,0),(2,6,'20:00','08:00',3,0,0,0,0,0,0,0),(2,7,'08:00','18:00',2,0,0,0,0,0,1,1);

/*Table structure for table `projects` */

DROP TABLE IF EXISTS `projects`;

CREATE TABLE `projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `in_charge` int(11) DEFAULT NULL,
  `in_charge_installation` int(11) DEFAULT NULL,
  `init` date DEFAULT NULL,
  `end` date DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `in_charge` (`in_charge`),
  KEY `in_charge_installation` (`in_charge_installation`),
  CONSTRAINT `projects_ibfk_1` FOREIGN KEY (`in_charge`) REFERENCES `people` (`id`),
  CONSTRAINT `projects_ibfk_2` FOREIGN KEY (`in_charge_installation`) REFERENCES `people` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `projects` */

/*Table structure for table `projects_areas` */

DROP TABLE IF EXISTS `projects_areas`;

CREATE TABLE `projects_areas` (
  `projects_id` int(11) NOT NULL,
  `areas_id` int(11) NOT NULL,
  PRIMARY KEY (`projects_id`,`areas_id`),
  KEY `areas_id` (`areas_id`),
  CONSTRAINT `projects_areas_ibfk_1` FOREIGN KEY (`projects_id`) REFERENCES `projects` (`id`),
  CONSTRAINT `projects_areas_ibfk_2` FOREIGN KEY (`areas_id`) REFERENCES `areas` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `projects_areas` */

/*Table structure for table `projects_departments` */

DROP TABLE IF EXISTS `projects_departments`;

CREATE TABLE `projects_departments` (
  `projects_id` int(11) NOT NULL,
  `departments_id` int(11) NOT NULL,
  PRIMARY KEY (`projects_id`,`departments_id`),
  KEY `departments_id` (`departments_id`),
  CONSTRAINT `projects_departments_ibfk_1` FOREIGN KEY (`projects_id`) REFERENCES `projects` (`id`),
  CONSTRAINT `projects_departments_ibfk_2` FOREIGN KEY (`departments_id`) REFERENCES `departments` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `projects_departments` */

/*Table structure for table `projects_intents` */

DROP TABLE IF EXISTS `projects_intents`;

CREATE TABLE `projects_intents` (
  `projects_id` int(11) NOT NULL,
  `doors_id` int(11) NOT NULL,
  `entry` tinyint(1) DEFAULT '0',
  `success` tinyint(1) DEFAULT '0',
  `created` datetime NOT NULL,
  PRIMARY KEY (`projects_id`,`doors_id`,`created`),
  KEY `doors_id` (`doors_id`),
  CONSTRAINT `projects_intents_ibfk_1` FOREIGN KEY (`projects_id`) REFERENCES `projects` (`id`),
  CONSTRAINT `projects_intents_ibfk_2` FOREIGN KEY (`doors_id`) REFERENCES `doors` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `projects_intents` */

/*Table structure for table `projects_minimum_requirements` */

DROP TABLE IF EXISTS `projects_minimum_requirements`;

CREATE TABLE `projects_minimum_requirements` (
  `projects_id` int(11) NOT NULL,
  `minimum_requirements` int(11) NOT NULL,
  PRIMARY KEY (`projects_id`,`minimum_requirements`),
  KEY `minimum_requirements` (`minimum_requirements`),
  CONSTRAINT `projects_minimum_requirements_ibfk_1` FOREIGN KEY (`projects_id`) REFERENCES `projects` (`id`),
  CONSTRAINT `projects_minimum_requirements_ibfk_2` FOREIGN KEY (`minimum_requirements`) REFERENCES `minimum_requirements` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `projects_minimum_requirements` */

/*Table structure for table `projects_people` */

DROP TABLE IF EXISTS `projects_people`;

CREATE TABLE `projects_people` (
  `projects_id` int(11) NOT NULL,
  `people_id` int(11) NOT NULL,
  PRIMARY KEY (`projects_id`,`people_id`),
  KEY `people_id` (`people_id`),
  CONSTRAINT `projects_people_ibfk_1` FOREIGN KEY (`projects_id`) REFERENCES `projects` (`id`),
  CONSTRAINT `projects_people_ibfk_2` FOREIGN KEY (`people_id`) REFERENCES `people` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `projects_people` */

/*Table structure for table `projects_route` */

DROP TABLE IF EXISTS `projects_route`;

CREATE TABLE `projects_route` (
  `projects_id` int(11) NOT NULL,
  `doors_id` int(11) NOT NULL,
  PRIMARY KEY (`projects_id`,`doors_id`),
  KEY `doors_id` (`doors_id`),
  CONSTRAINT `projects_route_ibfk_1` FOREIGN KEY (`projects_id`) REFERENCES `projects` (`id`),
  CONSTRAINT `projects_route_ibfk_2` FOREIGN KEY (`doors_id`) REFERENCES `doors` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `projects_route` */

/*Table structure for table `projects_schedules` */

DROP TABLE IF EXISTS `projects_schedules`;

CREATE TABLE `projects_schedules` (
  `projects_id` int(11) DEFAULT NULL,
  `time_init` time DEFAULT NULL,
  `time_end` time DEFAULT NULL,
  `L` tinyint(1) DEFAULT '0',
  `M` tinyint(1) DEFAULT '0',
  `Mi` tinyint(1) DEFAULT '0',
  `J` tinyint(1) DEFAULT '0',
  `V` tinyint(1) DEFAULT '0',
  `S` tinyint(1) DEFAULT '0',
  `D` tinyint(1) DEFAULT '0',
  KEY `projects_id` (`projects_id`),
  CONSTRAINT `projects_schedules_ibfk_1` FOREIGN KEY (`projects_id`) REFERENCES `projects` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `projects_schedules` */

/*Table structure for table `projects_vehicles` */

DROP TABLE IF EXISTS `projects_vehicles`;

CREATE TABLE `projects_vehicles` (
  `projects_id` int(11) NOT NULL,
  `vehicles_id` int(11) NOT NULL,
  PRIMARY KEY (`projects_id`,`vehicles_id`),
  KEY `vehicles_id` (`vehicles_id`),
  CONSTRAINT `projects_vehicles_ibfk_1` FOREIGN KEY (`projects_id`) REFERENCES `projects` (`id`),
  CONSTRAINT `projects_vehicles_ibfk_2` FOREIGN KEY (`vehicles_id`) REFERENCES `vehicles` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `projects_vehicles` */

/*Table structure for table `projects_zones` */

DROP TABLE IF EXISTS `projects_zones`;

CREATE TABLE `projects_zones` (
  `projects_id` int(11) NOT NULL,
  `zones_id` int(11) NOT NULL,
  PRIMARY KEY (`projects_id`,`zones_id`),
  KEY `zones_id` (`zones_id`),
  CONSTRAINT `projects_zones_ibfk_1` FOREIGN KEY (`projects_id`) REFERENCES `projects` (`id`),
  CONSTRAINT `projects_zones_ibfk_2` FOREIGN KEY (`zones_id`) REFERENCES `zones` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `projects_zones` */

/*Table structure for table `reasons_error` */

DROP TABLE IF EXISTS `reasons_error`;

CREATE TABLE `reasons_error` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reason` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `reason` (`reason`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `reasons_error` */

insert  into `reasons_error`(`id`,`reason`,`created`,`modified`) values (1,'Puerta no autorizada','2018-02-18 12:01:53','2018-02-18 12:01:53'),(2,'Fuera de horario permitido','2018-02-18 09:29:04','2018-02-18 09:29:04');

/*Table structure for table `reasons_visit` */

DROP TABLE IF EXISTS `reasons_visit`;

CREATE TABLE `reasons_visit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `reason` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `reason` (`reason`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `reasons_visit` */

insert  into `reasons_visit`(`id`,`reason`,`created`,`modified`) values (1,'Servicio de valija','2018-03-04 01:41:37','2018-03-04 01:41:37'),(2,'Servicio de encomienda','2018-03-04 01:41:54','2018-03-04 01:41:54'),(3,'Entrega de materia prima','2018-03-04 01:44:29','2018-03-04 01:44:29'),(4,'Despacho a puerto','2018-03-04 01:44:46','2018-03-04 01:44:46'),(5,'Cliente de productos','2018-03-04 03:17:05','2018-03-04 03:17:05'),(7,'Proveedor materia prima','2018-03-04 03:18:59','2018-03-04 03:18:59'),(8,'Retiro propietario','2018-03-04 03:21:32','2018-03-04 03:21:32'),(9,'test','2018-03-08 07:22:35','2018-03-08 07:22:35'),(10,'tset3','2018-03-08 07:23:22','2018-03-08 07:23:22');

/*Table structure for table `roles` */

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `rol` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `rol` (`rol`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `roles` */

insert  into `roles`(`id`,`rol`,`created`,`modified`) values (1,'Administrador','2018-02-12 05:34:39','2018-02-12 05:34:39'),(2,'Funcionario','2018-02-12 05:35:00','2018-02-12 05:35:00'),(3,'Visitante','2018-02-12 05:35:11','2018-02-12 05:35:11'),(4,'Guardia','2018-02-12 05:35:22','2018-02-12 05:35:22'),(5,'Operario','2018-02-12 05:35:51','2018-02-12 05:35:51');

/*Table structure for table `seasons` */

DROP TABLE IF EXISTS `seasons`;

CREATE TABLE `seasons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `season` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `season` (`season`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `seasons` */

insert  into `seasons`(`id`,`season`,`created`,`modified`) values (1,'Temporada 20188','2018-02-12 03:21:23','2018-02-12 03:26:00'),(2,'Temporada 2019','2018-02-12 03:21:53','2018-02-12 03:21:53'),(3,'Temporada 2020','2018-02-12 03:22:05','2018-02-12 03:22:05'),(4,'Temporada 2018-2019','2018-02-12 03:22:17','2018-02-12 03:22:17'),(5,'Temporada 2019 - 2020','2018-02-12 03:22:29','2018-02-12 03:22:29');

/*Table structure for table `sensors` */

DROP TABLE IF EXISTS `sensors`;

CREATE TABLE `sensors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sensor` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ip` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sensors_type` int(11) NOT NULL,
  `entry` tinyint(1) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `sensors_type` (`sensors_type`),
  CONSTRAINT `sensors_ibfk_1` FOREIGN KEY (`sensors_type`) REFERENCES `sensors_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `sensors` */

insert  into `sensors`(`id`,`code`,`sensor`,`description`,`ip`,`sensors_type`,`entry`,`created`,`modified`) values (1,'RASP-PI-3','RASPBERRY PI 3','','192.10.10.151',2,0,'2018-02-12 03:37:54','2018-03-20 13:04:40');

/*Table structure for table `sensors_doors` */

DROP TABLE IF EXISTS `sensors_doors`;

CREATE TABLE `sensors_doors` (
  `sensors_id` int(11) NOT NULL,
  `doors_id` int(11) NOT NULL,
  PRIMARY KEY (`sensors_id`,`doors_id`),
  KEY `doors_id` (`doors_id`),
  CONSTRAINT `sensors_doors_ibfk_1` FOREIGN KEY (`sensors_id`) REFERENCES `sensors` (`id`),
  CONSTRAINT `sensors_doors_ibfk_2` FOREIGN KEY (`doors_id`) REFERENCES `doors` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `sensors_doors` */

insert  into `sensors_doors`(`sensors_id`,`doors_id`) values (1,1);

/*Table structure for table `sensors_type` */

DROP TABLE IF EXISTS `sensors_type`;

CREATE TABLE `sensors_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `type` (`type`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `sensors_type` */

insert  into `sensors_type`(`id`,`type`,`created`,`modified`) values (1,'Movimiento','2018-02-11 11:40:36','2018-02-11 11:41:38'),(2,'Presencia','2018-02-11 11:41:56','2018-02-11 11:41:56');

/*Table structure for table `special_schedule` */

DROP TABLE IF EXISTS `special_schedule`;

CREATE TABLE `special_schedule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `people_id` int(11) NOT NULL,
  `doors_id` int(11) NOT NULL,
  `date_init` datetime NOT NULL,
  `date_end` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `people_id` (`people_id`),
  KEY `doors_id` (`doors_id`),
  CONSTRAINT `special_schedule_ibfk_1` FOREIGN KEY (`people_id`) REFERENCES `people` (`id`),
  CONSTRAINT `special_schedule_ibfk_2` FOREIGN KEY (`doors_id`) REFERENCES `doors` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `special_schedule` */

insert  into `special_schedule`(`id`,`people_id`,`doors_id`,`date_init`,`date_end`) values (2,3,3,'2018-02-18 01:59:00','2018-02-19 23:00:00'),(3,31,1,'2018-03-19 08:00:00','2018-03-19 14:00:00'),(4,30,1,'2018-03-19 15:30:00','2018-03-20 21:00:00'),(5,46,2,'2018-03-19 20:00:00','2018-03-19 23:00:00'),(6,31,1,'2018-03-19 20:00:00','2018-03-19 21:50:00');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles_id` int(11) NOT NULL,
  `people_id` int(11) NOT NULL,
  `users_state_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user` (`user`),
  KEY `people_id` (`people_id`),
  KEY `roles_id` (`roles_id`),
  KEY `users_state_id` (`users_state_id`),
  CONSTRAINT `users_ibfk_1` FOREIGN KEY (`people_id`) REFERENCES `people` (`id`),
  CONSTRAINT `users_ibfk_2` FOREIGN KEY (`roles_id`) REFERENCES `roles` (`id`),
  CONSTRAINT `users_ibfk_3` FOREIGN KEY (`users_state_id`) REFERENCES `users_state` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`user`,`password`,`roles_id`,`people_id`,`users_state_id`,`created`,`modified`) values (1,'pedrito','c20ad4d76fe97759aa27a0c99bff6710',1,2,1,'2018-02-14 08:23:20','2018-02-14 08:23:20');

/*Table structure for table `users_state` */

DROP TABLE IF EXISTS `users_state`;

CREATE TABLE `users_state` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `state` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `state` (`state`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `users_state` */

insert  into `users_state`(`id`,`state`,`created`,`modified`) values (1,'Activo a','2018-02-12 08:03:38','2018-02-12 08:06:49'),(2,'Inactivo','2018-02-12 08:03:47','2018-02-12 08:03:47'),(3,'Vacaciones','2018-02-12 08:03:58','2018-02-12 08:03:58'),(4,'estado a','2018-02-12 08:04:21','2018-02-12 08:04:21'),(5,'estado b','2018-02-12 08:04:28','2018-02-12 08:04:28');

/*Table structure for table `vehicles` */

DROP TABLE IF EXISTS `vehicles`;

CREATE TABLE `vehicles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `patent` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `internal` tinyint(1) NOT NULL,
  `nfc_code` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT '0',
  `companies_id` int(11) NOT NULL,
  `people_id` int(11) NOT NULL,
  `vehicles_type_id` int(11) NOT NULL,
  `vehicles_profiles_id` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `people_id` (`people_id`),
  KEY `vehicles_type_id` (`vehicles_type_id`),
  KEY `vehicles_profiles_id` (`vehicles_profiles_id`),
  KEY `companies_id` (`companies_id`),
  CONSTRAINT `vehicles_ibfk_1` FOREIGN KEY (`people_id`) REFERENCES `people` (`id`),
  CONSTRAINT `vehicles_ibfk_2` FOREIGN KEY (`vehicles_type_id`) REFERENCES `vehicles_type` (`id`),
  CONSTRAINT `vehicles_ibfk_3` FOREIGN KEY (`vehicles_profiles_id`) REFERENCES `vehicles_profiles` (`id`),
  CONSTRAINT `vehicles_ibfk_4` FOREIGN KEY (`companies_id`) REFERENCES `companies` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `vehicles` */

insert  into `vehicles`(`id`,`patent`,`model`,`internal`,`nfc_code`,`companies_id`,`people_id`,`vehicles_type_id`,`vehicles_profiles_id`,`created`,`modified`) values (1,'111111','charger',1,'0',2,2,1,1,'2018-02-11 10:15:23','2018-02-11 10:15:23'),(2,'222222','camaro ss',0,'0',1,2,1,6,'2018-02-11 04:38:05','2018-02-11 04:38:05'),(3,'333333','carreta',1,'0',2,2,2,5,'2018-02-11 02:16:48','2018-02-11 02:16:48'),(4,'444444','bici',1,'0',1,2,7,3,'2018-02-11 02:17:08','2018-02-11 02:17:08'),(5,'555555','z1',1,'0',2,2,3,5,'2018-02-11 02:18:48','2018-02-11 02:18:48'),(6,'666666','z4',1,'0',3,2,1,2,'2018-02-11 02:19:05','2018-02-11 02:19:05');

/*Table structure for table `vehicles_drivers` */

DROP TABLE IF EXISTS `vehicles_drivers`;

CREATE TABLE `vehicles_drivers` (
  `vehicles_id` int(11) NOT NULL,
  `people_id` int(11) NOT NULL,
  PRIMARY KEY (`vehicles_id`,`people_id`),
  KEY `people_id` (`people_id`),
  CONSTRAINT `vehicles_drivers_ibfk_1` FOREIGN KEY (`vehicles_id`) REFERENCES `vehicles` (`id`),
  CONSTRAINT `vehicles_drivers_ibfk_2` FOREIGN KEY (`people_id`) REFERENCES `people` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `vehicles_drivers` */

insert  into `vehicles_drivers`(`vehicles_id`,`people_id`) values (2,3);

/*Table structure for table `vehicles_profiles` */

DROP TABLE IF EXISTS `vehicles_profiles`;

CREATE TABLE `vehicles_profiles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `profile` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `profile` (`profile`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `vehicles_profiles` */

insert  into `vehicles_profiles`(`id`,`profile`,`created`,`modified`) values (1,'perfil 1','2018-02-10 07:14:30','2018-02-10 07:14:30'),(2,'perfil 2','2018-02-10 07:14:39','2018-02-10 07:14:39'),(3,'perfil 3','2018-02-10 07:14:47','2018-02-10 07:14:47'),(4,'perfil 4','2018-02-10 07:14:55','2018-02-10 07:14:55'),(5,'perfil 5','2018-02-10 07:15:00','2018-02-10 07:19:55'),(6,'perfil 6','2018-02-10 07:15:05','2018-02-10 07:15:05');

/*Table structure for table `vehicles_type` */

DROP TABLE IF EXISTS `vehicles_type`;

CREATE TABLE `vehicles_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `type` (`type`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `vehicles_type` */

insert  into `vehicles_type`(`id`,`type`,`created`,`modified`) values (1,'Automovil','2018-02-10 06:33:37','2018-02-10 06:39:44'),(2,'Camioneta','2018-02-10 06:34:19','2018-02-10 06:34:19'),(3,'Furgon','2018-02-10 06:34:26','2018-02-11 02:19:45'),(4,'Camión 3/4','2018-02-10 06:35:10','2018-02-10 06:35:10'),(5,'Camión acoplado','2018-02-10 06:35:38','2018-02-10 06:35:38'),(6,'Camión rampla','2018-02-10 06:35:47','2018-02-10 06:35:47'),(7,'Camión cisterna','2018-02-10 06:36:11','2018-02-10 06:36:11');

/*Table structure for table `zones` */

DROP TABLE IF EXISTS `zones`;

CREATE TABLE `zones` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `zone` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `zone` (`zone`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `zones` */

insert  into `zones`(`id`,`zone`,`created`,`modified`) values (1,'Zona 1','2018-02-09 01:49:41','2018-02-09 01:49:41'),(2,'Zona 2','2018-02-10 02:52:55','2018-02-10 02:52:55'),(3,'Zona 3','2018-02-10 02:53:03','2018-02-10 02:53:03'),(4,'Zona 4','2018-02-10 02:53:08','2018-02-10 02:53:08'),(5,'Zona 5','2018-02-10 02:53:14','2018-02-10 02:53:14'),(7,'Zona 7','2018-02-10 02:53:44','2018-02-10 02:53:44');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
